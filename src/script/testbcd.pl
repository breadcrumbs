#!/usr/bin/perl -w

# This file is part of the breadcrumbs tester (testbcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#ok, let's try to make some calls 


use IO::Socket;
use Getopt::Long;
use Pod::Usage;
use FindBin;
use constant EOL => "\015\012";
use Data::Dumper;

use lib("$FindBin::Bin/../lib");
use Bcd::Commands::CommandParser;
use Bcd::Errors::ErrorCodes;
use Storable qw(thaw freeze);

my $test = 0;
my $help = 0;
my $blob = 0;
my $for_ever = 0;
my $interactive = 0;

#let's see if the user wants to test...
GetOptions(
	   'test'             => \$test,
	   'help'             => \$help,
	   'blob'             => \$blob,
	   'forever'          => \$for_ever,
	   'interactive'      => \$interactive,
	   );

if ($for_ever){
    print "using the forever feature, force the blob\n";
    $blob = 1;
}

if ($blob) {
    print "using the blob feature, force the test server\n";
    $test = 1;
}


pod2usage(2) if $help;

my $file_to_process = shift(@ARGV);
if (!defined($file_to_process)){
    $file_to_process = "$FindBin::Bin/test_script";
    print "No file to process given, defaulting to $file_to_process\n";
} else {
    print "Processing $file_to_process\n";
}

my $PORT = 9000;                  # pick something not in use

if ($test){
    $PORT = 9001;
    print "----- Using test database I will use port $PORT ----\n";
} else {
    print "----- Using normal db I will connect to port $PORT ----\n";
}



my $sock = new IO::Socket::INET ( 
				  PeerAddr => 'localhost', 
				  PeerPort => $PORT,
				  Proto => 'tcp',
				  );

die "Could not create socket: $!\n" unless $sock;


#get the answer
{
    local $/ = "\015\012";
    my $greeting = <$sock>;
    print $greeting;
    while ((my $ans = <$sock> )=~ /BCD busy/){
	print "I will wait, ok: server sent me $ans\n";
    }
}

use Data::Dumper;

#my $CURRENT_SESSION="";

#this function simply gets the array of outputs
sub get_output_from_socket{

    my $output = "";
    local $/ = "\015\012";

    #first line is the mode
    my $stream_mode = <$sock>;
    print "The mode is $stream_mode\n";

    my $text = 0;
    my $compressed = 0;

    if ($stream_mode =~ /t/){
	$text = 1;
    } elsif ($stream_mode =~ /c/){
	$compressed = 1;
    }

    while(1){
	my $answer = <$sock>;
	if (!defined($answer)){
	    die "closed socket from remote server\n";
	}
	chomp $answer;
	if ($answer =~ /^.$/){
	    last;
	}
	#push (@output, $answer);
	$output .= $answer;

    }
    
    #ok, in the end I should simply restore the output...
    my $res;

    if ($text == 1){
	$res = eval("no strict; $output");
    } else {
	#I should get the binary...
	use MIME::Base64;
	use Compress::Zlib;
	my $decoded = decode_base64($output);

	my $stream;
	if ($compressed == 1){
	    #ok, I should decompress it!
	    $stream = Compress::Zlib::memGunzip($decoded);
	} else {
	    $stream = $decoded;
	}

	#my $res = eval("no strict; $output");
	$res = thaw $stream;
    }

    return $res;
}

sub print_output{
    my $output = shift;

    my $line;
    while (defined($line = shift(@{$output}))){
	print $line . "\n";
    }
}

my %outputs_stored;

sub process_input_line{
    my $line = shift;

    if ($line =~ "^--OUTPUT--"){
	my @arr = split (" @ ", $line);
	return $outputs_stored{$arr[1]};
    } else {
	return $line;
    }
}

if ( $blob ) {

    print $sock '\b' . EOL;
    my $ans = <$sock>;
    print $ans;

    if ($for_ever){
	my $i = 1;
	while(1){
	    send_a_blob();
	    print "$i\n";
	    $i++;
	}
    } else {
	for(1..100)
	{
	    send_a_blob();
	    print "$_\n";
	}
	exit(1);
    }

}

use Digest::SHA1;
use MIME::QuotedPrint;

sub send_a_blob{

    print $sock "tc_receive_blob" . EOL;
    
    #let's generate a test blob...
    my @set = ('\012', '\015', '.', 'é', 'à', 'ì', 'a' .. 'z', 'A' .. 'Z', 0 .. 9 );

    my $blob = "";
    foreach(1..1000){
	$blob .= @set[rand($#set+1)];
    }

    #$blob .= "\n\n\n";
    $blob = 'eot\n' . $blob;

    #ok, let's now compute the hash...

    my $sha = Digest::SHA1->new;
    $sha->add($blob);
    my $digest = $sha->hexdigest;

    #now let's quote print this blob
#    my $encoded = MIME::QuotedPrint::encode_qp($blob, "\015\012");

    #copied from Net::Cmd.... :)
#     $encoded =~ s/^(\.?)/$1$1/sg; #also at the start of the string...
#     $encoded =~ s/^(eot)/$1$1/sg; #also at the start of the string...
#     $encoded =~ s/\015?\012(\.?)/\015\012$1$1/sg;
#     $encoded =~ s/\015?\012(eot)/\015\012$1$1/sg;

#     print $sock $digest . EOL;
#     #print "$digest\n";
#     print $sock "dummy" . EOL;
#     #print "dummy\n";
#     print $sock $encoded . EOL;
#     #print "$encoded\n";
#     print $sock "." . EOL;
#     #print ".\n";
#     print $sock "dummy" . EOL;
#     #print "dummy\n";
#     print $sock "eot" . EOL;

    my @args = ($digest, "eot", $blob, "eot");

    my $dumped_u = freeze \@args;
    my $must_compress = 0;
    my $output_serialized;

    #a reasonable limit
    my $mode;
    if (length($dumped_u) > 200){
	$must_compress = 1;
	$mode = "c";
    } else {
	$mode = "u";
    }

    if ($must_compress == 1){
	my $dumped_c = Compress::Zlib::memGzip($dumped_u);
	$output_serialized = encode_base64($dumped_c, EOL);
    } else {
	$output_serialized = encode_base64($dumped_u, EOL);
    }

    $output_serialized =~ s/^(\.?)/$1$1/sg; #also at the start of the string...
    $output_serialized =~ s/\015?\012(\.?)/\015\012$1$1/sg;

    print $sock $mode . EOL          or $self->_dead_socket();
    print $sock $output_serialized   or $self->_dead_socket();
    print $sock "." . EOL            or $self->_dead_socket();

    my $output = get_output_from_socket();
    #print Dumper($output);
    my $res = Bcd::Commands::CommandParser->parse_command_output($output);
    #print Dumper($res);
    #print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n";

    if ($res->{exit_code} != 0){
	print "the blob was \n\n$blob\n\n";
	print "the encoding was \n\n$encoded\n\n";
	die "success expected\n";
    }

}


my $infile;
if ($interactive == 0){
    open ($infile, "<$file_to_process") or die "NOT FOUND: $file_to_process";
} else {
    $infile = STDIN;
}
$i = 1;
$first_line_of_command = 1;
my $success_required = 0;
my %outputs_to_parse;

my @args;
my $procesed_line;

while(<$infile>){

    #print "La riga e` $_";

    if (/^#/){
	#is a comment
	#print "Commento, pussa via\n";
	next;
    }

    #ONLY if I am before the first line of a command I search for a parametric command
    if ($first_line_of_command){


	#only between commands the spaces are not meaningful
	if (/^\s+$/){
	    #print "spazio vuoto, pussa via\n";
	    next;
	}



	#ok, I try to handle the commands which are parametric, like connect/disconnect
# 	if (handle_parametric_command($_)){
# 	    next;
# 	}

	chomp;

	if (/^\\(?)/){
	    #it is a slash command, I send it to the server
	    print $sock $_ . EOL;
	    my $ans = <$sock>;
	    print "Server's answer is $ans\n";
	    next;
	} else {
	    print "this is not a slash command\n";
	}

	if ( $_ eq "--PLEASE_WAIT_HERE--"){
	    print "Ok, I will wait... press a key....\n";
	    <STDIN>;
	    next;
	}

	#i should see if the user wants to parse the command
	@args = split (' ', $_);

	#print "Questa e` la riga che ho diviso\n";
	#print Dumper(\@args);

	$success_required = 1;
	%outputs_to_parse = ();

	if ($#args != 0){

	    my $starting_index = 1;

	    #nothing to do, the command is a simple string
	    if ($args[1] eq "!"){
		#I want to be sure that the command succedes
		#print "I want success!\n";
		$success_required = 0;
		$starting_index = 2;
	    }

	    my @pairs = splice (@args, $starting_index);

	    my $odd = 1;
	    my $output_key;
	    foreach(@pairs){
		#print "I have $_\n";

		if ($odd){
		    $output_key = $_;
		    $odd = 0;
		} else {
		    $outputs_to_parse{$output_key} = $_;
		    $odd = 1;
		}
	    }
	    
	}

	#print "Io devo prendere queste uscite\n";
	#print Dumper(\%outputs_to_parse);

	$processed_line = $args[0];

    } else {
	chomp;
	$processed_line = process_input_line($_);
    }



    print "*********** BEGIN OF Command: $i \n" if $first_line_of_command;
    $first_line_of_command = 0;



    print "$. : $processed_line\n";
    print $sock $processed_line . EOL;

    if ($_ ne "."){
	next;
    }

    print "********* result *************\n";

    my $output = get_output_from_socket();

    if ( $success_required || ($interactive == 1)){
	#I should parse the output
	my $res = Bcd::Commands::CommandParser->parse_command_output($output);
	
	print Dumper($res);

	if ($res->{exit_code} != Bcd::Errors::ErrorCodes::BEC_OK and $interactive == 0){
	    die "Success required, but got $res->{exit_code}\n";
	}

	foreach (keys(%outputs_to_parse)){
	    #ok, I should store the output
	    $outputs_stored{$outputs_to_parse{$_}} = $res->{$_};
	}
	#print "ecco le mie uscite\n";
	#print Dumper(\%outputs_stored);
    } else {
	#no success required, I simply print the output
	print_output($output);
    }

    print "*********** END OF Command: $i \n";
    $i ++;
    $first_line_of_command = 1;
}

$i--;
print "Processed $i commands \n";

close($sock);
close(INFILE);


