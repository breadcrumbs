#!/usr/bin/perl -w

# This file is part of the breadcrumbs project (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this file should simply manage the sessions in the cache

use strict;
use warnings;

use Cache::FastMmap;
use Data::Dumper;

use Getopt::Long;
my $test = 0;

GetOptions(
	   'test'             => \$test,
	   );

my $share_file;
if ($test == 1){
    $share_file = "/tmp/TEST_bcd_cache";
} else {
    $share_file = "/tmp/bcd_cache";
}

my $cache = Cache::FastMmap->new(
				 share_file => $share_file,
				 );

my @keys = $cache->get_keys(2);

foreach (@keys){
    print Dumper ($_);
}

#my $value = $cache->get($key);
#print Dumper($value);



