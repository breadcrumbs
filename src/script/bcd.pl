#!/usr/bin/perl -w

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use FindBin;
use lib("$FindBin::Bin/../lib");


use constant {
    BCD_VERSION => '0.10.2.3',
};


use Bcd;

Bcd::Data::Model->instance();
my $server = Bcd->new();
$server->run(conf_file => './etc/bcd.conf');

sub greetings{

    my $ver = BCD_VERSION();
    my $greetings = "bcd version $ver, Copyright (C) 2007 Pasqualino Ferrentino\n";
    $greetings .=   "bcd comes with ABSOLUTELY NO WARRANTY; for details\n";
    $greetings .=   "see the file COPYING in the distribution directory.\n";
    $greetings .=   "This is free software, and you are welcome\n";
    $greetings .=   "to redistribute it under certain conditions;\n";
    $greetings .=   "see the file COPYING for details.\n";

    return $greetings;

}

__END__

=head1 NAME

    bcd.pl - Breadcrumbs daemon (Bcd)

=head1 SYNOPSIS

    bcd.pl [options]

 Options:
    --help           display this help and exits
    --test           use test database and test port (usually 9001)
    --script         write all the commands in a script file

 The script option implies the test option.
 Other options should be the same of Net::Server (Bcd is a subclass of it)

 Some useful options:

    --setsid         daemonize
    --user <user>    run with user <user>
    --group <group>  run with group <group>

=head1 DESCRIPTION

Run the bcd daemon, you should use it to run the breadcrumbs site.

=head1 AUTHOR

Pasqualino Ferrentino <lino.ferrentino@yahoo.it>

=head1 COPYRIGHT

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

=cut
