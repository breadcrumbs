#!/usr/bin/perl -w

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

# This file is simply to generate a randomize set of names
# from a file of real names, internal use...

#very sloppy code follows, be careful.

use strict;
use warnings;

open (REALF, "<real_names") or die "help!\n";
open (RANDOM, ">randomized") or die "help!\n";

my @first_names;
my @last_names;

my $size = 0;

while(<REALF>){
    chomp;
    next if (/^$/);
    my @arr = split (/#/);
    push(@first_names, $arr[0]);
    push(@last_names, $arr[1]);
		     $size++;
}

    while (scalar(@first_names) != 0){

	my $choice_f = int(rand($#first_names+1));
	my $choice_l = int(rand($#last_names+1));

	my $first = $first_names[$choice_f];
	my $last = $last_names[$choice_l];

	splice(@first_names, $choice_f, 1);
	splice(@last_names, $choice_l, 1);

	print RANDOM "$first | $last\n";
    }

    close (RANDOM);

close (REALF);
