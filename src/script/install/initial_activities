#
# script file to populate the initial activities
#
# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).
#

####################################################################
casa % Casa e famiglia % ORIG_SERVICE


aiuti_dom % Aiuto faccende domestiche % casa

pulizie % Pulizie domestiche normali % aiuti_dom
stiro   % Stiro % aiuti_dom
traslochi % Aiuto per sgomberi e traslochi % aiuti_dom


bambini % Bambini % casa

baby_sitter_piccoli % Baby sitter per bambini 0-3 anni % bambini
baby_sitter_medi % Baby sitter per bambini sopra i 3 anni % bambini
lett_fiabe % Lettura fiabe % bambini
anim_feste % Animazione feste per bambini % bambini
cons_all % Consulenza allattamento al seno e puericultura % bambini

animali % Animali % casa

dog_sitting % Dog/cat sitting per meno di un giorno % animali
dog_training % Dog/cat sitting per più giorni % animali
altri_animali % Cura di altri animali % animali

piante % Piante % casa

innaffiare % Innaffiare piante % piante
consulenza_giardinaggio % Aiuto giardinaggio % piante
taglio_erba % Taglio erba % piante
potatura % Potatura ed altri lavori in giardino % piante

anziani % Anziani ed assistenza % casa

anziani_assistenza % Assistenza anziani autosufficienti % anziani
non_autosuff % Assistenza anziani non autosufficienti % anziani
punture % Servizi di infermeria % anziani

bricolage % Bricolage, fai-da-te % casa

idraulica % Idraulica % bricolage
elettricista % Elettricista, antennista % bricolage
montaggio_mobili % Montaggio mobili % bricolage
muratore % Lavori in muratura % bricolage
varie % Vari altri lavori di manutenzione casa % bricolage


info_elettronica % Informatica ed elettronica % casa

ser_ass_comp % Assistenza generica computer % info_elettronica
ser_ass_vcr % Configurazione video registratori, decoder ecc.. % info_elettronica
ser_ass_alf % Alfabetizzazione informatica per adulti % info_elettronica


cura_persona % Cura della persona % casa

parrucchiere_donna % Parrucchiere donna % cura_persona
estetista % Estetista % cura_persona
manicure % Manicure/Pedicure % cura_persona
massaggi % Massaggi per donna % cura_persona
barbiere % Barbiere uomo % cura_persona
massaggi_uomo % Massaggi per uomo % cura_persona
truccatrice % Trucco % cura_persona

################################################################################

campagna % Agricoltura, allevamento, lavori "vintage" % ORIG_SERVICE

agri % Agricoltura % campagna

lavori_orto % Lavori nell'orto % agri
conserve % Lezioni su marmellate e confetture % agri
sotto_sale % Lezioni su preparazioni sotto sale % agri
sotto_olio % Lezioni su preparazioni sott'olio % agri


lavori_dimenticati % Lavori della campagna "antichi" % campagna

lez_sapone % Lezioni per fare il sapone casalingo % lavori_dimenticati
lez_lana % Lezioni per cardare e filare la lana % lavori_dimenticati
lez_pelli % Lezioni per conciare pelli % lavori_dimenticati
lez_carro % Riparazioni carri ed altri attrezzi % lavori_dimenticati
lez_armi % Uso e riparazione armi antiche % lavori_dimenticati


allevamento % Allevamento % campagna

aiuto_allevamento_pic % Aiuto allevamento animali da cortile % allevamento
aiuto_all_gro % Pascolo % allevamento
miele % Lezioni di apicoltura % allevamento
macellazione_pic % Aiuto per la macellazione di piccoli  animali % allevamento
macellazione_gro % Aiuto per la macellazione di grossi animali  % allevamento
insaccati % Aiuto nella preparazione insaccati % allevamento
formaggi % Aiuto nella preparazione formaggi % allevamento




################################################################

studio % Studio e servizi culturali % ORIG_SERVICE


lezioni % Lezioni % studio
lez_compiti % Aiuto per compiti bambini elementari/medie % lezioni
lez_sup % Ripetizioni scuole superiori % lezioni
lez_uni % Ripetizioni livello universitario % lezioni
lez_lingue % Lezioni di lingue % lezioni
lez_met % Metodi di studio % lezioni
lez_civ % Educazione civica per bambini % lezioni
lez_ses % Educazione sessuale per bambini/adolescenti % lezioni
lez_eco % Educazione ecologica per bambini/adolescenti % lezioni

###### da spostare lez_man % Lavori manuali per bambini % lezioni


serv_vari % Servizi vari letteratura  % studio

battitura % Battitura tesi ed altro % serv_vari
editing % Editing e correttura bozze % serv_vari
ricerche % Ricerche bibliografiche % serv_vari

serv_cult % Servizi culturali % studio

conversazioni_in_lingua % Conversazioni in lingua straniera % serv_cult
cons_rel % Consulenza religiosa % serv_cult
cons_fil % Consulenza e dialoghi filosofici % serv_cult
cons_psi % Consulenza psicologica % serv_cult
cons_fin % Consulenza finanziaria economica % serv_cult
cons_leg % Consulenza legale / condominio % serv_cult
cons_mat % Consulenza matrimoniale % serv_cult


##
##
##                     oggetti fatti in casa
##
##

######################
###alimentari

alimentari % Alimentari % ORIG_HOMEMADE

pasticceria % Pasticceria % alimentari

torte_di_compleanno % Torte di compleanno % pasticceria
biscotti % Biscotti e simili % pasticceria
torte_normali % Torte normali % pasticceria
budini_e_altro % Budini ed altri dolci al cucchiaio % pasticceria
gelati % Gelati e semifreddi % pasticceria
brioche % Brioches ed altri dolcetti secchi % pasticceria
pasticcini % Pasticcini o dolcetti con crema % pasticceria

ar_al_conserve % Conserve alimentari % alimentari

marmellate % Marmellate e confetture % ar_al_conserve
sotto_olio % Conserve sott'olio % ar_al_conserve
sotto_sale % Conserve sotto sale % ar_al_conserve
sotto_spirito % Conserve sotto spirito % ar_al_conserve
salsa_pomodoro % Salsa di pomodoro % ar_al_conserve
insaccati % Insaccati ed altre conserve di carne % ar_al_conserve

ar_al_latticini % Latticini % alimentari

formaggio % Formaggi % ar_al_latticini
burro % Burro % ar_al_latticini
yogurt % Yogurt % ar_al_latticini
panna % Panna % ar_al_latticini

ar_al_prodotti % Prodotti di fattoria % alimentari

uova % Uova % ar_al_prodotti
frutta % Frutta % ar_al_prodotti
verdura % Verdura % ar_al_prodotti
latte % Latte % ar_al_prodotti
animali_da_cortile % Carne di animali da cortile % ar_al_prodotti

ar_al_rosticceria % Prodotti di rosticceria % alimentari

primi_piatti % Primi piatti % ar_al_rosticceria
secondi_piatti % Secondi piatti % ar_al_rosticceria
contorni % Contorni % ar_al_rosticceria
pasti_completi % Pasti completi % ar_al_rosticceria

ar_al_panificazione % Pane e affini % alimentari

pane % Pane % ar_al_panificazione
pani_speciali % Pani speciali % ar_al_panificazione
biscotti_cracker % Biscotti o cracker salati % ar_al_panificazione


ar_al_vini_e_affini % Vino e affini % alimentari

vino % Vino % ar_al_vini_e_affini
grappa_e_distillati % Grappa e distillati % ar_al_vini_e_affini
liquori % Liquori casalinghi % ar_al_vini_e_affini
birra % Birra % ar_al_vini_e_affini


#########################
###non alimentari

non_alimentari % Non alimentari % ORIG_HOMEMADE


abbigliamento_art % Abbigliamento % non_alimentari

abiti_su_misura % Abiti su misura % abbigliamento_art
maglieria % Maglieria a mano % abbigliamento_art
aggiusti % Aggiusti di sartoria % abbigliamento_art
scarpe % Scarpe o sandali artigianali % abbigliamento_art
abiti_carnevale % Abiti per carnevale o feste artigianali % abbigliamento_art
abiti_cerimonia % Abiti per cerimonia artigianali % abbigliamento_art


ar_na_decorazione % Lavori di decorazione % non_alimentari

uncinetto % Lavori all'uncinetto % ar_na_decorazione
ricamo % Ricami % ar_na_decorazione
decoupage % Decoupage % ar_na_decorazione
pasta_sale % Sculture in pasta di sale % ar_na_decorazione
piatti_tazze % Tazze e piatti decorati % ar_na_decorazione
lavori_vari % Altri lavori % ar_na_decorazione
quadri % Quadri % ar_na_decorazione
sculture % Sculture % ar_na_decorazione
gioielli % Gioielli, monili, spille % ar_na_decorazione


ar_na_prodotti % Oggetti o prodotti per la casa % non_alimentari

sapone % Saponi naturali o "aromatici" % ar_na_prodotti
candele % Candele % ar_na_prodotti
ceste % Ceste o cestini % ar_na_prodotti
cuscini % Cuscini e materassi % ar_na_prodotti
tende % Tende % ar_na_prodotti
lampade % Lampade e lampadari % ar_na_prodotti
complementi_arredo % Complementi d'arredo % ar_na_prodotti
vasi % Vasi, piatti e altro % ar_na_prodotti
mobili_artigianali % Mobili artigianali % ar_na_prodotti
ar_na_prodotti_oro % Orologi % ar_na_prodotti
oggetti_decoro % Altri oggetti da decoro % ar_na_prodotti


ar_na_letteratura % Prodotti "letterari" % non_alimentari

poesie % Poesie % ar_na_letteratura
racconti % Racconti e fiabe % ar_na_letteratura
lettere % Lettere non personali % ar_na_letteratura
lettere_amore % Lettere personali % ar_na_letteratura


############################################################
############################################################
## oggetti usati
############################################################
############################################################

#######################
## abbigliamento 
us_abb % Abbigliamento % ORIG_USED


us_abb_uomo % Abbigliamento per uomo % us_abb

us_abb_uomo_cam % Camicie % us_abb_uomo
us_abb_uomo_pan % Pantaloni % us_abb_uomo
us_abb_uomo_gia % Giacche % us_abb_uomo
us_abb_uomo_ves % Vestiti completi % us_abb_uomo
us_abb_uomo_sca % Scarpe normali % us_abb_uomo
us_abb_uomo_scp % Scarpe per uso speciale (sport, hobby) % us_abb_uomo
us_abb_uomo_spo % Abbigliamento sportivo (tute, altro) % us_abb_uomo
us_abb_uomo_mag % Magliette % us_abb_uomo
us_abb_uomo_pul % Pullover e maglioni % us_abb_uomo
us_abb_uomo_cer % Abiti da cerimonia % us_abb_uomo


us_acc_uomo % Accessori uomo % us_abb

us_acc_uomo_cin % Cinture % us_acc_uomo
us_acc_uomo_cra % Cravatte % us_acc_uomo
us_acc_uomo_bor % Borse % us_acc_uomo
us_acc_uomo_por % Portafogli % us_acc_uomo
us_acc_uomo_occ % Occhiali da sole % us_acc_uomo
us_acc_uomo_omb % Ombrelli % us_acc_uomo


us_abb_donna % Abbigliamento donna % us_abb

us_abb_donna_cam % Camicette % us_abb_donna
us_abb_donna_gon % Gonne % us_abb_donna
us_abb_donna_tai % Taillers % us_abb_donna
us_abb_donna_gia % Giacche % us_abb_donna
us_abb_donna_pan % Pantaloni % us_abb_donna
us_abb_donna_mag % Magliette % us_abb_donna
us_abb_donna_ves % Vestiti % us_abb_donna
us_abb_donna_cer % Abiti da cerimonia % us_abb_donna
us_abb_donna_spo % Abiti da sposa % us_abb_donna
us_abb_donna_int % Intimo % us_abb_donna
us_abb_donna_cos % Costumi da bagno % us_abb_donna
us_abb_donna_sca % Scarpe normali % us_abb_donna
us_abb_donna_sce % Scarpe eleganti % us_abb_donna


us_acc_donna % Accessori donna % us_abb

us_acc_donna_cin % Cinture % us_acc_donna
us_acc_donna_bor % Borse % us_acc_donna
us_acc_donna_occ % Occhiali % us_acc_donna
us_acc_donna_por % Portafogli % us_acc_donna
us_acc_donna_cap % Cappelli % us_acc_donna
us_acc_donna_poc % Portatrucchi, pochette % us_acc_donna
us_acc_donna_ful % Foulards, scialli % us_acc_donna
us_acc_donna_omb % Ombrelli % us_acc_donna





us_abb_neonati % Abbigliamento bambini piccoli (0-3) % us_abb

us_abb_neonati_tut % Tutine % us_abb_neonati
us_abb_neonati_pig % Pigiami % us_abb_neonati
us_abb_neonati_mag % Maglie % us_abb_neonati
us_abb_neonati_bod % Body % us_abb_neonati
us_abb_neonati_sca % Scarpe % us_abb_neonati
us_abb_neonati_gia % Giacche % us_abb_neonati
us_abb_neonati_pan % Pantaloni % us_abb_neonati
us_abb_neonati_gon % Gonne % us_abb_neonati
us_abb_neonati_gre % Grembiuli e divise % us_abb_neonati


us_abb_fanciulli % Abbigliamento bambini (4-12) % us_abb

us_abb_fanciulli_int % Intimo maschio % us_abb_fanciulli
us_abb_fanciulli_inf % Intimo femmina % us_abb_fanciulli
us_abb_fanciulli_pan % Pantaloni maschio % us_abb_fanciulli
us_abb_fanciulli_paf % Pantaloni femmina % us_abb_fanciulli
us_abb_fanciulli_gon % Gonne % us_abb_fanciulli
us_abb_fanciulli_gre % Grembiuli e divise % us_abb_fanciulli
us_abb_fanciulli_scm % Scarpe maschio % us_abb_fanciulli
us_abb_fanciulli_scf % Scarpe femmina % us_abb_fanciulli
us_abb_fanciulli_mag % Maglie maschio % us_abb_fanciulli
us_abb_fanciulli_maf % Maglie femmina % us_abb_fanciulli
us_abb_fanciulli_ves % Vestiti maschio % us_abb_fanciulli
us_abb_fanciulli_vef % Vestiti femmina % us_abb_fanciulli
us_abb_fanciulli_gia % Giacche maschio % us_abb_fanciulli
us_abb_fanciulli_gif % Giacche femmina % us_abb_fanciulli
us_abb_fanciulli_tut % Tute sportive maschio % us_abb_fanciulli
us_abb_fanciulli_tuf % Tute sportive femmina % us_abb_fanciulli


us_acc_fanciulli % Accessori bambini % us_abb

us_acc_fanciulli_zai % Zaini % us_acc_fanciulli
us_acc_fanciulli_cap % Cappelli, sciarpe, guanti % us_acc_fanciulli
us_acc_fanciulli_cin % Cinture, fiocchi, cravatte % us_acc_fanciulli
us_acc_fanciulli_cos % Costumi da bagno % us_acc_fanciulli
us_acc_fanciulli_cer % Cerchietti, spille, monili vari % us_acc_fanciulli


#######################
## oggetti per la casa
us_casa % Oggetti per la casa % ORIG_USED


us_casa_cucina % In cucina % us_casa

us_casa_cucina_sto % Stoviglie % us_casa_cucina
us_casa_cucina_pos % Posate % us_casa_cucina
us_casa_cucina_pen % Pentole % us_casa_cucina
us_casa_cucina_bic % Bicchieri % us_casa_cucina
us_casa_cucina_mes % Mestoli, forchettoni, cucchiai da cucina % us_casa_cucina
us_casa_cucina_sco % Scolapasta, vaporiere, altri accessori % us_casa_cucina


us_casa_non_ele % Accessori non elettrici % us_casa

us_casa_non_ele_bil % Bilance non elettriche % us_casa_non_ele
us_casa_non_ele_pas % Passaverdura % us_casa_non_ele
us_casa_non_ele_tri % Tritacarne % us_casa_non_ele
us_casa_non_ele_tap % Tappabottiglie % us_casa_non_ele
us_casa_non_ele_gel % Gelatiere non elettriche % us_casa_non_ele
us_casa_non_ele_alt % Altre cose non elettriche per la cucina % us_casa_non_ele


#######################
## elettrodomestici
us_elettrodomestici % Elettrodomestici % ORIG_USED


us_elettrodomestici_pic % Elettrodomestici trasportabili % us_elettrodomestici

us_elettrodomestici_pic_fru % Frullatori, mixer % us_elettrodomestici_pic
us_elettrodomestici_pic_toa % Tostapane % us_elettrodomestici_pic
us_elettrodomestici_pic_bis % Bistecchiere % us_elettrodomestici_pic
us_elettrodomestici_pic_fri % Friggitrici % us_elettrodomestici_pic
us_elettrodomestici_pic_vap % Vaporiere % us_elettrodomestici_pic
us_elettrodomestici_pic_pan % Macchine per il pane % us_elettrodomestici_pic
us_elettrodomestici_pic_sti % Ferri da stiro % us_elettrodomestici_pic
us_elettrodomestici_pic_asc % Asciugacapelli % us_elettrodomestici_pic
us_elettrodomestici_pic_bil % Bilance elettroniche per alimenti % us_elettrodomestici_pic
us_elettrodomestici_pic_bip % Bilance elettroniche per persone % us_elettrodomestici_pic
us_elettrodomestici_pic_imp % Impastatrici % us_elettrodomestici_pic
us_elettrodomestici_pic_gel % Gelatiere % us_elettrodomestici_pic
us_elettrodomestici_pic_vuo % Macchine per sottovuoto % us_elettrodomestici_pic
us_elettrodomestici_pic_asp % Aspirapolvere % us_elettrodomestici_pic
us_elettrodomestici_pic_pva % Pulitrici a vapore % us_elettrodomestici_pic
us_elettrodomestici_pic_for % Forni elettrici/microonde % us_elettrodomestici_pic


us_elettrodomestici_hob % Elettrodomestici per hobby e bricolage % us_elettrodomestici

us_elettrodomestici_hob_tra % Trapani % us_elettrodomestici_hob
us_elettrodomestici_hob_sme % Smerigliatrici % us_elettrodomestici_hob
us_elettrodomestici_hob_fre % Frese % us_elettrodomestici_hob
us_elettrodomestici_hob_idr % Idropulitrici % us_elettrodomestici_hob
us_elettrodomestici_hob_sal % Saldatrici a elettrodo % us_elettrodomestici_hob
us_elettrodomestici_hob_sas % Saldatori per stagno % us_elettrodomestici_hob
us_elettrodomestici_hob_seg % Seghe elettriche % us_elettrodomestici_hob
us_elettrodomestici_hob_dem % Demolitori % us_elettrodomestici_hob
us_elettrodomestici_hob_tes % Tester % us_elettrodomestici_hob
us_elettrodomestici_hob_com % Compressori % us_elettrodomestici_hob


us_elettrodomestici_gra % Elettrodomestici "grandi" % us_elettrodomestici

us_elettrodomestici_gra_lav % Lavatrici % us_elettrodomestici_gra
us_elettrodomestici_gra_fri % Frigoriferi % us_elettrodomestici_gra
us_elettrodomestici_gra_las % Lavastoviglie % us_elettrodomestici_gra
us_elettrodomestici_gra_asc % Asciugatrici % us_elettrodomestici_gra
us_elettrodomestici_gra_cuc % Cucine a gas % us_elettrodomestici_gra
us_elettrodomestici_gra_cal % Caldaie % us_elettrodomestici_gra
us_elettrodomestici_gra_con % Condizionatori % us_elettrodomestici_gra



###############################
##### Oggetti per giardino e orto
us_macgiard % Macchine e utensili da giardino e campagna % ORIG_USED


us_macgiard_ut % Utensili da giardino non a motore % us_macgiard

us_macgiard_ut_zap % Zappe, rastrelli, pale % us_macgiard_ut
us_macgiard_ut_seg % Seghe, accette, falci % us_macgiard_ut
us_macgiard_ut_ara % Aratri % us_macgiard_ut

us_macgiard_mac % Macchine per agricoltura % us_macgiard

us_macgiard_mac_dec % Decespugliatori % us_macgiard_mac
us_macgiard_mac_zap % Zappatrici % us_macgiard_mac
us_macgiard_mac_sgm % Seghe a motore % us_macgiard_mac
us_macgiard_mac_tra % Trattori % us_macgiard_mac
us_macgiard_mac_rim % Rimorchi e carri % us_macgiard_mac
us_macgiard_mac_tag % Tagliaerba % us_macgiard_mac
us_macgiard_mac_alt % Altre macchine per agricoltura % us_macgiard_mac



#######################
## arredamento
us_arr % Arredamento e accessori per la casa % ORIG_USED


us_arr_cuc % Cucine e tinelli % us_arr

us_arr_cuc_cuc % Cucine complete % us_arr_cuc
us_arr_cuc_tav % Tavoli da cucina % us_arr_cuc
us_arr_cuc_pen % Pensili % us_arr_cuc
us_arr_cuc_cre % Credenze % us_arr_cuc


us_arr_sal % Salotti % us_arr

us_arr_sal_div % Divani % us_arr_sal
us_arr_sal_pol % Poltrone % us_arr_sal
us_arr_sal_mob % Mobili per la sala % us_arr_sal
us_arr_sal_ang % Angoliere % us_arr_sal
us_arr_sal_bar % Banconi da bar % us_arr_sal
us_arr_sal_tav % Tavoli e sedie % us_arr_sal
us_arr_sal_bib % Biblioteche % us_arr_sal


us_arr_bag % Arredamento bagno % us_arr

us_arr_bag_spe % Specchi % us_arr_bag
us_arr_bag_mob % Mobili da bagno % us_arr_bag
us_arr_bag_doc % Cabine docce % us_arr_bag
us_arr_bag_vas % Vasche normali % us_arr_bag
us_arr_bag_idr % Vasche idromassaggio % us_arr_bag
us_arr_bag_san % Sanitari % us_arr_bag


us_arr_cam % Arredamento camera da letto % us_arr

us_arr_cam_com % Camere da letto complete % us_arr_cam
us_arr_cam_let % Letti e comodini % us_arr_cam
us_arr_cam_cmo % Comò e specchi % us_arr_cam
us_arr_cam_arm % Armadi % us_arr_cam


us_arr_ctt % Arredamento cameretta bimbi % us_arr

us_arr_ctt_cmp % Camerette complete % us_arr_ctt
us_arr_ctt_let % Letti normali % us_arr_ctt
us_arr_ctt_lct % Letti a castello % us_arr_ctt
us_arr_ctt_scr % Scrivanie % us_arr_ctt
us_arr_ctt_sed % Seggiole e poltrone % us_arr_ctt


us_arr_acc % Accessori e complementi d'arredo % us_arr

us_arr_acc_lam % Lampadari % us_arr_acc
us_arr_acc_lap % Lampade % us_arr_acc
us_arr_acc_orp % Orologi a parete % us_arr_acc
us_arr_acc_orm % Orologi a pendolo % us_arr_acc
us_arr_acc_tap % Tappeti % us_arr_acc
us_arr_acc_sti % Assi e mobili da stiro % us_arr_acc
us_arr_acc_men % Mensole e pensili vari % us_arr_acc
us_arr_acc_att % Attaccapanni % us_arr_acc
us_arr_acc_gua % Guardaroba % us_arr_acc



#######################
## gravidanza e maternità
us_gra % Gravidanza e maternità % ORIG_USED


us_gra_incinta % Per la mamma  % us_gra

us_gra_incinta_reg % Reggiseni da gravidanza % us_gra_incinta
us_gra_incinta_rea % Reggiseni da allattamento % us_gra_incinta
us_gra_incinta_gua % Guaine da gravidanza % us_gra_incinta
us_gra_incinta_ves % Vestiti pre-mamam % us_gra_incinta
us_gra_incinta_cal % Calze da gravidanza % us_gra_incinta
us_gra_incinta_fas % Fasce post parto % us_gra_incinta


us_gra_cam % Per la cameretta % us_gra

us_gra_cam_ltt % Lettini con sbarre % us_gra_cam
us_gra_cam_fas % Fasciatoi % us_gra_cam
us_gra_cam_vas % Vaschette per il bagnetto % us_gra_cam
us_gra_cam_cul % Culle % us_gra_cam
us_gra_cam_bil % Bilance per neonato % us_gra_cam
us_gra_cam_lec % Lettini trasportabili (da campeggio) % us_gra_cam


us_gra_acc % Altri accessori % us_gra

us_gra_acc_seg % Seggioloni % us_gra_acc
us_gra_acc_box % Box % us_gra_acc
us_gra_acc_gir % Girelli % us_gra_acc
us_gra_acc_pas % Passeggini % us_gra_acc
us_gra_acc_ste % Sterilizzatori % us_gra_acc
us_gra_acc_sbi % Scalda biberon % us_gra_acc
us_gra_acc_omo % Omogenizzatori % us_gra_acc
us_gra_acc_seg % Seggiolini per auto % us_gra_acc




#######################
## elettronica e computer
elettroniche % Elettronica e computer % ORIG_USED


pezzi_di_pc % PC e componenti % elettroniche

pezzi_di_pc_vin % Computer "obsoleti" (antiquariato informatico) % pezzi_di_pc
pezzi_di_pc_com % Pc completi da scrivania % pezzi_di_pc
pezzi_di_pc_lap % Laptop % pezzi_di_pc
pezzi_di_pc_pal % Computer palmari % pezzi_di_pc
hd % Hard Disks % pezzi_di_pc
schede_video % Schede Video % pezzi_di_pc
motherboards % Schede madri % pezzi_di_pc
cpus % Cpu % pezzi_di_pc
ram % Ram % pezzi_di_pc
usb_sticks % Penne Usb % pezzi_di_pc
altri_pezzi % Altri Pezzi % pezzi_di_pc


stampanti % Stampanti % elettroniche

impatto % A impatto % stampanti
ink_jet % A getto d'inchiostro % stampanti
laser_bn % Laser in b/n % stampanti
laser_col % Laser a colori % stampanti


us_ele_telefonia % Telefonia % elettroniche

us_ele_telefonia_cel % Cellulari % us_ele_telefonia
us_ele_telefonia_fis % Telefoni fissi con filo % us_ele_telefonia
us_ele_telefonia_crl % Cordless % us_ele_telefonia
us_ele_telefonia_seg % Segreterie telefoniche % us_ele_telefonia


us_ele_hifi % Musica ed Hi-Fi % elettroniche

us_ele_hifi_cmp % Stereo compatti % us_ele_hifi
us_ele_hifi_trn % Giradischi (LP) % us_ele_hifi
us_ele_hifi_pia % Piastre di registrazione % us_ele_hifi
us_ele_hifi_cas % Casse % us_ele_hifi
us_ele_hifi_acc % Accessori % us_ele_hifi
us_ele_hifi_cdp % Lettori CD % us_ele_hifi
us_ele_hifi_mp3 % Lettori mp3 portatili % us_ele_hifi
us_ele_hifi_wlk % "vecchi" walkman a cassette % us_ele_hifi
us_ele_hifi_rcr % Registratori portatili a nastro % us_ele_hifi
us_ele_hifi_rcd % Registratori portatili digitali % us_ele_hifi




#########################
## foto video
us_foto_video % Foto e video  % ORIG_USED


us_ele_video % Video e accessori % us_foto_video

us_ele_video_tel % Telecamere % us_ele_video
us_ele_video_vcr % Videoregistratori % us_ele_video
us_ele_video_dvd % Dvd player % us_ele_video
us_ele_video_dec % Decoder satellitari % us_ele_video
us_ele_video_par % Parabole e accessori % us_ele_video
us_ele_video_mix % Mixer video % us_ele_video
us_ele_video_luc % Luci % us_ele_video


us_foto_video_foto % Fotografia % us_foto_video

us_foto_video_foto_ana % Macchine fotografiche analogiche % us_foto_video_foto
us_foto_video_foto_dig % Macchine fotografiche digitali % us_foto_video_foto
us_foto_video_foto_aan % Accessori per macchine fotografiche analogiche % us_foto_video_foto
us_foto_video_foto_adi % Accessori per macchine fotografiche digitali % us_foto_video_foto
us_foto_video_foto_cos % Accessori per camera oscura % us_foto_video_foto
us_foto_video_foto_esp % Esposimetri ed altri accessori % us_foto_video_foto


##########################
## veicoli 
us_veicoli % Veicoli per adulti % ORIG_USED


us_veicoli_auto % Auto % us_veicoli

us_veicoli_auto_ita % Auto italiane % us_veicoli_auto
us_veicoli_auto_est % Auto estere % us_veicoli_auto


us_veicoli_moto % Moto % us_veicoli

us_veicoli_moto_ita % Moto italiane % us_veicoli_moto
us_veicoli_moto_est % Moto estere % us_veicoli_moto


us_veicoli_nom % Veicoli non a motore o elettrici % us_veicoli

us_veicoli_nom_bic % Biciclette adulto corsa % us_veicoli_nom
us_veicoli_nom_mtb % Mountain bike % us_veicoli_nom
us_veicoli_nom_bie % Biciclette elettriche % us_veicoli_nom
us_veicoli_nom_ale % Altri veicoli elettrici % us_veicoli_nom



#########################
## bambini e ragazzi
us_bamb % Bambini e ragazzi % ORIG_USED


us_bamb_vei % Veicoli per bambini % us_bamb

us_bamb_vei_bic % Biciclette % us_bamb_vei
us_bamb_vei_tri % Tricicli % us_bamb_vei
us_bamb_vei_mac % Macchine elettriche per bambini % us_bamb_vei


us_bamb_gio % Giocattoli % us_bamb

us_bamb_gio_neo % Giocattoli per 0-3 anni % us_bamb_gio
us_bamb_gio_tav % Giochi da tavolo % us_bamb_gio
us_bamb_gio_cos % Giochi da costruzione % us_bamb_gio
us_bamb_gio_bam % Bambole % us_bamb_gio
us_bamb_gio_bac % Accessori bambole % us_bamb_gio
us_bamb_gio_mac % Macchinine % us_bamb_gio
us_bamb_gio_pis % Piste di macchine % us_bamb_gio
us_bamb_gio_cuc % Cucine giocattolo % us_bamb_gio
us_bamb_gio_tav % Scrivanie per bambini % us_bamb_gio
us_bamb_gio_tre % Trenini ed altro modellismo % us_bamb_gio
us_bamb_gio_car % Carte da gioco % us_bamb_gio


us_bamb_scu % Scuola % us_bamb

us_bamb_scu_lim % Libri scolastici medie % us_bamb_scu
us_bamb_scu_lis % Libri scolastici superiori % us_bamb_scu
us_bamb_scu_acc % Accessori per disegno % us_bamb_scu
us_bamb_scu_zai % Zaini scolastici % us_bamb_scu



##########################
## hobby e tempo libero
us_hobby % Hobby e tempo libero % ORIG_USED


us_hobby_musica % Strumenti musicali % us_hobby

us_hobby_musica_chi % Chitarre acustiche e classiche % us_hobby_musica
us_hobby_musica_che % Chitarre elettriche % us_hobby_musica
us_hobby_musica_pia % Pianoforti % us_hobby_musica
us_hobby_musica_pie % Pianole elettriche % us_hobby_musica
us_hobby_musica_alt % Altri strumenti % us_hobby_musica
us_hobby_musica_bat % Batterie % us_hobby_musica
us_hobby_musica_vio % Violini % us_hobby_musica
us_hobby_musica_fla % Flauti ed altri strumenti scolastici % us_hobby_musica


us_hobby_pesca % Caccia e pesca % us_hobby

us_hobby_pesca_can % Canne da pesca % us_hobby_pesca
us_hobby_pesca_fio % Fucili subacquei % us_hobby_pesca
us_hobby_pesca_mut % Mute % us_hobby_pesca
us_hobby_pesca_pin % Pinne % us_hobby_pesca
us_hobby_pesca_abb % Abbigliamento caccia % us_hobby_pesca
us_hobby_pesca_ric % Richiami ed accessori caccia % us_hobby_pesca
us_hobby_pesca_bom % Bombole, boccali altri accessori da sub % us_hobby_pesca


us_hobby_sport % Attrezzatura e abbigliamento per lo sport % us_hobby

us_hobby_sport_ten % Tennis % us_hobby_sport
us_hobby_sport_bod % Body building (pesi e attrezzature) % us_hobby_sport
us_hobby_sport_sci % Sci % us_hobby_sport
us_hobby_sport_gol % Golf % us_hobby_sport
us_hobby_sport_alt % Altri sport % us_hobby_sport
us_hobby_sport_nuo % Nuoto e pallanuoto % us_hobby_sport
us_hobby_sport_cal % Calcio % us_hobby_sport
us_hobby_sport_pal % Pallavolo % us_hobby_sport
us_hobby_sport_bas % Basket % us_hobby_sport
us_hobby_sport_jog % Jogging % us_hobby_sport
us_hobby_sport_alp % Alpinismo, trekking % us_hobby_sport


us_hobby_coll % Collezionismo % us_hobby

us_hobby_coll_sch % Schede telefoniche % us_hobby_coll
us_hobby_coll_fra % Francobolli % us_hobby_coll
us_hobby_coll_num % Numismatica % us_hobby_coll
us_hobby_coll_sol % Soldatini ed altre miniature % us_hobby_coll
us_hobby_coll_fum % Collezioni di fumetti % us_hobby_coll
us_hobby_coll_alt % Altre collezioni % us_hobby_coll
us_hobby_coll_sta % Stampe artistiche e poster % us_hobby_coll


us_hobby_libri % Libri, fumetti, riviste % us_hobby

us_hobby_libri_gia % Libri gialli e polizieschi % us_hobby_libri
us_hobby_libri_ros % Libri rosa % us_hobby_libri
us_hobby_libri_fum % Fumetti % us_hobby_libri
us_hobby_libri_man % Manuali % us_hobby_libri
us_hobby_libri_enc % Enciclopedie % us_hobby_libri
us_hobby_libri_fan % Fantasy % us_hobby_libri
us_hobby_libri_sfi % Fantascienza % us_hobby_libri
us_hobby_libri_art % Arte e cinema % us_hobby_libri
us_hobby_libri_bam % Per bambini % us_hobby_libri
us_hobby_libri_riv % Riviste % us_hobby_libri


us_hobby_cd % Cd, dischi, mp3 musicali  % us_hobby

us_hobby_cd_cla % Musica classica % us_hobby_cd
us_hobby_cd_pop % Musica pop % us_hobby_cd
us_hobby_cd_tec % Musica techno/discoteca % us_hobby_cd
us_hobby_cd_roc % Musica rock % us_hobby_cd
us_hobby_cd_etn % Musica etnica % us_hobby_cd
us_hobby_cd_amb % Musica ambiente % us_hobby_cd
us_hobby_cd_bam % Musica per bambini % us_hobby_cd


us_hobby_dvd % Vhs e dvd % us_hobby

us_hobby_dvd_gia % Gialli % us_hobby_dvd
us_hobby_dvd_com % Commedie % us_hobby_dvd
us_hobby_dvd_thr % Thriller % us_hobby_dvd
us_hobby_dvd_sfi % Fantascienza % us_hobby_dvd
us_hobby_dvd_hor % Horror % us_hobby_dvd
us_hobby_dvd_ani % Animazione % us_hobby_dvd
us_hobby_dvd_bam % Per bambini % us_hobby_dvd
us_hobby_dvd_doc % Documentari % us_hobby_dvd

