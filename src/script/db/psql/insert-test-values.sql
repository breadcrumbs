-- -*- mode: sql -*-

-- This file is part of the breadcrumbs daemon (bcd).
-- Copyright (C) 2007 Pasqualino Ferrentino

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
-- 02110-1301, USA.

-- Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

--some activities
insert into activities values(1, NULL, 'Casa/famiglia');
insert into activities values(2, 1, 'Pulizie casa');
insert into activities values(3, 1, 'Maglia/cucito/ricamo');
insert into activities values(4, 1, 'Idraulica');
insert into activities values(5, NULL, 'Studio');
insert into activities values(6, 5, 'Lezioni di lingue');
insert into activities values(7, 5, 'Lezioni di matematica');
insert into activities values(8, 5, 'Baby sitter');
insert into activities values(9, 1, 'Massaggi');

--some sites
insert into sites values(1, 'A casa mia');
insert into sites values(2, 'A casa del cliente');
insert into sites values(3, 'In Sede');
insert into sites values(4, 'In un bricio-punto');
insert into sites values(5, 'In un bricio-negozio');

--some activites
insert into users_activities values(7,1,0,20000,25000,now(),1e-10,1e-5,'0',1,'15-20','lu-ma-ve','medie');
insert into users_activities values(8,2,0,15933,18378,now(),1e-6,1e-2,'1',2,'7-15','gio-sab','bambini 0-3');
insert into users_activities values(4,5,0,23991,34895,now(),1e-8,1e-4,'0',2,'7-18','lun','idraulica semplice');
insert into users_activities values(8,8,0,12838,17237,now(),5e-6,2.34e-3,'1',2,'17-18','mar','bambini > 3');
insert into users_activities values(9,7,0,39239,59393,now(),0.05,0.06,'1',2,'8-9','mar','massaggi orientali');

