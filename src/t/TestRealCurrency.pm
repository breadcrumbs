package t::TestRealCurrency;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::RealCurrency;

sub test_dehumanize{
    my $self = shift;

    my ($res, $dehumanized);

    ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string("456");
    $self->assert_equals(1, $res);
    $self->assert_equals("45600", $dehumanized);

    ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string("956.93");
    $self->assert_equals(1, $res);
    $self->assert_equals("95693", $dehumanized);


    ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string("456.3");
    $self->assert_equals(1, $res);
    $self->assert_equals("45630", $dehumanized);

    ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string("0.01");
    $self->assert_equals(1, $res);
    $self->assert_equals("1", $dehumanized);


    #two dots
    ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string("45.6.93");
    $self->assert_equals(0, $res);

    #dot in strange positions
    ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string("45.393");
    $self->assert_equals(0, $res);
    $self->assert_null($dehumanized);

    ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string("45393.");
    $self->assert_equals(0, $res);
    $self->assert_null($dehumanized);

    #italian format... no good
    ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string("453,98");
    $self->assert_equals(0, $res);
    $self->assert_null($dehumanized);

    #garbage
    ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string("453df.98");
    $self->assert_equals(0, $res);
    $self->assert_null($dehumanized);

}

sub test_humanize{
    my $self = shift;

    my $humanized;

    $humanized = Bcd::Data::RealCurrency::humanize_this_string("458");
    $self->assert_str_equals("4.58", $humanized);

    $humanized = Bcd::Data::RealCurrency::humanize_this_string("12458");
    $self->assert_str_equals("124.58", $humanized);

    $humanized = Bcd::Data::RealCurrency::humanize_this_string("12");
    $self->assert_str_equals("0.12", $humanized);

    $humanized = Bcd::Data::RealCurrency::humanize_this_string("1");
    $self->assert_str_equals("0.01", $humanized);

    $humanized = Bcd::Data::RealCurrency::humanize_this_string("");
    $self->assert_str_equals("0.00", $humanized);
}
