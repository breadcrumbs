package t::TestAccount;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Math::BigInt lib => "GMP";

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use DBI;
#this is the class I want to test
use Bcd::Data::Accounting::Account;
use Bcd::Data::Accounting::RealCurrencyAccount;
use Bcd::Data::Accounting::FakeCurrencyAccount;
use Bcd::Data::Accounting::AccountsPlan;
use Bcd::Data::Accounting::Transaction;
use Bcd::Data::StatementsStash;

use Data::Dumper;


sub new {
    my $self = shift()->SUPER::new(@_);

    my $stash = $self->{stash};

    return $self;
}

sub set_up{
   my $self = shift;
   my $stash = $self->{stash};

    $self->{account}   = Bcd::Data::Accounting::AccountsPlan->create_a_normal_account(11, "xyz euro", $stash);
    $self->{account_t} = Bcd::Data::Accounting::AccountsPlan->create_a_normal_account(12, "xyz tao",  $stash);
}

sub test_have_you_got_any_transactions{
    my $self = shift;
    my $stash = $self->{stash};

#    my $answer = $self->{"account"}->have_you_got_any_transactions($self->{"stash"});

    my $answer = Bcd::Data::Accounting::Accounts->get_transactions_count($stash, $self->{account});
    $self->assert_num_equals(0, $answer);

    $answer = Bcd::Data::Accounting::Accounts->get_transactions_count($stash, $self->{account_t});
    $self->assert_num_equals(0, $answer);
}

#the initial balance should be undef, because there is no data.
sub test_initial_balance{
    my $self = shift;
    my $stash = $self->{stash};

    my $balance = Bcd::Data::Accounting::Accounts->get_balance($stash, $self->{account});
    $self->assert_num_equals(0, $balance);

    $balance = Bcd::Data::Accounting::Accounts->get_balance($stash, $self->{account_t});
    $self->assert_num_equals(0, $balance);
}


sub test_add_splits_t{

    my $self = shift;

    #I should add a "fake" master transaction...
    my $conn = $self->{stash}->get_connection();
    my $stash = $self->{"stash"};
    my $sql = qq{ insert into transactions(date, description) values(now(), 'test transaction')};
    my $insert_st = $conn->prepare($sql);
    $insert_st->execute();

    my $balance;

    #ok, now I have a "master" transaction to test...

    $balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	($stash, $self->{account_t}, 10, 0);
#    $balance = $self->{"account_t"}->add_transaction_split(10, 0, $stash);

    #ok, now I should test that the balance is 10.
    $self->assert_num_equals(10, $balance);

    $insert_st->execute();

    $balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	($stash, $self->{account_t}, 3.44444, 0.11111);

    $self->assert_num_equals(13.33333, $balance);
    $insert_st->execute();

    $balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	($stash, $self->{account_t}, 0, 13.33333);

    my $round_balance = sprintf("%.2f", abs($balance));
    $self->assert_equals("0.00", $round_balance);

}

sub test_get_account_summary_e{
    my $self = shift;

    #I should add a "fake" master transaction...
    my $conn = $self->{stash}->get_connection();
    my $stash = $self->{"stash"};

    my $sql = qq{ insert into transactions(date, description) values(now(), ?)};
    my $insert_tr = $conn->prepare($sql);

    #my $total   = new Math::BigInt->bzero();

    my $amount = new Math::BigInt("1000"); #1000 is 10.00
    my $zero   = new Math::BigInt->bzero();

    my $balance;

    #I test a certain number of transactions
    for (1..10){
	$insert_tr->bind_param(1, "test transaction $_");
	$insert_tr->execute();

	$balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	    ($stash, $self->{account}, $amount, $zero);

    }

    $self->assert_equals("10000", $balance);

    #now I get the summary of this account
    my ($summary, $euro) = Bcd::Data::Accounting::Accounts->get_account_summary_ds
	($stash, $self->{account});

    $self->assert_equals(1, $euro);
    $self->assert_num_equals(11, scalar(@{$summary}));
    
    for (1..10){
	$self->assert_equals("test transaction $_", $summary->[$_]->[1]);
	$self->assert_equals("1000", $summary->[$_]->[2]);
 	$self->assert_equals("0", $summary->[$_]->[3]);
	$self->assert_equals(1000 * $_, $summary->[$_]->[4]);
    }



}

#I test the adding of a split either on the debit and on the credit
#part.
sub test_add_splits{

    my $self = shift;

    #I should add a "fake" master transaction...
    my $conn = $self->{stash}->get_connection();
    my $stash = $self->{"stash"};
    my $sql = qq{ insert into transactions(date, description) values(now(), 'test transaction')};
    my $insert_st = $conn->prepare($sql);
    $insert_st->execute();

    #ok, now I have a "master" transaction to test...

    my $amount = new Math::BigInt("1000"); #1000 is 10.00
    my $zero   = new Math::BigInt->bzero();


    #the parameters are account, debit and credit
    my $balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	($stash, $self->{account}, $amount, $zero);
    #my $balance = $self->{"account"}->add_transaction_split(10, 0, $stash);

    #ok, now I should test that the balance is 10.
    $self->assert_num_equals("1000", $balance);

    #I add another test transaction
    $insert_st->execute();

    $amount = "2500";
    my $amount1 = new Math::BigInt("500");

    #add a "composite" transaction split
    $balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	($stash, $self->{account}, $amount, $amount1);
    #$balance = $self->{"account"}->add_transaction_split(25, 5, $stash);

    #this is the total balance
    $self->assert_num_equals("3000", $balance);

    #now I make the account negative
    $insert_st->execute();

    $amount = "500";
    $amount1 = "4500";

    $balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	($stash, $self->{account}, $amount, $amount1);
    #$balance = $self->{"account"}->add_transaction_split(5, 45, $stash);

    #this is the total balance
    $self->assert_num_equals("-1000", $balance);

    $insert_st->execute();

    $amount = "1000";
    $amount1 = new Math::BigInt("2");

    #some fractional quantities
    $balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	($stash, $self->{account}, $amount , $amount1);
    #$balance = $self->{"account"}->add_transaction_split(10, 0.0232113, $stash);
    #$balance = $self->{"account"}->get_balance($stash);

    #this is the total balance
    $self->assert_num_equals("-2", $balance);

    $insert_st->execute();

    $amount = new Math::BigInt("10");
    $amount->bdiv(3);

    #some fractional quantities
    $balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	($stash, $self->{account}, $amount, 0);
#    $balance = $self->{"account"}->add_transaction_split(0.02500000001, 0, $stash);
#    $balance = $self->{"account"}->get_balance($stash);
    #this is the total balance
    $self->assert_num_equals("1", $balance);

#    $conn->rollback();
}


#this test should simply test the recursive balance of an account.
sub test_recursive_balance{
    #I create a test tree...

    my $self = shift;
    my $conn = $self->{stash}->get_connection();
    my $stash = $self->{"stash"};

    my $child0 = Bcd::Data::Accounting::AccountsPlan->create_a_normal_account(0, 'cash 0', $stash);

    #i create two childs
    my $child1 = Bcd::Data::Accounting::AccountsPlan->create_a_normal_account($child0, 'cash 1', $stash);
    my $child2 = Bcd::Data::Accounting::AccountsPlan->create_a_normal_account($child0, 'cash 2', $stash);

    #I make one transaction
    my $list_debit_accounts = [$child1, $child2]; #only one account
    my $list_debit_amounts = [30, 70];

    my $list_credit_accounts = [15];
    my $list_credit_amounts = [100];

    my $description = "mirror transaction: incomes for the shop";

    Bcd::Data::Accounting::Transaction->add_new_transaction_in_db($list_debit_accounts, $list_debit_amounts,
							     $list_credit_accounts, $list_credit_amounts, 
							     $description, $stash);

    #ok, now I should get the total balance of the account 3
    #my $parent = Bcd::Data::Accounting::AccountsPlan->get_account_with_number(0, $stash);

    my $total_balance = Bcd::Data::Accounting::Accounts->get_recursive_balance($stash, $child0);

    $self->assert_num_equals(100, $total_balance);

    $conn->rollback();
}


# sub test_recursive_balance_with_mirror_accounts{
#     my $self = shift;
#     my $conn = $self->{"conn"};

#     my $id_mirror = accounting::AccountsPlan->instance()->
# 	create_a_mirror_account(7, 'account shop', 3);

#     my $list_debit_accounts = [1]; #only one account
#     my $list_debit_amounts = [35];

#     my $list_credit_accounts = [$id_mirror, 15];
#     my $list_credit_amounts = [35, 35];

#     #the transaction is            debit           credit
#     # 1 cash Bank T                35
#     # $id mirror account shop                      35
#     # 15 incomes shop                              35

#     #the transaction is NOT imbalanced, because the account shop is a
#     #mirror account

#     #the transaction is imbalanced but only if you do not consider the
#     #mirror

#     my $description = "mirror transaction: incomes for the shop";

#     accounting::Transaction->add_new_transaction_in_db($list_debit_accounts, $list_debit_amounts,
# 						       $list_credit_accounts, $list_credit_amounts, 
# 						       $description);

#     #then I have the cash in T in the shop
#     my $id_cash_shop = accounting::AccountsPlan->instance()->
# 	create_a_normal_account(3, 'shop cash');

#     #Ok, I create another transaction
#     #                              debit           credit
#     # $id_cash_shop                90              0
#     # 15 incomes shop              0               90

#     $list_debit_accounts = [$id_cash_shop];
#     $list_debit_amounts = [90];
#     $list_credit_accounts = [15];
#     $list_credit_amounts = [90];
#     $description = "another test income";

#     accounting::Transaction->add_new_transaction_in_db($list_debit_accounts, $list_debit_amounts,
# 						       $list_credit_accounts, $list_credit_amounts, 
# 						       $description);
    
#     #now the total recursive of the account 3 should be 90+35

#     my $assets_shop_t = accounting::AccountsPlan->instance()->get_account_with_number(3);

#     my $recursive_balance = $assets_shop_t->get_recursive_balance();

#     $self->assert_num_equals(35+90, $recursive_balance);

#     #instead the recursive balance of the liabilities bank should be -35

#     my $liabilities_bank_t = accounting::AccountsPlan->instance()->get_account_with_number(7);
#     $recursive_balance = $liabilities_bank_t->get_recursive_balance();
#     $self->assert_num_equals(-35, $recursive_balance);

#     $conn->rollback();
    
# }
