package t::TestToken;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Token;
use Data::Dumper;

sub test_generate_new_token{
    my $self = shift;

    my $token = Bcd::Data::Token::generate_new_token();

    $self->assert_equals(Bcd::Data::Token::TOKEN_LENGTH + 
			 int((Bcd::Data::Token::TOKEN_LENGTH - 1) /
			     Bcd::Data::Token::TOKEN_CHUNK_LENGTH), length($token));

    $self->assert_num_equals(1, Bcd::Data::Token::is_valid_token($token));

}

sub test_is_valid_token{
    my $self = shift;

    $self->assert_num_equals(1, Bcd::Data::Token::is_valid_token("hsi dio 000"));
    #incorrect lenght
    $self->assert_num_equals(0, Bcd::Data::Token::is_valid_token("hsh did siid")); 
    #incorrect chars
    $self->assert_num_equals(0, Bcd::Data::Token::is_valid_token("cis did ci#"));
    $self->assert_num_equals(0, Bcd::Data::Token::is_valid_token("cis did ci+"));

    #no spaces
    $self->assert_num_equals(0, Bcd::Data::Token::is_valid_token("123456789"));
}

sub test_blind{
    my $self = shift;

    my ($token, $blinded) = Bcd::Data::Token::generate_long_token();
    my @blinded = ($blinded =~ /\#/xg);
    $self->assert_num_equals(Bcd::Data::Token::TOKEN_LENGTH, $#blinded+1);

    
}

sub test_blinded_secret {
    my $self = shift;

    my ($token, $blinded, $secret) = Bcd::Data::Token::generate_long_token();

    #print "token \n--$token-- \n--$blinded-- \n--$secret--\n";

    $self->assert_equals(1, Bcd::Data::Token::is_valid_token($secret));

    my $generated_secret = Bcd::Data::Token::unblind_the_token($token, $blinded);
    $self->assert_equals($generated_secret, $secret);
}
