package t::TestUsers;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Digest::SHA1;
use t::TestCommonDb;
#this should simply make some tests...
use Bcd::Errors::ErrorCodes;
use Bcd::Data::StatementsStash;
use Bcd::Data::Users;
use Bcd::Data::Accounting::AccountsPlan;

use Data::Dumper;

our @ISA = qw(t::TestCommonDb);

# sub new {
#     my $self = shift()->SUPER::new(@_);

#     my $stash = data::StatementsStash->new($self->{"conn"});
#     $self->{"stash"} = $stash;

#     return $self;
# }

sub test_bc_root_login_wrong{
    my $self = shift;
    my $stash = $self->{"stash"};
    my ($res, $role) = Bcd::Data::Users->login('bc-root', '', 'woiewioreiowe', $stash);
    $self->assert_num_equals(1, $res);
}

sub test_bc_root_login_right{
    my $self = shift;
    my $stash = $self->{"stash"};
    my ($res, $role) = Bcd::Data::Users->login('bc-root', '', 'qpqpqp', $stash);
    $self->assert_num_equals(0, $res);
    $self->assert_num_equals(Bcd::Data::Users::ROLE_FOR_BC_ROOT, $role);
}

sub test_login{
    my $self = shift;
    my $stash = $self->{"stash"};
    #my ($self, $user, $ant_nest_code, $password, $stash) = @_;
    my ($res, $role) = Bcd::Data::Users->login('barbara', '8012898', 'barbarap', $stash);
    $self->assert_num_equals(0, $res);
    $self->assert_num_equals(3, $role); #barbara is the boss...

    ($res, $role) = Bcd::Data::Users->login('barbara', '8012898', 'Cippo', $stash);
    $self->assert_num_equals(1, $res);
    $self->assert_null($role);
}

sub test_is_valid_password{
   my $self = shift;
   my $stash = $self->{stash};

   my $res = Bcd::Data::Users->is_valid_password_for_user($stash, 16, "barbarap");
   $self->assert_equals(1, $res);
   
   $res = Bcd::Data::Users->is_valid_password_for_user($stash, 16, "cici");
   $self->assert_equals(0, $res);
}

sub test_change_password{
   my $self = shift;
   my $stash = $self->{stash};

   Bcd::Data::Users->change_password_for_user($stash, 16, "newp");
   #the login with the old password should fail
   my ($res, $role) = Bcd::Data::Users->login('barbara', '8012898', 'barbarap', $stash);
   $self->assert_equals(1, $res);

   #with the new password should succed
   ($res, $role) = Bcd::Data::Users->login('barbara', '8012898', 'newp', $stash);
   $self->assert_equals(0, $res);
   $self->assert_num_equals(3, $role); #elisa is the boss
}

sub test_change_totem{
    my $self = shift;
    my $stash = $self->{stash};

    Bcd::Data::Users->change_totem_for_user($stash, 16, "supertotem");
    my $user = Bcd::Data::Users->get_user_base_data_hash($stash, 16);
    $self->assert_equals("supertotem", $user->{totem});
}

sub test_login_from_another_ant_nest{
   my $self = shift;
   my $stash = $self->{"stash"};
   my ($res, $role) = Bcd::Data::Users->login('elisa', '1613498', 'elisap', $stash);
   $self->assert_num_equals(0, $res);
   $self->assert_num_equals(3, $role); #elisa is the boss
}

sub test_not_existing_login{
    my $self = shift;
    my $stash = $self->{"stash"};

    my ($res, $role) = Bcd::Data::Users->login('lino_not_exist', '8012898', 'cippo', $stash);
    $self->assert_num_equals(2, $res);
    $self->assert_null($role);
}

sub test_get_ant_nest_from_user{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $an_code = Bcd::Data::Users->get_ant_nest_for_this_user(1, $stash);
    $self->assert_equals("1613498", $an_code);

}

sub test_get_ant_nest_for_not_existing_user{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $an_code = Bcd::Data::Users->get_ant_nest_for_this_user(0, $stash);
    $self->assert_null($an_code);
}


sub test_create_user_from_booking{
    my $self = shift;
    my $stash = $self->{stash};

    #ok, first of all I create a fake ant nest...
    my $radix = '410400';
    $self->_create_test_ant_nest($radix);

    my $code = $radix . '99';




    my $home_phone = "0813929394";
    my $mobile_phone = "392340223";
    my $email = 'johnw@noooo.com';
    my $id_card = "292939399";

    my %data = (
	       nick => "Philip",
	       first_name => "john",
	       last_name => "white",
	       address => "fake street 20",
	       home_phone => $home_phone,
	       mobile_phone => $mobile_phone,
	       sex => "m",
	       birthdate => "1978-12-29",
	       email => $email,
	       identity_card_id => $id_card,
	       );

    #ok, now I create a personal data record for a user.
    my $id_data = Bcd::Data::Users->create_personal_data_for_user($stash, \%data);

    #then I create the user...
    my $user_id = Bcd::Data::Users->create_user_from_booking($stash, $code, 'Philip', 'pass', $id_data);

    #ok, then I get the details of this user

    my $user = Bcd::Data::Users->select_user_data($stash, $user_id);

    #the personal data should be the same
    foreach (keys(%data)){
	$self->assert_equals($data{$_}, $user->{$_});
    }

    #the state should be initial
    $self->assert_equals(Bcd::Constants::Users::CREATION_STATE, $user->{users_state});
    $self->assert_equals($code, $user->{id_ant_nest});
    
}

sub test_update_personal_data{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $home_phone = "0813929394";
    my $mobile_phone = "392340223";
    my $email = 'fil.montinaro@yahoo.it';
    my $id_card = "c.i. 3923432423";

    my $code = "8012898";
    
    my $count_before = Bcd::Data::Users->get_ant_count_from_ant_nest($stash, $code);

    my %cmd = (
	       ant_nest => $code,
	       nick => "Philip",
	       first_name => "Filippo",
	       last_name => "Montinaro",
	       address => "Via S. Bartolomeo dell'amore 39",
	       home_phone => $home_phone,
	       mobile_phone => $mobile_phone,
	       sex => "m",
	       birth_date => "1978-12-29",
	       email => $email,
	       id_card => $id_card,
	       );

    my $token = 
	Bcd::Data::Users->create_normal_user($stash, \%cmd, 0.93, 0.91, 1, 0.34);


    #ok, then I update this user...

    my $user_id = $stash->get_id_of_last_user();

    my $sql = qq{select * from users where id = $user_id };
    my $st = $stash->get_connection()->prepare( $sql );
    $st->execute();

    my $arr_ref = $st->fetchrow_arrayref();
    $st->finish();

    my $personal_data_id = $arr_ref->[14];

    #ok, now I change the personal data....
    %cmd = (
	    first_name => "Albert",
	    last_name => "Einstein",
	    address => "Relative street",
	    home_phone => '88',
	    mobile_phone => '11',
	    sex => "m",
	    birthdate => "1880-12-20",
	    email => 'aeinstein@emc2.com',
	    identity_card_id => 'emc2',
	    );

    Bcd::Data::Users->update_personal_data_from_user_id($stash, $user_id, \%cmd);

    #get the personal data
    $sql = qq{select * from personal_users_data where id = $personal_data_id};
    $st = $stash->get_connection()->prepare( $sql );
    $st->execute();

    my $data = $st->fetchrow_hashref();
    $st->finish();

    foreach (keys(%cmd)){
	$self->assert_equals($cmd{$_}, $data->{$_});
    }

}

#this test should simply test that the create user succeds
sub test_create_user_basic{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $home_phone = "0813929394";
    my $mobile_phone = "392340223";
    my $email = 'fil.montinaro@yahoo.it';
    my $id_card = "c.i. 3923432423";

    my $code = "8012898";
    
    my $count_before = Bcd::Data::Users->get_ant_count_from_ant_nest($stash, $code);

    my %cmd = (
	ant_nest => $code,
	nick => "Philip",
	first_name => "Filippo",
	last_name => "Montinaro",
	address => "Via S. Bartolomeo dell'amore 39",
	home_phone => $home_phone,
	mobile_phone => $mobile_phone,
	sex => "m",
	birthdate => "1978-12-29",
	email => $email,
	identity_card_id => $id_card,
	       );

    my $token = 
	Bcd::Data::Users->create_normal_user($stash, \%cmd, 0.93, 0.91, 1, 0.34);

    my $count_after = Bcd::Data::Users->get_ant_count_from_ant_nest($stash, $code);

    $self->assert_num_equals ( $count_after-1, $count_before);

    #I should get the last insert id...
    my $user_id = $stash->get_connection()->last_insert_id(undef, undef, undef, undef, 
							   {sequence => "users_id_seq"});

    #ok, now let's see if there is the booking
    my $sql = qq{select * from new_users_bookings where new_user=$user_id};
    my $st = $stash->get_connection()->prepare( $sql );
    $st->execute();
    my $arr_ref = $st->fetchrow_arrayref();

    $self->assert_num_equals(0.34, $arr_ref->[1]);
    $self->assert_num_equals(0, $arr_ref->[2]); #the second trust
    $self->assert_equals($token, $arr_ref->[3]);

    #print Dumper($arr_ref);

    #ok, now let's see if there is the row in the db



    #ok, now I try to fetch the newly inserted record
    $sql = qq{select * from users where id = $user_id };
    $st = $stash->get_connection()->prepare( $sql );
    $st->execute();

    $arr_ref = $st->fetchrow_arrayref();

    #print Dumper($arr_ref);

    #ok, now let's check the results.
    # the first is the id, discard it

    $self->assert_equals("8012898", $arr_ref->[1]);
    $self->assert_equals("Philip", $arr_ref->[2]);
    $self->assert_equals("Philipt", $arr_ref->[3]);

    #ok, the password is digested
    my $password = "Philipp";
    my $sha = Digest::SHA1->new;
    $sha->add($password);
    my $digest = $sha->hexdigest;
    $self->assert_equals($digest, $arr_ref->[4]);

    #5 and 6 are the tutors, which are null, in the beginning
    $self->assert_equals(1, $arr_ref->[5]);
    $self->assert_null($arr_ref->[6]);
    
    
    $self->assert_equals(Bcd::Constants::Users::NEW_ANT_WITH_ONE_TUTOR, $arr_ref->[7]);
    $self->assert_equals("1", $arr_ref->[8]);
    $self->assert_equals("1", $arr_ref->[10]);
    $self->assert_equals("1", $arr_ref->[11]);
    $self->assert_equals("0.93", $arr_ref->[12]);
    $self->assert_equals("0.91", $arr_ref->[13]);



    my $personal_data_id = $arr_ref->[14];

    #let's get the personal data
    $sql = qq{select * from personal_users_data where id = $personal_data_id};
    $st = $stash->prepare_cached($sql);
    $st->execute();
    my $personal_data = $st->fetchrow_arrayref();
    $st->finish();

    #let's assert the personal data

    $self->assert_equals("Filippo", $personal_data->[1]);
    $self->assert_equals("Montinaro", $personal_data->[2]);
    $self->assert_equals("Via S. Bartolomeo dell'amore 39", $personal_data->[3]);
    $self->assert_equals($home_phone, $personal_data->[4]);
    $self->assert_equals($mobile_phone, $personal_data->[5]);
    $self->assert_equals("m", $personal_data->[6]);
    $self->assert_equals("1978-12-29", $personal_data->[7]);
    $self->assert_equals($email, $personal_data->[8]);
    $self->assert_equals($id_card, $personal_data->[9]);


    #now You should check the accounts created...
    
    my $id_acct_euro = $arr_ref->[15];
    my $id_acct_tao  = $arr_ref->[16];

    #now let's see the description.
    #sub get_account_with_number{
    #my ($self, $id_to_get, $stash) = @_;

    # my $account = Bcd::Data::Accounting::AccountsPlan->get_account_with_number
# 	($id_acct_euro, $stash);

    my $account = Bcd::Data::Accounting::AccountsPlan->select_account_hash($stash, $id_acct_euro);
    $self->assert_equals("Philip 8012898 E account", $account->{description});

    my $parent_id = $account->{id_parent_account};

    $account = Bcd::Data::Accounting::AccountsPlan->select_account_hash($stash, $parent_id);

    $self->assert_equals("8012898 users's accounts E", $account->{description});

    $account = Bcd::Data::Accounting::AccountsPlan->select_account_hash($stash, $id_acct_tao);
    $self->assert_equals("Philip 8012898 T account", $account->{description});

    my $hash = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	($stash, $code, "Philip");

    $self->assert_num_equals(1, $hash->{role_mask});

    #let's try to change the role to philip
    my $ans = Bcd::Data::Users->grant_role_to_user($stash, $code, "Philip", 4);
    $self->assert_num_equals(1, $ans);

    $hash = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	($stash, $code, "Philip");

    #the role is "OR-ed" so, the result is 1 | 4 = 5
    $self->assert_num_equals(5, oct("0b" . $hash->{role_mask}));

    #ok, now let's give this user some tutors
    $ans = Bcd::Data::Users->update_first_tutor($stash, $user_id, 3);
    $self->assert_num_equals(1, $ans);

    $ans = Bcd::Data::Users->update_second_tutor($stash, $user_id, 2);
    $self->assert_num_equals(1, $ans);

    #I should have the tutors now...
    $hash = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	($stash, $code, "Philip");

    $self->assert_equals(3, $hash->{tutor_first});
    $self->assert_equals(2, $hash->{tutor_second});

    #ok, now I should see if exists the row in the new_users_bookings table
    $sql = qq{select * from new_users_bookings where new_user=?};
    my $st_new = $stash->get_connection()->prepare($sql);
    $st_new->bind_param(1, $user_id);
    $st_new->execute();
    my $arr = $st_new->fetchrow_arrayref();
    my $token_arr = $arr->[3];
    $st_new->finish();

    $self->assert_equals($token_arr, $token);

    $self->assert_equals ("0.34", $arr->[1]);

    #ok, now let's see if I can have the user_id from the token
    my $new_user_id = Bcd::Data::Users->get_new_user_id_from_this_token($stash, $token);
    $self->assert_num_equals($user_id, $new_user_id);

    #not existing token
    $new_user_id = Bcd::Data::Users->get_new_user_id_from_this_token($stash, "foo bar");
    $self->assert_null($new_user_id);

    #ok, now let's fake the confirmation of the tutor
    #my ($class, $stash, $id_new_user, $id_tutor, $tutor_trust) = @_;
    $ans = Bcd::Data::Users->confirm_second_tutor($stash, $user_id, 2, 0.32);
    $self->assert_num_equals(1, $ans);

    #ok, now let's see the tutor trust
    $st_new->execute();
    $arr = $st_new->fetchrow_arrayref();

    $self->assert_num_equals (0.32, $arr->[2]);

    #I should check the state
    $hash = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	($stash, $code, "Philip");
    
    $self->assert_num_equals(Bcd::Constants::Users::NEW_ANT_BEFORE_TRUST, 
			     $hash->{users_state});

    
    #ok, now I fake the confirmation of the ant
    #my ($class, $stash, $my_id, $trust_t1, $trust_t2) = @_;
    Bcd::Data::Users->confirm_my_tutors($stash, $user_id, 0.12, 0.13);

    #then let's check what has changed.
    
    #the booking should be deleted
    $st_new->execute();
    $arr = $st_new->fetchrow_arrayref();
    $self->assert_null($arr);

    #my state should have changed
    $hash = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	($stash, $code, "Philip");
    
    $self->assert_num_equals(Bcd::Constants::Users::NORMAL_ACTIVE_ANT,
			     $hash->{users_state});

    #There should be the rows in the trust table...
    $sql = qq{select * from trusts where u2 = $user_id};
    my $trust_st = $stash->get_connection()->prepare($sql);
    $trust_st->execute();

    $arr = $trust_st->fetchrow_arrayref();
    
    #this is the first trust
    $self->assert_num_equals(3, $arr->[0]);
    $self->assert_num_equals($user_id, $arr->[1]);
    $self->assert_equals(0.34, $arr->[2]);
    $self->assert_equals(0.12, $arr->[3]);

    $arr = $trust_st->fetchrow_arrayref();
    
    #this is the second trust
    $self->assert_num_equals(2, $arr->[0]);
    $self->assert_num_equals($user_id, $arr->[1]);
    $self->assert_equals(0.32, $arr->[2]);
    $self->assert_equals(0.13, $arr->[3]);

    #there should be no third trust
    $arr = $trust_st->fetchrow_arrayref();
    $self->assert_null($arr);
        
    #undo the changes
    $stash->get_connection()->rollback();
}


#this test is very simple, I don't
sub test_grant_role_to_not_existing_user{

    my $self = shift;
    my $stash = $self->{stash};

    my $ans = Bcd::Data::Users->grant_role_to_user($stash, "393993", "cssoso", 4);
    $self->assert_num_equals(0, $ans);

    my $hash = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	($stash, "8012800", "liidjcino");
    
    $self->assert_null($hash);
}


sub test_get_all_ants_from_ant_nest{

   my $self = shift;
   my $stash = $self->{stash};

   my $arr_ref = Bcd::Data::Users->get_all_ants_in_this_ant_nest($stash, "1603598");

   $self->assert_num_equals(3, $#{$arr_ref}+1);
}

sub test_get_boss{
   my $self = shift;
   my $stash = $self->{stash};

   my $boss = Bcd::Data::Users->get_boss_of_this_ant_nest($stash, "8012898");
   $self->assert_equals("barbara", $boss->{nick});

}

sub test_get_tresaurer{
    my $self = shift;
    my $stash = $self->{stash};

    my $boss = Bcd::Data::Users->get_tresaurer_of_this_ant_nest($stash, "8012898");
    $self->assert_equals("tina", $boss->{nick});
}

sub test_get_children{
    my $self = shift;
    my $stash = $self->{stash};

    my $arr = Bcd::Data::Users->get_all_children_arr($stash, 16);

    $self->assert_num_equals(2, scalar(@{$arr}));
    $self->assert_equals(21, $arr->[0]->[0]);
    $self->assert_equals(22, $arr->[1]->[0]);

    $self->assert_equals('irene', $arr->[0]->[1]);
    $self->assert_equals('massimo', $arr->[1]->[1]);

    $self->assert_equals(19, $arr->[0]->[2]);
    $self->assert_equals(17, $arr->[1]->[2]);

    #let's see if also the second tutor works
    $arr = Bcd::Data::Users->get_all_children_arr($stash, 19);
    
    #ugo has three children
    $self->assert_num_equals(3, scalar(@{$arr}));
}

sub test_get_children_ds{
    my $self = shift;
    my $stash = $self->{stash};

    my $arr = Bcd::Data::Users->get_all_children_ds($stash, 16);

    #I have one more row
    $self->assert_num_equals(3, scalar(@{$arr}));
    $self->assert_equals(21,        $arr->[1]->[0]);
    $self->assert_equals(22,        $arr->[2]->[0]);

    $self->assert_equals('irene',   $arr->[1]->[1]);
    $self->assert_equals('massimo', $arr->[2]->[1]);

    $self->assert_equals(19,        $arr->[1]->[2]);
    $self->assert_equals(17,        $arr->[2]->[2]);

    $self->assert_equals('ugo',     $arr->[1]->[3]);
    $self->assert_equals('tina',    $arr->[2]->[3]);
}

