package t::TestCommandParser;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;
use DBI;

use Data::Dumper;
use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Commands::CommandParser;

sub new {
    my $self = shift()->SUPER::new(@_);

    return $self;
}

sub test_recordset{
    my $self = shift;
    my $stash = $self->{stash};

    my $output = {
	__recordsets => ['testrs'],
	testrs => [
		  [
		   'field1',
		   'field2',
		   ],
		  [
		   'r1f1',
		   'f1f2',
		   ],
		  [
		   'r2f1',
		   'r2f2',
		   ]
		  ],
	       };

    #ok, I try to parse this output
    my $res = Bcd::Commands::CommandParser->parse_command_output($output);

    $self->assert_equals('r1f1', $res->{testrs}->[0]->{field1});
    $self->assert_equals('r2f1', $res->{testrs}->[1]->{field1});
    $self->assert_null($res->{__recordsets});

}
