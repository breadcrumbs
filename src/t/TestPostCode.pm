package t::TestPostCode;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::PostCode;

sub test_test_post_code{
    my $self = shift;

    my $ans = Bcd::Data::PostCode::is_this_a_test_post_code(12000);
    $self->assert_num_equals(0, $ans);

    $ans = Bcd::Data::PostCode::is_this_a_test_post_code(12000_00);
    $self->assert_num_equals(0, $ans);

    $ans = Bcd::Data::PostCode::is_this_a_test_post_code(12000_90);
    $self->assert_num_equals(1, $ans);

    $ans = Bcd::Data::PostCode::is_this_a_test_post_code(8012898);
    $self->assert_num_equals(1, $ans);

    $ans = Bcd::Data::PostCode::is_this_a_test_post_code(82301099);
    $self->assert_num_equals(0, $ans);

    $ans = Bcd::Data::PostCode::is_this_a_test_post_code(82301919);
    $self->assert_num_equals(1, $ans);

    $ans = Bcd::Data::PostCode::is_this_a_test_post_code(13293);
    $self->assert_num_equals(0, $ans);
}

sub test_generic_post_code{

    my $self = shift;

    my ($ans, $city);

    ($ans, $city) = Bcd::Data::PostCode::is_this_a_generic_post_code("16134");
    $self->assert_num_equals(0, $ans);
    $self->assert_null($city);

    ($ans, $city) = Bcd::Data::PostCode::is_this_a_generic_post_code("16100");
    $self->assert_num_equals(1, $ans);
    $self->assert_equals("Genova", $city);

}

sub test_is_valid_post_code{
    my $self = shift;

    my $ans = Bcd::Data::PostCode::is_valid_post_code("12000");
    $self->assert_num_equals(1, $ans);

    $ans = Bcd::Data::PostCode::is_valid_post_code(10201);
    $self->assert_num_equals(1, $ans);

    $ans = Bcd::Data::PostCode::is_valid_post_code("120009");
    $self->assert_num_equals(0, $ans);

    $ans = Bcd::Data::PostCode::is_valid_post_code("1200");
    $self->assert_num_equals(0, $ans);

    #with letters
    $ans = Bcd::Data::PostCode::is_valid_post_code("1s00");
    $self->assert_num_equals(0, $ans);
}

sub test_is_valid_post_code_with_sub{
    my $self = shift;

    my $ans = Bcd::Data::PostCode::is_valid_post_code_with_sub("1200039");
    $self->assert_num_equals(1, $ans);

    $ans = Bcd::Data::PostCode::is_valid_post_code_with_sub("1613400");
    $self->assert_num_equals(1, $ans);

    $ans = Bcd::Data::PostCode::is_valid_post_code("1200039");
    $self->assert_num_equals(0, $ans);

    $ans = Bcd::Data::PostCode::is_valid_post_code_with_sub("12000ax");
    $self->assert_num_equals(0, $ans);

    $ans = Bcd::Data::PostCode::is_valid_post_code_with_sub("239");
    $self->assert_num_equals(0, $ans);

    $ans = Bcd::Data::PostCode::is_valid_post_code_with_sub("80128002");
    $self->assert_num_equals(0, $ans);
}

sub test_is_valid_search_pattern{
    my $self = shift;
    my $code = "1234567";

    my $ans;
    #the codes from 0 to seven digits are good
    while (1){
	$ans = Bcd::Data::PostCode::is_valid_search_pattern($code);
	$self->assert_num_equals(1, $ans);
	if(chop($code) eq "1"){
	    last;
	}
    }

    $ans = Bcd::Data::PostCode::is_valid_search_pattern("");
    $self->assert_num_equals(1, $ans);
}

sub test_get_min_max_search_range{
    my $self = shift;
    my $code = "";

    my ($min, $max) = Bcd::Data::PostCode::get_min_max_search_range($code);
    $self->assert_equals("0000000", $min);
    $self->assert_equals("9999999", $max);

    ($min, $max) = Bcd::Data::PostCode::get_min_max_search_range("16325");
    $self->assert_equals("1632500", $min);
    $self->assert_equals("1632599", $max);

    ($min, $max) = Bcd::Data::PostCode::get_min_max_search_range("8012800");
    $self->assert_equals("8012800", $min);
    $self->assert_equals("8012800", $max);


    ($min, $max) = Bcd::Data::PostCode::get_min_max_search_range("ci");
    $self->assert_null($min);
    $self->assert_null($max);
    
}

sub test_pretty_print_code{
    my $self = shift;

    my $pretty  = Bcd::Data::PostCode::pretty_print_post_code("8012892");
    $self->assert_equals("80128-92", $pretty);

    $pretty  = Bcd::Data::PostCode::pretty_print_post_code("801289");
    $self->assert_null($pretty);

}
