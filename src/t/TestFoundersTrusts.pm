package t::TestFoundersTrusts;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;
use DBI;
use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::NewAntNests;
use Bcd::Data::Model;
use Bcd::Data::AntNests;
use Bcd::Data::StatementsStash;
use Bcd::Data::FoundersTrusts;
use Data::Dumper;


sub test_add_founder_trust{
    my $self = shift;
    my $stash = $self->{stash};

    my $code = 12393;

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "test_ant_nest");

    my @founder_ids;

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){

	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $code, "john$_", 'john@mail.com' , 'password');

	  push(@founder_ids, $stash->get_last_founder_id());
      }

    #print Dumper(\@founder_ids);

    #ok, Now I have a test ant nest...
    #now I have a test matrix
    #the numbers make the test simpler to check
    my $trusts = 
	[
	 [0.12, 0.13, 0.14, 0.15],
	 [0.21, 0.23, 0.24, 0.25],
	 [0.31, 0.32, 0.34, 0.35],
	 [0.41, 0.42, 0.43, 0.45],
	 [0.51, 0.52, 0.53, 0.54],
	 ];

    #I must make a double loop
    my ($i, $j);
    $i = $j = 0;

    for (@{$trusts}){
	for (@{$_}){

	    $j++ if ($i == $j); #no trust with myself.
	    #ok, I can add the trust
	    #print "Adding trust $i, $j , $_\n";
	    Bcd::Data::FoundersTrusts->add_founder_trust($stash, $founder_ids[$i], $founder_ids[$j], $_);
	      $j++;
	}
	$j = 0;
	$i++;
    }

    #ok, now I should test that the values are correct
    my $sql = qq{SELECT trust_u1_u2, trust_u2_u1 FROM ant_nests_founders_trusts WHERE u1 = ? AND u2 = ?};
    my $st = $stash->prepare_cached($sql);
    

    for (0..3){
	$j = $_ + 1;
	while ($j < 5){

	    #print "Selecting trust from $_ and $j\n";
	    
	    #ok, I take the trust
	    $st->bind_param(1, $founder_ids[$_]);
	    $st->bind_param(2, $founder_ids[$j]);

	    $st->execute();
	    
	    my $row = $st->fetchrow_arrayref();

	    $st->finish();

	    #print Dumper($row);
	    my $temp_i = $_+1;
	    my $temp_j = $j+1;
	    $self->assert_equals("0.$temp_i$temp_j", $row->[0]);
	    $self->assert_equals("0.$temp_j$temp_i", $row->[1]);

	    $j++;
	}
	$j = 0;
    }
}


