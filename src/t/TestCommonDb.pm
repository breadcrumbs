package t::TestCommonDb;

#this should be the base class for all the tests which need a
#connection to the database
use strict;
use DBI;
use lib 'lib';
use Bcd::Data::StatementsStash;
use Bcd::Commands::CommandParser;
use Cache::FastMmap;
use Data::Dumper;
use Bcd::Common::CommonConstants;
use File::Basename;
use File::Find;
use File::Spec;

our @ISA = qw(Test::Unit::TestCase);

sub new{
    my $self = shift->SUPER::new(@_);
    shift;
    my $init_file = shift;

    my $file = __FILE__;
    my $abs_path = File::Spec->rel2abs( $file ) ;
    my $canon_path = File::Spec->canonpath($abs_path);
    my $base = File::Basename::dirname($canon_path);

    #the home is one level up
    my $bcd_home = $base . '/..';

    #I am testing... and I don't want mails
    my $stash = Bcd::Data::StatementsStash->new
	(
	 {
	     #change to suite your needs
	     test        => 1,
	     conn_string => 'DBI:Pg:database=bcdb-test',
	     user_db     => 'lino',
	     expire_time => '30m',
	     share_file  => "/tmp/TEST_bcd_cache",
	     bcd_home    => $bcd_home,
	 }, $init_file
	 );
    $self->{stash} = $stash;
    $stash->in_testing();

    return $self;
}

#when the test is finished, simply return to before.
sub tear_down{
    my $self = shift;
    $self->{stash}->get_connection()->rollback();
}

sub DESTROY{
    my $self = shift;
    $self->{stash}->get_connection()->disconnect();
}

sub _prepare_a_command{
    my $self = shift;
    my $sql = shift;

    my $conn = $self->{stash}->get_connection();
    my $sth = $conn->prepare($sql);

    return $sth;
}

#this function should simply connect as a boss, returns the session...
sub _connect_as_a_boss{
    my $self = shift;
    return $self->_connect_to_bcd('barbara', '8012898', 'barbarap');
}

sub _connect_as_sonia{
    my $self = shift;
    return $self->_connect_to_bcd('sonia', '8012898', 'soniap');
}

sub _connect_as_bc_root{
    my $self = shift;

    my $command = Bcd::Data::Model->instance()->get_command("bc_connect");
    $self->assert_not_null($command);
    my $input = {
	password => "qpqpqp",
    };
    $command->exec_for_test($self->{stash}, $input);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(0, $exit_code);

    my $output = $command->get_output();
    my $hash = Bcd::Commands::CommandParser->parse_command_output($output);
    my $session_id = $hash->{session_id};

    return $session_id;
}

sub _disconnect_from_bcd{
    my ($self, $id) = @_;
    my $stash = $self->{stash};

    #I should do two things...
    my $users_connected = $stash->get_cache()->get
	(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY);

    my $session_id = $users_connected->{$id};

    #remove the session
    $stash->get_cache()->remove($session_id);
    
    #remove the user
    delete ($users_connected->{$id});

    #update the hash
    $stash->get_cache()->set
	(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY,$users_connected)
}

sub _connect_to_bcd{

    my ($self, $user, $ant_nest, $password) = @_;

    my $command = Bcd::Data::Model->instance()->get_command("sv_connect");
    $self->assert_not_null($command);

    my $input = {
	user     => $user,
	ant_nest => $ant_nest,
	password => $password,
    };
    $command->exec_for_test($self->{stash}, $input);


    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(0, $exit_code);

    my $output = $command->get_output();
    my $hash = Bcd::Commands::CommandParser->parse_command_output($output);

    #$self->assert_num_equals(3, $#{$output}+1); #I should have two outputs
    my $session_id = $hash->{session_id};

    return $session_id;
}

#this function will create a simple row in the ant nest table.
sub _create_test_ant_nest{
    my ($self, $radix) = @_;

    my $id = "${radix}99"; #99 is the suffix of test ant nest
    my $name = "test $radix";
    
    my $sql = qq{insert into ant_nests(id, state_id, name) values($id, 1, '$name')};
    my $st = $self->{stash}->get_connection()->prepare($sql);
    $st->execute();

    return $id;
}

sub _create_a_test_user{
    my ($self, $user) = @_;

    my $personal_data_id = Bcd::Data::Users->create_personal_data_for_user($self->{stash}, {});

    my $sql = qq{insert into users(nick, id_ant_nest, }.
	      qq{users_state, alfa, bias, id_personal_data, role_mask) values (?, ?, 1, ?, ?, ?, }.
	      qq{cast(cast(? as integer) as bit(16)))};
    
    my $st = $self->{stash}->get_connection()->prepare($sql);

    $st->bind_param(1, $user->{nick});
    $st->bind_param(2, $user->{id_ant_nest});
    $st->bind_param(3, $user->{alfa});
    $st->bind_param(4, $user->{bias});
    $st->bind_param(5, $personal_data_id);
    $st->bind_param(6, $user->{role});
    $st->execute();

    my $id_of_insertion = $self->{stash}->get_id_of_last_user();

    return $id_of_insertion;
}


1;

