package t::TestAntNestsCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;
use DBI;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Model;
use Bcd::Data::AntNests;
use Bcd::Data::StatementsStash;
use Data::Dumper;


sub test_get_nearest_ant_nests{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $sql = qq{insert into ant_nests(id, state_id, name) values(?, 1, 'name')};
    my $st = $self->{stash}->get_connection()->prepare($sql);

    #the 88888 code should be not existing, but for example there is a post code 88832,
    #so the test could fail in a database with these ant nests
    my @test_codes = (88888_90, 88888_91, 88888_92, 88888_93);

    for (@test_codes){
	$st->bind_param(1, $_);
	$st->execute();
    }

    my $command = Bcd::Data::Model->instance()->get_command("an_get_nearest_ant_nest");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line("88888");
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);

    $self->assert_equals(4, scalar(@{$res->{ant_nests}}));
}

sub test_get_AntNests_from_post_code{
    
    my $self = shift;
    my $stash = $self->{"stash"};

    my $command = Bcd::Data::Model->instance()->get_command("an.lfpc");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    #without parameters... 7002
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_TOO_FEW_PARAMETERS, $exit_code);

    $command->start_parsing_command();
    $command->parse_line('as39');
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE, $exit_code);

    $command->start_parsing_command();
    $command->parse_line('16133439');
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    #without parameters... 7002
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE, $exit_code);

    
    $command->start_parsing_command();
    $command->parse_line('16134');
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    my $output = $command->get_output();

    #all ok
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    #there are only two ant nests which starts at 16134,
    #here I have 5 lines, just because I don't parse the output, in any case
    #the test is successful.
    #print Dumper($output);
    $self->assert_num_equals(3, scalar(@{$output->{ant_nests}}));

    #also this test could fail if there are ant nests with 8 digits...
    my $total_count = Bcd::Data::AntNests->get_count_ant_nests_min_max($stash, 0, 9999999);

    #return all the ant nests
    $command->start_parsing_command();
    $command->parse_line('');
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $output = $command->get_output();
    $self->assert_num_equals($total_count+1, scalar(@{$output->{ant_nests}}));

}

sub test_get_a_single_ant_nest{
   my $self = shift;
    my $stash = $self->{"stash"};

    my $command = Bcd::Data::Model->instance()->get_command("an.lfpc");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->parse_line('1613498');
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $output = $command->get_output();
    $self->assert_num_equals(2, scalar(@{$output->{ant_nests}})); #only one output
   #print Dumper($output);
}


sub test_ant_nests_get_summary{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $command = Bcd::Data::Model->instance()->get_command("an_get_summary");
    $self->assert_not_null($command);

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $output = $command->get_output();

    my $res = Bcd::Commands::CommandParser->parse_command_output($output);

    my $summary = $res->{ant_nests_summary};

    $self->assert_equals (11, $summary->[8]->{ants_count});
}

sub test_different_sizes{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $bc_root_token = $self->_connect_as_bc_root();
    $self->assert_not_null($bc_root_token);

    my $command = Bcd::Data::Model->instance()->get_command("an.create_ant_nest");
    $self->assert_not_null($command);

    ##########
    ## duplicate id

    #ok, now let's try to add an ant nest to the database.
    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $self->_prepare_command_to_create_a_ant_nest($command, "9900011", 4);
    $command->eot();
    $command->exec($stash, 1); #ignore the transaction

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_EVEN_INITIAL_SIZE, $exit_code);


    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $self->_prepare_command_to_create_a_ant_nest($command, "9900011", 1);
    $command->eot();
    $command->exec($stash, 1); #ignore the transaction

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INITIAL_SIZE_OUT_OF_RANGE, $exit_code);

    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $self->_prepare_command_to_create_a_ant_nest($command, "9900011"
						 , Bcd::Constants::AntNestsConstants::MAX_ANT_NESTS_INITIAL_SIZE+2);
    $command->eot();
    $command->exec($stash, 1); #ignore the transaction

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INITIAL_SIZE_OUT_OF_RANGE, $exit_code);
}

sub test_create_ant_nest_correctly{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $bc_root_token = $self->_connect_as_bc_root();
    $self->assert_not_null($bc_root_token);

    my $command = Bcd::Data::Model->instance()->get_command("an.create_ant_nest");
    $self->assert_not_null($command);

    ##########
    ## duplicate id

    #ok, now let's try to add an ant nest to the database.
    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $self->_prepare_command_to_create_a_ant_nest($command, "8012898");
    $command->eot();
    $command->exec_only_for_test($stash); #ignore the transaction

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_EXISTING_ANT_NEST_CODE, $exit_code);

    ##########
    ## invalid post code

    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $self->_prepare_command_to_create_a_ant_nest($command, "s234567");
    $command->eot();
    $command->exec($stash, 1); #ignore the transaction

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE, $exit_code);

    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $self->_prepare_command_to_create_a_ant_nest($command, "8829200");
    $command->eot();
    $command->exec($stash, 1); #ignore the transaction

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now let's count the ant nests in the db with this code
    my $count = Bcd::Data::AntNests->get_count_ant_nests_min_max($stash, "8829200", "8829200");
    $self->assert_num_equals(1, $count);

    $stash->get_connection()->rollback();

    $count = Bcd::Data::AntNests->get_count_ant_nests_min_max($stash, "8829200", "8829200");
    $self->assert_num_equals(0, $count);

}

sub test_create_ant_nest_command_security{
    my $self = shift;
    my $stash = $self->{"stash"};
    my $command = Bcd::Data::Model->instance()->get_command("an.create_ant_nest");
    $self->assert_not_null($command);

    #ok, now let's try to add an ant nest to the database.
    $command->start_parsing_command();
    $command->parse_line("11001100");
    $command->parse_line("Formicaio di test");
    $command->parse_line("very nice totem");
    $command->parse_line("di fronte al bar");
    $command->parse_line("mercoledì di test");
    $command->eot();
    $command->exec($stash, 1); #ignore the transaction


    #no, session, you cannot execute the command
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(7002, $exit_code);

    #ok, now let's try to put a session not existing
    $command->start_parsing_command();
    $command->parse_line("349023480932840239"); #very unlikely sesssion id
    $self->_prepare_command_to_create_a_ant_nest($command, "useless_id");
    $command->eot();
    $command->exec($stash, 1); #ignore the transaction

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code); #unauthorized.

    #ok, now let's try to make a login, but not sufficient
    my $session_boss = $self->_connect_as_a_boss();
    $self->assert_not_null($session_boss);

    #now let's try to create the ant nest with this role
    $command->start_parsing_command();
    $command->parse_line($session_boss); #very unlikely sesssion id
    $self->_prepare_command_to_create_a_ant_nest($command, "useless_id");
    $command->eot();
    $command->exec($stash, 1); #ignore the transaction

    $exit_code = $command->get_exit_code();
    #I should be unauthorized, like before.
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code); #unauthorized.
        

    #at the end of the test I should return as before.
    $stash->get_connection()->rollback();
}

sub _prepare_command_to_create_a_ant_nest{
    my ($self, $command, $id, $size) = @_;

    if (!defined($size)){
	$size = "3";
    }

    $command->parse_line($id);
    $command->parse_line("Formicaio di test");
    $command->parse_line("very nice totem");
    $command->parse_line("di fronte al bar");
    $command->parse_line("mercoledì di test");
    $command->parse_line($size);
}

sub test_get_ant_nest_detail{
    my $self = shift;
    my $stash = $self->{"stash"};
    my $command = Bcd::Data::Model->instance()->get_command("an.get_detail");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line(801289);
    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE, $exit_code);


    $command->start_parsing_command();
    $command->parse_line(8012893);
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_NO_ANT_NEST_IN_THIS_CODE, $exit_code);


    $command->start_parsing_command();
    $command->parse_line(8012898);
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(0, $exit_code);

    my $output = $command->get_output();
    my $res = Bcd::Commands::CommandParser->parse_command_output($output);

    $self->assert_equals(8012898, $res->{ant_nest}->{id});
    $self->assert_equals("Formicaio del Vomero (FORMICAIO DI TEST) (NA)", $res->{ant_nest}->{name});

    $self->assert_equals("barbara", $res->{ant_nest}->{boss_nick});
    $self->assert_equals("tina", $res->{ant_nest}->{treasurer_nick});
   
}
