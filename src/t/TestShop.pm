package t::TestShop;

#this should simply make some tests...
use errors::ErrorCodes;

use t::TestCommon;
our @ISA = qw(t::TestCommon);

sub test_create_unauthorized{
    my $self = shift;
    my $handle = $self->{"server"};

    #Ok, I should try to insert an element

    $self->_try_to_insert();
    $self->check_code(errors::ErrorCodes::BEC_UNAUTHORIZED);
}


#this test should test the insert and delete of a item...
sub test_create_and_delete_item{
    my $self = shift;
    my $handle = $self->{"server"};

    #first of all I should login myself as a (remote) administrator.
    $self->_super_user_connect();

    #Ok, I should try to insert an element

    $self->_try_to_insert();
    $self->check_code(errors::ErrorCodes::BEC_OK);

    #ok, now I try to delete the item which I have inserted.
    #to delete it I need the id.
    #the id is the last id inserted. 

    $self->_delete_last_item_inserted();
    $self->check_code(errors::ErrorCodes::BEC_OK);
}


#this method should simply try to insert an item
sub _try_to_insert{

    my $self = shift;
    my $handle = $self->{"server"};

    $self->write_server_eol("sp.insert_shop_item");
    $self->write_server_eol("2");
    $self->write_server_eol("hi-fi");
    $self->write_server_eol("un bello stereo");
    $self->write_server_eol("1000");
    $self->write_server_eol("394");
    $self->write_server("laggiu`");
    $self->write_server_eot();

}

sub _delete_last_item_inserted{

    my $self = shift;
    my $handle = $self->{"server"};

    $self->write_server_eol("sp.delete_shop_item");
    $self->write_server("2");
    $self->write_server_eot();
    
}

1;
