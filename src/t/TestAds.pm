package t::TestAds;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Ads;
use Data::Dumper;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Model;

sub test_change_locus_for_ad_with_two_loci{
    my $self = shift;
    my $stash= $self->{stash};

    #I create an ad with a presence
    my $id_presence1 = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "Christmas");

    my $id_presence2 = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "Easter");
    
    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "I sell in two loci", $id_presence1, 90, 100, 0.1, 0.2);

    #I add also the other presence to this add
    Bcd::Data::Ads->add_locus_to_ad
	($stash, $ad_id, $id_presence2, 91, 101, 0.2, 0.4);

    #ok, now I try to change the presence 1 in presence 2 and I should fail
    my $session = $self->_connect_as_sonia();
    my $command = Bcd::Data::Model->instance()->get_command("ws.move_ads_user_presence");

    my $input = {
	session_id       => $session,
	id_locus_to_move => $id_presence2,
	id_locus_new     => $id_presence1,
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_AD_WITH_BOTH_LOCI_PRESENT, $exit_code);

}

sub test_change_locus_for_ad{
    my $self = shift;
    my $stash= $self->{stash};

    #I create an ad with a presence
    my $id_presence1 = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "Christmas");

    my $id_presence2 = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "Easter");
    
    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "I sell in two loci", $id_presence1, 90, 100, 0.1, 0.2);

    my $ds = Bcd::Data::Ads->get_ads_for_locus_ds($stash, $id_presence1);
    $self->assert_equals(2, scalar(@{$ds}));

    #ok, now I change the presence for this ad
    Bcd::Data::Ads->change_locus_for_ads($stash, $id_presence2, $id_presence1);

    $ds = Bcd::Data::Ads->get_ads_for_locus_ds($stash, $id_presence2);
 
    #the ad should have been transferred in the new locus
    $self->assert_equals(2, scalar(@{$ds}));

    #ok, then I now try to move the ad using the command...
    my $session = $self->_connect_as_a_boss();
    my $command = Bcd::Data::Model->instance()->get_command("ws.move_ads_user_presence");

    my $input = {
	session_id       => $session,
	id_locus_to_move => $id_presence2,
	id_locus_new     => $id_presence1,
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code);

    #ok, now I connect myself as sonia
    $session = $self->_connect_as_sonia();
    $input->{session_id} = $session;

    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #I should have the ad moved
    $ds = Bcd::Data::Ads->get_ads_for_locus_ds($stash, $id_presence1);
    $self->assert_equals(2, scalar(@{$ds}));

    #the old presence should have been deleted
    my $presence = Bcd::Data::PublicSites->get_presence_hash($stash, $id_presence2);
    $self->assert_null($presence);
}

sub test_get_user_ads_summary_for_locus{
    my $self = shift;
    my $stash= $self->{stash};

    #I create an ad with a presence
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "Christmas");
    
    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "I sell in two loci", $id_presence, 90, 100, 0.1, 0.2);

    #ok, now let's try to have the summary for this person...

    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    my $command = Bcd::Data::Model->instance()->get_command("si.get_user_presences_summary");
    $self->assert_not_null($command);
    
    my $input = {
	session_id         => $session,
    };
    
    $command->exec_for_test($stash, $input);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $service_presences = $res->{service_presences};
    $self->assert_equals(1, scalar(@{$service_presences}));
    
    my $presence = $service_presences->[0];
    $self->assert_equals(1, $presence->{ads_count});
    $self->assert_equals(0, $presence->{special_site});
    $self->assert_equals("Christmas", $presence->{presence});
    $self->assert_equals(1, $presence->{on_request});
    $self->assert_null($presence->{name});
    $self->assert_null($presence->{id_site});

    #ok, now I try the ads in locus command
    $command = Bcd::Data::Model->instance()->get_command("ws.get_ads_in_locus");
    $self->assert_not_null($command);
    
    $input = {
	session_id         => $session,
	id_locus           => $id_presence,
    };
    
    $command->exec_for_test($stash, $input);
    
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    my $ads_in_locus = $res->{ads_in_locus};
    $self->assert_equals(1, scalar(@{$ads_in_locus}));
    $self->assert_equals(90, $ads_in_locus->[0]->{p_min});
    $self->assert_equals("I sell in two loci", $ads_in_locus->[0]->{ad_text});
}

sub test_add_locus_to_ad_cmd{
    my $self = shift;
    my $stash= $self->{stash};


    #I create an ad with a presence
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "special site presence");

    my $count = Bcd::Data::Ads->get_count_ads_for_locus($stash, $id_presence);
    $self->assert_equals(0, $count);

    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "I sell in two loci", $id_presence, 90, 100, 0.1, 0.2);

    $count = Bcd::Data::Ads->get_count_ads_for_locus($stash, $id_presence);
    $self->assert_equals(1, $count);

    #### just to test I add another add with the same locus...
    Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "I have two ads with the same locus", $id_presence, 90, 100, 0.1, 0.2);

    $count = Bcd::Data::Ads->get_count_ads_for_locus($stash, $id_presence);
    $self->assert_equals(2, $count);

    #now I try to get the dataset of these two ads
    my $dataset = Bcd::Data::Ads->get_ads_for_locus_ds($stash, $id_presence);
    
    #I have two ads, so three rows(the first is for the fields)
    $self->assert_equals(3, scalar(@{$dataset}));

    #the count for a not existing presence are zero...
    $count = Bcd::Data::Ads->get_count_ads_for_locus($stash, $id_presence+1);
    $self->assert_equals(0, $count);

    #ok, now I try to add another locus to this ad
    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    my $command = Bcd::Data::Model->instance()->get_command("ws.add_locus_to_ad");
    $self->assert_not_null($command);
    
    my $input = {
	session_id         => $session,
	id_ad              => $ad_id,
	id_locus           => $id_presence,
	p_min              => 100,
	p_max              => 110,
	t_e                => 12,
	t_c                => 99,
    };
    
    $command->exec_for_test($stash, $input);

    #this should fail, because I have a duplicate locus
    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_DUPLICATE_AD_LOCALITY, $exit_code);

    #ok, I try to add another locus
    my $id_site = Bcd::Data::Sites->create_a_site
	($stash, "public library", "oxford street 22", "turn left here, Arthur",
	 1, "mon-fri 9am-12am");

    #my ($class, $stash, $user, $site, $reason, $on_request, $available_hours) = @_;
    #I create another presence
    my $id_presence1 = Bcd::Data::PublicSites->add_presence_in_site
	($stash, 26, $id_site, Bcd::Constants::PublicSitesConstants::OBJECT_SELL_PRESENCE,
	 1, "mon 9-10 am");

    $input->{id_locus} = $id_presence1;
    $command->exec_for_test($stash, $input);

    #this should fail, because I have a duplicate locus
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INCOMPATIBLE_PRESENCE_TYPE, $exit_code);

    #Ok, now I add a presence with the correct type
    $id_presence1 = Bcd::Data::PublicSites->add_presence_in_site
	($stash, 26, $id_site, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE,
	 1, "mon 9-10 am");

    $input->{id_locus} = $id_presence1;
    $command->exec_for_test($stash, $input);

    #Now I should fail, because I don't have the money!
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_FUNDS, $exit_code);

    #I simulate a change in tao
    Bcd::Data::Bank->change_euro_in_tao
	($stash, 26, 8012898, 300);

    #ok, now I also test the price of this ad.
    my ($us_act_e, $us_act_t) = Bcd::Data::Users->get_users_accounts($stash, 26);
    my $total = Bcd::Data::Accounting::Accounts->get_balance($stash, $us_act_t);
    $self->assert_str_equals("5808.81", $total);

    #ok, now the adding of a presence should succed
    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #the ad should cost 0.05 tao
    $total = Bcd::Data::Accounting::Accounts->get_balance($stash, $us_act_t);
    $self->assert_str_equals("5808.76", $total);

    #ok, now I will have two loci for this ad...
    $command = Bcd::Data::Model->instance()->get_command("ws.get_my_ad_details");

    $input = {
	session_id => $session,
	id_ad      => $ad_id,
    };

    $command->exec_for_test($stash, $input);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $ads_loc = $res->{ads_loc};

    #I have two loci
    $self->assert_num_equals(2, scalar(@{$ads_loc}));
}

sub test_get_ad_cmd{
    my $self = shift;
    my $stash= $self->{stash};

    #I create some test ads, with some barriers...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "a presence");

    #ok, Now I should be able to create an ad
    my $ad_id1 = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "first ad", $id_presence, 90, 100, 0.1, 0.2);

    my $ad_id2 = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "second ad", $id_presence, 90, 100, 1e-4, 1e-4);

    #now I try to have the ad from the command

    my $session = $self->_connect_to_bcd('roberto', 8012898, 'robertop');
    my $command = Bcd::Data::Model->instance()->get_command("ws.get_ad");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("${ad_id2}");
    $command->parse_line("${id_presence}");
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    my $ad = $res->{ad};
    $self->assert_equals("a presence", $ad->{presence_text});
    $self->assert_equals("sonia", $ad->{nick});
    $self->assert_equals("service", $ad->{ad_type});
    $self->assert_equals("1", $ad->{on_request});

    #I connect to the ant nest and I see my own ads
    $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    $command = Bcd::Data::Model->instance()->get_command("ws.get_ad");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("${ad_id2}");
    $command->parse_line("${id_presence}");
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    $ad = $res->{ad};
    $self->assert_equals("a presence", $ad->{presence_text});
    $self->assert_equals("sonia", $ad->{nick});
    $self->assert_equals("service", $ad->{ad_type});
    $self->assert_equals("1", $ad->{on_request});

    #I try to see my ads with the new command
    $command = Bcd::Data::Model->instance()->get_command("ws.get_my_ad_details");
    
    my $input = {
	session_id => $session,
	id_ad      => $ad_id2,
    };

    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $ad = $res->{ad};

    $self->assert_equals(26, $ad->{id_user});
    $self->assert_equals("second ad", $ad->{ad_text});
    $self->assert_equals(1, $ad->{id_act});
    $self->assert_equals("Artigianato", $ad->{activity});
    
    my $ads_loc = $res->{ads_loc};

    $self->assert_equals(1, scalar(@{$ads_loc}));
    my $first_loc = $ads_loc->[0];
    #the trust is emitted in bBel
    $self->assert_equals("60.00",        $first_loc->{t_e});
    $self->assert_equals("60.00",        $first_loc->{t_c});
    $self->assert_equals("special_site", $first_loc->{presence_type});
    $self->assert_equals("a presence",   $first_loc->{presence_text});
    $self->assert_equals(1,              $first_loc->{on_request});
    $self->assert_equals(90,             $first_loc->{p_min});
    $self->assert_equals(100,            $first_loc->{p_max});
    
}

sub test_ad_with_two_loci{
    my $self = shift;
    my $stash= $self->{stash};

    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 0, "sat 7-9 pm");

    #ok, Now I should be able to create an ad
    my $ad_id1 = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "I sell used books", $id_presence, 90, 100, 0.1, 0.2);

    #I create a site
    my $id_site = Bcd::Data::Sites->create_a_site
	($stash, "public library", "oxford street 22", "turn left here, Arthur",
	 1, "mon-fri 9am-12am");

    #my ($class, $stash, $user, $site, $reason, $on_request, $available_hours) = @_;
    #I create another presence
    my $id_presence1 = Bcd::Data::PublicSites->add_presence_in_site
	($stash, 26, $id_site, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE,
	 1, "mon 9-10 am");

    #ok, I add this presence to the ad...
    #my ($class, $stash, $ad_id, $id_presence, $p_min, $p_max, $t_e, $t_c) = @_;
    Bcd::Data::Ads->add_locus_to_ad($stash, $ad_id1, $id_presence1, 40, 50, 0.2, 0.7);

    #ok, now I try to get the ad...
    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    my $command = Bcd::Data::Model->instance()->get_command("ws.get_my_ad_details");

    my $input = {
	session_id => $session,
	id_ad      => $ad_id1,
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $ad = $res->{ad};
    $self->assert_equals(26,                  $ad->{id_user});
    $self->assert_equals("I sell used books", $ad->{ad_text});
    $self->assert_equals(1,                   $ad->{id_act});
    $self->assert_equals("service",           $ad->{ad_type});
    
    my $ads_loc = $res->{ads_loc};

    $self->assert_equals(2, scalar(@{$ads_loc}));

    my $loc = $ads_loc->[0];
    $self->assert_equals("90.00",        $loc->{t_e});
    $self->assert_equals("93.01",        $loc->{t_c});
    $self->assert_equals("special_site", $loc->{presence_type});
    $self->assert_equals("sat 7-9 pm",   $loc->{presence_text});
    $self->assert_equals(0,              $loc->{on_request});
    $self->assert_equals(90,             $loc->{p_min});
    $self->assert_equals(100,            $loc->{p_max});   

    $loc = $ads_loc->[1];
    $self->assert_equals("93.01",        $loc->{t_e});
    $self->assert_equals("98.45",        $loc->{t_c});
    $self->assert_equals("an_point",     $loc->{presence_type});
    $self->assert_equals("mon 9-10 am",  $loc->{presence_text});
    $self->assert_equals(1,              $loc->{on_request});
    $self->assert_equals(40,             $loc->{p_min});
    $self->assert_equals(50,             $loc->{p_max});   


    #ok, now I try to have this ad
    $session = $self->_connect_to_bcd('franca', 8012898, 'francap');
    $command = Bcd::Data::Model->instance()->get_command("ws.get_ads");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("1");
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #I should see the same ad, but with two loci!
    my $ads = $res->{ads};

    $self->assert(2, scalar(@{$ads}));
    
    #some things should be the same in the two ads
    $self->assert_equals("sonia", $ads->[0]->{nick});
    $self->assert_equals("sonia", $ads->[1]->{nick});

    $self->assert_equals(26, $ads->[0]->{id_user});
    $self->assert_equals(26, $ads->[1]->{id_user});

    $self->assert_equals("I sell used books", $ads->[0]->{ad_text});
    $self->assert_equals("I sell used books", $ads->[1]->{ad_text});

    #the other should be different
    my $real_site_index = 1;
    my $spec_site_index = 0;
    if (defined($ads->[0]->{id_site})){
	$real_site_index = 0;
	$spec_site_index = 1;
    }
    
    $self->assert_equals("sat 7-9 pm",  $ads->[$spec_site_index]->{presence});
    $self->assert_equals("mon 9-10 am", $ads->[$real_site_index]->{presence});

    $self->assert_null(                    $ads->[$spec_site_index]->{site_name});
    $self->assert_equals("public library", $ads->[$real_site_index]->{site_name});

    $self->assert_equals(0, $ads->[$spec_site_index]->{on_request});
    $self->assert_equals(1, $ads->[$real_site_index]->{on_request});

    $self->assert_equals(90, $ads->[$spec_site_index]->{p_min});
    $self->assert_equals(40, $ads->[$real_site_index]->{p_min});

    $self->assert_equals(1, $ads->[$spec_site_index]->{pay_credit});
    $self->assert_equals(0, $ads->[$real_site_index]->{pay_credit});

    #ok, now I try to login as roberto, I should see only one locus of this ad
    $session = $self->_connect_to_bcd('roberto', 8012898, 'robertop');
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("1");
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $ads = $res->{ads};
    $self->assert_equals(1, scalar(@{$ads}));

    #I should see only the more expensive locus
    $self->assert_equals(90, $ads->[0]->{p_min});

    #ok, no I login as tina, I should not see anything
    $session = $self->_connect_to_bcd('tina', 8012898, 'tinap');
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("1");
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $ads = $res->{ads};
    $self->assert_equals(0, scalar(@{$ads}));

}

sub test_get_other_ant_ads{
    my $self = shift;
    my $stash= $self->{stash};

    #I create some test ads, with some barriers...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "a presence");

    #ok, Now I should be able to create an ad
    my $ad_id1 = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "first ad", $id_presence, 90, 100, 0.1, 0.2);

    my $ad_id2 = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "second ad", $id_presence, 90, 100, 1e-4, 1e-4);

    #first of all I try to get my ads...
    my $ds = Bcd::Data::Ads->get_user_ads_ds($stash, 26);
    $self->assert_equals("first ad", $ds->[1]->[4]);
    $self->assert_equals("second ad", $ds->[2]->[4]);

    #ok, now I test the get ads from activity
    my $ads_act = Bcd::Data::Ads->get_activity_post_code_ads_arr($stash, 1, 8012898);

    #print Dumper($ads_act);

    $self->assert_equals("first ad", $ads_act->[0]->[3]);
    $self->assert_equals("second ad", $ads_act->[1]->[3]);

    #if I am in another ant nest I should see nothing
    $ads_act = Bcd::Data::Ads->get_activity_post_code_ads_arr($stash, 1, 1613498);
    $self->assert_equals(0, scalar(@{$ads_act}));

    #ok... now I try to connect as roberto, I can see these ads from sonia...
    my $session = $self->_connect_to_bcd('roberto', 8012898, 'robertop');
    $self->assert_not_null($session);

    #ok, let's try the command
    my $command = Bcd::Data::Model->instance()->get_command("ws.get_ads");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("1");
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $ads = $res->{ads};

    $self->assert_equals(2, scalar(@{$ads}));

    #Ok, now, I should connect as tina, I should see only one ad
    $session = $self->_connect_to_bcd('tina', 8012898, 'tinap');
    $self->assert_not_null($session);

    #ok, let's try the command
    $command = Bcd::Data::Model->instance()->get_command("ws.get_ads");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("1");
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $ads = $res->{ads};

    $self->assert_equals(1, scalar(@{$ads}));

    #ok, I see only the second ad
    $self->assert_equals("second ad", $ads->[0]->{ad_text});

    #ok, now I should connect as Irene, I should not see anything
    $session = $self->_connect_to_bcd('irene', 8012898, 'irenep');
    $self->assert_not_null($session);

    #ok, let's try the command
    $command = Bcd::Data::Model->instance()->get_command("ws.get_ads");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("1");
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $ads = $res->{ads};

    #irene does not see anything
    $self->assert_equals(0, scalar(@{$ads}));

}


sub test_create_new_ad{
    my $self = shift;
    my $stash= $self->{stash};

    #first of all I should create a presence...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 16, 0, 2, 1, "a presence");

    my $count = Bcd::Data::Ads->get_count_activity_post_code_ads
	($stash, 1, 8012898);

    $self->assert_equals(0, $count);

    #ok, Now I should be able to create an ad
    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 16, "a text ad", $id_presence, 90, 100, 0.1, 0.2);

    #mmm I should have a count of 1
    $count = Bcd::Data::Ads->get_count_activity_post_code_ads
	($stash, 1, 8012898);

    $self->assert_equals(1, $count);

    #ok, now let's try to examine the db
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $ad_id);

    #print Dumper($ad);

    $self->assert_equals(16, $ad->{id_user});
    $self->assert_equals(1, $ad->{id_act});
    $self->assert_equals("a text ad", $ad->{ad_text});

    #mm, I should get the all the ads localities...
    my $locs = Bcd::Data::Ads->get_all_localities_for_ad_arr($stash, $ad_id);

    $self->assert_equals(1, scalar(@{$locs}));
    my $loc = $locs->[0];

    $self->assert_equals($id_presence, $loc->[0]);
    $self->assert_equals(90,           $loc->[1]);
    $self->assert_equals(100,          $loc->[2]);
    $self->assert_equals(0.1,          $loc->[3]); 
    $self->assert_equals(0.2,          $loc->[4]);
					      
}

sub test_create_new_ad_cmd {
    my $self = shift;
    my $stash= $self->{stash};

    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "a presence");

    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    $self->assert_not_null($session);

    #ok, let's try the command
    my $command = Bcd::Data::Model->instance()->get_command("ws.create_ad");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("1");
    $command->parse_line("This is a text line");
    $command->parse_line("second text line");
    $command->parse_line(".");
    $command->parse_line($id_presence);
    $command->parse_line(100); 
    $command->parse_line(0); 
    $command->parse_line("0");
    $command->parse_line("60");
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_FUNDS, $exit_code);

    #I make some funds in the bank
    my ($amount_deposited, $total_euro) = 
	Bcd::Data::Bank->deposit_user_account($stash, 26, 8012898, '100');

    my $amount_to_change = 50;
    my ($acc_e, $act_t) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 26, 8012898, $amount_to_change);

    #print "acc_e = $acc_e acc_t = $act_t\n";

    $self->assert_str_equals("968.135", $act_t);

    #ok, Now I can repeat the command...
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    
    $self->assert_str_equals("968.11668", $res->{new_total_tao});

    #ok, now I check that the ad is existing...
    my $ad_id = $res->{ad_id};
    
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $ad_id);

    $self->assert_str_equals("This is a text line\nsecond text line", $ad->{ad_text});

    my $income_ant_nest = Bcd::Data::AntNests->get_tao_ads_partial_income_id($stash, 8012898);

    my ($arr, $tot) = Bcd::Data::Accounting::Accounts->get_account_summary_ds
	($stash, $income_ant_nest);

    my $record = $arr->[1];

    $self->assert_str_equals("0.01832", $record->[3]);

    #let's see if sonia can see her ad
    my $ds = Bcd::Data::Ads->get_user_ads_ds($stash, 26);

    $ad = $ds->[1];

    $self->assert_equals(1, $ad->[1]);
    $self->assert_str_equals("This is a text line\nsecond text line", $ad->[4]);

    #ok, now I could test also the command to get my ads...
    $command = Bcd::Data::Model->instance()->get_command("ws.get_my_ads");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $my_ads = $res->{my_ads};

    $self->assert_equals(1, scalar(@{$my_ads}));

    $ad = $my_ads->[0];
    
    $self->assert_equals(1, $ad->{id_act});
    $self->assert_str_equals("This is a text line\nsecond text line", $ad->{ad_text});
}
