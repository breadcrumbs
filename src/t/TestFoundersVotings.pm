package t::TestFoundersVotings;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;

use Data::Dumper;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::FoundersVotings;
use Bcd::Data::NewAntNests;
use Bcd::Constants::NewAntNestsConstants;

sub set_up{

    my $self = shift;
    my $stash = $self->{stash};

    $self->{code} = 12393;
    $self->{founder_ids} = [];

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $self->{code}, "test_ant_nest");

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){

	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $self->{code}, "john$_", "john$_\@mail.com" , 'password');

	  push(@{$self->{founder_ids}}, $stash->get_last_founder_id());
      }
}

sub test_add_some_votes{
    my $self = shift;
    my $stash = $self->{stash};

    #I play with some votes...
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, $self->{founder_ids}->[0], undef);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, $self->{founder_ids}->[0], 
					   $self->{founder_ids}->[1]);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, $self->{founder_ids}->[0], undef);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, undef, $self->{founder_ids}->[1]);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, undef, $self->{founder_ids}->[2]);

    #then I get the votes...
    my $res = Bcd::Data::FoundersVotings->get_votes_result($stash, $self->{code});


    #ok, now let's see if there is what I expect
    my $bosses_votes = $res->{bosses};

    #two types of votes
    $self->assert_equals(2, scalar(@{$bosses_votes}));

    $self->assert_equals($self->{founder_ids}->[0], $bosses_votes->[0]->[0]);
    $self->assert_equals(3, $bosses_votes->[0]->[1]);

    my $treasurer_votes = $res->{treasurers};

    $self->assert_equals(3, scalar(@{$treasurer_votes}));

    $self->assert_equals($self->{founder_ids}->[1], $treasurer_votes->[0]->[0]);
    $self->assert_equals(2, $treasurer_votes->[0]->[1]);

    $self->assert_equals($self->{founder_ids}->[2], $treasurer_votes->[1]->[0]);
    $self->assert_equals(1, $treasurer_votes->[1]->[1]);

    #then I delete all
    Bcd::Data::FoundersVotings->delete_all_votes_for_this_ant_nest($stash, $self->{code});

    #I should have zero votes
    $res = Bcd::Data::FoundersVotings->get_votes_result($stash, $self->{code});

    $bosses_votes = $res->{bosses};
    $self->assert_equals(0, scalar(@{$bosses_votes}));
    $treasurer_votes = $res->{treasurers};
    $self->assert_equals(0, scalar(@{$treasurer_votes}));
    
    
}
