#!/usr/bin/perl -w

use strict;

use Test::Unit::Debug qw(debug_pkgs);
use Test::Unit::HarnessUnit;

#debug_pkgs(qw{Test::Unit::Result});

use lib 't/tlib', 'tlib';

#no it does not work, probably it is better to create a test database
#with a fake password.

# print "ok, let's give me a password for the super user bc-root,\n just for the test session\n";
# my $bc_pass = <STDIN>;
# chomp $bc_pass;
# print "you want !${bc_pass}! as the password\n";


my $testrunner = Test::Unit::HarnessUnit->new();
$testrunner->start("AllTests");
