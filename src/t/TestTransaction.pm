package t::TestTransaction;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Data::Dumper;

use DBI;
use t::TestCommonDb;

use Bcd::Data::Accounting::Transaction;
use Bcd::Data::Accounting::AccountsPlan;
use Bcd::Data::StatementsStash;

our @ISA = qw(t::TestCommonDb);

sub test_basic_transaction{
    my $self = shift;

    my $conn = $self->{stash}->get_connection();
    my $stash = $self->{"stash"};

    my $list_debit_accounts = [0,2,4];
    my $list_credit_accounts = [6, 8, 10];
    my $list_debit_amounts = [10,20,30];
    my $list_credit_amounts = [30, 20, 10];
    my $description = "test transaction";

    my ($debits, $credits) = 
	Bcd::Data::Accounting::Transaction->add_new_transaction_in_db
	($list_debit_accounts, $list_debit_amounts,
	 $list_credit_accounts, $list_credit_amounts, 
	 $description, $stash);

    $self->assert_equals(10, $debits->[0]);
    $self->assert_equals(-20, $debits->[1]);
    $self->assert_equals(-30, $debits->[2]);

    $self->assert_equals(-30, $credits->[0]);
    $self->assert_equals(20, $credits->[1]);
    $self->assert_equals(-10, $credits->[2]);

    #ok, let's take the transaction id and then we make some assumptions...
    my $sql = qq{ select amount_debit, amount_credit from transactions_splits_e 
		      where id_account = ? and id_transaction = lastval()};
    my $sth = $conn->prepare($sql);

    my $index = 0;

    foreach(@{$list_debit_accounts}){
	$sth->bind_param(1, $_);
	$sth->execute();
	
	my $debit;
	my $credit;

	$sth->bind_columns(undef, \$debit, \$credit);
	$sth->fetch();

	$self->assert_num_equals($list_debit_amounts->[$index], $debit);
	
	$sth->finish();

	$index++;
    }

    $index = 0;

    foreach(@{$list_credit_accounts}){
	$sth->bind_param(1, $_);
	$sth->execute();
	
	my $debit;
	my $credit;

	$sth->bind_columns(undef, \$debit, \$credit);
	$sth->fetch();

	$self->assert_num_equals($list_credit_amounts->[$index], $credit);
	
	$sth->finish();

	$index++;
    }

    $self->{stash}->get_connection()->rollback();
}
