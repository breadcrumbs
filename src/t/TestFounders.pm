package t::TestFounders;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Constants::NewAntNestsConstants;
use Bcd::Data::Founders;
use Bcd::Data::NewAntNests;

use Data::Dumper;

sub set_up{
    my $self = shift;
    my $stash = $self->{stash};

    $self->{code} = 12393;
    $self->{founder_ids} = [];

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $self->{code}, "test_ant_nest");

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){

	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $self->{code}, "john$_", "john$_\@mail.com" , 'password');

	  push(@{$self->{founder_ids}}, $stash->get_last_founder_id());
      }
}

sub test_get_complete_founder_data{
    my $self = shift;
    my $stash = $self->{stash};

    my $hash = Bcd::Data::Founders->get_founder_complete_data_hash
	($stash, $self->{founder_ids}->[0]);
    #this because otherwise you could get the other founder's profile
    $self->assert_equals($hash->{id}, $hash->{id_user});
}

sub test_candidates_flag{
    my $self = shift;
    my $stash = $self->{stash};

    my $bosses;
    my $treasurers;

    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[0], '1', '0');

    
    my $founder = Bcd::Data::Founders->get_founder_main_data_arr($stash, $self->{founder_ids}->[0]);
    $self->assert_equals(1, $founder->[7]);
    $self->assert_equals(0, $founder->[8]);

    $bosses = Bcd::Data::Founders->get_candidates_bosses($stash, $self->{code});
    $treasurers = Bcd::Data::Founders->get_candidates_treasurers($stash, $self->{code});

    $self->assert_equals(1, scalar(@{$bosses}));
    $self->assert_equals(0, scalar(@{$treasurers}));

    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[0], '0', '1');
    
    $founder = Bcd::Data::Founders->get_founder_main_data_arr($stash, $self->{founder_ids}->[0]);
    $self->assert_equals(0, $founder->[7]);
    $self->assert_equals(1, $founder->[8]);

    $bosses = Bcd::Data::Founders->get_candidates_bosses($stash, $self->{code});
    $treasurers = Bcd::Data::Founders->get_candidates_treasurers($stash, $self->{code});

    $self->assert_equals(0, scalar(@{$bosses}));
    $self->assert_equals(1, scalar(@{$treasurers}));

    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[0], '0', '0');
    
    $founder = Bcd::Data::Founders->get_founder_main_data_arr($stash, $self->{founder_ids}->[0]);
    $self->assert_equals(0, $founder->[7]);
    $self->assert_equals(0, $founder->[8]);

    $bosses = Bcd::Data::Founders->get_candidates_bosses($stash, $self->{code});
    $treasurers = Bcd::Data::Founders->get_candidates_treasurers($stash, $self->{code});

    $self->assert_equals(0, scalar(@{$bosses}));
    $self->assert_equals(0, scalar(@{$treasurers}));

    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[0], '1', '0');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[1], '1', '0');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[2], '0', '1');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[3], '0', '1');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[4], '0', '1');

    $bosses = Bcd::Data::Founders->get_candidates_bosses($stash, $self->{code});
    $self->assert_equals(2, scalar(@{$bosses}));

    $treasurers = Bcd::Data::Founders->get_candidates_treasurers($stash, $self->{code});
    $self->assert_equals(3, scalar(@{$treasurers}));

    #ok, now I test the nomination of the boss and the treasurer
    Bcd::Data::Founders->nominate_boss_and_treasurer
	($stash, $self->{code}, $self->{founder_ids}->[0], $self->{founder_ids}->[2]);

    $bosses = Bcd::Data::Founders->get_candidates_bosses($stash, $self->{code});
    $self->assert_equals(1, scalar(@{$bosses}));

    $treasurers = Bcd::Data::Founders->get_candidates_treasurers($stash, $self->{code});
    $self->assert_equals(1, scalar(@{$treasurers}));
}

sub test_get_confirmed_founders_to_email{
    my $self = shift;
    my $stash = $self->{stash};

    
    my $founders = Bcd::Data::Founders->get_confirmed_founders_to_email($stash, 12393);

    #the initial array is empty
    $self->assert_equals(0, scalar(@{$founders}));
}

sub test_get_founders_to_emails{   
    my $self = shift;
    my $stash = $self->{stash};

    my $founders = Bcd::Data::Founders->get_founders_to_email($stash, 12393);

    $self->assert_equals(5, scalar(@{$founders}));

    for (@{$founders}){
	my $nick = $_->[0];
	my $id;

	if ($nick =~ /john(\d)/){
	    $id = $1;
	}
	
	$self->assert_equals("john$id\@mail.com", $_->[2]);
	$self->assert_equals($self->{founder_ids}->[$id-1], $_->[3]);
    }
}
