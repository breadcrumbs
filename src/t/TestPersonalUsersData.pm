package t::TestPersonalUsersData;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Users;
use Bcd::Data::PersonalUsersData;
use Bcd::Data::Model;


use Data::Dumper;
#ok, now let's see if the data is created correctly...

sub test_basic_personal_data_trusts{
    my $self = shift;
    my $stash= $self->{stash};

    #let's get the id of the personal data of a user...
    my $personal_data_id = Bcd::Data::Users->get_user_personal_data_id
	($stash, 16);

    my $trusts = Bcd::Data::PersonalUsersData->get_personal_users_data_trusts_hash
	($stash, $personal_data_id);

    $self->assert_null($trusts);

    #ok, now I insert the trusts...
    Bcd::Data::PersonalUsersData->insert_personal_users_data_trusts
	($stash, $personal_data_id, 1, 2, 3, 4, 5, 6, 7, 8);

    $trusts = Bcd::Data::PersonalUsersData->get_personal_users_data_trusts_hash
	($stash, $personal_data_id);

    #print Dumper($trusts);

    $self->assert_equals(1, $trusts->{t_first_name});
    $self->assert_equals(2, $trusts->{t_last_name});
    $self->assert_equals(3, $trusts->{t_address});
    $self->assert_equals(4, $trusts->{t_home_phone});
    $self->assert_equals(5, $trusts->{t_mobile_phone});
    $self->assert_equals(6, $trusts->{t_sex});
    $self->assert_equals(7, $trusts->{t_birthdate});
    $self->assert_equals(8, $trusts->{t_email});

};

sub test_get_profile_with_personal_data_trusts{
    my $self = shift;
    my $stash= $self->{stash};

    #I have the command to get my profile
    my $session = $self->_connect_as_a_boss();
    $self->assert_not_null($session);

    #ok, now I get my profile
    my $command = Bcd::Data::Model->instance()->get_command("us_get_my_profile");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals(1, $res->{missing_trusts});

    #ok, now I try to insert the trusts.
    my $personal_data_id = Bcd::Data::Users->get_user_personal_data_id
	($stash, 16);

    Bcd::Data::PersonalUsersData->insert_personal_users_data_trusts
	($stash, $personal_data_id, 1, 2, 3, 4, 5, 6, 7, 8);

    
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $trusts = $res->{personal_trusts};

    #the results are > 100 because the command returns the result in bBel
    $self->assert_equals(100, $trusts->{t_first_name});
    $self->assert_equals(103.01, $trusts->{t_last_name});
    $self->assert_equals(104.77, $trusts->{t_address});
    $self->assert_equals(106.02, $trusts->{t_home_phone});
    $self->assert_equals(106.99, $trusts->{t_mobile_phone});
    $self->assert_equals(107.78, $trusts->{t_sex});
    $self->assert_equals(108.45, $trusts->{t_birthdate});
    $self->assert_equals(109.03, $trusts->{t_email});

}

sub test_insert_personal_data_trusts{
    my $self = shift;
    my $stash= $self->{stash};

    #I have the command to get my profile
    my $session = $self->_connect_as_a_boss();
    $self->assert_not_null($session);

    #ok, now I get my profile
    my $command = Bcd::Data::Model->instance()->get_command("us_insert_personal_data_trusts");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);

    $command->parse_line(100);
    $command->parse_line(90);
    $command->parse_line(80);
    $command->parse_line(70);
    $command->parse_line(60);
    $command->parse_line(50);
    $command->parse_line(40);
    $command->parse_line(30);

    $command->eot();
    $command->exec_only_for_test($stash);

    #ok, then I get the personal data.

    my $personal_data_id = Bcd::Data::Users->get_user_personal_data_id
	($stash, 16);

    my $trusts = Bcd::Data::PersonalUsersData->get_personal_users_data_trusts_hash
	($stash, $personal_data_id);

    
    $self->assert_equals("1", $trusts->{t_first_name});
    $self->assert_equals("0.1", $trusts->{t_last_name});
    $self->assert_equals("0.01", $trusts->{t_address});
    $self->assert_equals("0.001", $trusts->{t_home_phone});
    $self->assert_equals("1e-4", $trusts->{t_mobile_phone});
    $self->assert_equals("1e-5", $trusts->{t_sex});
    $self->assert_equals("1e-6", $trusts->{t_birthdate});
    $self->assert_equals("1e-7", $trusts->{t_email});
}

sub test_change_personal_data{
    my $self = shift;
    my $stash= $self->{stash};

    my $personal_data_id = Bcd::Data::Users->get_user_personal_data_id
	($stash, 16);

    Bcd::Data::PersonalUsersData->insert_personal_users_data_trusts
	($stash, $personal_data_id, 1, 2, 3, 4, 5, 6, 7, 8);

    #ok, now I change this data...
    Bcd::Data::PersonalUsersData->change_personal_users_data_trust
	($stash, "t_first_name", $personal_data_id, 99);

    Bcd::Data::PersonalUsersData->change_personal_users_data_trust
	($stash, "t_last_name", $personal_data_id, 100);

    Bcd::Data::PersonalUsersData->change_personal_users_data_trust
	($stash, "t_address", $personal_data_id, 101);

    Bcd::Data::PersonalUsersData->change_personal_users_data_trust
	($stash, "t_home_phone", $personal_data_id, 102);

    Bcd::Data::PersonalUsersData->change_personal_users_data_trust
	($stash, "t_mobile_phone", $personal_data_id, 103);

    Bcd::Data::PersonalUsersData->change_personal_users_data_trust
	($stash, "t_sex", $personal_data_id, 104);

    Bcd::Data::PersonalUsersData->change_personal_users_data_trust
	($stash, "t_birthdate", $personal_data_id, 105);

    Bcd::Data::PersonalUsersData->change_personal_users_data_trust
	($stash, "t_email", $personal_data_id, 106);

    #ok, now I get the trust...

    my $trusts = Bcd::Data::PersonalUsersData->get_personal_users_data_trusts_hash
	($stash, $personal_data_id);
    
    $self->assert_equals("99", $trusts->{t_first_name});
    $self->assert_equals("100", $trusts->{t_last_name});
    $self->assert_equals("101", $trusts->{t_address});
    $self->assert_equals("102", $trusts->{t_home_phone});
    $self->assert_equals("103", $trusts->{t_mobile_phone});
    $self->assert_equals("104", $trusts->{t_sex});
    $self->assert_equals("105", $trusts->{t_birthdate});
    $self->assert_equals("106", $trusts->{t_email});
}

sub test_change_personal_data_command{

    my $self = shift;
    my $stash= $self->{stash};

    my $personal_data_id = Bcd::Data::Users->get_user_personal_data_id
	($stash, 16);

    Bcd::Data::PersonalUsersData->insert_personal_users_data_trusts
	($stash, $personal_data_id, 1, 2, 3, 4, 5, 6, 7, 8);

    my $session = $self->_connect_as_a_boss();
    $self->assert_not_null($session);

    my $command = Bcd::Data::Model->instance()->get_command("us_change_personal_data_trust");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("t_first_name");
    $command->parse_line(11);
    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);


    my $trusts = Bcd::Data::PersonalUsersData->get_personal_users_data_trusts_hash
	($stash, $personal_data_id);
    
    $self->assert_equals("1.25892541179417e-09", $trusts->{t_first_name});    
}
