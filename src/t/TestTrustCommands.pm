package t::TestTrustCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use t::TestCommonDb;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::StatementsStash;
use Bcd::Data::Model;
use Bcd::Constants::Users;
use Bcd::Commands::CommandParser;
use Cache::FastMmap;
use Data::Dumper;

our @ISA = qw(t::TestCommonDb);



sub test_create_booking_command{
    my $self = shift;
    my $stash = $self->{stash};

    #ok, let's test the command
    my $command = Bcd::Data::Model->instance()->get_command("tr.new_trust");
    $self->assert_not_null($command);
    $command->parse_line("bad session");
    $command->parse_line("bad nick");
    $command->parse_line("bad_token");
    $command->parse_line("39"); #bad trust
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code);

    #ok, now let's try to connect
    my $session_id = $self->_connect_as_a_boss();

    $command->{session_id} = $session_id;
    $command->eot();
    
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_TRUST
			     , $exit_code);

    $command->{trust} = 0.39;
    $command->eot();
    
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST
			     , $exit_code);

    $command->{nick} = "tina";
    $command->eot();
    
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_DUPLICATE_TRUST_NOT_ALLOWED
			     , $exit_code);


    $command->{nick} = "sonia";
    $command->eot();
    
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_TOTEM
			     , $exit_code);


    $command->{totem} = "soniat";
    $command->eot();


    
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);


    #now I try to execute it another time
    $command->eot();


    
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_DUPLICATE_BOOKING
 			     , $exit_code);

}

sub test_create_trust_command{

    my $self = shift;
    my $stash = $self->{stash};

    my $session_id = $self->_connect_as_a_boss();
    
    #first of all I create a booking
    my $command = Bcd::Data::Model->instance()->get_command("tr.new_trust");
    $self->assert_not_null($command);
    $command->parse_line($session_id);
    $command->parse_line("sonia");
    $command->parse_line("soniat");
    $command->parse_line("0.23");
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);


    #ok, then I try to create a trust.

    #I login using a different user
    my $franca_session = $self->_connect_to_bcd("franca", "8012898", "francap");

    $command = Bcd::Data::Model->instance()->get_command("tr.new_trust");
    $self->assert_not_null($command);

    $command->parse_line($franca_session);
    $command->parse_line("barbara");
    $command->parse_line("barbarat");
    $command->parse_line("0.39");

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);
    my $output = $command->get_output();

    my $res = Bcd::Commands::CommandParser->parse_command_output($output);
    $self->assert_equals(1, $res->{new_booking});

    #I login using the right user
    my $sonia_session = $self->_connect_to_bcd("sonia", "8012898", "soniap");
    $command->{session_id} = $sonia_session;
    $command->{totem} = "cippo";

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_TOTEM
 			     , $exit_code);

    
    $command->{totem} = "barbarat";
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);

    $output = $command->get_output();

    $res = Bcd::Commands::CommandParser->parse_command_output($output);
    $self->assert_equals(0, $res->{new_booking});

    #ok, now there should exist a direct path between sonia and barbara
    #sub exists_direct_trust_between{
    # my ($self, $stash, $u_start, $u_end) = @_;

    my $user = Bcd::Data::Users->select_user_data_from_nick_ant_nest($stash, "8012898", "sonia");

    $res = Bcd::Data::Trust->exists_direct_trust_between($stash, 16, $user->{id});
    $self->assert_equals(1, $res);

     
}

sub test_change_trust{

    my $self = shift;
    my $stash = $self->{stash};
    my $session_id = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("tr.change_trust");
    $self->assert_not_null($command);
    $command->parse_line($session_id);
    $command->parse_line("sonia");
    $command->parse_line("0.23");
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_NO_TRUST_TO_CHANGE
 			     , $exit_code);    

    #ok, now I try to change a direct trust
    $command->{nick} = "tina";
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);    

    #now the trust should be changed
    my ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, 16, 17);
    $self->assert_equals("0.23", $trust);

    $command = Bcd::Data::Model->instance()->get_command("tr.rollback_change_trust");
    $self->assert_not_null($command);
    $command->parse_line($session_id);
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);    

    ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, 16, 17);
    $self->assert_equals("0.99", $trust);
    
}

sub test_commit_trust_change{
    my $self = shift;
    my $stash = $self->{stash};
    my $session_id = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("tr.change_trust");
    $self->assert_not_null($command);
    $command->parse_line($session_id);
    $command->parse_line("tina");
    $command->parse_line("0.23");
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);    

    #ok, now I commit the trust change

    my $command_c = Bcd::Data::Model->instance()->get_command("tr.commit_trust_change");
    $self->assert_not_null($command_c);
    $command_c->parse_line($session_id);
    $command_c->eot();

    $command_c->exec_only_for_test($stash);
    $exit_code = $command_c->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);    

    my ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, 16, 17);
    $self->assert_equals("0.23", $trust);

    #If I change another time and rollback, I will return to 0.23 NOT 0.99

    $command->{trust} = 0.45;

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);    

    #ok, now I rollback

    $command = Bcd::Data::Model->instance()->get_command("tr.rollback_change_trust");
    $self->assert_not_null($command);
    $command->parse_line($session_id);
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);    

    ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, 16, 17);
    $self->assert_equals("0.23", $trust);
}

sub test_get_trusted_ants{

    my $self = shift;
    my $stash = $self->{stash};
    my $session_id = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("tr.get_trusted_ants");
    $self->assert_not_null($command);
    $command->parse_line($session_id);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);    
    
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    
    #I should verify that the res contains the trusts... some of them at least...

    $self->assert_equals(1, $self->_is_this_nick_trusted("marco", $res));
    $self->assert_equals(1, $self->_is_this_nick_trusted("tina", $res));
    $self->assert_equals(1, $self->_is_this_nick_trusted("irene", $res));
    $self->assert_equals(0, $self->_is_this_nick_trusted("bob", $res));
}

#helper function...
sub _is_this_nick_trusted{
    my ($self, $nick, $res) = @_;

    for (@{$res->{trusted_ants}}){
	if ($_->{nick} eq $nick){
	    return 1;
	}
    }

    #no ant
    return 0;
}

sub test_get_reachable_set{
    my $self = shift;
    my $stash = $self->{stash};
    my $session_id = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("tr.get_reachable_set");
    $self->assert_not_null($command);
    $command->parse_line($session_id);
    $command->parse_line(60);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK
 			     , $exit_code);    
    
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    $self->assert_num_equals(10, scalar(@{$res->{reachable_set}}));
    
}

1;
