package t::TestDeposits;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#use Math::BigInt lib => "GMP";
use Digest::SHA1;

use Data::Dumper;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::StatementsStash;
use Bcd::Data::Deposits;
use Bcd::Data::Bank;



sub test_zero_deposit_pending{
    my $self = shift;
    my $stash = $self->{stash};

    #16 is barbara...
    my $res = Bcd::Data::Deposits->is_there_a_pending_request_for_this_user($stash, 16);
    $self->assert_num_equals(0, $res);
    
}

sub test_withdrawal{
    my $self = shift;
    my $stash = $self->{stash};

    #to test a withdrawal I should first make a deposit ( the command should do this test,
    #but now we are testing only the function, not the command).

    my ($amount_deposited, $total ) = Bcd::Data::Bank->deposit_user_account
	($stash, 16, 8012898, "1030");

    #I want to withdraw a certain sum
    my ($booking_token, $long_token) = 
	Bcd::Data::Deposits->create_withdrawal_booking($stash, 16, "400");

    my $bookings_created = Bcd::Data::Deposits->get_all_created_withdrawals_ant_nest_ds($stash, 8012898);

    $self->assert_num_equals(2, scalar(@{$bookings_created}));
    $self->assert_equals($booking_token, $bookings_created->[1]->[0]);
    $self->assert_equals("400", $bookings_created->[1]->[1]);    

    my $row = Bcd::Data::Deposits->get_withdrawal_booking_from_user_and_token
	($stash, 16, $booking_token);


    $self->assert_equals('0',   $row->[1]); #is_a_deposit_or_a_withdrawal
    $self->assert_equals(16,    $row->[2]); #userid
    $self->assert_equals("400", $row->[3]); #amount
    $self->assert_equals(Bcd::Constants::DepositsConstants::CREATION_STATE, $row->[4]); #Status
    $self->assert_equals($booking_token, $row->[5]); 

    my $blinded_token = $row->[6];
    my $hash_secret = $row->[7];

    #I should be able to obtain the secret from the long and the blinded token
    my $secret = Bcd::Data::Token::unblind_the_token($long_token, $blinded_token);

    #I make a hash of the secret..
    my $sha = Digest::SHA1->new;
    $sha->add($secret);
    my $digest = $sha->hexdigest;

    $self->assert_equals($digest, $hash_secret);

    #ok, I should now acknoledge this withdrawal... the treasurer
    #has verified that the blinded token and the full token match,
    #and so has given the user the euros...
    Bcd::Data::Deposits->treasurer_acknowledged($stash, $row->[0]);

    #now the treasurer claims the token 

    my ($amount_deposited_or_withdrawn, $new_total) = Bcd::Data::Deposits->claim_this_ticket
	($stash, 8012898, $row->[0]);

    $self->assert_equals($new_total, $amount_deposited-$amount_deposited_or_withdrawn);


    
}

sub test_create_deposit_request{
    my $self = shift;
    my $stash = $self->{stash};

    #barbara wants to deposit 3.29 Euro = 329 cent
    my $amount = "329";

    my ($booking_token, $blinded_token) = 
	Bcd::Data::Deposits->create_deposit_booking($stash, 16,  $amount);

    my $booking = Bcd::Data::Deposits->get_pending_booking_from_user_id_arr($stash, 16);

    #the booking should be the same
    $self->assert_equals("329", $booking->[3]);

    #ok, now I try to delete this booking
    Bcd::Data::Deposits->delete_booking_id($stash, $booking->[0]);

    $booking = Bcd::Data::Deposits->get_pending_booking_from_user_id_arr($stash, 16);

    $self->assert_null($booking);

    #ok, I will replay the request
    ($booking_token, $blinded_token) = 
	Bcd::Data::Deposits->create_deposit_booking($stash, 16,  $amount);

    my $bookings_created = Bcd::Data::Deposits->get_all_created_deposits_ant_nest_ds($stash, 8012898);

    $self->assert_num_equals(2, scalar(@{$bookings_created}));
    $self->assert_equals($booking_token, $bookings_created->[1]->[0]);
    $self->assert_equals("329", $bookings_created->[1]->[1]);
    

    #ok, now let's go into the database and see if the request is correctly put
#     my $sql = qq{select * from deposits where id_user=16};
#     my $st  = $self->_prepare_a_command($sql);

#     $st->execute();
#     my $row = $st->fetchrow_arrayref(); #I should have only one row...

    my $row = Bcd::Data::Deposits->
	get_deposit_booking_from_user_and_token($stash, 16, $booking_token);

    $self->assert_equals('1',   $row->[1]); #is_a_deposit_or_a_withdrawal
    $self->assert_equals(16,    $row->[2]); #userid
    $self->assert_equals("329", $row->[3]); #amount
    $self->assert_equals(Bcd::Constants::DepositsConstants::CREATION_STATE, $row->[4]); #Status
    $self->assert_equals($booking_token, $row->[5]); 

    my $full_token = $row->[6];
    my $hash_secret = $row->[7];

    #I should be able to obtain the secret from the full and the blinded...

    my $secret = Bcd::Data::Token::unblind_the_token($full_token, $blinded_token);

    #I make a hash of the secret..
    my $sha = Digest::SHA1->new;
    $sha->add($secret);
    my $digest = $sha->hexdigest;

    $self->assert_equals($digest, $hash_secret);

    #ok, now let's try to acknoledge this request

    Bcd::Data::Deposits->treasurer_acknowledged($stash, $row->[0]);

    #let's get again the booking
    $row = Bcd::Data::Deposits->get_deposit_booking_from_user_and_token
	($stash, 16, $booking_token);

    #nothing has changed, except the state
    $self->assert_equals(16,     $row->[2]); #userid
    $self->assert_equals("329", $row->[3]); #amount
    $self->assert_equals(Bcd::Constants::DepositsConstants::OFFLINE_ACKNOWLEDGED, $row->[4]); #Status
    $self->assert_equals($booking_token, $row->[5]); 
    $self->assert_equals($digest, $hash_secret);

    #now let's try to claim this token
    my ($amount_deposited, $new_total) = Bcd::Data::Deposits->claim_this_ticket
	($stash, 8012898, $row->[0]);

    $self->assert_equals("327", $amount_deposited);

}
