package t::TestBank;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#use Math::BigInt lib => "GMP";


use DBI;
#this is the class I want to test
use t::TestCommonDb;
use Bcd::Data::Bank;
use Bcd::Data::Accounting::AccountsPlan;
use Bcd::Data::StatementsStash;

our @ISA = qw(t::TestCommonDb);

use Data::Dumper;

sub test_pay_and_collect_invoices{
    my $self = shift;
    my $stash = $self->{stash};

    my $old_total = 
	Bcd::Data::Accounting::Accounts->get_balance($stash, Bcd::Common::BasicAccounts::PARKING_TAO_FUND->[0]);

    #I simulate first of all a payment... the invoice can also be not existing...
    my $total = Bcd::Data::Bank->ant_pays_invoice($stash, 16, 99, 1000);
    $self->assert_equals(-1000, $total);
    #the parking should have been credited
    $total = 
	Bcd::Data::Accounting::Accounts->get_balance($stash, Bcd::Common::BasicAccounts::PARKING_TAO_FUND->[0]);
    $self->assert_equals(1000, $total-$old_total);

    #then I simulate a collect
    $total = Bcd::Data::Bank->ant_collects_invoice($stash, 18, 99, 1000);
    $self->assert_equals(1000, $total);

    $total = 
	Bcd::Data::Accounting::Accounts->get_balance($stash, Bcd::Common::BasicAccounts::PARKING_TAO_FUND->[0]);
    $self->assert_equals(0, $total-$old_total);


}

sub test_big_deposit{
    my $self = shift;
    $self->_test_make_deposit("26323", "26323", "26192", "118", "13");

}

sub test_small_deposit{
    my $self = shift;
    $self->_test_make_deposit("300", "300", "298", "1", "1");
}

sub test_b_div_rounded{
    my $self = shift;

    my $dividend = new Math::BigInt("1298");
    my $income_tax_test = 2000;
    my $divisor = 222;

    Bcd::Data::Bank::bdiv_int_rounded($dividend, $divisor);
    $self->assert_equals("6", $dividend);

    $dividend = new Math::BigInt("26323");

    Bcd::Data::Bank::bdiv_int_rounded($dividend, $divisor);
    $self->assert_equals("119", $dividend);

    $dividend = new Math::BigInt("4");

    Bcd::Data::Bank::bdiv_int_rounded($dividend, 2);
    $self->assert_equals("2", $dividend);

    $dividend = new Math::BigInt("5");

    Bcd::Data::Bank::bdiv_int_rounded($dividend, 2);
    $self->assert_equals("3", $dividend);

    $dividend = new Math::BigInt("31");

    Bcd::Data::Bank::bdiv_int_rounded($dividend, 2);
    $self->assert_equals("16", $dividend);

    $dividend = new Math::BigInt("-31");

    Bcd::Data::Bank::bdiv_int_rounded($dividend, 2);
    $self->assert_equals("-16", $dividend);

    $dividend = new Math::BigInt("-31");

    Bcd::Data::Bank::bdiv_int_rounded($dividend, -2);
    $self->assert_equals("16", $dividend);

    $dividend = new Math::BigInt("31");

    Bcd::Data::Bank::bdiv_int_rounded($dividend, -2);
    $self->assert_equals("-16", $dividend);

    $dividend = new Math::BigInt("31");
    $divisor  = new Math::BigInt("-2");

    Bcd::Data::Bank::bdiv_int_rounded($dividend, $divisor);
    $self->assert_equals("-16", $dividend);

}


sub test_medium_deposit{
    my $self = shift;
    $self->_test_make_deposit("1298", "1298", "1291", "6", "1");
    $self->_test_make_deposit("300", "1598", "1589", "7", "2");
}

sub test_very_big_deposit{
   my $self = shift;
   $self->_test_make_deposit("3384229", "3384229", "3367308", "15229", "1692");
}

sub test_boundary_case_1{
   my $self = shift;
   $self->_test_make_deposit("1000", "1000", "995", "4", "1");
}

sub test_boundary_case_2{
   my $self = shift;
   $self->_test_make_deposit("1001", "1001", "995", "5", "1");
}

sub test_boundary_case_3{
   my $self = shift;
   $self->_test_make_deposit("3000", "3000", "2984", "14", "2");
}


sub _test_make_deposit{
    my ($self, $amount_to_deposit, $amount_in_cash, $check_deposited, $check_income_treasurer, $check_my_income) = @_;

    my $stash = $self->{stash};


    #I try to make a deposit in the bank
    Bcd::Data::Bank->deposit_user_account
	($stash, 16, 8012898, $amount_to_deposit);

    #ok, now I should see what has happened in the accounts...

    my $user = Bcd::Data::Users->select_user_data($stash, 16);
    my $id_acc_e = $user->{id_account_real};

    #Now I should get the balance of this account
    #my $acc = Bcd::Data::Accounting::AccountsPlan->get_account_with_number($id_acc_e, $stash);
    #my $balance = $acc->get_balance($stash);
    my $balance = Bcd::Data::Accounting::Accounts->get_balance($stash, $id_acc_e);


    $self->assert_equals($check_deposited, $balance);

    #let's see the ant nest cash
    my $ant_nest_cash_e = Bcd::Data::AntNests->get_cash_euro_account_id($stash, 8012898);

    #$acc = Bcd::Data::Accounting::AccountsPlan->get_account_with_number($ant_nest_cash_e, $stash);
    #$balance = $acc->get_balance($stash);
    $balance = Bcd::Data::Accounting::Accounts->get_balance($stash, $ant_nest_cash_e);

    $self->assert_equals($amount_in_cash, $balance);

    #then the partial incomes
    my $incomes_deposit_e = Bcd::Data::AntNests->
	get_euro_deposit_partial_income_id($stash, 8012898);

#    $acc = Bcd::Data::Accounting::AccountsPlan->get_account_with_number($incomes_deposit_e, $stash);
#    $balance = $acc->get_balance($stash);

    $balance = Bcd::Data::Accounting::Accounts->get_balance($stash, $incomes_deposit_e);

    $self->assert_equals($check_my_income, $balance);

    my $treasurer_account_e = Bcd::Data::Users->get_treasurer_euro_account_id($stash, 8012898);

#    $acc = Bcd::Data::Accounting::AccountsPlan->get_account_with_number($treasurer_account_e, $stash);
#    $balance = $acc->get_balance($stash);

    $balance = Bcd::Data::Accounting::Accounts->get_balance($stash, $treasurer_account_e);

    $self->assert_equals($check_income_treasurer, $balance);
    
}

sub _assert_account_id_balance{
    my ($self, $account_id, $balance) = @_;
    my $stash = $self->{stash};

    my $balance_check = Bcd::Data::Accounting::Accounts->get_balance($stash, $account_id);
    $self->assert_equals($balance, $balance_check);

}

sub test_change_euro_in_tao{
    my $self = shift;
    my $stash = $self->{stash};

    my $tao_in_circulation = Bcd::Data::Accounting::Accounts->get_balance
	($stash, Bcd::Common::BasicAccounts::TAO_IN_CIRCULATION->[0]);

    my $tao_in_circulation_f = new Math::BigFloat($tao_in_circulation);

    #I try to make a deposit in the bank
    my ($amount_deposited, $total_euro) = 
	Bcd::Data::Bank->deposit_user_account($stash, 16, 8012898, '5000');

    #ok, then I change some euros in tao
    my $amount_to_change = 2302;
    my ($acc_e, $act_t, $amount_given) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 16, 8012898, $amount_to_change);

    $self->assert_equals($total_euro-$amount_to_change, $acc_e);

    #I make the conversion myself.
    $self->assert_equals("44572.9354", $act_t);
    $self->assert_equals("44572.9354", $amount_given);

    #let's see also the other accounts...
    my $ant_nest_parking_e = Bcd::Data::AntNests->get_parking_euro_account_id
	($stash, 8012898);

    $self->_assert_account_id_balance($ant_nest_parking_e, $amount_to_change);
    
    #the tao in circulation
    $self->_assert_account_id_balance(Bcd::Common::BasicAccounts::TAO_IN_CIRCULATION->[0], 
				      $tao_in_circulation_f +$act_t);

    $amount_to_change = 235;
    ($acc_e, $act_t, $amount_given) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 16, 8012898, $amount_to_change);

    $self->assert_equals("4550.2345", $amount_given);

    $self->_assert_account_id_balance(Bcd::Common::BasicAccounts::TAO_IN_CIRCULATION->[0], 
				      $tao_in_circulation_f + 49123.1699);

    
}

sub test_cheque{
    my $self = shift;
    my $stash = $self->{stash};

    #at first the user cannot emit a cheque, because he doesn't have the money
    my $res = Bcd::Data::Bank->ant_emitting_cheque_fail($stash, 16, 2000);
    $self->assert_equals(1, $res);

    #ok, now let's put some tao in barbara's account, 20c
    my ($acc_e, $act_t) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 16, 8012898, 1);

    #print "You now have Euro $acc_e, tao $act_t\n";

    $res = Bcd::Data::Bank->ant_emitting_cheque_fail($stash, 16, 2000);
    $self->assert_equals(0, $res);

    #ok, now let's try to emit something above the credit limit
    $res = Bcd::Data::Bank->ant_emitting_cheque_fail($stash, 16, 200_000);
    $self->assert_equals(2, $res);

    #something above my account
    $res = Bcd::Data::Bank->ant_emitting_cheque_fail($stash, 16, 40_000);
    $self->assert_equals(1, $res);

}

sub test_get_maximum_cheque_for_ant{
    my $self = shift;
    my $stash = $self->{stash};

    #at first I cannot issue anything
    my $cheque_lim = Bcd::Data::Bank->get_ant_maximum_cheque_amount
	($stash, 16);
    
    $self->assert_equals(0, $cheque_lim);

    #ok, now I put some money in the account
    my ($acc_e, $act_t) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 16, 8012898, 1);

    $cheque_lim = Bcd::Data::Bank->get_ant_maximum_cheque_amount
	($stash, 16);
    
    $self->assert_equals("38338.146", $cheque_lim);

    #ok, now I have as limit the other amount...
    Bcd::Data::CreditsAndDebits->ant_credits_ant($stash, 20, 16, 40000);

    $cheque_lim = Bcd::Data::Bank->get_ant_maximum_cheque_amount
	($stash, 16);
    
    $self->assert_equals("10000", $cheque_lim);
}

sub test_issue_cheque{
    my $self = shift;
    my $stash = $self->{stash};

    #I put some money in the account, 20 eurocent
    my ($acc_e, $act_t) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 16, 8012898, 20);

    my $old_act_t = $act_t;

    #then I issue a cheque
    my ($user_t, $income_t) = Bcd::Data::Bank->ant_emits_cheque
	($stash, 16, 8012898, 2000);

    $self->assert_equals(1, $old_act_t-$user_t);
    $self->assert_equals(1, $income_t);
    
}


sub test_change_tao_euro{
    my $self = shift;
    my $stash = $self->{stash};

    #get the current tao in circulation
    my $tao_in_circulation = Bcd::Data::Accounting::Accounts->get_balance
	($stash, Bcd::Common::BasicAccounts::TAO_IN_CIRCULATION->[0]);

    my $tao_in_circulation_f = new Math::BigFloat($tao_in_circulation);

    #I try to make a deposit in the bank
    my ($amount_deposited, $total_euro) = 
	Bcd::Data::Bank->deposit_user_account($stash, 16, 8012898, '5000');

    #then I have some tao in it
    my $amount_to_change = 2302;
    my ($acc_e, $act_t) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 16, 8012898, $amount_to_change);

    my $acc_e_old = $acc_e;
    my $acc_t_old = $act_t;

    #print "{{{{{{{{{{{{{{{{ you have $acc_t_old tao\n";

    #ok, now I try to convert back some tao in euro
    my $user_credit;
    my $tao_converted;
    ($acc_e, $act_t, $user_credit, $tao_converted) = 
	Bcd::Data::Bank->change_tao_in_euro($stash, 16, 8012898, "44572");

    $self->assert_equals($acc_e_old + $user_credit, $acc_e);
    #print "{{{{{{{{{{{{{{{{ you have converted $tao_converted, in tao you have $act_t\n";
    #print "{{{{{{ acc_t_old = $acc_t_old the accounto now is $act_t\n";
    my $acc_t_old_bf = new Math::BigFloat($acc_t_old);
    my $tao_conv_bf  = new Math::BigFloat($tao_converted);
    $self->assert_equals($acc_t_old_bf - $tao_conv_bf     , $act_t);


    $self->_assert_account_id_balance(Bcd::Common::BasicAccounts::TAO_IN_CIRCULATION->[0], 
				      $tao_in_circulation_f + $acc_t_old_bf - $tao_conv_bf);

    $self->assert_equals($acc_t_old_bf - $tao_conv_bf, $act_t);

    #ok, let's make some checks

    my $part_exch_inc_e    = Bcd::Data::AntNests->get_euro_exchange_partial_income_id
 	($stash, 8012898);

    $self->_assert_account_id_balance($part_exch_inc_e, 1);

    my $ant_nest_parking_e = Bcd::Data::AntNests->get_parking_euro_account_id
	($stash, 8012898);

    $self->_assert_account_id_balance($ant_nest_parking_e, 1);

    #ok, now I have just 19,3627 in the count in Tao, if I change them
    #I will get nothing, all in taxes

    $acc_e_old = $acc_e;
    #print ">>>>>>>>>>>>>>>>>>>>>>>>>>> zeroing the account\n";
    ($acc_e, $act_t, $user_credit, $tao_converted) = 
	Bcd::Data::Bank->change_tao_in_euro($stash, 16, 8012898, "19.3627");

    #there are no more tao in circulation...
    $self->_assert_account_id_balance(Bcd::Common::BasicAccounts::TAO_IN_CIRCULATION->[0], $tao_in_circulation_f);

    #the euro account is not changed...
    $self->assert_equals($acc_e_old, $acc_e);
    $self->assert_equals(0, $user_credit);

    $self->_assert_account_id_balance($part_exch_inc_e, 2);
    $self->_assert_account_id_balance($ant_nest_parking_e, 0);
}
