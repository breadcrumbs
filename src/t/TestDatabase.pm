package t::TestDatabase;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;
use t::TestCommonDb;
use DBI;
use Bcd::Common::BasicAccounts;
use Bcd::Constants::AntNestsConstants;

# this package should simply test the "layout" of the database. If a
# breaking change occur in it it should be pointed out immediately

our @ISA = qw(t::TestCommonDb);

# sub new {
#     my $self = shift()->SUPER::new(@_);

#     return $self;
# }

#this test asserts the number of roles.
sub test_db_roles{

    my $self = shift;

    my $sth = $self->_prepare_a_command(qq{select count(*) from roles});

    $sth->execute();

    my $count_roles;
    $sth->bind_columns(undef, \$count_roles);

    $sth->fetch();
    
    #I should have a certain number of roles
    $self->assert_num_equals($count_roles, 6);

    $sth->finish();
}

sub test_roles{
    my $self = shift;

    my $sth = $self->_prepare_a_command(qq{select * from roles});

    $sth->execute();

    my $id_role;
    my $role;
    $sth->bind_columns(undef, \$id_role, \$role);

    while (defined($sth->fetch())){
	if ($role eq "bc-root"){
	    $self->assert_num_equals($id_role, 32768);
	} elsif ($role eq "guest"){
	    $self->assert_num_equals($id_role, 0);
	} elsif ($role eq "ant"){
	    $self->assert_num_equals($id_role, 1);
	} elsif ($role eq "boss"){
	    $self->assert_num_equals($id_role, 2);
	} elsif ($role eq "treasurer"){
	    $self->assert_num_equals($id_role, 4);
	} elsif ($role eq "founder"){
	    $self->assert_num_equals($id_role, 8);
	} else{
	    $self->fail("unknown role $role");
	}
    }


    $sth->finish();
}

#useless
# sub test_initial_users{
#     my $self = shift;

#     my $sth = $self->_prepare_a_command(qq{select count(*) from users});

#     $sth->execute();

#     my $count_users;
#     $sth->bind_columns(undef, \$count_users);
#     $sth->fetch();

#     $self->assert_num_equals(13, $count_users);

#     $sth->finish();
# }

#this function should test the layout of the accounts table...
sub test_accounts_type{

    #I should have five types of accounts
    my $self = shift;

    my $sth = $self->_prepare_a_command(qq{select count(*) from account_types});

    $sth->execute();

    my $count_types;
    $sth->bind_columns(undef, \$count_types);
    $sth->fetch();

    $self->assert_num_equals($count_types, 5);

    $sth->finish();
}


#this is very important: I test the layout of the database
sub test_accounts_plan{

    my $self = shift;

    #now I test the accounts
    my $initial_index = 0; #this is the initial index of the accounts
    my $sth = $self->_prepare_a_command(qq{select * from accounts_plan where id = ?});


    foreach (Bcd::Common::BasicAccounts::LIST_BASIC_ACCOUNTS){
#	print "id $_->[0] type $_->[1] parent $_->[2]  desc $_->[3]\n";

	$sth->bind_param(1, $initial_index);

	$sth->execute();
	my $id_parent;
	my $description;
	my $type;
	my $id;
	$sth->bind_columns(undef, \$id, \$type, \$id_parent, \$description);
	$sth->fetch();
	$sth->finish();


	$self->assert_num_equals($id, $_->[0]);
	$self->assert_num_equals($type, $_->[1]);
	if ($initial_index < 10){
	    $self->assert_null($_->[2]);
	} else {
	    $self->assert_num_equals($id_parent, $_->[2]);
	}
	$self->assert_equals($description, $_->[3]);

	$initial_index++;
    }

}

#you should have the right state in the db
sub test_ant_nest_states{

    my $self = shift;

    #now I test the accounts
    my $initial_index = 1; #this is the initial index of the accounts
    my $sth = $self->_prepare_a_command(qq{select * from ant_nest_states where id = ?});


    foreach (Bcd::Constants::AntNestsConstants::LIST_ANT_NEST_STATES){

	$sth->bind_param(1, $initial_index);

	$sth->execute();
	my $id;
	my $text;
	$sth->bind_columns(undef, \$id, \$text);
	$sth->fetch();
	$sth->finish();


	$self->assert_num_equals($id, $_->[0]);
	$self->assert_num_equals($text, $_->[1]);
	$initial_index++;
    }
}
