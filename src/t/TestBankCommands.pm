package t::TestBankCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#use Math::BigInt lib => "GMP";


use DBI;
#this is the class I want to test
use t::TestCommonDb;
use Bcd::Data::Bank;
use Bcd::Data::Accounting::AccountsPlan;
use Bcd::Data::StatementsStash;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Model;

our @ISA = qw(t::TestCommonDb);

use Data::Dumper;

sub test_change_tao_euro_command{

    my $self = shift;
    my $stash = $self->{stash};

    #I make a deposit and then I change some euros...
    my ($amount_deposited, $total_euro) = 
	Bcd::Data::Bank->deposit_user_account($stash, 16, 8012898, '5000');

    #then I have some tao in it
    my $amount_to_change = 2598;
    my ($acc_e, $act_t) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 16, 8012898, $amount_to_change);

    $self->assert_equals("50304.2946", $act_t);

    my $command = Bcd::Data::Model->instance()->get_command("bk.change_tao_in_euro");
    $self->assert_not_null($command);

    my $session = $self->_connect_as_a_boss();

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("50304.29460001");
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_WILL_RENDER_NEGATIVE_THE_ACCOUNT, $exit_code);

    $command->{amount} = "31724.2598";
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    
    $self->assert_equals("31716.1026", $res->{tao_converted});
    $self->assert_equals("16.37", $res->{euro_obtained});
    $self->assert_equals("18588.192", $res->{new_tot_tao});
    $self->assert_equals("40.15", $res->{new_tot_euro});

    $self->assert_equals($amount_deposited / 100 - 25.98 + $res->{euro_obtained}, 40.15);
    
}


sub test_change_euro_tao_command{
    my $self = shift;
    my $stash = $self->{stash};

    #I try to make a deposit in the bank
    my ($amount_deposited, $total_euro) = 
	Bcd::Data::Bank->deposit_user_account($stash, 16, 8012898, '5000');

    $self->assert_equals("4976", $total_euro);
    $self->assert_equals("4976", $amount_deposited);

    #ok, now I try to connect as barbara and make the change
    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("bk.change_euro_in_tao");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("50.01");
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_WILL_RENDER_NEGATIVE_THE_ACCOUNT, $exit_code);

    #now I make a withdrawal request...
    $command = Bcd::Data::Model->instance()->get_command("bk.create_withdrawal_booking");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("24.38");
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now I try to change an amount which will render negative the account with the withdrawal
    $command = Bcd::Data::Model->instance()->get_command("bk.change_euro_in_tao");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("26.00");
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_WILL_RENDER_NEGATIVE_THE_ACCOUNT, $exit_code);

    #now I delete my request...
    my $del_command = Bcd::Data::Model->instance()->get_command("bk.delete_my_booking");
    $self->assert_not_null($del_command);

    #ok, now, let's test some useful methods...
    $del_command->start_parsing_command();
    $del_command->parse_line($session);
    $del_command->eot();
    $del_command->exec_only_for_test($stash);
    $exit_code = $del_command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #now the request of change should succeed!
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    
    $self->assert_equals("50343.02", $res->{tao_obtained});
    $self->assert_equals("50343.02", $res->{new_tot_tao});
    my $tot_inhuman_euro = $total_euro - 2600;
    $self->assert_equals(Bcd::Data::RealCurrency::humanize_this_string($tot_inhuman_euro), $res->{new_tot_euro});
    

}
