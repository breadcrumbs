package t::TestConfiguration;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Configuration;
use Bcd::Constants::ConfigurationKeys;

sub test_get_default{
    my $self = shift;
    my $stash = $self->{stash};

    
    my $value = Bcd::Data::Configuration->get_key_default_value
	($stash, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    $self->assert_equals(Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE_AR->[2], $value);

}

sub test_ant_nest_configuration{
    my $self = shift;
    my $stash = $self->{stash};

    my $radix = 12000;
    
    my $code = $self->_create_test_ant_nest($radix);
    
    Bcd::Data::Configuration->insert_ant_nest_value
	($stash, $code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE, '888');

    my $value = Bcd::Data::Configuration->get_ant_nest_value
	($stash, $code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    $self->assert_equals('888', $value);


    #I test an update of the configuration value
    Bcd::Data::Configuration->insert_ant_nest_value
	($stash, $code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE, '333');

    $value = Bcd::Data::Configuration->get_ant_nest_value
	($stash, $code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    $self->assert_equals('333', $value);

    #then I test a default value
    $value = Bcd::Data::Configuration->get_ant_nest_value
	($stash, '10000', Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    $self->assert_equals(Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE_AR->[2], $value);
}
