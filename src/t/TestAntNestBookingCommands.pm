package t::TestAntNestBookingCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;
use DBI;
use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Model;
use Bcd::Data::NewAntNests;
use Bcd::Data::StatementsStash;
use Bcd::Constants::NewAntNestsConstants;
use Data::Dumper;

sub test_new_ant_nest_in_generic_code{
    my $self = shift;
    my $stash = $self->{stash};

    my $command = Bcd::Data::Model->instance()->get_command("an_create_new_founder");
    $self->assert_not_null($command);
    
    $command->parse_line("16100");
    $command->parse_line("Formicaio di Genova generica");
    $command->parse_line("Filippo");
    $command->parse_line('lino@bricioline.it');
    $command->parse_line("1");
    $command->parse_line("password");
    $command->parse_line("."); #null comment

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_CANNOT_CREATE_AN_ANT_NEST_IN_GENERIC_CODE, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    $self->assert_equals("Genova", $res->{error_info});
    
}

#ok, let's try to make simple tests...
sub test_initial_count{
    my $self = shift;
    my $stash = $self->{stash};

    my $command = Bcd::Data::Model->instance()->get_command("na_details");
    $self->assert_not_null($command);
    
    $command->parse_line("10201");
    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_NO_DATA, $exit_code);


}

sub test_not_zero_count{

    my $self = shift;
    my $stash = $self->{stash};

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, "10201", "formicaio di prova");

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, "10201", "lino", 'lino.ferrentino@yahoo.it' ,'password');

    my $command = Bcd::Data::Model->instance()->get_command("na_details");
    $self->assert_not_null($command);
    
    $command->parse_line("10201");
    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());


    $self->assert_equals(1, $res->{founders_count});
    $self->assert_equals(0, $res->{confirmed_founders_count});
    $self->assert_equals(0, $res->{lookers_count});
    $self->assert_equals("formicaio di prova", $res->{proposed_name});
    $self->assert_equals(Bcd::Constants::NewAntNestsConstants::WAITING_FOR_INITIAL_FOUNDERS, $res->{state});
}

sub test_create_new_looker {
    my $self = shift;
    my $stash = $self->{stash};

    my $command = Bcd::Data::Model->instance()->get_command("an_create_new_looker");
    $self->assert_not_null($command);
    
    $command->parse_line("12010");
    $command->parse_line("Formicaio di Cuneo");
    $command->parse_line("Filippo");
    $command->parse_line('lino@bricioline.it');

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, "12010");    

    $self->assert_equals(0, $count);

    $count = Bcd::Data::NewAntNests->get_ant_nest_lookers_count($stash, "12010");    
    $self->assert_equals(1, $count);
}

sub test_create_duplicate_nick{
    my $self = shift;
    my $stash = $self->{stash};

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, "80128", "formicaio di prova");

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, "80128", "elisa", 'lino@localhost' , 'password');

    my $command = Bcd::Data::Model->instance()->get_command("an_create_new_founder");
    $self->assert_not_null($command);
    
    $command->parse_line("80128");
    $command->parse_line("Formicaio di prova");
    $command->parse_line("elisa");
    $command->parse_line('lino@bricioline.it');
    $command->parse_line("1");
    $command->parse_line("password");
    $command->parse_line("."); #null comment

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_USER_ALREADY_EXISTING, $exit_code);
}

sub test_create_new_booking_with_existing_ant_nest{
    my $self = shift;
    my $stash = $self->{stash};

    my $command = Bcd::Data::Model->instance()->get_command("an_create_new_founder");
    $self->assert_not_null($command);
    
    $command->parse_line("80128");
    $command->parse_line("vomero 2");
    $command->parse_line("cippo");
    $command->parse_line('lino@bricioline.it');
    $command->parse_line("1");
    $command->parse_line("password");
    $command->parse_line("."); #null comment

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    #this should go ok, because there is only a test ant nest...
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);


    #now I create a fake ant nest...
    my $id = "1210000"; 
    my $name = "real ant nest";
    
    my $sql = qq{insert into ant_nests(id, state_id, name) values($id, 1, '$name')};
    my $st = $self->{stash}->get_connection()->prepare($sql);
    $st->execute();

    $command->{id_ant_nest} = "12100";
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_NOT_IMPLEMENTED, $exit_code);
}


sub test_create_last_founder{
    my $self = shift;
    my $stash = $self->{stash};

    my $code = 29335;

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "formicaio di prova");

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE - 1){
	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $code, "elisa$_", 'lino@localhost' , 'password');
      }

    my $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, $code);    
    $self->assert_equals(Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE - 1, $count);

    #ok, then I insert the last founder...    
    my $command = Bcd::Data::Model->instance()->get_command("an_create_new_founder");
    $self->assert_not_null($command);
    
    $command->parse_line($code);
    $command->parse_line("Formicaio di Cuneo");
    $command->parse_line("Filippo");
    $command->parse_line('lino@bricioline.it');
    $command->parse_line("1");
    $command->parse_line("password");
    $command->parse_line("."); #null comment

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #The state of this ant nest should be changed
    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_hash($stash, $code);
    $self->assert_equals(Bcd::Constants::NewAntNestsConstants::WAITING_FOR_CHECKING_IN, $ant_nest->{state});

    #ok, now I should not be able to add other founders...
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_NO_MORE_FOUNDERS_FOR_NOW, $exit_code);

}


sub test_create_new_booking_request{
    my $self = shift;
    my $stash = $self->{stash};

    my $command = Bcd::Data::Model->instance()->get_command("an_create_new_founder");
    $self->assert_not_null($command);
    
    $command->parse_line("12010");
    $command->parse_line("Formicaio di Cuneo");
    $command->parse_line("Filippo");
    $command->parse_line('lino@bricioline.it');
    $command->parse_line("1");
    $command->parse_line("password");
    $command->parse_line("."); #null comment

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $token = $res->{token};
    $self->assert_not_null($token);

    my $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, "12010");    

    $self->assert_equals(1, $count);

    $count = Bcd::Data::NewAntNests->get_ant_nest_lookers_count($stash, "12010");    
    $self->assert_equals(0, $count);

    $command = Bcd::Data::Model->instance()->get_command("an_create_new_looker");
    $self->assert_not_null($command);
    
    $command->parse_line("12010");
    $command->parse_line("altro nome");
    $command->parse_line("giuseppe");
    $command->parse_line('lino@bricioline.it');

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, "12010");    

    $self->assert_equals(1, $count);

    $count = Bcd::Data::NewAntNests->get_ant_nest_lookers_count($stash, "12010");    
    $self->assert_equals(1, $count);

    #the ant nest should have not changed name
    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_hash($stash, "12010");
    $self->assert_equals("Formicaio di Cuneo", $ant_nest->{proposed_name});
}

sub test_get_profile_commmand{
    my $self = shift;
    my $stash = $self->{stash};

    #I have an initial booking
    my $code = 12001;

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "formicaio di prova");

    my $token = Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, "elisa", 'lino@localhost' , 'password');

    my $sql = qq{select id_user from ant_nests_founders where id_ant_nest = $code and nick_or_name = 'elisa'};
    my $st = $stash->get_connection->prepare($sql);

    $st->execute();
    my $id = $st->fetchrow_arrayref()->[0];

    #ok, now I confirm this user...
    Bcd::Data::NewAntNests->new_founder_confirmed($stash, $id);

    #ok, then I should connect to the ant nest

    my $session = $self->_connect_to_bcd('elisa', $code, 'password');

    my $command = Bcd::Data::Model->instance()->get_command("us_get_my_profile");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #ok, now let's make some tests
    my $complete_data = $res->{user_details};
    $self->assert(exists($complete_data->{first_name}));
    $self->assert(exists($complete_data->{last_name}));
    $self->assert(exists($complete_data->{home_phone}));
    $self->assert(exists($complete_data->{mobile_phone}));

    $self->assert_null($complete_data->{first_name});
    $self->assert_null($complete_data->{last_name});
    $self->assert_null($complete_data->{home_phone});
    $self->assert_null($complete_data->{mobile_phone});
}

sub test_confirm_founder_command{
    my $self = shift;
    my $stash = $self->{stash};

    #I have an initial booking
    my $code = 12001;

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "formicaio di prova");

    my $token = Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, "elisa", 'lino@localhost' , 'password');


    #ok, let's try to have the command

    my $command = Bcd::Data::Model->instance()->get_command("na_confirm_founder");
    $self->assert_not_null($command);

    $command->parse_line($code);
    $command->parse_line("elisaiii");    
    $command->parse_line("ciocio");

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER, $exit_code);

    $command->{user} = "elisa";

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN, $exit_code);

    $command->{token} = $token;

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #If I repeat the command I should get an error

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);


    #I should be able to login now,
    my ($res, $id) = Bcd::Data::NewAntNests->login($stash, $code, 'elisa', 'password');
    $self->assert_equals(0, $res);

}

sub test_get_summary{

    my $self = shift;
    my $stash = $self->{stash};

    my $command = Bcd::Data::Model->instance()->get_command("na_get_summary");
    $self->assert_not_null($command);
    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());    

    #I am not able to test the output of this command, as the ant
    #nests can be different, but I can test the presence of the fields

    if (scalar(@{$res->{new_ant_nests_summary}}) != 0){
	my $first = $res->{new_ant_nests_summary}->[0];

	$self->assert_not_null($first->{id});
    }

}

sub test_update_founder_personal_data_command{
    my $self = shift;
    my $stash = $self->{stash};

    my $code = 12001;
    my $founder_nick = "giuseppe";

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "formicaio di prova");

    my $token = Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, $founder_nick, 'lino@localhost' , 'password');

    my $sql = qq{select id_user from ant_nests_founders where id_ant_nest = $code and nick_or_name = ?};
    my $st = $stash->get_connection->prepare($sql);
    $st->bind_param(1, $founder_nick);

    $st->execute();
    my $id = $st->fetchrow_arrayref()->[0];

    #I should change the state to this user...
    Bcd::Data::Founders->founder_immitting_data($stash, $id);

    #ok, now I should connect to the ant nest
    my $session = $self->_connect_to_bcd($founder_nick, $code, "password");
    $self->assert_not_null($session);

    #ok, now I get the command
    my %cmd = (
	    first_name => "Albert",
	    last_name => "Einstein",
	    address => "Relative street",
	    home_phone => '88',
	    mobile_phone => '11',
	    sex => "m",
	    birthdate => "1880-12-20",
	    email => 'aeinstein@emc2.com',
	    identity_card_id => 'emc2',
	    );

    #ok, I update the barbara personal data....

    my $command = Bcd::Data::Model->instance()->get_command("na_update_founder_personal_data");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line($cmd{first_name});
    $command->parse_line($cmd{last_name});
    $command->parse_line($cmd{address});
    $command->parse_line($cmd{home_phone});
    $command->parse_line($cmd{mobile_phone});
    $command->parse_line($cmd{sex});
    $command->parse_line($cmd{birthdate});
    $command->parse_line($cmd{email});
    $command->parse_line($cmd{identity_card_id});
    
    $command->eot();


    $command->exec_only_for_test($stash);
    
    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $data = Bcd::Data::Founders->get_founder_complete_data_hash($stash, $id);

    foreach (keys(%cmd)){
	$self->assert_equals($cmd{$_}, $data->{$_});
    }

    $self->assert_equals
	(Bcd::Constants::FoundersConstants::PERSONAL_DATA_IMMITTED, $data->{id_status});

    #ok, a second time the command should fail
    $command->eot();
    $command->exec_only_for_test($stash);
    
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);
}

sub test_get_founders_summary{

    my $self = shift;
    my $stash = $self->{stash};

    my $code = 12001;
    my $founder_nick = "giuseppe";

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "formicaio di prova");

    my $token = Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, $founder_nick, 'lino@localhost' , 'password');

    my $sql = qq{select id_user from ant_nests_founders where id_ant_nest = $code and nick_or_name = ?};
    my $st = $stash->get_connection->prepare($sql);
    $st->bind_param(1, $founder_nick);

    $st->execute();
    my $id = $st->fetchrow_arrayref()->[0];

    #I should change the state to this user...
    Bcd::Data::Founders->founder_immitting_data($stash, $id);

    #ok, now I should connect to the ant nest
    my $session = $self->_connect_to_bcd($founder_nick, $code, "password");
    $self->assert_not_null($session);

    #ok, I can now try to have the summary
    my $command = Bcd::Data::Model->instance()->get_command("na_get_founders_summary");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->eot();

    $command->exec_only_for_test($stash);
    
    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals (1, scalar(@{$res->{founders_summary}}));

    my $founder = $res->{founders_summary}->[0];

    $self->assert_equals($founder_nick, $founder->{nick_or_name});
    $self->assert_equals('lino@localhost', $founder->{email});
    $self->assert_equals(Bcd::Constants::FoundersConstants::WAITING_FOR_PERSONAL_DATA, $founder->{id_status});
    

}

sub get_id_of_founder{
    my ($self, $code, $nick) = @_;
    my $stash = $self->{stash};

    my $sql = qq{select id_user from ant_nests_founders where id_ant_nest = ? and nick_or_name = ?};
    my $st = $stash->get_connection->prepare($sql);
    $st->bind_param(1, $code);
    $st->bind_param(2, $nick);

    $st->execute();
    my $id = $st->fetchrow_arrayref()->[0];
    $st->finish();
    return $id;
}


sub test_get_other_founder_details_command{

    my $self = shift;
    my $stash = $self->{stash};

    my $code = 12001;
    my $nick = "victim_of_curiosity";

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "formicaio di prova");

    my $token = Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, $nick , 'lino@localhost' , 'password');

    my $victim_id = $self->get_id_of_founder($code, $nick);

    $code = 12002;
    $nick = "giuseppe";

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "formicaio di prova");

    #I want to see this users data
    $token = Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, $nick, 'lino@localhost' , 'password');

    my $other_id = $self->get_id_of_founder($code, $nick);

    $nick = "curious";

    #this is the curious user
    $token = Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, $nick, 'lino@localhost' , 'password');

    my $my_id = $self->get_id_of_founder($code, $nick);


    #I should change the state to this user...
    Bcd::Data::Founders->founder_immitting_data($stash, $my_id);


    ##############
    ## now I try to login to the ant nest.

    my $session = $self->_connect_to_bcd('curious', 12002, 'password');
    $self->assert_not_null($session);


    my $command = Bcd::Data::Model->instance()->get_command("na_get_founder_profile");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line(1_393_393_283); #SHOULD BE NOT EXISTING
    $command->eot();

    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    #I change my state
    Bcd::Data::Founders->founder_personal_data_given($stash, $my_id);

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER, $exit_code);

    $command->{id_other_user} = $victim_id;
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST, $exit_code);

    #ok, Now I put the id of the user

    $command->{id_other_user} = $other_id;
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);

    my $details = $res->{founder_details};

    $self->assert_equals("giuseppe", $details->{nick_or_name});
    $self->assert_equals("lino\@localhost", $details->{email});
    $self->assert_equals("password", $details->{password});
}
