package t::TestTrustBot;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;

use Data::Dumper;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Model;

use Bcd::Bots::TrustBot;
use Bcd::Common::CommonConstants;
use Data::Dumper;

sub new{
    my $self = shift()->SUPER::new(@_,1);
    return $self;
}

#IMPORTANT IMPORTANT IMPORTANT....
#IF THE TEST FAILS, MAYBE YOU SHOULD INIT THE CACHE FILE...

sub test_delete_expired_sessions{
    my $self = shift;
    my $stash = $self->{stash};

    my $bot = Bcd::Bots::TrustBot->new();
    $self->assert_not_null($bot);

    #I create the trust net
    my $session = $self->_connect_as_a_boss();
    my ($res, $cycle) = $bot->go_test($stash);

    #I have the users_connected
    my $users_connected = $stash->get_cache()->get
	(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY);

    $self->assert_equals($users_connected->{16}, $session);

    #I force the expiration time of the session
    $stash->get_cache()->remove($session);

    #ok, now I run the bot
    ($res, $cycle) = $bot->go_test($stash);
    $self->assert($res->[0] =~ /expired session: barbara/);

    $users_connected = $stash->get_cache()->get
	(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY);

    #it should be empty
    $self->assert_equals(0, scalar(keys(%{$users_connected})));
}

sub test_create_delete_trust_nets_rule{
    my $self = shift;
    my $stash = $self->{stash};

    my $bot = Bcd::Bots::TrustBot->new();
    $self->assert_not_null($bot);

    #first of all I try to fire the bot, nothing should happen
    my ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(0, scalar(@{$res}));


    #ok, now I connect as a boss
    my $session = $self->_connect_as_a_boss();

    ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(1, scalar(@{$res}));

    #I have added the net for the boss!
    $self->assert_equals("add: 16 " , $res->[0]);
    my $net = Bcd::Data::TrustsNet->get_user_net($stash, 16);
    $self->assert_not_null($net);

    #Ok, then I try to disconnect, just a force
    #$stash->get_cache()->remove($session);
    $self->_disconnect_from_bcd(16);

    ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(1, scalar(@{$res}));

    #I have added the net for the boss!
    $self->assert_equals("del: 16 " , $res->[0]);
    
    $net = Bcd::Data::TrustsNet->get_user_net($stash, 16);
    $self->assert_null($net);
}

sub test_delete_nets{
    my $self = shift;
    my $stash = $self->{stash};
    #please purge the cache.
    #$stash->get_cache()->purge();

    #print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";

    my $bot = Bcd::Bots::TrustBot->new();
    $self->assert_not_null($bot);

    #ok, now I connect as two users
    my $session = $self->_connect_as_a_boss();
    my $ses_tina = $self->_connect_to_bcd("tina", 8012898, "tinap");

    #print "I should add the nets\n";
    my ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(1, scalar(@{$res}));
    $self->assert($res->[0] =~ /^add: 1[6|7] add: 1[6|7] $/);

    my $row = Bcd::Data::TrustsNet->get_trust_data_arr($stash, 16, 18);
    $self->assert_not_null($row);

    #then I disconnect the boss
    #$stash->get_cache()->remove($session);
    $self->_disconnect_from_bcd(16);

    #print "NOW the 16 should be deleted\n";
    ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(1, scalar(@{$res}));
    $self->assert_equals("del: 16 " , $res->[0]);

    #BUT THE net of tina should be there...
    #for example this trust must be in db
    $row = Bcd::Data::TrustsNet->get_trust_data_arr($stash, 16, 17);
    $self->assert_not_null($row);

    #but other rows can be deleted
    $row = Bcd::Data::TrustsNet->get_trust_data_arr($stash, 16, 18);
    $self->assert_null($row);

    #I should not have a net
    $row = Bcd::Data::TrustsNet->get_user_net($stash, 16);
    $self->assert_null($row);

    #ok, now I reconnect 
    $session = $self->_connect_as_a_boss();
    #print "++++++++NOW I reconnect as the boss ..... \n";
    ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(1, scalar(@{$res}));
    $self->assert_equals("add: 16 " , $res->[0]);

    #ok now I lock the net
    my $sql = qq{update trust_nets_index SET is_locked = '1' where id_user = 16};
    $stash->get_connection()->do($sql);

    #now I disconnect
    #$stash->get_cache()->remove($session);
    $self->_disconnect_from_bcd(16);

    #the bot should not remove anything, because the net is blocked...
    #print "++++++NOW I have locked the boss net, so it will not be deleted \n";
    ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(0, scalar(@{$res}));

    #ok, now I disconnect tina
    #$stash->get_cache()->remove($ses_tina);
    $self->_disconnect_from_bcd(17);

    #the bot should remove tina but not barbara
    #print "++++++NOW I have disconnected tina... but barbara is still locked\n";
    ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(1, scalar(@{$res}));
    $self->assert_equals("del: 17 " , $res->[0]);

    #the nets ingoing and outgoing should be there...
    $row = Bcd::Data::TrustsNet->get_trust_data_arr($stash, 16, 18);
    $self->assert_not_null($row);

    $row = Bcd::Data::TrustsNet->get_trust_data_arr($stash, 17, 16);
    $self->assert_not_null($row);
}

#the founder does not add a new net...
sub test_no_founder_net{

    my $self = shift;
    my $stash = $self->{stash};
    #please purge the cache.
    #$stash->get_cache()->purge();

    my $bot = Bcd::Bots::TrustBot->new();
    $self->assert_not_null($bot);

    #I create a "fake" founder...
    $self->{code} = 12393;

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $self->{code}, "test_ant_nest");

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $self->{code}, "john", "john\@mail.com" , 'password');

    my $founder_id = $stash->get_last_founder_id();
    #a state just to allow me to login
    Bcd::Data::Founders->update_founder_state
	($stash, $founder_id, Bcd::Constants::FoundersConstants::VOTED);

    #ok, now I connect myself
    my $session = $self->_connect_to_bcd("john", $self->{code}, 'password');
    $self->assert_not_null($session);

    #ok, then I fire the bot
    my ($res, $cycle) = $bot->go_test($stash);
    #nothing should happen, I am a founder
    #print Dumper ($res);
    $self->assert_equals(0, scalar(@{$res}));
}

sub test_update_trusts_net{
    my $self = shift;
    my $stash = $self->{stash};

    my $bot = Bcd::Bots::TrustBot->new();
    $self->assert_not_null($bot);

    #ok, now I connect as a boss, and I crete the net for the boss
    my $session = $self->_connect_as_a_boss();
    my $session_tine = $self->_connect_to_bcd("tina", 8012898, "tinap");
    my ($res, $cycle) = $bot->go_test($stash);

    #another run should fail...
    ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(0, scalar(@{$res}));

    #ok, now I rollback my trust changes, (nothing to rollback, anyway);
    my $command = Bcd::Data::Model->instance()->get_command("tr.rollback_change_trust");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->eot();
    $command->exec_only_for_test($stash);

    #ok, now another run should update the net for the boss AND for the other ant...
    ($res, $cycle) = $bot->go_test($stash);
    $self->assert_equals(1, scalar(@{$res}));

    $self->assert($res->[0] =~ /Refreshed nets for : 1[6|7] 1[6|7] /);

    #and now the nets should be updated...
    my $net = Bcd::Data::TrustsNet->get_user_net($stash, 16);
    $self->assert_equals(0, $net->[1]);
}
