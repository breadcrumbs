package t::TestCreditsAndDebits;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);


use Data::Dumper;
use Bcd::Data::CreditsAndDebits;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Model;

sub test_ant_freezes_credit{
    my $self = shift;
    my $stash= $self->{stash};

    Bcd::Data::CreditsAndDebits->ant_freezes_credit($stash, 16, 1000);

    my $frozen_credit = Bcd::Data::CreditsAndDebits->get_ant_frozen_credit($stash, 16);
    $self->assert_equals(1000, $frozen_credit);

    Bcd::Data::CreditsAndDebits->ant_freezes_credit($stash, 16, 11);
    $frozen_credit = Bcd::Data::CreditsAndDebits->get_ant_frozen_credit($stash, 16);
    $self->assert_equals(1011, $frozen_credit);
}

sub test_ant_thaws_credit{
    my $self = shift;
    my $stash= $self->{stash};

    #first of all I simulate a pending situation
    Bcd::Data::CreditsAndDebits->ant_credits_ant($stash, 16, 18, 15000);

    #then I simulate a frozen credit
    Bcd::Data::CreditsAndDebits->ant_freezes_credit($stash, 16, 20000);

    #then I have a thaw...
    Bcd::Data::CreditsAndDebits->ant_thaws_credit($stash, 18, 16, 20000);

    #the situation is....
    my $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 16);
    $self->assert_equals(-5000, $net_credit);

    $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 18);
    $self->assert_equals(5000, $net_credit);
    
}

sub test_get_price_report_command{
    my $self = shift;
    my $stash= $self->{stash};

    my $sql = qq{INSERT INTO credits_and_debits VALUES(16, 18, 2000)};
    $stash->prep_exec($sql);

    my $session = $self->_connect_as_a_boss();
    my $command = Bcd::Data::Model->instance()->get_command("cad_get_price_estimate");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line(18);
    $command->parse_line("100000");
    $command->parse_line("1");
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #$self->_test_get_price_estimate_for_a_payment
#	(16, 18, 100_000, 1, 2000, 50_000, 25, 48000, 48_025, 0);

    $self->assert_equals(2000,  $res->{credits_towards_seller});
    $self->assert_equals(50000, $res->{cheque_amount});
    $self->assert_equals(48025, $res->{total_price});
    $self->assert_equals(25,    $res->{cheque_price});
    $self->assert_equals(0,     $res->{can_be_payed_now});
    $self->assert_equals(48000, $res->{convertible_tao});
}

sub test_get_price_report{
    my $self = shift;
    my $stash= $self->{stash};


    #some tests with no funds in the account of the buyer

    #test with no cheque
    $self->_test_get_price_estimate_for_a_payment
	(16, 18, 10000, 0, 0, 0, 0, 10000, 10000, 0);

    #test with all cheque
    $self->_test_get_price_estimate_for_a_payment
	(16, 18, 10000, 1, 0, 10000, 5, 0, 5, 0);

    #partial cheque
    $self->_test_get_price_estimate_for_a_payment
	(16, 18, 100_000, 1, 0, 50_000, 25, 50_000, 50_025, 0);

    #now with some credits
    my $sql = qq{INSERT INTO credits_and_debits VALUES(16, 18, 2000)};
    $stash->prep_exec($sql);

    #partial credit and cash
    $self->_test_get_price_estimate_for_a_payment
	(16, 18, 10000, 0, 2000, 0, 0, 8000, 8000, 0);

    #partial credit and cheque
    $self->_test_get_price_estimate_for_a_payment
	(16, 18, 10000, 1, 2000, 8000, 4, 0, 4, 0);

    #partial credit and partial cheque
    $self->_test_get_price_estimate_for_a_payment
	(16, 18, 100_000, 1, 2000, 50_000, 25, 48000, 48_025, 0);
}

sub _test_get_price_estimate_for_a_payment{
    my ($self, $buyer, $seller, $amount, $can_use_credit, 
	$check_credits_towards_seller, $check_cheque_amount,
	$check_cheque_price, $check_convertible_tao,
	$check_total_price, $check_can_be_payed_now) = @_;

    my $stash= $self->{stash};

    my 
	(
	 $credits_towards_seller, 
	 $cheque_amount, 
	 $cheque_price, 
	 $convertible_tao, 
	 $total_price,
	 $can_be_payed_now) = Bcd::Data::CreditsAndDebits->get_price_estimate_report_for_a_payment
	 ($stash, $buyer, $seller, $amount, $can_use_credit);

    $self->assert_equals($check_credits_towards_seller, $credits_towards_seller);
    $self->assert_equals($check_cheque_amount, $cheque_amount);
    $self->assert_equals($check_cheque_price, $cheque_price);
    $self->assert_equals($check_convertible_tao, $convertible_tao);
    $self->assert_equals($check_total_price, $total_price);
    $self->assert_equals($check_can_be_payed_now, $can_be_payed_now);
}

sub test_credit_report{
    my $self = shift;
    my $stash= $self->{stash};


    my $report = Bcd::Data::CreditsAndDebits->get_credit_reports_towards_ants($stash, 16, 8012898);

    #all the values should be zero.
    my $first = 1;
    foreach(@{$report}){
	if ($first == 1){
	    $first = 0;
	    next;
	}
	$self->assert_equals(0, $_->[1]);
    }

    my $sql = qq{INSERT INTO credits_and_debits VALUES(18, 16, 1001)};
    $stash->prep_exec($sql);

    $report = Bcd::Data::CreditsAndDebits->get_credit_reports_towards_ants($stash, 16, 8012898);

    #ok, I should have a value of -1001, with 18.
    $first = 1;
    foreach(@{$report}){
	if ($first == 1){
	    $first = 0;
	    next;
	}
	$self->assert_equals(0, $_->[1])     if $_->[0] != 18;
	$self->assert_equals(-1001, $_->[2]) if $_->[0] == 18;
    }

}

sub test_credit_report_command{
    my $self = shift;
    my $stash= $self->{stash};

    #I connect as the boss
    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("cad_get_credit_report");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);

    my $sql = qq{INSERT INTO credits_and_debits VALUES(18, 16, 1001)};
    $stash->prep_exec($sql);

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_num_equals(1001, $res->{total_debit});
}

sub test_initial_credits{
    my $self = shift;
    my $stash= $self->{stash};
    
    
    my $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 16);
    $self->assert_num_equals(0, $net_credit);

    #ok, now I put some credits
    
    my $sql = qq{INSERT INTO credits_and_debits VALUES(12, 16, 1001)};
    $stash->prep_exec($sql);

    $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 16);
    $self->assert_num_equals(-1001, $net_credit);

    #a frozen debit
    $sql = qq{INSERT INTO frozen_credits VALUES(16, 999)};
    $stash->prep_exec($sql);

    $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 16);
    $self->assert_num_equals(-2000, $net_credit);

    #a credit
    $sql = qq{INSERT INTO credits_and_debits VALUES(16, 9, 1500)};
    $stash->prep_exec($sql);

    $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 16);
    $self->assert_num_equals(-500, $net_credit);

    my $score = Bcd::Data::CreditsAndDebits->get_ant_credit_score($stash, 16);
    $self->assert_num_equals(100, $score);
}

sub test_credit_score{
    my $self = shift;
    my $stash= $self->{stash};

    
    my $score;
    $score = Bcd::Data::CreditsAndDebits::credit_score(32000, 10000);
    $self->assert_num_equals(100, $score);

    $score = Bcd::Data::CreditsAndDebits::credit_score(32000, 0);
    $self->assert_num_equals(100, $score);

    $score = Bcd::Data::CreditsAndDebits::credit_score(82000, 0);
    $self->assert_num_equals(100, $score);

    $score = Bcd::Data::CreditsAndDebits::credit_score(0, 82000);
    $self->assert_num_equals(-100, $score);

    $score = Bcd::Data::CreditsAndDebits::credit_score(1, 82000);
    $self->assert_num_equals("-99.7776809245822", $score);

    $score = Bcd::Data::CreditsAndDebits::credit_score(82000, 1);
    $self->assert_num_equals("99.7776809245822", $score);

    $score = Bcd::Data::CreditsAndDebits::credit_score(82000, 82000);
    $self->assert_num_equals("0", $score);

    $score = Bcd::Data::CreditsAndDebits::credit_score(23000, 82000);
    $self->assert_num_equals("-59.6048313960983", $score);
}

sub test_credit_score_from_db{

    my $self = shift;
    my $stash= $self->{stash};

    my $sql = qq{INSERT INTO credits_and_debits VALUES(12, 16, 82000)};
    $stash->prep_exec($sql);

    $sql = qq{INSERT INTO credits_and_debits VALUES(16, 15, 1)};
    $stash->prep_exec($sql);

    my $score = Bcd::Data::CreditsAndDebits->get_ant_credit_score($stash, 16);
    $self->assert_num_equals("-99.7776809245822", $score);
}

sub test_ant_credit_ant{

    my $self = shift;
    my $stash= $self->{stash};

    #barbara issues a cheque, so she will have a debit
    Bcd::Data::CreditsAndDebits->ant_credits_ant($stash, 20, 16, 20000);

    my $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 16);
    $self->assert_num_equals(-20000, $net_credit);

    my $res;
    ($res,$net_credit)  = Bcd::Data::CreditsAndDebits->get_net_position_between_two_ants($stash, 16, 20);
    $self->assert_num_equals(-20000, $net_credit);

    ($res, $net_credit)  = Bcd::Data::CreditsAndDebits->get_net_position_between_two_ants($stash, 20, 16);
    $self->assert_num_equals(20000, $net_credit);


    #ok, now barbara emits another cheque
    Bcd::Data::CreditsAndDebits->ant_credits_ant($stash, 20, 16, 7000);
    $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 16);
    $self->assert_num_equals(-27000, $net_credit);


    #now paola returns some cheques to barbara
    Bcd::Data::CreditsAndDebits->ant_credits_ant($stash, 16, 20, 13000);
    $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 16);
    $self->assert_num_equals(-14000, $net_credit);


    #another cheque, now paola becomes a debitor
    Bcd::Data::CreditsAndDebits->ant_credits_ant($stash, 16, 20, 15000);
    $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 16);
    $self->assert_num_equals(1000, $net_credit);

    $net_credit = Bcd::Data::CreditsAndDebits->get_net_ant_credit($stash, 20);
    $self->assert_num_equals(-1000, $net_credit);

    ($res, $net_credit)  = Bcd::Data::CreditsAndDebits->get_net_position_between_two_ants($stash, 20, 16);
    $self->assert_num_equals(-1000, $net_credit);
}

#this is the same in the bank
use constant R_MAX => 1.22;

sub test_get_maximum_cheque{
    my $self = shift;
    my $stash= $self->{stash};

    my $max_cheque;

    $max_cheque = Bcd::Data::CreditsAndDebits->get_maximum_debt_to_reach_this_rate
	($stash, 16, R_MAX);

    $self->assert_equals(Bcd::Data::CreditsAndDebits::MINIMUM_DEBIT_CREDIT, $max_cheque);

    #ok, now I have some credit
    Bcd::Data::CreditsAndDebits->ant_credits_ant($stash, 16, 20, 15000);

    $max_cheque = Bcd::Data::CreditsAndDebits->get_maximum_debt_to_reach_this_rate
	($stash, 16, R_MAX);
    $self->assert_equals(Bcd::Data::CreditsAndDebits::MINIMUM_DEBIT_CREDIT, $max_cheque);

    #some more credit
    Bcd::Data::CreditsAndDebits->ant_credits_ant($stash, 16, 20, 27000);

    $max_cheque = Bcd::Data::CreditsAndDebits->get_maximum_debt_to_reach_this_rate
	($stash, 16, R_MAX);
    $self->assert_equals(51240, $max_cheque);

    #some debit
    Bcd::Data::CreditsAndDebits->ant_credits_ant($stash, 19, 16, 17000);

    #some frozen debit
    my $sql = qq{INSERT INTO frozen_credits VALUES(16, 10000)};
    $stash->prep_exec($sql);

    $max_cheque = Bcd::Data::CreditsAndDebits->get_maximum_debt_to_reach_this_rate
	($stash, 16, R_MAX);
    $self->assert_equals(24240, $max_cheque);
}
