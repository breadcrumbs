package t::TestFoundersTrustsCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;
use DBI;
use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::NewAntNests;
use Bcd::Data::Model;
use Bcd::Data::AntNests;
use Bcd::Data::StatementsStash;
use Bcd::Data::FoundersTrusts;
use Data::Dumper;

sub test_add_founders_trusts{
    my $self = shift;
    my $stash = $self->{stash};

        my $code = 12393;

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "test_ant_nest");

    my @founder_ids;

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){

	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $code, "john$_", 'john@mail.com' , 'password');

	  push(@founder_ids, $stash->get_last_founder_id());
      }


    #ok, Now I have a test ant nest...
    #now I have a test matrix
    #the numbers make the test simpler to check
    my $trusts = 
	[
	 [0.12, 0.13, 0.14, 0.15],
	 [0.21, 0.23, 0.24, 0.25],
	 [0.31, 0.32, 0.34, 0.35],
	 [0.41, 0.42, 0.43, 0.45],
	 [0.51, 0.52, 0.53, 0.54],
	 ];

    #I should confirm the ant
    Bcd::Data::Founders->founder_immitting_data($stash, $founder_ids[0]);

    #then let's connect as the first user
    my $session = $self->_connect_to_bcd("john1", $code, "password");

    #ok, now I get the command from the factory.

    my $command = Bcd::Data::Model->instance()->get_command("na_create_founder_trusts");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line(1_393_393_283); #SHOULD BE NOT EXISTING
    $command->eot();

    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    #ok, then I put myself in the right state
    Bcd::Data::Founders->founder_waiting_for_trust($stash, $founder_ids[0]);

    $command->eot();

    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_NUMBER_OF_PARAMETERS, $exit_code);

    #OK, then I put the parameters ok
    $command->start_parsing_command();
    $command->parse_line($session);

    my $my_trusts = $trusts->[0];

    my $i = 1;
    for(@{$my_trusts}){

	$command->parse_line($founder_ids[$i]);
	$command->parse_line($_);

	$i++;
    }


    $command->eot();
    $command->exec_only_for_test($stash);

    #print Dumper($command);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #another execution should be forbidden
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    #then I should control that the founder has the right trusts.
    my $sql = qq{SELECT trust_u1_u2, trust_u2_u1 FROM ant_nests_founders_trusts WHERE u1 = ? AND u2 = ?};
    my $st = $stash->prepare_cached($sql);

    $i = 0;
    my $j = 1;
    while ($j < 5){

	#print "Selecting trust from $_ and $j\n";
	
	#ok, I take the trust
	$st->bind_param(1, $founder_ids[$i]);
	$st->bind_param(2, $founder_ids[$j]);

	$st->execute();
	
	my $row = $st->fetchrow_arrayref();

	$st->finish();

	#print Dumper($row);
	my $temp_i = $i+1;
	my $temp_j = $j+1;
	$self->assert_equals("0.$temp_i$temp_j", $row->[0]);
	$self->assert_null($row->[1]);
	$j++;
    }
}


1;
