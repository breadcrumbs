package t::TestActivities;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Activities;
use Data::Dumper;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Model;

sub test_get_all_parents{
    my $self = shift;
    my $stash= $self->{stash};

    my $list = Bcd::Data::Activities->get_all_parents($stash, 16);

    $self->assert_equals(4, scalar(@{$list}));
    
    $self->assert_equals('Servizi',                          $list->[0]);
    $self->assert_equals('Casa e famiglia',                  $list->[1]);
    $self->assert_equals('Bambini',                          $list->[2]);
    $self->assert_equals('Baby sitter per bambini 0-3 anni', $list->[3]);

    my $str = Bcd::Data::Activities->get_all_parents_str($stash, 16);
    $self->assert_equals
	('Servizi > Casa e famiglia > Bambini > Baby sitter per bambini 0-3 anni', $str);

    $list = Bcd::Data::Activities->get_all_parents($stash, 1);
    $self->assert_equals(1, scalar(@{$list}));
    $self->assert_equals("Artigianato", $list->[0]);
}

sub test_get_services_activities{
    my $self = shift;
    my $stash= $self->{stash};

    my $tree = Bcd::Data::Activities->get_service_activities($stash);
    #print Dumper($tree);

    my $sql = qq{SELECT description from activities where id = ?};
    my $st = $stash->get_connection()->prepare($sql);

    $self->_check_tree($tree, $st);
}

sub test_get_tree_from_command{
    my $self = shift;
    my $stash = $self->{stash};

    my $command = Bcd::Data::Model->instance()->get_command("ac.get_tree");
    $self->assert_not_null($command);
    $command->parse_line("0"); #this is the services tree
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    my $tree = $res->{act_tree};

    my $sql = qq{SELECT description from activities where id = ?};
    my $st = $stash->get_connection()->prepare($sql);
    $self->_check_tree($tree, $st);
}

sub test_get_summary_tree{
    my $self = shift;
    my $stash= $self->{stash};

    my $tree = Bcd::Data::Activities->build_activity_tree_summary_from
	($stash, 
	 Bcd::Constants::ActivitiesConstants::PARENT_SERVICES, 8012898);

    #ok... I should have all nulls... because I don't have ads, for now.
    $self->_check_tree_with_nulls($tree);

    #let's add a fake ad
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "a presence");

    #ok, Now I should be able to create an ad
    my $ad_id1 = Bcd::Data::Ads->create_new_ad
	($stash, 12, 16, "first ad", $id_presence, 90, 100, 0.1, 0.2);

    my $count = Bcd::Data::Ads->get_count_activity_post_code_ads
	($stash, 12, 8012898);

    $self->assert_equals(1, $count);

    $tree = Bcd::Data::Activities->build_activity_tree_summary_from
	($stash, 
	 Bcd::Constants::ActivitiesConstants::PARENT_SERVICES, 8012898);

    $count = Bcd::Data::Ads->get_count_activity_post_code_ads
	($stash, 12, 8012898);

    $self->assert_equals(1, $count);

    #this is hard coded, if you change the activity this test will surely
    #fail, but it is not so important
    $self->assert_equals(1, $tree->[1]->[1]->[0]->[1]->[1]->[2]);

    #now I try also the command
    #I must login
    my $session = $self->_connect_to_bcd("sonia", 8012898, "soniap");

    my $command = Bcd::Data::Model->instance()->get_command("ac.get_summary_tree");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("0"); #this is the services tree
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    $tree = $res->{act_summary_tree};

    #also this assert could fail, if you change the activity layout.
    $self->assert_equals(1, $tree->[1]->[1]->[0]->[1]->[1]->[2]);
}

sub _check_tree_with_nulls{
    my ($self, $tree) = @_;
    
    foreach(@{$tree}){
	if (ref($_->[1]) eq "ARRAY"){
	    #ok, let's recurse
	    $self->_check_tree_with_nulls($_->[1]);
	} else {
	    #ok, it is a scalar
	    #I should have zero ads.
	    $self->assert_equals(0, $_->[2]);
	}
    }
}

#This is a recursive function that checks the layout of the tree
sub _check_tree{
    my ($self, $tree, $st) = @_;
    
    foreach(@{$tree}){
	if (ref($_->[1]) eq "ARRAY"){
	    #ok, let's recurse
	    $self->_check_tree($_->[1], $st);
	} else {
	    #ok, it is a scalar
	    $st->bind_param(1, $_->[1]);
	    $st->execute();
	    my $row = $st->fetch();
	    $st->finish();
	    $self->assert_equals($_->[0], $row->[0]);
	}
    }

}
