package t::TestAccountsPlan;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use DBI;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::StatementsStash;
use Bcd::Data::Accounting::AccountsPlan;


# sub new {
#     my $self = shift()->SUPER::new(@_);

#     #$self->{"accounts_plan"} = accounting::AccountsPlan->instance();
#     # my $stash = data::StatementsStash->new($self->{"conn"});
# #     $self->{"stash"} = $stash;

#     my $sql = qq { select * from master_detail_accounts where id_parent_account = ? 
# 		       and id_child_account = ?};

#     my $sth_childs = $self->{"conn"}->prepare($sql);

#     $self->{"get_childs"} = $sth_childs;

#     return $self;
# }

sub test_create_ant_nests_accounts{
    my $self = shift;

    my $stash = $self->{"stash"};

    my $test_name = "test ant nest";

    Bcd::Data::Accounting::AccountsPlan->create_ant_nest_accounts($test_name, $stash);

    #ok, now I should test the creation of these accounts.

    my $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_CASH, $test_name);
    $self->check_existence_of_this_account($name);

    $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_INCOME_E, $test_name);
    $self->check_existence_of_this_account($name);

    $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_PAR_INC_E, $test_name);
    $self->check_existence_of_this_account($name);

    $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_PAR_INC_T, $test_name);
    $self->check_existence_of_this_account($name);

    $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_USER_ACC_E, $test_name);
    $self->check_existence_of_this_account($name);

    $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_USER_ACC_T, $test_name);
    $self->check_existence_of_this_account($name);

    $name = sprintf(Bcd::Common::BasicAccounts::PARKING_EURO_FUND_RAD, $test_name);
    $self->check_existence_of_this_account($name);

    $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_ACC_T, $test_name);
    $self->check_existence_of_this_account($name);

    $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_ACC_E, $test_name);
    $self->check_existence_of_this_account($name);

    #THEN THERE ARE THE INCOMES ACCOUNTS.




}

sub check_existence_of_this_account{
    my ($self, $name) = @_;
    my $stash = $self->{"stash"};

    #print ">>>>>>>>>>>. check_existence_of_this_account name --$name--\n";

    my $test_id = 
	Bcd::Data::Accounting::AccountsPlan->get_id_account_from_name($name, $stash);
    
    my $account = Bcd::Data::Accounting::AccountsPlan->select_account_data($stash, $test_id);

    $self->assert_equals($name, $account->[3]);
}


sub test_create_normal_child{

    my $self = shift;
    my $stash = $self->{"stash"};
    my $id_child = Bcd::Data::Accounting::AccountsPlan->create_a_normal_account(0, 'TEST_Cash', $stash);

    #now I should be able to get the child account
    #my $child = Bcd::Data::Accounting::AccountsPlan->get_account_with_number($id_child, $stash);

    my $child = Bcd::Data::Accounting::AccountsPlan->select_account_data($stash, $id_child);

    $self->assert_num_equals(0, $child->[1]);
    $self->assert_equals(0,     $child->[2]);
    $self->assert_equals('TEST_Cash', $child->[3]);


    $self->{stash}->get_connection()->rollback();
    
}

sub test_get_account_type{
    my $self = shift;
    my $stash = $self->{"stash"};

		
    my ($is_in_real_currency, $is_positive);

    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 0);

    $self->assert_equals(1, $is_in_real_currency);
    $self->assert_equals(1, $is_positive);

    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 1);

    $self->assert_equals(0, $is_in_real_currency);
    $self->assert_equals(1, $is_positive);

    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 2);

    $self->assert_equals(1, $is_in_real_currency);
    $self->assert_equals(0, $is_positive);

    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 3);

    $self->assert_equals(0, $is_in_real_currency);
    $self->assert_equals(0, $is_positive);

    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 4);

    $self->assert_equals(1, $is_in_real_currency);
    $self->assert_equals(0, $is_positive);

    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 5);

    $self->assert_equals(0, $is_in_real_currency);
    $self->assert_equals(0, $is_positive);


    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 6);

    $self->assert_equals(1, $is_in_real_currency);
    $self->assert_equals(1, $is_positive);

    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 7);

    $self->assert_equals(0, $is_in_real_currency);
    $self->assert_equals(1, $is_positive);

    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 8);

    $self->assert_equals(1, $is_in_real_currency);
    $self->assert_equals(0, $is_positive);

    ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, 9);

    $self->assert_equals(0, $is_in_real_currency);
    $self->assert_equals(0, $is_positive);
}
