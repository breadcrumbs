package t::TestTrust;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Users;
use Bcd::Constants::Users;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Trust;
use Bcd::Data::TrustsNet;
use Bcd::Data::StatementsStash;
use Bcd::Data::Model;

use Data::Dumper;

my @test_users = (
		  {
		      #0
		      nick => 'zero_user',
		      id_ant_nest => 1613499,
		      bias => 1.01,
		      alfa => 0.93
		      },
		  {
		      #1
		      nick => 'lino',
		      bias => 1.01,
		      alfa => 0.93
		      },
		  {
		      #2
		      nick => 'eli',
		      bias => 0.99,
		      alfa => 1.22
		      },
		  {
		      #3
		      nick => 'enzo',
		      bias => 1.12,
		      alfa => 0.98
		      },
		  {
		      #4
		      nick => 'giuseppe',
		      bias => 0.52,
		      alfa => 0.78,
		  },
		  {
		      #5
		      nick => 'andrea',
		      bias => 2.1,
		      alfa => 1.22
		      },
		  {
		      #6
		      nick => 'tizi',
		      bias => 1.92,
		      alfa => 0.77
		      },
		  {
		      #7
		      nick => 'monica',
		      bias => 0.83,
		      alfa => 1.12,
		  },
		  {
		      #8
		      nick => 'carla',
		      bias => 1.02,
		      alfa => 0.44,
		  },
		  {
		      #9
		      nick => 'benedetto',
		      id_ant_nest => 1613499,
		      bias => 1.14,
		      alfa => 0.88,
		  },
		  );

my @test_trusts = (
		   [1, 2, 0.98, 0.97],
		   [1, 3, 0.77, 0.80],
		   [2, 3, 0.93, 0.94],
		   [1, 5, 0.99, 0.60],
		   [5, 6, 0.99, 0.99],
		   [6, 7, 0.97, 0.96],
		   [7, 8, 0.43, 0.61],
		   [1, 9, 0.97, 0.67],
		   [3, 4, 0.89, 0.99],
		   [2, 4, 0.32, 0.94],
		   [9, 0, 0.90, 0.09],
		   );

my @user_ids = ();

#this function should set up the "fake" users in this ant nest
sub set_up{
    my $self = shift;

    #first of all I create the test ant nest
    $self->_create_test_ant_nest(80128);
    $self->_create_test_ant_nest(16134);

    my $personal_data_id = Bcd::Data::Users->create_personal_data_for_user($self->{stash}, {});

    @user_ids = ();

    for (@test_users){
	my $id = $self->_create_a_test_user_for_trust($_, $personal_data_id);
	push(@user_ids, $id);
    }

    #ok, now I should make the trusts
    for (@test_trusts){

	Bcd::Data::Trust->create_trust_between
	    ($self->{stash}, 
	     $user_ids[$_->[0]], 
	     $user_ids[$_->[1]], 
	     $_->[2], 
	     $_->[3]);
    }

    
}

#this method simply creates a user, just to test
#returns the id of this user.
sub _create_a_test_user_for_trust{
    my ($self, $user, $personal_data_id) = @_;

    my $ant_nest = exists($user->{id_ant_nest}) ? $user->{id_ant_nest} : 8012899;

    #the test db should be changed
    my $sql = qq{insert into users(nick, id_ant_nest, 
				   users_state, alfa, bias, id_personal_data) values (?, $ant_nest, 4, ?, ?, ?)};
    
    my $st = $self->{stash}->get_connection()->prepare($sql);

    $st->bind_param(1, $user->{nick});
    $st->bind_param(2, $user->{alfa});
    $st->bind_param(3, $user->{bias});
    $st->bind_param(4, $personal_data_id);
    $st->execute();

    my $id_of_insertion = $self->{stash}->get_id_of_last_user();

    return $id_of_insertion;
}

#this is the only test which uses this fixture, so I create the fixture only once
#but it is shared by all the test inside this file
sub test_trust{
    my $self = shift;

    $self->_test_is_valid_trust();
    $self->_test_is_valid_trust_bb();
    $self->_test_dec_from_bBel();
    $self->_test_exists_direct_trust_between();
    $self->_test_simple_path();
    $self->_test_composite_paths();
    $self->_test_simple_trusts();
    $self->_test_composite_trusts();
    #$self->_test_no_path_trust();
    $self->_test_reachables_no_limit();
    $self->_test_get_maximum_reachable_set();
    $self->_test_reachables_with_limit();
    
    $self->_test_get_trust();

    $self->_test_incoming_net();
    $self->_test_outgoing_net();

    $self->_test_create_trust_net();

    $self->_test_get_user_net_command();

    #these tests MUST be run in sequence.
    $self->_test_new_booking();
    $self->_test_create_new_trust();
    $self->_test_change_trust();
    $self->_test_rollback_change_trust();
    $self->_test_commit_trust_changes();
    $self->_test_get_backupped_trusts();
}

#this test is very bad... I force a login, just to have a session...
sub _test_get_user_net_command{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    #I force a session
    #just a random string
    my $session_id = "uoiuouoiueoiruweoiruwhg";
    my %test_session;
    $test_session{user_connected} = "andrea";
    $test_session{user_role} = Bcd::Data::Users::ANT_ROLE;
    $test_session{ant_nest}  = 8012899;
    $test_session{user_id}   = $user_ids[5];

    $stash->get_cache()->set($session_id, \%test_session);

    #ok, I try to issue a command
     my $command = Bcd::Data::Model->instance()->get_command("tr.get_user_nets");
    $self->assert_not_null($command);
    $command->parse_line($session_id);
    $command->parse_line(0);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now I get the net from the parser
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $cmd_net = $res->{net};


    $self->assert_str_equals("giuseppe", $cmd_net->{$user_ids[4]}->[0]);
    #the trusts are converted in bricioBel.
    $self->assert_str_equals("49.21", $cmd_net->{$user_ids[4]}->[1]);
    #$self->assert_str_equals("78.87", $cmd_net->{$user_ids[4]}->[2]);
    $self->assert_str_equals(0, $cmd_net->{$user_ids[4]}->[2]);
}

sub _test_create_trust_net{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    #this function simply tests that the net is stored correctly in the db
    my $user_net = Bcd::Data::TrustsNet->create_nets_for_user($stash, $user_ids[5]);

    #the user net should also be in cache...
    my $cached_net = $stash->get_cache()->get("net_$user_ids[5]");
    $self->assert_not_null($cached_net);

    $cached_net = $cached_net->{net};

    #print Dumper($user_net);

    #ok, then I see if the nets are stored in db
    my $sql = qq{ SELECT * from trusts_cache where u1 = ? and un = ? };
    my $st = $stash->get_connection()->prepare($sql);

    $st->bind_param(1, $user_ids[5]);
    $st->bind_param(2, $user_ids[4]);

    $st->execute();
    my $arr = $st->fetch();

    $self->assert_str_equals("8.33853676860981e-06", $arr->[3]);

    #the two nets should be equals
    $self->assert_str_equals("8.33853676860981e-06", $user_net->{$user_ids[4]}->[0]);
    $self->assert_str_equals("8.33853676860981e-06", $cached_net->{$user_ids[4]}->[0]);

    $self->assert_str_equals("$user_ids[5] | $user_ids[1] | $user_ids[3] | $user_ids[4]", $arr->[4]);

    #then an incoming trust.
    $st->bind_param(1, $user_ids[4]);
    $st->bind_param(2, $user_ids[5]);

    $st->execute();
    $arr = $st->fetch();

    $self->assert_str_equals("0.00770656129838086", $arr->[3]);

    $self->assert_str_equals("0.00770656129838086", $user_net->{$user_ids[4]}->[1]);
    $self->assert_str_equals("0.00770656129838086", $cached_net->{$user_ids[4]}->[1]);

    $self->assert_str_equals("$user_ids[4] | $user_ids[2] | $user_ids[1] | $user_ids[5]", $arr->[4]);

    #now I change artificially a trust... the net should be updated...
    Bcd::Data::Trust->change_trust($stash, $user_ids[5], $user_ids[1], 0.08);

    $user_net = Bcd::Data::TrustsNet->update_nets_for_user($stash, $user_ids[5]);

    $st->bind_param(1, $user_ids[5]);
    $st->bind_param(2, $user_ids[1]);

    $st->execute();
    $arr = $st->fetch();

    $self->assert_str_equals("0.0976", $user_net->{$user_ids[1]}->[0]);    
    $self->assert_str_equals("0.0976", $arr->[3]);

    #ok, I revert to the original trust
    Bcd::Data::Trust->change_trust($stash, $user_ids[5], $user_ids[1], 0.60);

    $user_net = Bcd::Data::TrustsNet->update_nets_for_user($stash, $user_ids[5]);
    $self->assert_str_equals("0.732", $user_net->{$user_ids[1]}->[0]);    
}

sub test_outdate_trust_net{
    my $self = shift;
    my $stash = $self->{stash};


    my $user_net = Bcd::Data::TrustsNet->create_nets_for_user($stash, $user_ids[5]);
    $user_net = Bcd::Data::TrustsNet->create_nets_for_user($stash, $user_ids[4]);
    $user_net = Bcd::Data::TrustsNet->create_nets_for_user($stash, $user_ids[9]);

    #I outdate the nets for user 5
    Bcd::Data::TrustsNet->outdate_net_for_ant_nest($stash, 8012899);

    #ok, now the nets for 5 and 4 should be outdated
    my $row = Bcd::Data::TrustsNet->get_user_net($stash, $user_ids[4]);
    $self->assert_equals(1, $row->[1]);

    $row = Bcd::Data::TrustsNet->get_user_net($stash, $user_ids[5]);
    $self->assert_equals(1, $row->[1]);

    #but not the net for user 9
    $row = Bcd::Data::TrustsNet->get_user_net($stash, $user_ids[9]);
    $self->assert_equals(0, $row->[1]);

}

sub _test_outgoing_net{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    #print "_________________________ test outgoing net\n";
    my $outgoing_net;
    $outgoing_net = Bcd::Data::Trust->get_outgoing_trust_net($stash, $user_ids[5]);
    $self->assert_equals(7, scalar(keys(%{$outgoing_net})));
    #print Dumper($outgoing_net);

    my $trust_4 = $outgoing_net->{$user_ids[4]};
    $self->assert_str_equals("8.33853676860981e-06", $trust_4->[2]);

    my $trust_8 = $outgoing_net->{$user_ids[8]};
    $self->assert_str_equals("0.000245666165323859", $trust_8->[2]);

    #the outgoing and the ingoing should be specular.
    foreach (keys(%{$outgoing_net})){
	#I compute the ingoing for this user...
	my $particular_incoming_net = Bcd::Data::Trust->get_incoming_trust_net($stash, $_);

	#the two trusts should be equal:
	my $trust_outgoing = $outgoing_net->{$_}->[2];
	my $trust_ingoing  = $particular_incoming_net->{$user_ids[5]}->[2];
	$self->assert_str_equals($trust_ingoing, $trust_outgoing);
    }


}


sub _test_incoming_net{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    #print "_________________________ test incoming_net\n";
    #print Dumper (\@user_ids);
    my $incoming_net;
    $incoming_net = Bcd::Data::Trust->get_incoming_trust_net($stash, $user_ids[5]);
    $self->assert_equals(7, scalar(keys(%{$incoming_net})));

    #print "||||||||||||||||||||||||||||||||||||||||||||||||||| end count\n";
    #print "the net is\n";
    #print Dumper($incoming_net);

    #ok, I take the path from the most distant people, just to test...
    my $trust_4 = $incoming_net->{$user_ids[4]};

    #print Dumper($trust_4);
    $self->assert_str_equals("0.00770656129838086", $trust_4->[2]);

    my $trust_8 = $incoming_net->{$user_ids[8]};
    $self->assert_str_equals("0.00688167479645286", $trust_8->[2]);

}

sub _test_get_trust{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    my ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, $user_ids[1], $user_ids[5]);

    $self->assert_equals("0.99", $trust);
    $self->assert_equals("0", $am_i_second);

    ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, $user_ids[5], $user_ids[1]);

    $self->assert_equals("0.60", $trust);
    $self->assert_equals("1", $am_i_second);

}

sub _test_new_booking{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    Bcd::Data::Trust->create_new_trust_booking($stash, 5, 7, 0.43);

    my $sql = qq{select * from trusts_bookings where u1 = ? and u2 = ?};

    my $st = $stash->get_connection()->prepare($sql);

    $st->bind_param(1, 5);
    $st->bind_param(2, 7);

    $st->execute();
    
    my $ref = $st->fetchrow_arrayref();
    $self->assert_equals("0.43", $ref->[2]);

}

sub _test_create_new_trust{

    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    #this is useless, because in this module the tests are run in sequence...
    #and so this row has been already put by the preceding test
    #Bcd::Data::Trust->create_new_trust_booking($stash, 5, 7, 0.43);


    #ok, now let's create the inverse trust
    Bcd::Data::Trust->create_new_trust($stash, 7, 5, 0.98);

    #there should be a new row in the trust table
    my $sql = qq{select * from trusts where u1=? and u2 = ?};
    my $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, 5);
    $st->bind_param(2, 7);
    
    $st->execute();
    my $row = $st->fetchrow_arrayref();


    $self->assert_equals("0.43", $row->[2]);
    $self->assert_equals("0.98", $row->[3]);
    $st->finish();

    #then I should assert myself that the booking has been deleted
    $sql = qq{select * from trusts_bookings where u1=? and u2 = ?};
    $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, 5);
    $st->bind_param(2, 7);

    $st->execute();
    $row = $st->fetchrow_arrayref();


    $self->assert_null($row); #nothing.
    $st->finish();
}

sub _test_change_trust{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    #ok, now let's try to change the trust of the first user
    Bcd::Data::Trust->change_trust($stash, 5, 7, 0.66);


    #I should have a row in the trusts_backups table
    my $sql = qq{SELECT * from trusts_backups where u1=? AND u2=?};
    my $st_select_backup = $stash->get_connection()->prepare($sql);

    $st_select_backup->bind_param(1, 5);
    $st_select_backup->bind_param(2, 7);

    $st_select_backup->execute();
    my $row = $st_select_backup->fetchrow_arrayref();

    #I should have the backup...
    $self->assert_equals("0.43", $row->[2]);

    #AND i have the trust changed in the trust table
    $sql = qq{SELECT * from trusts where u1=? AND u2=?};
    my $st_select_trust = $stash->get_connection()->prepare($sql);
    
    $st_select_trust->bind_param(1, 5);
    $st_select_trust->bind_param(2, 7);

    $st_select_trust->execute();
    $row = $st_select_trust->fetchrow_arrayref();

    #I should have the new trust
    $self->assert_equals("0.66", $row->[2]);

    #####################################################

    #let's try to change the trust once again
    Bcd::Data::Trust->change_trust($stash, 5, 7, 0.44);

    $st_select_backup->bind_param(1, 5);
    $st_select_backup->bind_param(2, 7);

    $st_select_backup->execute();
    $row = $st_select_backup->fetchrow_arrayref();

    #I should have the backup, unchanged
    $self->assert_equals("0.43", $row->[2]);

    #AND i have the trust changed in the trust table
    $st_select_trust->bind_param(1, 5);
    $st_select_trust->bind_param(2, 7);

    $st_select_trust->execute();
    $row = $st_select_trust->fetchrow_arrayref();

    #I should have the new trust
    $self->assert_equals("0.44", $row->[2]);

    ###################################################
    ## let's try to change the trust for the second user
    #let's try to change the trust once again
    Bcd::Data::Trust->change_trust($stash, 7, 5, 0.12);

    $st_select_backup->bind_param(1, 7);
    $st_select_backup->bind_param(2, 5);

    $st_select_backup->execute();
    $row = $st_select_backup->fetchrow_arrayref();

    #I should have the backup
    $self->assert_equals("0.98", $row->[2]);

    #AND i have the trust changed in the trust table
    $st_select_trust->bind_param(1, 5);
    $st_select_trust->bind_param(2, 7);

    $st_select_trust->execute();
    $row = $st_select_trust->fetchrow_arrayref();

    #I should have the new trust
    $self->assert_equals("0.12", $row->[3]);


    #################################################
    ## second change for the second user

    Bcd::Data::Trust->change_trust($stash, 7, 5, 0.11);

    $st_select_backup->bind_param(1, 7);
    $st_select_backup->bind_param(2, 5);

    $st_select_backup->execute();
    $row = $st_select_backup->fetchrow_arrayref();

    #I should have the backup, unchanged
    $self->assert_equals("0.98", $row->[2]);

    #AND i have the trust changed in the trust table
    $st_select_trust->bind_param(1, 5);
    $st_select_trust->bind_param(2, 7);

    $st_select_trust->execute();
    $row = $st_select_trust->fetchrow_arrayref();

    #I should have the new trust
    $self->assert_equals("0.11", $row->[3]);

}

sub _test_rollback_change_trust{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    #I try other changes
    Bcd::Data::Trust->change_trust($stash, $user_ids[5], $user_ids[1], 0.25);
    Bcd::Data::Trust->change_trust($stash, $user_ids[5], $user_ids[6], 0.22);

    my ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, $user_ids[5], $user_ids[1]);
    $self->assert_equals("0.25", $trust);
    $self->assert_equals("1", $am_i_second);

    Bcd::Data::Trust->rollback_trust_changes($stash, $user_ids[5]);

    #ok, now I should have the old trusts...
    ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, $user_ids[5], $user_ids[1]);
    
    $self->assert_equals("0.60", $trust);
    $self->assert_equals("1", $am_i_second);

    ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, $user_ids[5], $user_ids[6]);
    
    $self->assert_equals("0.99", $trust);
    $self->assert_equals("0", $am_i_second);

    #then I should see that the rollbacks are deleted...
    my $sql = qq{SELECT * from trusts_backups where u1=?};
    my $st_select_backup = $stash->get_connection()->prepare($sql);    

    $st_select_backup->bind_param(1, $user_ids[5]);
    $st_select_backup->execute();

    my $row = $st_select_backup->fetchrow_arrayref();

    $self->assert_null($row);
    
}

sub _test_commit_trust_changes{

    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    #I make some changes
    Bcd::Data::Trust->change_trust($stash, $user_ids[5], $user_ids[1], 0.25);
    Bcd::Data::Trust->change_trust($stash, $user_ids[5], $user_ids[6], 0.22);

    #now I commit them
    Bcd::Data::Trust->commit_trust_changes($stash, $user_ids[5]);

    #ok, if I make another change the previous change is the backup
    Bcd::Data::Trust->change_trust($stash, $user_ids[5], $user_ids[1], 0.24);

    my ($trust, $am_i_second) = Bcd::Data::Trust->_get_trust_between($stash, $user_ids[5], $user_ids[1]);
    $self->assert_equals("0.24", $trust);
    $self->assert_equals("1", $am_i_second);

    my $sql = qq{SELECT * from trusts_backups where u1=?};
    my $st_select_backup = $stash->get_connection()->prepare($sql);

    $st_select_backup->bind_param(1, $user_ids[5]);
    $st_select_backup->execute();

    my $row = $st_select_backup->fetchrow_arrayref();

    $self->assert_equals("0.25", $row->[2]);
    $self->assert_equals("1",    $row->[3]);

    

}

sub _test_get_backupped_trusts{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    Bcd::Data::Trust->commit_trust_changes($stash, $user_ids[5]);

    my ($trust51_old, $am_i_second1) = Bcd::Data::Trust->_get_trust_between($stash, $user_ids[5], $user_ids[1]);
    my ($trust56_old, $am_i_second2) = Bcd::Data::Trust->_get_trust_between($stash, $user_ids[5], $user_ids[6]);

    #I make some changes
    Bcd::Data::Trust->change_trust($stash, $user_ids[5], $user_ids[1], 0.55);
    Bcd::Data::Trust->change_trust($stash, $user_ids[5], $user_ids[6], 0.33);

    my $backups = Bcd::Data::Trust->get_user_backupped_trusts($stash, $user_ids[5]);

    $self->assert_equals($trust51_old, $backups->{$user_ids[1]});
    $self->assert_equals($trust56_old, $backups->{$user_ids[6]});


}

sub _test_exists_direct_trust_between{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    my $res = Bcd::Data::Trust->exists_direct_trust_between
	($stash, $user_ids[1],$user_ids[2]); 
    $self->assert_num_equals(1, $res);

    $res = Bcd::Data::Trust->exists_direct_trust_between
	($stash, $user_ids[1], $user_ids[6]); 
    $self->assert_num_equals(0, $res);
}

sub _test_simple_path{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";

    my ($res, $path) = Bcd::Data::Trust->get_path($user_ids[1], $user_ids[1], $stash);
    $self->assert_num_equals(1, $res);
    $self->assert_num_equals($user_ids[1], $path->[0]->[0]);


    ($res, $path) = Bcd::Data::Trust->get_path($user_ids[1], $user_ids[6], $stash);
    $self->assert_num_equals(1, $res);
    $self->assert_num_equals($user_ids[1], $path->[0]->[0]);
    $self->assert_num_equals($user_ids[5], $path->[1]->[0]);
    $self->assert_num_equals($user_ids[6], $path->[2]->[0]);


    ($res ,$path) = Bcd::Data::Trust->get_path($user_ids[2], $user_ids[8], $stash);

    $self->assert_num_equals(1, $res);
    $self->assert_num_equals($user_ids[2], $path->[0]->[0]);
    $self->assert_num_equals($user_ids[1], $path->[1]->[0]);
    $self->assert_num_equals($user_ids[5], $path->[2]->[0]);
    $self->assert_num_equals($user_ids[6], $path->[3]->[0]);
    $self->assert_num_equals($user_ids[7], $path->[4]->[0]);
    $self->assert_num_equals($user_ids[8], $path->[5]->[0]);

    ($res ,$path) = Bcd::Data::Trust->get_path($user_ids[8],$user_ids[2], $stash);


    $self->assert_num_equals(1, $res);
    $self->assert_num_equals($user_ids[8], $path->[0]->[0]);
    $self->assert_num_equals($user_ids[7], $path->[1]->[0]);
    $self->assert_num_equals($user_ids[6], $path->[2]->[0]);
    $self->assert_num_equals($user_ids[5], $path->[3]->[0]);
    $self->assert_num_equals($user_ids[1], $path->[4]->[0]);
    $self->assert_num_equals($user_ids[2], $path->[5]->[0]);

    ($res ,$path) = Bcd::Data::Trust->get_path($user_ids[1],$user_ids[4], $stash);
    $self->assert_num_equals(1, $res);
    $self->assert_num_equals($user_ids[1], $path->[0]->[0]);
    $self->assert_num_equals($user_ids[2], $path->[1]->[0]);
}

sub _test_composite_paths{
    my $self = shift;
    my $stash = $self->{stash};
    print ".";
 
   #I add some other trusts... and see what happens
    my $sql = qq{insert into trusts values($user_ids[7],$user_ids[1],0.78,0.88)};
    my $st = $self->{stash}->get_connection()->prepare($sql);
    $st->execute();

    my ($res, $path);

    ($res ,$path) = Bcd::Data::Trust->get_path($user_ids[2],$user_ids[7], $stash);
    $self->assert_num_equals(1, $res);
    $self->_check_path([2, 1, 7], $path);

    ($res ,$path) = Bcd::Data::Trust->get_path($user_ids[8], $user_ids[3], $stash);
    $self->assert_num_equals(1, $res);
    $self->_check_path([8, 7, 1, 3], $path);

    #undo the changes...
    #$self->{"conn"}->rollback();
    #you cannot rollback, with a rollback you erase the entire fixture.

    #I simply delete the row
    $sql = qq{DELETE from trusts where u1=$user_ids[7] and u2=$user_ids[1]};
    $st = $self->{stash}->get_connection()->prepare($sql);
    $st->execute();

    #the old path, without the new trust...
    ($res ,$path) = Bcd::Data::Trust->get_path($user_ids[2], $user_ids[7], $stash);
    $self->assert_num_equals(1, $res);
    $self->_check_path([2, 1, 5, 6, 7], $path);
}

sub _check_path{
    my ($self, $path_to_check, $returned_path) = @_;

    my $index = 0;
    foreach (@{$path_to_check}){
	$self->assert_num_equals($user_ids[$_], $returned_path->[$index]->[0]);
	$index++;
    }
}

# #this should test some simple trusts...
sub _test_simple_trusts{
    
    my $self = shift;
    my $stash = $self->{stash};
    my $trust;

    #the trust with myself is one -> 100bB
    $trust = Bcd::Data::Trust->get_trust($user_ids[2],$user_ids[2], $stash);
    $self->assert_num_equals(100, $trust);

    $trust = Bcd::Data::Trust->get_trust($user_ids[1], $user_ids[3], $stash);
    $self->assert_num_equals(98.55, $trust);

}

sub _test_composite_trusts{
    my $self = shift;
    my $stash = $self->{stash};
    my $trust;


    $trust = Bcd::Data::Trust->get_trust($user_ids[3], $user_ids[2], $stash);
    $self->assert_num_equals(99.64, $trust);

    $trust = Bcd::Data::Trust->get_trust($user_ids[3], $user_ids[6], $stash);
    $self->assert_num_equals(83.41, $trust);

    $trust = Bcd::Data::Trust->get_trust($user_ids[3], $user_ids[7], $stash);
    $self->assert_num_equals(45.99, $trust);

    $trust = Bcd::Data::Trust->get_trust($user_ids[3], $user_ids[8], $stash);
    $self->assert_num_equals(3.60, $trust);

    $trust = Bcd::Data::Trust->get_trust($user_ids[1], $user_ids[6], $stash);
    $self->assert_num_equals(94.43, $trust);
}

#this test is useless now, because there is ALWAYS a path
# sub _test_no_path_trust{
#     my $self = shift;
#     my $trust;

#     #the trust with a not existing path is zero
#     $trust = Bcd::Data::Trust->get_trust_decimal($user_ids[3], $user_ids[0] , $self->{stash});
#     $self->assert_num_equals(0, $trust);
# }


#this hash is a precomputed set from some users in the test ant nest
my $expected_set_from_user_1 = {
    3 => 0.7161,
    4 => 0.0074,
    2 => 0.9114,
    5 => 0.9207,
    9 => 0.9021,
    6 => 0.2775,
    7 => 0.000807065,
    0 => 0.0730,
};

sub _check_reachable_set_from_user {
    my ($self, $expected_list_of_reachable_ants, $expectd_set, $returned_set) = @_;

    $self->assert_num_equals($#{$expected_list_of_reachable_ants}+1, scalar(keys(%{$returned_set})));

    for (@{$expected_list_of_reachable_ants}){
	$self->assert_str_equals(
				 $expectd_set->{$_}
				 ,
				 substr($returned_set->{$user_ids[$_]}, 0, length($expectd_set->{$_}))
				 );
    }

}

sub _test_reachables_no_limit{
    my $self = shift;
    my $hash;
    my $stash = $self->{stash};


    $hash = Bcd::Data::Trust->get_reachable_users_from_user_trust_no_limit($user_ids[1], 0.7060, $stash);
    $self->_check_reachable_set_from_user([3, 2, 5, 9], $expected_set_from_user_1, $hash);

    $hash = Bcd::Data::Trust->get_reachable_users_from_user_trust_no_limit($user_ids[1], 0.26, $stash);
    $self->_check_reachable_set_from_user([3, 2, 5, 9, 6], $expected_set_from_user_1, $hash);

    $hash = Bcd::Data::Trust->get_reachable_users_from_user_trust_no_limit($user_ids[1], 0.00003, $stash);
    $self->_check_reachable_set_from_user([3, 2, 4, 5, 9, 6, 7, 0], $expected_set_from_user_1, $hash);

    $hash = Bcd::Data::Trust->get_reachable_users_from_user_trust_no_limit($user_ids[0], 0.03, $stash);
    $self->assert($hash->{$user_ids[9]} eq '0.0837');
}

sub _test_reachables_with_limit{
    my $self = shift;
    my $hash;
    my $stash = $self->{stash};

    #I should NOT reach the ant in the other ant nest
    $hash = Bcd::Data::Trust->get_reachable_users_from_user_trust_limited($user_ids[1], 0.7060, $stash);
    $self->_check_reachable_set_from_user([3, 2, 5], $expected_set_from_user_1, $hash);

    $hash = Bcd::Data::Trust->get_reachable_users_from_user_trust_limited($user_ids[1], 0.26, $stash);
    $self->_check_reachable_set_from_user([3, 2, 5, 6], $expected_set_from_user_1, $hash);

    $hash = Bcd::Data::Trust->get_reachable_users_from_user_trust_limited($user_ids[1], 0.00003, $stash);
    $self->_check_reachable_set_from_user([3, 2, 5, 6, 7, 4], $expected_set_from_user_1, $hash);
}

sub _test_get_maximum_reachable_set{
    my $self=shift;
    my $stash = $self->{"stash"};

    my $maximum_set = Bcd::Data::Trust->_get_maximum_reachable_set($user_ids[5], $stash);

    #the maximum set should be equal to all the test set
    my $index = 0;
    for(@test_users){
	if (!defined($_->{id_ant_nest})){
	    $self->assert_not_null($maximum_set->{$user_ids[$index]});
	}
	$index++;
    }
}

sub _test_is_valid_trust{
    my $self=shift;
    my $stash = $self->{"stash"};
    print ".";

    $self->assert_num_equals(1, Bcd::Data::Trust->is_valid_trust(0.5));
    $self->assert_num_equals(0, Bcd::Data::Trust->is_valid_trust(0));
    $self->assert_num_equals(0, Bcd::Data::Trust->is_valid_trust(-0.5));
    $self->assert_num_equals(1, Bcd::Data::Trust->is_valid_trust(1));
    $self->assert_num_equals(0, Bcd::Data::Trust->is_valid_trust(1.5));
}

sub _test_is_valid_trust_bb{

    my $self=shift;
    my $stash = $self->{"stash"};
    print ".";

    $self->assert_num_equals(1, Bcd::Data::Trust->is_valid_trust_bb(0.5));
    $self->assert_num_equals(1, Bcd::Data::Trust->is_valid_trust_bb(0));
    $self->assert_num_equals(1, Bcd::Data::Trust->is_valid_trust_bb(-0.5));
    $self->assert_num_equals(1, Bcd::Data::Trust->is_valid_trust_bb(100));
    $self->assert_num_equals(1, Bcd::Data::Trust->is_valid_trust_bb(-100));
    $self->assert_num_equals(0, Bcd::Data::Trust->is_valid_trust_bb(150));
    $self->assert_num_equals(1, Bcd::Data::Trust->is_valid_trust_bb("-INF"));
}

sub _test_dec_from_bBel{
    my $self=shift;
    my $stash = $self->{"stash"};
    print ".";

    $self->assert_num_equals(0,       Bcd::Data::Trust::dec_from_bBel("-INF"));
    $self->assert_str_equals("0.1",   Bcd::Data::Trust::dec_from_bBel("90"));
    $self->assert_str_equals("0.01",  Bcd::Data::Trust::dec_from_bBel("80"));
    $self->assert_str_equals("1e-10", Bcd::Data::Trust::dec_from_bBel("0"));
    $self->assert_str_equals("1e-11", Bcd::Data::Trust::dec_from_bBel("-10"));
}

1;

