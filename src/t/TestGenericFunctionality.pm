package t::TestGenericFunctionality;

#this should simply make some tests...

use errors::ErrorCodes;

use t::TestCommon;
our @ISA = qw(t::TestCommon);

# sub set_up {

#     my $self = shift;


#     #call the base class
#     $self->SUPER::set_up(@_);
# }


sub test_unknown_command{
    my $self = shift;



    $self->write_server_eol("this is an unknown command!!!");
    $self->write_server("some useless parameters");
    $self->write_server_eot();

    $self->check_code(errors::ErrorCodes::BEC_UNKNOWN_COMMAND);
}

sub test_basic_hand_shake{
    my $self = shift;
    
    $self->write_server_eol("sv.are_you_there");
    $self->write_server_eot();

    $self->check_code(errors::ErrorCodes::BEC_OK);
}
