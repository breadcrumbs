package t::TestNewAntNestsBot;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;

use Data::Dumper;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Bots::NewAntNestsBot;
use Bcd::Data::NewAntNests;
use Bcd::Data::FoundersTrusts;
use Digest::SHA;

sub test_new_founder_ant_not_checked_rule{
    my $self = shift;
    my $stash = $self->{stash};

    my $bot = Bcd::Bots::NewAntNestsBot->new();
    $self->assert_not_null($bot);


    #ok, now let's insert a fake ant nest...
    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, "8012899", "formicaio di prova");

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, "8012899", "elisa", 'lino@localhost' , 'password');


    my ($res, $cycle) = $bot->go_test($stash, '8012899');

    #print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n";
    #print Dumper($res);

    #ok, now there should be no firing
    $self->assert_equals(0, scalar(@{$res}));

    my $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, "8012899");
    $self->assert_equals(1, $count);

    #time passes...
    my $sql = 
	qq{UPDATE ant_nests_founders SET last_status_change = localtimestamp - interval '86401 seconds' where id_ant_nest = 8012899};

    my $st = $stash->get_connection()->prepare($sql);
    $st->execute();


    ($res, $cycle) = $bot->go_test($stash, '8012899');
    #ok, now there should be a firing
    #$self->assert_equals(1, scalar(@{$res}));

    #print Dumper($res);

    
    $self->assert($res->[0] =~ /deleted ONLY lazy ant elisa/);
    
    #no founders now...
    $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, "8012899");
    $self->assert_equals(0, $count);

    #the ant nest should not be existing...
    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_hash($stash, "8012899");
    $self->assert_null($ant_nest);
    
}

sub test_downgrade_the_ant_nest{

    my $self = shift;
    my $stash = $self->{stash};

    my $bot = Bcd::Bots::NewAntNestsBot->new();
    $self->assert_not_null($bot);

    #I should first of all have some founders in this ant nest...
    my $code = 80128;

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, "80128", "formicaio di prova");

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, "80128", "elisa", 'lino@localhost' , 'password');

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, "80128", "elisa1", 'lino@localhost' , 'password');

    #ok, then I should fake the upgrade of this ant nest.

    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $code, Bcd::Constants::NewAntNestsConstants::WAITING_FOR_CHECKING_IN);

    #I confirm one founder...
    #first of all I get the founder...
    my $user = Bcd::Data::NewAntNests->select_new_founder_hash_post_nick
	($stash, $code, 'elisa');

    $self->assert_not_null($user);
    Bcd::Data::NewAntNests->new_founder_confirmed($stash, $user->{id_user});

    #ok, then I make a lazy ant
    $user = Bcd::Data::NewAntNests->select_new_founder_hash_post_nick
	($stash, $code, 'elisa1');
    $self->assert_not_null($user);

    my $sql = 
	qq{UPDATE ant_nests_founders SET last_status_change = localtimestamp - interval '86401 seconds' where id_user = ?};

    my $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, $user->{id_user});
    $st->execute();

    
    my ($res, $cycle) = $bot->go_test($stash, $code);

    #print Dumper($res);

    #Ok, I have downgraded the ant nest...
    $self->assert($res->[0] =~ /DOWNGRADED.*80128/);

    #now I should have only one founder...
    my $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, $code);
    $self->assert_equals(1, $count);    

    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_hash($stash, $code);
    $self->assert_equals(Bcd::Constants::NewAntNestsConstants::WAITING_FOR_INITIAL_FOUNDERS, $ant_nest->{state});

}


sub test_all_ants_have_checked_in_rule{
    my $self = shift;

    $self->_test_generic_all_founders_have_reached_rule
	(
	 Bcd::Constants::NewAntNestsConstants::WAITING_FOR_CHECKING_IN,
	 Bcd::Constants::FoundersConstants::USER_BOOKED_CONFIRMED,
	 "immitting",
	 Bcd::Constants::FoundersConstants::WAITING_FOR_PERSONAL_DATA,
	 Bcd::Constants::NewAntNestsConstants::FOUNDERS_IMMITTING_DATA
	 );
}


sub test_all_ants_have_immitted_data_rule{
    my $self = shift;

    $self->_test_generic_all_founders_have_reached_rule
	(
	 Bcd::Constants::NewAntNestsConstants::FOUNDERS_IMMITTING_DATA,
	 Bcd::Constants::FoundersConstants::PERSONAL_DATA_IMMITTED,
	 "waiting for trust",
	 Bcd::Constants::FoundersConstants::TRUST_NOT_PRESENT,
	 Bcd::Constants::NewAntNestsConstants::TRUSTS_NETWORK_BUILDING
	 );
}

sub test_all_ants_have_built_the_web_of_trust{
    my $self = shift;

    $self->_test_generic_all_founders_have_reached_rule
	(
	 Bcd::Constants::NewAntNestsConstants::TRUSTS_NETWORK_BUILDING,
	 Bcd::Constants::FoundersConstants::TRUST_PRESENT,
	 "selecting candidates",
	 Bcd::Constants::FoundersConstants::NO_CHOICE_OF_CANDIDATE,
	 Bcd::Constants::NewAntNestsConstants::CANDIDATES_RESEARCH
	 );
}

sub _test_generic_all_founders_have_reached_rule{
    my ($self, $initial_ant_nest_state, $initial_founders_state,
	$message_expected, $new_founders_state, $new_ant_nest_state) = @_;

    my $stash = $self->{stash};

    my $bot = Bcd::Bots::NewAntNestsBot->new();
    $self->assert_not_null($bot);

    #I should first of all have some founders in this ant nest...
    my $code = 12393;

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "formicaio di prova");

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){

	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $code, "elisa$_", 'lino@localhost' , 'password');

	  
      }

    #Ok, then I simulate the confirmation of this ants
    my $sql = qq{UPDATE ant_nests_founders SET id_status = } . 
	$initial_founders_state . qq{where id_ant_nest = $code};
    my $st = $stash->get_connection()->prepare($sql);
    $st->execute();

    #and I upgrade the ant nest...
    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $code, $initial_ant_nest_state);

    #ok, then I run the bot.

    my ($res, $cycle) = $bot->go_test($stash, $code);

    $self->assert($res->[0] =~ /$code.*upgraded.*$message_expected/);

    #ok, now let's see the state of all the founders
    my $founders = Bcd::Data::NewAntNests->get_founders_ant_nest_arr($stash, $code);

    foreach(@{$founders}){
	my $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $_->[0]);
	$self->assert_equals
	    ($new_founders_state, $founder->{id_status});
    }

    #let's get the state of this ant nest
    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_hash($stash, $code);
    $self->assert_equals($new_ant_nest_state, $ant_nest->{state});

    #Another time the rule should not fire
    ($res, $cycle) = $bot->go_test($stash, $code);
    $self->assert_null($res->[0]);
    
}

sub _update_the_state_of_all_the_ants{
    my ($self, $new_state) = @_;
    my $stash = $self->{stash};

    foreach(@{$self->{founder_ids}}){
	Bcd::Data::Founders->update_founder_state
	    ($stash, $_, $new_state);
    }
    
}

sub _check_all_the_founders_in_this_state{
    my ($self, $state_to_check) = @_;
    my $stash = $self->{stash};

    foreach(@{$self->{founder_ids}}){
	my $founder = Bcd::Data::Founders->get_founder_main_data_arr($stash, $_);
	$self->assert_equals($state_to_check, $founder->[1]);
    }
    
}

sub _add_test_trusts{
    my $self = shift;
    my $stash = $self->{stash};

    ######################### adding test trusts
    my $trusts = 
	[
	 [0.12, 0.13, 0.14, 0.15],
	 [0.21, 0.23, 0.24, 0.25],
	 [0.31, 0.32, 0.34, 0.35],
	 [0.41, 0.42, 0.43, 0.45],
	 [0.51, 0.52, 0.53, 0.54],
	 ];

    #I must make a double loop
    my ($i, $j);
    $i = $j = 0;

    for (@{$trusts}){
	for (@{$_}){

	    $j++ if ($i == $j); #no trust with myself.
	    #ok, I can add the trust
	    #print "Adding trust $i, $j , $_\n";
	    Bcd::Data::FoundersTrusts->add_founder_trust($stash, 
							 $self->{founder_ids}->[$i], 
							 $self->{founder_ids}->[$j], 
							 $_);
	    $j++;
	}
	$j = 0;
	$i++;
    }
    ############################### finished adding test trusts
}

sub test_the_post_vote_rule{
    my $self = shift;
    my $stash = $self->{stash};

    $self->{code} = 12393;
    $self->{founder_ids} = [];

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $self->{code}, "test_ant_nest");

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){

	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $self->{code}, "john$_", "john$_\@mail.com" , 'password');

	  push(@{$self->{founder_ids}}, $stash->get_last_founder_id());
      }


    $self->_add_test_trusts();

    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $self->{code}, Bcd::Constants::NewAntNestsConstants::MAKING_POOL);

    #Ok, now I should update the ants
    $self->_update_the_state_of_all_the_ants(Bcd::Constants::FoundersConstants::VOTED);

    
    #two for boss and two for treasurers candidates...
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[0], '1', '0');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[1], '1', '0');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[2], '0', '1');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[3], '0', '1');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[4], '0', '1');

    my $bot = Bcd::Bots::NewAntNestsBot->new();
    $self->assert_not_null($bot);

    #first test all null votes, NOT VALID
    foreach (@{$self->{founder_ids}}){
	Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, undef, undef);
    }

    my ($res, $cycle) = $bot->go_test($stash, $self->{code});
    $self->assert($res->[0] =~ /vote failed/);

    #then I a pair in the boss
    #return to start state....
    Bcd::Data::FoundersVotings->delete_all_votes_for_this_ant_nest($stash, $self->{code});
    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $self->{code}, Bcd::Constants::NewAntNestsConstants::MAKING_POOL);
    $self->_update_the_state_of_all_the_ants(Bcd::Constants::FoundersConstants::VOTED);

    Bcd::Data::FoundersVotings->add_a_vote
	($stash, $self->{code}, $self->{founder_ids}->[0], $self->{founder_ids}->[2]);
    Bcd::Data::FoundersVotings->add_a_vote
	($stash, $self->{code}, $self->{founder_ids}->[0], undef);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, $self->{founder_ids}->[1], undef);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, $self->{founder_ids}->[1], undef);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, undef, undef);

    my $fail;
    my $winner;

    my $votes = Bcd::Data::FoundersVotings->get_votes_result($stash, $self->{code});

    ($fail, $winner) = $bot->_check_votes_validity_fail($votes->{bosses});
    $self->assert_equals(1, $fail);

    ($fail, $winner) = $bot->_check_votes_validity_fail($votes->{treasurers});
    $self->assert_equals(0, $fail);
    $self->assert_equals($self->{founder_ids}->[2], $winner);
	
    ($res, $cycle) = $bot->go_test($stash, $self->{code});
    $self->assert($res->[0] =~ /vote failed/);

    ###### a successful run
    Bcd::Data::FoundersVotings->delete_all_votes_for_this_ant_nest($stash, $self->{code});
    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $self->{code}, Bcd::Constants::NewAntNestsConstants::MAKING_POOL);
    $self->_update_the_state_of_all_the_ants(Bcd::Constants::FoundersConstants::VOTED);

    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, 
					   $self->{founder_ids}->[0], $self->{founder_ids}->[2]);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, 
					   $self->{founder_ids}->[0], $self->{founder_ids}->[2]);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, 
					   $self->{founder_ids}->[1], undef);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, 
					   $self->{founder_ids}->[1], $self->{founder_ids}->[3]);
    Bcd::Data::FoundersVotings->add_a_vote($stash, $self->{code}, 
					   $self->{founder_ids}->[1], $self->{founder_ids}->[2]);

    $votes = Bcd::Data::FoundersVotings->get_votes_result($stash, $self->{code});

    ($fail, $winner) = $bot->_check_votes_validity_fail($votes->{bosses});
    $self->assert_equals(0, $fail);
    $self->assert_equals($self->{founder_ids}->[1], $winner);

    ($fail, $winner) = $bot->_check_votes_validity_fail($votes->{treasurers});
    $self->assert_equals(0, $fail);
    $self->assert_equals($self->{founder_ids}->[2], $winner);
	
    ($res, $cycle) = $bot->go_test($stash, $self->{code});
    $self->assert($res->[0] =~ /vote OK.*=(\d+)$/);
    my $assigned_code = $1;

    #print "\n\n>>>>>>>>>>>>>>>>>>>>>> AFTER THE VOTE \n\n\n";
    $self->_check_the_presence_of_the_new_ant_nest($assigned_code);

    #now I test the final rule, because I need also the presence of the ant nest

    #I simulate the set up done by the treasurer and the boss
    Bcd::Data::Founders->update_founder_state($stash, $self->{founder_ids}->[1], 
					      Bcd::Constants::FoundersConstants::SET_UP_DONE);

    Bcd::Data::Founders->update_founder_state($stash, $self->{founder_ids}->[2], 
					      Bcd::Constants::FoundersConstants::SET_UP_DONE);

    #ok, the  bot should fire now

    ($res, $cycle) = $bot->go_test($stash, $self->{code});
    $self->assert($res->[0] =~ /^Final creation.*ready$/);

    #another time the rule should not fire
    ($res, $cycle) = $bot->go_test($stash, $self->{code});
    $self->assert_null($res->[0]);

    #ok, then I should check the state of all the founders.
    #the founders should be deleted now...

    #test TODO

    my $founders = Bcd::Data::Founders->get_all_founders_ant_nest_arr
	($stash, $self->{code});
    $self->assert_equals(0, scalar(@{$founders}));
    #the ant nest is not existing now
    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_hash($stash, $self->{code});
    $self->assert_null($ant_nest);


    #and all the ants in the ant nest of the assigned code should be created as normal ants, so they can login
    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){
	my $nick = "john$_";

	my $user_new = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	    ($stash, $assigned_code, $nick);

	$self->assert_not_null($user_new);

	#Ok, then I will the check some data
	$self->assert_equals(Bcd::Constants::Users::NORMAL_ACTIVE_ANT,
			     $user_new->{users_state});

      }

}

sub _check_the_presence_of_the_new_ant_nest{
    my ($self, $assigned_code) = @_;

    my $stash = $self->{stash};


    #let's see if there is the assigned code in the table of the ant nest
    my $new_ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr
	($stash, $self->{code});
    $self->assert_equals($assigned_code, $new_ant_nest->[4]);

    foreach(@{$self->{founder_ids}}){
	my $founder = Bcd::Data::Founders->get_founder_main_data_arr($stash, $_);

	#if the founder is a boss or a treasurer the state should be different
	if ($founder->[7] == '1'){
	    $self->assert_equals(Bcd::Constants::FoundersConstants::BOSS_SET_UP_TO_DO, $founder->[1]);
	} elsif ( $founder->[8] == '1'){
	    $self->assert_equals(Bcd::Constants::FoundersConstants::WAITING_BOSS_SET_UP, $founder->[1]);
	} else {
	    $self->assert_equals(Bcd::Constants::FoundersConstants::WAITING_TO_BE_CREATED, $founder->[1]);
	}
    }

    #now I should check that there is an ant nest in this code...
    my $ant_nest = Bcd::Data::AntNests->get_ant_nest_detail($stash, $assigned_code);

    $self->assert_not_null($ant_nest);
    $self->assert_equals($self->{code}, substr($ant_nest->[0], 0, 5));
    $self->assert_equals("test_ant_nest", $ant_nest->[3]);
    $self->assert_equals(Bcd::Constants::AntNestsConstants::CREATION_STATE, $ant_nest->[1]);

    #the boss and the treasurer are "hardcoded" in the test.
    #boss is john2 and treasurer is john3

    #and then the users...
    my @users_ids;
    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){
	my $nick = "john$_";

	my $user_new = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	    ($stash, $assigned_code, $nick);

	#print Dumper($user_new);

	push(@users_ids, $user_new->{id});

	$self->assert_not_null($user_new);

	#Ok, then I will the check some data
	$self->assert_equals($nick,          $user_new->{nick});
	$self->assert_equals($assigned_code, $user_new->{id_ant_nest});
	$self->assert_equals(Bcd::Constants::Users::CREATION_STATE,
			     $user_new->{users_state});

	$self->assert_equals('password', $user_new->{totem});

	my $sha = Digest::SHA1->new;
	$sha->add('password');
	my $digest = $sha->hexdigest;

	$self->assert_equals($digest, $user_new->{password});
	$self->assert_null($user_new->{ro});
	$self->assert_null($user_new->{theta});
	$self->assert_null($user_new->{tutor_first});
	$self->assert_null($user_new->{tutor_second});

	#check the roles...
	if ( $_ == 2){
	    #I should be a boss
	    $self->assert_equals(3, oct("0b" . $user_new->{role_mask}));
	} elsif ($_ == 3){
	    $self->assert_equals(5, oct("0b" . $user_new->{role_mask}));
	} else {
	    $self->assert_equals(1, oct("0b" . $user_new->{role_mask}));
	}
      }


    my $sql = qq{SELECT trust_u1_u2, trust_u2_u1 FROM trusts  WHERE u1 = ? AND u2 = ?};
    my $st = $stash->prepare_cached($sql);

    #print Dumper(\@users_ids);

    for (0..3){
	my $j = $_ + 1;
	while ($j < 5){

	    #print "Selecting trust from $_ and $j\n";
	    
	    #ok, I take the trust
	    $st->bind_param(1, $users_ids[$_]);
	    $st->bind_param(2, $users_ids[$j]);

	    $st->execute();
	    
	    my $row = $st->fetchrow_arrayref();

	    $st->finish();

	    #print Dumper($row);
	    my $temp_i = $_+1;
	    my $temp_j = $j+1;
	    $self->assert_equals("0.$temp_i$temp_j", $row->[0]);
	    $self->assert_equals("0.$temp_j$temp_i", $row->[1]);

	    $j++;
	}
	$j = 0;
    }
}

sub test_the_end_of_candidates_research_rule{
    my $self = shift;
    my $stash = $self->{stash};

    $self->{code} = 12393;
    $self->{founder_ids} = [];

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $self->{code}, "test_ant_nest");

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){

	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $self->{code}, "john$_", "john$_\@mail.com" , 'password');

	  push(@{$self->{founder_ids}}, $stash->get_last_founder_id());
      }

    $self->_add_test_trusts();

    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $self->{code}, Bcd::Constants::NewAntNestsConstants::CANDIDATES_RESEARCH);

    #Ok, now I should update the ants
    $self->_update_the_state_of_all_the_ants(Bcd::Constants::FoundersConstants::CHOICE_MADE);

    #first rule: no boss and no treasurer...
    foreach (@{$self->{founder_ids}}){
	Bcd::Data::Founders->set_founders_flags($stash, $_, '0','0');
    }

    #get the bot.
    my $bot = Bcd::Bots::NewAntNestsBot->new();
    $self->assert_not_null($bot);

    my ($res, $cycle) = $bot->go_test($stash, $self->{code});
    $self->assert($res->[0] =~ /no boss or no treasurer found/);

    #all the ants should be in the NO_CHOICE_OF_CANDIDATE state
    $self->_check_all_the_founders_in_this_state(Bcd::Constants::FoundersConstants::NO_CHOICE_OF_CANDIDATE);

    ################################
    ##testing of vote needed
    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $self->{code}, Bcd::Constants::NewAntNestsConstants::CANDIDATES_RESEARCH);

    $self->_update_the_state_of_all_the_ants(Bcd::Constants::FoundersConstants::CHOICE_MADE);

    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[2], '0','1');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[0], '1','0');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[1], '1','0');

    ($res, $cycle) = $bot->go_test($stash, $self->{code});
    $self->assert($res->[0] =~ /all ok, vote needed/);

    #all the ants should be in the NO_CHOICE_OF_CANDIDATE state
    $self->_check_all_the_founders_in_this_state(Bcd::Constants::FoundersConstants::NO_VOTED_YET);

    ##############################
    ## testing of no vote needed, the ant nest should be present, here.

    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $self->{code}, Bcd::Constants::NewAntNestsConstants::CANDIDATES_RESEARCH);
    $self->_update_the_state_of_all_the_ants(Bcd::Constants::FoundersConstants::CHOICE_MADE);

    #then I test a valid vote 1 boss and 1 treasurer
    #Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[0], '1','0');
    #Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[1], '0','1');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[0], '0','0');

    ($res, $cycle) = $bot->go_test($stash, $self->{code});
    #print Dumper($res);
    $self->assert($res->[0] =~ /all ok.*no vote needed.*=(\d+)$/);

    my $assigned_code=$1;
    #then I should check the presence of the ant nest...
    #print "\n\n>>>>>>>>>>>>>>>>>>>>>> NO VOTE NEEDED!!!!! \n\n\n";
    $self->_check_the_presence_of_the_new_ant_nest($assigned_code);
    
    #all the ants should be in the NO_CHOICE_OF_CANDIDATE state
    #$self->_check_all_the_founders_in_this_state(Bcd::Constants::FoundersConstants::WAITING_TO_BE_CREATED);
}
