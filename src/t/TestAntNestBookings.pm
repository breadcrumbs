package t::TestAntNestBookings;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;
use DBI;
use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::NewAntNests;
use Bcd::Data::Model;
use Bcd::Data::AntNests;
use Bcd::Data::StatementsStash;
use Data::Dumper;

# sub set_up{
#     my $self = shift;
#     my $stash = $self->{stash};
# }

sub test_initial_number_of_bookings{
    my $self = shift;
    my $stash = $self->{stash};

    my $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, "8012899");
    $self->assert_equals(0, $count);

    $count = Bcd::Data::NewAntNests->get_ant_nest_lookers_count($stash, "8012899");
    $self->assert_equals(0, $count);
    
}


sub test_new_booking{
    my $self = shift;
    my $stash = $self->{stash};


    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, "8012899", "formicaio di prova");

    #ok, then I will create a looker
    Bcd::Data::NewAntNests->insert_a_new_looker($stash, "8012899", "lino", 'lino@localhost');

    my $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, "8012899");
    $self->assert_equals(0, $count);

    $count = Bcd::Data::NewAntNests->get_ant_nest_lookers_count($stash, "8012899");
    $self->assert_equals(1, $count);

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, "8012899", "elisa", 'lino@localhost' , 'password');

    $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, "8012899");
    $self->assert_equals(1, $count);

    $count = Bcd::Data::NewAntNests->get_ant_nest_lookers_count($stash, "8012899");
    $self->assert_equals(1, $count);
}

sub test_login_new_user{
    my $self = shift;
    my $stash = $self->{stash};

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, "8012899", "formicaio di prova");

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, "8012899", "elisa", 'lino@localhost' , 'password');

    my ($res, $id) = Bcd::Data::NewAntNests->login($stash, 8012899, "elisa", 'password');
    $self->assert_equals(2, $res);
    #the account is not ready

    #let's confirm this ant

    my $user = Bcd::Data::NewAntNests->select_new_founder_hash_post_nick($stash, 8012899, "elisa");

    $id = $user->{id_user};

    Bcd::Data::NewAntNests->new_founder_confirmed($stash, $id);

    my $role;

    ($res, $role, $id) = Bcd::Data::NewAntNests->login($stash, 8012899, "elisa", 'password');
    $self->assert_equals(0, $res);
    $self->assert_equals(Bcd::Data::Users::ROLE_FOR_A_FOUNDER_ANT, $role);

    #ok, now let's get the new user by this id
    my $hash = Bcd::Data::Founders->get_founder_main_data_hash($stash, $id);
    $self->assert_equals("elisa", $hash->{nick_or_name});

    #different password
    $res = Bcd::Data::NewAntNests->login($stash, 8012899, "elisa", 'passwordooo');
    $self->assert_equals(1, $res);

    #different user
    $res = Bcd::Data::NewAntNests->login($stash, 8012899, "elisaiii", 'password');
    $self->assert_equals(1, $res);

    #test the get complete user data
    my $complete_data = Bcd::Data::Founders->get_founder_complete_data_hash($stash, $id);

    $self->assert_equals('lino@localhost', $complete_data->{email});

    #these fields should exists, even if they are null
    $self->assert(exists($complete_data->{first_name}));
    $self->assert(exists($complete_data->{last_name}));
    $self->assert(exists($complete_data->{home_phone}));
    $self->assert(exists($complete_data->{mobile_phone}));

    $self->assert_null($complete_data->{first_name});
    $self->assert_null($complete_data->{last_name});
    $self->assert_null($complete_data->{home_phone});
    $self->assert_null($complete_data->{mobile_phone});


}

sub test_confirm_new_booked_user{

    my $self = shift;
    my $stash = $self->{stash};

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, "8012899", "formicaio di prova");

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, "8012899", "elisa", 'lino@localhost' , 'password');

    #ok, then I should confirm this user...
    my $sql = qq{select id_user from ant_nests_founders where id_ant_nest = 8012899 and nick_or_name = 'elisa'};
    my $st = $stash->get_connection->prepare($sql);

    $st->execute();
    my $id = $st->fetchrow_arrayref()->[0];

    #ok, now I confirm this user...
    Bcd::Data::NewAntNests->new_founder_confirmed($stash, $id);
    
    #ok, now I get the new user
    my $user = Bcd::Data::Founders->get_founder_main_data_hash($stash, $id);

    $self->assert_equals(Bcd::Constants::FoundersConstants::USER_BOOKED_CONFIRMED, $user->{id_status});

    #the token should be deleted...
    
    $sql = qq{select * from ant_nests_founders_tokens where id_user = ?};
    $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, $id);
    
    $st->execute();

    my $row = $st->fetch();

    $self->assert_null($row);
    
}

# sub test_insert_new_booked_user{
#     my $self = shift;
#     my $stash = $self->{stash};

#     #this should fail
#     eval {
# 	local $stash->get_connection()->{PrintError} = 0;  # localize and turn off for this block
# 	local $stash->get_connection()->{RaiseError} = 0;  # localize and turn off for this block
# 	Bcd::Data::NewAntNests->insert_a_new_founder
# 	    ($stash, "8012899", "lino", 'lino@localhost' , 'password');
#       };

#     $self->assert($@ =~ /Key \(id_ant_nest\)=\(8012899\) is not present/); 


# }
