package t::TestFoundersCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Model;
use Bcd::Constants::NewAntNestsConstants;
use Bcd::Data::Founders;
use Bcd::Data::NewAntNests;
use Bcd::Errors::ErrorCodes;


use Data::Dumper;

sub set_up{
    my $self = shift;
    my $stash = $self->{stash};

    $self->{code} = 89899;
    $self->{founder_ids} = [];

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $self->{code}, "test_ant_nest");

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){

	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $self->{code}, "john$_", "john$_\@mail.com" , 'password');

	  push(@{$self->{founder_ids}}, $stash->get_last_founder_id());
      }

    #this ant should be allowed to login
    Bcd::Data::Founders->update_founder_state
	($stash, $self->{founder_ids}->[0], 
	 Bcd::Constants::FoundersConstants::USER_BOOKED_CONFIRMED);
}

sub test_get_candidates_summary{
    my $self = shift;
    my $stash = $self->{stash};

    
    my $session = $self->_connect_to_bcd('john1', $self->{code}, 'password');

    my $command = Bcd::Data::Model->instance()->get_command("na_get_candidates_summary");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #I should be no candidates...
    my $bosses     = $res->{bosses_candidates};
    my $treasurers = $res->{treasurers_candidates};

    $self->assert_equals(0, scalar(@{$bosses}));
    $self->assert_equals(0, scalar(@{$treasurers}));

    #ok, now I put some candidates

    #print Dumper($self);

    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[0], '1', '0');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[1], '1', '0');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[2], '0', '1');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[3], '0', '1');
    Bcd::Data::Founders->set_founders_flags($stash, $self->{founder_ids}->[4], '0', '1');


    $bosses = Bcd::Data::Founders->get_candidates_bosses($stash, $self->{code});
    $self->assert_equals(2, scalar(@{$bosses}));

    $treasurers = Bcd::Data::Founders->get_candidates_treasurers($stash, $self->{code});
    $self->assert_equals(3, scalar(@{$treasurers}));

    #I reexecute the command

    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $bosses     = $res->{bosses_candidates};
    $treasurers = $res->{treasurers_candidates};

    $self->assert_equals(2, scalar(@{$bosses}));
    $self->assert_equals(3, scalar(@{$treasurers}));


}

sub test_candidates_flag{
    my $self = shift;
    my $stash = $self->{stash};

    
    my $session = $self->_connect_to_bcd('john1', $self->{code}, 'password');

    #ok, now I get the command.
    my $command = Bcd::Data::Model->instance()->get_command("nu_update_candidates_flags");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line('1');
    $command->parse_line('2093');
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    Bcd::Data::Founders->update_founder_state
	($stash, $self->{founder_ids}->[0], 
	 Bcd::Constants::FoundersConstants::NO_CHOICE_OF_CANDIDATE);

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_BOOLEAN_PARAMETER_OUT_OF_RANGE, $exit_code);

    #ok, now I test with another set  of flags
    $command->{treasurer_flag} = '1';

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_YOU_CANNOT_BE_BOSS_AND_TREASURER, $exit_code);


    $command->{treasurer_flag} = '0';

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #the flags should be changed
    my $founder = Bcd::Data::Founders->get_founder_main_data_arr($stash, $self->{founder_ids}->[0]);
    
    $self->assert_equals(1, $founder->[7]);
    $self->assert_equals(0, $founder->[8]);
    
    #another time the command should fail
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

}

1;
