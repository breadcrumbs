package AllTests;

use lib 't';

use Test::Unit::TestSuite;

sub new {
    my $class = shift;
    return bless {}, $class;
}

sub suite {
    my $class = shift;
    my $suite = Test::Unit::TestSuite->empty_new("Framework Tests");

    #these are "basic test" they don't call the server
    $suite->add_test('t::TestDatabase');
    $suite->add_test('t::TestConfiguration');

    $suite->add_test('t::TestAntNests');
    $suite->add_test('t::TestAntNestBookings');
    $suite->add_test('t::TestAntNestsCommands');
    $suite->add_test('t::TestAntNestBookingCommands');
    $suite->add_test('t::TestFounders');
    $suite->add_test('t::TestFoundersCommands');
    $suite->add_test('t::TestFoundersVotings');
    $suite->add_test('t::TestFoundersVotingsCommands');

    $suite->add_test('t::TestInitialSetupCommands');


    $suite->add_test('t::TestCommandsFactory');
    $suite->add_test('t::TestPostCode');

    $suite->add_test('t::TestToken');
    $suite->add_test('t::TestRealCurrency');


    $suite->add_test('t::TestTrust');
    $suite->add_test('t::TestTrustCommands');
    $suite->add_test('t::TestFoundersTrusts');
    $suite->add_test('t::TestFoundersTrustsCommands');

    $suite->add_test('t::TestAccount');
    $suite->add_test('t::TestUsersActivities');
    $suite->add_test('t::TestAccountsPlan');
    $suite->add_test('t::TestTransaction');

    $suite->add_test('t::TestBank');
    $suite->add_test('t::TestBankCommands');
    $suite->add_test('t::TestUsers');
    $suite->add_test('t::TestUsersCommands');

    $suite->add_test('t::TestDeposits');
    $suite->add_test('t::TestDepositCommands');

    $suite->add_test('t::TestCommandParser');

    $suite->add_test('t::TestActivities');
    $suite->add_test('t::TestPersonalUsersData');

    $suite->add_test('t::TestAds');
    $suite->add_test('t::TestWebSite');
    $suite->add_test('t::TestPublicSites');
    $suite->add_test('t::TestCreditsAndDebits');
    $suite->add_test('t::TestInvoices');

    #the bots...
    $suite->add_test('t::TestNewAntNestsBot');
    $suite->add_test('t::TestTrustBot');


    return $suite;
}

1;
