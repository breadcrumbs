package t::TestCommon;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this should be the base class for all the tests in this directory
use IO::Socket;
use Test::Unit::TestCase;
our @ISA = qw(Test::Unit::TestCase);

use constant EOL => "\015\012";
use constant EOT => EOL."eot".EOL; #This is the end of transmission marker.


sub new {
    my $self = shift()->SUPER::new(@_);
    # your state for fixture here

    $/ = "\015\012"; # I change the "end of line" separator.

    #in the set up of the test I should connect to the server
    my $handle = IO::Socket::INET->new(Proto     => "tcp",
				       PeerAddr  => "localhost",
				       PeerPort  => "9000");

    $self->{"server"} = $handle;

    <$handle>; # get rid of the welcome message

    return $self;
}

sub set_up {
    my $self = shift;
    my $handle = $self->{"server"};

    $self->assert_not_null($handle);

}

sub DESTROY{
   # clean up after test
    my $self = shift;

    #I simply close the handle
    close($self->{"server"}); 
}

sub tear_down {
 
}


sub connect_to_server{

#create a tcp connection to the specified host and port
    my $handle = IO::Socket::INET->new(Proto     => "tcp",
				       PeerAddr  => "localhost",
				       PeerPort  => "9000");

    unless ($handle){
	die "the server is not responding... exiting\n"
	}

    $/ = "\015\012"; # I change the "end of line" separator.

    return $handle;

}

sub write_server_eol{
    my $self = shift;
    my $what = shift;
    $self->write_server($what);
    my $handle = $self->{"server"};
    print $handle EOL;
}

sub write_server{
    my $self = shift;
    my $what = shift;
    my $handle = $self->{"server"};
    print $handle $what;
    
}

sub write_server_eot{
    my $self = shift;
    my $handle = $self->{"server"};
    print $handle EOT;
}

sub check_code{

    my $self = shift;
    my $check = shift;
    my $message = shift;

    my $handle = $self->{"server"};

    my $code = <$handle>;

    chomp $code;

    #ok ($code eq $check, $name);

    if ($message){
	$self->assert_num_equals($code, $check, $message);
    } else{
	$self->assert_num_equals($code, $check, "I have unluckily $code instead of $check");
    }

    <$handle>; #get rid of the eot
}

sub _super_user_connect{

    my $self = shift;
    my $handle = $self->{"server"};

    print $handle "sv.connect", EOL;
    print $handle "bc-root", EOL;
    print $handle "cippo"; #the test password
    print $handle EOT;

    $self->check_code(errors::ErrorCodes::BEC_OK);
}

sub _login_to_server{
    my $self = shift;
    
    my $user = shift;
    my $password = shift;

    $self->write_server_eol("sv.connect");
    $self->write_server_eol($user);
    $self->write_server($password);
    $self->write_server_eot();
}

1;
