package t::TestFoundersVotingsCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;

use Data::Dumper;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Model;
use Bcd::Data::Founders;
use Bcd::Data::FoundersVotings;
use Bcd::Data::NewAntNests;
use Bcd::Constants::NewAntNestsConstants;

sub _add_an_ant_nest_booking{
    my ($self, $code, $index) = @_;
    my $stash = $self->{stash};

    $self->{"code$index"} = $code;
    $self->{"founder_ids$index"} = [];

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $self->{"code$index"}, "test_ant_nest");

    for (1..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){

	Bcd::Data::NewAntNests->insert_a_new_founder
	    ($stash, $self->{"code$index"}, "john$_", "john$_\@mail.com" , 'password');

	  push(@{$self->{"founder_ids$index"}}, $stash->get_last_founder_id());
      }
}


sub set_up{

    my $self = shift;
    my $stash = $self->{stash};


    $self->_add_an_ant_nest_booking(10001, 0);
    $self->_add_an_ant_nest_booking(10002, 1);


}

sub test_create_founder_vote_command{

    my $self = shift;
    my $stash = $self->{stash};

    #I update the state of the ant
    Bcd::Data::Founders->update_founder_state($stash, 
					      $self->{founder_ids1}->[0],
					      Bcd::Constants::FoundersConstants::NO_CHOICE_OF_CANDIDATE);

    my $code = $self->{code1};
    my $session = $self->_connect_to_bcd('john1', $self->{code1}, 'password');
    $self->assert_not_null($session);

    #ok, now I get the command
    my $command = Bcd::Data::Model->instance()->get_command("fv_create_voting");
    $self->assert_not_null($command);

    my $other_person_id = $self->{founder_ids0}->[0];

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line(34);
    $command->parse_line($other_person_id);
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    #I put myself in the right state
    Bcd::Data::Founders->update_founder_state($stash, 
					      $self->{founder_ids1}->[0],
					      Bcd::Constants::FoundersConstants::NO_VOTED_YET);

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_VOTE, $exit_code);

    #Now I change the votes, they are in the same ant nest, but they are not candidates...
    $command->{boss_vote}      = $self->{founder_ids1}->[0];
    $command->{treasurer_vote} = $self->{founder_ids1}->[1];

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_VOTE, $exit_code);

    #now I candidate these ants
    Bcd::Data::Founders->set_founders_flags
	($stash, $self->{founder_ids1}->[0], '1', '0');

    Bcd::Data::Founders->set_founders_flags
	($stash, $self->{founder_ids1}->[1], '0', '1');

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);


    #I should be able to vote only once
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    #Ok, there should be a vote...
    my $votes = Bcd::Data::FoundersVotings->get_votes_result($stash, $self->{code1});

    my $boss_vote = $votes->{bosses};
    $self->assert_equals(1, scalar(@{$boss_vote}));
    $self->assert_equals($self->{founder_ids1}->[0], $boss_vote->[0]->[0]);
    $self->assert_equals(1, $boss_vote->[0]->[1]); #only one vote

    my $treasurer_vote = $votes->{treasurers};
    $self->assert_equals(1, scalar(@{$treasurer_vote}));
    $self->assert_equals($self->{founder_ids1}->[1], $treasurer_vote->[0]->[0]);
    $self->assert_equals(1, $treasurer_vote->[0]->[1]); #only one vote
			 
    
    #####################3
    # I make another vote a null vote
    Bcd::Data::Founders->update_founder_state($stash, 
					      $self->{founder_ids1}->[0],
					      Bcd::Constants::FoundersConstants::NO_VOTED_YET);


    #Now I change the votes, they are in the same ant nest, but they are not candidates...
    $command->{boss_vote}      = "NULL";
    $command->{treasurer_vote} = "NULL";

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);


    #the votes should be also the same
    $votes = Bcd::Data::FoundersVotings->get_votes_result($stash, $self->{code1});

    $boss_vote = $votes->{bosses};
    $self->assert_equals(2, scalar(@{$boss_vote}));
    $self->assert_equals($self->{founder_ids1}->[0], $boss_vote->[0]->[0]);
    $self->assert_equals(1, $boss_vote->[0]->[1]); #only one vote

    $treasurer_vote = $votes->{treasurers};
    $self->assert_equals(2, scalar(@{$treasurer_vote}));
    $self->assert_equals($self->{founder_ids1}->[1], $treasurer_vote->[0]->[0]);
    $self->assert_equals(1, $treasurer_vote->[0]->[1]); #only one vote
    
}
