package t::TestUsersCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use t::TestCommonDb;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::StatementsStash;
use Bcd::Data::Model;
use Bcd::Data::Founders;
use Bcd::Constants::Users;
use Bcd::Commands::CommandParser;
use Cache::FastMmap;
use Data::Dumper;

our @ISA = qw(t::TestCommonDb);


sub test_get_cash_user_euro_summary{
    my $self = shift;
    my $stash = $self->{"stash"};

    #ok, now let's invoke the command
    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("us.get_summary_euro_cash");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $num_of_transactions = scalar(@{$res->{account_summary}}) - 1;


    #I get the account for a user
    my $account_euro = Bcd::Data::Users->get_user_account_e_id($stash, 16);
    #ok, now let's make some transactions...
    my $sql = qq{ insert into transactions(date, description) values(now(), ?)};
    my $insert_tr = $stash->get_connection()->prepare($sql);

    #my $total   = new Math::BigInt->bzero();

    my $amount = new Math::BigInt("1000"); #1000 is 10.00
    my $zero   = new Math::BigInt->bzero();

    my $balance;

    #I test a certain number of transactions
    for (1..10){
	$insert_tr->bind_param(1, "test transaction $_");
	$insert_tr->execute();

	$balance = Bcd::Data::Accounting::Accounts->add_transaction_split
	    ($stash, $account_euro, $amount, $zero);

    }

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $num_of_transactions_after = scalar(@{$res->{account_summary}}) - 1;

    $self->assert_num_equals(10, $num_of_transactions_after - $num_of_transactions);

    #ok, now I see all these transactions...

    my $summary = $res->{account_summary};

    for (0..9){
	my $index = $_+1;
	$self->assert_equals("test transaction $index", $summary->[$_]->{description});
	$self->assert_equals("10.00", $summary->[$_]->{amount_debit});
	$self->assert_equals("0.00", $summary->[$_]->{amount_credit});
	$self->assert_equals(-10.00 * $index, $summary->[$_]->{total});
    }

}

sub test_login_with_another_ant_nest{
    my $self = shift;
    my $stash = $self->{"stash"};
    
    my $command = Bcd::Data::Model->instance()->get_command("sv_connect");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    my $input = {
	user     => "lino",
	ant_nest => "1613498",
	password => "cippo"
    };
    $command->exec_for_test($stash, $input);

    my $exit_code = $command->get_exit_code();

    my $output = $command->get_output();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER, $exit_code);

    my $hash = Bcd::Commands::CommandParser->parse_command_output($output);

    my $session_id = $hash->{"session_id"};
    my $role =  $hash->{"role"};

    $self->assert_null($session_id);
    $self->assert_null($role);

    #now with an invalid post code
    $input->{ant_nest} = "cisiis";
    $command->exec_for_test($stash, $input);

    $exit_code = $command->get_exit_code();
    $output = $command->get_output();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE, $exit_code);

    $hash = Bcd::Commands::CommandParser->parse_command_output($output);

    $session_id = $hash->{"session_id"};
    $role =  $hash->{"role"};

    $self->assert_null($session_id);
    $self->assert_null($role);

}

sub test_grant_role_command{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $command = Bcd::Data::Model->instance()->get_command("sv.grant_role");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line('not valid session');
    $command->parse_line('8012800');
    $command->parse_line('lino');
    $command->parse_line('4');
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code);

    my $bc_root_token = $self->_connect_as_bc_root();
    $self->assert_not_null($bc_root_token);

    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $command->parse_line('8012800');
    $command->parse_line('lino');
    $command->parse_line('45403');
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_CANNOT_GRANT_BC_ROOT_ROLE, $exit_code);

    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $command->parse_line('80128SD');
    $command->parse_line('lino');
    $command->parse_line('4');
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE, $exit_code);

    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $command->parse_line('1613400');
    $command->parse_line('lino');
    $command->parse_line('5');
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_NOT_IMPLEMENTED, $exit_code);
}

sub test_founder_login{
    my $self = shift;
    my $stash = $self->{"stash"};

    #I should add a "fake" founder.

    my $code = 12001;

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "formicaio di prova");

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, "elisa", 'lino@localhost' , 'password');

    #ok, let's try to connect
    my $command = Bcd::Data::Model->instance()->get_command("sv_connect");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    my $input = {
	user         => "elisa",
	ant_nest     => $code,
	password     => "password",
    };
    $command->exec_for_test($stash, $input);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_ACCOUNT_NOT_READY, $exit_code);
    my $output = $command->get_output();


    #ok, now let's confirm this ant
    my $user = Bcd::Data::NewAntNests->select_new_founder_hash_post_nick($stash, $code, "elisa");
    my $id = $user->{id_user};
    Bcd::Data::NewAntNests->new_founder_confirmed($stash, $id);

    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $output = $command->get_output();

    my $hash = Bcd::Commands::CommandParser->parse_command_output($output);

    my $session_id = $hash->{session_id};
    my $role =  $hash->{role};

    $self->assert_not_null($session_id);
    $self->assert_num_equals(Bcd::Data::Users::ROLE_FOR_A_FOUNDER_ANT, $role);

    #ok! I have the cache, I can peek inside it!!!!
    my $value = $stash->get_cache()->get($session_id);
    $self->assert_not_null($value);
    $self->assert_equals('elisa', $value->{user_connected});
    $self->assert_equals(Bcd::Data::Users::ROLE_FOR_A_FOUNDER_ANT, $value->{user_role});
    $self->assert_equals($code, $value->{ant_nest});

    $id = $value->{user_id};
    $self->assert_not_null($id);

    my $data = Bcd::Data::Founders->get_founder_main_data_hash($stash, $id);
    $self->assert_not_null($data);
    $self->assert_equals('elisa', $data->{nick_or_name});
}

sub test_change_totem_command{

    my $self = shift;
    my $stash = $self->{"stash"};

    my $session = $self->_connect_as_a_boss();
    my $command = Bcd::Data::Model->instance()->get_command("us_change_totem");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("new_super_totem");
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now I get the user data
    my $user = Bcd::Data::Users->get_user_base_data_hash($stash, 16);
    $self->assert_equals("new_super_totem", $user->{totem});
}

sub test_change_password_command{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $session = $self->_connect_as_a_boss();
    my $command = Bcd::Data::Model->instance()->get_command("us_change_password");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    my $input = {
	session_id   => $session,
	old_password => "wrongpass",
	new_password => "cippo",
    };

    $command->exec_for_test($stash, $input);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_PASSWORD, $exit_code);

    #Ok, now I try to change the password with the right old password
    $input->{old_password} = "barbarap";

    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #Ok, now I should login with the new password
    $command = Bcd::Data::Model->instance()->get_command("sv_connect");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $input = {
	user     => "barbara",
	ant_nest => "8012898",
	password => "cippo",
    };
    $command->exec_for_test($stash, $input);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
}

sub test_login_command{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $command = Bcd::Data::Model->instance()->get_command("sv_connect");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    my $input = {
	user     => "barbara",
	ant_nest => "8012898",
	password => "barbarap",
    };
    $command->exec_for_test($stash, $input);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $output = $command->get_output();

    my $hash = Bcd::Commands::CommandParser->parse_command_output($output);

    #$self->assert_num_equals(3, $#{$output}+1); #I should have two outputs
    my $session_id = $hash->{"session_id"};
    my $role =  $hash->{"role"};

    $self->assert_not_null($session_id);
    $self->assert_num_equals(3, $role);

    #ok! I have the cache, I can peek inside it!!!!
    my $value = $stash->get_cache()->get($session_id);
    $self->assert_not_null($value);
    $self->assert_equals('barbara', $value->{"user_connected"});
    $self->assert_equals(3, $value->{"user_role"});
    $self->assert_equals(8012898, $value->{"ant_nest"});

    my $id = $value->{user_id};
    $self->assert_not_null($id);

    my $data = Bcd::Data::Users->select_user_data($stash, $id);
    $self->assert_not_null($data);
    $self->assert_equals('barbara', $data->{nick});


    #now I try to disconnect myself
    my $disconnect_command = Bcd::Data::Model->instance()->get_command("sv.disconnect");
    $self->assert_not_null($disconnect_command);

    $input = {
	session_id => $session_id,
    };
    $disconnect_command->exec_for_test($stash, $input);
    $exit_code = $disconnect_command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #after the disconnection the cache should be emptied.
    $value = $stash->get_cache()->get($session_id);
    $self->assert_null($value);

    #I try to disconnect using another session...
    $input = {
	session_id => "not existing session",
    };

    $disconnect_command->exec_for_test($stash, $input);
    $exit_code = $disconnect_command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_NOT_CONNECTED, $exit_code);

    $input = {
	user     => "barbara",
	ant_nest => "8012898",
	password => "cippO",
    };

    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();

    #invalid password
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_PASSWORD, $exit_code);

    $output = $command->get_output();
    $hash = Bcd::Commands::CommandParser->parse_command_output($output);

    $session_id = $hash->{"session_id"};
    $self->assert_null($session_id);
}

sub test_not_existing_user{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $command = Bcd::Data::Model->instance()->get_command("sv_connect");
    $self->assert_not_null($command);

    my $input = {
	user        => "unknown",
	ant_nest    => "8012898",
	password    => "cippo",
    };
    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER, $exit_code);

    my $output = $command->get_output();

    my $hash = Bcd::Commands::CommandParser->parse_command_output($output);
    my $session_id = $hash->{"session_id"};
    $self->assert_null($session_id);
}

sub test_create_normal_user{
    my $self = shift;
    my $stash = $self->{"stash"};
    my $trust = 0.99;
    my $code = 8012898;

    my $command = Bcd::Data::Model->instance()->get_command("an.create_normal_user");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line("session not existing");
    $self->_prepare_command_to_create_a_user($command, $code);
    $command->parse_line($trust);
    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code);

    #now I try to login as a normal user
    my $session_id = $self->_connect_as_a_boss();
    
    #duplicate nick
    $command->start_parsing_command();
    $command->parse_line($session_id);
    $self->_prepare_command_to_create_a_user($command, $code);
    $command->parse_line($trust);
    
    $command->{nick} = "tina";

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_USER_ALREADY_EXISTING, $exit_code);

    #invalid nick
    $command->{nick} = "bc-lino";
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_RESERVED_ID, $exit_code);


    #too short
    $command->{nick} = "si";
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_LENGTH_OR_CHAR_NOT_VALID_IN_ID, $exit_code);

    #too long 
    $command->{nick} = "didiidsofusoifuioweurwoiruweoiu";
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_LENGTH_OR_CHAR_NOT_VALID_IN_ID, $exit_code);
    
    #invalid chars
    $command->{nick} = "cippo 39";
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_LENGTH_OR_CHAR_NOT_VALID_IN_ID, $exit_code);

    #create outside my nest
    $command->{nick} = "test_user";
    $command->{ant_nest} = "1539900";
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_TRYING_TO_CREATE_ANT_OUTSIDE_YOUR_NEST, $exit_code);

    #create a good ant
    $command->{nick} = "test_user";
    $command->{ant_nest} = $code;
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #Ok, now I should parse its output
    my $output = $command->get_output();
    my $res = Bcd::Commands::CommandParser->parse_command_output($output);

    my $token = $res->{token};
    $self->assert_not_null($token);


    #before the confirmation I should test that I can receive the details of
    #this new user
    $command = Bcd::Data::Model->instance()->get_command("an.get_new_user_details");
    $self->assert_not_null($command);
    #I should be able to get the details of a registration...
    $command->parse_line($session_id);
    $command->parse_line("bad tokeniiii");
    $command->eot();
    $command->exec_only_for_test($stash);

    $output = $command->get_output();
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN, $exit_code);

    $command->{token} = $token;
    $command->eot();
    $command->exec_only_for_test($stash);

    $output = $command->get_output();
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    $res = Bcd::Commands::CommandParser->parse_command_output($output);

    #some simple checks to the congruence of the new user
    $self->assert_equals("test_user", $res->{new_user_details}->{nick});
    $self->assert_equals('nick@huhu.it', $res->{new_user_details}->{email});
    $self->assert_equals("Sergio", $res->{new_user_details}->{first_name});
    $self->assert_equals("Conti", $res->{new_user_details}->{last_name});
    $self->assert_equals("Via delle memorie perdute 39", $res->{new_user_details}->{address});
    $self->assert_equals(16, $res->{new_user_details}->{tutor_first});


    #let's try to get the tutors details...
    $command = Bcd::Data::Model->instance()->get_command("an.get_tutors_details");
    $self->assert_not_null($command);
    #I should be able to get the details of a registration...
    $command->parse_line($token);
    $command->eot();
    $command->exec_only_for_test($stash);

    $output = $command->get_output();
    $exit_code = $command->get_exit_code();

    $res = Bcd::Commands::CommandParser->parse_command_output($output);
    $self->assert_num_equals(0, $res->{tutor_2_existing});

    #just a simple check that exist the information of the user.
    $self->assert_equals("test_user", $res->{new_user}->{nick});


    ############################
    ### confirmation of this user

    #ok, now I should test the confirmation of this new user...
    $command = Bcd::Data::Model->instance()->get_command("an.confirm_new_user");
    $self->assert_not_null($command);
    $command->parse_line($session_id);
    $command->parse_line("bad token");
    $command->parse_line(0.39);
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN, $exit_code);

    #ok, now I put the right token, but the tutors are the same
    $command->{token} = $token;
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_THE_TWO_TUTORS_MUST_BE_DIFFERENT, $exit_code);

    
    #ok, now I try to connect as another ant
    my $other_session = $self->_connect_to_bcd("bruno", "1603598", "brunop");
    $command->{session_id} = $other_session;
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_TRYING_TO_CREATE_ANT_OUTSIDE_YOUR_NEST, $exit_code);

    #FOR now I cannot test the indirect path error, because in the test db there are now only
    #ant in the ring 0.
    $other_session = $self->_connect_to_bcd("marco", "8012898", "marcop");
    $command->{session_id} = $other_session;
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #If I repeat the command I should get an error
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_NEW_USER_ALREADY_CONFIRMED, $exit_code);


    #the token should be still valid
    my $new_user_id = Bcd::Data::Users->
	get_new_user_id_from_this_token($stash, $token);

    $self->assert_not_null($new_user_id);

    ###################################################
    ## the tutors details

    $command = Bcd::Data::Model->instance()->get_command("an.get_tutors_details");
    $self->assert_not_null($command);
    #I should be able to get the details of a registration...
    $command->parse_line($token);
    $command->eot();
    $command->exec_only_for_test($stash);

    $output = $command->get_output();
    $exit_code = $command->get_exit_code();

    $res = Bcd::Commands::CommandParser->parse_command_output($output);
    $self->assert_num_equals(1, $res->{tutor_2_existing});

    $self->assert_equals("marco", $res->{tutor_second}->{nick});


    ##################################################
    #ok, now I test the confirmation of my tutors
    $command = Bcd::Data::Model->instance()->get_command("an.confirm_my_tutors");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line("false token");
    $command->parse_line(1.4);
    $command->parse_line(0.8);
    $command->eot();

    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_INVALID_TRUST, $exit_code);

    #change the first trust
    $command->{trust_first_tutor} = 0.7;
    $command->{trust_second_tutor} = 0;
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_INVALID_TRUST, $exit_code);

    $command->{trust_first_tutor} = 0.7;
    $command->{trust_second_tutor} = 0.009;
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN, $exit_code);

    #ok, now I put the right token
    $command->{token} = $token;
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now I should check that there is a direct path between the 
    #tutors and the new user.

    #my ($self, $stash, $u_start, $u_end) = @_;

    my $new_user_id_from_sequence = $stash->get_id_of_last_user();
    $self->assert_num_equals($new_user_id, $new_user_id_from_sequence);

    #barbara is 16, this is fixed during the initial population
    #if this assert fails then I have changed the script...
    my $ans = Bcd::Data::Trust->exists_direct_trust_between
	($stash, $new_user_id, 16);

    $self->assert_num_equals(1, $ans);
    
    $ans = Bcd::Data::Trust->exists_direct_trust_between
	($stash, $new_user_id, 18);

    $self->assert_num_equals(1, $ans);
    
}

sub test_create_initial_users{

    my $self = shift;
    my $stash = $self->{"stash"};

    my $command = Bcd::Data::Model->instance()->get_command("an.create_initial_user");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line("session not existing");
    $self->_prepare_command_to_create_a_user($command, "code");
    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code);

    #I try to add an initial ant to an ant nest which is already completed
    my $bc_root_token = $self->_connect_as_bc_root();
    $self->assert_not_null($bc_root_token);

    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $self->_prepare_command_to_create_a_user($command, "1202000");
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_NO_MORE_INITIAL_ANTS_PLEASE, $exit_code);

    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $self->_prepare_command_to_create_a_user($command, "1eweri");
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE, $exit_code);

    my $create_ant_nest_cmd = Bcd::Data::Model->instance()->get_command("an.create_ant_nest");
    $self->assert_not_null($create_ant_nest_cmd);
    $create_ant_nest_cmd->start_parsing_command();
    $create_ant_nest_cmd->parse_line($bc_root_token);
    #some test parameters
    $create_ant_nest_cmd->parse_line("9900381");
    $create_ant_nest_cmd->parse_line("Formicaio di test");
    $create_ant_nest_cmd->parse_line("very nice totem");
    $create_ant_nest_cmd->parse_line("di fronte al bar");
    $create_ant_nest_cmd->parse_line("mercoledì di test");
    $create_ant_nest_cmd->parse_line(3);
    $create_ant_nest_cmd->eot();
    $create_ant_nest_cmd->exec_only_for_test($stash, 1); #ignore the transaction

    $exit_code = $create_ant_nest_cmd->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);


#now the command should be ok...
    $command->start_parsing_command();
    $command->parse_line($bc_root_token);
    $self->_prepare_command_to_create_a_user($command, "9900381");
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now I should check if the user is effectively created.
    #the id should be the last created.
    my $user_id = $stash->get_id_of_last_user();
    my $hash = Bcd::Data::Users->select_user_data($stash, $user_id);

    #now I should check the data.
    $self->assert_equals($hash->{id_ant_nest}, "9900381");
    $self->assert_equals($hash->{nick}, "test_nick");
    $self->assert_equals($hash->{totem}, "test_nickt");
    $self->assert_equals($hash->{users_state}, Bcd::Constants::Users::NORMAL_ACTIVE_ANT);
    $self->assert_equals($hash->{role_mask}, Bcd::Data::Users::ANT_ROLE);
    $self->assert_equals($hash->{alfa}, 1);
    $self->assert_equals($hash->{bias}, 1);
    $self->assert_equals($hash->{ro}, 1);
    $self->assert_equals($hash->{theta}, 0);
    $self->assert_equals($hash->{first_name}, "Sergio");
    $self->assert_equals($hash->{last_name}, "Conti");
    $self->assert_equals($hash->{address}, "Via delle memorie perdute 39");
    $self->assert_equals($hash->{home_phone}, "010202020");
    $self->assert_equals($hash->{mobile_phone}, "343934929");
    $self->assert_equals($hash->{sex}, "m");
    $self->assert_equals($hash->{birthdate}, "1970-12-29");
    $self->assert_equals($hash->{email}, 'nick@huhu.it');
    $self->assert_equals($hash->{identity_card_id}, 'c.i. 39393939');

    #then I should test the creation of the users accounts...
    my $id_euro = $hash->{id_account_real};
    my $id_tao = $hash->{id_account_local};

    #ok, let's see if these accounts exist.
    my $acc_euro = Bcd::Data::Accounting::AccountsPlan->select_account_hash($stash, $id_euro);
    $self->assert_not_null($acc_euro);

    my $name = sprintf("%s %s %s account", "test_nick", "9900381", "E");
    $self->assert_equals($name, $acc_euro->{description});
    $self->assert_equals(2, $acc_euro->{id_type});
    $self->assert_num_equals(0, Bcd::Data::Accounting::Accounts->get_balance($stash, $id_euro));

    my $acc_tao = Bcd::Data::Accounting::AccountsPlan->select_account_hash( $stash, $id_tao);
    $self->assert_not_null($acc_tao);

    $name = sprintf("%s %s %s account", "test_nick", "9900381", "T");
    $self->assert_equals($name, $acc_tao->{description});
    $self->assert_equals(3, $acc_tao->{id_type});
    $self->assert_num_equals(0, Bcd::Data::Accounting::Accounts->get_balance($stash, $id_tao));

    
    $stash->get_connection()->rollback(); #return to start
}

sub test_get_user_details{
    my $self = shift;
    my $stash = $self->{"stash"};   

    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("an.get_user_details");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("bruno");
    $command->parse_line("brunot");
    $command->eot();
    $command->exec_only_for_test($stash);

    
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST, $exit_code);

    #ok, now I try another user
    $command->{nick} = "ugo";
    $command->{totem} = "uudsodifu";

    $command->eot();
    $command->exec_only_for_test($stash);

    
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_INVALID_TOTEM, $exit_code);


    $command->{totem} = "ugot";

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals("Ugo", $res->{user_details}->{first_name});
    
}

sub test_get_other_ant_profile_with_children{
    my $self = shift;
    my $stash = $self->{stash};
    my $session = $self->_connect_to_bcd("sonia", 8012898, "soniap");

    my $command = Bcd::Data::Model->instance()->get_command("us.get_other_ant_profile");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line(19);
    $command->eot();
    $command->exec_only_for_test($stash);
    
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #then I should check the answer
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #ugo has three children
    $self->assert_equals(3, scalar(@{$res->{his_children}}));
    
}

sub test_get_other_ant_profile{
    my $self = shift;
    my $stash = $self->{stash};

    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("us.get_other_ant_profile");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line(2);
    $command->eot();
    $command->exec_only_for_test($stash);

    
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST, $exit_code);

    $command->{id_other_user} = 21;
    
    $command->eot();
    $command->exec_only_for_test($stash);

    
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #then I should check the answer
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    $self->assert_equals(1, $res->{other_ant}->{direct_trust});
    $self->assert_equals("Massa", $res->{personal_data}->{last_name});
    $self->assert_equals("barbara", $res->{other_ant}->{nick_first_tutor});
    $self->assert_equals("ugo", $res->{other_ant}->{nick_second_tutor});
    $self->assert_equals(0, scalar(@{$res->{his_children}}));

    #from marco to sonia there is not a direct trust AND marco is not a boss
    my $session_marco = $self->_connect_to_bcd("marco", 8012898, "marcop");

    $command->{session_id} = $session_marco;
    $command->{id_other_user} = 26;
    
    $command->eot();
    $command->exec_only_for_test($stash);
    
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #then I should check the answer
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals(1, $res->{no_personal_trusts});

    #ok, now I add the personal trusts for sonia.
    #sonia trusts marco 0,000405
    Bcd::Data::PersonalUsersData->insert_personal_users_data_trusts
	($stash, 26, 1e-5, 1e-5, 1e-3, 1e-3, 1e-3, 1e-2, 1e-2, 1e-7);

    $command->eot();
    $command->exec_only_for_test($stash);
    
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #then I should check the answer
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals(0, $res->{no_personal_trusts});

    #I can see the first name, last name and email
    $self->assert_equals("Sonia", $res->{personal_data}->{first_name});
    $self->assert_equals("Bianco", $res->{personal_data}->{last_name});
    $self->assert_equals('sonia@localhost', $res->{personal_data}->{email});
    #the other data is masked...
    $self->assert_null($res->{personal_data}->{home_phone});
    $self->assert_null($res->{personal_data}->{mobile_phone});
    $self->assert_null($res->{personal_data}->{address});
    $self->assert_null($res->{personal_data}->{sex});
    $self->assert_null($res->{personal_data}->{birthdate});

    #I connect as a boss, there is not direct trust, but I can see all, because I am the boss...
    $command->{session_id} = $session;
    $command->{id_other_user} = 26;
    
    $command->eot();
    $command->exec_only_for_test($stash);
    
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #then I should check the answer
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #the boss can see all.
    $self->assert_equals("Sonia", $res->{personal_data}->{first_name});
    $self->assert_equals("Bianco", $res->{personal_data}->{last_name});
    $self->assert_equals('sonia@localhost', $res->{personal_data}->{email});
    $self->assert_equals("0815349499", $res->{personal_data}->{home_phone});
    $self->assert_equals("3949499333", $res->{personal_data}->{mobile_phone});
    $self->assert_equals("Via Scarlatti 12", $res->{personal_data}->{address});
    $self->assert_equals("f", $res->{personal_data}->{sex});
    $self->assert_equals("1993-09-20", $res->{personal_data}->{birthdate});
}



sub test_update_personal_data_command{
    my $self = shift;
    my $stash = $self->{"stash"};   


    #ok, first of all I should connect to the ant nest
    my $session = $self->_connect_to_bcd("barbara", 8012898, "barbarap");
    $self->assert_not_null($session);

    #ok, now I change the personal data....
    my %cmd = (
	    first_name => "Albert",
	    last_name => "Einstein",
	    address => "Relative street",
	    home_phone => '88',
	    mobile_phone => '11',
	    sex => "m",
	    birthdate => "1880-12-20",
	    email => 'aeinstein@emc2.com',
	    identity_card_id => 'emc2',
	    );

    #ok, I update the barbara personal data....

    my $command = Bcd::Data::Model->instance()->get_command("us_update_personal_data");
    $self->assert_not_null($command);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line($cmd{first_name});
    $command->parse_line($cmd{last_name});
    $command->parse_line($cmd{address});
    $command->parse_line($cmd{home_phone});
    $command->parse_line($cmd{mobile_phone});
    $command->parse_line($cmd{sex});
    $command->parse_line($cmd{birthdate});
    $command->parse_line($cmd{email});
    $command->parse_line($cmd{identity_card_id});
    
    $command->eot();


    $command->exec_only_for_test($stash);

    
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals
	(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #then I get the personal data...

    #I KNOW the id of barbara...
    my $sql = qq{select * from personal_users_data where id = 16};
    my $st = $stash->get_connection()->prepare( $sql );
    $st->execute();

    my $data = $st->fetchrow_hashref();
    $st->finish();

    foreach (keys(%cmd)){
	$self->assert_equals($cmd{$_}, $data->{$_});
    }
}


sub _prepare_command_to_create_a_user{
    my ($self, $command, $an_code) = @_;

    $command->parse_line($an_code);
    $command->parse_line("test_nick");
    $command->parse_line('nick@huhu.it');
    $command->parse_line("Sergio");
    $command->parse_line("Conti");
    $command->parse_line("Via delle memorie perdute 39");
    $command->parse_line("010202020");
    $command->parse_line("343934929");
    $command->parse_line("m");
    $command->parse_line("1970-12-29");
    $command->parse_line("c.i. 39393939");
}
