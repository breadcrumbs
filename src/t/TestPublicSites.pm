package t::TestPublicSites;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Ads;
use Data::Dumper;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Model;

sub test_get_public_sites_ds{
    my $self = shift;
    my $stash= $self->{stash};

    my $ds = Bcd::Data::Sites->get_public_sites_ant_nest_ds($stash, 8012898);
    $self->assert_equals(2, scalar(@{$ds}));
}

sub test_get_public_sites_cmd{
    my $self = shift;
    my $stash= $self->{stash};

    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    $self->assert_not_null($session);

    #ok, let's try the command
    my $command = Bcd::Data::Model->instance()->get_command("an_get_public_sites");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_num_equals(1, scalar(@{$res->{public_sites}}));
    $self->assert_equals('a name', $res->{public_sites}->[0]->{name});
}

sub test_get_public_site_details_cmd{
    my $self = shift;
    my $stash= $self->{stash};

    #I have the command which will add a public site presence
    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    $self->assert_not_null($session);

    #ok, let's try the command
    my $command = Bcd::Data::Model->instance()->get_command("an.get_site_details");
    $self->assert_not_null($command);
    
    my $input = {
	session_id => $session,
	id_site    => 5,
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_SITE_OUTSIDE_YOUR_NEST, $exit_code);

    #ok, now I try to see my site
    $input->{id_site} = 6;
    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $site_details = $res->{site_details};
    $self->assert_equals("bar nella FNAC di via Luca Giordano", $site_details->{location});

}

sub test_edit_presence_space{
    my $self = shift;
    my $stash= $self->{stash};

    my $sun_pres = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 1, 'sunday morning');

    my $presence = Bcd::Data::PublicSites->get_presence_decoded_hash($stash, $sun_pres);

    $self->assert_equals("special_site", $presence->{presence_type});

    #ok, now I convert this special site presence in a "normal" site presence
    #I create a site.
    my $id_site = Bcd::Data::Sites->create_a_site
	($stash, "test site", undef, undef, undef, undef);

    Bcd::Data::PublicSites->update_presence_space($stash, $sun_pres, $id_site, undef);

    #then I get the presence
    $presence = Bcd::Data::PublicSites->get_presence_decoded_hash($stash, $sun_pres);

    #Ok, I have changed the presence...
    $self->assert_equals("an_point", $presence->{presence_type});
    $self->assert_equals("test site", $presence->{site_name});

    #ok, now I test the same thing with the command, it should work the same
    my $session = $self->_connect_as_sonia();
    my $command = Bcd::Data::Model->instance()->get_command("ws.change_site_in_presence");
    $self->assert_not_null($command);
    
    my $input = {
	session_id      => $session,
	id_locus        => $sun_pres,
	id_site_new     => 'NULL',
	id_special_site => 'your_home',
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, then I get back the presence
    $presence = Bcd::Data::PublicSites->get_presence_decoded_hash($stash, $sun_pres);

    #the presence is changed
    $self->assert_equals("special_site", $presence->{presence_type});
    $self->assert_equals(1, $presence->{special_site});
    
}

sub test_edit_presence{
    my $self = shift;
    my $stash= $self->{stash};

    my $sun_pres = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 1, 'sunday morning');

    Bcd::Data::PublicSites->update_presence($stash, $sun_pres, 0, 'sunday morning 10-11am');

    my $presence = Bcd::Data::PublicSites->get_presence_hash($stash, $sun_pres);
    $self->assert_equals(0,                        $presence->{on_request});
    $self->assert_equals('sunday morning 10-11am', $presence->{presence});

    #ok, now I test the same thing also with the command
    my $session = $self->_connect_as_sonia();
    my $command = Bcd::Data::Model->instance()->get_command("ws.edit_user_presence");
    $self->assert_not_null($command);
    
    my $input = {
	session_id      => $session,
	id_locus        => $sun_pres,
	on_request      => '1',
	presence_text   => 'sunday morning 11-11.30am',
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    $presence = Bcd::Data::PublicSites->get_presence_hash($stash, $sun_pres);
    $self->assert_equals(1,                           $presence->{on_request});
    $self->assert_equals('sunday morning 11-11.30am', $presence->{presence});
}

sub test_get_similar_presences{
    my $self = shift;
    my $stash= $self->{stash};

    Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 1, 'sunday morning');

    Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 0, 'monday afternoon');

    my $presence_id = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 0, 'tuesday evening');

    #ok, I can now try to get the two similar presences to this one
    my $ds = Bcd::Data::PublicSites->get_presences_similar_in_space_ds
	($stash, $presence_id, 26, undef, 0, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE);

    $self->assert_equals(3, scalar(@{$ds}));
    $self->assert_equals("sunday morning",   $ds->[1]->[1]);
    $self->assert_equals("monday afternoon", $ds->[2]->[1]);

    ###### now I test the command
    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    my $command = Bcd::Data::Model->instance()->get_command("si.get_similar_presences");
    
    my $input = {
	session_id      => $session,
	id_locus        => $presence_id,
	similarity_type => 'space',
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $sim_pres = $res->{similar_presences};
    $self->assert_equals(2, scalar(@{$sim_pres}));
    $self->assert_equals("sunday morning",   $sim_pres->[0]->{presence});
    $self->assert_equals("monday afternoon", $sim_pres->[1]->{presence});

    #ok, now I test some similar presences in a normal site...

    my $id_site = Bcd::Data::Sites->create_a_site
	($stash, "test name for summary", undef, undef, undef, undef);
   
    #I add a presence in this site...
    Bcd::Data::PublicSites->add_presence_in_site
	($stash, 26, $id_site, 
	 Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 1, 
	 'monday1');
   
    #I add a presence in this site...
    my $tuesday_presence = Bcd::Data::PublicSites->add_presence_in_site
	($stash, 26, $id_site, 
	 Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 1, 
	 'tuesday2');

   
    #I add a presence in this site...
    $presence_id = Bcd::Data::PublicSites->add_presence_in_site
	($stash, 26, $id_site, 
	 Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 1, 
	 'wednesday3');

    $ds = Bcd::Data::PublicSites->get_presences_similar_in_space_ds
	($stash, $presence_id, 26, $id_site, undef, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE);

    $self->assert_equals(3, scalar(@{$ds}));
    $self->assert_equals("monday1",   $ds->[1]->[1]);
    $self->assert_equals("tuesday2",  $ds->[2]->[1]);

    $input->{id_locus} = $presence_id;

    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $sim_pres = $res->{similar_presences};
    $self->assert_equals(2, scalar(@{$sim_pres}));
    $self->assert_equals("monday1",   $sim_pres->[0]->{presence});
    $self->assert_equals("tuesday2",  $sim_pres->[1]->{presence});

    #ok, now I try to delete a presence...
    Bcd::Data::PublicSites->delete_presence($stash, $tuesday_presence);

    #now I should only have one similar presence in output
    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $sim_pres = $res->{similar_presences};
    $self->assert_equals(1, scalar(@{$sim_pres}));
    $self->assert_equals("monday1",   $sim_pres->[0]->{presence});
    
}

sub test_get_user_summary_presences_cmd{
    my $self = shift;
    my $stash= $self->{stash};

    #I have the command which will add a public site presence
    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    $self->assert_not_null($session);

    #ok, let's try the command
    my $command = Bcd::Data::Model->instance()->get_command("si.get_user_presences_summary");
    $self->assert_not_null($command);

    my $input = {
	session_id => $session,
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #ok, I should have three empty arrays
    $self->assert_equals(0, scalar(@{$res->{object_presences}}));
    $self->assert_equals(0, scalar(@{$res->{homemade_presences}}));
    $self->assert_equals(0, scalar(@{$res->{service_presences}}));

    #ok, now I try to insert a presence
    
    #a special site presence
    ##my ($class, $stash, $user, $id_special_site, $reason, $on_request, $available_hours) = @_;
    Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 1, 'sunday morning');
    
    $command->exec_for_test($stash, $input);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    
    #print Dumper($res);
    my $service_presences = $res->{service_presences};
    $self->assert_equals(1, scalar(@{$service_presences}));
    my $presence = $service_presences->[0];

    $self->assert_equals(0, $presence->{special_site});
    $self->assert_equals(1, $presence->{on_request});
    $self->assert_equals("sunday morning", $presence->{presence});

    #ok, now I add a "presence in a public site";
    my $id_site = Bcd::Data::Sites->create_a_site
	($stash, "test name for summary", undef, undef, undef, undef);
   
    #I add a presence in this site...
    Bcd::Data::PublicSites->add_presence_in_site
	($stash, 26, $id_site, 
	 Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, 1, 
	 'monday afternoon');

    $command->exec_for_test($stash, $input);
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());


    $service_presences = $res->{service_presences};
    $self->assert_equals(2, scalar(@{$service_presences}));
    $presence = $service_presences->[0];

    $self->assert_equals(0, $presence->{special_site});
    $self->assert_equals(1, $presence->{on_request});
    $self->assert_equals("sunday morning", $presence->{presence});
    $self->assert_null($presence->{id_site});
    #no ads for this locus
    $self->assert_equals(0, $presence->{ads_count});

    $presence = $service_presences->[1];
    $self->assert_null($presence->{special_site});
    $self->assert_equals(1, $presence->{on_request});
    $self->assert_equals("monday afternoon", $presence->{presence});
    $self->assert_equals("test name for summary", $presence->{name});
    $self->assert_equals(0, $presence->{ads_count});
}


sub test_create_public_site_presence_cmd{
    my $self = shift;
    my $stash= $self->{stash};

    #I have the command which will add a public site presence
    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    $self->assert_not_null($session);

    #ok, let's try the command
    my $command = Bcd::Data::Model->instance()->get_command("si.create_presence");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("NULL");
    $command->parse_line("my_home");
    $command->parse_line('service');
    $command->parse_line('1'); 
    $command->parse_line('every thursday 9-10 am'); 
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now I try to get the presences for this ant
    my $ds = Bcd::Data::PublicSites->get_this_ant_presences_in_public_sites_ds
	($stash, 26);
    
    $self->assert_equals(1, scalar(@{$ds}));

    $ds = Bcd::Data::PublicSites->get_availability_in_special_site_ds
	($stash, 26, 0);

    $self->assert_equals(2, scalar(@{$ds}));

    my $presence = $ds->[1];

    $self->assert_equals(Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE, $presence->[1]);
    $self->assert_equals('1', $presence->[2]);
    $self->assert_equals('every thursday 9-10 am', $presence->[3]);

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("NULL");
    $command->parse_line("your_home");
    $command->parse_line('object');
    $command->parse_line('0'); 
    $command->parse_line('every monday 9-10 am'); 
    $command->eot();

    $command->exec_only_for_test($stash);

    $ds = Bcd::Data::PublicSites->get_availability_in_special_site_ds
	($stash, 26, 1);

    $self->assert_equals(2, scalar(@{$ds}));

    $presence = $ds->[1];
    $self->assert_equals(Bcd::Constants::PublicSitesConstants::OBJECT_SELL_PRESENCE, $presence->[1]);
    $self->assert_equals('0', $presence->[2]);
    $self->assert_equals('every monday 9-10 am', $presence->[3]);

    #let's try the list of presences
    $ds = Bcd::Data::PublicSites->get_presences_user_ds
	($stash, undef, 1, 26, 3);

    $self->assert_equals(2, scalar(@{$ds}));
    $self->assert_equals('every monday 9-10 am', $ds->[1]->[2]);

    $command->{id_special_site} = "ocioueoif";
    $command->eot();

    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_SPECIAL_SITE_SPECIFICATION, $exit_code);

    $command->{id_special_site} = "my_home";
    $command->{id_site} = "1";
    $command->eot();

    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_SITE_OUTSIDE_YOUR_NEST, $exit_code);

    $command->{id_site} = "6";
    $command->{presence} = "a new presence";
    $command->eot();

    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    $ds = Bcd::Data::PublicSites->get_presences_user_ds
	($stash, 6, undef, 26, 3);

    $self->assert_equals(2, scalar(@{$ds}));
    $self->assert_equals('a new presence', $ds->[1]->[2]);

    #ok, now I try the command to get the presences
    $command = Bcd::Data::Model->instance()->get_command("si.get_presences");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("NULL");
    $command->parse_line("my_home");
    $command->parse_line('service');
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();    

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals(1, scalar(@{$res->{presences}}));
    $self->assert_equals("every thursday 9-10 am", $res->{presences}->[0]->{presence});

    #ok, now I should try to get another dataset
    $command->start_parsing_command;
    $command->parse_line($session);
    $command->parse_line("6");
    $command->parse_line("NULL");
    $command->parse_line('object');
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();    

    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    $self->assert_equals("a new presence", $res->{presences}->[0]->{presence});
}
