package t::TestAntNests;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;
use DBI;
use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::Model;
use Bcd::Data::AntNests;
use Bcd::Data::StatementsStash;
use Data::Dumper;

sub set_up{
    my $self = shift;
    my $stash = $self->{stash};

    my $count_before = $self->get_ant_nests_count();

    #I have also the initial ant nests...
    Bcd::Data::AntNests->create_a_ant_nest($stash, 8402000, "Colliano", "valle del Sele", 4);

    #ok, now let's see if exists the ant nest in the db
    my $count_after = $self->get_ant_nests_count();

    #I should have another ant nest here...
    $self->assert_num_equals($count_before, $count_after-1);
}

sub test_create_ant_nest_from_booking{
    my $self = shift;
    my $stash = $self->{stash};

    my $assigned_code = Bcd::Data::AntNests->create_ant_nest_from_booking($stash, 84020, "colliano1");
    $self->assert_equals(8402001, $assigned_code);

    my $detail = Bcd::Data::AntNests->get_ant_nest_detail($stash, $assigned_code);

    $self->assert_equals(84020_01, $detail->[0]);
    $self->assert_equals(Bcd::Constants::AntNestsConstants::CREATION_STATE, $detail->[1]);
    $self->assert_equals("colliano1", $detail->[3]);
    $self->assert_null($detail->[4]);
    $self->assert_null($detail->[5]);
    $self->assert_null($detail->[6]);
    
}

sub test_get_assigned_code{
    my $self = shift;
    my $stash = $self->{stash};
    
    #suppose I want to create another ant nest in the same post code
    my $assigned_code = Bcd::Data::AntNests->_get_assigned_code($stash, 84020);
    $self->assert_equals(8402001, $assigned_code);

    $assigned_code = Bcd::Data::AntNests->_get_assigned_code($stash, 16134);
    $self->assert_equals("1613402", $assigned_code);

}



sub test_get_detail{
    my $self = shift;
    my $stash = $self->{stash};

    my $detail = Bcd::Data::AntNests->get_ant_nest_detail($stash, 8402000);

    $self->assert_equals(84020_00, $detail->[0]);
    $self->assert_equals(Bcd::Constants::AntNestsConstants::CREATION_STATE, $detail->[1]);

    $self->assert_equals("Colliano", $detail->[3]);
    $self->assert_equals("valle del Sele", $detail->[4]);
    #$self->assert_equals("municipio", $detail->[5]);
    #$self->assert_equals("lunedì 9-12", $detail->[6]);

}


#I try to create an ant nest...
sub test_create_ant_nest{
    my $self = shift;
    my $stash = $self->{"stash"};
    


    #now I should also check the temporary table
    my $sql = qq{select * from ant_nests_beginnings where id=?};
    my $beg_st = $stash->get_connection()->prepare($sql);

    $beg_st->bind_param(1, 8402000);
    $beg_st->execute();
    
    my $code;
    my $remaining_ants;
    my $miss_boss;
    my $miss_tres;
    $beg_st->bind_columns(undef, \$code, \$remaining_ants, \$miss_boss, \$miss_tres);

    $beg_st->fetch();
    
    $self->assert_equals($code, 8402000);
    $self->assert_equals($remaining_ants, 4);
    $self->assert_equals($miss_boss, '1');
    $self->assert_equals($miss_tres, '1');

    $beg_st->finish();

    #ok, let's see if also the code to get the remaining ants is ok
    $remaining_ants = Bcd::Data::AntNests->get_initial_ants_remaining($stash, "8402000");
    $self->assert_num_equals(4, $remaining_ants);

    #I fake a creation of an initial ant
    my $test_code = "8402000";
    $self->_create_a_fake_ant($test_code);
    Bcd::Data::AntNests->a_new_initial_ant_has_been_created($stash, "8402000");
    $remaining_ants = Bcd::Data::AntNests->get_initial_ants_remaining($stash, "8402000");
    $self->assert_num_equals(3, $remaining_ants);

    #I fake the creation of other initial ants
    $self->_create_a_fake_ant($test_code);
    Bcd::Data::AntNests->a_new_initial_ant_has_been_created($stash, "8402000");
    $self->_create_a_fake_ant($test_code);
    Bcd::Data::AntNests->a_new_initial_ant_has_been_created($stash, "8402000");    
    $self->_create_a_fake_ant($test_code);
    Bcd::Data::AntNests->a_new_initial_ant_has_been_created($stash, "8402000");

    $remaining_ants = Bcd::Data::AntNests->get_initial_ants_remaining($stash, "8402000");
    $self->assert_num_equals(0, $remaining_ants);

    #ok, now I should test the making of the geometry...
    my $home_ants = Bcd::Data::Users->get_all_ants_in_this_ant_nest($stash, $test_code);

    $sql = qq{select trust_u1_u2, trust_u2_u1 from trusts where u1=? and u2=?};
    my $trust_st = $stash->get_connection()->prepare($sql);
    
    my $max_index = $#{$home_ants};
    for (0..$max_index){
	my $pivot_ant = $home_ants->[$_]->[0];
	for (($_+1)..$max_index){
	    my $second_ant = $home_ants->[$_]->[0];
	    #ok, now let's see the trust
	    
	    $trust_st->bind_param(1, $pivot_ant);
	    $trust_st->bind_param(2, $second_ant);
	    $trust_st->execute();
	    my $row = $trust_st->fetchrow_arrayref();
	    $trust_st->finish();

	    $self->assert_num_equals(0.99, $row->[0]);
	    $self->assert_num_equals(0.99, $row->[1]);

	}
    }


    #ok, now I fake the grant of the role for the boss and the treasurer
    Bcd::Data::AntNests->new_starting_boss($stash, "8402000");

    #the boss should be not missing now
    $beg_st->bind_param(1, 8402000);
    $beg_st->execute();
    my $arr = $beg_st->fetchrow_arrayref();
    $self->assert_equals($arr->[2], '0');
    $beg_st->finish();

    Bcd::Data::AntNests->new_starting_treasurer($stash, "8402000");

    #the state should be now TESTING
    $sql = qq{select state_id from ant_nests where id=?};
    my $state_st = $stash->get_connection()->prepare($sql);
    $state_st->bind_param(1, "8402000");
    $state_st->execute();
    
    $arr = $state_st->fetchrow_arrayref();
    $self->assert_num_equals(Bcd::Constants::AntNestsConstants::TESTING_STATE, $arr->[0]);

    #and the line in the beginnings table should be deleted
    $beg_st->bind_param(1, 8402000);
    $beg_st->execute();
    $arr = $beg_st->fetchrow_arrayref();
    $self->assert_null($arr);
    $beg_st->finish();

    $stash->get_connection()->rollback();
}


sub _create_a_fake_ant{
    my ($self, $ant_nest_code) = @_;

    my $personal_data_id = Bcd::Data::Users->create_personal_data_for_user($self->{stash}, {});

    my $sql = qq{insert into users(id_ant_nest, nick, users_state, id_personal_data) values (?, 'test nick', 1, ?)};
    my $st = $self->{stash}->get_connection()->prepare($sql);
    $st->bind_param(1, $ant_nest_code);
    $st->bind_param(2, $personal_data_id);
    
    $st->execute();
}

sub test_remaining_ants_for_a_not_existing_code{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $remaining_ants = Bcd::Data::AntNests->get_initial_ants_remaining($stash, "8402001");
    $self->assert_num_equals(0, $remaining_ants);

}


sub get_ant_nests_count{
   my $self = shift;
   my $stash = $self->{"stash"};

   my $sql = qq{select count(*) from ant_nests};
   my $count_st = $stash->get_connection()->prepare($sql);

   $count_st->execute();
   my $count;
   $count_st->bind_columns(undef, \$count);
   $count_st->fetch();

   return $count;
}


sub test_get_from_post_code{
    my $self = shift;
    my $stash = $self->{"stash"};

    my $sql = qq{insert into ant_nests(id, state_id, name) values(?, 1, 'name')};
    my $st = $self->{stash}->get_connection()->prepare($sql);

    my @test_codes = (12345_900, 12345_901, 12345_902, 12345_910);

    for (@test_codes){
	$st->bind_param(1, $_);
	$st->execute();
    }


    my $array = Bcd::Data::AntNests->get_ant_nests_from_post_code($stash, 12345_900, 12345_901);
    my $count = Bcd::Data::AntNests->get_count_ant_nests_min_max ($stash,  12345_900, 12345_901);
    $self->assert_num_equals(scalar(@{$array}), 3);
    $self->assert_num_equals(2, $count);

    $array = Bcd::Data::AntNests->get_ant_nests_from_post_code($stash, 12345_900, 12345_999);
    $count = Bcd::Data::AntNests->get_count_ant_nests_min_max ($stash, 12345_900, 12345_999);
    $self->assert_num_equals(scalar(@{$array}), 5);
    $self->assert_num_equals(4, $count);

    $array = Bcd::Data::AntNests->get_ant_nests_from_post_code($stash, 12345_903, 12345_904);
    $count = Bcd::Data::AntNests->get_count_ant_nests_min_max ($stash, 12345_903, 12345_904);
    $self->assert_num_equals(scalar(@{$array}), 1);
    $self->assert_num_equals(0, $count);

    #this test will fail in the case that in the db there are ant nests in this range
    my $res = Bcd::Data::AntNests->get_near_ant_nests($stash, 123459);
    #print Dumper($res);
    $self->assert_equals(5, scalar(@{$res}));

}

# sub test_get_near_ant_nests{
#     my $self = shift;
#     my $stash = $self->{stash};

#     my $res = Bcd::Data::AntNests->get_near_ant_nests($stash, 16123);
    
#     $self->assert_equals(3, scalar(@{$res}));

# }


# sub test_count_from_post_code{

#     my $self = shift;
#     my $stash = $self->{"stash"};

#     my $count = Bcd::Data::AntNests->get_count_ant_nests_min_max($stash, 1613400, 1613499);
#     $self->assert_num_equals($count, 2);

#     $count = Bcd::Data::AntNests->get_count_ant_nests_min_max($stash, 1410000, 1410099);
#     $self->assert_num_equals($count, 1);

#     $count = Bcd::Data::AntNests->get_count_ant_nests_min_max($stash, 1000000, 1999999);
#     $self->assert_num_equals($count, 4);

#     $count = Bcd::Data::AntNests->get_count_ant_nests_min_max($stash, 3000000, 3999999);
#     $self->assert_num_equals($count, 0);
# }

sub test_init_db{
    my $self = shift;
    my $stash = $self->{"stash"};

    local $stash->get_connection()->{PrintError};  # localize and turn off for this block

    eval {
	Bcd::Data::AntNests->init_db($stash);
      };

    #I should have this message, you should have the db inited before the tests
    $self->assert($@ =~ /duplicate key/); 
}
