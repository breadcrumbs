package t::TestDepositCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Errors::ErrorCodes;
use Bcd::Data::StatementsStash;
use Bcd::Commands::CommandFactory;
use Bcd::Data::Model;

use Data::Dumper;

sub test_delete_deposit_request{
    my $self = shift;
    my $stash = $self->{stash};

    #first of all I try to play the command without a request
    my $session = $self->_connect_as_a_boss();
    my $command = Bcd::Data::Model->instance()->get_command("bk.delete_my_booking");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNKNOWN_DEPOSIT, $exit_code);

    #ok, now I try to make a deposit request...
    my ($booking_token, $blinded_token) = 
	Bcd::Data::Deposits->create_deposit_booking($stash, 16,  '333');

    
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #NO booking now
    my $booking = Bcd::Data::Deposits->get_pending_booking_from_user_id_arr($stash, 16);
    $self->assert_null($booking);

    #The command cannot be repeated, because the request has gone!
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNKNOWN_DEPOSIT, $exit_code);

}


sub test_simple_deposit{
    my $self = shift;
    my $stash = $self->{stash};

    #first of all I try to connect
    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("bk.create_deposit_booking");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line('didfj');
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();


    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_CURRENCY, $exit_code);

    
    #now an amount too small
    $command->{amount} = "0.02";
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_DEPOSIT_TOO_SMALL, $exit_code);


    #AMOUNT TOO BIG
    $command->{amount} = "25.01";
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_DEPOSIT_TOO_BIG, $exit_code);


    $command->{amount} = "4.31";
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);


    #If I now repeat the booking the answer should be pending request...
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_PENDING_BOOKING_PRESENT, $exit_code);

    #ok, now I try to get the summary, I should get this booking in the summary...
    $session = $self->_connect_to_bcd("tina", 8012898, "tinap");

    $command = Bcd::Data::Model->instance()->get_command("an.get_summary_bookings");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    $self->assert_equals("4.31", $res->{total_deposits});
    $self->assert_equals("4.31", $res->{deposits_summary}->[0]->{amount});
}

sub test_claim_the_ticket{
    my $self = shift;
    my $stash = $self->{stash};

    #ok, now I simulate a deposit booking
    my ($booking_token, $blinded_token) = 
	Bcd::Data::Deposits->create_deposit_booking($stash, 16,  "2932");

    #then the treasuer acknowledges this booking
    my $session = $self->_connect_to_bcd("tina", 8012898, "tinap");
    my $command = Bcd::Data::Model->instance()->get_command("bk.lookup_deposit");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($booking_token);
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    
    my $res_total = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    my $res = $res_total->{booking_details};

    $command = Bcd::Data::Model->instance()->get_command("bk.treasurer_acknowledged");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($res->{id});
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #Ok, now I am able to test the claim of this ticket
    my $session_ugo = $self->_connect_to_bcd("ugo", 8012898, "ugop");

    $command = Bcd::Data::Model->instance()->get_command("bk.claim_deposit");
    $self->assert_not_null($command);
    $command->parse_line($session_ugo);
    $command->parse_line("invalid secret");
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNKNOWN_DEPOSIT, $exit_code);

    #ok, now I put the right booking
#     $command->{booking_token} = $res->{booking_token};

#     $command->eot();
#     $command->exec_only_for_test($stash);

#     $exit_code = $command->get_exit_code();
#     $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code);

    #ok, now I connect as barbara
    my $session_boss = $self->_connect_as_a_boss();

    $command->{session_id} = $session_boss;
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_SECRET_TOKEN, $exit_code);

    #Ok, let's now have the valid secret token
    
    #the secret token is obtained by the full and the blinded token
    my $secret_token = Bcd::Data::Token::unblind_the_token
	($res->{full_token}, $blinded_token);

    #print Dumper($res);
    #print "The secret token is $secret_token, the blinded was $blinded_token\n";

    $command->{secret_token} = $secret_token;
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now I should parse it
    $res_total = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals("29.18", $res_total->{amount_deposited});
    $self->assert_equals("29.18", $res_total->{new_total});
    #print Dumper($res_total);
    
    #now barbara wants to claim the ticket two times...
    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNKNOWN_DEPOSIT, $exit_code);

    ########################### I cannot lookup another time
    $session = $self->_connect_to_bcd("tina", 8012898, "tinap");
    $command = Bcd::Data::Model->instance()->get_command("bk.lookup_deposit");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($booking_token);
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_THIS_BOOKING_HAS_BEEN_COLLECTED, $exit_code);

}

sub test_withdrawal_claim{
    my $self = shift;
    my $stash = $self->{stash};

    my $session = $self->_connect_as_a_boss();

    #ok, I have made a deposit...
    my ($amount_deposited, $new_total) = Bcd::Data::Bank->deposit_user_account
	($stash, 16, 8012898, "938");

    #now I make a withdrawal request, 30 cents
    my ($booking_token, $full_token) = 
	Bcd::Data::Deposits->create_withdrawal_booking($stash, 16,  "30");

    #print "booking $booking_token full $full_token\n";

    #ok, now I try to claim this this

    $session = $self->_connect_to_bcd("tina", 8012898, "tinap");

    my $command = Bcd::Data::Model->instance()->get_command("bk.lookup_withdrawal");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($booking_token);
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $booking_id = $res->{booking_details}->{id};

    #ok, I can now claim this ticket
    my $secret_token = Bcd::Data::Token::unblind_the_token
	($full_token, $res->{booking_details}->{full_token});

    $command = Bcd::Data::Model->instance()->get_command("bk.claim_withdrawal");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($booking_id);
    $command->parse_line($secret_token);
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now I test that the user account has been credited
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);

    #ok, let's see the balance of this poor user...
    my $account_euro = Bcd::Data::Users->get_user_account_e_id($stash, 16);
    my $balance = Bcd::Data::Accounting::Accounts->get_balance($stash, $account_euro);

    $self->assert_equals($new_total-30, $balance);

}

sub test_withdrawal_request{
    my $self = shift;
    my $stash = $self->{stash};

    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("bk.lookup_my_booking");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_NO_DATA, $exit_code);

    #ok, now I will try to make a withdrawal, I should fail, because,
    #the account will go negative

    my ($amount_deposited, $new_total) = Bcd::Data::Bank->deposit_user_account
	($stash, 16, 8012898, "938");


    #ok, now I try to make a withdrawal > than the amount in the account

    $command = Bcd::Data::Model->instance()->get_command("bk.create_withdrawal_booking");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("9.38");
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_WILL_RENDER_NEGATIVE_THE_ACCOUNT, $exit_code);

    $command->{amount} = "9.33";

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    my $booking_token = $res->{booking_token};

    #ok, another time I should fail
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_PENDING_BOOKING_PRESENT, $exit_code);

    $command = Bcd::Data::Model->instance()->get_command("bk.lookup_my_booking");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals($booking_token, $res->{booking_details}->{booking_token});
    $self->assert_equals(0, $res->{booking_details}->{is_deposit});
    $self->assert_equals("9.33", $res->{booking_details}->{amount});

    #ok, now I try to get the booking from the database... this should fail, because it is a withdrawal
    $session = $self->_connect_to_bcd("tina", 8012898, "tinap");

    ########
    #test the summary

    $command = Bcd::Data::Model->instance()->get_command("an.get_summary_bookings");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    
    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    $self->assert_equals("9.33", $res->{total_withdrawals});
    $self->assert_equals("9.33", $res->{withdrawals_summary}->[0]->{amount});

    ########

    $command = Bcd::Data::Model->instance()->get_command("bk.lookup_deposit");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($booking_token);
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_THIS_BOOKING_IS_A_WITHDRAWAL, $exit_code);

    #ok, then I can see lookup a withdrawal.
    $command = Bcd::Data::Model->instance()->get_command("bk.lookup_withdrawal");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($booking_token);
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals("9.33", $res->{booking_details}->{amount});

}

sub test_treasurer_acknowledge{

    my $self = shift;
    my $stash = $self->{stash};

    #ok, now I simulate a deposit booking
    my ($booking_token, $blinded_token) = 
	Bcd::Data::Deposits->create_deposit_booking($stash, 16,  "2932");



    my $session = $self->_connect_to_bcd("tina", 8012898, "tinap");

    my $command = Bcd::Data::Model->instance()->get_command("bk.lookup_deposit");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($booking_token);
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    
    my $res_total = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    my $res = $res_total->{booking_details};

    #I connect as a normal ant
    my $session_ugo = $self->_connect_to_bcd("ugo", 8012898, "ugop");
    #Ok, now I am able to acknoledge them
    $command = Bcd::Data::Model->instance()->get_command("bk.treasurer_acknowledged");
    $self->assert_not_null($command);
    $command->parse_line($session_ugo);
    $command->parse_line($res->{id});
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code);

    #ok, now I try with the treasurer
    $command->{session_id} = $session;

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #The booking should have changed state
    my $booking = Bcd::Data::Deposits->get_booking_from_id_hash($stash, $res->{id});

    $self->assert_equals(Bcd::Constants::DepositsConstants::DEPOSIT_TAKEN_ONLINE, $booking->{status});

    #the command cannnot be repeated
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    #I cannot delete a deposit which has been acknowledged
    my $session_boss = $self->_connect_as_a_boss();
    $command = Bcd::Data::Model->instance()->get_command("bk.delete_my_booking");
    $self->assert_not_null($command);
    $command->parse_line($session_boss);
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);
    

    #I cannot lookup a deposit which has been acknowledged
    $command = Bcd::Data::Model->instance()->get_command("bk.lookup_deposit");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($booking_token);
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_THIS_IS_A_DEPOSIT_NOT_YET_COLLECTED, $exit_code);

}

sub test_get_booking_details{

    my $self = shift;
    my $stash = $self->{stash};

    #first of all I try to connect as a boss, I should fail
    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("bk.lookup_deposit");
    $self->assert_not_null($command);

    #ok, now, let's test some useful methods...
    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line('weu weu wow');
    $command->eot();
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code);


    $session = $self->_connect_to_bcd("tina", 8012898, "tinap");

    $command->{session_id} = $session;
    $command->{booking_id} = "diswerw";
    $command->eot();

    $command->exec_only_for_test($stash);
    
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN, $exit_code);

    #ok, now I simulate a deposit booking
    my ($booking_token, $blinded_token) = 
	Bcd::Data::Deposits->create_deposit_booking($stash, 16,  "2932");

    $command->{booking_id} = $booking_token;

    $command->eot();

    $command->exec_only_for_test($stash);
    
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res_total = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    my $res = $res_total->{booking_details};

    #$self->assert_equals(16, $res->{id_user});
    $self->assert_equals("29.32", $res->{amount});
    #$self->assert_equals(Bcd::Constants::DepositsConstants::CREATION_STATE, $res->{status});
    #$self->assert_equals($booking_token, $res->{booking_token});
}


1;
