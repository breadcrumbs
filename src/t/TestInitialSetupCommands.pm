package t::TestInitialSetupCommands;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


use strict;
use warnings;
use DBI;
use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::NewAntNests;
use Bcd::Data::Founders;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Model;

use Data::Dumper;

sub set_up{
    #I should create an initial ant nest
    my $self = shift;
    my $stash= $self->{stash};

    my $code = 12393;
    $self->{code} = $code;
    $self->{founder_ids} = [];

    Bcd::Data::NewAntNests->create_new_ant_nest_booking
	($stash, $code, "test ant nest");

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, "founder_boss", 'lino@localhost' , 'password');

    push(@{$self->{founder_ids}}, $stash->get_last_founder_id());

    Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $code, "founder_treasurer", 'lino@localhost' , 'password');

    push(@{$self->{founder_ids}}, $stash->get_last_founder_id());

    #ok, then I can create an initial ant nest
    $self->_create_test_ant_nest($code);
    my $real_code = $code . "99";

    #I should add the correspondence between the booking ant nest and the new ant nest
    my $sql = qq{update ant_nests_bookings set assigned_id = ? where id = ?};
    my $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, $real_code);
    $st->bind_param(2, $code);
    $st->execute();

    #ok, then I shoul create the two users.
    $self->{new_ids} = [];
    push (@{$self->{new_ids}}, $self->_create_a_test_user
	  (
	   {id_ant_nest => $real_code,
	    nick        => 'founder_boss',
	    role        => 2,
	   }));

    push (@{$self->{new_ids}}, $self->_create_a_test_user
	  (
	   {
	       id_ant_nest => $real_code,
	       nick        => 'founder_treasurer',
	       role        => 4,
	   }
	   ));

    #ok, then I should put the ants in the correct state
    Bcd::Data::Founders->update_founder_state($stash, 
					      $self->{founder_ids}->[0], 
					      Bcd::Constants::FoundersConstants::BOSS_SET_UP_TO_DO);

    Bcd::Data::Founders->update_founder_state($stash, 
					      $self->{founder_ids}->[1], 
					      Bcd::Constants::FoundersConstants::WAITING_BOSS_SET_UP);

    #then I should set the boss and the treasurer flag.
    Bcd::Data::Founders->set_founders_flags
	($stash, $self->{founder_ids}->[0], '1', '0');

    Bcd::Data::Founders->set_founders_flags
	($stash, $self->{founder_ids}->[1], '0', '1');

}


sub test_create_initial_hq{
    my $self = shift;
    my $stash = $self->{stash};

    #first of all I should connect to bcd
    my $session_boss = $self->_connect_to_bcd("founder_boss", $self->{code} , "password");
    $self->assert_not_null($session_boss);

    #ok, I start asking for the command
    my $command = Bcd::Data::Model->instance()->get_command("na.create_hq");
    $self->assert_not_null($command);
    
    $command->parse_line($session_boss);
    $command->parse_line("testname");
    $command->parse_line("testloc");
    $command->parse_line("testdir first line");
    $command->parse_line("testdir second line");
    $command->parse_line("."); #this is the end of the multiline parameter
    $command->parse_line("1");
    $command->parse_line("9-10 am");
    $command->parse_line("1");
    $command->parse_line("9-9.30 am");

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #another time the command should fail
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    #Ok, the state should be changed
    my $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $self->{founder_ids}->[0]);
    $self->assert_equals(Bcd::Constants::FoundersConstants::SET_UP_DONE, $founder->{id_status});

    $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $self->{founder_ids}->[1]);
    $self->assert_equals(Bcd::Constants::FoundersConstants::TREASURER_SET_UP_TO_DO, $founder->{id_status});


    #ok, first of all we check that there is really a site...
    my $sql = qq{select * from ant_nests_public_sites where id_ant_nest = ?};
    my $st = $stash->get_connection()->prepare($sql);
    
    $st->bind_param(1, $self->{code} . "99");
    $st->execute();
    my $row = $st->fetch();
    $st->finish();

    my $site_id = $row->[1];

    #ok, then I can search for this site.
    $sql = qq{SELECT * from bc_sites where id = ?};
    $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, $site_id);
    $st->execute();
    
    $row = $st->fetch();
    $st->finish();

    #some assertions
    $self->assert_equals("testname", $row->[1]);
    $self->assert_equals("testloc", $row->[2]);
    $self->assert_equals("testdir first line\ntestdir second line", $row->[3]);
    $self->assert_equals("1", $row->[4]);
    $self->assert_equals("9-10 am", $row->[5]);


    #ok, now let's see if there is all the information in the db
    $sql = qq{select * FROM users_in_sites WHERE id_user = ?};
    $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, $self->{new_ids}->[0]);
    $st->execute();
    
    $row = $st->fetch();
    $st->finish();

    $self->assert_equals($site_id, $row->[2]);
    $self->assert_equals(Bcd::Constants::PublicSitesConstants::OFFICE_PRESENCE, $row->[4]);
    $self->assert_equals('1', $row->[5]);
    $self->assert_equals('9-9.30 am', $row->[6]);




    #
    #
    # test of the get hq... for a founder post code
    #
    $command = Bcd::Data::Model->instance()->get_command("an_get_hq");
    $self->assert_not_null($command);
    
    $command->parse_line($self->{code});

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    #ok, here you should check the output, when it works....
    $self->assert_equals($res->{hq}->{name}, "testname");


    #
    #
    #ok, now I should test the treasurer setup
    #
    #
    my $session_treasurer = $self->_connect_to_bcd("founder_treasurer", $self->{code} , "password");
    $self->assert_not_null($session_treasurer);

    #ok, I start asking for the command
    $command = Bcd::Data::Model->instance()->get_command("na.treasurer_setup");
    $self->assert_not_null($command);
    
    $command->parse_line($session_treasurer);
    $command->parse_line('0');
    $command->parse_line('0');
    $command->parse_line("name-t");
    $command->parse_line("loc-t");
    $command->parse_line("dir-t-1st-line");
    $command->parse_line("."); #this is the end of the multiline parameter
    $command->parse_line("1");
    $command->parse_line("9-10 am");
    $command->parse_line("0");
    $command->parse_line("every thursday 9-9.30");

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #another time the command should fail
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    #ok, now I test the results of this commands....
    
    #first of all I see the state of the ant
    $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $self->{founder_ids}->[1]);
    $self->assert_equals(Bcd::Constants::FoundersConstants::SET_UP_DONE, $founder->{id_status});    

    #ok, then the online payments
    my $value = Bcd::Data::Configuration->get_ant_nest_value
	($stash, $self->{code} . "99", Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    $self->assert_equals('0', $value);

    #ok, then there should be another public site...

    $sql = qq{select * from ant_nests_public_sites where id_ant_nest = ?};
    $st = $stash->get_connection()->prepare($sql);
    
    $st->bind_param(1, $self->{code} . "99");
    $st->execute();
    $row = $st->fetch();

    if ($row->[1] == $site_id){
	$row = $st->fetch(); #I take the second row
    } 

    $site_id = $row->[1];

    $st->finish();

    
    #ok, then I can search for this site.
    $sql = qq{SELECT * from bc_sites where id = ?};
    $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, $site_id);
    $st->execute();
    
    $row = $st->fetch();
    $st->finish();

    #some assertions
    $self->assert_equals("name-t", $row->[1]);
    $self->assert_equals("loc-t", $row->[2]);
    $self->assert_equals("dir-t-1st-line", $row->[3]);
    $self->assert_equals("1", $row->[4]);
    $self->assert_equals("9-10 am", $row->[5]);


    #ok, now let's see if there is all the information in the db
    $sql = qq{select * FROM users_in_sites WHERE id_user = ?};
    $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, $self->{new_ids}->[1]);
    $st->execute();
    
    $row = $st->fetch();
    $st->finish();

    $self->assert_equals($site_id, $row->[2]);
    $self->assert_equals(Bcd::Constants::PublicSitesConstants::OFFICE_PRESENCE, $row->[4]);
    $self->assert_equals('0', $row->[5]);
    $self->assert_equals('every thursday 9-9.30', $row->[6]);    


}

sub test_treasurer_setup_with_shared_site{
    my $self = shift;
    my $stash = $self->{stash};

    #first of all I should connect to bcd
    my $session_boss = $self->_connect_to_bcd("founder_boss", $self->{code} , "password");
    $self->assert_not_null($session_boss);

    #ok, I start asking for the command
    my $command = Bcd::Data::Model->instance()->get_command("na.create_hq");
    $self->assert_not_null($command);
    
    $command->parse_line($session_boss);
    $command->parse_line("name-shared");
    $command->parse_line("loc-shared");
    $command->parse_line("dir_shared_first_line");
    $command->parse_line("dir_shared_second_line");
    $command->parse_line("."); #this is the end of the multiline parameter
    $command->parse_line("1");
    $command->parse_line("9-10 am");
    $command->parse_line("1");
    $command->parse_line("9-9.30 am");

    $command->eot();
    $command->exec_only_for_test($stash);

    my $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);


    #
    #
    #ok, now I should test the treasurer setup
    #
    #
    my $session_treasurer = $self->_connect_to_bcd("founder_treasurer", $self->{code} , "password");
    $self->assert_not_null($session_treasurer);

    #ok, I start asking for the command
    $command = Bcd::Data::Model->instance()->get_command("na.treasurer_setup");
    $self->assert_not_null($command);
    
    $command->parse_line($session_treasurer);
    $command->parse_line('0');
    $command->parse_line('1');
    $command->parse_line("foo");
    $command->parse_line("foo");
    $command->parse_line("."); #this is the end of the multiline parameter
    $command->parse_line("foo");
    $command->parse_line("foo");
    $command->parse_line("0");
    $command->parse_line("every thursday 9-9.30");

    $command->eot();
    $command->exec_only_for_test($stash);

    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #another time the command should fail
    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_STATE, $exit_code);

    #ok, now I test the results of this commands....
    
    #first of all I see the state of the ant
    my $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $self->{founder_ids}->[1]);
    $self->assert_equals(Bcd::Constants::FoundersConstants::SET_UP_DONE, $founder->{id_status});    

    #ok, then the online payments
    my $value = Bcd::Data::Configuration->get_ant_nest_value
	($stash, $self->{code} . "99", Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    $self->assert_equals('0', $value);

    #I should have the same public site...

    my $sql = qq{select count(*) from ant_nests_public_sites where id_ant_nest = ?};
    my $st = $stash->get_connection()->prepare($sql);

    $st->bind_param(1, $self->{code} . "99");
    $st->execute();
    my $row = $st->fetch();

    my $count = $row->[0];

    $st->finish();
    $self->assert_equals(1, $count);


    #ok, now let's see if there is all the information in the db
    $sql = qq{select * FROM users_in_sites WHERE id_user = ?};
    $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, $self->{new_ids}->[1]);
    $st->execute();
    
    $row = $st->fetch();
    $st->finish();

    #$self->assert_equals($site_id, $row->[1]);

    my $site_id = $row->[2];

    $self->assert_equals(Bcd::Constants::PublicSitesConstants::OFFICE_PRESENCE, $row->[4]);
    $self->assert_equals('0', $row->[5]);
    $self->assert_equals('every thursday 9-9.30', $row->[6]);    

    $sql = qq{SELECT * from bc_sites where id = ?};
    $st = $stash->get_connection()->prepare($sql);
    $st->bind_param(1, $site_id);
    $st->execute();
    
    $row = $st->fetch();
    $st->finish();

    #print Dumper($row);
    #the row should be shared
    $self->assert_equals("name-shared", $row->[1]);
}
