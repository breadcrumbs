package t::TestInvoices;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);


use Data::Dumper;
use Bcd::Data::CreditsAndDebits;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::PublicSites;
use Bcd::Data::Ads;
use Bcd::Data::Invoices;
use Bcd::Data::Model;


sub test_buy_object{

    my $self = shift;
    my $stash= $self->{stash};

    #first of all I create a fake ad...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 17, 0, 2, 1, "a presence");

    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 17, "a text ad", $id_presence, 90, 100, 0.1, 0.2);

    #ok, then I try to pay this invoice...
    my $session = $self->_connect_as_a_boss();
    my $command = Bcd::Data::Model->instance()->get_command("ws_buy_object");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($ad_id);
    $command->parse_line($id_presence);
    $command->eot();

    #I should have insufficient funds
    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_FUNDS, $exit_code);

    #ok, I try to have some tao in my account
    my ($total_euro, $total_tao, $amount) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 16, 8012898, 154);

    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);


    #Ok, then I have the balance of my account
    my $user_account_t  = Bcd::Data::Users->get_user_account_t_id($stash, 16);
    my $new_total_t = Bcd::Data::Accounting::Accounts->get_balance($stash, $user_account_t);

    
    $self->assert_str_equals("0.044999999999618", $total_tao-$new_total_t);

    #ok, the ad must be inactive now
    my $ad  = Bcd::Data::Ads->get_ad_hash($stash, $ad_id);
    $self->assert_equals(Bcd::Constants::AdsConstants::INACTIVE, $ad->{id_state});
}

sub test_pay_invoice{

    my $self = shift;
    my $stash= $self->{stash};

    #first of all I create a fake ad...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 17, 0, 2, 1, "a presence");

    #of course now there are no active invoices for this presence
    my $ans = Bcd::Data::Invoices->are_there_active_invoices_in_this_locus
	($stash, $id_presence);
    $self->assert_equals(0, $ans);

    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 17, "a text ad", $id_presence, 90, 100, 1, 1);

    #look, I now have a chance to create an invoice
    my $invoice_id = Bcd::Data::Invoices->create_new_invoice
	($stash, 16, $ad_id, $id_presence, 90);

    $ans = Bcd::Data::Invoices->are_there_active_invoices_in_this_locus
	($stash, $id_presence);
    $self->assert_equals(1, $ans);

    #ok, then I simulate the emitting of this invoice
    Bcd::Data::Invoices->emit_invoice($stash, $invoice_id, 33);

    #ok, first of all I ask a payment plan
    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("cad_get_price_estimate_for_invoice");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($invoice_id);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #I cannot pay it by credit
    $self->assert_equals(2970, $res->{convertible_tao});
    $self->assert_equals(2970, $res->{total_price});
    $self->assert_equals(0, $res->{can_be_payed_now});

    ######## pay the invoice


    $command = Bcd::Data::Model->instance()->get_command("ws_pay_invoice");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($invoice_id);
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_FUNDS, $exit_code);

    #ok, I have joked, now I try to have more funds in my pocket
    my ($total_euro, $total_tao, $amount) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, 16, 8012898, 154);

    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    my $long_token = $res->{long_token};

    my $sql = qq{select * from invoices where id = ? };
    my $hash = $stash->select_one_row_hash($sql, $invoice_id);
    $self->assert_equals(90, $hash->{amount});
    $self->assert_equals(33, $hash->{quantity});
    $self->assert_equals(Bcd::Constants::InvoicesConstants::PAID, $hash->{id_status});
    $self->assert_equals(16, $hash->{user_to});

    #Ok, then I have the balance of my account
    my $user_account_t  = Bcd::Data::Users->get_user_account_t_id($stash, 16);
    my $new_total_t = Bcd::Data::Accounting::Accounts->get_balance($stash, $user_account_t);

    #33*90 = 2970, I cannot pay by credit...
    $self->assert_str_equals("2970", $total_tao-$new_total_t);

    #ok, now I look at the tokens
    $sql = qq{select * from invoices_tokens where id_invoice = ? };
    $hash = $stash->select_one_row_hash($sql, $invoice_id);

    $self->assert_equals(0, $hash->{amount_paid_by_credit});

    my $secret_token = Bcd::Data::Token::unblind_the_token
	($long_token, $hash->{full_token});

    #ok, then I take the digest...
    my $sha = Digest::SHA1->new;
    $sha->add($secret_token);
    my $digest = $sha->hexdigest;

    $self->assert_equals($digest, $hash->{secret_token});

    ###########################
    ##Just a test for the list of invoices.
    $command = Bcd::Data::Model->instance()->get_command("ws_get_invoices_list");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line("received");
    $command->parse_line("paid");

    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals(1, scalar(@{$res->{invoices_list}}));
    $self->assert_equals("tina", $res->{invoices_list}->[0]->{nick});

    $session = $self->_connect_to_bcd("tina", 8012898, "tinap");

    $command->start_parsing_command();
    $command->parse_line($session);
    $command->parse_line("emitted");
    $command->parse_line("paid");

    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals(1, scalar(@{$res->{invoices_list}}));
    $self->assert_equals("barbara", $res->{invoices_list}->[0]->{nick});

    ##########################
    ## just a test to get the blinded token
    $command = Bcd::Data::Model->instance()->get_command("ws_get_invoice_blinded_token");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($invoice_id);
    $command->eot();

    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals($hash->{full_token}, $res->{blinded_token});
}

sub test_get_invoice_details{
    my $self = shift;
    my $stash= $self->{stash};

    #first of all I create a fake ad...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 16, 0, 2, 1, "a presence");

    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 16, 16, "a text ad", $id_presence, 90, 100, 1, 1);

    #look, I now have a chance to create an invoice
    my $invoice_id = Bcd::Data::Invoices->create_new_invoice
	($stash, 17, $ad_id, $id_presence, 90);

    my $session = $self->_connect_as_a_boss();
    my $session_fake = $self->_connect_to_bcd("marco", 8012898, "marcop");
    my $command = Bcd::Data::Model->instance()->get_command("ws_get_invoice_details");
    $self->assert_not_null($command);

    my $input = {
	session_id => $session_fake,
	id_invoice => $invoice_id,
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED, $exit_code);

    $input->{session_id} = $session;
    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #ok, let's check some data...
    $self->assert_equals(0,            $res->{invoice_details}->{am_I_the_buyer});
    $self->assert_equals(90,           $res->{invoice_details}->{invoice}->{amount});
    $self->assert_equals("a text ad",  $res->{invoice_details}->{ad}->{ad_text});
    $self->assert_equals("a presence", $res->{invoice_details}->{presence}->{presence});
    $self->assert_equals("Servizi > Casa e famiglia > Bambini > Baby sitter per bambini 0-3 anni",
			 $res->{invoice_details}->{activity});
    $self->assert_equals('tina',       $res->{invoice_details}->{buyer});

    #now I get the details from the other part...
    $session = $self->_connect_to_bcd("tina", 8012898, "tinap");
    $input->{session_id} = $session;
    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());
    $self->assert_equals(1,            $res->{invoice_details}->{am_I_the_buyer});
    $self->assert_equals('barbara',    $res->{invoice_details}->{seller});

}

sub test_change_site_for_a_presence{
    my $self = shift;
    my $stash= $self->{stash};

    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 16, 0, 2, 1, "a presence");

    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 16, "a text ad", $id_presence, 90, 100, 1, 1);

    my $invoice_id = Bcd::Data::Invoices->create_new_invoice
	($stash, 17, $ad_id, $id_presence, 90);

    #ok, now I try to change the site of the presence, I should fail
    my $session = $self->_connect_as_a_boss();
    my $command = Bcd::Data::Model->instance()->get_command("ws.change_site_in_presence");
    $self->assert_not_null($command);
    
    my $input = {
	session_id      => $session,
	id_locus        => $id_presence,
	id_site_new     => 'NULL',
	id_special_site => 'your_home',
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_CANNOT_EDIT_ACTIVE_PRESENCE, $exit_code);

    #Ok, Now I pay the invoice and collect it
    my ($long_token, $blinded_token) = Bcd::Data::Invoices->pay_invoice_and_get_token($stash, $invoice_id, 100);

    #I simulate the collection of the invoice
    Bcd::Data::Invoices->collect_invoice($stash, $invoice_id);

    #now the command should succed
    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #I should have created another presence
    my $arr = Bcd::Data::Ads->get_all_localities_for_ad_arr($stash, $ad_id);

    $self->assert_equals(1, scalar(@{$arr}));
    
    #then I get the new presence
    my $new_presence_id = $arr->[0]->[0];
    my $presence = Bcd::Data::PublicSites->get_presence_hash($stash, $new_presence_id);

    $self->assert_equals(1, $presence->{special_site});
    $self->assert_null  ($presence->{id_site});

    #and the invoice should be tied to the OLD presence
    my $invoice = Bcd::Data::Invoices->get_invoice_hash($stash, $invoice_id);
    $self->assert_equals($id_presence, $invoice->{id_locus})
}

sub test_collect_invoice{

    my $self = shift;
    my $stash= $self->{stash};

    #first of all I create a fake ad...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 16, 0, 2, 1, "a presence");

    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 16, "a text ad", $id_presence, 90, 100, 1, 1);

    #look, I now have a chance to create an invoice
    my $invoice_id = Bcd::Data::Invoices->create_new_invoice
	($stash, 17, $ad_id, $id_presence, 90);

    #ok, then I simulate the emitting of this invoice
    Bcd::Data::Invoices->emit_invoice($stash, $invoice_id, 33);

    #then I simulate the payment of this invoice, 100 tao in credit...
    my ($long_token, $blinded_token) = Bcd::Data::Invoices->pay_invoice_and_get_token($stash, $invoice_id, 100);

    my $ans = Bcd::Data::Invoices->are_there_active_invoices_in_this_locus
	($stash, $id_presence);
    $self->assert_equals(1, $ans);

    my $count = Bcd::Data::Invoices->get_invoices_count_for_this_locus($stash, $id_presence);
    $self->assert_equals(1, $count);

    my $session = $self->_connect_as_a_boss();
    my $command = Bcd::Data::Model->instance()->get_command("ws_collect_invoice");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($invoice_id);
    $command->parse_line("fake code");
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN, $exit_code);

    #ok, now I should put the right token

    my $sql = qq{select * from invoices_tokens where id_invoice = ? };
    my $hash = $stash->select_one_row_hash($sql, $invoice_id);

    my $secret_token = Bcd::Data::Token::unblind_the_token
	($long_token, $hash->{full_token});

    $command->{secret_token} = $secret_token;

    $command->eot();
    $command->exec_only_for_test($stash);
    $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #there should be no active invoice now
    $ans = Bcd::Data::Invoices->are_there_active_invoices_in_this_locus
	($stash, $id_presence);
    $self->assert_equals(0, $ans);

    #but the count should still be one
    $count = Bcd::Data::Invoices->get_invoices_count_for_this_locus($stash, $id_presence);
    $self->assert_equals(1, $count);

    #ok, now I should have 2970 in my account
    my $user_account_t  = Bcd::Data::Users->get_user_account_t_id($stash, 16);
    my $new_total_t = Bcd::Data::Accounting::Accounts->get_balance($stash, $user_account_t);

    #33*90 = 2970, but I have received 100 in credit...
    $self->assert_str_equals("2870", $new_total_t);   

    my $net_position = Bcd::Data::CreditsAndDebits->get_net_position_between_two_ants
	($stash, 16, 17);

    $self->assert_equals(100, $net_position);

    #the row should be deleted now
    $hash = $stash->select_one_row_hash($sql, $invoice_id);
    $self->assert_null($hash);


    ####################
    ### ok, now the invoice is collected, let's try to change the presence
    $session = $self->_connect_as_a_boss();
    $command = Bcd::Data::Model->instance()->get_command("ws.edit_user_presence");
    $self->assert_not_null($command);
    
    my $input = {
	session_id      => $session,
	id_locus        => $id_presence,
	on_request      => '0',
	presence_text   => 'test presence edited',
    };

    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #ok, now let's see if the ads has changed the presence...
    my $arr = Bcd::Data::Ads->get_all_localities_for_ad_arr($stash, $ad_id);

    $self->assert_equals(1, scalar(@{$arr}));
    
    #then I get the new presence
    my $new_presence_id = $arr->[0]->[0];
    my $presence = Bcd::Data::PublicSites->get_presence_hash($stash, $new_presence_id);

    $self->assert_equals(0,                      $presence->{on_request});
    $self->assert_equals('test presence edited', $presence->{presence});

    #ok, BUT the invoice should be tied to the OLD presence
    my $invoice = Bcd::Data::Invoices->get_invoice_hash($stash, $invoice_id);
    $self->assert_equals($id_presence, $invoice->{id_locus})

}

sub test_emit_invoice{
    my $self = shift;
    my $stash= $self->{stash};

    #first of all I create a fake ad...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 16, 0, 2, 1, "a presence");

    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 16, "a text ad", $id_presence, 90, 100, 0.1, 0.2);

    #look, I now have a chance to create an invoice
    my $invoice_id = Bcd::Data::Invoices->create_new_invoice
	($stash, 17, $ad_id, $id_presence, 90);

    #ok, I have created the invoice, now I test the command...
    my $session = $self->_connect_as_a_boss();
    my $command = Bcd::Data::Model->instance()->get_command("ws_emit_invoice");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($invoice_id);
    $command->parse_line(89);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    #let's see that the invoice has been effectively created...

    my $sql = qq{select * from invoices where id = ? };

    my $hash = $stash->select_one_row_hash($sql, $invoice_id);

    $self->assert_equals(90, $hash->{amount});
    $self->assert_equals(89, $hash->{quantity});
    $self->assert_equals(Bcd::Constants::InvoicesConstants::EMITTED, $hash->{id_status});
    $self->assert_equals(17, $hash->{user_to});
    
}


sub test_invoice_basic{
    my $self = shift;
    my $stash= $self->{stash};

    #first of all I create a fake ad...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 16, 0, 2, 1, "a presence");

    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 16, "a text ad", $id_presence, 90, 100, 0.1, 0.2);

    #look, I now have a chance to create an invoice
    my $invoice_id = Bcd::Data::Invoices->create_new_invoice
	($stash, 17, $ad_id, $id_presence, 90);

    #ok, now we can test also the summary
    my $summary = Bcd::Data::Invoices->get_received_invoices_summary_by_state_arr
	($stash, 17, Bcd::Constants::InvoicesConstants::AUTHORIZED);

    $self->assert_equals(1, $summary->[0]);
    $self->assert_null($summary->[1]);

    #now the summary of the invoices emitted
    $summary = Bcd::Data::Invoices->get_emitted_invoices_summary_by_state_arr
	($stash, 16, Bcd::Constants::InvoicesConstants::AUTHORIZED);

    $self->assert_equals(1, $summary->[0]);
    $self->assert_null($summary->[1]);

    #ok, I should have a row in the db
    my $hash = Bcd::Data::Invoices->get_invoice_hash($stash, $invoice_id);

    $self->assert_equals($ad_id, $hash->{id_ad});
    $self->assert_equals($id_presence, $hash->{id_locus});
    $self->assert_equals(Bcd::Constants::InvoicesConstants::AUTHORIZED, $hash->{id_status});
    $self->assert_equals(90, $hash->{amount});
    $self->assert_null($hash->{quantity});
    $self->assert_equals(17, $hash->{user_to});

    #ok, now I emit the invoice
    Bcd::Data::Invoices->emit_invoice($stash, $invoice_id, 33);

    $summary = Bcd::Data::Invoices->get_emitted_invoices_summary_by_state_arr
	($stash, 16, Bcd::Constants::InvoicesConstants::EMITTED);

    $self->assert_equals(1,    $summary->[0]);
    $self->assert_equals(2970, $summary->[1]);

    $hash = Bcd::Data::Invoices->get_invoice_hash($stash, $invoice_id);

    $self->assert_equals($ad_id, $hash->{id_ad});
    $self->assert_equals($id_presence, $hash->{id_locus});
    $self->assert_equals(Bcd::Constants::InvoicesConstants::EMITTED, $hash->{id_status});
    $self->assert_equals(90, $hash->{amount});
    $self->assert_equals(33, $hash->{quantity});
    $self->assert_equals(17, $hash->{user_to});

    #ok, then I simulate the payment
    my ($long_token, $blinded_token) = Bcd::Data::Invoices->pay_invoice_and_get_token($stash, $invoice_id, 44);

    #the invoice is the same (only the status has changed)
    $hash = Bcd::Data::Invoices->get_invoice_hash($stash, $invoice_id);

    $self->assert_equals($ad_id, $hash->{id_ad});
    $self->assert_equals($id_presence, $hash->{id_locus});
    $self->assert_equals(Bcd::Constants::InvoicesConstants::PAID, $hash->{id_status});
    $self->assert_equals(90, $hash->{amount});
    $self->assert_equals(33, $hash->{quantity});
    $self->assert_equals(17, $hash->{user_to});

    #then I should have a row in the invoices_tokens table

    $hash = Bcd::Data::Invoices->get_invoice_token_hash($stash, $invoice_id);
    $self->assert_equals(44, $hash->{amount_paid_by_credit});

    #ok, then I should verify that the long token unlocks the secret token.
    my $secret_token = Bcd::Data::Token::unblind_the_token
	($long_token, $hash->{full_token});

    #ok, then I take the digest...
    my $sha = Digest::SHA1->new;
    $sha->add($secret_token);
    my $digest = $sha->hexdigest;

    $self->assert_equals($digest, $hash->{secret_token});

    #ok, then I simulate the collection
    Bcd::Data::Invoices->collect_invoice($stash, $invoice_id);

    #the invoice is the same (only the status has changed)
    $hash = Bcd::Data::Invoices->get_invoice_hash($stash, $invoice_id);

    $self->assert_equals($ad_id, $hash->{id_ad});
    $self->assert_equals($id_presence, $hash->{id_locus});
    $self->assert_equals(Bcd::Constants::InvoicesConstants::COLLECTED, $hash->{id_status});
    $self->assert_equals(90, $hash->{amount});
    $self->assert_equals(33, $hash->{quantity});
    $self->assert_equals(17, $hash->{user_to});

    #The token has gone
    $hash = Bcd::Data::Invoices->get_invoice_token_hash($stash, $invoice_id);
    $self->assert_null($hash);

    #I try the summary command
    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("ws_get_invoices_summary");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    $self->assert_equals(1, $res->{count_emitted_collected});
    $self->assert_equals(2970, $res->{total_emitted_collected});


    #now I just test the two functions which return the invoices list
    my $ds = Bcd::Data::Invoices->get_emitted_invoices_list_by_state_ds
	($stash, 16, Bcd::Constants::InvoicesConstants::COLLECTED);

    $self->assert_equals(17,   $ds->[1]->[1]);
    $self->assert_equals(90,   $ds->[1]->[2]);
    $self->assert_equals(33,   $ds->[1]->[3]);
    $self->assert_equals(2970, $ds->[1]->[4]);

    $ds = Bcd::Data::Invoices->get_received_invoices_list_by_state_ds
	($stash, 17, Bcd::Constants::InvoicesConstants::COLLECTED);

    $self->assert_equals(16,   $ds->[1]->[1]);
    $self->assert_equals(90,   $ds->[1]->[2]);
    $self->assert_equals(33,   $ds->[1]->[3]);
    $self->assert_equals(2970, $ds->[1]->[4]);
}

sub test_authorize_invoice_command{

    my $self = shift;
    my $stash= $self->{stash};

    #first of all I create a fake ad...
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 18, 0, 2, 1, "a presence");

    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 18, "a text ad", $id_presence, 90, 100, 0.1, 0.2);

    my $session = $self->_connect_as_a_boss();

    my $command = Bcd::Data::Model->instance()->get_command("ws_authorize_invoice");
    $self->assert_not_null($command);
    $command->parse_line($session);
    $command->parse_line($ad_id);
    $command->parse_line($id_presence);
    $command->eot();

    $command->exec_only_for_test($stash);
    my $exit_code = $command->get_exit_code();

    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);
    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #let's see that the invoice has been effectively created...

    my $sql = qq{select * from invoices where id_ad = ? and id_locus = ? };

    my $hash = $stash->select_one_row_hash($sql, $ad_id, $id_presence);

    #print Dumper($hash);

    $self->assert_equals(90, $hash->{amount});
    $self->assert_null($hash->{quantity});
    $self->assert_equals(Bcd::Constants::InvoicesConstants::AUTHORIZED, $hash->{id_status});
    $self->assert_equals(16, $hash->{user_to});
    
}

