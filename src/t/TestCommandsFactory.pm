package t::TestCommandsFactory;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Commands::ConnectCommand;
use Bcd::Commands::CommandFactory;
use Bcd::Data::StatementsStash;
use Data::Dumper;
use Bcd::Data::Model;

sub test_factory_existence{
    my $self = shift;

    my $factory = Bcd::Data::Model->instance()->get_factory();
    $self->assert_not_null($factory);

}

sub test_new_get_command{
    my $self = shift;

    my $factory = Bcd::Data::Model->instance()->get_factory();

    my $cmd = $factory->get_command("sv_connect");

    $self->assert_not_null($cmd);

    $cmd = $factory->get_command("unknown");

    $self->assert_null($cmd);
}
