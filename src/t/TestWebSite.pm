package t::TestWebSite;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use t::TestCommonDb;
our @ISA = qw(t::TestCommonDb);

use Bcd::Data::WebSite;
use Data::Dumper;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Model;
use Bcd::Data::AntNests;
use Bcd::Commands::CommandParser;


sub test_ad_price{
    my $self = shift;
    my $stash= $self->{stash};

    my $price = Bcd::Data::WebSite->get_price_for_this_ad($stash, 26, 100, 0, 0.8, 0.8);
    $self->assert_str_equals("0.05", $price);

    #0% coverabe
    $price = Bcd::Data::WebSite->get_price_for_this_ad($stash, 26, 100, 0, 0.8e-6, 0.8);
    $self->assert_str_equals("0.05", $price);

    #100% coverabe
    $price = Bcd::Data::WebSite->get_price_for_this_ad($stash, 26, 100, 0, 0.8e-6, 0.8e-6);
    $self->assert_str_equals("0.0005", $price);

    #20% coverage
    $price = Bcd::Data::WebSite->get_price_for_this_ad($stash, 26, 100, 0, 0.1, 0.1);
    $self->assert_str_equals("0.04802", $price);

    #80% coverage
    $price = Bcd::Data::WebSite->get_price_for_this_ad($stash, 26, 100, 0, 1e-10, 1e-4);
    $self->assert_str_equals("0.01832", $price);

    
}

sub test_ad_price_with_other_presences{
    my $self = shift;
    my $stash= $self->{stash};

    #first of all I should add an ad with a fake presence
    my $id_presence = Bcd::Data::PublicSites->add_special_site_presence
	($stash, 26, 0, 2, 1, "special site presence");

    #I have an ad with a presence with 0% coverage.
    my $ad_id = Bcd::Data::Ads->create_new_ad
	($stash, 1, 26, "I sell something", $id_presence, 90, 100, 0.01, 0.8);

    #now I try to make another presence
    my ($price, $reachable_ants, $total_ants) = 
      Bcd::Data::WebSite->get_price_report_for_adding_a_presence
      ($stash, $ad_id, 26, 90, 0, 0.1, 0.1);

    #I should not have to pay anything...
    $self->assert_equals(0, $price);

    ($price, $reachable_ants, $total_ants) = 
      Bcd::Data::WebSite->get_price_report_for_adding_a_presence
      ($stash, $ad_id, 26, 100, 0, 0.1, 0.1);

    #I should not have to pay anything...
    $self->assert_str_equals("0.00302", $price);

    ($price, $reachable_ants, $total_ants) = 
      Bcd::Data::WebSite->get_price_report_for_adding_a_presence
      ($stash, $ad_id, 26, 1_000_000, 0, 1e-10, 1e-10);

    #even if the coverage is 100% I have a high price, so I must pay something
    $self->assert_str_equals("4.955", $price);

    #I test that also with the command I get the same answer
    my $session = $self->_connect_to_bcd('tina', 8012898, 'tinap');
    $self->assert_not_null($session);

    #ok, let's try the command
    my $command = Bcd::Data::Model->instance()->get_command("ws.get_price_for_adding_presence");
    $self->assert_not_null($command);

    my $input = {
	session_id    => $session,
	p_min         => 1_000_000,
	p_max         => 0,
	t_e           => 0,
	t_c           => 0,
	id_ad         => $ad_id,
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_YOU_DO_NOT_OWN_THIS_AD, $exit_code);

    $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    $input->{session_id} = $session;

    $command->exec_for_test($stash, $input);
    $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    $self->assert_str_equals("4.955", $res->{price});
    $self->assert_str_equals("10", $res->{reachable_ants});
    $self->assert_str_equals("10", $res->{total_ants});
}

sub test_ad_price_cmd{
    my $self = shift;
    my $stash= $self->{stash};

    #first of all I connect to the server
    my $session = $self->_connect_to_bcd('sonia', 8012898, 'soniap');
    $self->assert_not_null($session);

    #ok, let's try the command
    my $command = Bcd::Data::Model->instance()->get_command("ws.get_price_for_ad");
    $self->assert_not_null($command);

    my $input = {
	session_id    => $session,
	p_min         => 100,
	p_max         => 0,
	t_e           => 90,
	t_c           => 90,
    };

    $command->exec_for_test($stash, $input);
    my $exit_code = $command->get_exit_code();
    $self->assert_num_equals(Bcd::Errors::ErrorCodes::BEC_OK, $exit_code);

    my $res = Bcd::Commands::CommandParser->parse_command_output($command->get_output());

    #print Dumper($res);
    $self->assert_str_equals("0.04802", $res->{price});
    $self->assert_str_equals("2", $res->{reachable_ants});
    $self->assert_str_equals("10", $res->{total_ants});
}

sub test_insert_ad_in_web_site {
    my $self = shift;
    my $stash= $self->{stash};

    my $total = Bcd::Data::WebSite->charge_user_for_this_ad($stash, 26, 8012898, 100, 0, 0.1, 0.1);
    #the account goes negative, because the user 26 has not any money...
    $self->assert_str_equals("-0.04802", $total);

    my $par_tao_ads_incomes = Bcd::Data::AntNests->get_tao_ads_partial_income_id($stash, 8012898);

    my $balance = Bcd::Data::Accounting::Accounts->get_balance($stash, $par_tao_ads_incomes);
    $self->assert_str_equals("0.04802", $balance);
}
