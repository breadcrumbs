package Bcd::Common::BcdGnuPGMail;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#I simply derive from the Mail::GnuPG because there is a thing which
#does not work for me in that module.

use Mail::GnuPG;
use base (qq/Mail::GnuPG/);

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    bless($self, $class);
    return $self;
}

sub clear_sign{
    my ($self, $entity) = @_;
    
    die "Not a mime entity"
	unless $entity->isa("MIME::Entity");

    my $body = $entity->bodyhandle;
    
    die "Message has no body"
	unless defined $body;

    my $gnupg = GnuPG::Interface->new();
    $self->_set_options( $gnupg );
    $gnupg->passphrase ( $self->{passphrase} );

    my ( $input, $output, $error )
	= ( new IO::Handle, new IO::Handle, new IO::Handle);

    my $handles = GnuPG::Handles->new(
				      stdin	=> $input,
				      stdout	=> $output,
				      stderr	=> $error,
				      );

    my $pid = $gnupg->clearsign ( handles => $handles );

    my $plaintext = $body->as_string;

    #$plaintext =~ s/\x0A/\x0D\x0A/g;
    #$plaintext =~ s/\x0D+/\x0D/g;

    print $input $plaintext;
    close $input;
    
    my @ciphertext = <$output>;
    my @error_output = <$error>;
    
    close $output;
    close $error;

    waitpid $pid, 0;
    my $exit_value  = $? >> 8;

    $self->{last_message} = [@error_output];

    my $io = $body->open ("w") or die "can't open entity body";
    $io->print (join('',@ciphertext));
    $io->close;

    return $exit_value;
}

1;

