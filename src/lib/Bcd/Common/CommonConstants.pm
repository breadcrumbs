package Bcd::Common::CommonConstants;

use strict;

#this file simply stores the constant error codes of the server
use constant {
    BINARY_MODE => 0,
    TEXT_MODE   => 1,
};

use constant {
    BC_ROOT             => 'bc-root', #this is the super user login, reserved.
    USERS_CONNECTED_KEY => "users_connected",
    TEST_POST_CODE      => '1827300',
};


1;
