package Bcd::Common::BasicAccounts;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#These are the types of the accounts.
use constant ASSETS_TYPE      => [0, 'Assets'];
use constant LIABILITIES_TYPE => [1, 'Liabilities'];
use constant INCOMES_TYPE     => [2, 'Incomes'];
use constant EXPENSES_TYPE    => [3, 'Expenses'];
use constant EQUITY_TYPE      => [4, 'Equity'];

use constant LIST_TYPES => 
    (
     ASSETS_TYPE,
     LIABILITIES_TYPE,
     INCOMES_TYPE,
     EXPENSES_TYPE,
     EQUITY_TYPE,
     );

#this stores the correspondence between a type and a currency.
#we have two currencies in the system '1' is euro and '0' is tao
use constant LIST_MASTER_ACCOUNTS =>
(
 [0 , 0, '1'],
 [1 , 0, '0'],
 [2 , 1, '1'],
 [3 , 1, '0'],
 [4 , 2, '1'],
 [5 , 2, '0'],
 [6 , 3, '1'],
 [7 , 3, '0'],
 [8 , 4, '1'],
 [9 , 4, '0'],
 );


#the meaning is [id account, type account, id parent account, description


#These are the master accounts in the system... probably they are fixed forever
use constant ASSETS_E           => [0,  0, undef, 'Assets E'];      
use constant ASSETS_T           => [1, 	1, undef, 'Assets T'];      
use constant LIABILITIES_E      => [2, 	2, undef, 'Liabilities E']; 
use constant LIABILITIES_T      => [3, 	3, undef, 'Liabilities T']; 
use constant INCOMES_E          => [4, 	4, undef, 'Incomes E'];     
use constant INCOMES_T          => [5, 	5, undef, 'Incomes T'];     
use constant EXPENSES_E         => [6, 	6, undef, 'Expenses E'];    
use constant EXPENSES_T         => [7, 	7, undef, 'Expenses T'];    
use constant EQUITY_E           => [8, 	8, undef, 'Equity E'];      
use constant EQUITY_T           => [9,  9, undef, 'Equity T'];      

#these instead could change in the future
use constant ANT_NEST_CASHES    => [10, 0, 0,     'ant nests cashes'];		 
use constant MY_CASH            => [11, 0, 0, 	  'my cash'];			 
use constant TAO_IN_CIRCULATION => [12, 1, 1, 	  'T in circulation (M1 - T)'];	 
use constant ACTIVE_TAO_CREDITS => [13, 1, 1,     'T in circulation (M2)'];
use constant PARKING_EURO_FUND  => [14, 2, 2, 	  'parking E fund'];		 
use constant USERS_ACCOUNTS_E   => [15, 2, 2, 	  "users'accounts E"];		 
use constant ANT_NEST_ACCOUNTS_E=> [16, 2, 2, 	  "ant nests'accounts E"];		 
use constant VAT                => [17, 2, 2, 	  'VAT'];				 
use constant PARKING_TAO_FUND   => [18, 3, 3,     'parking T fund'];
use constant USERS_ACCOUNTS_T   => [19, 3, 3, 	  "users'accounts T"];		 
use constant ANT_NEST_ACCOUNTS_T=> [20, 3, 3, 	  "ant nests'accounts T"];		 
use constant ANT_NEST_INCOMES_E => [21, 4, 4, 	  'incomes from ant nests E'];	 
use constant PARTIAL_INCOMES_E  => [22, 4, 4, 	  'incomes receivable (partials) E'];
use constant PARTIAL_INCOMES_T  => [23, 5, 5, 	  'partial incomes T'];              

use constant LIST_BASIC_ACCOUNTS => 
    (
     ASSETS_E,     
     ASSETS_T,    
     LIABILITIES_E,
     LIABILITIES_T,
     INCOMES_E,    
     INCOMES_T,    
     EXPENSES_E,   
     EXPENSES_T,   
     EQUITY_E,   
     EQUITY_T,     
     ANT_NEST_CASHES,
     MY_CASH,
     TAO_IN_CIRCULATION,
     ACTIVE_TAO_CREDITS,
     PARKING_EURO_FUND,
     USERS_ACCOUNTS_E,
     ANT_NEST_ACCOUNTS_E,
     VAT,
     PARKING_TAO_FUND,
     USERS_ACCOUNTS_T,
     ANT_NEST_ACCOUNTS_T,
     ANT_NEST_INCOMES_E,
     PARTIAL_INCOMES_E,
     PARTIAL_INCOMES_T,
     );

#These are the accounts of the ant nests

use constant ANT_NEST_CASH => "%s cash E";
use constant ANT_NEST_INCOME_E => "%s incomes E";
use constant ANT_NEST_PAR_INC_E => "%s partial incomes E";
use constant ANT_NEST_PAR_INC_T => "%s partial incomes T";
use constant ANT_NEST_USER_ACC_E => "%s users'accounts E";
use constant ANT_NEST_USER_ACC_T => "%s users'accounts T";
use constant PARKING_EURO_FUND_RAD => "%s parking euro";
use constant ANT_NEST_ACC_T  => "%s account T";
use constant ANT_NEST_ACC_E  => "%s account E";


use constant EURO => "E";
use constant TAO => "T";

use constant TOTAL => "tot";
use constant PARTIAL => "par";

use constant  DEMOURRAGE_INCOMES    => "%s demourrage incomes %s %s";
use constant  EXCHANGE_INCOMES      => "%s exchange incomes %s %s";
use constant  DEPOSITS_INCOMES      => "%s deposits incomes %s %s";
use constant  ACTIVITIES_INCOMES    => "%s activities incomes %s %s";
use constant  OBJECTS_INCOMES       => "%s objects incomes %s %s";
use constant  CHEQUES_INCOMES       => "%s cheques incomes %s %s";

1;


