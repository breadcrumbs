package Bcd::Common::Mail;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Mail::Sendmail;
use Template;
use MIME::Entity;
use Data::Dumper;
use Encode qw/encode decode/; 
use Net::SMTP;

use Bcd::Common::BcdGnuPGMail;
use FindBin;

sub new {
    my ($class, $props) = @_;
    my $self = {};

    #my $smtp = Net::SMTP->new('localhost', Hello => 'bricioline.it', Debug => 1);
    #$self->{smtp} = $smtp;

    $self->{is_testing} = 0;
    $self->{mailer} = $props->{mailer};
    $self->{home}   = $props->{bcd_home};


    bless($self, $class);
    return $self;
}

sub testing_do_not_send_mails{
    my $self = shift;
    $self->{is_testing} = 1;
}

sub create_the_sign_object{
    my $self = shift;

    local $/ = "\n";

    my $name = $self->{home} . "/passphrase";
    open PASSWD, "< $name" or die "Not found the passphrase file\n";
    my $key        = <PASSWD>;
    my $passphrase = <PASSWD>;
    chomp $key;
    chomp $passphrase;
    close PASSWD;

    #the home directory of the gnupg is two levels up.
    #maybe this can be configurable, for now it is simply fixed
    my $homedir = $self->{home} . "/../../.gnupg";

    my $mg = Bcd::Common::BcdGnuPGMail->new (
					     key => $key,
					     passphrase => $passphrase,
					     keydir     => $homedir,
					     );
    $self->{mg} = $mg;
}


#for now the mail is very simple...
sub mail_this_message{
    my ($self, $stash, $to, $template, $vars) = @_;

    #do not send mails if I am testing, please
    return if ($self->{is_testing} == 1);

    #lazy creation, because the tests are called by another executable
    $self->create_the_sign_object() if !defined($self->{mg});

    eval{

	local $/ = "\n";

	my $output = '';
	
	eval{
	    $stash->process_this_template($template, $vars, \$output);
	};

	if ($@){
	    $output = "I cannot send mails, error: $@\n";
	}

	my $header = encode('MIME-Q', "C'è un messaggio dalle Bricioline:");

	my $ent = MIME::Entity->build(
				      To         => $to,
				      From       => 'breadcrumb@bricioline.it',
				      Type        => "text/plain",
				      Subject     =>  $header,
				      Charset  => "utf-8",
				      Encoding    => "quoted-printable",
				      Data        => $output);

	my $ans = $self->{mg}->clear_sign ($ent);

	if ($ans != 0){
	    $ent = MIME::Entity->build(
				       To          => $to,
				       From        => 'breadcrumb@bricioline.it',
				       Type        => "text/plain",
				       Subject     =>  $header,
				       Charset     => "utf-8",
				       Encoding    => "quoted-printable",
				       Data        => $self->{mg}->{last_message}
				       );
	}

	$self->_send_this_entity_safely($ent);

    };

    if ($@){
	print "NO mail ... error $@\n";
	return 0;
    }

    return 1;
}

sub _send_this_entity_safely{
    my ($self, $entity) = @_;

    #at max one retry
    for(0..1){
	my $smtp = $self->_get_safe_smtp_server();

	my $res = $entity->smtpsend( Host => $smtp);

	if (!defined ($res)){
	    #ok, the server has gone
	    delete($self->{smtp});
	} elsif ($res == 1){
	    last; #the message was sent... go on
	}
	
    }
}

sub _get_safe_smtp_server{
    my $self = shift;

    if (!defined($self->{smtp})){
	my $smtp = Net::SMTP->new($self->{mailer}, Hello => 'bricioline.it');
	$self->{smtp} = $smtp;
    }

    return $self->{smtp};
}

1;
