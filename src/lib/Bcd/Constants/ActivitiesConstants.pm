package Bcd::Constants::ActivitiesConstants;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#this file defines the costants relative to the activities
#this are the parent activities, the macro activities in the system
use constant {
    PARENT_SERVICES   => 0,
    PARENT_HOMEMADE   => 1,
    PARENT_USED_ITEMS => 2,
};

use constant PARENT_SERVICE_AR       => [PARENT_SERVICES,      "Services"];
use constant PARENT_HOMEMADE_AR      => [PARENT_HOMEMADE,     "Homemade products"];
use constant PARENT_USED_ITEMS_AR    => [PARENT_USED_ITEMS,   "Used products"];

#this is the list of the ant nests states...
use constant LIST_MASTER_ACTIVITIES => 
    (
     PARENT_SERVICE_AR,
     PARENT_HOMEMADE_AR,
     PARENT_USED_ITEMS_AR,
     );

use constant MAX_SERVICE_ID  =>   999_999;
use constant MAX_HOMEMADE_ID => 1_999_999;
use constant MAX_OBJECT_ID   => 2_999_999;

1;
