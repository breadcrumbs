package Bcd::Constants::InvoicesConstants;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Data::Dumper;

use constant {
    AUTHORIZED   => 0,
    EMITTED      => 1,
    PAID         => 2,
    COLLECTED    => 3,
};

use constant LIST_INVOICES_STATES => 
(
 [AUTHORIZED,                            'Authorized'],
 [EMITTED,                               'Emitted'],
 [PAID,                                  'Paid'],
 [COLLECTED,                             'Collected'],
 );


1;
