package Bcd::Constants::AntNestsConstants;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#This file simply defines the constants used by the ant nests,
#in particular the states.

use constant {
    CREATION_STATE  => 1,
    TESTING_STATE   => 2,
    RUNNING_STATE   => 3,
    DYING_STATE     => 4,
    DEAD_STATE      => 5,
    SUSPENDED_STATE => 6,
};

use constant CREATION_STATE_AR    => [CREATION_STATE,      'Created'];
use constant TESTING_STATE_AR     => [TESTING_STATE,       'Testing'];
use constant RUNNING_STATE_AR     => [RUNNING_STATE,       'Running'];
use constant DYING_STATE_AR       => [DYING_STATE,         'Dying'];
use constant DEAD_STATE_AR        => [DEAD_STATE,          'Dead'];
use constant SUSPENDED_STATE_AR   => [SUSPENDED_STATE,     'Suspended'];

#this is the list of the ant nests states...
use constant LIST_ANT_NEST_STATES => 
    (
     CREATION_STATE_AR,
     TESTING_STATE_AR,
     RUNNING_STATE_AR,
     DYING_STATE_AR,
     DEAD_STATE_AR,
     SUSPENDED_STATE_AR,
     );



#the initial size should be odd and fairly small.
#the range allowed is [min, max]
#the ANT_NEST_INITIAL_SIZE is the default for the size.
use constant {
    MIN_ANT_NESTS_INITIAL_SIZE => 3,
    MAX_ANT_NESTS_INITIAL_SIZE => 7,

};

1;
