package Bcd::Constants::FoundersConstants;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use constant {
    USER_BOOKED_CREATED         => 10,
    USER_BOOKED_CONFIRMED       => 20,
    WAITING_FOR_PERSONAL_DATA   => 30,
    PERSONAL_DATA_IMMITTED      => 40,
    TRUST_NOT_PRESENT           => 50,
    TRUST_PRESENT               => 60,
    NO_CHOICE_OF_CANDIDATE      => 70,
    CHOICE_MADE                 => 80,
    NO_VOTED_YET                => 90,
    VOTED                       =>100,
    WAITING_TO_BE_CREATED       =>110,
    BOSS_SET_UP_TO_DO           =>120,
    WAITING_BOSS_SET_UP         =>130,
    TREASURER_SET_UP_TO_DO      =>140,
    SET_UP_DONE                 =>150,
    FINISHED_CREATED_AS_AN_ANT  =>160,
};
#other states will follow

use constant USER_BOOKED_CREATED_AR       => [USER_BOOKED_CREATED,       'Created'  ];
use constant USER_BOOKED_CONFIRMED_AR     => [USER_BOOKED_CONFIRMED,     'Confirmed'];
use constant WAITING_FOR_PERSONAL_DATA_AR => [WAITING_FOR_PERSONAL_DATA, 'no personal data yet'];
use constant PERSONAL_DATA_IMMITTED_AR    => [PERSONAL_DATA_IMMITTED,    'personal data present'];
use constant TRUST_NOT_PRESENT_AR         => [TRUST_NOT_PRESENT,         'no trust yet'];
use constant TRUST_PRESENT_AR             => [TRUST_PRESENT,             'trust present'];
use constant NO_CHOICE_OF_CANDIDATE_AR    => [NO_CHOICE_OF_CANDIDATE,    'no choice of candidate'];
use constant CHOICE_MADE_AR               => [CHOICE_MADE,               'choice made'];
use constant NO_VOTED_YET_AR              => [NO_VOTED_YET,              'no voted yet'];
use constant VOTED_AR                     => [VOTED,                     'voted'];
use constant WAITING_TO_BE_CREATED_AR     => [WAITING_TO_BE_CREATED,     'waiting to be created'];
use constant BOSS_SET_UP_TO_DO_AR         => [BOSS_SET_UP_TO_DO,         'boss must do initial setup'];
use constant WAITING_BOSS_SET_UP_AR       => [WAITING_BOSS_SET_UP,       'treasurer is waiting for boss setup'];
use constant TREASURER_SET_UP_TO_DO_AR    => [TREASURER_SET_UP_TO_DO,    'treasuer must do his/her setup'];
use constant SET_UP_DONE_AR               => [SET_UP_DONE,               'boss or treasurer initial setup DONE'];
use constant FINISHED_CREATED_AS_AN_ANT_AR=> [FINISHED_CREATED_AS_AN_ANT,'finished... created!'];



use constant LIST_NEW_USER_STATES =>
(
 USER_BOOKED_CREATED_AR,
 USER_BOOKED_CONFIRMED_AR,
 WAITING_FOR_PERSONAL_DATA_AR,
 PERSONAL_DATA_IMMITTED_AR,
 TRUST_NOT_PRESENT_AR,
 TRUST_PRESENT_AR,
 NO_CHOICE_OF_CANDIDATE_AR,
 CHOICE_MADE_AR,
 NO_VOTED_YET_AR,
 VOTED_AR,
 WAITING_TO_BE_CREATED_AR,
 BOSS_SET_UP_TO_DO_AR,
 WAITING_BOSS_SET_UP_AR,
 TREASURER_SET_UP_TO_DO_AR,
 SET_UP_DONE_AR,
 FINISHED_CREATED_AS_AN_ANT_AR
);

1;
