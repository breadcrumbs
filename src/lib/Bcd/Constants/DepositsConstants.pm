package Bcd::Constants::DepositsConstants;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use constant {
    CREATION_STATE         => 1,
    DEPOSIT_TAKEN_ONLINE   => 2,
    OFFLINE_ACKNOWLEDGED   => 3,
    COLLECTED              => 4,
};

use constant CREATION_STATE_AR         => [CREATION_STATE,        'Created'];
use constant DEPOSIT_TAKEN_ONLINE_AR   => [DEPOSIT_TAKEN_ONLINE,  'Got money but not collected'];
use constant OFFLINE_ACKNOWLEDGED_AR   => [OFFLINE_ACKNOWLEDGED,  'Acknowledged'];
use constant COLLECTED_AR              => [COLLECTED,             'Collected'];

#this is the list of the ant nests states...
use constant LIST_DEPOSITS_STATES => 
    (
     CREATION_STATE_AR,
     DEPOSIT_TAKEN_ONLINE_AR,
     OFFLINE_ACKNOWLEDGED_AR,
     COLLECTED_AR,
     );

1;
