package Bcd::Constants::NewAntNestsConstants;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

###############################################
## these are the states for an initial ant nest

use constant {

    WAITING_FOR_INITIAL_FOUNDERS   => 10,
    WAITING_FOR_CHECKING_IN        => 20,
    FOUNDERS_IMMITTING_DATA        => 40,
    TRUSTS_NETWORK_BUILDING        => 50,
    CANDIDATES_RESEARCH            => 60,
    MAKING_POOL                    => 70,
    WAITING_FOR_INITIAL_SET_UP     => 80,
    CREATED                        => 90,
};

use constant WAITING_FOR_INITIAL_FOUNDERS_AR   => [WAITING_FOR_INITIAL_FOUNDERS, 'waiting founders'  ];
use constant WAITING_FOR_CHECKING_IN_AR        => [WAITING_FOR_CHECKING_IN     , 'waiting for checking in'];
use constant FOUNDERS_IMMITTING_DATA_AR        => [FOUNDERS_IMMITTING_DATA     , 'founders immitting data'];
use constant TRUSTS_NETWORK_BUILDING_AR        => [TRUSTS_NETWORK_BUILDING     , 'trust network building'];
use constant CANDIDATES_RESEARCH_AR            => [CANDIDATES_RESEARCH         , 'candidates research'];
use constant MAKING_POOL_AR                    => [MAKING_POOL                 , 'making pool'];
use constant WAITING_FOR_INITIAL_SET_UP_AR     => [WAITING_FOR_INITIAL_SET_UP  , 'waiting for initial setup'];
use constant CREATED_AR                        => [CREATED                     , 'created'];


use constant LIST_NEW_ANT_NEST_STATES =>
(
 WAITING_FOR_INITIAL_FOUNDERS_AR,
 WAITING_FOR_CHECKING_IN_AR,
 FOUNDERS_IMMITTING_DATA_AR,
 TRUSTS_NETWORK_BUILDING_AR,
 CANDIDATES_RESEARCH_AR,
 MAKING_POOL_AR,
 WAITING_FOR_INITIAL_SET_UP_AR,
 CREATED_AR,
);

#this is used to have an initial size... 
#it should be odd for geometrical purposes
use constant ANT_NEST_INITIAL_SIZE      => 5;

1;
