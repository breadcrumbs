package Bcd::Constants::it::TransactionsTemplates;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use constant USER_DEPOSIT            => "deposito utente: Euro: %s";
use constant USER_WITHDRAWAL         => "ritiro utente: Euro: %s";
use constant USER_CHANGE_EURO_TAO    => "cambiati %s Euro in Tao: %s";
use constant USER_CHANGE_TAO_EURO    => "cambiati %s tao  in Euro: %s";
use constant USER_PUT_AD             => "pubblicato annuncio";
use constant USER_ISSUE_CHEQUE       => "emesso assegno per Tao %s";
use constant USER_PAYS_INVOICE       => "pagata fattura %s";

1;


