package Bcd::Constants::PublicSitesConstants;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use constant {
    OFFICE_PRESENCE         => 0,
    INTRODUCTION_PRESENCE   => 1,
    SERVICE_PRESENCE        => 2,
    OBJECT_SELL_PRESENCE    => 3,
    OBJECT_VIEW_PRESENCE    => 4,
    HOMEMADE_PRESENCE       => 5,
    
    NOT_SPECIFIED_PRESENCE  =>99,
    
};
#other states will follow

use constant OFFICE_PRESENCE_AR       => [OFFICE_PRESENCE,       'Present because of office'  ];
use constant INTRODUCTION_PRESENCE_AR => [INTRODUCTION_PRESENCE, 'Introduction presence'  ];
use constant NOT_SPECIFIED_PRESENCE_AR=> [NOT_SPECIFIED_PRESENCE,'Not specified'  ];


use constant LIST_USER_SITE_REL_TYPES =>
(
 OFFICE_PRESENCE_AR,
 INTRODUCTION_PRESENCE_AR,
 [SERVICE_PRESENCE, 'Present because of service'],
 [OBJECT_SELL_PRESENCE,  'Present because I want to sell an object'],
 [OBJECT_VIEW_PRESENCE,  'Present because you want to view an object'],
 [HOMEMADE_PRESENCE,     'Present because I want to sell a product made by myself'],
 NOT_SPECIFIED_PRESENCE_AR,
);

use constant {
    SPECIAL_SITE_MY_HOME   => 0,
    SPECIAL_SITE_YOUR_HOME => 1,
};

use constant LIST_SPECIAL_SITES =>
(
 [SPECIAL_SITE_MY_HOME,   'my home'],
 [SPECIAL_SITE_YOUR_HOME, 'your home'],
);

1;
