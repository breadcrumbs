package Bcd::Constants::ConfigurationKeys;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#this file should simply list the keys used by the site.

use constant {
    DEPOSITS_ONLINE                            =>    1,
    DEPOSIT_LIMIT                              =>    2,
    
    MINIMUM_BOSS_TRUST                         =>    3,

    
};

use constant DEPOSITS_ONLINE_AR         => [DEPOSITS_ONLINE,   'Deposit online',          '1'];
use constant DEPOSIT_LIMIT_AR           => [DEPOSIT_LIMIT,     'Deposit limit',           '2500'];
use constant MINIMUM_BOSS_TRUST_AR      => [MINIMUM_BOSS_TRUST,'Minimum boss trust (bB)', '0'];

use constant LIST_KEYS         =>
    (
     DEPOSITS_ONLINE_AR,
     DEPOSIT_LIMIT_AR,
     MINIMUM_BOSS_TRUST_AR,
     );


1;
