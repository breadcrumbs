package Bcd::Constants::Users;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#this file defines the costants relative to the users.
use constant {
    CREATION_STATE         => 1,
    NEW_ANT_WITH_ONE_TUTOR => 2,
    NEW_ANT_BEFORE_TRUST   => 3, #This is the state before the new ant
                                 #decides the trust to his/her tutors

    NORMAL_ACTIVE_ANT      => 4, #This is the normal ant state
    
};

use constant CREATION_STATE_AR       => [CREATION_STATE,         'Created'];
use constant NEW_ANT_ONE_TUT_AR      => [NEW_ANT_WITH_ONE_TUTOR, 'New ant with one tutor'];
use constant NEW_ANT_BEFORE_TRUST_AR => [NEW_ANT_BEFORE_TRUST,   'New ant before trust'];
use constant NORMAL_ACTIVE_ANT_AR    => [NORMAL_ACTIVE_ANT,      'Normal state'];


#this is the list of the ant nests states...
use constant LIST_USER_STATES => 
    (
     CREATION_STATE_AR,
     NEW_ANT_ONE_TUT_AR,
     NEW_ANT_BEFORE_TRUST_AR,
     NORMAL_ACTIVE_ANT_AR,
     );


#this is the parametric account for the user.
#the parameters are ant_nest, nick , e/t
use constant USER_ACCT_PARAMETRIC => "%s %s %s account";


1;
