package Bcd::Commands::GetAdCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Storable;

use constant NAME => "ws.get_ad";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I need the id of the activity
    push (@{$self->{parameters}}, "id_ad");
    push (@{$self->{parameters}}, "id_locus");

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;
    my $my_id = $stash->get_session_id($self->{session_id});

    #ok, first of all I should get all the ads for a particular activity in my ant nest!!!!!
    my $post_code = $stash->get_session_post_code($self->{session_id});
    
    #ok, first of all I should get the ad
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $self->{id_ad});

    #then I get the locus
    my $loc = Bcd::Data::Ads->get_ad_locality_hash($stash, $self->{id_ad}, $self->{id_locus});

    if (!defined($loc)){
	#there is some mismatch
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_AD_LOCALITY_MISMATCH;
	return;
    }

    #ok, now I have the locality and the ad... am I allowed to see them?
    my $net = Bcd::Data::TrustsNet->get_nets_for_user($stash, $my_id);
    my $incoming_trust = $net->{$ad->{id_user}}->[1];

    #my $max_trust = ($loc->{t_e} < $loc->{t_c}) ? $loc->{t_c} : $loc->{t_e};


    #print "inco trust = $incoming_trust t_e = $loc->{t_e}\n";

    my $my_own_ad = 0;
    if ($my_id == $ad->{id_user}){
	$my_own_ad = 1;
    }
    if ($my_own_ad == 0) {
	if ($incoming_trust < $loc->{t_e}){
	    #no trust...
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_TRUST;
	    return;
	}
    }

    #ok, I can see the ad...
    my $presence = Bcd::Data::PublicSites->get_presence_decoded_hash
	($stash, $self->{id_locus});

    my $nick = Bcd::Data::Users->get_nick_from_id($stash, $ad->{id_user});

    #ok, I prepare the output
    my $res = {};

    %{$res} = %{$ad};
    for(keys(%{$loc})){
	$res->{$_} = $loc->{$_};
    }
    $res->{nick} = $nick;
    $res->{site_name} = $presence->{site_name};

    #ok, now the type of ad...
    my $ad_type = Bcd::Data::Ads->decode_ad_type_from_activity
	($ad->{id_act});

    $res->{ad_type}       = $ad_type;
    $res->{presence_type} = $presence->{presence_type};
    $res->{id_site}       = $presence->{id_site};
    $res->{special_site}  = $presence->{special_site};
    $res->{on_request}    = $presence->{on_request};
    $res->{presence_text} = $presence->{presence};
    if ($my_own_ad == 1){
	$res->{pay_credit} = 1;
    } else {
	$res->{pay_credit} = $incoming_trust < $loc->{t_c} ? 0 : 1;
    }

    $self->{output} = $res;


    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_object($self->{output}, "ad", $output);
}

1;
