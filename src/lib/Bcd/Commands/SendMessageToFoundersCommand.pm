package Bcd::Commands::SendMessageToFoundersCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base qw/ Bcd::Commands::SessionCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::NewAntNests;
use Bcd::Data::Founders;
use Data::Dumper;
use Storable;

use constant NAME => "na.send_message_to_founders";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I must be a founder to make this
    $self->{privilege_required} = Bcd::Data::Users::FOUNDER_ROLE;

    my $parameter = &share({});

    $parameter->{name} = "message";
    $parameter->{type} = "multi_line";

    #no privileges required, I have other two parameters
    push (@{$self->{"parameters"}}, "subject");
    push (@{$self->{"parameters"}}, $parameter  );

    bless ($self, $class);

    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    # a very simple command
    $self->_inform_the_founders($stash);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


sub _inform_the_founders{
    my ($self, $stash) = @_;

    #first of all I get all the confirmed founders...
    my $code = $stash->get_session_post_code($self->{session_id});
    my $nick = $stash->get_session_nick     ($self->{session_id});

    my $confirmed_founders = Bcd::Data::Founders->get_confirmed_founders_to_email
	($stash, $code);



    foreach(@{$confirmed_founders}){
	    $stash->get_mailer()->mail_this_message($stash, $_->[2],
						    'founder_message.tt2', 
						    {
							name => $_->[1],
							nick => $_->[0],
							founder_name => $nick,
							id_ant_nest => $code,
							subject => $self->{subject},
							message => $self->{message},
						    }
						    );
    }



}

#no specific output, only the exit code

1;
