package Bcd::Commands::CreateInitialUserCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::CreateUserCommand;
use base(qq/Bcd::Commands::CreateUserCommand/);
use Bcd::Data::Users;

use constant NAME => "an.create_initial_user";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{"privilege_required"} = Bcd::Data::Users::BC_ROOT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #if the common checks don't pass is futile to go forward
    if ($self->_common_checks_fail($stash)){
	return;
    }

    #I should see if there are more initial users to put...
    my $remaining_ants = Bcd::Data::AntNests->get_initial_ants_remaining
	($stash, $self->{ant_nest});

    if ($remaining_ants == 0){
	#there are no more initial ants to put here...
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_MORE_INITIAL_ANTS_PLEASE;
	return;
    }

    #ok, this should be the only check in this class, now I can call the common exec
    $self->_common_exec($stash);

    #then I should tell the ant nest that a new initial ants has been created
    Bcd::Data::AntNests->a_new_initial_ant_has_been_created($stash, $self->{ant_nest});

    #I should get the id of the last inserted user...
    my $user_id = $stash->get_id_of_last_user();

    #to do, probably the account should be activated with a token.
    Bcd::Data::Users->update_users_state
	($stash, $user_id, Bcd::Constants::Users::NORMAL_ACTIVE_ANT);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

1;
