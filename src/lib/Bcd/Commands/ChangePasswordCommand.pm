package Bcd::Commands::ChangePasswordCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Bcd::Data::Users;

use constant NAME => "us_change_password";

use constant PARAM_TABLE =>
    [
     ["old_password", "new_password"],
     {
	 old_password     => ["string_type", "required"],
	 new_password     => ["string_type", "required"],
     },
     [Bcd::Commands::SessionCommand::PARAM_TABLE],
     ];


sub get_name{
    return NAME;
}

sub get_param_table{
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;
    my $input = $self->{_in}->get_input();
    my $my_id = $stash->get_session_id($input->{session_id});

    my $post_code = $stash->get_session_post_code($input->{session_id});

    #You cannot change the passwords of the test users...
    if ($post_code eq Bcd::Common::CommonConstants::TEST_POST_CODE){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_CANNOT_CHANGE_PASSWORD_FOR_TEST_USERS;
	return;
    }

    #first of all I should check the validity of the old password
    if (Bcd::Data::Users->is_valid_password_for_user($stash, $my_id, $input->{old_password}) == 0){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_PASSWORD;
	return;
    }

    #ok, now I should be able to change the password
    Bcd::Data::Users->change_password_for_user($stash, $my_id, $input->{new_password});

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

1;
