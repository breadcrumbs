package Bcd::Commands::CreateUserInSitePresenceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::PresencesCommand;
use base(qq/Bcd::Commands::PresencesCommand/);
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Constants::PublicSitesConstants;

use constant NAME => "si.create_presence";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;


    push (@{$self->{parameters}}, "id_site");
    push (@{$self->{parameters}}, "id_special_site");

    #can be "service, object, homemade";
    push (@{$self->{parameters}}, "rel_type");
    push (@{$self->{parameters}}, "request");
    push (@{$self->{parameters}}, "presence");
    

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});

    #ok, I should discriminate between the various presence types
    my ($res, $id_rel_type) = $self->_decode_rel_type_fail();

    if ($res == 1){
	return;
    }

    if ($self->_check_boolean_range_fail("request")){
	return;
    }

    #ok, now the special site... and the id_site
    my $id_nullable_site;
    my $id_special_site;
    ($res, $id_nullable_site, $id_special_site) = 
	$self->_decode_site_fail($stash);

    if ($res == 1){
	return;
    }

    #ok, I should be able to issue the command.
    my $new_id;
    if (defined($id_nullable_site)){
		
	$new_id = Bcd::Data::PublicSites->add_presence_in_site
	    ($stash, $my_id, $id_nullable_site, $id_rel_type, $self->{request}, $self->{presence});
      } else {
	$new_id = Bcd::Data::PublicSites->add_special_site_presence
	    ($stash, $my_id, $id_special_site, $id_rel_type, $self->{request}, $self->{presence});
      };

    $self->{new_id} = $new_id;
    
    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_scalar("presence_id", $self->{new_id}, $output);
}

1;
