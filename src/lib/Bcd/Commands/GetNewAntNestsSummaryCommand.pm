package Bcd::Commands::GetNewAntNestsSummaryCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SimpleCommand;
use base qw/ Bcd::Commands::SimpleCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::AntNests;
use Data::Dumper;
use Storable;

use constant NAME => "na_get_summary";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no parameters, I present only a summary...

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, let's see now the summary
    my @dataset;

    my @fields = ('id', 'state', 'name', 'requests');

    unshift(@dataset, \@fields);

    #ok, now I should get all the building ant nests...

    my $ant_nests = Bcd::Data::NewAntNests->get_ant_nests_summary($stash);

    #ok, now I should iterate along this array...
    for(@{$ant_nests}){
	#I should be able to copy the row
	my @row = @{$_};

	#then I should be able to put a number of requests
	my $requests = Bcd::Data::NewAntNests->get_ant_nest_lookers_count($stash, $_->[0]);
	push (@row, $requests);

	push(@dataset, \@row);	
    }

    $self->{former_frozen_output} = \@dataset;
    $self->{exit_code}     = Bcd::Errors::ErrorCodes::BEC_OK;

}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $output_cmd = $self->{former_frozen_output};
    $self->_add_record_set($output_cmd, 'new_ant_nests_summary', $output);

}

1;
