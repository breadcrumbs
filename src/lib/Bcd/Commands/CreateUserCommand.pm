package Bcd::Commands::CreateUserCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::SessionCommand;
use Bcd::Common::CommonConstants;
use Bcd::Data::PostCode;

use base(qq/Bcd::Commands::SessionCommand/);

#this command should simply model the command which add a new user.
#This command is ABSTRACT (in the sense that cannot be called directly
#from the server, as there are other more specific commands to add a
#new user into the system).


sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #these are the parameters specific to this command.
    #there are other parameters for the descendant classes...
    push (@{$self->{"parameters"}}, "ant_nest"); #totem and password are set by the user...
    push (@{$self->{"parameters"}}, "nick"); #totem and password are set by the user...
    push (@{$self->{"parameters"}}, "email");  #the email could be changed


    #these can only be changed by the tutors or by the ant nest boss
    push (@{$self->{"parameters"}}, "first_name");
    push (@{$self->{"parameters"}}, "last_name");
    push (@{$self->{"parameters"}}, "address");
    push (@{$self->{"parameters"}}, "home_phone");
    push (@{$self->{"parameters"}}, "mobile_phone");
    push (@{$self->{"parameters"}}, "sex");
    push (@{$self->{"parameters"}}, "birthdate");
    push (@{$self->{"parameters"}}, "identity_card_id");
    

    bless ($self, $class);
    return $self;
}

#these checks are common for all
sub _common_checks_fail{

    my ($self, $stash) = @_;

    my $code = $self->{ant_nest};
    if (! Bcd::Data::PostCode::is_valid_post_code_with_sub($code)){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE;
	return 1;
    }

    #the bc-root id is reserved, and also all the ids which start with "bc-"
    if ( $self->{"nick"} eq Bcd::Common::CommonConstants::BC_ROOT){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_RESERVED_ID;
	return 1;
    }

    if ( $self->{"nick"} =~ /^bc-/){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_RESERVED_ID;
	return 1;
    }

    if ( $self->{nick} !~ /^\w{3,15}$/){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_LENGTH_OR_CHAR_NOT_VALID_IN_ID;
	return 1;
    }

    if ( Bcd::Data::Users->exists_this_nick_already($stash, $code, $self->{nick})){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_USER_ALREADY_EXISTING;
	return 1;
    }

    return 0;
}

#this is the common exec for all the create user commands,
#which are different in parameters and in privileges required.
sub _common_exec{
    my ($self, $stash) = @_;

    #my ($self, $stash, $cmd, $ro, $theta) = @_; 

    #I should only pass the command to the users class
    

    #to do... you should compute the theta.
    my $theta = 0;
    Bcd::Data::Users->create_user($stash, $self, 1, $theta);

}

1;
