package Bcd::Commands::LookupDepositWithdrawalBookingCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;
use Bcd::Data::Configuration;
use Bcd::Constants::ConfigurationKeys;

use Data::Dumper;


#this is not a command any more, it is a base command
# sub get_name{
#     return "bk.lookup_deposit_withdrawal";
# }

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #only the treasurer can make this
    $self->{"privilege_required"} = Bcd::Data::Users::TREASURER_ROLE;



    bless ($self, $class);
    return $self;
}

sub _check_presence_of_booking_token_fail{
    my ($self, $stash) = @_;

    #I have the booking...
    
    my $booking_arr = Bcd::Data::Deposits->get_booking_from_token_arr($stash, $self->{booking_id});

    if (! defined($booking_arr )){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN;
	return 1;
    }

    my $code = $stash->get_session_post_code($self->{session_id});
    my $online = Bcd::Data::Configuration->get_ant_nest_value
	($stash, $code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    if ($online == 0){
	#in offline case the booking must be simply acknowledged
	if ( $booking_arr->[4] != Bcd::Constants::DepositsConstants::OFFLINE_ACKNOWLEDGED){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	    return 1;
	}
    } else {

	if ( $booking_arr->[4] == Bcd::Constants::DepositsConstants::OFFLINE_ACKNOWLEDGED){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_BOOKING_HAS_BEEN_ACKNOWLEDGED;
	    return 1;
	}

	if ( $booking_arr->[4] == Bcd::Constants::DepositsConstants::DEPOSIT_TAKEN_ONLINE){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_IS_A_DEPOSIT_NOT_YET_COLLECTED;
	    return 1;
	}

	if ( $booking_arr->[4] == Bcd::Constants::DepositsConstants::COLLECTED){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_BOOKING_HAS_BEEN_COLLECTED;
	    return 1;
	}
    }

    return (0, $booking_arr);
}

sub _put_this_booking_in_output{
    my ($self, $booking) = @_;

    #ok, I have the booking hash, I store it in a shared hash

    my $res  = {};

    #%{$res} = %{$booking_hash}; #?

    #in the output I store the id, the full token and the amount.
    $res->{id}            = $booking->[0];						      
    $res->{amount}     	  = Bcd::Data::RealCurrency::humanize_this_string($booking->[3]);
    $res->{full_token}    = $booking->[6];
    $res->{booking_token} = $booking->[5];
    $res->{is_deposit}    = $booking->[1];
    $res->{status}        = $booking->[4];

    $self->{output} = $res;

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{

    my ($self, $output) = @_;

    $self->_add_object($self->{output}, "booking_details", $output);

    ###### duplicate outputs, only ad usum script...
    $self->_add_scalar("id",         $self->{output}->{id}, $output);
    $self->_add_scalar("full_token", $self->{output}->{full_token}, $output);

}

1;
