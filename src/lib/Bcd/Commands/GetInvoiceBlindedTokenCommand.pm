package Bcd::Commands::GetInvoiceBlindedTokenCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::InvoiceCommand;
use base(qq/Bcd::Commands::InvoiceCommand/);
use Data::Dumper;
use Bcd::Constants::AdsConstants;

use constant NAME => "ws_get_invoice_blinded_token";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I need only the invoice id
    push (@{$self->{parameters}}, "id_invoice");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});

    #the invoice should be mine
    my $invoice = Bcd::Data::Invoices->get_invoice_hash($stash, $self->{id_invoice});
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $invoice->{id_ad});

    if ($ad->{id_user} != $my_id){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_YOU_DO_NOT_OWN_THIS_AD;
	return;
    }

    if ($invoice->{id_status} != Bcd::Constants::InvoicesConstants::PAID){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #ok, I can grab the invoice token
    my $token_details = Bcd::Data::Invoices->get_invoice_token_hash
	($stash, $self->{id_invoice});

    $self->{blinded_token} = $token_details->{full_token};

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#I have the long token as the output
sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_scalar("blinded_token", $self->{blinded_token}, $output);
}

1;
