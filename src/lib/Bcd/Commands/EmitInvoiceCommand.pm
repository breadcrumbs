package Bcd::Commands::EmitInvoiceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Bcd::Constants::AdsConstants;

use constant NAME => "ws_emit_invoice";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    push (@{$self->{parameters}}, "id_invoice");
    push (@{$self->{parameters}}, "quantity");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});
    
    #let's check only that the invoice correspond to an ad
    my $invoice = Bcd::Data::Invoices->get_invoice_hash($stash, $self->{id_invoice});

    #ok, now I get the id of the ad
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $invoice->{id_ad});

    #ok, the user must own the ad...
    if ($ad->{id_user} != $my_id){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_YOU_DO_NOT_OWN_THIS_AD;
	return;
    }

    #the invoice must be in the correct state
    if ($invoice->{id_status} != Bcd::Constants::InvoicesConstants::AUTHORIZED){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #a simple check on the quantity, some reasonable limits...
    if ($self->{quantity} < 0 or $self->{quantity} > 999){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INTEGER_OUT_OF_RANGE;
	return;
    }

    #ok, I can now emit the invoice
    Bcd::Data::Invoices->emit_invoice
	($stash, $self->{id_invoice}, $self->{quantity});


    #I should advise the buyer...
    my $buyer = $invoice->{user_to};

    my $seller_nick = $stash->get_session_nick($self->{session_id});

    my $template_vars = {
	seller   => $seller_nick,
	quantity => $self->{quantity},
	ad       => $ad,
	invoice  => $invoice,
    };

    $self->_send_mail_to_user_with_id
	($stash, "invoice_emitted.tt2", $buyer, $template_vars);
    

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#no output only the exit code.

1;
