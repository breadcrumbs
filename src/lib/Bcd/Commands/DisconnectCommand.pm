package Bcd::Commands::DisconnectCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this class should simply disconnect a user

use strict;
use warnings;
use Bcd::Commands::SessionCommand;
use Bcd::Errors::ErrorCodes;
use Bcd::Common::CommonConstants;


use base qw(Bcd::Commands::SessionCommand);

use constant NAME => "sv.disconnect";

#I don't add parameters in the table
use constant PARAM_TABLE =>
    Bcd::Commands::SessionCommand::PARAM_TABLE;

sub get_param_table{
    return PARAM_TABLE;
}

sub get_name{
    return NAME;
}

#I NEED a new, because the factory calls this function by name.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    
    bless($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my $parsed_result = $self->{_in}->get_parsed_result();
    my $session = $stash->get_cache()->get($parsed_result->{session_id});

    if (defined($session)){

	#ok, I should get the id of the user...
	my $my_id = $stash->get_session_id($parsed_result->{session_id});
	my $users_connected = $stash->get_cache()->get
	    (Bcd::Common::CommonConstants::USERS_CONNECTED_KEY);
	
	delete ($users_connected->{$my_id});
	$stash->get_cache()->set(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY);
	$stash->get_cache()->remove($parsed_result->{session_id});

	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_OK;
    } else {
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_NOT_CONNECTED;
    }
}

1;
