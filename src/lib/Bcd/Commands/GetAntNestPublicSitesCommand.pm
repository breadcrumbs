package Bcd::Commands::GetAntNestPublicSitesCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::SessionCommand;
use base qw/ Bcd::Commands::SessionCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::AntNests;
use Bcd::Data::Sites;
use Data::Dumper;
use Storable;

use constant NAME => "an_get_public_sites";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no parameters, but I must be an ant to make this command
    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, I should get my post code
    my $code = $stash->get_session_post_code($self->{session_id});

    #ok, now I get the list of the public sites.
    my $ds = Bcd::Data::Sites->get_public_sites_ant_nest_ds($stash, $code);

    #I can now put this dataset in output
    $self->{former_frozen_output} = $ds;

    
    $self->{exit_code}     = Bcd::Errors::ErrorCodes::BEC_OK;

}


sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $ds = $self->{former_frozen_output};
    $self->_add_record_set($ds, "public_sites", $output);
}

1;
