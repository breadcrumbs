package Bcd::Commands::GetOtherAntProfileCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Constants::Users;

use Storable;
use Data::Dumper;

use constant NAME => "us.get_other_ant_profile";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I can be only an ant to execute this command
    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I have one parameter: the id of the user.
    push (@{$self->{parameters}}, "id_other_user");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #If I am here I am a founder...

    #Ok, first of all I check that the user belongs to my ant nest.
    my $my_id = $stash->get_session_id($self->{session_id});

    my $user = Bcd::Data::Users->get_user_base_data_arr($stash, $self->{id_other_user});

    if ( !defined ($user)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER;
	return;
    }

    #ok, I have a user... let's see if the user you want to peek is ready
    if ( $user->[7] != Bcd::Constants::Users::NORMAL_ACTIVE_ANT){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_ACCOUNT_NOT_READY;
	return;
    }

    #ok, the user is ready. is it in the same ant nest?
    #ok, the founder is defined, let's see if he belongs to our ant nest
    my $post_code = $stash->get_session_post_code($self->{session_id});

    if ( $post_code != $user->[1]){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST;
	return;
    }

    #ok, the user is existing in the same ant nest... now I can really get the details
    my $res = {};

    $res->{nick} = $user->[2];
    $res->{role} = oct("0b" . $user->[8]);
    $res->{tutor_first} = $user->[5]; 
    $res->{tutor_second} = $user->[6]; 

    if (defined($res->{tutor_first})){
	#if a tutor is defined, it must be defined also the second...
	my $nick_first_tutor  = Bcd::Data::Users->get_nick_from_id($stash, $user->[5]);
	my $nick_second_tutor = Bcd::Data::Users->get_nick_from_id($stash, $user->[6]);

	$res->{nick_first_tutor}  = $nick_first_tutor;
	$res->{nick_second_tutor} = $nick_second_tutor;
    }


    #then let's see if there is a direct trust.
    my $direct = Bcd::Data::Trust->exists_direct_trust_between
	($stash, $my_id, $user->[0]);

    $res->{direct_trust} = $direct;

    $self->{output} = $res;

    #I can grab also the personal data...
    my $personal_data = Bcd::Data::Users->get_user_personal_data_hash
	($stash, $user->[0]);

    #my $personal = &share({});
    #%{$personal} = %{$personal_data};
    $self->{personal_profile} = $personal_data;
    
    my $role = $stash->get_session_role($self->{session_id});

    if ($direct == 0 and !($role & Bcd::Data::Users::BOSS_ROLE)){
	#no direct trust and no boss? well I must mask some data.

	#I must have the personal data trusts of this ant...
	my $trusts = Bcd::Data::PersonalUsersData->get_personal_users_data_trusts_arr
	    ($stash, $self->{id_other_user});

	if (!defined($trusts)){
	    #ok, no defined the trusts..., delete the personal profile
	    #BUT, if the other ant IS the boss, I can see at least the first name and last name
	    my $backup_first_name;
	    my $backup_last_name;
	    if ($self->{output}->{role} & Bcd::Data::Users::BOSS_ROLE){
		$backup_first_name = $self->{personal_profile}->{first_name};
		$backup_last_name = $self->{personal_profile}->{last_name};
	    } else {
		$self->{no_personal_trusts} = 1;
	    }
	    
	    $self->{personal_profile} = {};

	    if ($self->{output}->{role} & Bcd::Data::Users::BOSS_ROLE){
		$self->{personal_profile}->{first_name} = $backup_first_name;
		$self->{personal_profile}->{last_name} = $backup_last_name;
	    }

	} else {
	    $self->{no_personal_trusts} = 0;

	    #ok, now I should grab my trust net, 
	    #I could equally grab the other user's net... and look at the outgoing trust
	    my $net = Bcd::Data::TrustsNet->get_nets_for_user($stash, $my_id);
	    
	    #ok, now I should mask the personal data if the trust is insufficient
	    my $trust_incoming = $net->{$self->{id_other_user}}->[1];

	    if ($trust_incoming < $trusts->[0]){
		$self->{personal_profile}->{first_name} = undef;
	    }

	    if ($trust_incoming < $trusts->[1]){
		$self->{personal_profile}->{last_name} = undef;
	    }

	    if ($trust_incoming < $trusts->[2]){
		$self->{personal_profile}->{address} = undef;
	    }

	    if ($trust_incoming < $trusts->[3]){
		$self->{personal_profile}->{home_phone} = undef;
	    }

	    if ($trust_incoming < $trusts->[4]){
		$self->{personal_profile}->{mobile_phone} = undef;
	    }

	    if ($trust_incoming < $trusts->[5]){
		$self->{personal_profile}->{sex} = undef;
	    }

	    if ($trust_incoming < $trusts->[6]){
		$self->{personal_profile}->{birthdate} = undef;
	    }

	    if ($trust_incoming < $trusts->[7]){
		$self->{personal_profile}->{email} = undef;
	    }

	    #in any case the document number is masked
	    $self->{personal_profile}->{identity_card_id} = undef;
	    
	}

	
    }

    #ok, then the presence in public sites...
    my $ds = Bcd::Data::PublicSites->get_this_ant_presences_in_public_sites_ds
	($stash, $user->[0]);

    $self->{former_frozen_output} = $ds;

    $ds = Bcd::Data::Users->get_all_children_ds($stash, $user->[0]);
    $self->{his_children} = $ds;

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_object($self->{output}, "other_ant", $output);

    $self->_add_object($self->{personal_profile}, "personal_data", $output);
    $self->_add_scalar("no_personal_trusts", $self->{no_personal_trusts}, $output);

    my $output_cmd = $self->{former_frozen_output};
    $self->_add_record_set($output_cmd, 'presences', $output);
    $self->_add_record_set($self->{his_children}, 'his_children', $output);
}

1;
