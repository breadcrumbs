package Bcd::Commands::LookupWithdrawalBookingCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::LookupDepositWithdrawalBookingCommand;
use base(qq/Bcd::Commands::LookupDepositWithdrawalBookingCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use Data::Dumper;

use constant NAME => "bk.lookup_withdrawal";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    push (@{$self->{parameters}}, "booking_id");
    bless ($self, $class);
    return $self;
}


sub _exec{
    my ($self, $stash) = @_;

    my ($res, $booking) = $self->_check_presence_of_booking_token_fail($stash);
    
    if ($res == 1){
	return; #fail.
    }

    #ok, I have the token... is is a deposit token
    if ($booking->[1] == 1){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_BOOKING_IS_A_DEPOSIT;
	return;
    }

    #ok, it is a withdrawal
    $self->_put_this_booking_in_output($booking);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

}


1;
