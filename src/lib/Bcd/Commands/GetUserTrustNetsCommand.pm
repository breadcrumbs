package Bcd::Commands::GetUserTrustNetsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);

use Data::Dumper;
use Storable;

use constant NAME => "tr.get_user_nets";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #this parameter is only used to tell that I have a certain version of this nets,
    #and I ask if there is a new version....
    push (@{$self->{parameters}}, "time");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash ) = @_;

    #I get my id...
    my $id = $stash->get_session_id($self->{session_id});
    
    #ok, then I get the nets in the cache
    my $value = $stash->get_cache()->get("net_$id");

    if (!defined($value)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_DATA;
	return;
    }

    #ok, there is data...
    if ($value->{time} == $self->{time}){

	#ok, let's get the row
	my $row = Bcd::Data::TrustsNet->get_user_net($stash, $id);
	if ($row->[1] != 0){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_DATA_NOT_READY;
	} else {
	    #the data could also be not yet ready... because the bot is calculating it...
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_DATA_NOT_CHANGED;
	}
	return;
    }

    #this should not happen
    if ($value->{time} < $self->{time}){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INTERNAL_ERROR;
	return;
    }

    #ok, the data here is more updated, I will return it
    my $net = $value->{net};


    #ok, I then add the nick to this net, just to render it more "human friendlier"
    #and I convert the trusts in bBel.
    for(keys(%{$net})){
	#get the nick of this user...
	my $nick = Bcd::Data::Users->get_nick_from_id($stash, $_);
	unshift(@{$net->{$_}}, $nick);

	$net->{$_}->[1] = Bcd::Data::Trust::bBel($net->{$_}->[1]);
	#delete($net->{$_}->[2] = 0; #I mask the second element in the array, the incoming trust
	#I remove the incoming trust
	splice (@{$net->{$_}}, 2, 1);
    }

    $self->{former_frozen_output} = $net;
    $self->{net_time} = $value->{time};

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $net = $self->{former_frozen_output};
    $self->_add_dumped_object($net, 'net', $output);
    #I also add the net time...
    $self->_add_scalar("net_time", $self->{net_time}, $output);

}

1;
