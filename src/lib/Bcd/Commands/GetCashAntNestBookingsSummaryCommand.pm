package Bcd::Commands::GetCashAntNestBookingsSummaryCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;
use Bcd::Data::AntNests;

use Storable;


use constant NAME => "an.get_summary_bookings";

sub get_name{
    return NAME;
}


sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I must be an ant to issue this
    $self->{privilege_required} = Bcd::Data::Users::TREASURER_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #no controls, my session is sufficient

    my $code = $stash->get_session_post_code($self->{session_id});

    #let's see if I am online or offline...
    my $online = Bcd::Data::Configuration->get_ant_nest_value
	($stash, $code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    my $withdrawals;
    my $deposits;

    if ($online == 0){

	#in the case of offline mode I have three summaries...
	$deposits = Bcd::Data::Deposits->get_deposits_to_do_offline_ds   ($stash, $code);
	$withdrawals    = Bcd::Data::Deposits->get_withdrawals_to_do_offline_ds($stash, $code);

	#moreover I have the withdrawals to collect
	my $withdrawals_to_collect = Bcd::Data::Deposits->get_withdrawals_to_collect_offline_ds($stash, $code);
	$self->{tot_offline_withdrawals} = $self->_humanize_the_summary($withdrawals_to_collect);
	$self->{former_frozen_offline_withdrawals} = $withdrawals_to_collect;
	
    } else {

	#ok, I am online, so I can have the summary of the pendings movements.
	$withdrawals = Bcd::Data::Deposits->get_all_created_withdrawals_ant_nest_ds($stash, $code);
	$deposits    = Bcd::Data::Deposits->get_all_created_deposits_ant_nest_ds   ($stash, $code);

    }

    #I should humanize all the strings...
    $self->{tot_withdrawals} = $self->_humanize_the_summary($withdrawals);
    $self->{tot_deposits}    = $self->_humanize_the_summary($deposits);

    $self->{former_frozen_withdrawals} = $withdrawals;
    $self->{former_frozen_deposits}    = $deposits;


    $self->{online_mode}        = $online;

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _humanize_the_summary{
    my ($self, $summary) = @_;

    my $inhuman_total = 0;

    for (1..(@{$summary}-1)){
	my $datum = $summary->[$_];
	$inhuman_total += $datum->[1];
	$datum->[1] = Bcd::Data::RealCurrency::humanize_this_string($datum->[1]);
    }

    my $human_total = Bcd::Data::RealCurrency::humanize_this_string($inhuman_total);
    return $human_total;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $output_cmd = $self->{former_frozen_withdrawals};
    $self->_add_record_set($output_cmd, 'withdrawals_summary', $output);

    $output_cmd = $self->{former_frozen_deposits};
    $self->_add_record_set($output_cmd, 'deposits_summary', $output);

    #and then the totals
    $self->_add_scalar('total_withdrawals', $self->{tot_withdrawals}, $output);
    $self->_add_scalar('total_deposits',    $self->{tot_deposits},    $output);

    $self->_add_scalar('online'        ,    $self->{online_mode} ,    $output);

    if ($self->{online_mode} == 0){

	$output_cmd = $self->{former_frozen_offline_withdrawals};
	$self->_add_record_set($output_cmd, 'offline_withdrawals', $output);
	$self->_add_scalar('tot_offline_withdrawals', $self->{tot_offline_withdrawals}, $output);
    }
    
}

1;
