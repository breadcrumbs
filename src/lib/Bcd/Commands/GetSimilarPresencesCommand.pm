package Bcd::Commands::GetSimilarPresencesCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::PresencesCommand;
use base(qq/Bcd::Commands::PresencesCommand/);
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Constants::PublicSitesConstants;

use constant NAME => "si.get_similar_presences";

sub get_name{
    return NAME;
}

use constant PARAM_TABLE =>
    [
     ["id_locus", "similarity_type"],
     {
	 id_locus        => ["id_type",         "required"],
	 similarity_type => ["similarity_type", "required"],
     },
     [Bcd::Commands::SessionCommand::PARAM_TABLE],
     ];


sub get_param_table{
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    my $input = $self->{_in}->get_input();
    my $my_id = $stash->get_session_id($input->{session_id});
    my $id_locus = $input->{id_locus};

    #first of all I should check that the presence belongs to me.
    my $presence = Bcd::Data::PublicSites->get_presence_hash($stash, $id_locus);

    if ($my_id != $presence->{id_user}){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED;
	return;
    }

    #ok, the presence belongs to me.

    #now I should get the presences similar to this in space (in time
    #is not yet implemented)

    my $sim_pres = Bcd::Data::PublicSites->get_presences_similar_in_space_ds
	($stash, $id_locus, $my_id, $presence->{id_site}, $presence->{special_site}, $presence->{id_rel_type});
    $self->{similar_presences} = $sim_pres;


    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_record_set($self->{similar_presences}, "similar_presences",  $output);
}

1;
