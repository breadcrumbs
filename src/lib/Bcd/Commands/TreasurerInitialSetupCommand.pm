package Bcd::Commands::TreasurerInitialSetupCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Commands::CreatePublicSiteCommand;

use constant NAME => "na.treasurer_setup";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ROLE_FOR_A_FOUNDER_TREASURER;

    #Ok, I have some parameters
    push (@{$self->{parameters}}, "online_deposits");
    push (@{$self->{parameters}}, "deposits_in_hq");

    #if the deposits are not in hq, then here I have the new site
    Bcd::Commands::CreatePublicSiteCommand->_add_your_parameters($self->{parameters});

    #parameters relative to the treasurers
    push (@{$self->{parameters}}, "request");
    push (@{$self->{parameters}}, "treasurer_hours");

    #all done.

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;


    my $id_user = $stash->get_session_id($self->{session_id});

    #I should be a boss which
    my $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $id_user);

    #print Dumper($founder);
    if ($founder->{id_status} != Bcd::Constants::FoundersConstants::TREASURER_SET_UP_TO_DO){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }



    my $site_id;

    #If I am here I have passed the security check, so, please insert the site...
    my $post_code = $stash->get_session_post_code($self->{session_id});

    my $new_ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr
	($stash, $post_code);

    my $assigned_code = $new_ant_nest->[4];

    #ok, now I see if the treasurer wants the online payments
    if ($self->{online_deposits} == 0){
	#no, online, so I should update the property in the ant nest

	Bcd::Data::Configuration->insert_ant_nest_value
	    ($stash, $assigned_code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE, '0');
    }

    if ($self->{deposits_in_hq} == 0){
	$site_id = Bcd::Commands::CreatePublicSiteCommand->_exec_embedded
	    ($stash, $self, $assigned_code);	
    } else {
        #I have the deposits in the hq, so I take the hq of this ant nest
	$site_id = Bcd::Data::Sites->get_first_public_site($stash, $assigned_code);
    }

    #ok, then I add the presence in this site

    my $nick = $stash->get_session_nick($self->{session_id});


    #I should get my "new" id in the ant nest
    my $user = Bcd::Data::Users->select_basic_user_data_from_nick_nest_arr
	($stash, $assigned_code, $nick);

    #ok, then I can add the presence in this public site
    Bcd::Data::PublicSites->add_office_presence_in_site
	($stash, $user->[0], $site_id, $self->{request}, $self->{treasurer_hours});

    #I should update my state
    Bcd::Data::Founders->update_founder_state
	($stash, $id_user, Bcd::Constants::FoundersConstants::SET_UP_DONE);


    #Ok, the ant nest is really finished... the bot will now send a mail to all
    #the ants to say that the ant nest has been created.

    #the mails are sent by the bot, they could be many, and then I
    #should also change the state of all the ants... it is not trivial

    
    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#no output, only the exit code
1;
