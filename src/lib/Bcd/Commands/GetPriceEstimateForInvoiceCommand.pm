package Bcd::Commands::GetPriceEstimateForInvoiceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


#My base class is the create user command
use Bcd::Commands::GetGenericPriceEstimateCommand;
use base(qq/Bcd::Commands::GetGenericPriceEstimateCommand/);
use Data::Dumper;

use constant NAME => "cad_get_price_estimate_for_invoice";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I have only one parameter, the id of the invoice
    push (@{$self->{parameters}}, "id_invoice");

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;
    my $my_id = $stash->get_session_id($self->{session_id});

    #I must have the invoice, the ad and the locus
    my $invoice = Bcd::Data::Invoices->get_invoice_hash($stash, $self->{id_invoice});

    my $ad  = Bcd::Data::Ads->get_ad_hash($stash, $invoice->{id_ad});
    my $loc = Bcd::Data::Ads->get_ad_locality_hash($stash, $invoice->{id_ad}, $invoice->{id_locus});

    #ok, the user must own the ad...
    if ($invoice->{user_to} != $my_id){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_INVOICE_IS_NOT_FOR_YOU;
	return;
    }

    #the invoice must be in the correct state
    if ($invoice->{id_status} != Bcd::Constants::InvoicesConstants::EMITTED){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #ok, the invoice is for me and it is in the right state
    my $total_due = $invoice->{amount} * $invoice->{quantity};

    #mmm...to pay the invoice I should ask the payment plan...
    my $net = Bcd::Data::TrustsNet->get_nets_for_user($stash, $my_id);
    my $incoming_trust = $net->{$ad->{id_user}}->[1];
    my $can_use_credit = $incoming_trust < $loc->{t_c} ? 0 : 1;

    #ok, now I can ask the price estimate
    $self->_compute_price_estimate
	($stash, $my_id, $ad->{id_user}, $total_due, $can_use_credit);

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

1;
