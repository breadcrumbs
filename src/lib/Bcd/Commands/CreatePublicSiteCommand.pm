package Bcd::Commands::CreatePublicSiteCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Data::Users;
use Data::Dumper;

use constant NAME => "an_create_public_site";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #Only the boss can create a public site
    $self->{privilege_required} = Bcd::Data::Users::BOSS_ROLE;


    
    #parameters relative to the public site
    bless ($self, $class);
    $self->_add_your_parameters($self->{parameters});

    return $self;
}

sub _add_your_parameters{
    my ($class, $params) = @_;

    my $ml_par = {};

    $ml_par->{name} = "directions";
    $ml_par->{type} = "multi_line";

    push (@{$params}, "name");
    push (@{$params}, "location"); 
    push (@{$params}, $ml_par);
    push (@{$params}, "time_restricted");
    push (@{$params}, "opening_hours");
}

#this is a strange function, it is static and has a pointer to the "outer" command
sub _exec_embedded{
    my ($class, $stash, $outer_cmd, $post_code) = @_;

    my $site_id = Bcd::Data::Sites->create_a_site
	($stash, $outer_cmd->{name}, 
	 $outer_cmd->{location}, 
	 $outer_cmd->{directions}, 
	 $outer_cmd->{time_restricted}, 
	 $outer_cmd->{opening_hours});

    #ok, I can create the correspondence
    Bcd::Data::Sites->create_a_public_ant_nest_site
	($stash, $post_code, $site_id);

    return $site_id;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    #this should be written...
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#no output, only the exit code...
1;
