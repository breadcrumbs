package Bcd::Commands::GetAntNestHQCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::SimpleCommand;
use base qw/ Bcd::Commands::SimpleCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::AntNests;
use Data::Dumper;
use Storable;

use constant NAME => "an_get_hq";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I need the post code
    push(@{$self->{parameters}}, "post_code");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my $post_code;

    #first of all I must have a valid post code.
    if ($self->post_code_validity_fail($self->{post_code})){

	#ok, it is not a valid post code, maybe it is a founder post code
	if (!$self->post_code_without_sub_validity_fail($self->{post_code})){

            #ok, let's see if I can obtain the assigned code...
	    my $new_ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr
		($stash, $self->{post_code});

	    if (!defined ($new_ant_nest)){
		$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_ANT_NEST_IN_THIS_CODE;
		return;
	    }

	    my $assigned_code = $new_ant_nest->[4];

	    if (!defined($assigned_code)){
		$self->{exit_code} = 
		    Bcd::Errors::ErrorCodes::BEC_THIS_ANT_NEST_HAS_NOT_BEEN_CREATED_YET;
		return;
	    }

	    $post_code = $assigned_code;

	} else {
	    #no hope
	    return;
	}
    } else {
	$post_code = $self->{post_code};
    }

    

    #ok, the post code is valid... now I should get the site where the
    #boss is in office
    my $boss = Bcd::Data::Users->get_boss_of_this_ant_nest($stash, $post_code);

    #ok, then I should get the sites where he is in office.
    #in this first version I take only the first
    my $sites = Bcd::Data::PublicSites->get_sites_where_ant_is_in_office($stash, $boss->{id});

    #I take the first field of the first rowp
    my $first_site = $sites->[0]->[0];
    
    $self->_add_sites_details_in_output($stash, $first_site);
    $self->_add_availability_in_output($stash, $boss->{id}, $first_site);
    
    $self->{exit_code}     = Bcd::Errors::ErrorCodes::BEC_OK;

}

sub _add_sites_details_in_output{
    my ($self, $stash, $id_site) = @_;

    my $hash = Bcd::Data::Sites->get_site_hash($stash, $id_site);
    $self->{output} = $hash;
}

sub _add_availability_in_output{
    my ($self, $stash, $user, $id_site) = @_;

    my $row =
	Bcd::Data::PublicSites->get_availability_of_this_ant_in_office
	($stash, $user, $id_site);

    $self->{output}->{on_request} = $row->[0];
    $self->{output}->{presence}   = $row->[1];
}

sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_object($self->{output}, "hq", $output);
}

1;
