package Bcd::Commands::GetAdsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Storable;

use constant NAME => "ws.get_ads";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I need the id of the activity
    push (@{$self->{parameters}}, "id_act");

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;
    my $my_id = $stash->get_session_id($self->{session_id});

    #ok, first of all I should get all the ads for a particular activity in my ant nest!!!!!
    my $post_code = $stash->get_session_post_code($self->{session_id});
    
    my $ads = Bcd::Data::Ads->get_activity_post_code_ads_arr
	($stash, $self->{id_act}, $post_code);

    #ok, now I should make a cycle...
    my @dataset;

    my @fields = ('id_ad', 'id_user', 'nick', 'created_on', 'ad_text', 'id_site', 
		  'id_special_site', 'site_name', 'on_request', 'presence', 'presence_id',
		  'p_min', 'p_max', 'pay_credit');

    push(@dataset, \@fields);

    #I get my net
    my $net = Bcd::Data::TrustsNet->get_nets_for_user($stash, $my_id);
    
    foreach(@{$ads}){
	#I should get all the presences of this ad...
	my $locs = Bcd::Data::Ads->get_all_localities_for_ad_arr($stash, $_->[0]);
	my $current_user = $_->[1];
	my $nick = Bcd::Data::Users->get_nick_from_id($stash, $_->[1]);
	my $current_ad = $_;

	my $my_own_ad = 0;
	if ($my_id == $current_user){
	    $my_own_ad = 1;
	}

	
	foreach(@{$locs}){

	    #ok, I have this locality... is the user able to see it?
	    my $incoming_trust = $net->{$current_user}->[1];
	    #of course I can see my own ads.
	    if (!$my_own_ad and $incoming_trust < $_->[3]){
		#nope, next please
		next;
	    }

	    #ok, for each locality I should get the presence in the database...
	    my $presence = Bcd::Data::PublicSites->get_presence_arr($stash, $_->[0]);

	    my $site_name;
	    if (defined($presence->[1])){
		#ok, this is a presence with a real site, so I get this site
		my $site = Bcd::Data::Sites->get_site_arr($stash, $presence->[1]);

		$site_name = $site->[1];
	    }

	    #and I can fill the record!!!!
	    my $pay_credit = 0;
	    if ($my_own_ad or $incoming_trust > $_->[4]){
		$pay_credit = 1;
	    }
	    my @record = ($current_ad->[0], $current_user, $nick, $current_ad->[2], $current_ad->[3],
			  $presence->[1], $presence->[2], $site_name, $presence->[4], $presence->[5],
			  $_->[0], $_->[1], $_->[2], $pay_credit);

	    push (@dataset, \@record);
	}
    }

    $self->{former_frozen_output} = \@dataset;

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $dataset = $self->{former_frozen_output};
    $self->_add_record_set($dataset, "ads", $output);
}

1;
