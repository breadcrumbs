package Bcd::Commands::GetUserDetailsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);

use Data::Dumper;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest

use constant NAME => "an.get_user_details";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I can be only an ant to execute this command
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I must have the nick and the totem of the user
    push (@{$self->{"parameters"}}, "nick");
    push (@{$self->{"parameters"}}, "totem");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #first of all I get the ant_nest
    my $code = $stash->get_session_post_code($self->{session_id});

    #then I get the user
    my $user = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	($stash, $code, $self->{nick});


    if (! defined($user)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST;
	return;
    }

    #I don't have to check if the user is in the same ant nest... it has been already
    #implicity checked by the query.

    #ok, now I should check the totem
    if ( $self->{totem} ne $user->{totem} ){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TOTEM;
	return;
    }

    #my $res  = &share({});

    #I should copy the user hash in the shared hash
    #%{$res} = %{$user}; #?

    $self->{output} = $user;
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_object($self->{output}, "user_details", $output);
}

1;
