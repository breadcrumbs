package Bcd::Commands::CreateWithdrawalBookingCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use constant {
    MINIMUM_WITHDRAWAL => 1, #ONE cent
    MAXIMUM_WITHDRAWAL => 2500, #25 EURO
};

use constant NAME =>"bk.create_withdrawal_booking";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I need a simple ant role to create a user.
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I need the tutor trust parameter
    push (@{$self->{"parameters"}}, "amount");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #Some simple checks...

    #I should denormalize the amount
    my ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string($self->{amount});

    if ($res == 0){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_CURRENCY;
	return;
    }

    #got the id for this user...
    my $id = $stash->get_session_id($self->{session_id});

    if (Bcd::Data::Deposits->is_there_a_pending_request_for_this_user($stash, $id)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_PENDING_BOOKING_PRESENT;
	return;
    }

    #no booking in progress, ok, let's see if the amount is above the minimum (2 cents...);
    if ($dehumanized < MINIMUM_WITHDRAWAL ){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_TOO_SMALL;
	return;
    }

    if ($dehumanized > MAXIMUM_WITHDRAWAL ){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_TOO_BIG;
	return;
    }

    #NOW THE MOST IMPORTANT CHECK: the account should not become negative!
    if (! Bcd::Data::Bank->can_this_user_withdraw($stash, $id, $dehumanized)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_WILL_RENDER_NEGATIVE_THE_ACCOUNT;
	return;
    }


    #ok, the booking is valid go for it!

    #I can create a booking

    my ($booking_token, $full_token) = 
	Bcd::Data::Deposits->create_withdrawal_booking($stash, $id,  $dehumanized);

    #ok, let's get the tokens
    $self->{booking_token} = $booking_token;
    $self->{full_token} = $full_token;

    my $code = $stash->get_session_post_code($self->{session_id});

    my $online = Bcd::Data::Configuration->get_ant_nest_value
	($stash, $code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    my $template_vars = 
    {
	amount => $self->{amount},
	online => $online,
	booking_token => $booking_token,
	full_token => $full_token,
    };

    $self->_send_mail_to_user_with_id
	($stash, 'withdrawal_request_done.tt2', $id, $template_vars);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    #I should return the booking and the blinded token
    $self->_add_scalar("booking_token", $self->{booking_token}, $output);
    $self->_add_scalar("full_token", $self->{full_token}, $output);
}

1;
