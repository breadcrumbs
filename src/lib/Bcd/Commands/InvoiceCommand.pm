package Bcd::Commands::InvoiceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Bcd::Constants::AdsConstants;

#this is simply an abstract class which defines some useful methods used by
#the BuyObjectCommand and the other commands

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _get_ad_and_locus_fail{
    my ($self, $stash) = @_;

    my $ad  = Bcd::Data::Ads->get_ad_hash         ($stash, $self->{id_ad});

    if (!defined($ad)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_AD;
	return (1, undef, undef);
    }

    my $loc = Bcd::Data::Ads->get_ad_locality_hash($stash, $self->{id_ad}, $self->{id_locus});

    if (!defined($loc)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_AD_LOCALITY_MISMATCH;
	return (1, undef, undef);
    }

    return (0, $ad, $loc);
}

sub _exec_authorize_invoice_fail{
    my ($self, $stash) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});

    my ($res, $ad, $loc) = $self->_get_ad_and_locus_fail($stash);
    
    if ($res == 1){
	return (1, undef);
    }

    #the ad must be active
    if ($ad->{id_state} != Bcd::Constants::AdsConstants::ACTIVE){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return (1, undef);
    }

    #ok, then I get the trust, for this ad
    my $net = Bcd::Data::TrustsNet->get_nets_for_user($stash, $my_id);
    my $incoming_trust = $net->{$ad->{id_user}}->[1];
    
    if ($my_id == $ad->{id_user}){
	#mmm, I cannot buy from myself
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_CANNOT_BUY_FROM_YOURSELF;
	return (1, undef);
    }

    #are you enabled to see this ad?
    if ($incoming_trust < $loc->{t_e}){
	#no trust...
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_TRUST;
	return (1, undef);
    }

    #ok, the invoice can be created.
    #the amount for now is fixed... maybe in another version it will be variable.
    my ($invoice_id) = Bcd::Data::Invoices->create_new_invoice
	($stash, $my_id, $self->{id_ad}, $self->{id_locus}, $loc->{p_min});

    #ok, it should be all ok
    return (0, $invoice_id);
}

sub _pay_invoice_fail{
    my ($self, $stash, $my_id, $invoice, $invoice_id, $ad, $loc) = @_;

    #the amount of the invoice is with the quantity
    my $total_due = $invoice->{amount} * $invoice->{quantity};



    #mmm...to pay the invoice I should ask the payment plan...
    my $net = Bcd::Data::TrustsNet->get_nets_for_user($stash, $my_id);
    my $incoming_trust = $net->{$ad->{id_user}}->[1];

    my $can_use_credit = $incoming_trust < $loc->{t_c} ? 0 : 1;

    #ok, now I ask the payment plan
    my 
	(
	 $credits_towards_seller, 
	 $cheque_amount, 
	 $cheque_price, 
	 $convertible_tao, 
	 $total_price,
	 $can_be_payed_now) = Bcd::Data::CreditsAndDebits->get_price_estimate_report_for_a_payment
	 ($stash, $my_id, $ad->{id_user}, $total_due, $can_use_credit);

    if ($can_be_payed_now == 0){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_FUNDS;
	return 1;
    }

    my $post_code = $stash->get_session_post_code($self->{session_id});

    #ok, I can pay this invoice..., first of all I pay the cheque
    if ($cheque_amount > 0){
	Bcd::Data::Bank->ant_emits_cheque($stash, $my_id, $post_code, $cheque_amount);
    }


    #Ok, now I can freeze the credit
    my $total_credit = $cheque_amount + $credits_towards_seller;
    if ($total_credit > 0){
	Bcd::Data::CreditsAndDebits->ant_freezes_credit($stash, $my_id, $cheque_amount + $credits_towards_seller);
    }

    #ok, now I freeze the payment in real tao...
    if ($convertible_tao > 0){
	Bcd::Data::Bank->ant_pays_invoice($stash, $my_id, $invoice_id, $convertible_tao);
      }
    
    #then I can really pay the invoice...
    my $blinded_token;
    ($self->{long_token}, $blinded_token) = 
	Bcd::Data::Invoices->pay_invoice_and_get_token($stash, $invoice_id, $total_credit);

    #I should advise the seller.
    my $buyer = $stash->get_session_nick($self->{session_id});

    #I advise the seller with the details of the invoice payment
    my $template_vars = {
	blinded_token => $blinded_token,
	buyer         => $buyer,
	ad            => $ad,
	loc           => $loc,
	invoice       => $invoice,
	invoice_id    => $invoice_id,
	credits_towards_seller => $credits_towards_seller,
	cheque_amount          => $cheque_amount,
	convertible_tao        => $convertible_tao,
	total_due              => $total_due,
	long_token             => $self->{long_token},
    };

    my $seller = $ad->{id_user};

    $self->_send_mail_to_user_with_id
	($stash, "invoice_paid.tt2", $seller , $template_vars);

    #mail to the buyer
    $self->_send_mail_to_user_with_id
	($stash, "you_have_paid_an_invoice.tt2", $my_id , $template_vars);
 
    #ok, it should be all ok
    return 0;
}


1;
