package Bcd::Commands::GetCandidatesSummaryCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;

use Storable;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest

use constant NAME => "na_get_candidates_summary";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I can be only an ant to execute this command
    $self->{privilege_required} = Bcd::Data::Users::FOUNDER_ROLE;

    #no parameters

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #If I have passed the authorization phase I am ok, I can get the summary.

    #I get the ant nest id from the session
    my $post_code = $stash->get_session_post_code($self->{session_id});

    my $bosses = Bcd::Data::Founders->get_candidates_bosses_ds($stash, $post_code);
    $self->{former_frozen_bosses} = $bosses;

    my $treasures = Bcd::Data::Founders->get_candidates_treasurers_ds($stash, $post_code);
    $self->{former_frozen_treasures} = $treasures;

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $output_cmd = $self->{former_frozen_bosses};
    $self->_add_record_set($output_cmd, 'bosses_candidates', $output);

    $output_cmd = $self->{former_frozen_treasures};
    $self->_add_record_set($output_cmd, 'treasurers_candidates', $output);
}

1;
