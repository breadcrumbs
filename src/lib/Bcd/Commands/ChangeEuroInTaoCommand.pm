package Bcd::Commands::ChangeEuroInTaoCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Digest::SHA1;

use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use Data::Dumper;

use constant NAME => "bk.change_euro_in_tao";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I need the number of euros to change
    push (@{$self->{parameters}}, "amount");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, first of all I should check that I could withdraw this amount
    my ($res, $dehumanized) = Bcd::Data::RealCurrency::dehumanize_this_string($self->{amount});

    if ($res == 0){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_CURRENCY;
	return;
    }

    #check if there is a pending withdrawal request, if there is assume it will be withdrawn
    my $my_id = $stash->get_session_id($self->{session_id});
    my $booking = Bcd::Data::Deposits->get_pending_booking_from_user_id_arr($stash, $my_id);

    my $corrected_amount = $dehumanized;

    if (defined($booking)){
	#print Dumper($booking);
	if ($booking->[1] == 0){
	    #ok, is a withdraw
	    $corrected_amount += $booking->[3];
	}
    }
    #print "The amount before is $dehumanized after is $corrected_amount \n";

    #then, has the user enough euros to make this change?
    if (! Bcd::Data::Bank->can_this_user_withdraw($stash, $my_id, $corrected_amount)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_WILL_RENDER_NEGATIVE_THE_ACCOUNT;
	return;
    }

    #ok, the amount is good, it will not render negative the account...
    my $post_code = $stash->get_session_post_code($self->{session_id});

    my ($new_tot_euro, $new_tot_tao, $tao_obtained) = 
	Bcd::Data::Bank->change_euro_in_tao($stash, $my_id, $post_code, $dehumanized);

    #I should humanize the euro string
    $new_tot_euro = Bcd::Data::RealCurrency::humanize_this_string($new_tot_euro);

    #ok, let's send a confirmation letter...
    my $template_vars = {
	new_tot_euro => $new_tot_euro,
	new_tot_tao  => $new_tot_tao,
	tao_obtained => $tao_obtained,
	amount       => $self->{amount},
    };

    $self->{tao_obtained} = $tao_obtained;
    $self->{new_tot_euro} = $new_tot_euro;
    $self->{new_tot_tao}  = $new_tot_tao;

    $self->_send_mail_to_user_with_id
	($stash, 'change_euro_tao_done.tt2', $my_id, $template_vars);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{

    my ($self, $output) = @_;

    $self->_add_scalar("tao_obtained", $self->{tao_obtained}, $output);
    $self->_add_scalar("new_tot_euro", $self->{new_tot_euro}, $output);
    $self->_add_scalar("new_tot_tao" , $self->{new_tot_tao} , $output);
}

1;
