package Bcd::Commands::CommandParser;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Data::Dumper;

#this is a static method, it simply returns a hash
sub parse_command_output{
    my ($class, $output) = @_;

    #print "parsing $output\n\n";

    #my $hash = eval("no strict; $output");

#     #ok, I start with an empty hash
#     my $hash = {};

#     #I scan the output one line at a time
#     my $next_item;
#     while(1){
# 	$next_item = shift(@{$output});
# 	last if !defined($next_item);

# 	my @item = split(/ \| /, $next_item);
# 	#print Dumper(\@item);

# 	if ($item[1] eq '$'){
# 	    #ok, I have a scalar
# 	    $hash->{$item[0]} = $item[2];
# 	} elsif ($item[1] eq '*'){
# 	    #it is a recordset
# 	    $class->_parse_recordset($hash, $output, $item[0], $item[2]);
# 	} elsif ($item[1] eq '%'){
# 	    #it is an object
# 	    $class->_parse_object($hash, $output, $item[0], $item[2]);
# 	} elsif ($item[1] eq '!'){
# 	    #it is an object
# 	    $class->_parse_dumped_object($hash, $output, $item[0]);
# 	}

#     }
    if (ref($output) eq "HASH"){

	###mmm if there is a recordset you should parse it...
	if (exists($output->{__recordsets})){
	    foreach(@{$output->{__recordsets}}){
		$output->{$_} = $class->_parse_recordset($output->{$_});
	    }

	    delete $output->{__recordsets};
	}
	
	return $output;
    }
}

sub _parse_dumped_object{
    my ($self, $hash, $output, $name) = @_;
    my $row = shift(@{$output});

    #this is the dumped representation...
    $hash->{$name} = eval("no strict; $row");
}

#This function will parse an object (hash)
sub _parse_object{
    my ($self, $hash, $output, $name, $num_of_fields) = @_;

    my $object = {};

    #I simple iterate over the fields
    for(1..$num_of_fields){
	my $row = shift(@{$output});
	my @record = split(/ \| /, $row);
	$object->{$record[0]} = $record[2];
    }

    #ok, I have now the hash, let's put it in the outer hash
    $hash->{$name} = $object;
}

sub _parse_recordset{
    my ($self, $recordset) = @_;

    #ok, the first array is the record of the fields
    my $fields = $recordset->[0];
    my @parsed_rs;
    
    foreach(@{$recordset}[1..$#{$recordset}]){
	my %record;
	my $record_naked = $_;
	my $i = 0;
	foreach(@{$fields}){
	    $record{$_} = $record_naked->[$i];
	    $i++;
	}

	push(@parsed_rs, \%record);
    }

    return \@parsed_rs;
    
}

sub _parse_recordset_old{
    my ($self, $hash, $output, $name, $num_of_rows) = @_;

    #ok, first of all I should have the fields
    my $fields = shift(@{$output});
    my @fields = split(/ \| /, $fields);

    #my $rs = {};
    #$rs->{"fields"} = \@fields;

    my @records = ();

    for (1..$num_of_rows){
	my $row = shift(@{$output});
	my @record_naked = split(/ \| /, $row);

	my %record;

	my $index = 0;
	foreach(@fields){
	    my $field = $record_naked[$index];

	    if ($field eq "BCDNULL"){
		$field = undef;
	    } elsif ($field =~ /^BCDNULL/){
		$field = substr($field, 7); 
	    }

	    $record{$_} = $field;
	    $index ++;
	}

	push (@records, \%record);
	
    }

    #$rs->{"rows"} = \@records;


    #I store the hash, inside the bigger hash
    $hash->{$name} = \@records;
}

1;
