package Bcd::Commands::GetUserPresencesSummaryCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::PresencesCommand;
use base(qq/Bcd::Commands::PresencesCommand/);
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Constants::PublicSitesConstants;

use constant NAME => "si.get_user_presences_summary";

sub get_name{
    return NAME;
}

use constant PARAM_TABLE =>
    Bcd::Commands::SessionCommand::PARAM_TABLE;

sub get_param_table{
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    my $input = $self->{_in}->get_input();
    my $my_id = $stash->get_session_id($input->{session_id});

    #no controls to be done, a simple "get" command

    #I should have three recordsets, one for each specific presence type.
    #my $presences_services = 
    my $service_presences = Bcd::Data::PublicSites->get_user_presences_summary_ds
	($stash, $my_id, Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE);
    $self->_add_the_ads_count($stash, $service_presences);

    $self->{service_presences} = $service_presences;

    my $object_presences = Bcd::Data::PublicSites->get_user_presences_summary_ds
	($stash, $my_id, Bcd::Constants::PublicSitesConstants::OBJECT_SELL_PRESENCE);
    $self->_add_the_ads_count($stash, $object_presences);

    $self->{object_presences} = $object_presences;

    my $homemade_presences = Bcd::Data::PublicSites->get_user_presences_summary_ds
	($stash, $my_id, Bcd::Constants::PublicSitesConstants::HOMEMADE_PRESENCE);
    $self->_add_the_ads_count($stash, $homemade_presences);

    $self->{homemade_presences} = $homemade_presences;


    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_the_ads_count{
    my ($self, $stash, $dataset) = @_;

    
    #first of all I add the "ads_count" column;
    #But you should add it, ONLY if it is hasn't been added yet:
    #because the command is cached...
    if ($dataset->[0]->[0] ne "ads_count"){
	unshift(@{$dataset->[0]}, "ads_count");
    }

    #ok, I should now get the ads count for this particular presence
    foreach(@{$dataset}[1..$#{$dataset}]){
	#ok, I should now get the id_locus for this particular presence.
	my $id_locus = $_->[0];
	my $ads_count = Bcd::Data::Ads->get_count_ads_for_locus($stash, $id_locus);
	unshift(@{$_}, $ads_count);
    }
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_record_set($self->{service_presences}, "service_presences",  $output);
    $self->_add_record_set($self->{object_presences},  "object_presences",   $output);
    $self->_add_record_set($self->{homemade_presences},"homemade_presences", $output);

}

1;
