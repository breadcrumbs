package Bcd::Commands::UpdateCandidatesFlagsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base qw/ Bcd::Commands::SessionCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::NewAntNests;
use Bcd::Data::Founders;
use Data::Dumper;
use Storable;

use constant NAME => "nu_update_candidates_flags";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I need to be a founder
    $self->{privilege_required} = Bcd::Data::Users::FOUNDER_ROLE;

    push (@{$self->{"parameters"}}, "boss_flag");
    push (@{$self->{"parameters"}}, "treasurer_flag");

    bless ($self, $class);

    return $self;
}



sub _exec{
    my ($self, $stash) = @_;

    #first of all I should check my state
    my $id_user = $stash->get_session_id($self->{session_id});

    my $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $id_user);

    if ($founder->{id_status} != Bcd::Constants::FoundersConstants::NO_CHOICE_OF_CANDIDATE){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #ok, I am in the right state... I should check the parameter validity
    if ($self->_check_boolean_range_fail("boss_flag")){
	return;
    }

    if ($self->_check_boolean_range_fail("treasurer_flag")){
	return;
    }

    #Ok, the two flags must not be on at the same time
    if ($self->{boss_flag} eq '1' and $self->{treasurer_flag} eq '1'){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_YOU_CANNOT_BE_BOSS_AND_TREASURER;
	return;
    }

    #ok, I can update the flags
    Bcd::Data::Founders->set_founders_flags
	($stash, $id_user, $self->{boss_flag}, $self->{treasurer_flag});

    #ok, I can update the state of this founder
    Bcd::Data::Founders->update_founder_state($stash, $id_user, Bcd::Constants::FoundersConstants::CHOICE_MADE);

    #ok, I can return... all ok.
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

}

#no output only the exit code
1;

