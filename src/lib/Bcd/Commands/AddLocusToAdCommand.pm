package Bcd::Commands::AddLocusToAdCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Storable;

use constant NAME => "ws.add_locus_to_ad";

use constant PARAM_TABLE => 
    [
     ["id_ad", "id_locus", "p_min", "p_max", "t_e", "t_c"],
     {
	 id_ad    => ["id_type",    "required"],
	 id_locus => ["id_type",    "required"],
	 p_min    => ["tao_type",   "required"],
	 p_max    => ["tao_type",   "required"],
	 t_e      => ["trust_bb",   "required"],
	 t_c      => ["trust_bb",   "required"],
     },
     [Bcd::Commands::SessionCommand::PARAM_TABLE],
     ];


sub get_name{
    return NAME;
}

sub get_param_table(){
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;
    my $input = $self->{_in}->get_parsed_result();
    my $my_id = $stash->get_session_id($input->{session_id});

    #Ok, I should try to get the ad
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $input->{id_ad});
    if (!defined($ad)){
	$self->{exit_code} =  Bcd::Errors::ErrorCodes::BEC_NO_DATA;
	return;
    }

    #the ad must belong to me
    if ($ad->{id_user} != $my_id){
	$self->{exit_code} =  Bcd::Errors::ErrorCodes::BEC_YOU_DO_NOT_OWN_THIS_AD;
	return;
    }

    #ok, now I get the presences of this ad...
    if ($ad->{id_state} != Bcd::Constants::AdsConstants::ACTIVE){
	$self->{exit_code} =  Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #Ok, now let's see if the ad does already have this locus
    my $ad_loc = Bcd::Data::Ads->get_ad_locality_arr($stash, $input->{id_ad},
						     $input->{id_locus});

    if (defined($ad_loc)){
	$self->{exit_code} =  Bcd::Errors::ErrorCodes::BEC_DUPLICATE_AD_LOCALITY;
	return;
    }

    #ok, now the last check: the locality must have the presence compatible with the
    #type of the ad...
    my $ad_type = Bcd::Data::Ads->decode_ad_type_from_activity
	($ad->{id_act});
    
    #ok, now I try to get the locus
    my $presence = Bcd::Data::PublicSites->get_presence_arr($stash, $input->{id_locus});

    if (!defined($presence)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_PRESENCE;
	return;
    }

    #ok, now I should check the compatibility of the presence with the ad in question
    if ($ad_type eq "service"){
	if ($presence->[3] != Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INCOMPATIBLE_PRESENCE_TYPE;
	    return;
	}
    } elsif ($ad_type eq "homemade"){
	if ($presence->[3] != Bcd::Constants::PublicSitesConstants::HOMEMADE_PRESENCE){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INCOMPATIBLE_PRESENCE_TYPE;
	    return;
	}
    } else {
	if ($presence->[3] != Bcd::Constants::PublicSitesConstants::OBJECT_SELL_PRESENCE){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INCOMPATIBLE_PRESENCE_TYPE;
	    return;
	}
    }
    
    #End of compatibility test

    #now, have you got enough money?
    #Ok, it seems all ok now, I can insert the ad's locality
    my $t_e = Bcd::Data::Trust::dec_from_bBel($input->{t_e});
    my $t_c = Bcd::Data::Trust::dec_from_bBel($input->{t_c});

    my $tax = Bcd::Data::WebSite->get_price_for_this_presence
	($stash, $input->{id_ad}, $my_id, $input->{p_min}, $input->{p_max}, $t_e, $t_c);

    if ($tax > 0){
	#can you afford it?
	if (! Bcd::Data::Bank->can_this_user_withdraw_tao($stash, $my_id, $tax)){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_FUNDS;
	    return;
	}
	my $post_code = $stash->get_session_post_code($input->{session_id});

	#ok, you can afford, debit the account
	$self->{new_total_tao} = Bcd::Data::WebSite->charge_user_for_this_ad
	    ($stash, $my_id, $post_code, $input->{p_min}, $input->{p_max},
	     $t_e, $t_c);
    }


    Bcd::Data::Ads->add_locus_to_ad
	($stash, $input->{id_ad}, $input->{id_locus}, 
	 $input->{p_min}, $input->{p_max}, $t_e, $t_c);

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#no output, only the exit code.

1;
