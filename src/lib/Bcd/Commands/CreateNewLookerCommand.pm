package Bcd::Commands::CreateNewLookerCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::CreateAntNestBookingRequestCommand;
use base qw/ Bcd::Commands::CreateAntNestBookingRequestCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::NewAntNests;
use Data::Dumper;
use Storable;

use constant NAME => "an_create_new_looker";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I have the parameters of the base class

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #if these checks fail is futile to continue
    if ($self->_common_checks_fail($stash)){
	return;
    }


    #ok, now I should see if exists already a booking for this ant nest.
    my $new_ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_hash
	($stash, $self->{id_ant_nest});

    my $is_first_user;

    if (defined ($new_ant_nest)){

	#the name of the ant nest is already defined
	$self->{name_ant_nest} = $new_ant_nest->{proposed_name};
	
	$is_first_user = 0;

    } else {
	#this is a new booking..., let's see if exists already a working ant nest in the same code
	$is_first_user = 1;
    }

    $self->_create_new_ant_nest_booking($stash, $is_first_user);
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

}

sub _create_new_ant_nest_booking{
    my ($self, $stash, $is_first_user) = @_;

    my $token;

    if ( $is_first_user == 1){
	$token = Bcd::Data::NewAntNests->create_new_ant_nest_booking
	    ($stash, $self->{id_ant_nest}, 
	     $self->{name_ant_nest});
    } 

    $token = Bcd::Data::NewAntNests->insert_a_new_looker
	($stash, $self->{id_ant_nest}, 
	 $self->{name_user},
	 $self->{email}
	 );


    #I should inform the ant and give to her the token
    $stash->get_mailer()->mail_this_message($stash, $self->{email},
					    'new_looker_ant.tt2', 
					    {
						name => $self->{name_user},
						id_ant_nest => $self->{id_ant_nest},
						name_ant_nest => $self->{name_ant_nest},
					    }
					    );

}


#no specific output, only the exit code

1;
