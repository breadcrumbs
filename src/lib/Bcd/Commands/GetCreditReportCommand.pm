package Bcd::Commands::GetCreditReportCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;

use Bcd::Data::WebSite;

use constant NAME => "cad_get_credit_report";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no parameters, I have only the session
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash ) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});
    my $code  = $stash->get_session_post_code($self->{session_id});


    my ($credit, $debit, $frozen_debit, $score) = 
	Bcd::Data::CreditsAndDebits->get_ant_credit_summary($stash, $my_id);

    $self->{total_credit} = $credit;
    $self->{total_debit} = $debit;
    $self->{frozen_debit} = $frozen_debit;
    $self->{score} = $score;

    #Ok, then I should get the report of all the credits and debits of this ant
    #towards the other ants in the system.
    my $report = Bcd::Data::CreditsAndDebits->get_credit_reports_towards_ants
	($stash, $my_id, $code);

    $self->{former_frozen_output} = $report;

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $report = $self->{former_frozen_output};

    #and I add this report as a dataset...
    $self->_add_record_set($report, 'credit_report', $output);

    $self->_add_scalar("total_credit", $self->{total_credit}, $output);
    $self->_add_scalar("total_debit", $self->{total_debit}, $output);
    $self->_add_scalar("frozen_debit", $self->{frozen_debit}, $output);
    $self->_add_scalar("score", $self->{score}, $output);

    #ok, this is enough for now.

}

1;
