package Bcd::Commands::TestReceiveBlobCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SimpleCommand;
use base qw/ Bcd::Commands::SimpleCommand /;

use Bcd::Errors::ErrorCodes;
use Data::Dumper;
use Storable;
use Digest::SHA1;


use constant NAME => "tc_receive_blob";

sub get_name{
    return NAME;
}


#this command should simply test the "blob" capabilities of the server
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #my ($class, $stash, $code, $name_ant_nest, $name, $email, $want_to_be_founder) = @_;

    my $parameter = &share({});

    $parameter->{name} = "blob";
    $parameter->{type} = "multi_line";

    push (@{$self->{"parameters"}}, "hash_blob");
    push (@{$self->{"parameters"}}, "dummy");
    push (@{$self->{"parameters"}}, $parameter  );
    push (@{$self->{"parameters"}}, "check_dummy");

    #the two dummies must be the same

    bless ($self, $class);

    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #I should make the sha1sum of the blob...

    #first of all I should decode the blob
    #my $decoded = MIME::QuotedPrint::decode_qp($self->{blob});

    my $sha = Digest::SHA1->new;
    $sha->add($self->{blob});
    my $digest = $sha->hexdigest;

    #print "the sha is $digest expected $self->{hash_blob}\n";

    if (($self->{hash_blob} ne $digest) or ($self->{dummy} ne $self->{check_dummy})){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INTERNAL_ERROR;
	return;
    }
    
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


#no specific output, only the exit code

1;
