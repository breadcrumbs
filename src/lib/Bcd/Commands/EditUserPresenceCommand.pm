package Bcd::Commands::EditUserPresenceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::PresencesCommand;
use base(qq/Bcd::Commands::PresencesCommand/);
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Constants::PublicSitesConstants;

use constant NAME => "ws.edit_user_presence";

sub get_name{
    return NAME;
}

use constant PARAM_TABLE =>
    [
     ["id_locus", "on_request", "presence_text"],
     {
	 id_locus        => ["id_type",      "required"],
	 on_request      => ["boolean_type", "required"],
	 presence_text   => ["string_type",  "required"],
     },
     [Bcd::Commands::SessionCommand::PARAM_TABLE],
     ];


sub get_param_table{
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    my $input = $self->{_in}->get_input();
    my $my_id = $stash->get_session_id($input->{session_id});

    my $id_locus = $input->{id_locus};

    my ($ans, $presence_old) = 
	$self->_check_owner_and_free_state_of_presence_fail($stash, $id_locus);

    if ($ans == 1){
	return;
    }

    #ok, the locus is mine and it is "free"...

    #is it completely free?
    my $count = Bcd::Data::Invoices->get_invoices_count_for_this_locus($stash, $id_locus);

    if ($count == 0){
	#ok, it is completely free: simply an update!
	Bcd::Data::PublicSites->update_presence($stash, $id_locus, $input->{on_request},
						$input->{presence_text});
	
    } else {
	#no, there are some invoices, so create another presence and move the ads, please.
	
	#mmm is this a special or a normal presence?
	my $new_presence_id;

	if (defined($presence_old->{id_site})){
	    #normal presence
	    #my ($class, $stash, $user, $site, $reason, $on_request, $available_hours) = @_;
	    $new_presence_id = Bcd::Data::PublicSites->add_presence_in_site
		($stash, $my_id, $presence_old->{id_site}, $presence_old->{id_rel_type},
		 $input->{on_request}, $input->{presence_text});
	} else {
	    #special presence
	    $new_presence_id = Bcd::Data::PublicSites->add_special_site_presence
		($stash, $my_id, $presence_old->{special_site}, $presence_old->{id_rel_type},
		 $input->{on_request}, $input->{presence_text});
	}


	#ok, I have created the new presence: move all the ads to this new presence, please.
	Bcd::Data::Ads->change_locus_for_ads($stash, $new_presence_id, $id_locus);
    }


    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#No output, only the exit code is sufficient.

1;
