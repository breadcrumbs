package Bcd::Commands::GetMyAdDetailsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Storable;

use constant NAME => "ws.get_my_ad_details";

use constant PARAM_TABLE => 
    [
     ["id_ad"],
     {
	 id_ad    => ["id_type", "required"],
     },
     [Bcd::Commands::SessionCommand::PARAM_TABLE],
     ];


sub get_name{
    return NAME;
}

sub get_param_table(){
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;
    my $input = $self->{_in}->get_parsed_result();
    my $my_id = $stash->get_session_id($input->{session_id});

    #Ok, I should try to get the ad
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $input->{id_ad});
    if (!defined($ad)){
	$self->{exit_code} =  Bcd::Errors::ErrorCodes::BEC_NO_DATA;
	return;
    }

    #the ad must belong to me
    if ($ad->{id_user} != $my_id){
	$self->{exit_code} =  Bcd::Errors::ErrorCodes::BEC_YOU_DO_NOT_OWN_THIS_AD;
	return;
    }

    #the ad must be active? no, it could be also in another state

    #I also get the ad with the ad_type hash
    my $ad_type = Bcd::Data::Ads->decode_ad_type_from_activity($ad->{id_act});
    $ad->{ad_type} = $ad_type;

    my $act_str = Bcd::Data::Activities->get_all_parents_str($stash, $ad->{id_act});
    $ad->{activity} = $act_str;

    #then I build the dataset of the presences

    #locus, p_min, p_max, t_e, t_c
    my $ads_arr = Bcd::Data::Ads->get_all_localities_for_ad_arr
	($stash, $input->{id_ad});

    #ok, Now I try to build the recordset
    my @dataset;
    my @fields = ('id_locus', 'p_min', 'p_max', 't_e', 't_c', 
		  'presence_type', 'id_site', 'special_site', 'on_request', 'presence_text', 'site_name');

    push(@dataset, \@fields);

    foreach(@{$ads_arr}){

	my @row;

	#I should convert the trusts in bBel
	$_->[3] = Bcd::Data::Trust::bBel($_->[3]);
	$_->[4] = Bcd::Data::Trust::bBel($_->[4]);

	#the firse fields are from the ads_user_localities table
	foreach(@{$_}){
	    push(@row, $_);
	}

	my $id_presence = $_->[0];
	
	my $presence = Bcd::Data::PublicSites->get_presence_decoded_hash
	    ($stash, $id_presence);

	
	push(@row, $presence->{presence_type});
	push(@row, $presence->{id_site});
	push(@row, $presence->{special_site});
	push(@row, $presence->{on_request});
	push(@row, $presence->{presence});
	push(@row, $presence->{site_name});

	push(@dataset, \@row);
    }



    $self->{output}  = $ad;
    $self->{ads_loc} = \@dataset;

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_object    ($self->{output}, "ad", $output);
    $self->_add_record_set($self->{ads_loc},"ads_loc", $output);
}

1;
