package Bcd::Commands::WriteToBossCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use base(qq/Bcd::Commands::SimpleCommand/);
use Bcd::Data::Users;
use Data::Dumper;

use constant NAME => "an.write_to_boss";

sub get_name{
    return NAME;
}

use constant PARAM_TABLE => 
    [
     ["id_ant_nest", "subject", "comment"],
     {
	 id_ant_nest => ["id_type", "required"],
	 subject     => ["string_type", "required"],
	 comment     => ["string_type", "required"],
     },
     ];


sub get_param_table{
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    my $input = $self->{_in}->get_input();

    #I should only take the boss of this ant nest
    #ok, let's get the boss
    my $boss = Bcd::Data::Users->get_boss_of_this_ant_nest
	($stash, $input->{id_ant_nest});

    
    my $template_vars = {
	subject => $input->{subject},
	comment => $input->{comment},
    };

    $self->_send_mail_to_user_with_id
	($stash, "write_to_boss.tt2", $boss->{id}, $template_vars);
    
    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#no output, only the exit code...

1;
