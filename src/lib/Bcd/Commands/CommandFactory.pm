package Bcd::Commands::CommandFactory;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#use Log::Log4perl qw(:easy);
#Log::Log4perl->easy_init($INFO);

use Bcd::Commands::ConnectCommand;
use Bcd::Commands::TestCommand;
use Bcd::Commands::AntNestsListByCodeCommand;
use Bcd::Commands::DisconnectCommand;
use Bcd::Commands::AntNestGetDetailCommand;
use Bcd::Commands::CreateAntNestCommand;
use Bcd::Commands::InitDbCommand;
use Bcd::Commands::CreateInitialUserCommand;
use Bcd::Commands::GrantRoleCommand;
use Bcd::Commands::CreateNormalUserCommand;
use Bcd::Commands::ConfirmNewUserCommand;
use Bcd::Commands::ConfirmMyTutorsCommand;
use Bcd::Commands::GetNewUserDetailsCommand;
use Bcd::Commands::GetTutorsDetailsCommand;
use Bcd::Commands::NewTrustCommand;
use Bcd::Commands::ChangeTrustCommand;
use Bcd::Commands::RollbackTrustChangeCommand;
use Bcd::Commands::CommitTrustChangeCommand;
use Bcd::Commands::GetUserDetailsCommand;
use Bcd::Commands::GetTrustedAntsCommand;
use Bcd::Commands::GetReachableSetCommand;
use Bcd::Commands::CreateDepositBookingCommand;
use Bcd::Commands::TreasurerAcknowledgedCommand;
use Bcd::Commands::BcConnectCommand;
use Bcd::Commands::GetAntNestsSummaryCommand;
use Bcd::Commands::GetNewAntNestDetailsCommand;
use Bcd::Commands::CreateNewFounderCommand;
use Bcd::Commands::CreateNewLookerCommand;
use Bcd::Commands::GetNearestAntNestCommand;
use Bcd::Commands::TestReceiveBlobCommand;
use Bcd::Commands::ConfirmFounderCommand;
use Bcd::Commands::GetNewAntNestsSummaryCommand;
use Bcd::Commands::GetMyProfileCommand;
use Bcd::Commands::UpdatePersonalUserDataCommand;
use Bcd::Commands::UpdateFounderPersonalDataCommand;
use Bcd::Commands::GetFoundersSummaryCommand;
use Bcd::Commands::GetFounderProfileCommand;
use Bcd::Commands::CreateFounderTrustsCommand;
use Bcd::Commands::UpdateCandidatesFlagsCommand;
use Bcd::Commands::CreateFounderVotingCommand;
use Bcd::Commands::GetCandidatesSummaryCommand;
use Bcd::Commands::CreateFounderHQCommand;
use Bcd::Commands::TreasurerInitialSetupCommand;
use Bcd::Commands::GetAntNestHQCommand;
use Bcd::Commands::LookupDepositBookingCommand;
use Bcd::Commands::ClaimDepositCommand;
use Bcd::Commands::CreateWithdrawalBookingCommand;
use Bcd::Commands::LookupWithdrawalBookingCommand;
use Bcd::Commands::LookupMyBookingCommand;
use Bcd::Commands::DeletePendingRequestCommand;
use Bcd::Commands::GetCashUserEuroSummaryCommand;
use Bcd::Commands::GetCashAntNestEuroSummaryCommand;
use Bcd::Commands::GetCashAntNestBookingsSummaryCommand;
use Bcd::Commands::ClaimWithdrawalCommand;
use Bcd::Commands::SendMessageToFoundersCommand;
use Bcd::Commands::GetOtherAntProfileCommand;
use Bcd::Commands::TreasurerOfflineAcknowledgeCommand;
use Bcd::Commands::ChangeEuroInTaoCommand;
use Bcd::Commands::GetCashUserTaoSummaryCommand;
use Bcd::Commands::ChangeTaoInEuroCommand;
use Bcd::Commands::GetUserTrustNetsCommand;
use Bcd::Commands::InitActivitiesTableCommand;
use Bcd::Commands::GetActivitiesTreeCommand;
use Bcd::Commands::InsertPersonalUsersDataTrustsCommand;
use Bcd::Commands::ChangePersonalDataTrustCommand;
use Bcd::Commands::GetPriceForThisAdCommand;
use Bcd::Commands::CreateUserInSitePresenceCommand;
use Bcd::Commands::CreateAdCommand;
use Bcd::Commands::GetMyAdsCommand;
use Bcd::Commands::GetAdsCommand;
use Bcd::Commands::ClaimDepositCommandBlinded;
use Bcd::Commands::GetAntNestPublicSitesCommand;
use Bcd::Commands::GetPresencesCommand;
use Bcd::Commands::GetActivitiesTreeWithAdsSummaryCommand;
use Bcd::Commands::GetAdCommand;
use Bcd::Commands::GetCreditReportCommand;
use Bcd::Commands::GetPriceEstimateCommand;
use Bcd::Commands::AuthorizeInvoiceCommand;
use Bcd::Commands::EmitInvoiceCommand;
use Bcd::Commands::PayInvoiceCommand;
use Bcd::Commands::CollectInvoiceCommand;
use Bcd::Commands::BuyObjectCommand;
use Bcd::Commands::GetInvoicesSummaryCommand;
use Bcd::Commands::GetInvoicesListByStateCommand;
use Bcd::Commands::GetInvoiceBlindedTokenCommand;
use Bcd::Commands::GetPriceEstimateForInvoiceCommand;
use Bcd::Commands::ChangePasswordCommand;
use Bcd::Commands::ChangeTotemCommand;
use Bcd::Commands::GetMyAdDetailsCommand;
use Bcd::Commands::AddLocusToAdCommand;
use Bcd::Commands::GetPriceForAddingPresenceCommand;
use Bcd::Commands::GetSiteDetailsCommand;
use Bcd::Commands::GetInvoiceDetailsCommand;
use Bcd::Commands::GetUserPresencesSummaryCommand;
use Bcd::Commands::GetAdsInLocusCommand;
use Bcd::Commands::WriteToBossCommand;
use Bcd::Commands::GetSimilarPresencesCommand;
use Bcd::Commands::MoveAdsUserPresenceCommand;
use Bcd::Commands::EditUserPresenceCommand;
use Bcd::Commands::ChangeSiteInUserPresenceCommand;
use Bcd::Commands::TestSendMailCommand;

use Data::Dumper;
use Bcd::Common::CommonConstants;

#this is used by the factory.
#it is not used in the multithreaded environment, only for the tests
#which use the legacy code... of course the mode is text, and NOT SCRIPT
my $shared_input = Bcd::Commands::InputCommandParser->new
    (Bcd::Common::CommonConstants::TEXT_MODE, 0);

#this function simply gets a command with a particular name
sub get_command{
    my ($self, $name, $id, $in, $out) = @_;

    #lock($self);
    my $input = defined($in) ? $in : $shared_input;
    my $id_d  = defined($id) ? $id : 0;

    my $command_entry = $self->{command_table}->{$name};    

    if(!defined($command_entry)){
	#get_logger()->warn("undefined command $name requested");
 	return undef;
    } else {
	return $self->_internal_commands_factory
	    ($command_entry->{name_of_new}, $command_entry->{complete_name},
	     $id_d, $input, $out);
    }

}

sub register_command{
    my ($self, $name) = @_;

    my $complete_name = "Bcd::Commands::" . $name;
    my $name_of_new   = $complete_name . "::new";
    
    no strict 'refs';
    my $get_name_function = $complete_name . "::NAME";
    my $out_name = $get_name_function->();
    use strict;

    #Now I can register the command in the factory with his name

    my $command_struct = {};
    
    $command_struct->{name_of_new}   = $name_of_new;
    $command_struct->{complete_name} = $complete_name;
    $self->{command_table}->{$out_name} = $command_struct;
}

sub _internal_commands_factory{
    my ($self, $name_of_new, $complete_name, $id, $in, $out) = @_;

    no strict 'refs';

    my $cmd = $name_of_new->
	($complete_name, $id, $in, $out);

    #return strict, please
    use strict;

    return $cmd;
}

sub new{

    #I create the factory
    my $class = shift;
    my $self;

    $self = {};
    bless ($self, $class);

#    $self->{id_cmd} = 0;

    my $command_table = {};
    $self->{command_table} = $command_table;

    #I could register all the commands...
    $self->register_command("ConnectCommand");
    $self->register_command("DisconnectCommand");


    $self->register_command("AddLocusToAdCommand");

    $self->register_command("AntNestsListByCodeCommand");
    $self->register_command("AntNestGetDetailCommand");

    $self->register_command("AuthorizeInvoiceCommand");

    $self->register_command("BcConnectCommand");
    $self->register_command("BuyObjectCommand");

    $self->register_command("ChangeEuroInTaoCommand");
    $self->register_command("ChangePasswordCommand");
    $self->register_command("ChangePersonalDataTrustCommand");
    $self->register_command("ChangeSiteInUserPresenceCommand");
    $self->register_command("ChangeTaoInEuroCommand");
    $self->register_command("ChangeTotemCommand");
    $self->register_command("ChangeTrustCommand");

    $self->register_command("ClaimDepositCommand");
    $self->register_command("ClaimDepositCommandBlinded");
    $self->register_command("ClaimWithdrawalCommand");

    $self->register_command("CollectInvoiceCommand");
    $self->register_command("CommitTrustChangeCommand");
    $self->register_command("ConfirmFounderCommand");
    $self->register_command("ConfirmNewUserCommand");
    $self->register_command("ConfirmMyTutorsCommand");

    $self->register_command("CreateAdCommand");
    $self->register_command("CreateAntNestCommand");
    $self->register_command("CreateDepositBookingCommand");
    $self->register_command("CreateFounderHQCommand");
    $self->register_command("CreateFounderTrustsCommand");
    $self->register_command("CreateFounderVotingCommand");
    $self->register_command("CreateInitialUserCommand");
    $self->register_command("CreateNewFounderCommand");
    $self->register_command("CreateNewLookerCommand");
    $self->register_command("CreateNormalUserCommand");
    $self->register_command("CreateUserInSitePresenceCommand");
    $self->register_command("CreateWithdrawalBookingCommand");

    $self->register_command("DeletePendingRequestCommand");

    $self->register_command("EditUserPresenceCommand");
    $self->register_command("EmitInvoiceCommand");

    $self->register_command("GetActivitiesTreeCommand");
    $self->register_command("GetActivitiesTreeWithAdsSummaryCommand");
    $self->register_command("GetAdsInLocusCommand");
    $self->register_command("GetAntNestHQCommand");
    $self->register_command("GetCandidatesSummaryCommand");
    $self->register_command("GetCashAntNestBookingsSummaryCommand");
    $self->register_command("GetCashAntNestEuroSummaryCommand");

    $self->register_command("GetCashUserEuroSummaryCommand");
    $self->register_command("GetCashUserTaoSummaryCommand");

    $self->register_command("GetCreditReportCommand");

    $self->register_command("GetAdCommand");
    $self->register_command("GetAdsCommand");
    $self->register_command("GetAntNestPublicSitesCommand");
    $self->register_command("GetAntNestsSummaryCommand");
    $self->register_command("GetMyAdDetailsCommand");
    $self->register_command("GetMyAdsCommand");
    $self->register_command("GetMyProfileCommand");
    $self->register_command("GetFoundersSummaryCommand");
    $self->register_command("GetFounderProfileCommand");
    $self->register_command("GetInvoiceBlindedTokenCommand");
    $self->register_command("GetInvoiceDetailsCommand");
    $self->register_command("GetInvoicesSummaryCommand");
    $self->register_command("GetInvoicesListByStateCommand");

    $self->register_command("GetNearestAntNestCommand");
    $self->register_command("GetNewAntNestDetailsCommand");
    $self->register_command("GetNewAntNestsSummaryCommand");
    $self->register_command("GetNewUserDetailsCommand");

    $self->register_command("GetPresencesCommand");
    $self->register_command("GetPriceForThisAdCommand");
    $self->register_command("GetOtherAntProfileCommand");
    $self->register_command("GetPriceEstimateCommand");
    $self->register_command("GetPriceEstimateForInvoiceCommand");
    $self->register_command("GetPriceForAddingPresenceCommand");
    $self->register_command("GetReachableSetCommand");
    $self->register_command("GetSimilarPresencesCommand");
    $self->register_command("GetSiteDetailsCommand");
    $self->register_command("GetTrustedAntsCommand");
    $self->register_command("GetTutorsDetailsCommand");

    $self->register_command("GetUserDetailsCommand");
    $self->register_command("GetUserPresencesSummaryCommand");
    $self->register_command("GetUserTrustNetsCommand");

    $self->register_command("GrantRoleCommand");

    $self->register_command("InitDbCommand");
    $self->register_command("InitActivitiesTableCommand");

    $self->register_command("InsertPersonalUsersDataTrustsCommand");

    $self->register_command("LookupDepositBookingCommand");
    $self->register_command("LookupMyBookingCommand");
    $self->register_command("LookupWithdrawalBookingCommand");

    $self->register_command("MoveAdsUserPresenceCommand");

    $self->register_command("NewTrustCommand");

    $self->register_command("PayInvoiceCommand");

    $self->register_command("RollbackTrustChangeCommand");

    $self->register_command("SendMessageToFoundersCommand");

    $self->register_command("TestCommand");
    $self->register_command("TestReceiveBlobCommand");
    $self->register_command("TestSendMailCommand");
    $self->register_command("TreasurerAcknowledgedCommand");
    $self->register_command("TreasurerInitialSetupCommand");
    $self->register_command("TreasurerOfflineAcknowledgeCommand");

    $self->register_command("UpdatePersonalUserDataCommand");
    $self->register_command("UpdateFounderPersonalDataCommand");
    $self->register_command("UpdateCandidatesFlagsCommand");

    $self->register_command("WriteToBossCommand");

    return $self;
}

1;
