package Bcd::Commands::CreateAntNestBookingRequestCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::SimpleCommand;
use base qw/ Bcd::Commands::SimpleCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::AntNests;
use Data::Dumper;
use Storable;


#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);


    #no privileges required
    push (@{$self->{"parameters"}}, "id_ant_nest");
    push (@{$self->{"parameters"}}, "name_ant_nest");
    push (@{$self->{"parameters"}}, "name_user");
    push (@{$self->{"parameters"}}, "email");

    bless ($self, $class);

    return $self;
}

#here is ok
sub _checks_when_new_ant_nests_is_present_fail{
    return 0;
}

sub _exec{
    my ($self, $stash) = @_;

    #if these checks fail is futile to continue
    if ($self->_common_checks_fail($stash)){
	return;
    }

    #ok, now I should see if exists already a booking for this ant nest.
    my $new_ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_hash
	($stash, $self->{id_ant_nest});

    my $is_first_user;

    if (defined ($new_ant_nest)){

	if ($self->_checks_when_new_ant_nests_is_present_fail($stash, $new_ant_nest)){
	    return;
	}

	$is_first_user = 0;

    } else {

	#this is a new booking..., let's see if exists already a working ant nest in the same code
	$is_first_user = 1;

	#I should build the search range
	my ($min_code, $max_code) = 
	    Bcd::Data::PostCode::get_min_max_search_range($self->{id_ant_nest});


# 	my $count = Bcd::Data::AntNests->get_count_ant_nests_min_max
# 	    ($stash, $min_code, $max_code);

	my $ant_nests = Bcd::Data::AntNests->get_codes_in_this_range_arr($stash, $min_code, $max_code);
	
	my $real_ant_nest_count = 0;

	foreach(@{$ant_nests}){
	    if ( ! Bcd::Data::PostCode::is_this_a_test_post_code($_->[0])){
		$real_ant_nest_count ++;
	    }
	}

	#there is one or more real ant nest in this code... I should inform... etc...
	if ($real_ant_nest_count > 0){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NOT_IMPLEMENTED;
	    return;
	}

    }

    $self->_create_new_ant_nest_booking($stash, $is_first_user);
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}



#this is a list of common checks to be made
sub _common_checks_fail{

    my ($self, $stash) = @_;

    #print "you want to create an ant nest in $self->{id_ant_nest}\n";
    
    #ok, first of all a check on the parameters.
    if ($self->post_code_without_sub_validity_fail($self->{id_ant_nest})){
	return 1;
    }

    my ($ans, $city) = Bcd::Data::PostCode::is_this_a_generic_post_code($self->{id_ant_nest});
    if ($ans == 1){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_CANNOT_CREATE_AN_ANT_NEST_IN_GENERIC_CODE;
	$self->{error_string} = $city;
	return 1;
    }
}

#no specific output, only the exit code

1;
