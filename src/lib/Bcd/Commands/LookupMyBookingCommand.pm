package Bcd::Commands::LookupMyBookingCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::LookupDepositWithdrawalBookingCommand;
use base(qq/Bcd::Commands::LookupDepositWithdrawalBookingCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use Data::Dumper;

use constant NAME => "bk.lookup_my_booking";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no parameters, only the session...

    #I lower the privilege required for this command
    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}


sub _exec{
    my ($self, $stash) = @_;

    #I simply have to get the booking for this user
    my $my_id = $stash->get_session_id($self->{session_id});

    my $booking = Bcd::Data::Deposits->get_pending_booking_from_user_id_arr
	($stash, $my_id);

    if (! defined($booking)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_DATA;
	return;
    }

    #ok, it is a deposit, so I can return it to the sender
    $self->_put_this_booking_in_output($booking);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

}


1;
