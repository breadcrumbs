package Bcd::Commands::GetNewAntNestDetailsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SimpleCommand;
use base qw/ Bcd::Commands::SimpleCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::NewAntNests;
use Data::Dumper;
use Storable;

use constant NAME => "na_details";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no privileges required
    push (@{$self->{"parameters"}}, "id_ant_nest");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #just a valid post code...
    if ($self->post_code_without_sub_validity_fail($self->{id_ant_nest})){
	return;
    }

    #if there isn't a booking it is futile to continue
    my $hash = Bcd::Data::NewAntNests->select_new_ant_nest_booking_hash
	($stash, $self->{id_ant_nest});

    if (!defined($hash)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_DATA;
	return;
    }
    
    $self->{output} = {};
        
    $self->{output}->{proposed_name} = $hash->{proposed_name};
    $self->{output}->{state}         = $hash->{state};

    #ok, now I have a valid post code...
    my $count = Bcd::Data::NewAntNests->get_ant_nest_founders_count
	($stash, $self->{id_ant_nest});
    $self->{output}->{founders_count} = $count;

    my $total = $count;

    $count = Bcd::Data::NewAntNests->get_ant_nest_lookers_count
	($stash, $self->{id_ant_nest});
    $self->{output}->{lookers_count} = $count;

    $total += $count;

    $self->{output}->{total_interested} = $total;

    #then the confirmed founders count
    $count = Bcd::Data::NewAntNests->get_confirmed_founders_count
	($stash, $self->{id_ant_nest});
    $self->{output}->{confirmed_founders_count} = $count;

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output {
    my ($self, $output) = @_;

    $self->_add_output_hash($output);
}


1;
