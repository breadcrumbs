package Bcd::Commands::CollectInvoiceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Bcd::Constants::AdsConstants;

use constant NAME => "ws_collect_invoice";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #ok, I have the id of the invoice and the secret token...
    push (@{$self->{parameters}}, "id_invoice");
    push (@{$self->{parameters}}, "secret_token");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});
    
    #let's check only that the invoice correspond to an ad
    my $invoice = Bcd::Data::Invoices->get_invoice_hash($stash, $self->{id_invoice});

    #ok, now I get the id of the ad
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $invoice->{id_ad});
    #my $loc = Bcd::Data::Ads->get_ad_locality_hash($stash, $invoice->{id_ad}, $invoice->{id_locus});

    #ok, the user must own the ad...
    if ($ad->{id_user} != $my_id){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_YOU_DO_NOT_OWN_THIS_AD;
	return;
    }

    #the invoice must be in the correct state
    if ($invoice->{id_status} != Bcd::Constants::InvoicesConstants::PAID){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #Ok, now I should check the validity of the secret code
    my $inv_token = Bcd::Data::Invoices->get_invoice_token_hash($stash, $self->{id_invoice});

    my $sha = Digest::SHA1->new;
    $sha->add($self->{secret_token});
    my $digest = $sha->hexdigest;

    if ($digest ne $inv_token->{secret_token}){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN;
	return;
    }

    my $total_due = $invoice->{amount} * $invoice->{quantity};

    #ok, now the token is correct, I can thaw the credits...
    if ($inv_token->{amount_paid_by_credit} > 0){
	Bcd::Data::CreditsAndDebits->ant_thaws_credit
	    ($stash, $my_id, $invoice->{user_to}, 
	     $inv_token->{amount_paid_by_credit});
    }

    #now I can thaw the amount paid in convertible taos...
    my $convertible_taos = $total_due - $inv_token->{amount_paid_by_credit};

    if ($convertible_taos > 0){
	Bcd::Data::Bank->ant_collects_invoice($stash, $my_id, $self->{id_invoice}, $convertible_taos);
    }

    #then I collect the invoice
    Bcd::Data::Invoices->collect_invoice($stash, $self->{id_invoice});

    #mmm, maybe I should now send a receipt for the movement
    my $template_vars = {
	convertible_tao      => $convertible_taos,
	total_due            => $total_due,
	paid_by_credit       => $inv_token->{amount_paid_by_credit},
	invoice              => $invoice,
	ad                   => $ad,
    };

    $self->_send_mail_to_user_with_id
	($stash, "invoice_collected.tt2", $my_id, $template_vars);

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#no specific output

1;
