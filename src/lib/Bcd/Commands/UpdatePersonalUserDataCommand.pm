package Bcd::Commands::UpdatePersonalUserDataCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Data::Users;
use Bcd::Errors::ErrorCodes;


#TEMPORARY COMMAND, BECAUSE THE USERS DO NOT CHANGE THE DATA IN THIS WAY
use constant NAME => "us_update_personal_data";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I need a simple ant role to update my data...
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I need the tutor trust parameter
    push (@{$self->{parameters}}, "first_name");
    push (@{$self->{parameters}}, "last_name");
    push (@{$self->{parameters}}, "address");
    push (@{$self->{parameters}}, "home_phone");
    push (@{$self->{parameters}}, "mobile_phone");
    push (@{$self->{parameters}}, "sex");
    push (@{$self->{parameters}}, "birthdate");
    push (@{$self->{parameters}}, "email");
    push (@{$self->{parameters}}, "identity_card_id");

    bless ($self, $class);
    return $self;
}

#TEMPORARY FUNCTION....
sub _exec{
    my ($self, $stash) = @_;
    my $id_user = $stash->get_session_id($self->{session_id});
    $self->_do_change_personal_data($stash, $id_user);
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _do_change_personal_data{
    my ($self, $stash, $id_user) = @_;

    #a function makes all
    Bcd::Data::Users->update_personal_data_from_user_id($stash, $id_user, $self);

}

#no specific output, only the exit code

1;
