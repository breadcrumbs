package Bcd::Commands::ConnectCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use Bcd::Errors::ErrorCodes;
use Bcd::Commands::GenericConnectCommand;
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Common::CommonConstants;
use Bcd::Data::Model;
use Bcd::Constants::NewAntNestsConstants;
use Bcd::Commands::InputCommandParser;

our @ISA = qw(Bcd::Commands::GenericConnectCommand);

use constant NAME => "sv_connect";

use constant PARAM_TABLE => 
    [
     ["user", "ant_nest", "password"],
     {
	 user     => ["nick_type",                      "required"],
	 ant_nest => ["post_code_with_sub_or_not_type", "required"],
	 password => ["string_type",                    "required"],
     },
     Bcd::Commands::GenericConnectCommand->get_param_table(),
     ];


sub get_name(){
    return NAME;
}
sub get_param_table(){
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.

    my ($self, $stash) = @_;

    my $parsed_result = $self->{_in}->get_parsed_result();

    if ( $parsed_result->{user} eq Bcd::Common::CommonConstants::BC_ROOT){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_RESERVED_ID;
	return;
    }

    #if the post code is not valid it is useless to continue
    my $code = $parsed_result->{ant_nest};

    if (Bcd::Data::PostCode::is_valid_post_code($code)){
	#this is a normal post code, so the ant maybe is a founder
	#let's see the state of this code
	my $new_ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr
	    ($stash, $code);
	if (! defined($new_ant_nest)){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_ANT_NEST_IN_THIS_CODE;
	    return;
	}

	#if the ant nest has been created deny the login
	if ($new_ant_nest->[1] == Bcd::Constants::NewAntNestsConstants::CREATED){
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_ANT_NEST_HAS_BEEN_CREATED;
	    return;
	}

	$self->_founder_login($stash);
	return;
    }

    #this is a post code with the suffix, so this is a "normal" login

    #first of all I should check the ant nest state
    if ( ! Bcd::Data::AntNests->
	 is_this_ant_nest_active($stash, $parsed_result->{ant_nest})){
	#ko, this ant nest is not active
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_LOGIN_DENIED_ANT_NEST_NOT_READY;
	return;
    }

    my $user = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	($stash, $code, $parsed_result->{user});

    if (!defined($user)){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER;
	return;
    }

    #check the status of this ant
    if ($user->{users_state} != Bcd::Constants::Users::NORMAL_ACTIVE_ANT){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_ACCOUNT_NOT_READY;
	return;
    }

    $self->_generic_connect($stash);

}


1;

