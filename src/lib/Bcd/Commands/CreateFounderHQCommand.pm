package Bcd::Commands::CreateFounderHQCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Commands::CreatePublicSiteCommand;

use constant NAME => "na.create_hq";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ROLE_FOR_A_FOUNDER_BOSS;

#     my $ml_par = &share({});

#     $ml_par->{name} = "directions";
#     $ml_par->{type} = "multi_line";
    
#     #parameters relative to the public site
#     push (@{$self->{parameters}}, "name");
#     push (@{$self->{parameters}}, "location"); 
#     push (@{$self->{parameters}}, $ml_par);
#     push (@{$self->{parameters}}, "time_restricted");
#     push (@{$self->{parameters}}, "opening_hours");

    Bcd::Commands::CreatePublicSiteCommand->_add_your_parameters($self->{parameters});

    #parameters relative to the boss
    push (@{$self->{parameters}}, "request");
    push (@{$self->{parameters}}, "boss_hours");

    

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;


    my $id_user = $stash->get_session_id($self->{session_id});

    #I should be a boss which
    my $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $id_user);

    #print Dumper($founder);
    if ($founder->{id_status} != Bcd::Constants::FoundersConstants::BOSS_SET_UP_TO_DO){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #If I am here I have passed the security check, so, please insert the site...
    my $post_code = $stash->get_session_post_code($self->{session_id});

    my $new_ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr
	($stash, $post_code);

    my $assigned_code = $new_ant_nest->[4];

    my $site_id = Bcd::Commands::CreatePublicSiteCommand->_exec_embedded
	($stash, $self, $assigned_code);

#     my $site_id = Bcd::Data::Sites->create_a_site
# 	($stash, $self->{name}, 
# 	 $self->{location}, 
# 	 $self->{directions}, 
# 	 $self->{time_restricted}, 
# 	 $self->{opening_hours});

#     #ok, then I should create a correspondence between this site and
#     #the newly created ant nest

#     #ok, I can create the correspondence
#     Bcd::Data::Sites->create_a_public_ant_nest_site
# 	($stash, $assigned_code, $site_id);

    my $nick = $stash->get_session_nick($self->{session_id});


    #I should get my "new" id in the ant nest
    my $user = Bcd::Data::Users->select_basic_user_data_from_nick_nest_arr
	($stash, $assigned_code, $nick);

    #ok, then I can add the presence in this public site
    Bcd::Data::PublicSites->add_office_presence_in_site
	($stash, $user->[0], $site_id, $self->{request}, $self->{boss_hours});

    #I should update my state
    Bcd::Data::Founders->update_founder_state
	($stash, $id_user, Bcd::Constants::FoundersConstants::SET_UP_DONE);

    #Then I should also update the state of the treasurer
    my $treasurers = Bcd::Data::Founders->get_candidates_treasurers($stash, $post_code);

    #the treasurer id is the first field of the first row
    my $treasurer_id = $treasurers->[0]->[0];

    #ok, I can update his state
    Bcd::Data::Founders->update_founder_state
	($stash, $treasurer_id, Bcd::Constants::FoundersConstants::TREASURER_SET_UP_TO_DO);

    #I should also send an email
    my $treasurer = Bcd::Data::Founders->get_founder_with_email($stash, $treasurer_id);

    $stash->get_mailer()->mail_this_message($stash, $treasurer->[2],
					    'the_boss_has_completed_the_setup.tt2', 
					    {
						name => $treasurer->[1],
						nick => $treasurer->[0],
						id_ant_nest => $post_code,
						boss_nick => $nick,
					    }
					    );
    
    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#no output, only the exit code...
1;
