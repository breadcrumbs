package Bcd::Commands::SimpleCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Data::Dumper;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Users;
use Thread::Semaphore;
use Carp;
use MIME::QuotedPrint;

#I have some states for this command which acts like a virtual machine.

use constant{
    BEGINNING => 0,
    ACCEPTING_LINES => 1,
    LINE_TOO_LONG_IN_INPUT => 2,
    TOO_MANY_LINES => 3,
    TOO_FEW_LINES => 4,
    READY_TO_EXECUTE => 5,
    DONE => 6,
    RETRIEVING_OUTPUT => 7,
    NO_MORE_OUTPUT => 8,
    INVALID_STATE => 9 # this is used to mark a state which has no exit
    };

#to easy the migration
sub get_param_table{
    undef;
}

sub new(){
    my ($class, $id, $in, $out) = @_;

    my $self = {};

    #the first parameter is the id
    $self->{_in}  = $in;
    $self->{_out} = $out;

    #this is the index into which I parse the parameters array.
    $self->{"index"} = 0;
    $self->{"state"} = DONE;
    $self->{parameters} = [];
    $self->{"exit_code"} = undef;

    #The simple command require only guest access, more restricted
    #commands can of course reset this
    $self->{"privilege_required"} = Bcd::Data::Users::GUEST_ROLE;

    $self->{"id_cmd"} = $id;

    bless ($self, $class);
    return $self;
}

sub get_parameters_for_logging{
    my $self = shift;

    my $output = "";
    my $param_table = $self->get_param_table();

    if (defined($param_table)){
	#ok, this is a "new" command...
	my $param_list = $param_table->[0];
	my $result     = $self->{_in}->get_parsed_result();
	foreach(@{$param_list}){
	    my $value = $result->{$_};
	    if (defined($value)){
		$output .= " $_ => $value ";
	    } else {
		$output .= " $_ => undef ";
	    }
	}

    } else {

	foreach (@{$self->{parameters}}){
	    if (defined ($self->{$_})){
		$output .= " $_ => $self->{$_} ";
	    } else {
		$output .= " $_ => undef ";
	    }
	}
    }

    return $output;
}

# sub wait_for_execution{
#     my $self = shift;
#     #this will block the thread until done.
#     $self->{"end_flag"}->down();
# }

sub finish{
    my $self = shift; #the command is no more necessary...
    
    Bcd::Data::Model->instance()->get_factory()->delete_command($self->{"id_cmd"});
}

#this function is called whenever the transmission is ended
sub eot{
    my $self = shift;

    #if the command is "new" pass this message to the input parser.
    my $param_table = $self->get_param_table();
    if (defined($param_table)){
	croak("no eot for new commands! please compose the input yourself\n");
    }

#print Dumper($self);

    my $current_index = $self->{"index"};
    my $num_of_parameters = @{$self->{"parameters"}};

    #if the last parameter is an ellipsis the actual number of
    #parameters is one less
    my $last_parameter = $self->{parameters}->[$num_of_parameters - 1];

    if (ref ($last_parameter) eq "HASH"){
	if ($last_parameter->{type} eq "ellipsis"){
	    $num_of_parameters--;
	}
    }

    if ($current_index == $num_of_parameters){
	#ok I have received all the parameters, I am ready to be executed
	$self->{"state"} = READY_TO_EXECUTE;
    } elsif ( $current_index < $num_of_parameters){
	#too few parameters
	$self->{"state"} = TOO_FEW_LINES;
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_TOO_FEW_PARAMETERS;
    }
}

#this function should check the presence of all the parameters...
sub check_presence_of_parameters{
}

# This method simply check if the user has sufficient privileges to
# execute this command
sub _role_check{
    my ($self, $stash) = @_;

    if ( $self->{"privilege_required"} == Bcd::Data::Users::GUEST_ROLE){
	return 1; #the guest role is always granted.
    }

    my $role_in_the_session = $self->_get_session_value($stash, "user_role");

    #no session, maybe the session is outdated
    if ( !defined($role_in_the_session)){
	return 0;
    }

    my $privilege_required = $self->{privilege_required};

    #ok, now lets' see if the role in the session is sufficient
    #I can make a simple bit and.
    if ( $role_in_the_session & $self->{privilege_required}){
	return 1;
    }
    else {
	return 0; #role insufficient
    }
}

#this is called ONLY for the new commands...
#I pass the input hash
sub exec_for_test{
    my ($self, $stash, $input) = @_;

    #I have only to inject the parsed input
    $self->{_in}->{parsed_result} = $input;
    #I inject also my param table
    $self->{_in}->{param_table} = $self->get_param_table();
    #I validate this input
    $self->{_in}->_validate_params();

    if ($self->{_in}->{parsing_result} != Bcd::Errors::ErrorCodes::BEC_OK){
	$self->{exit_code} = $self->{_in}->{parsing_result};
	return;
    }

    if ( ! $self->_role_check($stash)){
	#insufficient privileges
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED;
    } else {
	#Ok! role is sufficient, exec, but ignore the transaction, please.
	$self->_do_exec($stash, 1);
    }

    $self->{state} = DONE;

}

#This method should simply ignore the transaction.
sub exec_only_for_test{
    my ($self, $stash) = @_;

    #this is valid ONLY for 
    $self->exec($stash, 1); #Ignore the transaction...
}

#This method tries to execute the command, if it is in its right state
sub exec{
    my ($self, $stash, $ignore_transaction) = @_;

    #change... in the exec I will also parse the parameters.
    my $can_execute = 1;
    $self->{_in}->new_command_from_input($self->get_name());

    if (!$ignore_transaction){
	my $param_table = $self->get_param_table();
	if (defined($param_table)){
	    #this is a "new" command
	    $self->{_in}->select_command(undef); #just to say that this is a new command
	    $self->{_in}->start_parsing_command($param_table);
	    $self->{_in}->parse_input();
	    if ($self->{_in}->{parsing_result} != Bcd::Errors::ErrorCodes::BEC_OK){
		$self->{exit_code} = $self->{_in}->{parsing_result};
		$can_execute = 0;
	    }

	} else {
	    #this is an "old" command, so I use the cmd as the suppor
	    $self->{_in}->select_command($self);
	    $self->{_in}->parse_input();

	    #this is the "old" condition
	    if ($self->{state} != READY_TO_EXECUTE){
		$can_execute = 0;
	    }
	}
    } else {

	my $param_table = $self->get_param_table();
	if (defined($param_table)){
	    croak("no exec_only_for_test for new commands! please use the exec_for_test instead!\n");
	}
	if ($self->{state} != READY_TO_EXECUTE){
	    $can_execute = 0;
	}
    }


    if ($can_execute == 1){

	if ( ! $self->_role_check($stash)){
	    #insufficient privileges
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED;
	} else {
	    #Ok! role is sufficient
	    $self->_do_exec($stash, $ignore_transaction);
	}
    }

    #in any case I write the result to the output.
    if (!$ignore_transaction){
	my $output = $self->get_output();
	$self->{_out}->write_output($output);
    }

    #in any case the state is "done"... the parameters were ok,
    #you were only not allowed to use them
    $self->{"state"} = DONE;
}

sub _do_exec{
    my ($self, $stash, $ignore_transaction) = @_;

    #This function should simply call the internal _exec.
    #all the commands are transactional, the ignore_transaction flag
    #is set only by the test scripts.

    eval{
	$self->_exec($stash);
	$stash->get_connection()->commit() if !$ignore_transaction;
    };

    if ($@){
	print STDERR "Transation aborted in the server... error: $@\n";
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_ABORTED_TRANSACTION;
	eval{ 
	    $stash->get_connection()->rollback() if !$ignore_transaction;
	};
	if ($@){
	    #I cannot even rollback... this is very serious...
	    print STDERR "I cannot even rollback... error: $@\n";
	    $self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_INTERNAL_ERROR;
	    
	    #TODO... this thread should stop.
	}
    }
    
}

#In this class I simply put the exit code
sub get_output{
    my $self = shift;

    #the output is now a simple hash, which will be then serialized...
    my %output;

    $self->_add_exit_code(\%output);

    if ($self->{exit_code} == Bcd::Errors::ErrorCodes::BEC_OK){
	$self->_add_your_specific_output(\%output);
    } else {
	#no ok, let's see if there is an error string
	if (defined($self->{error_string})){
	    $self->_add_scalar("error_info", $self->{error_string}, \%output);
	}
    }

    return \%output;
}

#virtual, in this base does nothing
sub _add_your_specific_output{
}

sub get_exit_code{

    #this function should return the exit code. The exit code is
    #returned by the command. It is the semantically exit of the command.

    #some exit codes are for "syntactical" errors.

    my $self = shift;
    return $self->{"exit_code"};

}

sub parse_hash_parameter{
    my ($self, $line, $parameter_to_parse) = @_;

    #print "PARSING $line\n";

    #ok, I should get the type of this parameter

    if ($parameter_to_parse->{type} eq "multi_line"){
	#ok, this is a multi_line parameter

	if ($line =~ /^\.$/){
	    #a single dot... end of this data.
	    $self->{index}++;
	    if (!defined ($self->{encoded_blob})){
		$self->{$parameter_to_parse->{name}} = "";
	    } else {
		#i should decode the blob
		$self->{$parameter_to_parse->{name}} = MIME::QuotedPrint::decode_qp
		    (delete $self->{encoded_blob});
	    }
	    return;
	} elsif ( $line =~ /^\.\./){
	    $line = substr($line, 1);
	}
	
	if (!defined ($self->{encoded_blob})){
	    $self->{encoded_blob} = $line;
	} else {
	    $self->{encoded_blob} .= "\n" . $line;
	}
    } elsif ($parameter_to_parse->{type} eq "ellipsis"){
	#I put all the others parameters in a single list
	if (! exists($self->{ellipsis})){
	    $self->{ellipsis} = []; #empty array
	}

	push(@{$self->{ellipsis}}, $line);
	
    } else {
	#?? unknown type... this should not happen
    }

}

sub parse_line{
    my ($self, $line) = @_;

    my $param_table = $self->get_param_table();
    if (defined($param_table)){
	croak("no parse_line for new commands! please compose the input yourself\n");
    }

    my $parameter_to_parse = $self->{parameters}->[$self->{index}];

    if (ref ($parameter_to_parse) eq "HASH"){
	#Ok, new parameter.
	#handle it differently
	$self->parse_hash_parameter($line, $parameter_to_parse);
	return;
    }


    #if I have too many parameters... complain.
    my $current_index = $self->{"index"};
    my $num_of_parameters = @{$self->{"parameters"}};

    if ($current_index >= $num_of_parameters) {
	#no good... I thrash the parameters
	#too many parameters
	$self->{"state"} = TOO_MANY_LINES;
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_TOO_MANY_PARAMETERS;
    } else {
	#the parameter at the index is equal to the line actually read
	$self->{$self->{"parameters"}->[$self->{"index"}]} = $line;
    }

    $self->{"index"}++; #I advance in the index

}

sub start_parsing_command{
    my $self = shift;

    my $param_table = $self->get_param_table();
    if (defined($param_table)){
	croak("no start_parsing_command for new commands! please compose the input yourself\n");
	return;
    } else {
	#legacy code
	$self->{index} = 0;
	#if there was an ellipsis erase it
	delete $self->{ellipsis};
    }
}

#this is poor hack, just for this first version...
sub _add_dumped_object{
    my ($self, $object, $ob_name, $output) = @_;

#     my $row = "$ob_name | ! | ";
#     push (@{$output}, $row);

#     #then I dump the object in ONE big row! THIS IS IMPORTANT ONLY ONE ROW
#     $Data::Dumper::Indent = 0;
#     my $row_dumped = Dumper($object);
#     push (@{$output}, $row_dumped);
#     $Data::Dumper::Indent = 2;

    $output->{$ob_name} = $object;
}

#these functions should add a recordset to the output of the command
sub _add_record_set{
    my ($self, $rs, $rs_name, $output) = @_;

#     #the first row of the recordset represent the fields
    
#     my $fields = shift (@{$rs});
#     my $count = $#{$rs}+1;
    
#     my $row = "$rs_name | * | $count";
#     push (@{$output}, $row);

#     #ok, the fields...
#     $row = '';
#     for (@{$fields}){
# 	$row .= "$_ | ";
#     }

#     push (@{$output}, $row);

    
#     for (@{$rs}){
# 	$row = '';
# 	foreach(@{$_}){
# 	    if (defined($_)){
# 		if ($_ eq 'BCDNULL'){
# 		    $_ = $_ . $_;
# 		}
# 	    } else {
# 		$_ = 'BCDNULL';
# 	    }
# 	    $row .= "$_ | ";
# 	}
# 	push (@{$output}, $row);
#     }

    $output->{$rs_name} = $rs;

    if (!exists($output->{__recordsets})){
	$output->{__recordsets} = [];
    }

    push(@{$output->{__recordsets}}, $rs_name);

}

#for now the object is modelled like a simple hash.
sub _add_object {
    my ($self, $object, $object_name, $output) = @_;

#     my $count= scalar(keys(%{$object}));
#     my $row = "$object_name | % | $count";
#     push (@{$output}, $row);

#     #ok, now I put the fields...
#     foreach(keys(%{$object})){
# 	$self->_add_scalar($_, $object->{$_}, $output);
#     }

    $output->{$object_name} = $object;

}

sub _add_scalar{
    my ($self, $scalar_name, $scalar, $output) = @_;

#     my $scalar_null = defined($scalar) ? $scalar : "";
#     my $line = "$scalar_name | \$ | $scalar_null";

#     push (@{$output}, $line);
    $output->{$scalar_name} = $scalar;
}

sub _add_output_hash{
    my ($self, $output) = @_;

    #ok, I simply have to iterate in the keys
    foreach(keys(%{$self->{output}})){
	$self->_add_scalar($_, $self->{output}->{$_}, $output);
    }
}

sub _add_exit_code{
    my ($self, $output) = @_;
    $self->_add_scalar("exit_code", $self->{exit_code}, $output);
}

####################################################
## some common validity checks (but I can do better)

sub post_code_without_sub_validity_fail{
    my ($self, $post_code) = @_;

    if ( ! Bcd::Data::PostCode::is_valid_post_code($post_code)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE;
	return 1;
    } else {
	return 0;
    }
}

sub post_code_validity_fail{
    my ($self, $post_code) = @_;

    if ( ! Bcd::Data::PostCode::is_valid_post_code_with_sub($post_code)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE;
	return 1;
    } else {
	return 0;
    }
}

sub trust_validity_fail{
    my ($self, $trust) = @_;

    if ( ! Bcd::Data::Trust->is_valid_trust($trust)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TRUST;
	return 1;
    } else {
	return 0;
    }
}

sub trust_bb_validity_fail{
    my ($self, $trust) = @_;

    if ( ! Bcd::Data::Trust->is_valid_trust_bb($trust)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TRUST;
	return 1;
    } else {
	return 0;
    }
}

sub _check_boolean_range_fail{
    my ($self, $parameter_name) = @_;

    if (exists($self->{$parameter_name})){
	my $par = $self->{$parameter_name};
	if ($par eq '1' or $par eq '0'){
	    return 0; #all ok.
	}
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_BOOLEAN_PARAMETER_OUT_OF_RANGE;
	return 1; #fail
    }

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_REQUIRED_PARAMETER_MISSING;
    return 1; #fail

}

###################
## simple mail to a user...

sub _send_mail_to_user_with_id{
    my ($self, $stash, $letter_to_send, $user, $template_vars) = @_;

    my $personal_data = Bcd::Data::Users->get_user_personal_data_arr
	($stash, $user);

    $template_vars->{name} = $personal_data->[1];

    $stash->get_mailer()->mail_this_message
	($stash, $personal_data->[8], $letter_to_send, $template_vars);
}

1;
