package Bcd::Commands::OutputCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Common::CommonConstants;
use Data::Dumper;
use Compress::Zlib;
use MIME::Base64;
use constant EOL => "\015\012";
use Storable qw(freeze);

sub new {
    my ($class, $mode) = @_;
    my $self  = {};
    $self->{mode}   = $mode;
    bless ($self, $class);
    return $self;
}

sub init_output{
    my $self = shift;
    $self->{output} = {};
}

# sub select_stream{
#     my ($self, $output) = @_;
#     $self->{_output} = $output;
# }

sub select_mode{
    my ($self, $mode) = @_;
    $self->{mode} = $mode;
}

sub write_output{
    my ($self, $output) = @_;

    #ok, I write the output...
    my $output_serialized;
    my $must_compress = 0;
    my $mode;
    local $/ = "\015\012";

    if ($self->{mode} == Bcd::Common::CommonConstants::TEXT_MODE){
	local $Data::Dumper::Indent = 1;
	local $Data::Dumper::Useqq  = 1;
	$output_serialized = Dumper($output);
	$output_serialized =~ s/\012/\015\012/sg;
	$mode = "t";
    } else {
	#binary mode
	$mode = "b";
	my $dumped_u = freeze $output;

	#a reasonable limit

	if (length($dumped_u) > 200){
	    $must_compress = 1;
	    $mode .= "c";
	} else {
	    $mode .= "u";
	}

	if ($must_compress == 1){
	    my $dumped_c = Compress::Zlib::memGzip($dumped_u);
	    $output_serialized = encode_base64($dumped_c, EOL);
	} else {
	    $output_serialized = encode_base64($dumped_u, EOL);
	}
    }


    #ok, now I have the dumped object

    $output_serialized =~ s/^(\.?)/$1$1/sg; #also at the start of the string...

    $output_serialized =~ s/\015?\012(\.?)/\015\012$1$1/sg;

    #my $socket = $self->{_output};

    print $mode . EOL;
    print $output_serialized;
    print "." . EOL; #end of transmission
}


1;

