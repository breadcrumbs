package Bcd::Commands::TrustCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);

#this function simply makes some common checks.
sub _common_trust_checks_fail{
    my ($self, $stash) = @_;

    #first of all the trust should be valid.
    if (!Bcd::Data::Trust->is_valid_trust($self->{trust})){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TRUST;
	return 1;
    }

    #I take my post code from the session;
    my $code = $stash->get_session_post_code($self->{session_id});

    #the nick should belong to the same ant nest.
    my $user = Bcd::Data::Users->select_user_data_from_nick_ant_nest
	($stash, $code, $self->{nick});

    #no user, no party
    if ( ! defined ($user)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST;
	return 1;
    }

    #the common checks have passed.
    return (0, $user);
}


1;
