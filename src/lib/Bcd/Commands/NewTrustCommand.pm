package Bcd::Commands::NewTrustCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::TrustCommand;
use base(qq/Bcd::Commands::TrustCommand/);

use Data::Dumper;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest

use constant NAME => "tr.new_trust";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I need a simple ant role to create a booking
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I need the tutor trust parameter
    push (@{$self->{"parameters"}}, "nick");
    push (@{$self->{"parameters"}}, "totem");
    push (@{$self->{"parameters"}}, "trust");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my ($res, $user) = $self->_common_trust_checks_fail($stash);

    if ($res == 1){
	#nothing to do.
	return;
    }

    #the user should be in the normal state
    if ( $user->{users_state} != Bcd::Constants::Users::NORMAL_ACTIVE_ANT){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE_FOR_THIS_USER;
	return;
    }

    my $id = $stash->get_session_id($self->{session_id});

    #ok, now I should see if there is a duplicate trust...
    if ( Bcd::Data::Trust->exists_direct_trust_between($stash, $id, $user->{id})){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_DUPLICATE_TRUST_NOT_ALLOWED;
	return;
    }

    #let's see if the totem is the same
    if ( $self->{totem} ne $user->{totem}){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TOTEM;
	return;
    }

    #I should see if there is already a booking with the user as the first actor...
    if (Bcd::Data::Trust->exists_booking_between($stash, $id, $user->{id})){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_DUPLICATE_BOOKING;
	return;
    }

    #ok, now let's see if exists a booking with the user as the second actor
    if (Bcd::Data::Trust->exists_booking_between($stash, $user->{id}, $id)){
	
	#I can create a new trust
	Bcd::Data::Trust->create_new_trust($stash, $id, $user->{id}, $self->{trust});
	  $self->{new_booking} = 0;

	  #I have created a new trust, so all the nets should be updated...
	  my $post_code = $stash->get_session_post_code($self->{session_id});
	  Bcd::Data::TrustsNet->outdate_net_for_ant_nest($stash, $post_code);
	  
    } else {

	#ok, seems all ok.
	Bcd::Data::Trust->create_new_trust_booking($stash, $id, $user->{id}, $self->{trust});
	  $self->{new_booking} = 1;

      }
    
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;   
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_scalar("new_booking", $self->{new_booking}, $output);
}

1;
