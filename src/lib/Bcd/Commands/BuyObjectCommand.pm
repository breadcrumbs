package Bcd::Commands::BuyObjectCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
#My base class is the create user command
use Bcd::Commands::InvoiceCommand;
use base(qq/Bcd::Commands::InvoiceCommand/);
use Data::Dumper;

use constant NAME => "ws_buy_object";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    push (@{$self->{parameters}}, "id_ad");
    push (@{$self->{parameters}}, "id_locus");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;
    my $my_id = $stash->get_session_id($self->{session_id});

    #the buy object command is composed of 3 phases

    #phase 1: authorize invoice
    my ($res, $invoice_id) = $self->_exec_authorize_invoice_fail($stash);
    if ( $res == 1){
	return;
    }

    #phase 2: emit invoice

    #ok, the invoice has been authorized, then I will emit it
    #the quantity is fixed: 1.
    Bcd::Data::Invoices->emit_invoice
	($stash, $invoice_id, 1);

    #this must not fail, otherwise I will not be in this phase.
    my ($res_c, $ad, $loc) = $self->_get_ad_and_locus_fail($stash);

    #phase 3: pay invoice
    #mmm I must have the invoice
    my $invoice = Bcd::Data::Invoices->get_invoice_hash($stash, $invoice_id);

    if ($self->_pay_invoice_fail
	($stash, $my_id, $invoice, $invoice_id, $ad, $loc) == 1){
	return;
    }

    #if the object is sold, then the ad must be inactive now.
    Bcd::Data::Ads->this_ad_is_inactive_now($stash, $self->{id_ad});

    #ok, it seems to be all ok.
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#I have the long token as the output
sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_scalar("long_token", $self->{long_token}, $output);
}

1;
