package Bcd::Commands::GetAccountSummaryCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use Storable;


sub _get_summary_of_this_account{
    my ($self, $stash, $account_id) = @_;

    #I get the dataset
    my ($summary, $euro) = Bcd::Data::Accounting::Accounts->get_account_summary_ds($stash, $account_id);

    #ok, then I should convert all the amounts, if the account is in euro
    if ($euro == 1){
	#ok, let's convert all the amounts.
	for (1..(@{$summary}-1)){
	    my $datum = $summary->[$_];

	    for (2..4){
		$datum->[$_] = Bcd::Data::RealCurrency::humanize_this_string($datum->[$_]);
	    }
	}
    }

    #ok, now I should put this recordset in the output
    $self->{former_frozen_output} = $summary;


}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $output_cmd = $self->{former_frozen_output};
    $self->_add_record_set($output_cmd, 'account_summary', $output);
}


1;
