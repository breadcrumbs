package Bcd::Commands::GetInvoiceDetailsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Errors::ErrorCodes;
use Bcd::Commands::SessionCommand;
use Data::Dumper;

our @ISA = qw(Bcd::Commands::SessionCommand);

use constant NAME => "ws_get_invoice_details";

use constant PARAM_TABLE => 
    [
     ["id_invoice"],
     {
	 id_invoice => ["id_type", "required"],
     },
     [Bcd::Commands::SessionCommand::PARAM_TABLE],
     ];


sub get_name(){
    return NAME;
}
sub get_param_table(){
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    my $input = $self->{_in}->get_parsed_result();
    my $my_id = $stash->get_session_id($input->{session_id});

    #the invoice must exist and must belong to me or directed to me...
    my $invoice = Bcd::Data::Invoices->get_invoice_hash($stash, $input->{id_invoice});

    if (!defined($invoice)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_DATA;
	return;
    }

    #let's get also the ad to which this invoice is related
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $invoice->{id_ad});
    
    #ok, is the invoice directed to me?
    my $am_I_the_buyer;
    my $nick;
    if ($my_id == $invoice->{user_to}){
	$am_I_the_buyer = 1;
	#ok, I am the buyer, so I put in the result the nick of the seller
	$nick = Bcd::Data::Users->get_nick_from_id($stash, $ad->{id_user});

    } elsif ($ad->{id_user} == $my_id){
	$am_I_the_buyer = 0;
	#ok, I am the seller, so I put in the result the nick of the buyer
	$nick = Bcd::Data::Users->get_nick_from_id($stash, $invoice->{user_to});

    } else {
	#I cannot see this invoice, generic authorization error
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED;
	return;
    }

    #ok, now I also get the presence for this invoice.
    my $presence = Bcd::Data::PublicSites->get_presence_decoded_hash
	($stash, $invoice->{id_locus});

    #I now decode the activity
    my $activity = Bcd::Data::Activities->get_all_parents_str($stash, $ad->{id_act});

    #ok, I have a simple output...
    my $output = {};
    $output->{ad}             = $ad;
    $output->{invoice}        = $invoice;
    $output->{presence}       = $presence;
    $output->{activity}       = $activity;
    $output->{am_I_the_buyer} = $am_I_the_buyer;
    if ($am_I_the_buyer == 1){
	$output->{seller} = $nick;
    } else {
	$output->{buyer} = $nick;
    }

    $self->{my_output} = $output;

    
    #if the invoice is in the correct state and I am the buyer then I can
    #also display the full token...

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_object($self->{my_output}, "invoice_details", $output);
}


1;

