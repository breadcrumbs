package Bcd::Commands::GetAntNestsSummaryCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::SimpleCommand;
use base qw/ Bcd::Commands::SimpleCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::AntNests;
use Data::Dumper;
use Storable;

use constant NAME => "an_get_summary";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no parameters, I present only a summary...

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, let's see now the summary
    my @dataset;

    my @fields = ('post_code_interval', 'ant_nests_count', 'ants_count', 'gdp');

    unshift(@dataset, \@fields);

    for (0..9){

	my @row;

	my $code_min = "${_}000000";
	my $code_max = "${_}999999";

	my $human_search_range = "${_}0000 : ${_}9999";

	push(@row, $human_search_range);

	#ok, now I can go on...
	my $count = 
	    Bcd::Data::AntNests->get_count_ant_nests_min_max($stash, $code_min, $code_max);

	push(@row, $count);

	#then the number of ants...

	my $ants = Bcd::Data::Users->get_count_users_in_ant_nests_range($stash, $code_min, $code_max);

	push(@row, $ants);

	#just a fake number
	my $gdp = 38.392 * $ants * $count;
	
	push(@row, $gdp);

	push(@dataset, \@row);
	
    }

    #ok, now I can store the output

    $self->{former_frozen_output} = \@dataset;
    $self->{exit_code}     = Bcd::Errors::ErrorCodes::BEC_OK;

}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $output_cmd = $self->{former_frozen_output};
    $self->_add_record_set($output_cmd, 'ant_nests_summary', $output);

}

1;
