package Bcd::Commands::CreateNewFounderCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::CreateAntNestBookingRequestCommand;
use base qw/ Bcd::Commands::CreateAntNestBookingRequestCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::NewAntNests;
use Bcd::Data::Founders;
use Data::Dumper;
use Storable;

use constant NAME => "an_create_new_founder";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #my ($class, $stash, $code, $name_ant_nest, $name, $email, $want_to_be_founder) = @_;

    my $parameter = {};

    $parameter->{name} = "comment";
    $parameter->{type} = "multi_line";

    #no privileges required, I have other two parameters
    push (@{$self->{"parameters"}}, "password");
    push (@{$self->{"parameters"}}, $parameter  );

    bless ($self, $class);

    return $self;
}

sub _checks_when_new_ant_nests_is_present_fail{
    my ($self, $stash, $new_ant_nest) = @_;

    #exists already a booking, let's see if the user is enabled to join
    if ($new_ant_nest->{state} != 
	Bcd::Constants::NewAntNestsConstants::WAITING_FOR_INITIAL_FOUNDERS){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_MORE_FOUNDERS_FOR_NOW;
	return 1;
    }

    #let's see if there is a duplicate nick
    my $user = Bcd::Data::NewAntNests->select_new_founder_hash_post_nick
	($stash, $self->{id_ant_nest}, $self->{name_user});

    if (defined($user)){
	#this nick is already defined...
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_USER_ALREADY_EXISTING;
	return 1;
    }

    return 0; #no fail
}


sub _create_new_ant_nest_booking{
    my ($self, $stash, $is_first_user) = @_;

    my $token;

    if ( $is_first_user == 1){

	$token = Bcd::Data::NewAntNests->create_new_ant_nest_booking
	    ($stash, $self->{id_ant_nest}, 
	     $self->{name_ant_nest});


    } else {
	#no first user?



	#are we arrived to the initial number of founders?
	my $founder_count = Bcd::Data::NewAntNests->get_ant_nest_founders_count
	    ($stash, $self->{id_ant_nest});



	if ($founder_count == (Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE - 1)){
	    #ok, I should change the state of this ant nest
	    Bcd::Data::NewAntNests->update_ant_nest_state
		($stash, $self->{id_ant_nest}, Bcd::Constants::NewAntNestsConstants::WAITING_FOR_CHECKING_IN);
	}



	#inform the other founder
	$self->_inform_the_founders($stash);


    }

    $token = Bcd::Data::NewAntNests->insert_a_new_founder
	($stash, $self->{id_ant_nest}, 
	 $self->{name_user},
	 $self->{email}, 
	 $self->{password},
	 );

    $self->{output_token} = $token;


    #I should inform the ant and give to her the token
    $stash->get_mailer()->mail_this_message($stash, $self->{email},
					    'new_booked_ant_token.tt2', 
					    {
						name => $self->{name_user},
						token => $token,
						id_ant_nest => $self->{id_ant_nest},
						name_ant_nest => $self->{name_ant_nest},
						want_to_be_founder => $self->{want_to_be_founder},
					    }
					    );


}

sub _inform_the_founders{
    my ($self, $stash) = @_;

    #first of all I get all the confirmed founders...

    my $confirmed_founders = Bcd::Data::Founders->get_confirmed_founders_to_email
	($stash, $self->{id_ant_nest});



    foreach(@{$confirmed_founders}){
	    $stash->get_mailer()->mail_this_message($stash, $_->[2],
						    'a_new_founder_has_joined.tt2', 
						    {
							name => $_->[1],
							nick => $_->[0],
							founder_name => $self->{name_user},
							id_ant_nest => $self->{id_ant_nest},
							comment => $self->{comment},
						    }
						    );
    }



}


#I add the token to the output, mainly to make an automated script
sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_scalar("token", $self->{output_token}, $output);
}

1;
