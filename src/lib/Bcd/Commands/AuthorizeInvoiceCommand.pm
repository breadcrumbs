package Bcd::Commands::AuthorizeInvoiceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
#My base class is the create user command
use Bcd::Commands::InvoiceCommand;
use base(qq/Bcd::Commands::InvoiceCommand/);
use Data::Dumper;
use Bcd::Constants::AdsConstants;

use constant NAME => "ws_authorize_invoice";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    push (@{$self->{parameters}}, "id_ad");
    push (@{$self->{parameters}}, "id_locus");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    if($self->_exec_authorize_invoice_fail($stash) == 1){
	return;
    }

    #this must succed, because I have passed the test before...
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $self->{id_ad});
    my $buyer = $stash->get_session_nick($self->{session_id});
    my $loc = Bcd::Data::Ads->get_ad_locality_hash($stash, $self->{id_ad}, $self->{id_locus});

    #I should send a mail to the seller
    my $template_vars = {
	buyer => $buyer,
	ad    => $ad,
	loc   => $loc,
    };

    $self->_send_mail_to_user_with_id
	($stash, "invoice_authorized.tt2", $ad->{id_user}, $template_vars);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#no output only the exit code.

1;
