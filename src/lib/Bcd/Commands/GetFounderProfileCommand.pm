package Bcd::Commands::GetFounderProfileCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;



use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Bcd::Data::Founders;

use Storable;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest

use constant NAME => "na_get_founder_profile";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I can be only an ant to execute this command
    $self->{privilege_required} = Bcd::Data::Users::FOUNDER_ROLE;

    #I have one parameter: the id of the user.
    push (@{$self->{parameters}}, "id_other_user");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #If I am here I am a founder...

    #but am I in the correct state, only founder which have already
    #immitted data can see other founders' data
    my $my_id = $stash->get_session_id($self->{session_id});
    my $data = Bcd::Data::Founders->get_founder_main_data_arr($stash, $my_id);

    if ($data->[1] <= Bcd::Constants::FoundersConstants::WAITING_FOR_PERSONAL_DATA){
	#well, you cannot see the other details
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }


    #get the founder complete data
    my $founder = Bcd::Data::Founders->get_founder_complete_data_hash
	($stash, $self->{id_other_user});

    if ( !defined ($founder)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER;
	return;
    }

    #ok, the founder is defined, let's see if he belongs to our ant nest
    my $post_code = $stash->get_session_post_code($self->{session_id});

    if ( $post_code != $founder->{id_ant_nest}){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST;
	return;
    }

    #ok, I can store the output
    #my $res = &share({});
    #%{$res} = %{$founder};
    $self->{output} = $founder;

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_object($self->{output}, "founder_details", $output);
}

1;
