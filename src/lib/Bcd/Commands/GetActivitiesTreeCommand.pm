package Bcd::Commands::GetActivitiesTreeCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SimpleCommand;
use base(qq/Bcd::Commands::SimpleCommand/);

use Data::Dumper;
use Storable;

use constant NAME => "ac.get_tree";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #only one parameter, the type of the tree...
    push (@{$self->{parameters}}, "tree_type");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash ) = @_;
    
    #no session, no id, just a query function
    #check the validity of the tree_type parameter
    if ($self->{tree_type} < Bcd::Constants::ActivitiesConstants::PARENT_SERVICES
	or $self->{tree_type} > Bcd::Constants::ActivitiesConstants::PARENT_USED_ITEMS){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TREE_REQUESTED;
	return;
    }

    #ok, now I can request the tree
    my $tree = Bcd::Data::Activities->build_activity_tree_from($stash, $self->{tree_type});
    $self->{former_frozen_output} = $tree;

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $tree = $self->{former_frozen_output};
    $self->_add_dumped_object($tree, 'act_tree', $output);
}

1;
