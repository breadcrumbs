package Bcd::Commands::GetInvoicesListByStateCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::InvoiceCommand;
use base(qq/Bcd::Commands::InvoiceCommand/);
use Data::Dumper;
use Bcd::Constants::AdsConstants;

use constant NAME => "ws_get_invoices_list";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I have two parameters, one to have the emitted/received flag
    push (@{$self->{parameters}}, "invoice_type");
    push (@{$self->{parameters}}, "invoice_state");
    

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});

    #I should build a dataset
    my $emitted;
    if ($self->{invoice_type} eq "emitted"){
	$emitted = 1;
    } elsif($self->{invoice_type} eq "received"){
	$emitted = 0;
    } else {
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_ENUM_VALUE_UNDEFINED;
	return;
    }

    #now I should transform the state
    my $state_required;
    if ($self->{invoice_state} eq "authorized"){
	$state_required = Bcd::Constants::InvoicesConstants::AUTHORIZED;
    } elsif ($self->{invoice_state} eq "emitted"){
	$state_required = Bcd::Constants::InvoicesConstants::EMITTED;
    } elsif ($self->{invoice_state} eq "paid"){
	$state_required = Bcd::Constants::InvoicesConstants::PAID;
    } elsif ($self->{invoice_state} eq "collected"){
	$state_required = Bcd::Constants::InvoicesConstants::COLLECTED;
    } else {
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_ENUM_VALUE_UNDEFINED;
	return;
    }

    #ok, now I can have the dataset...
    my $ds;
    if ($emitted == 1){
	$ds = Bcd::Data::Invoices->get_emitted_invoices_list_by_state_ds
	($stash, $my_id, $state_required);
    } else {
	$ds = Bcd::Data::Invoices->get_received_invoices_list_by_state_ds
	($stash, $my_id, $state_required);
    }

    #ok, I now should add the nick in the dataset
    if ($ds->[0]->[0] ne "nick"){
	#as the statement is cached, also the fields are cached, so I have to
	#add the "nick" field only if it is not existing already
	unshift(@{$ds->[0]}, "nick");
    }

    foreach(@{$ds}[1..$#{$ds}]){
	#ok, I get the id
	my $id = $_->[1];
	my $nick = Bcd::Data::Users->get_nick_from_id($stash, $id);
	unshift(@{$_}, $nick);
    }

    $self->{former_frozen_output} = $ds;

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#I have the long token as the output
sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $ds = $self->{former_frozen_output};
    $self->_add_record_set($ds, 'invoices_list', $output);

}

1;
