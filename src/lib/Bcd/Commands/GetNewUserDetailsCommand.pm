package Bcd::Commands::GetNewUserDetailsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);

use Data::Dumper;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest
use constant NAME => "an.get_new_user_details";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    #I can be only an ant to execute this command
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #The token has been built with the first tutor
    push (@{$self->{"parameters"}}, "token");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, first of all I should check that the token is valid
    my $new_user_id = Bcd::Data::Users->
	get_new_user_id_from_this_token($stash, $self->{token});

    if (!defined($new_user_id)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN;
	return;
    }

    #ok, now I should check if the new user is in the same ant nest
    my $user = Bcd::Data::Users->select_user_data($stash, $new_user_id);
    my $value = $stash->get_cache()->get($self->{session_id});

    if ( $value->{ant_nest} != $user->{id_ant_nest}){
	$self->{exit_code} = 
	    Bcd::Errors::ErrorCodes::BEC_TRYING_TO_CREATE_ANT_OUTSIDE_YOUR_NEST;
	return;
    }

    #ok, the user has a valid token AND it is in the same ant nest.

    #my $res  = &share({});

    #I should copy the user hash in the shared hash
    #%{$res} = %{$user}; #?

    $self->{output} = $user;
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_object($self->{output}, "new_user_details", $output);
}

1;
