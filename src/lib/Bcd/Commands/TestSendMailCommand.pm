package Bcd::Commands::TestSendMailCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


use Bcd::Errors::ErrorCodes;
use Bcd::Commands::SimpleCommand;
use Bcd::Commands::InputCommandParser;

our @ISA = qw(Bcd::Commands::SimpleCommand);

use constant NAME => "tc_send_test_mail";

use constant PARAM_TABLE => 
    [
     ["user_to"],
     {
	 user_to  => ["email_type",  "required"],
     },
     ];


sub get_name(){
    return NAME;
}
sub get_param_table(){
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.

    my ($self, $stash) = @_;

    #I just send an email
    my $input = $self->{_in}->get_input();

    my $template_vars = {
	name    => $input->{user_to},
	user_to => $input->{user_to},
    };

    $stash->get_mailer()->mail_this_message
	($stash, $input->{user_to}, "test_mail.tt2", $template_vars);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

}


1;

