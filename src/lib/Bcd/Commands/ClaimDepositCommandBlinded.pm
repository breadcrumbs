package Bcd::Commands::ClaimDepositCommandBlinded;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this is only used for the scripts

use strict;
use warnings;

use Digest::SHA1;

use Bcd::Commands::SessionCommand;
use Bcd::Commands::ClaimDepositCommand;
use base(qq/Bcd::Commands::SessionCommand/);

use Data::Dumper;

use constant NAME => "bk.claim_deposit_blinded";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #The privilege is not only for the ant... if the ticket is a withdrawal ticket than 
    #only the treasurer can do this.
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I need only the secret token
    push (@{$self->{"parameters"}}, "blinded_token");
    push (@{$self->{"parameters"}}, "full_token");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;
    #I build the secret token
    $self->{secret_token} = Bcd::Data::Token::unblind_the_token
	($self->{full_token}, $self->{blinded_token});
    return Bcd::Commands::ClaimDepositCommand::_exec($self, $stash);
}

sub _add_your_specific_output{
    my ($self, $output) = @_;
    return Bcd::Commands::ClaimDepositCommand::_add_your_specific_output($self, $output);
}

1;
