package Bcd::Commands::SessionCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::SimpleCommand;
use base qw(Bcd::Commands::SimpleCommand);
use Data::Dumper;

use constant PARAM_TABLE => 
    [
     ["session_id"],
     {
	 session_id     => ["string_type",             "required"],
     },
     undef,
     ];



#the session command is simply a command which must have a session id
#as the first parameter.

sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    
    #these are the parameters for this command
    push (@{$self->{"parameters"}}, "session_id");

    bless ($self, $class);

    return $self;
}

#this method simply returns a value stored in the session, if present
sub _get_session_value{
    my ($self, $stash, $key) = @_;

    #let's take the session
    my $cache = $stash->get_cache();
    my $param_table = $self->get_param_table();
    my $key_in_cache;
    if (defined($param_table)){
	my $input = $self->{_in}->get_input();
	$key_in_cache = $input->{session_id}
    } else {
	$key_in_cache = $self->{"session_id"};
    }
    my $value = $cache->get($key_in_cache);
    
    if (! defined($value)){
	return undef; #session outdated... perhaps...
    }

    return $value->{$key};
}

1;
