package Bcd::Commands::TreasurerOfflineAcknowledgeCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use Data::Dumper;

use constant NAME => "bk.treasurer_offline_acknowledge";


sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #only the treasurer can make this
    $self->{privilege_required} = Bcd::Data::Users::TREASURER_ROLE;

    #No, privileges, I suffice myself.


    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #this is a rather straigthforward command, I should simply control
    #that the booking exists and it is in the right state...


    #I should simply get the deposits and withdrawals which are in the
    #CREATION state
    my $code = $stash->get_session_post_code($self->{session_id});

    my $pending_bookings = 
	Bcd::Data::Deposits
	->get_deposits_and_withdrawals_to_acknowledge_offline($stash, $code);


    my $num_deposits    = 0;
    my $num_withdrawals = 0;


    #ok, for all this bookings I should send a mail
    foreach(@{$pending_bookings}){

	#I should humanize the amount
	$_->[3] = Bcd::Data::RealCurrency::humanize_this_string($_->[3]);

	my $vars = {
	    is_deposit    => $_->[4],
	    amount     	  => $_->[3],
	    booking_token => $_->[2],
	};

	#my ($self, $stash, $letter_to_send, $user, $template_vars) = @_;
	$self->_send_mail_to_user_with_id
	    ($stash, "treasurer_has_acknowledged_offline.tt2", $_->[1], $vars );

	#ok, then I acknowledge this booking
	Bcd::Data::Deposits->treasurer_acknowledged($stash, $_->[0]);
	
	if ($_->[4] == 1){
	    $num_deposits ++;
	} else {
	    $num_withdrawals ++;
	}

    }
    my $my_id = $stash->get_session_id($self->{session_id});

    my $vars = {
	num_deposits     => $num_deposits,
	num_withdrawals  => $num_withdrawals,
	offline_bookings => $pending_bookings,
    };

    #I send a letter to myself with all the data...
    $self->_send_mail_to_user_with_id
	($stash, "treasurer_offline_report.tt2", $my_id, $vars);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

};

#no specific output

1;

