package Bcd::Commands::UpdateFounderPersonalDataCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::UpdatePersonalUserDataCommand;
use base(qq/Bcd::Commands::UpdatePersonalUserDataCommand/);
use Bcd::Data::Users;
use Bcd::Data::Founders;
use Data::Dumper;
use Bcd::Errors::ErrorCodes;

use constant NAME => "na_update_founder_personal_data";

sub get_name{
    return NAME;
}

#I have the same parameters
sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I need a simple ant role to update my data...
    $self->{"privilege_required"} = Bcd::Data::Users::FOUNDER_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, I should get the state of this founder...
    my $id_user = $stash->get_session_id($self->{session_id});

    my $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $id_user);

    if ($founder->{id_status} != Bcd::Constants::FoundersConstants::WAITING_FOR_PERSONAL_DATA){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #ok, now I can change the personal data
    #$self->_do_change_personal_data($stash, $id_user);
    Bcd::Data::Users->update_personal_data($stash, $founder->{id_personal_data}, $self);

    #ok, I can update the state of this founder
    Bcd::Data::Founders->founder_personal_data_given($stash, $id_user);

    #ok, I can return... all ok.
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

1;
