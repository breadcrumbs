package Bcd::Commands::AntNestGetDetailCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use base qw/ Bcd::Commands::SimpleCommand /;

use constant NAME => "an.get_detail";

sub get_name{
    return NAME;
}

sub _exec{
    #empty for now

    my ($self, $stash) = @_;

    my $post_code = $self->{ant_nest};
    if ( ! Bcd::Data::PostCode::is_valid_post_code_with_sub($post_code)){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE;
 	return;
    }

    #ok, now I should simply get the details for this ant nest
    my $res = Bcd::Data::AntNests->get_ant_nest_detail($stash, $post_code);

    if ( ! defined($res)){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_NO_ANT_NEST_IN_THIS_CODE;
	return;
    }

    my $res2  = {};

    $res2->{id}              = $res->[0];
    $res2->{name}            = $res->[3];

    #$res2->{hq}              = $res->[5];
    #$res2->{opening_hours}   = $res->[6];

    #ok, now let's see the number of ants...
    $res2->{num_of_ants}     = Bcd::Data::Users->get_ant_count_from_ant_nest($stash, $post_code);

    my $boss = Bcd::Data::Users->get_boss_nick_name_arr($stash, $post_code);
    $res2->{boss_id}         = $boss->[0];
    $res2->{boss_first_name} = $boss->[2];
    $res2->{boss_last_name}  = $boss->[3];
    $res2->{boss_nick}       = $boss->[1];

    my $treasurer = Bcd::Data::Users->get_tresaurer_of_this_ant_nest($stash, $post_code);

    $res2->{treasurer_id}    = $treasurer->{id};
    $res2->{treasurer_nick}  = $treasurer->{nick};
    #$res2->{GDP}             = "20000";

    $self->{output} = $res2;
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
    return;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    bless ($self, $class);

    push (@{$self->{"parameters"}}, "ant_nest");

    return $self;
}

# sub get_output{
#     my $self = shift;
#     my @output;

#     $self->_add_exit_code(\@output);

#     if ( $self->{exit_code} == Bcd::Errors::ErrorCodes::BEC_OK){
# 	$self->_add_object($self->{output}, "ant_nest", \@output);
#     }
#     return \@output;
# }

sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_object($self->{output}, "ant_nest", $output);
}

1;
