package Bcd::Commands::CreateAdCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;

use constant NAME => "ws.create_ad";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #ok, now I have some parameters for the ad...
    my $parameter = {};
    $parameter->{name} = "ad_text";
    $parameter->{type} = "multi_line";

    push (@{$self->{parameters}}, "id_act");
    push (@{$self->{"parameters"}}, $parameter);

    #this is the id of a presence...it can be created before, or I 
    #can use one of the past presences...

    push (@{$self->{parameters}}, "id_locus");
    push (@{$self->{parameters}}, "p_min");
    push (@{$self->{parameters}}, "p_max");
    push (@{$self->{parameters}}, "t_e");
    push (@{$self->{parameters}}, "t_c");
    

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;
    my $my_id = $stash->get_session_id($self->{session_id});

    #validity of the trusts...
    if ($self->trust_bb_validity_fail($self->{t_e})){
	return;
    }
    if ($self->trust_bb_validity_fail($self->{t_c})){
	return;
    }


    my $t_e = Bcd::Data::Trust::dec_from_bBel($self->{t_e});
    my $t_c = Bcd::Data::Trust::dec_from_bBel($self->{t_c});

    #first of all I should check the price: can the user afford it?
    my $tax = Bcd::Data::WebSite->get_price_for_this_ad
	($stash, $my_id, $self->{p_min}, $self->{p_max}, $t_e, $t_c);

    #can you afford it?
    if (! Bcd::Data::Bank->can_this_user_withdraw_tao($stash, $my_id, $tax)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_FUNDS;
	return;
    }
    my $post_code = $stash->get_session_post_code($self->{session_id});

    #ok, you can afford, debit the account
    $self->{new_total_tao} = Bcd::Data::WebSite->charge_user_for_this_ad
	($stash, $my_id, $post_code, $self->{p_min}, $self->{p_max},
	 $t_e, $t_c);

    #ok, now I should really create the ad...
    $self->{ad_id} = Bcd::Data::Ads->create_new_ad
	($stash, $self->{id_act}, $my_id, $self->{ad_text},
	 $self->{id_locus}, $self->{p_min}, $self->{p_max},
	 $t_e, $t_c);

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_scalar("new_total_tao", $self->{new_total_tao}, $output);
    $self->_add_scalar("ad_id", $self->{ad_id}, $output);
}

1;
