package Bcd::Commands::ClaimWithdrawalCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Digest::SHA1;

use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use Data::Dumper;

use constant NAME => "bk.claim_withdrawal";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::TREASURER_ROLE;

    #I need only the secret token
    push (@{$self->{parameters}}, "withdrawal_id");
    push (@{$self->{parameters}}, "secret_token");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, first of all I should get the booking which is not collected which I own
    my $my_id = $stash->get_session_id($self->{session_id});

    #I get the booking from the id
    my $booking = Bcd::Data::Deposits->get_booking_from_id_arr
	($stash, $self->{withdrawal_id});

    if (! defined($booking)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_BOOKING;
	return;
    }

    #ok, is this booking a WITHDRAWAL?
    if ( $booking->[1] == 1){
	#nope, fail
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_BOOKING_IS_A_DEPOSIT;
	return;
    }

    #let's see if we are in "online" mode;
    my $code = $stash->get_session_post_code($self->{session_id});
    my $online = Bcd::Data::Configuration->get_ant_nest_value
	($stash, $code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    if (
	($online == 1 and $booking->[4] != Bcd::Constants::DepositsConstants::CREATION_STATE)
	or
	($online == 0 and $booking->[4] != Bcd::Constants::DepositsConstants::OFFLINE_ACKNOWLEDGED)
	){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }


    #OK, Test the secret code
    my $sha = Digest::SHA1->new;
    $sha->add($self->{secret_token});
    my $digest = $sha->hexdigest;


    if ($digest ne $booking->[7]){
	#no valid secret token
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_SECRET_TOKEN;
	return;
    }

    #ok, at last I can claim this deposit
    my ($amount_withdrawn, $new_total) = 
	Bcd::Data::Deposits->claim_this_withdrawal_object($stash, $code, $booking);

    #I should humanize these two amounts
    $amount_withdrawn    = Bcd::Data::RealCurrency::humanize_this_string($amount_withdrawn);
    $new_total           = Bcd::Data::RealCurrency::humanize_this_string($new_total);
    my $amount_received  = Bcd::Data::RealCurrency::humanize_this_string($booking->[3]);

    $self->{amount_withdrawn} = $amount_withdrawn;
    $self->{new_total}        = $new_total;
    $self->{amount_received}  = $amount_received;


    #send the mail
    my $template_vars = 
    {
	amount_withdrawn   => $amount_withdrawn,
	new_total          => $new_total,
	amount_received    => $amount_received,
    };

    $self->_send_mail_to_user_with_id
	($stash, 'withdrawal_done.tt2', 
	 $booking->[2], $template_vars);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{

    my ($self, $output) = @_;

    $self->_add_scalar("amount_withdrawn", $self->{amount_withdrawn}, $output);
    $self->_add_scalar("new_total"       , $self->{new_total}       , $output);
    $self->_add_scalar("amount_received" , $self->{amount_received} , $output);
}

1;
