package Bcd::Commands::GenericConnectCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SimpleCommand;
our @ISA = qw(Bcd::Commands::SimpleCommand);

use Bcd::Common::CommonConstants;


sub _check_old_session{
    my ($self, $stash, $id) = @_;

    my $users_connected = $stash->get_cache()->get(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY);

    if (defined($users_connected)){
	my $old_session = $users_connected->{$id};
	if (defined($old_session)){
	    #ok, I will delete the old session
	    $stash->get_cache->remove($old_session);
	}
    }
}

sub _store_login_in_cache{
    my ($self, $stash, $id, $session) = @_;
    my $users_connected = $stash->get_cache()->get(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY);

    if (defined($users_connected)){
	$users_connected->{$id} = $session;
	$stash->get_cache()->set(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY, $users_connected);
    } else {
	my %hash = ( $id => $session );
	$stash->get_cache()->set(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY, \%hash);
    }
}

#called whenever I should do a connect
sub _generic_connect{
    my ($self, $stash) = @_;

    my $parsed_result = $self->{_in}->get_parsed_result();

    my ($res, $role, $id) = Bcd::Data::Users->login(
						    $parsed_result->{user}, 
						    $parsed_result->{ant_nest}, 
						    $parsed_result->{password}, 
						    $stash);

    if ($res == 0){
	
	$self->_check_old_session($stash, $id);
	
	#Ok, I should create a new session id
	my $session_id = $self->{id_cmd} . rand;

	#I try to store some value in the cache
	my %test_session;
	$test_session{"user_connected"} = "$parsed_result->{\"user\"}";
	$test_session{"user_role"} = $role;
	$test_session{"ant_nest"}  = $parsed_result->{ant_nest};
	$test_session{"user_id"}   = $id;

	$stash->get_cache()->set($session_id, \%test_session);

	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_OK;

	$self->{"session_id"} = $session_id;
	$self->{"role"} = $role;

	$self->_store_login_in_cache($stash, $id, $session_id);

    } elsif ($res == 1) {
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_INVALID_PASSWORD;
    }

}

sub _founder_login{
    my ($self, $stash) = @_;

    my $parsed_result = $self->{_in}->get_parsed_result();

    my ($res, $role, $id) = Bcd::Data::NewAntNests->login
	($stash, $parsed_result->{ant_nest}, $parsed_result->{user}, $parsed_result->{password});

    if ($res == 0){

	$self->_check_old_session($stash, $id);

	my $session_id = $self->{id_cmd} . rand;

	my %test_session;
	$test_session{user_connected} = "$parsed_result->{\"user\"}";
	$test_session{user_role} = Bcd::Data::Users::ROLE_FOR_A_FOUNDER_ANT;
	$test_session{ant_nest}  = $parsed_result->{"ant_nest"};
	$test_session{user_id}   = $id;

	$stash->get_cache()->set($session_id, \%test_session);

	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_OK;
	$self->{"session_id"} = $session_id;
	$self->{"role"} = $role;

	$self->_store_login_in_cache($stash, $id, $session_id);

    } elsif ($res == 1) {
	#no success
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_PASSWORD;
    } elsif ($res == 2) {
	#invalid state, the founder has not yet checked in
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_ACCOUNT_NOT_READY;
    }
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_scalar("session_id", $self->{"session_id"}, $output);
    $self->_add_scalar("role", $self->{"role"}, $output);
}

1;
