package Bcd::Commands::ConfirmFounderCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Common::CommonConstants;

use Bcd::Commands::SimpleCommand;
our @ISA = qw(Bcd::Commands::SimpleCommand);

use constant NAME => "na_confirm_founder";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #these are the parameters for this command
    push (@{$self->{"parameters"}}, "ant_nest");
    push (@{$self->{"parameters"}}, "user");
    push (@{$self->{"parameters"}}, "token");

    bless ($self, $class);

    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    #I simply get the user from the new ant nest
    my $user = Bcd::Data::NewAntNests->select_new_founder_hash_post_nick
	($stash, $self->{ant_nest}, $self->{user});

    #no user, no party.
    if (!defined($user)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER;
	return;
    }

    #the user is there... is he in the right state?
    if ($user->{id_status} != Bcd::Constants::FoundersConstants::USER_BOOKED_CREATED){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #right state... is it the right token?
    my $token_hash = Bcd::Data::NewAntNests->get_founder_token_hash
	($stash, $user->{id_user});

    #????
    if (!defined($token_hash)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INTERNAL_ERROR;
	return;
    }

    if( $token_hash->{token} ne $self->{token}){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN;
	return;
    }

    #all ok... I can confirm this ant
    Bcd::Data::NewAntNests->new_founder_confirmed($stash, $user->{id_user});

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


#no output, only the exit code...
1;
