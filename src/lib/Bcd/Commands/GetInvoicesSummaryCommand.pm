package Bcd::Commands::GetInvoicesSummaryCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::InvoiceCommand;
use base(qq/Bcd::Commands::InvoiceCommand/);
use Data::Dumper;
use Bcd::Constants::AdsConstants;

use constant NAME => "ws_get_invoices_summary";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;
    
    #no parameters, I have only my session

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});

    #no, checks, I have only the session... and I ask the summary.
    my $summary = Bcd::Data::Invoices->get_received_invoices_summary_by_state_arr
	($stash, $my_id, Bcd::Constants::InvoicesConstants::AUTHORIZED);

    #the amount is null, because the quantity is null
    $self->{count_received_authorized} = $summary->[0];

    $summary = Bcd::Data::Invoices->get_received_invoices_summary_by_state_arr
	($stash, $my_id, Bcd::Constants::InvoicesConstants::EMITTED);

    #the amount is null, because the quantity is null
    $self->{count_received_emitted} = $summary->[0];
    $self->{total_received_emitted} = $summary->[1];

    $summary = Bcd::Data::Invoices->get_received_invoices_summary_by_state_arr
	($stash, $my_id, Bcd::Constants::InvoicesConstants::PAID);

    #the amount is null, because the quantity is null
    $self->{count_received_paid} = $summary->[0];
    $self->{total_received_paid} = $summary->[1];

    $summary = Bcd::Data::Invoices->get_received_invoices_summary_by_state_arr
	($stash, $my_id, Bcd::Constants::InvoicesConstants::COLLECTED);

    #the amount is null, because the quantity is null
    $self->{count_received_collected} = $summary->[0];
    $self->{total_received_collected} = $summary->[1];

    #####################
    ##now the emitted
 
    $summary = Bcd::Data::Invoices->get_emitted_invoices_summary_by_state_arr
	($stash, $my_id, Bcd::Constants::InvoicesConstants::AUTHORIZED);

    #the amount is null, because the quantity is null
    $self->{count_emitted_authorized} = $summary->[0];

    $summary = Bcd::Data::Invoices->get_emitted_invoices_summary_by_state_arr
	($stash, $my_id, Bcd::Constants::InvoicesConstants::EMITTED);

    #the amount is null, because the quantity is null
    $self->{count_emitted_emitted} = $summary->[0];
    $self->{total_emitted_emitted} = $summary->[1];

    $summary = Bcd::Data::Invoices->get_emitted_invoices_summary_by_state_arr
	($stash, $my_id, Bcd::Constants::InvoicesConstants::PAID);

    #the amount is null, because the quantity is null
    $self->{count_emitted_paid} = $summary->[0];
    $self->{total_emitted_paid} = $summary->[1];

    $summary = Bcd::Data::Invoices->get_emitted_invoices_summary_by_state_arr
	($stash, $my_id, Bcd::Constants::InvoicesConstants::COLLECTED);

    #the amount is null, because the quantity is null
    $self->{count_emitted_collected} = $summary->[0];
    $self->{total_emitted_collected} = $summary->[1];



    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#I have the long token as the output
sub _add_your_specific_output{
    my ($self, $output) = @_;

    #I have several scalars...
    $self->_add_scalar("count_received_authorized", $self->{count_received_authorized}, $output);

    $self->_add_scalar("count_received_emitted", $self->{count_received_emitted}, $output);
    $self->_add_scalar("total_received_emitted", $self->{total_received_emitted}, $output);

    $self->_add_scalar("count_received_paid", $self->{count_received_paid}, $output);
    $self->_add_scalar("total_received_paid", $self->{total_received_paid}, $output);

    $self->_add_scalar("count_received_collected", $self->{count_received_collected}, $output);
    $self->_add_scalar("total_received_collected", $self->{total_received_collected}, $output);


    #now the emitted scalars

    $self->_add_scalar("count_emitted_authorized", $self->{count_emitted_authorized}, $output);

    $self->_add_scalar("count_emitted_emitted", $self->{count_emitted_emitted}, $output);
    $self->_add_scalar("total_emitted_emitted", $self->{total_emitted_emitted}, $output);

    $self->_add_scalar("count_emitted_paid", $self->{count_emitted_paid}, $output);
    $self->_add_scalar("total_emitted_paid", $self->{total_emitted_paid}, $output);

    $self->_add_scalar("count_emitted_collected", $self->{count_emitted_collected}, $output);
    $self->_add_scalar("total_emitted_collected", $self->{total_emitted_collected}, $output);

}

1;
