package Bcd::Commands::InitDbCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#this is simply a command which should be issued once...
use Bcd::Commands::SessionCommand;
use Bcd::Data::AntNests;
use Bcd::Data::Users;
use Bcd::Data::Deposits;
use base qw(Bcd::Commands::SessionCommand);
use Bcd::Data::NewAntNests;
use Bcd::Data::Founders;
use Bcd::Data::Configuration;
use Bcd::Data::PublicSites;
use Bcd::Data::Accounting::AccountsPlan;
use Bcd::Data::TrustsNet;
use Bcd::Data::Activities;
use Bcd::Data::Ads;
use Bcd::Data::Invoices;

use constant NAME => "sv.initdb";

sub get_name{
    return NAME;
}

sub new{

    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no parameters, but I must be root to do this.
    $self->{privilege_required} = Bcd::Data::Users::BC_ROOT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    #here I put the classes which must be inited...
    Bcd::Data::Accounting::AccountsPlan  ->init_db($stash);
    Bcd::Data::AntNests                  ->init_db($stash);
    Bcd::Data::NewAntNests  		 ->init_db($stash);
    Bcd::Data::Founders     		 ->init_db($stash);
    Bcd::Data::Users        		 ->init_db($stash);
    Bcd::Data::Deposits     		 ->init_db($stash);
    Bcd::Data::Configuration		 ->init_db($stash);
    Bcd::Data::PublicSites  		 ->init_db($stash);
    Bcd::Data::Activities                ->init_db($stash);
    Bcd::Data::Ads                       ->init_db($stash);
    Bcd::Data::Invoices                  ->init_db($stash);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

1;
