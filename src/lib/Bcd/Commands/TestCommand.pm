package Bcd::Commands::TestCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


#this is simply a test command, useful to test the daemon.  it takes a
#variable list of parameters and returns the parameters in the reverse
#order


use strict;
use warnings;

use Bcd::Commands::SimpleCommand;
use Data::Dumper;

our @ISA = qw(Bcd::Commands::SimpleCommand);

use constant NAME => "tc";

sub get_name{
    return NAME;
}

#Ok, I simply have a variable list of parameters...
#so I should override the parse_line
sub parse_line{
    my $self = shift;
    my $line = shift;
    push(@{$self->{"parameters"}}, $line);
}

#ok, now I should exec the command...
sub exec{
    my $self = shift;

    #nothing to execute... I will simply return the lines in the
    #reverse order

    $self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_OK;
    $self->{"end_flag"}->up();
}

sub eot{
    my $self = shift;

    my $num_of_parameters = @{$self->{"parameters"}};

    #I start to give the output from the bottom of the list

    $self->{"output_index"} = $num_of_parameters;

}

sub get_next_line_of_output{
    my $self = shift;

    my $index = $self->{"output_index"};

    if ( $index == 0){
	return ("", 0);
    } else {
	my $ans = $self->{"parameters"}[$index-1];
	$index--;
	$self->{"output_index"} = $index;
	return ($ans, 1);
    }
}
