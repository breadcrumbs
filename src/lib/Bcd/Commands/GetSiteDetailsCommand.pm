package Bcd::Commands::GetSiteDetailsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base qw/ Bcd::Commands::SessionCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::AntNests;
use Data::Dumper;
use Storable;

use constant NAME => "an.get_site_details";

sub get_name{
    return NAME;
}

use constant PARAM_TABLE =>
    [
     ["id_site"],
     {
	 id_site     => ["id_type", "required"],
     },
     [Bcd::Commands::SessionCommand::PARAM_TABLE],
     ];

sub get_param_table{
    return PARAM_TABLE;
}


#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no parameters, but I must be an ant to make this command
    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;


    my $input = $self->{_in}->get_input();

    #for now the sites are only "public", so I should only check that this site
    #is in my ant nest.
    my $code = $stash->get_session_post_code($input->{session_id});

    #is this site in my ant nest?
    if (! Bcd::Data::Sites->does_this_site_belongs_to_this_ant_nest($stash, $input->{id_site}, $code)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_SITE_OUTSIDE_YOUR_NEST;
	return;
    }

    #ok, this site is in my ant nest, so I can view it
    my $hash = Bcd::Data::Sites->get_site_hash($stash, $input->{id_site});
    $self->{output} = $hash;

    
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

}

sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_object($self->{output}, "site_details", $output);
}

1;
