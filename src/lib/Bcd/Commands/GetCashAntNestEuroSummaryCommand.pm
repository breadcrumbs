package Bcd::Commands::GetCashAntNestEuroSummaryCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::GetAccountSummaryCommand;
use base(qq/Bcd::Commands::GetAccountSummaryCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;
use Bcd::Data::AntNests;


use constant NAME => "an.get_summary_euro_cash";

sub get_name{
    return NAME;
}


sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I must be an ant to issue this
    $self->{privilege_required} = Bcd::Data::Users::TREASURER_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #no controls, my session is sufficient

    #I should simply get the post code... I can see only my ant nest cash
    my $post_code = $stash->get_session_post_code($self->{session_id});

    my $ant_nest_cash_e = Bcd::Data::AntNests->get_cash_euro_account_id($stash, $post_code);

    $self->_get_summary_of_this_account($stash, $ant_nest_cash_e);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}



1;
