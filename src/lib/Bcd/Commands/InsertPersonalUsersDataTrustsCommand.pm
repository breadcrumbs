package Bcd::Commands::InsertPersonalUsersDataTrustsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Errors::ErrorCodes;
use Bcd::Commands::SessionCommand;
our @ISA = qw(Bcd::Commands::SessionCommand);

use Bcd::Data::Trust;
use Bcd::Data::Users;

use constant NAME => "us_insert_personal_data_trusts";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I have several parameters...
    push (@{$self->{parameters}}, "t_first_name");
    push (@{$self->{parameters}}, "t_last_name");
    push (@{$self->{parameters}}, "t_address");
    push (@{$self->{parameters}}, "t_home_phone");
    push (@{$self->{parameters}}, "t_mobile_phone");
    push (@{$self->{parameters}}, "t_sex");
    push (@{$self->{parameters}}, "t_birthdate");
    push (@{$self->{parameters}}, "t_email");
    
    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, I see if the trusts are valid... they are expressed in bBel
    if ($self->trust_bb_validity_fail($self->{t_first_name})){
	return;
    }
    if ($self->trust_bb_validity_fail($self->{t_last_name})){
	return;
    }
    if ($self->trust_bb_validity_fail($self->{t_address})){
	return;
    }
    if ($self->trust_bb_validity_fail($self->{t_home_phone})){
	return;
    }
    if ($self->trust_bb_validity_fail($self->{t_mobile_phone})){
	return;
    }
    if ($self->trust_bb_validity_fail($self->{t_sex})){
	return;
    }
    if ($self->trust_bb_validity_fail($self->{t_birthdate})){
	return;
    }
    if ($self->trust_bb_validity_fail($self->{t_email})){
	return;
    }

    #I should get my id from the session
    my $my_id = $stash->get_session_id($self->{session_id});
    
    #then I get the user data
    my $personal_data_id = Bcd::Data::Users->get_user_personal_data_id($stash, $my_id);

    #ok, the parameters seems ok now...
    Bcd::Data::PersonalUsersData->insert_personal_users_data_trusts
	($stash,
	 $personal_data_id,
	 Bcd::Data::Trust::dec_from_bBel($self->{t_first_name}),
	 Bcd::Data::Trust::dec_from_bBel($self->{t_last_name}),
	 Bcd::Data::Trust::dec_from_bBel($self->{t_address}),
	 Bcd::Data::Trust::dec_from_bBel($self->{t_home_phone}),
	 Bcd::Data::Trust::dec_from_bBel($self->{t_mobile_phone}),
	 Bcd::Data::Trust::dec_from_bBel($self->{t_sex}),
	 Bcd::Data::Trust::dec_from_bBel($self->{t_birthdate}),
	 Bcd::Data::Trust::dec_from_bBel($self->{t_email})
	 );

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#no output, only the exit code.

1;
