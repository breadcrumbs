package Bcd::Commands::GetPriceForAddingPresenceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::GetPriceForThisAdCommand;
use base(qq/Bcd::Commands::GetPriceForThisAdCommand/);
use Data::Dumper;

use Bcd::Data::WebSite;

use constant NAME => "ws.get_price_for_adding_presence";

use constant PARAM_TABLE => 
    [
     ["id_ad"],
     {
	 id_ad    => ["id_type",    "required"],
     },
     [Bcd::Commands::GetPriceForThisAdCommand::PARAM_TABLE],
     ];

sub get_param_table(){
    return PARAM_TABLE;
}


sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash ) = @_;

    my $input = $self->{_in}->get_parsed_result();
    my $my_id = $stash->get_session_id($input->{session_id});

    #the ad must belong to myself...
    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $input->{id_ad});
    if (!defined($ad)){
	$self->{exit_code} =  Bcd::Errors::ErrorCodes::BEC_UNKNOWN_AD;
	return;
    }

    if ($ad->{id_user} != $my_id){
	$self->{exit_code} =  Bcd::Errors::ErrorCodes::BEC_YOU_DO_NOT_OWN_THIS_AD;
	return;
    }

    my ($price, $reachable_ants, $total_ants) = 
	Bcd::Data::WebSite->get_price_report_for_adding_a_presence
	($stash, $input->{id_ad}, $my_id, $input->{p_min},
	 $input->{p_max},
	 Bcd::Data::Trust::dec_from_bBel($input->{t_e}),
	 Bcd::Data::Trust::dec_from_bBel($input->{t_c}));

    $self->{price} = $price;
    $self->{reachable_ants} = $reachable_ants;
    $self->{total_ants} = $total_ants;

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

1;
