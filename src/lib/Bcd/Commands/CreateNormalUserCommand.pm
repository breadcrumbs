package Bcd::Commands::CreateNormalUserCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::CreateUserCommand;
use base(qq/Bcd::Commands::CreateUserCommand/);
use Bcd::Data::Users;
use Bcd::Errors::ErrorCodes;


use constant NAME => "an.create_normal_user";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I need a simple ant role to create a user.
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I need the tutor trust parameter
    push (@{$self->{"parameters"}}, "tutor_trust");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #if the common checks don't pass is futile to go forward
    if ($self->_common_checks_fail($stash)){
	return;
    }

    #the other check is to see if I want to create the ant in my own ant nest
    
    #I should check the ant nest of the user.
    my $value = $stash->get_cache()->get($self->{session_id});

    if ($value->{ant_nest} ne $self->{ant_nest}){
	#You are trying to create an ant outside your nest
	$self->{exit_code} = 
	    Bcd::Errors::ErrorCodes::BEC_TRYING_TO_CREATE_ANT_OUTSIDE_YOUR_NEST;
	return;
    }

    #I should get my id
    my $id = $value->{user_id};
    my $token = 
	Bcd::Data::Users->create_normal_user($stash, $self, 1, 1, $id, $self->{tutor_trust});

    $self->{output_token} = $token;

    my $nick_tutor = $stash->get_session_nick($self->{session_id});

    my $template_vars = {
	user         => $self,
	token        => $token,
	nick_tutor   => $nick_tutor,
    };

    #I should send two letters...
    #one to myself, just to remind the token and the user that I have invited...
    $self->_send_mail_to_user_with_id
	($stash, 'you_have_created_a_user.tt2', $id, $template_vars);

    #the other to the invited ant
    my $other_id = $stash->get_id_of_last_user();
    $self->_send_mail_to_user_with_id
	($stash, 'you_have_been_invited.tt2', $other_id, $template_vars);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#ok, now I should get the output...
# sub get_output{
#     my $self = shift;
#     my @output;
    
#     $self->_add_exit_code(\@output);

#     if ($self->{"exit_code"} == Bcd::Errors::ErrorCodes::BEC_OK){
# 	$self->_add_scalar("token", $self->{"output_token"}, \@output);
#     } 

#     return \@output;
# }

sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_scalar("token", $self->{output_token}, $output);
}

1;
