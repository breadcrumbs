package Bcd::Commands::PayInvoiceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::InvoiceCommand;
use base(qq/Bcd::Commands::InvoiceCommand/);
use Data::Dumper;
use Bcd::Constants::AdsConstants;

use constant NAME => "ws_pay_invoice";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I have only one parameter...
    #the way of payment is decided by the function... automatically
    push (@{$self->{parameters}}, "id_invoice");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});
    
    #let's check only that the invoice correspond to an ad
    my $invoice = Bcd::Data::Invoices->get_invoice_hash($stash, $self->{id_invoice});

    my $ad = Bcd::Data::Ads->get_ad_hash($stash, $invoice->{id_ad});
    my $loc = Bcd::Data::Ads->get_ad_locality_hash($stash, $invoice->{id_ad}, $invoice->{id_locus});

    #ok, the user must own the ad...
    if ($invoice->{user_to} != $my_id){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_INVOICE_IS_NOT_FOR_YOU;
	return;
    }

    #the invoice must be in the correct state
    if ($invoice->{id_status} != Bcd::Constants::InvoicesConstants::EMITTED){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    if ($self->_pay_invoice_fail($stash, $my_id, $invoice, $self->{id_invoice}, $ad, $loc) == 1){
	return;
    }

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#I have the long token as the output
sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_scalar("long_token", $self->{long_token}, $output);
}

1;
