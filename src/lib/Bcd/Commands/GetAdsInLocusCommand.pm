package Bcd::Commands::GetAdsInLocusCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Data::Users;
use Data::Dumper;

use constant NAME => "ws.get_ads_in_locus";

sub get_name{
    return NAME;
}

use constant PARAM_TABLE => 
    [
     ["id_locus"],
     {
	 id_locus => ["id_type", "required"],
     },
     [Bcd::Commands::SessionCommand::PARAM_TABLE],
     ];


sub get_param_table{
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    my $input = $self->{_in}->get_input();
    my $my_id = $stash->get_session_id($input->{session_id});

    #ok, first of all I should get the locus and check if it belongs to me
    my $presence = Bcd::Data::PublicSites->get_presence_decoded_hash
	($stash, $input->{id_locus});

    if (!defined($presence)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_DATA;
	return;
    }

    #does the presence belong to me?
    if ($presence->{id_user} != $my_id){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED;
	return;
    }

    #ok, the presence belongs to me.
    my $dataset = Bcd::Data::Ads->get_ads_for_locus_ds($stash, $input->{id_locus});
    
    $self->{ads_in_locus} = $dataset;
    $self->{presence} = $presence;
    
    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_record_set($self->{ads_in_locus},"ads_in_locus", $output);
    $self->_add_object    ($self->{presence},    "presence",     $output);
}

1;
