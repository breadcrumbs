package Bcd::Commands::CommitTrustChangeCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::TrustCommand;
use base(qq/Bcd::Commands::TrustCommand/);

use Data::Dumper;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest

use constant NAME => "tr.commit_trust_change";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I need a simple ant role to create a booking
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I don't have any parameters... I rollback my own changes

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #no controls... I am only connected to the daemon, I need only my
    #id

    my $id = $stash->get_session_id($self->{session_id});
    Bcd::Data::Trust->commit_trust_changes($stash, $id);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;


}

#no output

1;
