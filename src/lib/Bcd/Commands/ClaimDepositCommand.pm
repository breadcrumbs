package Bcd::Commands::ClaimDepositCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Digest::SHA1;

use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use Data::Dumper;

use constant NAME => "bk.claim_deposit";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #The privilege is not only for the ant... if the ticket is a withdrawal ticket than 
    #only the treasurer can do this.
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I need only the secret token
    push (@{$self->{"parameters"}}, "secret_token");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, first of all I should get the booking which is not collected which I own
    my $my_id = $stash->get_session_id($self->{session_id});

    #then I get the booking
    my $booking = Bcd::Data::Deposits->get_pending_booking_from_user_id_arr
	($stash, $my_id);

    if (! defined($booking)){
	#there is no deposit pending
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_DEPOSIT;
	return;
    }

    #is this booking in the right state?
    if (
	( $booking->[4] != Bcd::Constants::DepositsConstants::OFFLINE_ACKNOWLEDGED)
	and 
	( $booking->[4] != Bcd::Constants::DepositsConstants::DEPOSIT_TAKEN_ONLINE)
	){
	#nope, fail
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #ok, is this booking a deposit?
    if ( $booking->[1] == 0){
	#nope, fail
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_BOOKING_IS_A_WITHDRAWAL;
	return;
    }

    #OK, Test the secret code
    my $sha = Digest::SHA1->new;
    $sha->add($self->{secret_token});
    my $digest = $sha->hexdigest;


    if ($digest ne $booking->[7]){
	#no valid secret token
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_SECRET_TOKEN;
	return;
    }

    #ok, at last I can claim this deposit
    my $ant_nest_code = $stash->get_session_post_code($self->{session_id});

    my ($amount_deposited, $new_total) = 
	Bcd::Data::Deposits->claim_this_deposit_object($stash, $ant_nest_code, $booking);

    #I should humanize these two amounts
    $amount_deposited = Bcd::Data::RealCurrency::humanize_this_string($amount_deposited);
    $new_total        = Bcd::Data::RealCurrency::humanize_this_string($new_total);
    my $amount_given  = Bcd::Data::RealCurrency::humanize_this_string($booking->[3]);

    $self->{amount_deposited} = $amount_deposited;
    $self->{new_total}        = $new_total;
    $self->{amount_given}     = $amount_given;



    #send the mail
    my $template_vars = 
    {
	amount_deposited   => $amount_deposited,
	new_total          => $new_total,
	amount_given       => $amount_given,
    };

    $self->_send_mail_to_user_with_id
	($stash, 'deposit_done.tt2', 
	 $my_id, $template_vars);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{

    my ($self, $output) = @_;

    $self->_add_scalar("amount_deposited", $self->{amount_deposited}, $output);
    $self->_add_scalar("new_total"       , $self->{new_total}       , $output);
    $self->_add_scalar("amount_given"    , $self->{amount_given}    , $output);
}

1;
