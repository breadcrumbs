package Bcd::Commands::GrantRoleCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Data::Users;

use constant NAME => "sv.grant_role";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{"privilege_required"} = Bcd::Data::Users::BC_ROOT_ROLE;
    
    push (@{$self->{"parameters"}}, "ant_nest");
    push (@{$self->{"parameters"}}, "nick"); 
    push (@{$self->{"parameters"}}, "role");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #first of all I check that the role is not very large
    if ($self->{role} & Bcd::Data::Users::BC_ROOT_ROLE){
	#you cannot give the bc root role
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_CANNOT_GRANT_BC_ROOT_ROLE;
	return;
    }

    #ok, check the post code.
    my $code = $self->{ant_nest};
    if (! Bcd::Data::PostCode::is_valid_post_code_with_sub($code)){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE;
	return;
    }

    #the command is implemented only for initial ant nests
    if (! Bcd::Data::AntNests->is_this_ant_nest_starting($stash, $code)){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_NOT_IMPLEMENTED;
	return;
    }

    if ( Bcd::Data::AntNests->get_initial_ants_remaining($stash, $code) != 0){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_NOT_IMPLEMENTED;
	return;
    }

    if (
	$self->{role} != Bcd::Data::Users::BOSS_ROLE
	and 
	$self->{role} != Bcd::Data::Users::TREASURER_ROLE
	){
	#you can only grant the boss or treasurer, for now
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_NOT_IMPLEMENTED;
	return;
    }

    my $new_boss = 0;

    #Ok, now I can grant the role, at least for now.
    if ($self->{role} == Bcd::Data::Users::BOSS_ROLE){
	if (! Bcd::Data::AntNests->is_missing_boss($stash, $code)){
	    #the boss is not missing
	    $self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_DUPLICATE_INITIAL_BOSS_OR_TREASURER;
	    return;
	}
	$new_boss = 1;
    }

    if ($self->{role} == Bcd::Data::Users::TREASURER_ROLE){
	if (! Bcd::Data::AntNests->is_missing_treasurer($stash, $code)){
	    #the treasurer is not missing
	    $self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_DUPLICATE_INITIAL_BOSS_OR_TREASURER;
	    return;
	}
    }

    #Ok, seems all working now
    my $ans = Bcd::Data::Users->grant_role_to_user
	($stash, $code, $self->{nick}, $self->{role});

    if ( $ans == 0){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER;
	return;
    }

    #ok, now I can tell the ant nest that it has a new boss
    if ($new_boss){
	Bcd::Data::AntNests->new_starting_boss($stash, $code);


	  ##############################
	  ### patch, only to have the correspondence between the user and the site
	  ### to be removed...
	  Bcd::Data::PublicSites->add_initial_hq($stash, $code);


    } else {
	Bcd::Data::AntNests->new_starting_treasurer($stash, $code);
    }

    $self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_OK;
    return;

}


1;
