package Bcd::Commands::BcConnectCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::GenericConnectCommand;
use Bcd::Errors::ErrorCodes;
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Common::CommonConstants;
use Bcd::Commands::InputCommandParser;

our @ISA = qw(Bcd::Commands::GenericConnectCommand);

use constant PARAM_TABLE => 
    [
     ["password"],
     {
	 password => ["string_type",            "required"],
     },
     ];

use constant NAME => "bc_connect";

sub get_name{
    return NAME;
}

sub get_param_table(){
    return PARAM_TABLE;
}


sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);
    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #the user and ant nests are not relevant here...
    #the only real parameter is the password...

    my $parsed_result = $self->{_in}->get_parsed_result();
    $parsed_result->{user} = Bcd::Common::CommonConstants::BC_ROOT;
    $parsed_result->{ant_nest} = 0; #no ant nest...

    $self->_generic_connect($stash);

}



1;

