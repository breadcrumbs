package Bcd::Commands::ChangeTaoInEuroCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Digest::SHA1;

use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use Data::Dumper;

#This is only a patch, just to have a safeguard
use constant MINIMUM_TAO_CHANGE => 40;

use constant NAME => "bk.change_tao_in_euro";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I need the number of euros to change
    push (@{$self->{parameters}}, "amount");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my $my_id = $stash->get_session_id($self->{session_id});

    if (! Bcd::Data::Bank->can_this_user_withdraw_tao($stash, $my_id, $self->{amount})){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_WILL_RENDER_NEGATIVE_THE_ACCOUNT;
	return;
    }

    if ($self->{amount} < MINIMUM_TAO_CHANGE ){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_TOO_SMALL;
	return;
    }

    #ok, the amount is good, it will not render negative the account...
    my $post_code = $stash->get_session_post_code($self->{session_id});

    my ($new_tot_euro, $new_tot_tao, $euro_obtained, $tao_converted) = 
	Bcd::Data::Bank->change_tao_in_euro($stash, $my_id, $post_code, $self->{amount});

    #I should humanize the euro string
    $new_tot_euro  = Bcd::Data::RealCurrency::humanize_this_string($new_tot_euro);
    $euro_obtained = Bcd::Data::RealCurrency::humanize_this_string($euro_obtained);

    #ok, let's send a confirmation letter...
    my $template_vars = {
	new_tot_euro  => $new_tot_euro,
	new_tot_tao   => $new_tot_tao,
	euro_obtained => $euro_obtained,
	amount        => $self->{amount},
	tao_converted => $tao_converted,
    };

    $self->{euro_obtained} = $euro_obtained;
    $self->{new_tot_euro} = $new_tot_euro;
    $self->{new_tot_tao}  = $new_tot_tao;
    $self->{tao_converted} = $tao_converted;

    $self->_send_mail_to_user_with_id
	($stash, 'change_tao_euro_done.tt2', $my_id, $template_vars);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{

    my ($self, $output) = @_;

    $self->_add_scalar("euro_obtained", $self->{euro_obtained},   $output);
    $self->_add_scalar("new_tot_euro", $self->{new_tot_euro},     $output);
    $self->_add_scalar("new_tot_tao" , $self->{new_tot_tao} ,     $output);
    $self->_add_scalar("tao_converted" , $self->{tao_converted} , $output);
}

1;
