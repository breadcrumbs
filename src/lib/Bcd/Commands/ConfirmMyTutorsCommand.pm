package Bcd::Commands::ConfirmMyTutorsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#I don't have the session, to confirm my tutors I need only
#the token and the trust.
use Bcd::Commands::SimpleCommand;
use base(qq/Bcd::Commands::SimpleCommand/);

use Data::Dumper;

use constant NAME => "an.confirm_my_tutors";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I don't need a particular privilege

    push (@{$self->{"parameters"}}, "token");
    push (@{$self->{"parameters"}}, "trust_first_tutor");
    push (@{$self->{"parameters"}}, "trust_second_tutor");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    if ($self->trust_validity_fail($self->{trust_first_tutor})){
	return;
    }
    if ($self->trust_validity_fail($self->{trust_second_tutor})){
	return;
    }

    my $new_user_id = Bcd::Data::Users->
	get_new_user_id_from_this_token($stash, $self->{token});

    if (!defined($new_user_id)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN;
	return;
    }

    Bcd::Data::Users->confirm_my_tutors($stash, $new_user_id, 
					$self->{trust_first_tutor},
					$self->{trust_second_tutor});

    #ok, send a mail:
    my $template_vars = {};
    $self->_send_mail_to_user_with_id
	($stash, "you_are_an_ant_now.tt2", $new_user_id, $template_vars);


    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

1;
