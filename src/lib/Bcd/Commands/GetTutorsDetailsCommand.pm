package Bcd::Commands::GetTutorsDetailsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SimpleCommand;
use base(qq/Bcd::Commands::SimpleCommand/);

use Data::Dumper;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest


use constant NAME => "an.get_tutors_details";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #any one can exec this command, but you should have the token
    push (@{$self->{"parameters"}}, "token");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, first of all I should check that the token is valid
    my $new_user_id = Bcd::Data::Users->
	get_new_user_id_from_this_token($stash, $self->{token});

    if (!defined($new_user_id)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN;
	return;
    }

    my $user = Bcd::Data::Users->select_user_data($stash, $new_user_id);

    #my $user_shared = &share({});
    #%{$user_shared} = %{$user}; #?
    $self->{new_user} = $user;

    my ($tut1, $tut2) = Bcd::Data::Users->get_tutors_details($stash, $new_user_id);

    #ok, I can now store these two informations...

    #my $res  = &share({});

    #I should copy the user hash in the shared hash
    #%{$res} = %{$tut1}; #?

    $self->{tutor_first} = $tut1;

    if (defined($tut2)){

	#my $res1  = &share({});

	#I should copy the user hash in the shared hash
	#%{$res1} = %{$tut2}; #?

	$self->{tutor_second} = $tut2;
	$self->{tutor_2_existing} = 1;

    } else {
	$self->{tutor_2_existing} = 0;
    }

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_object($self->{new_user},    "new_user",    $output);
    $self->_add_object($self->{tutor_first}, "tutor_first", $output);

    $self->_add_scalar("tutor_2_existing", $self->{tutor_2_existing} , $output);

    if ($self->{tutor_2_existing}){
	$self->_add_object($self->{tutor_second}, "tutor_second", $output);
    }
}

1;
