package Bcd::Commands::GetGenericPriceEstimateCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _compute_price_estimate{
    my ($self, $stash, $buyer, $seller, $price, $can_use_credit) = @_;

    my 
	(
	 $credits_towards_seller, 
	 $cheque_amount, 
	 $cheque_price, 
	 $convertible_tao, 
	 $total_price,
	 $can_be_payed_now) = Bcd::Data::CreditsAndDebits->get_price_estimate_report_for_a_payment
	 ($stash, $buyer, $seller, $price, $can_use_credit);

    $self->{credits_towards_seller} = $credits_towards_seller;
    $self->{cheque_amount}          = $cheque_amount;
    $self->{cheque_price}           = $cheque_price;
    $self->{convertible_tao}        = $convertible_tao;
    $self->{total_price}            = $total_price;
    $self->{can_be_payed_now}       = $can_be_payed_now;

}


sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_scalar("credits_towards_seller", $self->{credits_towards_seller}, $output);
    $self->_add_scalar("cheque_amount", $self->{cheque_amount}, $output);
    $self->_add_scalar("cheque_price", $self->{cheque_price}, $output);
    $self->_add_scalar("convertible_tao", $self->{convertible_tao}, $output);
    $self->_add_scalar("total_price", $self->{total_price}, $output);
    $self->_add_scalar("can_be_payed_now", $self->{can_be_payed_now}, $output);

}

1;
