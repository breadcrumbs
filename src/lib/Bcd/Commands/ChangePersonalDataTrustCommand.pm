package Bcd::Commands::ChangePersonalDataTrustCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::SessionCommand;
our @ISA = qw(Bcd::Commands::SessionCommand);
use Bcd::Data::PersonalUsersData;

use Data::Dumper;
use constant NAME => "us_change_personal_data_trust";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no parameters, but I must be an ant to make this command
    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #I have two parameters, the trust item and the new value
    push (@{$self->{parameters}}, "trust_item");
    push (@{$self->{parameters}}, "new_trust");
    
    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #ok, first of all I check the validity of the trust...
    if ($self->trust_bb_validity_fail($self->{new_trust})){
	return;
    }

    #ok, then I check the validity of the trust item
    my $validity = 0;
    if ($self->{trust_item} eq "t_first_name"){
	$validity = 1;
    } elsif ($self->{trust_item} eq "t_last_name"){
	$validity = 1;
    } elsif ($self->{trust_item} eq "t_address"){
	$validity = 1;
    } elsif ($self->{trust_item} eq "t_home_phone"){
	$validity = 1;
    } elsif ($self->{trust_item} eq "t_mobile_phone"){
	$validity = 1;
    } elsif ($self->{trust_item} eq "t_sex"){
	$validity = 1;
    } elsif ($self->{trust_item} eq "t_birthdate"){
	$validity = 1;
    } elsif ($self->{trust_item} eq "t_email"){
	$validity = 1;
    }

    if ($validity == 0){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_PERSONAL_DATA_ITEM;
	return;
    }

    #ok, I can update the data...
    my $my_id = $stash->get_session_id($self->{session_id});
    my $personal_data_id = Bcd::Data::Users->get_user_personal_data_id($stash, $my_id);

    #ok, let's update...
    Bcd::Data::PersonalUsersData->change_personal_users_data_trust
	($stash, $self->{trust_item}, $personal_data_id, Bcd::Data::Trust::dec_from_bBel($self->{new_trust}));


    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

}

#no output, only the exit code
1;
