package Bcd::Commands::InputCommandParser;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Errors::ErrorCodes;
use Data::Dumper;
use MIME::Base64;
use Compress::Zlib;
use Storable qw(thaw);
use Bcd::Data::PostCode;
use Bcd::Data::Trust;
use POSIX qw(strftime);
use Mail::RFC822::Address qw(valid);



use constant PARAM_CHECK_TABLE => {
    nick_type                      => \&_check_nick,
    post_code_with_sub_or_not_type => \&_check_post_code_with_sub_or_not,
    string_type                    => \&_check_string,
    id_type                        => \&_check_id,
    tao_type                       => \&_check_tao,
    trust_bb                       => \&_check_trust_bb,
    post_code_with_sub             => \&_check_post_code_with_sub,
    similarity_type                => \&_check_similarity_type,
    boolean_type                   => \&_check_boolean_type,
    id_nullable_type               => \&_check_nullable_id_type,
    special_site_type              => \&_check_special_site_type,
    email_type                     => \&_check_email_type,
};

sub _check_email_type{
    my $value = shift;

    if (valid($value)) {
	return 0;
    } else {
	return Bcd::Errors::ErrorCodes::BEC_INVALID_EMAIL;
    }

}

sub _check_special_site_type{
    my $value = shift;

    if ($value ne 'my_home' and $value ne 'your_home' and $value ne "NULL"){
	return Bcd::Errors::ErrorCodes::BEC_INVALID_SPECIAL_SITE_SPECIFICATION;
    }

    return 0;
}

sub _check_nullable_id_type{
    my $value = shift;

    #this id can be 'NULL'
    if ($value eq "NULL"){
	return 0;
    }
    return _check_id($value);
}

sub _check_boolean_type{
    my $value = shift;
    if ($value ne '0' and $value ne '1'){
	return Bcd::Errors::ErrorCodes::BEC_BOOLEAN_PARAMETER_OUT_OF_RANGE;
    }
    return 0;
}

sub _check_similarity_type{
    my $value = shift;
    if ($value ne "space"){
	return Bcd::Errors::ErrorCodes::BEC_NOT_IMPLEMENTED;
    }
    return 0;
}

sub _check_tao{
    my $value = shift;

    #the value should be positive
    if ($value < 0){
	return Bcd::Errors::ErrorCodes::BEC_INVALID_TAO_IMPORT;
    }
    
}

sub _check_trust_bb{
    my $value = shift;
    if ( ! Bcd::Data::Trust->is_valid_trust_bb($value)){
	return Bcd::Errors::ErrorCodes::BEC_INVALID_TRUST;
    }
}

sub _check_id{
    my $value = shift;

    if ($value !~ /^\d+$/){
	return Bcd::Errors::ErrorCodes::BEC_INVALID_ID;
    }
}

##
##These are the functions which check the validity of the parameter.
sub _check_nick{
    my $value = shift;

    #the nicks which start with "bc-" are reserved
    if ($value =~ /^bc-/){
	return Bcd::Errors::ErrorCodes::BEC_RESERVED_ID;
    }

    if ($value !~ /^[\w-]{3,20}$/){
	return Bcd::Errors::ErrorCodes::BEC_LENGTH_OR_CHAR_NOT_VALID_IN_ID;
    }

    return 0;
}

sub _check_post_code_with_sub{
    my $value = shift;

    if (! Bcd::Data::PostCode::is_valid_post_code_with_sub($value)){
	return Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE;
    }

    return 0;
}

sub _check_post_code_with_sub_or_not{
    my $value = shift;



    #let's check the post code. It can be generic, so with or without suffix
    if (! Bcd::Data::PostCode::is_valid_post_code_with_sub($value)){
	if ( ! Bcd::Data::PostCode::is_valid_post_code($value)){
	    return Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE;
	}
    }

    return 0;
}

sub _check_string{
    my $value = shift;

    #no controls, for now... maybe length or other... a regex?

    return 0;
}


sub new {
    my ($class, $mode, $script) = @_;
    my $self  = {};

    $self->{mode}   = $mode;
    $self->{script} = $script;
    if ($script == 1){
	#ok, I can open the file for scripting
	my @time = localtime();
	my $year = $time[5] + 1900;
	my $month = $time[4] + 1;
	my $name = "/tmp/${year}_${month}_$time[3]_$time[2]_$time[1]_$time[0]_bc_script";
	my $file;
	open $file, "> $name" or die "cannot open file $name";
	$self->{script_file} = $file;
    }

    bless ($self, $class);
    return $self;
}

sub end_of_input{
    my $self = shift;
    if ($self->{script} == 1){
	close $self->{script_file};
    }
}

sub select_mode{
    my ($self, $mode) = @_;
    $self->{mode} = $mode;
}

sub start_parsing_command {
    my ($self, $param_table) = @_;

    $self->{param_table}   = $param_table;
    $self->{parsing_result}= Bcd::Errors::ErrorCodes::BEC_OK;
    $self->{parsing_index} = 0;
    #$self->{err_flag}      = 0;
    $self->{parsed_result} = {};
    $self->{_args} = [];

}

#this method selects a particular input stream from which
#we can extract the parameters.
sub new_command_from_input{
    my ($self, $command) = @_;
    if ($self->{script} == 1){
	#ok, I write the command name in the file
	$self->{script_file}->flush() if ($command eq "an_get_summary");
	my $now_string = strftime "%a %b %e %H:%M:%S %Y", localtime;
	my $file = $self->{script_file};
	print $file "\n#### new command at $now_string\n";
	print $file $command . "\n";
    }
    #$self->{_input} = $input_stream;
}

sub select_command{
    my ($self, $command) = @_;
    $self->{_command} = $command;
}

sub _validate_params{
    my $self = shift;

    $self->{parsing_result} = Bcd::Errors::ErrorCodes::BEC_OK;

    my $param_list = $self->{param_table}->[0];
    my $param_hash = $self->{param_table}->[1];
    foreach(@{$param_list}){
	if ($self->_validate_param($param_hash->{$_}, $self->{parsed_result}->{$_})){
	    return 1;
	}
    }

    return 0;
}

sub _validate_param{
    my ($self, $param_desc, $param_value) = @_;

    #first of all I check the existence of the parameter
    if (!defined($param_value)){
	if ($param_desc->[1] eq "required"){
	    $self->{parsing_result} = Bcd::Errors::ErrorCodes::BEC_REQUIRED_PARAMETER_MISSING;
	    return 1; #this parameter is required
	} else {
	    return 0;
	}
    }

    #ok, the parameter has a value, let's check it
    my $param_type = $param_desc->[0];

    if (!exists(PARAM_CHECK_TABLE->{$param_type})){
	die "unknown parameter type $param_type";
    } else {
	my $res;
	if ($res = &PARAM_CHECK_TABLE->{$param_type}($param_value)){
	    if ($res != 0){
		$self->{parsing_result} = $res;
		return 1;
	    }
	}
    }

    #ok all is ok
    return 0;
}

sub parse_binary_input{
    my $self    = shift;
    my $command = $self->{_command};
    #my $input   = $self->{_input};

    my $stream_mode = <STDIN>;
    my $compressed = 0;

    if ($stream_mode =~ /c/){
	$compressed = 1;
    }

    my $blob = "";
    while(<STDIN>){
	chomp;
	if ($_ eq "."){
	    last;
	}
	#the dot at the start of the string should be avoided
	s/^\.(.*)$/$1/sg;

	$blob .= $_;
    }

    #ok, I have the blob
    my $decoded = decode_base64($blob);

    my $stream;
    if ($compressed == 1){
	#ok, I should decompress it!
	$stream = Compress::Zlib::memGunzip($decoded);
    } else {
	$stream = $decoded;
    }

    my $res = thaw $stream;

    #ok, now I make the script, if it is needed
    if ($self->{script} == 1){
	if (ref $res eq "ARRAY"){
	    #Ok, I make a cycle
	    foreach(@{$res}){
		my $tmp = $_;
		$tmp =~ s/^(\.?)/$1$1/sg;
		$tmp =~ s/\012(\.?)/\012$1$1/sg;
		$tmp =~ s/\012(.)/\\\012$1/sg;
		print {$self->{script_file}} $tmp . "\n";
	    }
	    #at the end a print a single dot
	    print {$self->{script_file}} ".\n#END OF COMMAND\n";
	}
    }

    #ok, I have the input... 
    my $param_list;
    if (defined($command)){
	$self->_inject_the_old_parameters($res);
    } else {
	$self->fill_params($self->{param_table}, $res);
    }
}

#this is a function used only to inject the parameters in the old commands
sub _inject_the_old_parameters{
    my ($self, $params) = @_;
    
    my $command = $self->{_command};

    my $param_list = $command->{parameters};

    my $i = 0;
    #if I have the command I inject the parameters
    #inside the command, this is a hack for the legacy commands
    my $ellipsis = 0;
    foreach(@{$params}){
	my $param = $param_list->[$i];
	my $name;
	if (ref($param) eq "HASH"){
	    if ($param->{type} eq "multi_line"){
		$name = $param->{name};
	    } elsif ($param->{type} eq "ellipsis"){
		$ellipsis = 1;
		#ok... I should inject the ellipsis
		if (! exists($command->{ellipsis})){
		    $command->{ellipsis} = []; #empty array
		}
		push(@{$command->{ellipsis}}, $_);
		#print Dumper($command->{ellipsis});
		next; #I do not advance in the counter...
	    }
	} else {
	    $name = $param_list->[$i];
	}

	$command->{$name} = $_;
	$i++;
    }

    #ok, now I should check the number of parameters
    if ($ellipsis == 0){
	#no ellipsis, the number of parameters should be the same
	if (scalar(@{$params}) > scalar(@{$param_list})){
	    $self->{parsing_result} = Bcd::Errors::ErrorCodes::BEC_TOO_MANY_PARAMETERS;
	} elsif (scalar(@{$params}) < scalar(@{$param_list})){
	    $self->{parsing_result} = Bcd::Errors::ErrorCodes::BEC_TOO_FEW_PARAMETERS;
	}
    } else {
	#there is an ellipsis, the number of parameters should be greater
	if (scalar(@{$params}) < scalar(@{$param_list})){
	    $self->{parsing_result} = Bcd::Errors::ErrorCodes::BEC_TOO_FEW_PARAMETERS;
	}
    }

    #I force the ok
    $command->{state} = Bcd::Commands::SimpleCommand::READY_TO_EXECUTE;
}

#this is a recursive function, I fill the params in the tables walking the tree
#of the descendants
sub fill_params{
    my ($self, $param_table, $params) = @_;

    my $children = $param_table->[2];

    if (defined($children)) {
	#recurse inside the children
	foreach(@{$children}){
	    $self->fill_params($_, $params);
	}
    }

    #ok, then I fill this part of the table and return one level up
    my $list = $param_table->[0];
    foreach(@{$list}){
	$self->{parsed_result}->{$_} = shift(@{$params}); #they are in order
    }

}

sub parse_input{
    my $self = shift;

    my $command = $self->{_command};
    
    $command->start_parsing_command() if (defined($command));

    if ($self->{mode} == Bcd::Common::CommonConstants::BINARY_MODE){
	$self->parse_binary_input();
	$self->eot() if (!defined($command));
    } else {

	my @input_params;
	my $multi_line = 0;
	my $back_line  = "";

	while (<STDIN>){

	    chomp;

	    if ($_ eq "."){
		last;
	    }
	    
	    s/^\.(.+)/$1/sg;
	    s/\\\\$/\\/sg;

	    #now the slash part
	    #if it has remained a single slash before the end simply continues on the same line
	    if (/\\$/){
		chop; #eliminate the single slash...
		if ($multi_line == 0){
		    $back_line = $_;
		    $multi_line = 1;
		} else {
		    $back_line .= "\n";
		    $back_line .= $_;
		}
		next;
	    }

	    my $cur_line;	    

	    if ($multi_line == 1){
		$back_line .= "\n";
		$back_line .= $_;
		#I return normally for the next part
		$multi_line = 0;
		$cur_line = $back_line;
	    } else {
		$cur_line = $_;
	    }

	    #if (defined($command)){
		#$command->parse_line($cur_line);
	    #} else {
	    push(@input_params, $cur_line);
	    #}

	}

	if (defined($command)){
	    $self->_inject_the_old_parameters(\@input_params);
	} else {
	    $self->fill_params($self->{param_table}, \@input_params);
	    $self->eot();
	    
	}
    }
}

sub eot{
    my $self = shift;

#     if ($self->{parsing_index} < scalar(@{$self->{param_table}->[0]})){
# 	$self->{parsing_result} = Bcd::Errors::ErrorCodes::BEC_TOO_FEW_PARAMETERS;
#     }

    #ok, I have all the parameters... let's validate them
    $self->_validate_params();
}

#this is called for the tests
sub eot_from_test{
    my $self = shift;

    #I have to fill the parameters
    $self->fill_params($self->{param_table}, $self->{_args});
    $self->_validate_params();

    #print "eot_from_test\n";
    #print Dumper($self);
    #print "eot_from_test end \n";
}

sub parse_line{
    my ($self, $line) = @_;

#     if ($self->{err_flag} != 0){
# 	#out of sync...
# 	return;
#     }

#     if ($self->{parsing_index} >= scalar(@{$self->{param_table}->[0]})){
# 	#too many parameters...
# 	$self->{err_flag} = 1;
# 	$self->{parsing_result} = Bcd::Errors::ErrorCodes::BEC_TOO_MANY_PARAMETERS;
# 	return;
#     } else {
# 	#check the validity of the parameter... ?
# 	$self->{parsed_result}->{$self->{param_table}->[0]->[$self->{parsing_index}]} =
# 	    $line;
# 	$self->{parsing_index} ++;
# 	return;
#     }

    push(@{$self->{_args}}, $line);
}

sub parse_input_blob{
}

sub get_parsing_result{
    my $self = shift;
    return $self->{parsing_result};
}

sub get_parsed_result{
    my $self = shift;
    return $self->{parsed_result};
}

#an alias
sub get_input{
    my $self = shift;
    return $self->{parsed_result};
}

1;
