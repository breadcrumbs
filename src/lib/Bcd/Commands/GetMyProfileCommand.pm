package Bcd::Commands::GetMyProfileCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Errors::ErrorCodes;
use Bcd::Commands::SessionCommand;
use Bcd::Data::PersonalUsersData;
our @ISA = qw(Bcd::Commands::SessionCommand);

use Bcd::Data::Founders;
use Data::Dumper;
use constant NAME => "us_get_my_profile";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #no parameters, but I must be an ant to make this command
    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;
    
    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #the profile is different if the ant is a founder or a normal ant,
    #but the personal data part is the same

    #check the role inside the cache
    my $role = $stash->get_session_role($self->{session_id});
    my $id   = $stash->get_session_id  ($self->{session_id});

    my $user;
    if ( $role & Bcd::Data::Users::FOUNDER_ROLE){
	#I should take the data for a founder ant...
	$user = Bcd::Data::Founders->get_founder_complete_data_hash($stash, $id);
	#a founder does not have the personal trusts.
	$self->{missing_trusts} = 1;
    }  else {
	#this is a normal ant
	$user = Bcd::Data::Users->select_user_data($stash, $id);

	#now, I should get the trusts for the personal data
	my $trusts = Bcd::Data::PersonalUsersData->get_personal_users_data_trusts_hash
	    ($stash, $user->{id_personal_data});

	if (!defined($trusts)){
	    #ok, this user has not defined his/her personal trusts
	    $self->{missing_trusts} = 1;
	} else {
	    $self->{missing_trusts} = 0;

	    my $res  = {};
	    #I should convert the trusts in bBel
	    foreach(keys(%{$trusts})){
		$res->{$_} = Bcd::Data::Trust::bBel($trusts->{$_});
	    }
	    $self->{personal_trusts} = $res;

	}
    }

    #my $res  = &share({});

    #I should copy the user hash in the shared hash
    #%{$res} = %{$user}; #?
    $self->{output} = $user;
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

}

#ok, I should return this hash to the outside world

sub _add_your_specific_output{
    my ($self, $output) = @_;
    $self->_add_object($self->{output}, "user_details", $output);
    $self->_add_scalar("missing_trusts", $self->{missing_trusts}, $output);

    if ($self->{missing_trusts} == 0){
	$self->_add_object($self->{personal_trusts}, "personal_trusts", $output);
    }
}

1;
