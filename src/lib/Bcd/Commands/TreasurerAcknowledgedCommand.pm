package Bcd::Commands::TreasurerAcknowledgedCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Errors::ErrorCodes;
use Bcd::Data::RealCurrency;

use Data::Dumper;

use constant NAME => "bk.treasurer_acknowledged";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #only the treasurer can make this
    $self->{"privilege_required"} = Bcd::Data::Users::TREASURER_ROLE;

    #I can have simply the booking id, not the token...
    push (@{$self->{"parameters"}}, "id");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    #this is a rather straigthforward command, I should simply control
    #that the booking exists and it is in the right state...

    my $booking_hash = Bcd::Data::Deposits->get_booking_from_id_hash($stash, $self->{id});

    if (!defined($booking_hash)){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNKNOWN_DEPOSIT;
	return;
    }

    #let's see if it is in the right state
    if ( $booking_hash->{status} != Bcd::Constants::DepositsConstants::CREATION_STATE){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #this is only valid for a deposit...
    if ( $booking_hash->{is_deposit} == 0){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_THIS_BOOKING_IS_A_WITHDRAWAL;
	return;
    }


    #let's see if the deposits are online...
    my $code = $stash->get_session_post_code($self->{session_id});

    my $online = Bcd::Data::Configuration->get_ant_nest_value
	($stash, $code, Bcd::Constants::ConfigurationKeys::DEPOSITS_ONLINE);

    if ($online == 1){
	#ok, I can send an email with the full receipt token

	my $human_value = Bcd::Data::RealCurrency::humanize_this_string($booking_hash->{amount});

	my $template_vars = 
	{
	    full_token => $booking_hash->{full_receipt_token},
	    amount     => $human_value,
	};

	$self->_send_mail_to_user_with_id
	    ($stash, 'confirmed_online_deposit.tt2', 
	     $booking_hash->{id_user}, $template_vars);
	
    } else {
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NOT_IMPLEMENTED;
	return;
    }

    #Ok, seems all ok, now...
    #Bcd::Data::Deposits->treasurer_acknowledged($stash, $self->{id});
    Bcd::Data::Deposits->change_state_to_booking
	($stash, $booking_hash->{id}, Bcd::Constants::DepositsConstants::DEPOSIT_TAKEN_ONLINE);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;

};

#no specific output

1;

