package Bcd::Commands::CreateFounderVotingCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Data::Dumper;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);

use constant NAME => "fv_create_voting";


sub get_name {
    return NAME;
}


sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I must be a founder to do this
    $self->{privilege_required} = Bcd::Data::Users::FOUNDER_ROLE;

    #two parameters: the votes, which can be NULL
    push (@{$self->{parameters}}, "boss_vote" );
    push (@{$self->{parameters}}, "treasurer_vote" );

    bless ($self, $class);
    return $self;
}

sub _check_this_candidate_fail{
    my ($self, $candidate, $index_of_flag, $code) = @_;

    if (!defined($candidate) or                        # the candidate does not exist
	!defined($candidate->[$index_of_flag]) or      # the candidate is not a candidate to this mansion
	$candidate->[$index_of_flag] != '1' or         # idem
	$candidate->[2] != $code) {                    # the candidate is not in the same ant nest

	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_VOTE;
	return 1;
    }
}

sub _check_vote_coherence_fail{
    my ($self, $stash, $boss_vote, $treasurer_vote, $code) = @_;

    #the votes must be given to ant which have candidates themselves
    #in the SAME ant nest of the voting ant

    #print "You want to vote for boss $boss_vote and treasurer_vote $treasurer_vote code id $code\n";
    
    if (defined($boss_vote)){
	my $boss = Bcd::Data::Founders->get_founder_main_data_arr($stash, $boss_vote);

	if ($self->_check_this_candidate_fail($boss, 7, $code)){
	    return 1;
	}

    }

    if (defined($treasurer_vote)){
	my $treasurer = Bcd::Data::Founders->get_founder_main_data_arr($stash, $treasurer_vote);

	#print Dumper($treasurer);

	if ($self->_check_this_candidate_fail($treasurer, 8, $code)){
	    return 1;
	}
    }

    #all ok
    return 0;

}


sub _exec{
    my ($self, $stash) = @_;

    #I should check if I am in the right state...
    my $id_user = $stash->get_session_id       ($self->{session_id});
    my $code    = $stash->get_session_post_code($self->{session_id});

    my $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $id_user);

    if ($founder->{id_status} != Bcd::Constants::FoundersConstants::NO_VOTED_YET){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    $self->{boss_vote}      = undef if ($self->{boss_vote} eq 'NULL');
    $self->{treasurer_vote} = undef if ($self->{treasurer_vote} eq 'NULL');

    #let's get the data of the boss and the treasurer
    if ($self->_check_vote_coherence_fail
	($stash, $self->{boss_vote}, $self->{treasurer_vote}, $code)){
	return;
    }
   
    #ok, the votes are coherent I can add them to the db
    Bcd::Data::FoundersVotings->add_a_vote
	($stash, $code, $self->{boss_vote}, $self->{treasurer_vote});


    #then I should update the state of this founder
    Bcd::Data::Founders->update_founder_state($stash, $id_user, Bcd::Constants::FoundersConstants::VOTED);

    #all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
    
}

#no output, only the exit code

1;
