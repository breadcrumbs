package Bcd::Commands::GetPriceEstimateCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::GetGenericPriceEstimateCommand;
use base(qq/Bcd::Commands::GetGenericPriceEstimateCommand/);
use Data::Dumper;

use constant NAME => "cad_get_price_estimate";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);


    push (@{$self->{parameters}}, "seller_id");
    push (@{$self->{parameters}}, "price");
    push (@{$self->{parameters}}, "can_use_credit");

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;
    my $my_id = $stash->get_session_id($self->{session_id});

    $self->_compute_price_estimate
	($stash, $my_id, $self->{seller_id}, $self->{price},
	 $self->{can_use_credit});

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

1;
