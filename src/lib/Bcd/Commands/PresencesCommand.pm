package Bcd::Commands::PresencesCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Constants::PublicSitesConstants;


sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    bless ($self, $class);
    return $self;
}

sub _decode_rel_type_fail{
    my $self = shift;;

    my $id_rel_type;

    if ($self->{rel_type} eq "service"){
	$id_rel_type = Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE;
    } elsif ($self->{rel_type} eq "object"){
	$id_rel_type = Bcd::Constants::PublicSitesConstants::OBJECT_SELL_PRESENCE;
    } elsif ($self->{rel_type} eq "homemade"){
	$id_rel_type = Bcd::Constants::PublicSitesConstants::HOMEMADE_PRESENCE;
    } else {
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_PRESENCE_SPECIFICATION;
	return (1, undef);
    }
    
    return (0, $id_rel_type);
}

sub _decode_site_fail{
    my ($self, $stash, $id_site_in, $id_special_site_in, $post_code_in) = @_;

    ####################
    #section temporary, just to ease the migration towards the new commands
    $id_site_in         = $self->{id_site} if !defined($id_site_in);
    $id_special_site_in = $self->{id_special_site} if !defined($id_special_site_in);
    $post_code_in       = $stash->get_session_post_code($self->{session_id}) if !defined($post_code_in);
    #end of temporary section
    ####################

    my $id_nullable_site;
    my $id_special_site;

    if ($id_site_in ne "NULL"){
	$id_nullable_site = $id_site_in;

	#is this site in my ant nest?
	if (! Bcd::Data::Sites->does_this_site_belongs_to_this_ant_nest
	    ($stash, $id_nullable_site, $post_code_in)){

	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_SITE_OUTSIDE_YOUR_NEST;
	    return (1, undef, undef);
	}

    } else {
	#ok, the site is null, so I should have the special site...
	if ($id_special_site_in eq "my_home"){
	    $id_special_site = Bcd::Constants::PublicSitesConstants::SPECIAL_SITE_MY_HOME;
	} elsif ($id_special_site_in eq "your_home"){
	    $id_special_site = Bcd::Constants::PublicSitesConstants::SPECIAL_SITE_YOUR_HOME;
	} else {
	    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_SPECIAL_SITE_SPECIFICATION;
	    return (1, undef, undef);
	}
    }

    return (0, $id_nullable_site, $id_special_site);
}

sub _check_owner_fail{
    my ($self, $stash, $id_locus_to_check) = @_;

    my $presence = Bcd::Data::PublicSites->get_presence_hash($stash, $id_locus_to_check);

    my $input = $self->{_in}->get_input();
    my $my_id = $stash->get_session_id($input->{session_id});

    if ($my_id != $presence->{id_user}){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED;
	return 1;
    }

    return (0, $presence);
}

sub _check_owner_and_free_state_of_presence_fail{
    my ($self, $stash, $id_locus_to_check) = @_;
    
    my ($ans, $presence) = $self->_check_owner_fail($stash, $id_locus_to_check);
    if ($ans == 1){
	return 1;
    }

    #ok, the locus is mine, let's check if it is "free".
    $ans = Bcd::Data::Invoices->are_there_active_invoices_in_this_locus
	($stash, $id_locus_to_check);

    if ($ans == 1){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_CANNOT_EDIT_ACTIVE_PRESENCE;
	return 1;
    }

    #all ok
    return (0, $presence);
}

#no output, only the exit code...
1;
