package Bcd::Commands::AntNestsListByCodeCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::SimpleCommand;
use Bcd::Errors::ErrorCodes;
use base qw/ Bcd::Commands::SimpleCommand /;
use Bcd::Data::AntNests;
use Data::Dumper;
use Storable;

use constant NAME => "an.lfpc";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I have only one parameter, the post code
    push (@{$self->{"parameters"}}, "post_code");

    bless ($self, $class);
    return $self;
}

sub _exec{
    #nothing to do for now...
    my ($self, $stash) = @_;

    $self->{"output_index"} = 0; 

    #ok, I should now get the code... and transform it in a form like (min-max).

    my $post_code = $self->{"post_code"};

    if ( ! Bcd::Data::PostCode::is_valid_search_pattern($post_code)){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE;
 	return;
    }

    my ($min_code, $max_code) = Bcd::Data::PostCode::get_min_max_search_range($post_code);

    my $array_ref;
    $array_ref = Bcd::Data::AntNests->get_ant_nests_from_post_code($stash, $min_code, $max_code);
    $self->{former_frozen_output} = $array_ref;

    #all ok...
    $self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_OK;;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $output_cmd = $self->{former_frozen_output};
    $self->_add_record_set($output_cmd, 'ant_nests', $output);
}

1;

