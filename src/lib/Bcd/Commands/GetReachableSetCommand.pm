package Bcd::Commands::GetReachableSetCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);

use Data::Dumper;
use Storable;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest

use constant NAME => "tr.get_reachable_set";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I have only one parameter: the limit of the reachable set...
    push (@{$self->{"parameters"}}, "trust");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash ) = @_;

    if ($self->trust_bb_validity_fail($self->{trust})){
	return;
    }

    #I should only control that the trust is valid
    my $t_lim = Bcd::Data::Trust::dec_from_bBel($self->{trust});

    my $id = $stash->get_session_id($self->{session_id});

    my $set = Bcd::Data::Trust->get_reachable_users_from_user_trust_limited
	($id, $t_lim, $stash);


    my @fields =  ('id', 'nick', 'trust');

    my @output;
    push (@output, \@fields);


    foreach(keys(%{$set})){

	my $user = Bcd::Data::Users->select_user_data($stash, $_);
	my $bBelTrust = Bcd::Data::Trust::bBel($set->{$_});

	my @row = ($_, $user->{nick}, $bBelTrust);

	push(@output, \@row);
    }

    $self->{former_frozen_output} = \@output;

    my $code = $stash->get_session_post_code($self->{session_id});
    $self->{ants_count} = Bcd::Data::Users->get_ant_count_from_ant_nest($stash, $code);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{

    my ($self, $output) = @_;

    my $reachable_set = $self->{former_frozen_output};
    $self->_add_record_set($reachable_set, 'reachable_set', $output);
    $self->_add_scalar    ('ants_count', $self->{ants_count}, $output);
}

1;
