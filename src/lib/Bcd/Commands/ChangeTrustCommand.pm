package Bcd::Commands::ChangeTrustCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Commands::TrustCommand;
use base(qq/Bcd::Commands::TrustCommand/);

use Data::Dumper;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest
use constant NAME => "tr.change_trust";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I need a simple ant role to create a booking
    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I need the tutor trust parameter
    push (@{$self->{"parameters"}}, "nick");
    push (@{$self->{"parameters"}}, "trust");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;

    my ($res, $user) = $self->_common_trust_checks_fail($stash);


    if ($res == 1){
	#nothing to do.
	return;
    }

    #ok, I have the user and the trust is valid...

    #now I should see if there is a direct path between the two users
    my $id = $stash->get_session_id($self->{session_id});

    if (! Bcd::Data::Trust->exists_direct_trust_between($stash, $user->{id}, $id)){
	#no, trust, no change possible
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_NO_TRUST_TO_CHANGE;
	return;
    }

    #then I can change the trust

    Bcd::Data::Trust->change_trust($stash, $id, $user->{id}, $self->{trust});

    my $post_code = $stash->get_session_post_code($self->{session_id});
    Bcd::Data::TrustsNet->outdate_net_for_ant_nest($stash, $post_code);

    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


#no specific output.

1;
