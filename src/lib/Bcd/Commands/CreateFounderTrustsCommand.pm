package Bcd::Commands::CreateFounderTrustsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use Bcd::Data::FoundersTrusts;

use base(qq/Bcd::Commands::SessionCommand/);

use constant NAME => "na_create_founder_trusts";

sub get_name {
    return NAME;
}


sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    #I must be a founder to do this
    $self->{privilege_required} = Bcd::Data::Users::FOUNDER_ROLE;

    #I have only one parameter, an ellipsis
    my $parameter = {};

    $parameter->{type} = "ellipsis";
    push (@{$self->{"parameters"}}, $parameter  );

    bless ($self, $class);
    return $self;
}


sub _exec{
    my ($self, $stash) = @_;

    #I should check if I am in the right state...
    my $id_user = $stash->get_session_id($self->{session_id});

    my $founder = Bcd::Data::Founders->get_founder_main_data_hash($stash, $id_user);

    if ($founder->{id_status} != Bcd::Constants::FoundersConstants::TRUST_NOT_PRESENT){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_STATE;
	return;
    }

    #check the number of parameters
    if ( scalar(@{$self->{ellipsis}}) != 
	 ((Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE-1) * 2)){
	#I don't specify the exact number requested
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INVALID_NUMBER_OF_PARAMETERS;
	return;
    }

    #ok, I must cycle for all the ellipsis...
    my $i = -1;
    foreach (@{$self->{ellipsis}}){
	$i++;
	next if ( $i % 2 == 1);

	my $other_id = $_;
	my $trust    = $self->{ellipsis}->[$i+1];

	#I should add the trust
	#print "COMMAND => adding trust $id_user to $other_id trust = $trust\n";
	Bcd::Data::FoundersTrusts->add_founder_trust($stash, $id_user, $other_id, $trust);


    }

    #then I should update the state of this founder
    Bcd::Data::Founders->founder_trust_given($stash, $id_user);

    #all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
    
}

#no output, only the exit code

1;
