package Bcd::Commands::GetNearestAntNestCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SimpleCommand;
use base qw/ Bcd::Commands::SimpleCommand /;

use Bcd::Errors::ErrorCodes;
use Bcd::Data::AntNests;
use Data::Dumper;
use Storable;

use constant NAME => "an_get_nearest_ant_nest";

sub get_name{
    return NAME;
}

#this command should simply try to get the ant nests near a certain
#post code.
sub new{
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    push (@{$self->{"parameters"}}, "id_ant_nest");

    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash) = @_;
    
    #ok, first of all a check on the parameters.
    if ($self->post_code_without_sub_validity_fail($self->{id_ant_nest})){
	return;
    }

    #I should now get the recordset from the ant nest
    my $res = Bcd::Data::AntNests->get_near_ant_nests($stash, $self->{id_ant_nest});

    $self->{former_frozen_output} = $res;
    $self->{exit_code}     = Bcd::Errors::ErrorCodes::BEC_OK;;
}


sub _add_your_specific_output{
    my ($self, $output) = @_;

    $self->_add_record_set($self->{former_frozen_output}, 'ant_nests', $output);
}

1;
