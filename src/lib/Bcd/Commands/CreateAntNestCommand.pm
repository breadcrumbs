package Bcd::Commands::CreateAntNestCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU. General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Data::Users;
use Bcd::Commands::SessionCommand;
use Bcd::Data::AntNests;
use Bcd::Data::PostCode;
use Bcd::Data::Sites;
use base qw(Bcd::Commands::SessionCommand);

use constant NAME => "an.create_ant_nest";

sub get_name{
    return NAME;
}

sub new{

    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    push (@{$self->{"parameters"}}, "code");
    push (@{$self->{"parameters"}}, "name");
    push (@{$self->{"parameters"}}, "totem");
    push (@{$self->{"parameters"}}, "hq");
    push (@{$self->{"parameters"}}, "openings");
    push (@{$self->{"parameters"}}, "initial_ants");

    $self->{"privilege_required"} = Bcd::Data::Users::BC_ROOT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    #I should create here the ant nest...
    #if you are here, then you have passed the security test.

    #first of all I should check that the code is correct.
    my $code = $self->{code};
    if (! Bcd::Data::PostCode::is_valid_post_code_with_sub($code)){
	$self->{"exit_code"} = Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE;
	return;
    }

    #then I should check if there is already an ant nest at this address
    if ( Bcd::Data::AntNests->get_count_ant_nests_min_max
	 ($stash, $code, $code) > 0){
	#no good
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_EXISTING_ANT_NEST_CODE;
	return;
    }

    #...other checks? Maybe that the strings are not null...
    
    #the initial ants should be a positive integer >= 3 < MAX_INITIAL_ANTS AND ODD!
    #check oddiness of the integer
    my $initial_ants = $self->{initial_ants};

    if (($initial_ants % 2 ) == 0){
	#no good, it is even
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_EVEN_INITIAL_SIZE;
	return;
    }

    if (
	($initial_ants < Bcd::Constants::AntNestsConstants::MIN_ANT_NESTS_INITIAL_SIZE)
	||
	($initial_ants > Bcd::Constants::AntNestsConstants::MAX_ANT_NESTS_INITIAL_SIZE)
	){
	#ko, out of range
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INITIAL_SIZE_OUT_OF_RANGE;
	return;
    }


    #I should simply pass the message to the model.
    Bcd::Data::AntNests->create_a_ant_nest
	(
	 $stash,
	 $self->{code},
	 $self->{name},
	 $self->{totem},
	 #$self->{hq},
	 #$self->{openings},
	 $initial_ants
	 );

    #I should create the site and the relationship between the ant nest and the site
    my $site = Bcd::Data::Sites->create_a_site
	($stash, "a name", $self->{hq}, 'no directions', '1', $self->{openings});

    #and then I create the correspondence between the site and the ant nest
    Bcd::Data::Sites->create_a_public_ant_nest_site($stash, $self->{code}, $site);
					
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

1;
