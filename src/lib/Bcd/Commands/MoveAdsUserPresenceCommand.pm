package Bcd::Commands::MoveAdsUserPresenceCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::PresencesCommand;
use base(qq/Bcd::Commands::PresencesCommand/);
use Bcd::Data::Users;
use Data::Dumper;
use Bcd::Constants::PublicSitesConstants;

use constant NAME => "ws.move_ads_user_presence";

sub get_name{
    return NAME;
}

use constant PARAM_TABLE =>
    [
     ["id_locus_to_move", "id_locus_new"],
     {
	 id_locus_to_move        => ["id_type", "required"],
	 id_locus_new            => ["id_type", "required"],
     },
     [Bcd::Commands::SessionCommand::PARAM_TABLE],
     ];


sub get_param_table{
    return PARAM_TABLE;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;

    my $input = $self->{_in}->get_input();
    my $my_id = $stash->get_session_id($input->{session_id});

    my $id_locus_to_move = $input->{id_locus_to_move};
    my $id_locus_new     = $input->{id_locus_new};
    
    my ($ans, $presence_old) = 
	$self->_check_owner_and_free_state_of_presence_fail($stash, $id_locus_to_move);

    if ($ans == 1){
	return;
    }

    my $presence_new;
    ($ans, $presence_new) = $self->_check_owner_fail($stash, $id_locus_new);

    if ($ans == 1){
	return;
    }

    #the new presence should be compatible: (the same rel type!)
    if ($presence_old->{id_rel_type} != $presence_new->{id_rel_type}){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_INCOMPATIBLE_PRESENCE_TYPE;
	return;
    }

    #ok, now I should check that no invoice exists for the ads which
    #insist on the old locus...
    $ans = Bcd::Data::Invoices->are_there_active_invoices_in_this_locus
	($stash, $id_locus_to_move);

    if ($ans == 1){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_CANNOT_EDIT_ACTIVE_PRESENCE;
	return;
    }

    #last control... is there an ad which has already the two loci?
    $ans = Bcd::Data::Ads->is_there_an_ad_which_has_this_two_loci
	($stash, $id_locus_to_move, $id_locus_new);

    if ($ans == 1){
	$self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_AD_WITH_BOTH_LOCI_PRESENT;
	return;
    }

    #Ok, I can move the invoice
    Bcd::Data::Ads->change_locus_for_ads($stash, $id_locus_new, $id_locus_to_move);

    #last, if the presence is not used any more, I can delete it.
    my $count = Bcd::Data::Invoices->get_invoices_count_for_this_locus($stash, $id_locus_to_move);

    if ($count == 0){
	#ok, this locus can be deleted...
	Bcd::Data::PublicSites->delete_presence($stash, $id_locus_to_move);
    }

    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

#No output, only the exit code is sufficient.

1;
