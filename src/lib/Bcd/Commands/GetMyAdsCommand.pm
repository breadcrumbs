package Bcd::Commands::GetMyAdsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#My base class is the create user command
use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);
use Data::Dumper;
use Storable;

use constant NAME => "ws.get_my_ads";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{privilege_required} = Bcd::Data::Users::ANT_ROLE;

    #no parameters, I need only my id

    bless ($self, $class);
    return $self;
}

sub _exec{
    #I should get the name and password of the user.
    my ($self, $stash) = @_;
    my $my_id = $stash->get_session_id($self->{session_id});

    my $ads_ds = Bcd::Data::Ads->get_user_ads_ds($stash, $my_id);
    $self->{former_frozen_output} = $ads_ds;


    #ok, it should be all ok
    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}


sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $dataset = $self->{former_frozen_output};
    $self->_add_record_set($dataset, "my_ads", $output);
}

1;
