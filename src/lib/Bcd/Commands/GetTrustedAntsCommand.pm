package Bcd::Commands::GetTrustedAntsCommand;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Commands::SessionCommand;
use base(qq/Bcd::Commands::SessionCommand/);

use Data::Dumper;
use Storable;

#this command is issued whenever a second ant wants to confirm the
#registration of a new ant in his/her ant nest

use constant NAME => "tr.get_trusted_ants";

sub get_name{
    return NAME;
}

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    $self->{"privilege_required"} = Bcd::Data::Users::ANT_ROLE;

    #I don't need any parameter
    bless ($self, $class);
    return $self;
}

sub _exec{
    my ($self, $stash ) = @_;

    #I don't have to make any controls, just a simple query
    my $id = $stash->get_session_id($self->{session_id});

    
    #I can now have the destinations of this user...
    my $destinations = Bcd::Data::Trust->_get_all_destinations_from_user($id, undef, $stash);
    my @fields =  ('id', 'nick', 'first', 'last', 'cur_trust_bBel', 'backup_trust_bBel');

    my @output;
    push (@output, \@fields);
    

    #then I should get all the trust which were changed.
    my $backups = Bcd::Data::Trust->get_user_backupped_trusts($stash, $id);

    foreach( @{$destinations}){
	my $user = Bcd::Data::Users->select_user_data($stash, $_->[0]);

	my $bBelTrust = Bcd::Data::Trust::bBel($_->[1]);

	my $backup_trust_bb = defined($backups->{$_->[0]}) ? 
	    Bcd::Data::Trust::bBel($backups->{$_->[0]})
	: "NULL";

	my @row = ( $user->{id} , $user->{nick}, 
		    $user->{first_name} , $user->{last_name}, 
		    $bBelTrust, $backup_trust_bb);
	
	push(@output, \@row);
	
    }

    $self->{former_frozen_output} = \@output;


    $self->{exit_code} = Bcd::Errors::ErrorCodes::BEC_OK;
}

sub _add_your_specific_output{
    my ($self, $output) = @_;

    my $trusted_ants = $self->{former_frozen_output};

    $self->_add_record_set($trusted_ants, 'trusted_ants', $output);
}

1;
