package Bcd::Data::Founders;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this module is responsible to create the new ant nests in the system

use strict;
use warnings;

use Data::Dumper;
use Bcd::Constants::FoundersConstants;
use Bcd::Data::StatementsStash;

use constant {

    SELECT_FOUNDERS_EMAILS_WITH_STATUS                    =>
	qq{ SELECT nick_or_name, first_name, email, id_user FROM ant_nests_founders, personal_users_data } .
	qq{ WHERE id_ant_nest = ? AND id_status >= ? } .
	qq{ AND ant_nests_founders.id_personal_data = personal_users_data.id},
	

    SELECT_FOUNDERS_EMAILS                                =>
    qq{ SELECT nick_or_name, first_name, email, id_user FROM ant_nests_founders, personal_users_data } .
    qq{ WHERE id_ant_nest = ? } .
    qq{ AND ant_nests_founders.id_personal_data = personal_users_data.id},

    SELECT_FOUNDER_EMAIL                                     =>
    qq{ SELECT nick_or_name, first_name, email, id_user FROM ant_nests_founders, personal_users_data } .
    qq{ WHERE id_user = ? } .
    qq{ AND ant_nests_founders.id_personal_data = personal_users_data.id},
    
    SELECT_FOUNDERS_ANT_NEST                              =>
    qq{ SELECT * from ant_nests_founders WHERE id_ant_nest = ?},

    SET_CANDIDATES_FLAGS                                  =>
    qq{ UPDATE ant_nests_founders SET boss_candidate_or_flag = ?, }.
    qq{ treasurer_candidate_or_flag = ? where id_user = ?},

    SELECT_FOUNDER_DATA                                   =>
    qq{SELECT * from ant_nests_founders where id_user = ?},

    UPDATE_FOUNDER_STATE                                  =>
    qq{UPDATE ant_nests_founders set id_status=?, last_status_change = localtimestamp where id_user = ?},

    AFTER_VOTE_UPDATE_FLAGS_AND_STATUS                    =>
    qq{UPDATE ant_nests_founders SET boss_candidate_or_flag = 'f',  treasurer_candidate_or_flag = 'f', }.
    qq{ id_status = }. Bcd::Constants::FoundersConstants::WAITING_TO_BE_CREATED  .
    qq{WHERE id_ant_nest = ?},

    SELECT_BOSSES_CANDIDATES                              =>
    qq{SELECT id_user, nick_or_name from ant_nests_founders WHERE id_ant_nest = ? AND boss_candidate_or_flag = 't'},

    SELECT_TREASURERS_CANDIDATES                          =>
    qq{SELECT id_user, nick_or_name from ant_nests_founders WHERE id_ant_nest = ? AND treasurer_candidate_or_flag = 't'},

};

sub get_founder_with_email{
    my ($class, $stash, $id_user) = @_;
    
    my $st = $stash->prepare_cached(SELECT_FOUNDER_EMAIL);
    $st->bind_param(1, $id_user);
    $st->execute();

    my $row = $st->fetchrow_arrayref();
    $st->finish();

    return $row;
}

sub get_all_founders_ant_nest_arr{
    my ($class, $stash, $id_ant_nest) = @_;

    my $st = $stash->prepare_cached(SELECT_FOUNDERS_ANT_NEST);
    $st->bind_param(1, $id_ant_nest);
    $st->execute();

    return $st->fetchall_arrayref();
}

sub nominate_boss_and_treasurer{
    my ($class, $stash, $id_ant_nest, $boss, $treasurer) = @_;

    #first of all I reset all the flags
    my $st = $stash->prepare_cached(AFTER_VOTE_UPDATE_FLAGS_AND_STATUS);
    $st->bind_param(1, $id_ant_nest);
    $st->execute();

    #and then I nominate the boss and the treasurer
    $class->set_founders_flags ($stash, $boss,      '1', '0');
    $class->set_founders_flags ($stash, $treasurer, '0', '1');

    #print "Ok, I nominate boss $boss, and treasurer $treasurer\n";
    $class->update_founder_state
	($stash, $boss, Bcd::Constants::FoundersConstants::BOSS_SET_UP_TO_DO);

    $class->update_founder_state
	($stash, $treasurer, Bcd::Constants::FoundersConstants::WAITING_BOSS_SET_UP);
}

sub get_candidates_bosses{
    my ($class, $stash, $id_ant_nest) = @_;

    my $st = $stash->prepare_cached(SELECT_BOSSES_CANDIDATES);
    $st->bind_param(1, $id_ant_nest);
    $st->execute();
    
    my $arr = $st->fetchall_arrayref();
    return $arr;
}

sub get_candidates_bosses_ds{

    my ($class, $stash, $id_ant_nest) = @_;

    my $st = $stash->prepare_cached(SELECT_BOSSES_CANDIDATES);
    $st->bind_param(1, $id_ant_nest);
    $st->execute();
    
    my $arr = $st->fetchall_arrayref();

    my $var = $st->{NAME_lc};
    unshift (@{$arr}, $var);

    return $arr;
}

sub get_candidates_treasurers{
    my ($class, $stash, $id_ant_nest) = @_;

    my $st = $stash->prepare_cached(SELECT_TREASURERS_CANDIDATES);
    $st->bind_param(1, $id_ant_nest);
    $st->execute();
    
    my $arr = $st->fetchall_arrayref();
    return $arr;
}

sub get_candidates_treasurers_ds{
    my ($class, $stash, $id_ant_nest) = @_;

    my $st = $stash->prepare_cached(SELECT_TREASURERS_CANDIDATES);
    $st->bind_param(1, $id_ant_nest);
    $st->execute();
    
    my $arr = $st->fetchall_arrayref();

    my $var = $st->{NAME_lc};
    unshift (@{$arr}, $var);

    return $arr;
}

sub founder_waiting_for_trust{
    my ($class, $stash, $id_user) = @_;
    
    $class->update_founder_state
	($stash, $id_user, Bcd::Constants::FoundersConstants::TRUST_NOT_PRESENT);
}

sub founder_trust_given{
   my ($class, $stash, $id_user) = @_;
    
    $class->update_founder_state
	($stash, $id_user, Bcd::Constants::FoundersConstants::TRUST_PRESENT);
}

sub founder_immitting_data{
    my ($class, $stash, $id_user) = @_;
    
    $class->update_founder_state
	($stash, $id_user, Bcd::Constants::FoundersConstants::WAITING_FOR_PERSONAL_DATA);
}

sub founder_personal_data_given{
   my ($class, $stash, $id_user) = @_;
    
    $class->update_founder_state
	($stash, $id_user, Bcd::Constants::FoundersConstants::PERSONAL_DATA_IMMITTED);
}

sub update_founder_state{
    my ($class, $stash, $id_user, $new_state) = @_;

    my $st = $stash->prepare_cached(UPDATE_FOUNDER_STATE);
    $st->bind_param(1, $new_state);
    $st->bind_param(2, $id_user);
    $st->execute();
}

sub get_founder_complete_data_hash{
    my ($class, $stash, $id) = @_;

    my $hashref = $class->get_founder_main_data_hash($stash, $id);

    if (!defined($hashref)){
	return undef;
    }

    my %hash;
    %hash = %{$hashref};


    my $data = Bcd::Data::Users->select_personal_data_hash($stash, $hashref->{id_personal_data});

    my $user_id_save = $hashref->{id_user};
    @hash{keys %{$data}} = values %{$data};
    $hash{id} = $user_id_save;

    return \%hash;
    
}

sub get_founder_main_data_hash{
    my ($class, $stash, $id) = @_;
    
    my $st = $stash->prepare_cached(SELECT_FOUNDER_DATA);
    $st->bind_param(1, $id);
    $st->execute();

    my $hash = $st->fetchrow_hashref();
    $st->finish();
    return $hash;
}

sub get_founder_main_data_arr{
    my ($class, $stash, $id) = @_;
	
    my $st = $stash->prepare_cached(SELECT_FOUNDER_DATA);
    $st->bind_param(1, $id);
    $st->execute();

    my $arr = $st->fetchrow_arrayref();
    $st->finish();
    return $arr;
}

sub set_founders_flags {
    my ($class, $stash, $id_founder, $boss_flag, $treasurer_flag) = @_;

    my $st = $stash->prepare_cached(SET_CANDIDATES_FLAGS);
    $st->bind_param(1, $boss_flag);
    $st->bind_param(2, $treasurer_flag);
    $st->bind_param(3, $id_founder);
    $st->execute();
}

#this function get all the confirmed founders to mail them
sub get_confirmed_founders_to_email{
    my ($class, $stash, $id_ant_nest) = @_;

    my $st = $stash->prepare_cached(SELECT_FOUNDERS_EMAILS_WITH_STATUS);
    $st->bind_param(1, $id_ant_nest);
    $st->bind_param(2, Bcd::Constants::FoundersConstants::USER_BOOKED_CONFIRMED);
    $st->execute();

    return $st->fetchall_arrayref();
}

sub get_founders_to_email {
    my ($class, $stash, $id_ant_nest) = @_;

    my $st = $stash->prepare_cached(SELECT_FOUNDERS_EMAILS);
    $st->bind_param(1, $id_ant_nest);
    $st->execute();

    return $st->fetchall_arrayref();
}

sub init_db{

    my ($self, $stash) = @_;
    my $conn = $stash->get_connection();

    my $sth = $conn->prepare(qq{insert into ant_nests_founders_statuses values(?,?)});

    foreach (Bcd::Constants::FoundersConstants::LIST_NEW_USER_STATES){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }

    $sth->finish();


}

1;
