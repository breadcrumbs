package Bcd::Data::Bank;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Data::Dumper;

use Math::BigInt   lib => "GMP";
use Math::BigFloat lib => "GMP";

use Bcd::Data::Accounting::AccountsPlan;
use Bcd::Data::Accounting::Transaction;
use Bcd::Data::CreditsAndDebits;
use Bcd::Common::BasicAccounts;
use Bcd::Data::AntNests;
use Bcd::Constants::Users;

use Bcd::Constants::it::TransactionsTemplates;
use Bcd::Data::RealCurrency;

use Data::Dumper;

use constant {
    OLD_DEPOSIT_TAX      => 0.0005,
    OLD_TREASURER_INCOME => 0.0045,

    DEPOSIT_TAX          => 2000,
    TREASURER_INCOME     => 222,
    
    RATE_EURO_TAO        => 19.3627,
    RATE_TAO_EURO        => 19.37238135,
    
    MINIMUM_CREDIT_SCORE => -13,
    R_MAX                => 1.22,
    CHEQUE_TAX           => 0.0005,
};

# r_max is the r which leads to the minimum credit score.
# actually the real r is a bit larger, but in this case we are "safe".
# score(r_max) =~ -12.51 not real -13.

#this function simply makes a transaction from the user's account in tao
#and the parking tao fund
sub ant_pays_invoice{
    my ($class, $stash, $user_id, $invoice_id, $amount) = @_;

    my $user_account_t  = Bcd::Data::Users->get_user_account_t_id($stash, $user_id);

    my $list_debit_accounts = [$user_account_t];
    my $list_debit_amounts  = [$amount];

    my $list_credit_accounts= [Bcd::Common::BasicAccounts::PARKING_TAO_FUND->[0]];
    my $list_credit_amounts = [$amount];

    my $description = sprintf
	(Bcd::Constants::it::TransactionsTemplates::USER_PAYS_INVOICE, $invoice_id);

    my ($debits, $credits) = 
	Bcd::Data::Accounting::Transaction->add_new_transaction_in_db
	($list_debit_accounts, $list_debit_amounts,
	 $list_credit_accounts, $list_credit_amounts, 
	 $description, $stash);

    my $new_total_tao  = $debits->[0];

    return $new_total_tao;
}

#this is the opposite function: I can simply collect the money from the fund
sub ant_collects_invoice{
    my ($class, $stash, $seller, $invoice_id, $amount) = @_;

    my $user_account_t  = Bcd::Data::Users->get_user_account_t_id($stash, $seller);

    my $list_debit_accounts = [Bcd::Common::BasicAccounts::PARKING_TAO_FUND->[0]];
    my $list_debit_amounts  = [$amount];

    my $list_credit_accounts= [$user_account_t];
    my $list_credit_amounts = [$amount];

    my $description = sprintf
	(Bcd::Constants::it::TransactionsTemplates::USER_PAYS_INVOICE, $invoice_id);

    my ($debits, $credits) = 
	Bcd::Data::Accounting::Transaction->add_new_transaction_in_db
	($list_debit_accounts, $list_debit_amounts,
	 $list_credit_accounts, $list_credit_amounts, 
	 $description, $stash);

    my $new_total_tao  = $credits->[0];

    return $new_total_tao;
  
}

sub ant_emitting_cheque_fail{
    my ($class, $stash, $user_id, $amount) = @_;

    # I must have two checks, on the credit score and for the account...

    my $credit_score = Bcd::Data::CreditsAndDebits->
	get_ant_credit_score_if_it_emits_this_cheque
	($stash, $user_id, $amount);

    #print "BANK: the credit score would be $credit_score\n";

    if ($credit_score < MINIMUM_CREDIT_SCORE){
	return 2; #nope
    }

    #ok now I check the price for this cheque...
    my $price = $amount * CHEQUE_TAX;

    #print "BANK: the price would be $price\n";

    if (! $class->can_this_user_withdraw_tao($stash, $user_id, $price)){
	#print "BANK: YOU CANNOT withdraw $price\n";
	return 1; #nope
    } else {
	return 0; #ok, this ant can emit the cheque
    }
}

sub get_price_for_this_cheque{
    my ($class, $amount) = @_;
    return $amount * CHEQUE_TAX;
}

sub get_ant_maximum_cheque_considering_score{
    my ($class, $stash, $user_id) = @_;

    my $maximum_credit_score_cheque = 
      Bcd::Data::CreditsAndDebits->get_maximum_debt_to_reach_this_rate
      ($stash, $user_id, R_MAX);

    return $maximum_credit_score_cheque;
}

sub get_ant_maximum_cheque_amount{
    my ($class, $stash, $user_id) = @_;

    #ok, first of all I should get the maximum cheque
    #because of the credit score


    #well, now I should compute the maximum cheque that this ant can issue...
    my $maximum_credit_score_cheque = 
      Bcd::Data::CreditsAndDebits->get_maximum_debt_to_reach_this_rate
      ($stash, $user_id, R_MAX);

    #ok, then I compute the maximum cheque that this ant can buy
    my $user_account_t  = Bcd::Data::Users->get_user_account_t_id($stash, $user_id);

    my $total_t = Bcd::Data::Accounting::Accounts->get_balance($stash, $user_account_t);

    #this just to be sure that the account does not go really to zero.
    $total_t *= 0.99;

    my $maximum_cheque_buyable = $total_t / CHEQUE_TAX;

    return ($maximum_cheque_buyable > $maximum_credit_score_cheque) ?
	$maximum_credit_score_cheque : $maximum_cheque_buyable;
}

sub ant_emits_cheque{
    my ($class, $stash, $user_id, $ant_nest_code, $amount) = @_;

    my $price = $amount * CHEQUE_TAX;

    #ok, I should simply make a transaction...

    my $user_account_t  = Bcd::Data::Users->get_user_account_t_id($stash, $user_id);
    my $incomes_cheque_t = Bcd::Data::AntNests->
	get_tao_cheques_partial_income_id($stash, $ant_nest_code);

    my $list_debit_accounts = [$user_account_t];
    my $list_debit_amounts  = [$price];

    my $list_credit_accounts= [$incomes_cheque_t];
    my $list_credit_amounts = [$price];

    my $description = sprintf
	(Bcd::Constants::it::TransactionsTemplates::USER_ISSUE_CHEQUE, $amount);

    my ($debits, $credits) = 
	Bcd::Data::Accounting::Transaction->add_new_transaction_in_db
	($list_debit_accounts, $list_debit_amounts,
	 $list_credit_accounts, $list_credit_amounts, 
	 $description, $stash);

    my $new_total_tao  = $debits->[0];
    my $new_incomes    = $credits->[0];

    return ($new_total_tao, $new_incomes);
    
}


#this function should create the 2 accounts for the user: in Eur and
#in Tao
sub create_user_accounts{
    my ($self, $stash, $ant_nest, $user_id) = @_;

    #I should first of all search the ant nest accounts which holds the account

    my ($ant_nest_user_accounts_e, $ant_nest_user_accounts_t) = 
	Bcd::Data::AntNests->get_users_accounts_id($stash, $ant_nest);

    #ok, now I should ask the accounts plan to create the accounts.
    my $account_e = sprintf (Bcd::Constants::Users::USER_ACCT_PARAMETRIC, 
			     $user_id, 
			     $ant_nest, 
			     Bcd::Common::BasicAccounts::EURO);



    my $acct_e = Bcd::Data::Accounting::AccountsPlan->create_a_user_account
	($ant_nest_user_accounts_e,
	 $account_e,
	 $stash);

    my $account_t = sprintf (Bcd::Constants::Users::USER_ACCT_PARAMETRIC, 
			     $user_id, 
			     $ant_nest, 
			     Bcd::Common::BasicAccounts::TAO);
    

    my $acct_t = Bcd::Data::Accounting::AccountsPlan->create_a_user_account
	($ant_nest_user_accounts_t,
	 $account_t,
	 $stash);


    #just test numbers for now.
    return ($acct_e, $acct_t);
    
}

sub bdiv_int_rounded{
    my ($dividend, $divisor) = @_;

    #print "dividend $dividend divisor $divisor\n";

    my ($quot, $remainder) = $dividend->bdiv($divisor);

    #print ("I have quot $quot rem $remainder\n");

    if ($remainder >= ($divisor / 2)){
	#print "I increment it................\n";
	if ($dividend->is_pos()){
	    $dividend->binc();
	} else {
	    $dividend->bdec();
	}
    }
    
}

sub can_this_user_withdraw_tao{
    my ($class, $stash, $user_id, $amount_string) = @_;

    my $account_t = Bcd::Data::Users->get_user_account_t_id($stash, $user_id);
    my $balance_string = Bcd::Data::Accounting::Accounts->get_balance($stash, $account_t);

    my $amount_to_withdraw = new Math::BigFloat($amount_string);
    my $balance            = new Math::BigFloat($balance_string);

    #print "balance is $balance, amount to withdraw $amount_string\n";

    $balance->bsub($amount_to_withdraw);

    #print "balance is now $balance\n";

    if ($balance->is_pos() or $balance->is_zero()){
	return 1; #ok
    } else {
	return 0; #nope, amount too big
    }
    
}

sub can_this_user_withdraw{
    my ($class, $stash, $user_id, $amount_string) = @_;

    #let's get the balance of this account...
    my $account_euro = Bcd::Data::Users->get_user_account_e_id($stash, $user_id);

    #ok, get this balance
    my $balance_string = Bcd::Data::Accounting::Accounts->get_balance($stash, $account_euro);

    #ok, then I make the difference
    my $amount_to_withdraw = new Math::BigInt($amount_string);
    my $balance            = new Math::BigInt($balance_string);

    #print "balance is $balance, amount to withdraw $amount_string\n";

    $balance->bsub($amount_to_withdraw);

    #print "balance is now $balance\n";

    if ($balance->is_pos() or $balance->is_zero()){
	return 1; #ok
    } else {
	return 0; #nope, amount too big
    }
}

sub withdraw_user_account{
    my ($self, $stash, $user_id, $ant_nest_code, $amount_string) = @_;

    #the amount is a integer, no calculations made, here
    my $amount_withdrawn = new Math::BigInt($amount_string);

    #the withdraw is very simple: it involves only two accounts
    my $user_account_e  = Bcd::Data::Users->get_user_account_e_id($stash, $user_id);
    my $ant_nest_cash_e = Bcd::Data::AntNests->get_cash_euro_account_id($stash, $ant_nest_code);

    my $list_debit_accounts = [$user_account_e];
    my $list_debit_amounts  = [$amount_withdrawn];

    my $list_credit_accounts= [$ant_nest_cash_e];
    my $list_credit_amounts = [$amount_withdrawn];

    my $human = Bcd::Data::RealCurrency::humanize_this_string($amount_withdrawn);
    my $description = 
	sprintf(Bcd::Constants::it::TransactionsTemplates::USER_WITHDRAWAL, $human);

    my ($debits, $credits) = 
	Bcd::Data::Accounting::Transaction->add_new_transaction_in_db
	($list_debit_accounts, $list_debit_amounts,
	 $list_credit_accounts, $list_credit_amounts, 
	 $description, $stash);


    my $total = $debits->[0];
    return ($amount_withdrawn, $total);
}

=head2
    
    This function should simply deposit some euros in the user account

=cut
sub deposit_user_account{
    my ($self, $stash, $user_id, $ant_nest_code, $amount_string) = @_;

    my $amount = new Math::BigFloat($amount_string);
    $amount->precision(0); #it is an integer...

    #print ">>>>>>>>>>>>>>>>>>>>>>>>>>>  AMOUNT $amount\n";

    my $user_account_e  = Bcd::Data::Users->get_user_account_e_id($stash, $user_id);

    my $ant_nest_cash_e = Bcd::Data::AntNests->get_cash_euro_account_id($stash, $ant_nest_code);

    my $incomes_deposit_e = Bcd::Data::AntNests->
	get_euro_deposit_partial_income_id($stash, $ant_nest_code);

    my $treasurer_account_e = Bcd::Data::Users->get_treasurer_euro_account_id($stash, $ant_nest_code);

    my $treasurer_deposit = 0;
    if ($treasurer_account_e == $user_account_e){
	#the two accounts are the same: the treasurer is depositing
	$treasurer_deposit = 1;
    }

    #then I should make the transaction...

    #my $income           = $amount / DEPOSIT_TAX;
    #my $treasurer_income = $amount / TREASURER_INCOME;

    my $income           = $amount->copy();
    my $treasurer_income = $amount->copy();
    #print "income $income, t_income $treasurer_income\n";

    #bdiv_int_rounded($income, DEPOSIT_TAX);
    #bdiv_int_rounded($treasurer_income, TREASURER_INCOME);

    #my $tax = new Math::BigFloat("0.0005");

    $income          ->bmul( OLD_DEPOSIT_TAX);
    $treasurer_income->bmul( OLD_TREASURER_INCOME);

    #print "income $income, t_income $treasurer_income\n";

    $income = 1           if ($income < 1);
    $treasurer_income = 1 if ($treasurer_income < 1);

    #print "### deposit $amount income $income, t_income $treasurer_income\n";

    my $amount_deposited;
    if ($treasurer_deposit == 0 ){
	$amount_deposited = $amount - $income - $treasurer_income;
    } else {
	$amount_deposited = $amount - $income;
    }

    #ok, now I can make the transation.
    my $list_debit_accounts = [$ant_nest_cash_e];
    my $list_debit_amounts  = [$amount];

    my $list_credit_accounts; 
    my $list_credit_amounts;  

    if ($treasurer_deposit == 0){
	$list_credit_accounts = [$user_account_e, $incomes_deposit_e, $treasurer_account_e];
	$list_credit_amounts  = [$amount_deposited, $income, $treasurer_income];
    } else {
	$list_credit_accounts = [$user_account_e, $incomes_deposit_e];
	$list_credit_amounts  = [$amount_deposited, $income];
    }

    my $human = Bcd::Data::RealCurrency::humanize_this_string($amount);
    my $description = sprintf
	(Bcd::Constants::it::TransactionsTemplates::USER_DEPOSIT, $human);



    my ($debits, $credits) = 
    Bcd::Data::Accounting::Transaction->add_new_transaction_in_db($list_debit_accounts, $list_debit_amounts,
						       $list_credit_accounts, $list_credit_amounts, 
						       $description, $stash);

    #ok, now I can return the amount deposited and the new total

    #to do....
    #my $total = Bcd::Accounting::AccountsPlan->get_balance_for_this_account($stash, $user_account_e);

    my $total = $credits->[0];
    
    #at the outside I have only strings...
    return ($amount_deposited->bstr(), $total->bstr());
}

sub change_euro_in_tao{
    my ($self, $stash, $user_id, $ant_nest_code, $amount_string) = @_;

    #it should be simple...
    #there is a mixed transaction, that involves some accounts in tao and some
    #accounts in euro
    my ($us_act_e, $us_act_t) = Bcd::Data::Users->get_users_accounts($stash, $user_id);

    #then I should get the account for the parking euros...
    my $ant_nest_parking_e = Bcd::Data::AntNests->get_parking_euro_account_id
	($stash, $ant_nest_code);

#     #the account for the incomes...
#     my $part_exch_inc_e    = Bcd::Data::AntNests->get_euro_exchange_partial_income_id
# 	($stash, $ant_nest_code);

    #then the account for the tao in circulation is fixed
    my $tao_in_circulation_id = Bcd::Common::BasicAccounts::TAO_IN_CIRCULATION->[0];

    #ok, now I have all the accounts, let's make the transaction...
    #in the change euro-tao there are no taxes.

    #First of all I have the number
    my $amount = new Math::BigFloat($amount_string);

    #it is NOT an integer!!, I convert it into tao
    $amount->bmul(RATE_EURO_TAO);

    my $amount_euro = new Math::BigInt($amount_string);

    #Ok, now I can make the transaction..., it is simply a turn of some currencies around
    my $list_debit_accounts = [$us_act_e, $tao_in_circulation_id];
    my $list_debit_amounts  = [$amount_euro, $amount];

    my $list_credit_accounts= [$ant_nest_parking_e, $us_act_t];
    my $list_credit_amounts = [$amount_euro, $amount];

    my $human = Bcd::Data::RealCurrency::humanize_this_string($amount_string);
    my $description = sprintf
	(Bcd::Constants::it::TransactionsTemplates::USER_CHANGE_EURO_TAO, $human, $amount);


    my ($debits, $credits) = 
    Bcd::Data::Accounting::Transaction->add_new_transaction_in_db($list_debit_accounts, $list_debit_amounts,
						       $list_credit_accounts, $list_credit_amounts, 
						       $description, $stash);
    
    #I return the new totals
    my $total_euro = $debits->[0];
    my $total_tao  = $credits->[1];

    return ($total_euro->bstr(), $total_tao->bstr(), $amount->bstr());
}

sub change_tao_in_euro{
    my ($self, $stash, $user_id, $ant_nest_code, $amount_string) = @_;

    #ok, the mechanism is similar... Of course now I have an amount in tao to withdraw...
    my ($us_act_e, $us_act_t) = Bcd::Data::Users->get_users_accounts($stash, $user_id);

    #then I should get the account for the parking euros...
    my $ant_nest_parking_e = Bcd::Data::AntNests->get_parking_euro_account_id
	($stash, $ant_nest_code);

#     #the account for the incomes...
    my $part_exch_inc_e    = Bcd::Data::AntNests->get_euro_exchange_partial_income_id
 	($stash, $ant_nest_code);

    #then the account for the tao in circulation is fixed
    my $tao_in_circulation_id = Bcd::Common::BasicAccounts::TAO_IN_CIRCULATION->[0];

    #In this case I should make two conversions and the tax is the
    #difference between these two conversions.

    #First of all I have the number,
    #my $amount = new Math::BigFloat($amount_string);

    my $euro_without_taxes_f = new Math::BigFloat($amount_string);
    #$euro_without_taxes->precision(0);
    my $euro_with_taxes_f    = new Math::BigFloat($amount_string);
    #$euro_with_taxes->precision(0);

    $euro_with_taxes_f->bdiv(RATE_TAO_EURO);
    $euro_without_taxes_f->bdiv(RATE_EURO_TAO);

    #print "you want to convert $amount_string tao\n";
    #print "it would be equivalent to $euro_with_taxes_f with tax or $euro_without_taxes_f without tax\n";

    #the euros are integer!
    my $euro_without_taxes = $euro_without_taxes_f->as_number();
    my $euro_with_taxes    = $euro_with_taxes_f ->as_number();

    my $tax = $euro_without_taxes->copy();
    #print "BUT!!!! truncated they are $euro_with_taxes and $euro_without_taxes \n";

    my $tao_converted = new Math::BigFloat($euro_without_taxes);
    $tao_converted->bmul(RATE_EURO_TAO);
    #print "So I convert $euro_without_taxes in tao which is $tao_converted\n";

    #Ok, now I should make the difference between these two numbers
    $tax->bsub($euro_with_taxes);

    #I have at least 1 cent of tax.
    $tax = 1 if ($tax < 1);

    #print "the tax is $tax\n";

    #ok, so the real amount to convert is
    my $user_credit = $euro_without_taxes->copy();
    $user_credit->bsub($tax);

    #print "you have $euro_without_taxes I give $user_credit and tax $tax, I will debit $amount_string\n";

    #ok, I can now make the transaction
    #my $tao_amount = new Math::BigFloat($amount_string);
    my $list_debit_accounts = [$us_act_t, $ant_nest_parking_e];
    my $list_debit_amounts  = [$tao_converted, $euro_without_taxes];

    my $list_credit_accounts= [$tao_in_circulation_id, $us_act_e, $part_exch_inc_e];
    my $list_credit_amounts = [$tao_converted, $user_credit, $tax];

    my $human = Bcd::Data::RealCurrency::humanize_this_string($user_credit);
    my $description = sprintf
	(Bcd::Constants::it::TransactionsTemplates::USER_CHANGE_TAO_EURO, $amount_string, $human);


    my ($debits, $credits) = 
	Bcd::Data::Accounting::Transaction->add_new_transaction_in_db($list_debit_accounts, $list_debit_amounts,
								      $list_credit_accounts, $list_credit_amounts, 
								      $description, $stash);

    my $new_total_euro = $credits->[1];
    my $new_total_tao  = $debits->[0];

    return ($new_total_euro->bstr(), $new_total_tao->bstr(), $user_credit->bstr(), $tao_converted->bstr());
    
}



1;
