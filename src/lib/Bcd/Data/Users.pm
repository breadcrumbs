package Bcd::Data::Users;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Digest::SHA1;
use Bcd::Common::CommonConstants;
use Bcd::Constants::Users;
use Bcd::Data::Bank;
use Bcd::Data::Token;
use Data::Dumper;
use FindBin;

#this file should simply models the users in the system.

#these are the roles defined in the system...
#they are in power of twos, so I can have a set of them...
use constant{
    GUEST_ROLE      => 0,
    ANT_ROLE        => 1,
    BOSS_ROLE       => 2,
    TREASURER_ROLE  => 4,
    #This role is not stored in the db... it is the role of the founders
    FOUNDER_ROLE    => 8,
    BC_ROOT_ROLE    => 32768,

    #this is NOT stored in the db... it is given to bc-root
    ROLE_FOR_BC_ROOT             => 65535, #this number has all the bit sets...
    ROLE_FOR_A_FOUNDER_ANT       =>     9,
    ROLE_FOR_A_FOUNDER_TREASURER =>    13,
    ROLE_FOR_A_FOUNDER_BOSS      =>    11,
};

use constant ROLES_DB => 
(
 [GUEST_ROLE,     'guest'],
 [ANT_ROLE,   	  'ant'  ],
 [BOSS_ROLE,  	  'boss' ],
 [TREASURER_ROLE, 'treasurer'],
 [FOUNDER_ROLE,   'founder'],
 [BC_ROOT_ROLE,   'bc-root'],
 );

use constant{
    SELECT_ANT_NEST_FROM_USER          => "User_select_ant_nest_from_user",

    CREATE_USER                        =>
	qq{insert into users (id_ant_nest, nick, totem, password, users_state,}.
	qq{role_mask,ro, theta, id_personal_data, }.
	qq{id_account_real, id_account_local) values (?, ?, ?, ?,}.Bcd::Constants::Users::CREATION_STATE.
	", cast(".ANT_ROLE.qq{as bit(16)), ?, ?, ?, ?, ?)},

    UPDATE_ROLE_USER_ID                =>
    qq{update users set role_mask = role_mask | cast(cast(? as integer) as bit(16)) where id = ?},

    UPDATE_PASSWORD_FOR_USER           =>
    qq{UPDATE users SET password = ? WHERE id = ?},

    UPDATE_TOTEM_FOR_USER              =>
    qq{UPDATE users SET totem = ? WHERE id = ?},

    SELECT_USER_DATA                   => 
    qq{select * from users where id=?},

    SELECT_USER_WITH_NICK_AND_ANT_NEST => "Users_select_with_nick_and_an",
    UPDATE_ROLE_WITH_NICK_AND_ANT_NEST => "Users_update_role_with_nick",
    UPDATE_FIRST_TUTOR                 => "Users_update_first_tutor",
    UPDATE_SECOND_TUTOR                => "Users_update_second_tutor",
    CREATE_NEW_USER_BOOKING            => "Users_create_new_user_booking",
    SELECT_ALL_USERS_FROM_ANT_NEST     => "Users_select_all_users",
    UPDATE_USERS_STATE                 => "Users_update_users_state",
    CONFIRM_2ND_TUTOR                  => "Users_confirm_2nd_tutor",
    SELECT_MY_BOOKING                  => "Users_select_my_booking",
    DELETE_MY_BOOKING                  => "Users_delete_my_booking",
    GET_NEW_USER_FROM_TOKEN            => "Users_get_new_user_from_token",
    SELECT_COUNT_USERS_FROM_ANT_NEST   => "Users_select_count_users_from_ant_nest",
    SELECT_USER_WITH_ANT_NEST_AND_ROLE => "Users_select_user_with_ant_nest_and_role",

    SELECT_NICK_FROM_ID                =>
    qq{SELECT nick from users where id = ? },

    SELECT_USER_NAME_ANT_NEST_AND_ROLE =>
    qq{SELECT u.id, u.nick, p.first_name, p.last_name from users as u, personal_users_data as p }.
    qq{ where u.id_ant_nest = ? and }.
    qq{( (u.role_mask & ?::integer::bit(16)) != 0::bit(16)) }.
    qq{ AND u.id_personal_data = p.id},

    SELECT_COUNT_USERS_RANGE           =>
	qq{SELECT count(*) from users where id_ant_nest >= ? AND id_ant_nest <= ?},

    UPDATE_ALL_USERS_IN_NEST_STATE     =>
    qq{UPDATE users SET users_state = ? where id_ant_nest = ?},


    INSERT_PERSONAL_DATA               =>
    qq{INSERT into personal_users_data(	first_name, last_name, address,} .
    qq{ home_phone , mobile_phone , sex , birthdate , email , identity_card_id ) }.
    qq{ values (?, ?, ?, ?, ?, ?, ?, ?, ?)},

    UPDATE_PERSONAL_DATA               =>
    qq{UPDATE personal_users_data set first_name = ? , last_name = ? , address = ?, }.
    qq{ home_phone = ?, mobile_phone = ?, sex = ?, birthdate = ? , email = ?,}.
    qq{ identity_card_id = ? where id = ?},

    SELECT_PERSONAL_DATA               =>
    qq{SELECT * from personal_users_data WHERE id=?},

    SELECT_PERSONAL_DATA_OF_USER       =>
    qq{SELECT p.* from personal_users_data as p, users as u WHERE u.id=? and p.id = u.id_personal_data},

    SELECT_PERSONAL_DATA_ID            =>
    qq{SELECT id_personal_data FROM users WHERE id = ?},

    GET_ALL_CHILDREN_FIRST_TUTOR       =>
    qq{SELECT id, nick, tutor_first FROM users WHERE tutor_second = ?},

    GET_ALL_CHILDREN_SECOND_TUTOR      =>
    qq{SELECT id, nick, tutor_second FROM users WHERE tutor_first = ?},
};



#These are used to have a default password for the user
use constant{
    PASSWORD_SUFFIX => "p",
    TOTEM_SUFFIX    => "t",
};

sub get_all_children_ds{
    my ($class, $stash, $user) = @_;

    #first of all I get the array...
    my $arr = $class->get_all_children_arr($stash, $user);
    
    #ok, then I prepare the dataset
    my @ds;
    my @fields = ('id', 'nick', 'id_other_tutor', 'nick_other_tutor');
    push(@ds, \@fields);

    foreach(@{$arr}){
	my $nick = $class->get_nick_from_id($stash, $_->[2]);
	push(@{$_}, $nick);
	push(@ds, $_);
    }

    return \@ds;
}

sub get_all_children_arr{
    my ($class, $stash, $user) = @_;

    #I have to make two queries...
    my $first_res = $stash->select_all_arr(GET_ALL_CHILDREN_FIRST_TUTOR, $user);
    my $second_res = $stash->select_all_arr(GET_ALL_CHILDREN_SECOND_TUTOR, $user);

    #now I should make the union of these arrays
    push(@{$first_res}, @{$second_res});
    return $first_res;
}

sub get_nick_from_id {
    my ($class, $stash, $user) = @_;

    my $st = $stash->prepare_cached(SELECT_NICK_FROM_ID);
    $st->bind_param(1, $user);
    $st->execute();

    my $nick;
    $st->bind_columns(undef, \$nick);
    my $row = $st->fetch();
    $st->finish();

    return $nick;
    
}

sub get_user_personal_data_arr {
    my ($class, $stash, $user) = @_;

    my $st = $stash->prepare_cached(SELECT_PERSONAL_DATA_OF_USER);
    $st->bind_param(1, $user);
    $st->execute();
    my $row = $st->fetchrow_arrayref();
    $st->finish();

    return $row;
}

sub get_user_personal_data_hash {
    my ($class, $stash, $user) = @_;

    my $st = $stash->prepare_cached(SELECT_PERSONAL_DATA_OF_USER);
    $st->bind_param(1, $user);
    $st->execute();
    my $hash = $st->fetchrow_hashref();
    $st->finish();

    return $hash;
}

sub update_state_of_all_users_in_ant_nest{
    my ($class, $stash, $id_ant_nest, $state) = @_;

    my $st = $stash->prepare_cached(UPDATE_ALL_USERS_IN_NEST_STATE);
    $st->bind_param(1, $state);
    $st->bind_param(2, $id_ant_nest);
    $st->execute();
}

sub select_personal_data_hash{
    my ($self, $stash, $id_data) = @_;

    my $st = $stash->prepare_cached(SELECT_PERSONAL_DATA);
    $st->bind_param(1, $id_data);
    $st->execute();

    my $hash = $st->fetchrow_hashref();
    $st->finish();

    return $hash;
}

sub get_count_users_in_ant_nests_range{
    my ($self, $stash, $min, $max) = @_;

    my $st = $stash->prepare_cached(SELECT_COUNT_USERS_RANGE);

    $st->bind_param(1, $min);
    $st->bind_param(2, $max);

    $st->execute();

    my $row = $st->fetch();
    $st->finish();

    return $row->[0];
}

sub get_boss_of_this_ant_nest{
    my ($self, $stash, $code) = @_;

    $self->_return_first_ant_with_role($stash, $code, BOSS_ROLE);


}

sub get_boss_nick_name_arr{
    my ($self, $stash, $code) = @_;

    my $st = $stash->prepare_cached(SELECT_USER_NAME_ANT_NEST_AND_ROLE);
    $st->bind_param(1, $code);
    $st->bind_param(2, BOSS_ROLE);
    $st->execute();
    
    my $row = $st->fetchrow_arrayref();
    $st->finish();
    return $row;
}

sub get_tresaurer_of_this_ant_nest{
    my ($self, $stash, $code) = @_;

    $self->_return_first_ant_with_role($stash, $code, TREASURER_ROLE);
}

sub get_treasurer_euro_account_id{
    my ($self, $stash, $code) = @_;

    my $treasurer = $self->get_tresaurer_of_this_ant_nest($stash, $code);
    return $treasurer->{id_account_real};
}

sub get_users_accounts{
    my ($self, $stash, $user_id) = @_;

    my $user = $self->get_user_base_data_arr($stash, $user_id);
    my $account_e = $user->[15];
    my $account_t = $user->[16];

    return ($account_e, $account_t);
}

sub get_user_account_t_id{
    my ($self, $stash, $user_id) = @_;
    my $user = $self->get_user_base_data_arr($stash, $user_id);
    my $account_t = $user->[16];
    return $account_t;
}

sub get_user_personal_data_id{
    my ($self, $stash, $user_id) = @_;
    my $user = $self->get_user_base_data_arr($stash, $user_id);
    my $personal_data_id = $user->[14];
    return $personal_data_id;
}

sub get_user_account_e_id{
    my ($self, $stash, $user_id) = @_;

    #the account is in my record
    my $user = $self->select_user_data($stash, $user_id);
    my $account_e = $user->{id_account_real};

    return $account_e;
}

#this function will return *only* the first ant with a role
sub _return_first_ant_with_role{

    my ($self, $stash, $code, $role) = @_;

    my $st = $stash->get_statement(SELECT_USER_WITH_ANT_NEST_AND_ROLE, $self);
    $st->bind_param(1, $code);
    $st->bind_param(2, $role);

    $st->execute();
    my $res = $st->fetchrow_hashref();
    $st->finish();

    return $res;
}

sub get_ant_count_from_ant_nest{
   my ($self, $stash, $code) = @_;

   my $st = $stash->get_statement(SELECT_COUNT_USERS_FROM_ANT_NEST, $self);
   $st->bind_param(1, $code);
   $st->execute();

   my $arr = $st->fetchrow_arrayref();
   $st->finish();
   return $arr->[0];
}

sub get_ant_nest_for_this_user{
    my ($self, $user, $stash) = @_;

    my $st= $stash->get_statement(SELECT_ANT_NEST_FROM_USER, $self);
    $st->bind_param(1, $user);
    $st->execute();
    my $ant_nest_code;
    $st->bind_columns(undef, \$ant_nest_code);
    $st->fetch();
    $st->finish();

    return $ant_nest_code;
}

sub select_basic_user_data_from_nick_nest_arr{
    my ($self, $stash, $ant_nest_code, $user) = @_;

    my $st = $stash->get_statement(SELECT_USER_WITH_NICK_AND_ANT_NEST, $self);
    $st->bind_param(1, $ant_nest_code);
    $st->bind_param(2, $user);

    $st->execute();
    my $arr = $st->fetchrow_arrayref();
    $st->finish();

    return $arr;
    
}

sub select_user_data_from_nick_ant_nest{
    my ($self, $stash, $ant_nest_code, $user) = @_;

    #ok, let's take the user
    my $st = $stash->get_statement(SELECT_USER_WITH_NICK_AND_ANT_NEST, $self);
    $st->bind_param(1, $ant_nest_code);
    $st->bind_param(2, $user);

    $st->execute();
    my $hashref = $st->fetchrow_hashref();
    $st->finish();
    
    if (!defined($hashref)){
	return undef;
    }

    my $user_id_save = $hashref->{id};

    my %hash;
    %hash = %{$hashref};

    #print Dumper(\%hash);
    my $data = $self->select_personal_data_hash($stash, $hashref->{id_personal_data});

    #%hash = %{$data};
    @hash{keys %{$data}} = values %{$data};


    $hash{id} = $user_id_save;

    return \%hash;

}

sub exists_this_nick_already{
   my ($self, $stash, $ant_nest_code, $user) = @_;

   my $hash = $self->select_user_data_from_nick_ant_nest($stash, $ant_nest_code, $user);
   
   if (defined($hash)){
       return 1;
   } else {
       return 0;
   }

}

sub grant_role_to_user{
    my ($self, $stash, $ant_nest_code, $user, $role) = @_;

    #ok, let's take the user
    my $st = $stash->get_statement(UPDATE_ROLE_WITH_NICK_AND_ANT_NEST, $self);
    $st->bind_param(1, $role);
    $st->bind_param(2, $ant_nest_code);
    $st->bind_param(3, $user);
    return $st->execute();
}

sub grant_role_to_user_id{
    my ($self, $stash, $user_id, $role) = @_;

    my $st = $stash->prepare_cached(UPDATE_ROLE_USER_ID);
    $st->bind_param(1, $role);
    $st->bind_param(2, $user_id);
    return $st->execute();
    
}

sub update_users_state{
    my ($class, $stash, $id_user, $state) = @_;
    my $st = $stash->get_statement(UPDATE_USERS_STATE, $class);
    
    $st->bind_param(1, $state);
    $st->bind_param(2, $id_user);

    return $st->execute();
}

sub update_first_tutor{
    my ($class, $stash, $id_user, $id_tutor) = @_;
    my $st = $stash->get_statement(UPDATE_FIRST_TUTOR, $class);
    
    $st->bind_param(1, $id_tutor);
    $st->bind_param(2, $id_user);

    return $st->execute();
}

sub update_second_tutor{
    my ($class, $stash, $id_user, $id_tutor) = @_;
    my $st = $stash->get_statement(UPDATE_SECOND_TUTOR, $class);
    
    $st->bind_param(1, $id_tutor);
    $st->bind_param(2, $id_user);

    return $st->execute();
}

sub confirm_second_tutor{
    my ($class, $stash, $id_new_user, $id_tutor, $tutor_trust) = @_;

    #I should simply update the 2nd tutor
    my $ans = $class->update_second_tutor($stash, $id_new_user, $id_tutor);
    return $ans if $ans != 1;
    
    #then I should update the row in the new_users_bookings table
    $ans = $class->update_users_state($stash, $id_new_user, 
				      Bcd::Constants::Users::NEW_ANT_BEFORE_TRUST);
    return $ans if $ans != 1;


    my $st = $stash->get_statement(CONFIRM_2ND_TUTOR, $class);
    $st->bind_param(1, $tutor_trust);
    $st->bind_param(2, $id_new_user);
    return $st->execute();

}

sub get_new_user_id_from_this_token{
    my ($class, $stash, $token) = @_;

    #ok, let's try to select this user
    my $st = $stash->get_statement(GET_NEW_USER_FROM_TOKEN, $class);
    $st->bind_param(1, $token);
    $st->execute();
    
    my $arr = $st->fetch();
    $st->finish();

    return defined($arr) ? $arr->[0] : undef;
}

sub get_tutors_details{
    my ($class, $stash, $my_id) = @_;

    my $myself = $class->select_user_data($stash, $my_id);

    my $tut1 = $class->select_user_data($stash, $myself->{tutor_first});

    my $tut2;

    if (defined($myself->{tutor_second})){
	$tut2 = $class->select_user_data($stash, $myself->{tutor_second});
    }

    return ($tut1, $tut2);
}

#this method is called by a new ant to confirm his/her tutors
sub confirm_my_tutors{
    my ($class, $stash, $my_id, $trust_t1, $trust_t2) = @_;

    #I take my booking
    my $st = $stash->get_statement(SELECT_MY_BOOKING, $class);
    $st->bind_param(1, $my_id);
    $st->execute();
    my $booking = $st->fetch();
    #now I have the trust from the tutors to myself
    my $trust_t1_to_me = $booking->[1];
    my $trust_t2_to_me = $booking->[2];
    $st->finish();    

    #ok, now I should get my tutors
    my $myself = $class->select_user_data($stash, $my_id);

    #now I should be able to create the trusts
    Bcd::Data::Trust->create_trust_between
	(
	 $stash,
	 $myself->{tutor_first},
	 $my_id,
	 $trust_t1_to_me,
	 $trust_t1
	 );

    Bcd::Data::Trust->create_trust_between
	(
	 $stash,
	 $myself->{tutor_second},
	 $my_id,
	 $trust_t2_to_me,
	 $trust_t2
	 );

    #then I should update the state
    $class->update_users_state($stash, $my_id, Bcd::Constants::Users::NORMAL_ACTIVE_ANT);

    #and I should delete the booking
    $st = $stash->get_statement(DELETE_MY_BOOKING, $class);
    $st->bind_param(1, $my_id);
    $st->execute();


}

sub login{
    my ($self, $user, $ant_nest_code, $password, $stash) = @_;

    my $password_db;
    my $role;
    my $id;

    if ( $user eq Bcd::Common::CommonConstants::BC_ROOT){
	$role = ROLE_FOR_BC_ROOT;
	$id = $user;
	$password_db = $stash->get_bc_root_password();
    } else {
	#this is a normal login... so the password and role are in the db
	my $st = $stash->get_statement(SELECT_USER_WITH_NICK_AND_ANT_NEST, $self);
	$st->bind_param(1, $ant_nest_code);
	$st->bind_param(2, $user);
	$st->execute();
	my $data = $st->fetchrow_hashref();

	return 2 if !defined($data);

	$password_db = $data->{password};
	$role        = oct("0b" . $data->{role_mask}); #the data in the database is a bit string
	$id          = $data->{id};
	$st->finish();
    }



    #ok, now let's make the digest of the password
    my $sha = Digest::SHA1->new;

    $sha->add($password);
    my $digest = $sha->hexdigest;


    #ok, let's compare the passwords
    if ($password_db eq $digest){
	return (0, $role, $id);
    } else {
	return (1, undef);
    }
}

sub change_totem_for_user{
    my ($self, $stash, $id_user, $new_totem) = @_;
    $stash->prep_exec(UPDATE_TOTEM_FOR_USER, $new_totem, $id_user);
}

sub change_password_for_user{
    my ($self, $stash, $id_user, $new_password) = @_;
    
    my $sha = Digest::SHA1->new;
    $sha->add($new_password);
    my $digest = $sha->hexdigest;
    
    $stash->prep_exec(UPDATE_PASSWORD_FOR_USER, $digest, $id_user);
}

sub is_valid_password_for_user{
    my ($self, $stash, $id_user, $password) = @_;

    my $user = $self->get_user_base_data_arr($stash, $id_user);
    my $password_db = $user->[4];

    my $sha = Digest::SHA1->new;
    $sha->add($password);
    my $digest = $sha->hexdigest;

    #ok, let's compare the passwords
    if ($password_db eq $digest){
	return 1;
    } else {
	return 0;
    }
}

sub get_user_base_data_arr{
   my ($self, $stash, $id) = @_;

   my $st = $stash->prepare_cached(SELECT_USER_DATA);
   $st->bind_param(1, $id);
   $st->execute();
   my $arr = $st->fetchrow_arrayref();
   $st->finish();

   return $arr;
}

sub get_user_base_data_hash{
    my ($self, $stash, $id) = @_;
    return $stash->select_one_row_hash(SELECT_USER_DATA, $id);
}

sub select_user_data{
    my ($self, $stash, $id) = @_;

    my $st = $stash->prepare_cached(SELECT_USER_DATA);
    $st->bind_param(1, $id);
    $st->execute();
    my $hash = $st->fetchrow_hashref();
    $st->finish();

    return undef if(!defined $hash);

    #this should be saved, because the other table has a same column
    my $user_id_save = $hash->{id};

    my %res;
    %res = %{$hash};

    #ok, also in this case I should get the personal data
    my $data = $self->select_personal_data_hash($stash, $hash->{id_personal_data});

    #%hash = %{$data};
    @res{keys %{$data}} = values %{$data};

    $res{id} = $user_id_save;

    return \%res;
}

sub get_all_ants_in_this_ant_nest{
    my ($class, $stash, $ant_nest_code) = @_;
    
    my $st = $stash->get_statement(SELECT_ALL_USERS_FROM_ANT_NEST, $class);
    $st->bind_param(1, $ant_nest_code);
    $st->execute();
    my $arr = $st->fetchall_arrayref();
    return $arr;
}

sub create_normal_user{
    my ($self, $stash, $cmd, $ro, $theta, $tutor_id, $tutor_trust) = @_;

    #I simply call the create_user normally
    $self->create_user($stash, $cmd, $ro, $theta);

    #then I create a row in the new_users_bookings table.
    #first of all I should get a new token
    my $token = Bcd::Data::Token::generate_new_token();
    my $st = $stash->get_statement(CREATE_NEW_USER_BOOKING, $self);

    #I should get the id of the last inserted user...
    my $user_id = $stash->get_id_of_last_user();

    $st->bind_param(1, $user_id);
    $st->bind_param(2, $tutor_trust);
    $st->bind_param(3, $token);

    $st->execute();

    #then I should update the tutor for this user
    $self->update_first_tutor($stash, $user_id, $tutor_id);

    #I should also update its state
    $self->update_users_state($stash, $user_id, Bcd::Constants::Users::NEW_ANT_WITH_ONE_TUTOR);

    return $token;
}

sub update_personal_data_from_user_id{
    my ($self, $stash, $user_id, $cmd) = @_;

    #first of all I get the id of the personal data...
    my $st = $stash->prepare_cached(SELECT_PERSONAL_DATA_ID);
    $st->bind_param(1, $user_id);
    $st->execute();
    my $id_personal_data = $st->fetch()->[0];
    $st->finish();

    #ok, then I can update the personal data
    $self->update_personal_data($stash, $id_personal_data, $cmd);
}

sub update_personal_data{
    my ($self, $stash, $id, $cmd) = @_;

    my $st = $stash->prepare_cached(UPDATE_PERSONAL_DATA);

    $st->bind_param(1 , $cmd->{first_name});
    $st->bind_param(2 , $cmd->{last_name});
    $st->bind_param(3 , $cmd->{address});
    $st->bind_param(4 , $cmd->{home_phone});
    $st->bind_param(5 , $cmd->{mobile_phone});
    $st->bind_param(6 , $cmd->{sex});
    $st->bind_param(7 , $cmd->{birthdate});
    $st->bind_param(8 , $cmd->{email});
    $st->bind_param(9 , $cmd->{identity_card_id});
    $st->bind_param(10, $id);

    $st->execute();
}

sub create_personal_data_for_user{
    my ($self, $stash, $cmd) = @_;

    my $st = $stash->prepare_cached(INSERT_PERSONAL_DATA);

    $st->bind_param(1 , $cmd->{first_name});
    $st->bind_param(2 , $cmd->{last_name});
    $st->bind_param(3 , $cmd->{address});
    $st->bind_param(4 , $cmd->{home_phone});
    $st->bind_param(5 , $cmd->{mobile_phone});
    $st->bind_param(6 , $cmd->{sex});
    $st->bind_param(7 , $cmd->{birthdate});
    $st->bind_param(8 , $cmd->{email});
    $st->bind_param(9 , $cmd->{identity_card_id});

    $st->execute();
    
    return $stash->get_last_personal_data_id();
}

sub create_user_from_booking{
    my ($class, $stash, $id_ant_nest, $nick, $password, $id_personal_data) = @_;

    my ($account_e_id, $account_t_id) =
	Bcd::Data::Bank->create_user_accounts($stash, 
					      $id_ant_nest,
					      $nick);

    my $st = $stash->prepare_cached(CREATE_USER);

    #I should get the hash of the password.
    my $sha = Digest::SHA1->new;
    $sha->add($password);
    my $digest = $sha->hexdigest;
    
    #ok, now I should bind some parameters...
    $st->bind_param(1,  $id_ant_nest);
    $st->bind_param(2,  $nick);             #nick
    $st->bind_param(3,  $password);  #the totem is equal to the password, initially
    $st->bind_param(4,  $digest);   #password
    $st->bind_param(5,  undef);
    $st->bind_param(6,  undef);

    $st->bind_param(7,  $id_personal_data);

    $st->bind_param(8, $account_e_id);  #the accounts should be createed.
    $st->bind_param(9, $account_t_id);

    $st->execute();

    return $stash->get_id_of_last_user();
}

#this method should simply create the user.
#I simply pass the command, which stores the parameters.
sub create_user{
    my ($self, $stash, $cmd, $ro, $theta) = @_;

    #  insert into users (id_ant_nest, nick, totem, password,
    #  users_state,role_mask,ro, theta, first_name, last_name,
    #  address,home_phone, mobile_phone, sex, birth_date, email,
    #  identity_card_id,id_account_real, id_account_local) values
    #  (?, ?, ?, ?,1, 1, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ? ?, ?)

    my ($account_e_id, $account_t_id) =
	Bcd::Data::Bank->create_user_accounts($stash, 
					      $cmd->{ant_nest},
					      $cmd->{nick});

    #ok, now first of all I should create the row for the personal
    #data of this user...
    my $personal_data_id = $self->create_personal_data_for_user
	($stash, $cmd);
    
    my $st = $stash->prepare_cached(CREATE_USER);

    #I should get the hash of the password.
    my $password = $cmd->{nick} . PASSWORD_SUFFIX;
    my $sha = Digest::SHA1->new;
    $sha->add($password);
    my $digest = $sha->hexdigest;

    
    #ok, now I should bind some parameters...
    $st->bind_param(1,  $cmd->{ant_nest});
    $st->bind_param(2,  $cmd->{nick});             #nick
    $st->bind_param(3,  $cmd->{nick} . TOTEM_SUFFIX);  #totem
    $st->bind_param(4,  $digest);   #password
    $st->bind_param(5,  $ro);
    $st->bind_param(6,  $theta);

    $st->bind_param(7,  $personal_data_id);

    $st->bind_param(8, $account_e_id);  #the accounts should be createed.
    $st->bind_param(9, $account_t_id);

    $st->execute();
        
    return $stash->get_id_of_last_user();
}

#this function is one shot... so it does not put the commands in the
#stash
sub init_db{
    my ($self, $stash) = @_;
    my $conn = $stash->get_connection();

    #ok, now I should insert the values in the table...
    my $sth = $conn->prepare(qq{insert into user_states values(?,?)});

    foreach (Bcd::Constants::Users::LIST_USER_STATES){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }


    $sth = $conn->prepare(qq{insert into roles values(?,?)});
    foreach(ROLES_DB){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }

}

sub populate_the_stash{

    my ($self, $db_stash) = @_;

    my $sql;
    my $sth;

    $sql = qq{select id_ant_nest from users where id=?};
    $sth = $db_stash->get_connection()->prepare( $sql );
    $db_stash->insert_statement(SELECT_ANT_NEST_FROM_USER, $sth);

#    $sql = qq{select * from users where id=?};
 #   $db_stash->record_this_statement(SELECT_USER_DATA, $sql);

    $sql = qq{select * from users where id_ant_nest=? and nick=?};
    $db_stash->record_this_statement(SELECT_USER_WITH_NICK_AND_ANT_NEST, $sql);

    #this to set, to unset use the & ~ (role)  syntax
    $sql = qq{update users set role_mask = role_mask | cast(cast(? as integer) as bit(16)) 
		  where id_ant_nest=? and nick=?};
    $db_stash->record_this_statement(UPDATE_ROLE_WITH_NICK_AND_ANT_NEST, $sql);

    $sql = qq{update users set tutor_first = ? where id = ?};
    $db_stash->record_this_statement(UPDATE_FIRST_TUTOR, $sql);

    $sql = qq{update users set tutor_second = ? where id = ?};
    $db_stash->record_this_statement(UPDATE_SECOND_TUTOR, $sql);

    $sql = qq{insert into new_users_bookings values(?, ?, 0, ?)};
    $db_stash->record_this_statement(CREATE_NEW_USER_BOOKING, $sql);

    $sql = qq{select id from users where id_ant_nest=?};
    $db_stash->record_this_statement(SELECT_ALL_USERS_FROM_ANT_NEST, $sql);

    $sql = qq{update users set users_state=? where id = ?};
    $db_stash->record_this_statement(UPDATE_USERS_STATE, $sql);

    $sql = qq{update new_users_bookings set tutor_2_trust = ? where new_user = ?};
    $db_stash->record_this_statement(CONFIRM_2ND_TUTOR, $sql);

    $sql = qq{SELECT * from new_users_bookings where new_user = ?};
    $db_stash->record_this_statement(SELECT_MY_BOOKING, $sql);

    $sql = qq{DELETE from new_users_bookings where new_user = ?};
    $db_stash->record_this_statement(DELETE_MY_BOOKING, $sql);

    $sql = qq{SELECT new_user from new_users_bookings where token = ?};
    $db_stash->record_this_statement(GET_NEW_USER_FROM_TOKEN, $sql);

    $sql = qq{SELECT count(*) from users where id_ant_nest = ?};
    $db_stash->record_this_statement(SELECT_COUNT_USERS_FROM_ANT_NEST, $sql);

    $sql = qq{SELECT * from users where id_ant_nest = ? and }.
     qq{( (role_mask & ?::integer::bit(16)) != 0::bit(16)) };
    $db_stash->record_this_statement(SELECT_USER_WITH_ANT_NEST_AND_ROLE, $sql);
}

1;
