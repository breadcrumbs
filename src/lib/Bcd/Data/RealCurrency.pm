package Bcd::Data::RealCurrency;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU. General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Data::Dumper;


#I have only a function to dehumanize and to "humanize" a string which
#hold a currency... The currency calculations are done with Big*
#classes, but in here we have only strings

sub dehumanize_this_string{
    my $currency = shift; #something like 0.03, 323.92 etc

    #first of all let's see if there is a dot
    my $dehumanized;
    my @dots;

    #Only digits and at most a dot.
    if ($currency =~ /[^\d\.]/){
	return 0;
    }

    if (@dots = ($currency =~ /(\.)/g)){

	if ($#dots > 0){
	    #more than one dot...
	    return 0; #not a number
	}

	#ok, I have a dot, I simply drop it
	my $old_lenght = length($currency);
	$currency =~ s/\.//g;

	#print "currency $currency you have a dot at position $-[0]\n";

	#let's see in which position it goes
	if ( $-[0] == ($old_lenght - 3)){
	    #normal case... two precision digits, I simply drop the dot

	} elsif ( $-[0] == ($old_lenght -2)){
	    #I have only one digit of precision
	    #I drop the dot, and then I add a zero
	    $currency .= "0";
	} else {
	    #more digits... or the point is the last one... not implemented
	    return (0, undef);
	}

	

    } else {
	#no dot... so you should simply append a double zero to it
	$currency .=  "00";
    }

    return (1, $currency);
}

#this is the inverse... I have from the db a string like xxcc and I
#should convert it into a "currency" format like xx.cc (where cc are
#the cents) (in practice I make a div 100 by hand)

sub humanize_this_string{

    my $dehumanized = shift;

    my $currency;

    if (length($dehumanized) >= 3){
	#very simple case..., I simply insert a dot in the last - 3rd position
	
	$currency = substr ($dehumanized, 0, -2);
	$currency .= ".";
	$currency .= substr($dehumanized, -2);
	
    } else {
	#I append some zeroes to the string, just to make it of length 3.
	my $zeroes = 2 - length($dehumanized);

	for (0..$zeroes){
	    $dehumanized = "0" . $dehumanized;
	}



	#recursive call
	return humanize_this_string($dehumanized);
    }

    return $currency;
}

1;
