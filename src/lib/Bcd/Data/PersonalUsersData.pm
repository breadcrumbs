package Bcd::Data::PersonalUsersData;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Data::Dumper;

use constant {
    SELECT_PERSONAL_USERS_DATA_TRUSTS              =>
	qq{SELECT t_first_name, t_last_name, t_address, t_home_phone, t_mobile_phone, t_sex, t_birthdate,}.
	qq{t_email FROM  personal_users_data_trusts WHERE id = ?},

	INSERT_PERSONAL_USERS_DATA_TRUSTS          =>
	qq{INSERT INTO personal_users_data_trusts(id, t_first_name, t_last_name, t_address, t_home_phone, }.
	       qq{t_mobile_phone, t_sex, t_birthdate, t_email) VALUES (?,?,?,?,?,?,?,?,?)},

	UPDATE_PERSONAL_USERS_DATA_TRUST           =>
	qq{UPDATE personal_users_data_trusts SET %s = ? WHERE id = ? },
};

sub get_personal_users_data_trusts_arr {
    my ($class, $stash, $id) = @_;
    return $stash->select_one_row_arr(SELECT_PERSONAL_USERS_DATA_TRUSTS, $id);
};

sub get_personal_users_data_trusts_hash {
    my ($class, $stash, $id) = @_;
    return $stash->select_one_row_hash(SELECT_PERSONAL_USERS_DATA_TRUSTS, $id);
};

sub insert_personal_users_data_trusts {
    my ($class, $stash, @args) = @_;
    
    return $stash->prep_exec(INSERT_PERSONAL_USERS_DATA_TRUSTS, @args);
};

sub change_personal_users_data_trust{
    my ($class, $stash, $trust_item, $id, $new_trust) = @_;
    
    #I should make a command with the field...
    my $st_sql = sprintf(UPDATE_PERSONAL_USERS_DATA_TRUST, $trust_item);

    return $stash->prep_exec($st_sql, $new_trust, $id);
}

1;
