package Bcd::Data::Sites;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


#this file models the set of ant nests in the system
use strict;
use warnings;

use Data::Dumper;

use constant{
    
    CREATE_SITE                         =>
	qq{INSERT into bc_sites(name, location, directions, time_restricted, }.
	       qq{ opening_hours) values (?, ?, ?, ?, ?)},

	CREATE_PUBLIC_ANT_NEST_SITE     =>
	qq{INSERT into ant_nests_public_sites VALUES (?,?)},

	SELECT_PUBLIC_SITES_OF_ANT_NEST =>
	qq{SELECT id_site from ant_nests_public_sites WHERE id_ant_nest = ?},

	SELECT_ALL_SITES_OF_ANT_NEST    =>
	qq{SELECT b.id, b.name FROM ant_nests_public_sites AS a, bc_sites AS b }.
	qq{WHERE b.id = a.id_site AND a.id_ant_nest = ?},

	SELECT_SITE                     =>
	qq{SELECT * from bc_sites WHERE id = ?},

	TEST_ANT_NEST_PUBLIC_SITE       =>
	qq{SELECT * from ant_nests_public_sites WHERE id_ant_nest = ? AND id_site = ?},

};

sub does_this_site_belongs_to_this_ant_nest{
    my ($class, $stash, $id_site, $code) = @_;
    my $row = $stash->select_one_row_arr(TEST_ANT_NEST_PUBLIC_SITE, $code, $id_site);
    return (defined($row)) ? 1 : 0;
}

sub get_site_arr{
    my ($class, $stash, $id) = @_;
    return $stash->select_one_row_arr(SELECT_SITE, $id);
}

sub get_site_hash{
    my ($class, $stash, $id) = @_;
    
    my $st = $stash->prepare_cached(SELECT_SITE);
    $st->bind_param(1, $id);
    $st->execute();

    my $hash = $st->fetchrow_hashref();
    $st->finish();
    return $hash;
}

sub get_public_sites_ant_nest_ds{
    my ($class, $stash, $code) = @_;
    return $stash->select_all_ds(SELECT_ALL_SITES_OF_ANT_NEST, $code);
}

#temporary function, ad usum the commands to create the test ant nests...
sub get_first_public_site{
    my ($class, $stash, $code) = @_;
    
    my $st = $stash->prepare_cached(SELECT_PUBLIC_SITES_OF_ANT_NEST);
    $st->bind_param(1, $code);
    $st->execute();

    my $row = $st->fetch();
    $st->finish();

    return $row->[0];
}


sub create_a_site{
    my ($class, $stash, 
	$name, $location, $directions, 
	$time_restricted, $opening_hours) = @_;

    my $st = $stash->prepare_cached(CREATE_SITE);
    $st->bind_param(1, $name);
    $st->bind_param(2, $location);
    $st->bind_param(3, $directions);
    $st->bind_param(4, $time_restricted);
    $st->bind_param(5, $opening_hours);

    $st->execute();

    return $stash->get_id_of_last_site();
}

#I simply have a correspondence between an ant nest and a site.
sub create_a_public_ant_nest_site{
    my ($class, $stash, $code, $site) = @_;

    my $st = $stash->prepare_cached(CREATE_PUBLIC_ANT_NEST_SITE);
    $st->bind_param(1, $code);
    $st->bind_param(2, $site);
    $st->execute();
}

1;
