package Bcd::Data::PublicSites;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


#this file models the set of ant nests in the system
use strict;
use warnings;

use Data::Dumper;
use Bcd::Constants::PublicSitesConstants;

use constant {
    CREATE_PUBLIC_SITE_PRESENCE                   =>
	qq{INSERT into users_in_sites(id_user, id_site, id_rel_type, on_request, presence) values (?,?,?,?,?)},

    CREATE_SPECIAL_SITE_PRESENCE                   =>
	qq{INSERT into users_in_sites(id_user, special_site, id_rel_type, }.
	       qq{ on_request, presence) values (?,?,?,?,?)},

	SELECT_SITES_WHERE_THIS_ANT_IS_IN_OFFICE     =>
	qq{SELECT id_site FROM users_in_sites WHERE id_user=? AND id_rel_type = }.
	Bcd::Constants::PublicSitesConstants::OFFICE_PRESENCE,

	SELECT_AVAILABILITY_OF_THIS_ANT_IN_OFFICE    =>
	qq{SELECT on_request, presence FROM users_in_sites WHERE id_user=? AND id_site = ? AND id_rel_type = }.
	Bcd::Constants::PublicSitesConstants::OFFICE_PRESENCE,

	SELECT_AVAILABILITY_OF_THIS_ANT    =>
	qq{SELECT u.id_site, b.name, u.id_rel_type, u.on_request, u.presence }.
	qq{FROM users_in_sites as u, bc_sites as b }.
	qq{WHERE u.id_user=? AND u.id_site = b.id},

	SELECT_AVAILABILITY_IN_SPECIAL_SITE  =>
	qq{SELECT id, id_rel_type, on_request, presence FROM users_in_sites WHERE }.
	qq{ id_user = ? AND special_site = ? },

	GET_PRESENCE                                 =>
	qq{SELECT id_user, id_site, special_site, id_rel_type, on_request, presence }.
	qq{FROM users_in_sites WHERE id = ? },

	GET_PRESENCES_USER_SITE_REASON               =>
	qq{SELECT id, on_request, presence FROM users_in_sites WHERE }.
	qq{ (id_site = ? OR special_site = ?) AND id_user = ? AND id_rel_type = ?},

	GET_USER_PRESENCES_SUMMARY                   =>
	qq{SELECT u.id, u.id_site, b.name, u.special_site, u.on_request, u.presence }.
	qq{FROM users_in_sites as u left outer join bc_sites as b on (u.id_site = b.id)}.
	qq{WHERE u.id_user = ? AND u.id_rel_type = ?},

	GET_SIMILAR_PRESENCES_IN_SITE                =>
	qq{SELECT id, presence, on_request FROM users_in_sites WHERE }.
	qq{id != ? AND id_user = ? AND id_site = ? AND id_rel_type = ?},
	
	GET_SIMILAR_PRESENCES_IN_SPECIAL_SITE        =>
	qq{SELECT id, presence, on_request FROM users_in_sites WHERE }.
	qq{id != ? AND id_user = ? AND special_site = ? AND id_rel_type = ?},

	DELETE_PRESENCE                              =>
	qq{DELETE FROM users_in_sites WHERE id = ?},

	UPDATE_PRESENCE                              =>
	qq{UPDATE users_in_sites SET on_request = ?, presence = ? WHERE id = ?},

	UPDATE_SPACE_PRESENCE                        =>
	qq{UPDATE users_in_sites SET id_site = ? , special_site = ? WHERE id = ?},
	
};

sub update_presence_space{
    my ($class, $stash, $id_locus, $id_site, $id_special_site) = @_;
    $stash->prep_exec(UPDATE_SPACE_PRESENCE, $id_site, $id_special_site, $id_locus);
}

sub update_presence{
    my ($class, $stash, $id_locus, $on_request, $presence) = @_;
    $stash->prep_exec(UPDATE_PRESENCE, $on_request, $presence, $id_locus);
}

sub delete_presence{
    my ($class, $stash, $id_locus) = @_;
    $stash->prep_exec(DELETE_PRESENCE, $id_locus);
}

sub get_presences_similar_in_space_ds{
    my ($class, $stash, $id_locus, $id_user, $id_site, $special_site, $id_rel_type) = @_;

    #ok, I can have a presence in a real site or in a virtual site.
    if (defined($id_site)){
	return $stash->select_all_ds(GET_SIMILAR_PRESENCES_IN_SITE,
				     $id_locus, $id_user, $id_site, $id_rel_type);
    } else {
	return $stash->select_all_ds(GET_SIMILAR_PRESENCES_IN_SPECIAL_SITE,
				     $id_locus, $id_user, $special_site, $id_rel_type);
    }
}

sub get_user_presences_summary_ds{
    my ($class, $stash, $id_user, $id_rel_type) = @_;

    return $stash->select_all_ds
	(GET_USER_PRESENCES_SUMMARY, $id_user, $id_rel_type);
}

sub get_presences_user_ds{
    my ($class, $stash, $id_site, $id_special_site, $id_user, $id_rel_type) = @_;


    return $stash->select_all_ds
	(GET_PRESENCES_USER_SITE_REASON, 
	 $id_site, $id_special_site, $id_user, $id_rel_type);
    
}

sub get_presence_hash{
    my ($class, $stash, $id) = @_;
    return $stash->select_one_row_hash(GET_PRESENCE, $id);
}

sub get_presence_arr{
    my ($class, $stash, $id) = @_;
    return $stash->select_one_row_arr(GET_PRESENCE, $id);
}

sub get_presence_decoded_hash{
    my ($class, $stash, $id) = @_;
    my $hash = $stash->select_one_row_hash(GET_PRESENCE, $id);
    
    my $presence_type = "special_site";
    if (defined($hash->{id_site})){
	#ok, this is a presence with a real site, so I get this site
	my $site = Bcd::Data::Sites->get_site_arr($stash, $hash->{id_site});
	$hash->{site_name} = $site->[1];
	$presence_type = "an_point";
    }
    $hash->{presence_type} = $presence_type;
    return $hash;
    
}

sub get_availability_in_special_site_ds{
    my ($class, $stash, $user, $special_site) = @_;
    return $stash->select_all_ds(SELECT_AVAILABILITY_IN_SPECIAL_SITE, $user, $special_site);
}

sub get_this_ant_presences_in_public_sites_ds{
    my ($class, $stash, $user) = @_;

    my $st = $stash->prepare_cached(SELECT_AVAILABILITY_OF_THIS_ANT);
    $st->bind_param(1, $user);
    $st->execute();


    my $arr = $st->fetchall_arrayref();
    my $var = $st->{NAME_lc};
    unshift (@{$arr}, $var);
    return $arr;
}

sub get_availability_of_this_ant_in_office{
    my ($class, $stash, $user, $id_site) = @_;

    my $st = $stash->prepare_cached(SELECT_AVAILABILITY_OF_THIS_ANT_IN_OFFICE);
    $st->bind_param(1, $user);
    $st->bind_param(2, $id_site);
    $st->execute();
    my $arr = $st->fetchrow_arrayref();
    $st->finish();
    return $arr;
}

sub get_sites_where_ant_is_in_office{
    my ($class, $stash, $user) = @_;

    my $st = $stash->prepare_cached(SELECT_SITES_WHERE_THIS_ANT_IS_IN_OFFICE);
    $st->bind_param(1, $user);
    $st->execute();
    my $arr = $st->fetchall_arrayref();
    return $arr;
}

#this is a temporary function, just to allow the population of the db.
sub add_initial_hq{
    my ($class, $stash, $code) = @_;

    
    my $boss = Bcd::Data::Users->get_boss_of_this_ant_nest($stash, $code);
    my $site_id = Bcd::Data::Sites->get_first_public_site($stash, $code);

    my $boss_id = $boss->{id};

    my $st = $stash->prepare_cached(CREATE_PUBLIC_SITE_PRESENCE);

    $st->bind_param(1, $boss_id);
    $st->bind_param(2, $site_id);
    $st->bind_param(3, Bcd::Constants::PublicSitesConstants::OFFICE_PRESENCE);
    $st->bind_param(4, undef);
    $st->bind_param(5, undef);

    $st->execute();
}

sub add_presence_in_site{
    my ($class, $stash, $user, $site, $reason, $on_request, $available_hours) = @_;

    
    $stash->prep_exec(CREATE_PUBLIC_SITE_PRESENCE, 
		      $user, $site, 
		      $reason,
		      $on_request,
		      $available_hours);

    return $stash->get_last_public_site_presence();
}

sub add_special_site_presence{
    my ($class, $stash, $user, $id_special_site, $reason, $on_request, $available_hours) = @_;

    
    $stash->prep_exec(CREATE_SPECIAL_SITE_PRESENCE, 
		      $user, $id_special_site, 
		      $reason,
		      $on_request,
		      $available_hours);

    return $stash->get_last_public_site_presence();
}

sub add_service_presence_in_site{
    my ($class, $stash, $user, $site, $on_request, $available_hours) = @_;

    $stash->prep_exec(CREATE_PUBLIC_SITE_PRESENCE, 
		      $user, $site, 
		      Bcd::Constants::PublicSitesConstants::SERVICE_PRESENCE,
		      $on_request,
		      $available_hours);

    return $stash->get_last_public_site_presence();
}

sub add_office_presence_in_site{
    my ($class, $stash, $user, $site, $on_request, $available_hours) = @_;

    my $st = $stash->prepare_cached(CREATE_PUBLIC_SITE_PRESENCE);

    $st->bind_param(1, $user);
    $st->bind_param(2, $site);
    $st->bind_param(3, Bcd::Constants::PublicSitesConstants::OFFICE_PRESENCE);
    $st->bind_param(4, $on_request);
    $st->bind_param(5, $available_hours);

    $st->execute();
}

#as usual I can init the db with the constants...
sub init_db{

    my ($self, $stash) = @_;
    my $conn = $stash->get_connection();

    my $sth = $conn->prepare(qq{insert into site_user_rel_type values(?,?)});

    foreach (Bcd::Constants::PublicSitesConstants::LIST_USER_SITE_REL_TYPES){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }

    $sth->finish();

    $sth = $conn->prepare(qq{insert into bc_special_sites values(?,?)});

    foreach (Bcd::Constants::PublicSitesConstants::LIST_SPECIAL_SITES){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }

    $sth->finish();
    
    
}

1;
