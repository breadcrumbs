package Bcd::Data::Activities;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Data::Dumper;
use Bcd::Constants::ActivitiesConstants;
use Bcd::Constants::it::ActivitiesNames;

#this file should manage the activities in the system
use constant {
    CREATE_NEW_CHILD_ACTIVITY                        =>
	qq{INSERT INTO activities(id, id_parent, description) VALUES (NEXTVAL(?), ?, ?)},

    CREATE_NEW_ACTIVITY                        =>
	qq{INSERT INTO activities(id, id_parent, description) VALUES (?, ?, ?)},
    
    SELECT_CHILD_ACTIVITIES                    =>
    qq{SELECT id, description FROM activities WHERE id_parent = ? ORDER BY description},

    GET_ACTIVITY                        =>
    qq{SELECT description, id_parent FROM activities WHERE id = ? },

};

#this function should simply return a string like "services >> home/services >> "
#like a breadcrumbs trail
sub get_all_parents{
    my ($class, $stash, $id_act) = @_;

#     my @list_act;

#     while(1){
# 	my $activity = $stash->select_one_row_arr(GET_ACTIVITY, $id_act);

# 	$id_act =  $activity->[1];
# 	unshift(@list_act, $activity->[0]);

# 	if ($id_act == Bcd::Constants::ActivitiesConstants::PARENT_SERVICES){
# 	    unshift(@list_act, Bcd::Constants::it::ActivitiesNames::SERVICE_ROOT);
# 	    last;
# 	} elsif ($id_act == Bcd::Constants::ActivitiesConstants::PARENT_HOMEMADE) {
# 	    unshift(@list_act, Bcd::Constants::it::ActivitiesNames::HOMEMADE_ROOT);
# 	    last;
# 	} elsif ($id_act == Bcd::Constants::ActivitiesConstants::PARENT_USED_ITEMS){
# 	    unshift(@list_act, Bcd::Constants::it::ActivitiesNames::OBJECTS_ROOT);
# 	    last;
# 	}

#     }

#     return \@list_act;

    #first recursion...
    my @list_act;
    my $activity = $stash->select_one_row_arr(GET_ACTIVITY, $id_act);

    if (defined($activity)){
	$class->get_all_parents_recurse($stash, $activity->[1], $activity->[0], \@list_act);
    }
    return \@list_act;
}

#it is of course a recursive function...
sub get_all_parents_recurse{
    my ($class, $stash, $id_act, $desc, $list_act) = @_;

    #end of recursion?
    if (!defined($id_act)){
	#ok, this is the end of recursion...
	if ($desc eq Bcd::Constants::ActivitiesConstants::PARENT_SERVICE_AR->[1]){
	    unshift(@{$list_act}, Bcd::Constants::it::ActivitiesNames::SERVICE_ROOT);
	    return;
	} elsif ($desc eq Bcd::Constants::ActivitiesConstants::PARENT_HOMEMADE_AR->[1]) {
	    unshift(@{$list_act}, Bcd::Constants::it::ActivitiesNames::HOMEMADE_ROOT);
	    return;
	} elsif ($desc eq Bcd::Constants::ActivitiesConstants::PARENT_USED_ITEMS_AR->[1]){
	    unshift(@{$list_act}, Bcd::Constants::it::ActivitiesNames::OBJECTS_ROOT);
	    return;
	}
    }

    #normal activity, I put it in list and get its parent
    unshift(@{$list_act}, $desc);

    my $activity = $stash->select_one_row_arr(GET_ACTIVITY, $id_act);
    return $class->get_all_parents_recurse($stash, $activity->[1], $activity->[0], $list_act);
}

sub get_all_parents_str{
    my ($class, $stash, $id_act) = @_;

    my $list = $class->get_all_parents($stash, $id_act);
    return join(' > ', @{$list});
}

sub get_service_activities{
    my ($class, $stash) = @_;

    #ok, I should get all the activities which are child of PARENT_SERVICES
    return $class->build_activity_tree_from($stash, Bcd::Constants::ActivitiesConstants::PARENT_SERVICES);
};

sub build_activity_tree_from{
    my ($class, $stash, $root_id) = @_;
    #print "recursion, root is $root_id \n";

    #this is my tree, I simply make a recursive descent...
    
    my $st = $stash->prepare_cached(SELECT_CHILD_ACTIVITIES);
    $st->bind_param(1, $root_id);
    $st->execute();

    my $children = $st->fetchall_arrayref();

    #if this node has no children, then it was a leaf, end of recursion
    if (scalar(@{$children}) == 0){
	#print "End of recursion, I have $root_id\n";
	return $root_id;
    }

    #print Dumper($children);

    #print "I have to recurse, $root_id \n";
    
    my @list;
    foreach(@{$children}){
	my $item = [$_->[1], $class->build_activity_tree_from($stash, $_->[0])];
	#print "my item is \n";
	#print Dumper($item);
	push(@list, $item);
    }

    return \@list;
}

sub build_activity_tree_summary_from{
    my ($class, $stash, $root_id, $post_code) = @_;
    
    my $tree = $class->build_activity_tree_from($stash, $root_id);

    #ok, now for each activity I get the summary
    $class->_populate_with_summary_this_tree($stash, $tree, $post_code);

    return $tree;
}

sub _populate_with_summary_this_tree{
    my ($class, $stash, $tree, $post_code) = @_;

    #this is a recursive function just to add the number of ads for each activity
    foreach(@{$tree}){
	if (ref($_->[1]) eq "ARRAY"){
	    #ok, let's recurse
	    $class->_populate_with_summary_this_tree($stash, $_->[1], $post_code);
	} else {
            #ok, it is a scalar
	    #get the number of ads
	    my $ads = Bcd::Data::Ads->get_count_activity_post_code_ads($stash, $_->[1], $post_code);
	    push(@{$_}, $ads);
	}
    }
}


sub init_db{
    my ($class, $stash) = @_;

    my $st = $stash->prepare_cached(CREATE_NEW_ACTIVITY);

    #first of all the macro activities...
    foreach (Bcd::Constants::ActivitiesConstants::LIST_MASTER_ACTIVITIES){
	$st->bind_param(1, $_->[0]);
	$st->bind_param(2, undef);
	$st->bind_param(3, $_->[1]);
	$st->execute();
    }

    #I should open the text file with the activities...

    my $name = "script/install/initial_activities";
    open ACTIVITIES, "< $name" or die "Not found the initial activities file\n";

    local $/ = "\n";
    my %activities_map;

    $st = $stash->prepare_cached(CREATE_NEW_CHILD_ACTIVITY);
    while (<ACTIVITIES>){

	next if (/^\043/); #\043 is #
	next if (/^\s+$/);
	chomp;
	my @row = split (/ % /);
	my $current_type;


	if ($row[2] eq 'ORIG_SERVICE'){
	    #this is a parent activity
	    $st->bind_param(2, Bcd::Constants::ActivitiesConstants::PARENT_SERVICES);
	    $st->bind_param(3, $row[1]);
	    $current_type = Bcd::Constants::ActivitiesConstants::PARENT_SERVICES;
	} elsif ($row[2] eq 'ORIG_HOMEMADE'){
	    #this is a parent activity
	    $st->bind_param(2, Bcd::Constants::ActivitiesConstants::PARENT_HOMEMADE);
	    $st->bind_param(3, $row[1]);
	    $current_type = Bcd::Constants::ActivitiesConstants::PARENT_HOMEMADE;
	} elsif ($row[2] eq 'ORIG_USED'){
	    #this is a parent activity
	    $st->bind_param(2, Bcd::Constants::ActivitiesConstants::PARENT_USED_ITEMS);
	    $st->bind_param(3, $row[1]);
	    $current_type = Bcd::Constants::ActivitiesConstants::PARENT_USED_ITEMS;
	} else {
	    #this is a child activity
	    my $parent_id   = $activities_map{$row[2]}->[0];
	    $current_type   = $activities_map{$row[2]}->[1];
	    $st->bind_param(2, $parent_id);
	    $st->bind_param(3, $row[1]);
	}

	my $sequence;
	if ($current_type == Bcd::Constants::ActivitiesConstants::PARENT_SERVICES){
	    $sequence = "activities_services_seq";
	} elsif ($current_type == Bcd::Constants::ActivitiesConstants::PARENT_HOMEMADE){
	    $sequence = "activities_homemade_seq";
	} elsif ($current_type == Bcd::Constants::ActivitiesConstants::PARENT_USED_ITEMS){
	    $sequence = "activities_used_seq";
	}
	$st->bind_param(1, $sequence);

	$st->execute();
	my $act_id = $stash->get_last_activity_id($sequence);
	#I store this new activity
	$activities_map{$row[0]} = [$act_id, $current_type];
    }

}



#the super user can create all the activities he want
sub create_activity{
    my ($class, $stash, $activity_name, $parent) = @_;

    #ok, I create the activity...
    my $st = $stash->prepare_cached(CREATE_NEW_ACTIVITY);
    $st->bind_param(1, $parent);
    $st->bind_param(2, $activity_name);
    $st->execute();

}



1;
