package Bcd::Data::WebSite;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Data::Dumper;

use Bcd::Constants::it::TransactionsTemplates;

use constant {
    MINIMUM_AD_TAX => 0.000005,
    MAXIMUM_AD_TAX => 0.000495,
};

#the preconditions of this function are that the invoice should be in the right
#state
sub user_pays_invoice{
    my ($class, $stash, $buyer, $seller, 
	$credits_towards_seller, $cheque_amount, $convertible_tao) = @_;

    #this function simply performs the payment... the plan has been decided already
    
}

#????? this is to be defined better.
sub user_collect_payment{
    my ($class, $stash, $seller, $price) = @_;
}


#this module is responsible for the bookkeeping of the users accounts
#whenever they put ads in the system

sub get_price_for_this_ad{
    my ($class, $stash, $id_user, $p_min, $p_max, $t_e, $t_c) = @_;
    
    my ($price, $reachable_ants, $total_ants) = 
	$class->get_price_report_for_this_ad
	($stash, $id_user, $p_min, $p_max, $t_e, $t_c);

    return $price;
}

sub get_price_for_this_presence{
    my ($class, $stash, $id_ad, $id_user, $p_min, $p_max, $t_e, $t_c) = @_;

    my ($price, $reachable_ants, $total_ants) = 
	$class->get_price_report_for_adding_a_presence
	($stash, $id_ad, $id_user, $p_min, $p_max, $t_e, $t_c);

    return $price;
}

sub get_price_report_for_adding_a_presence{
    my ($class, $stash, $id_ad, $id_user, $p_min, $p_max, $t_e, $t_c) = @_;

    #ok, first of all I get all the presences of this ad...
    my $max_price = 0;
    my $ads_loc = Bcd::Data::Ads->get_all_localities_for_ad_arr($stash, $id_ad);

    foreach(@{$ads_loc}){
	#I get the price for this presence
	my $price = $class->get_price_for_this_ad
	    ($stash, $id_user, $_->[1], $_->[2], $->[3], $_->[4]);
	if ($price > $max_price){
	    $max_price = $price;
	}
    }

    #ok, then I get the price report for the presence I want to add
    my ($price, $reachable_ants, $total_ants) = 
	$class->get_price_report_for_this_ad
	($stash, $id_user, $p_min, $p_max, $t_e, $t_c);

    if ($price > $max_price){
	#I pay the difference
	$price -= $max_price;
    } else {
	$price = 0;
    }

    return ($price, $reachable_ants, $total_ants);
    
}

sub get_price_report_for_this_ad{
    my ($class, $stash, $id_user, $p_min, $p_max, $t_e, $t_c) = @_;

    #ok, this function should simply define the price for an ad in
    #this version of the system the price is given by p_min (p_max is
    #not used).

    my $t_max = ($t_c > $t_e) ?  $t_c : $t_e;

    #I should get the percentage of ants which can pay by credit...
    
    #mmm, I get the nets of this user
    
    #I should ask them to the trustnet
    my $net = Bcd::Data::TrustsNet->get_nets_for_user($stash, $id_user);

    #print Dumper($net);

    my $reachable_ants = 0;

    #let's get the percentage of ants which can pay by credit
    foreach (keys(%{$net})){
	if ($net->{$_}->[0] > $t_max){
	    $reachable_ants ++;
	}
    }
    my $total_ants = scalar(keys(%{$net}));
    my $percentage = $reachable_ants / $total_ants;;

    #now I compute the price... the price is in tao, I don't use the
    #big* library
    my $price = $p_min * ( MINIMUM_AD_TAX + MAXIMUM_AD_TAX * ( 1 - ($percentage * $percentage)));

    #my $price_p = $price / $p_min * 100;
    #print "reach = $reachable_ants, perc = $percentage, price = $p_min tax = $price %p = $price_p \n";

    return ($price, $reachable_ants, $total_ants);
}

sub charge_user_for_this_presence{
    my ($class, $stash, $id_ad, $user, $ant_nest, $p_min, $p_max, $t_e, $t_c) = @_;

    my $price = $class->get_price_for_this_presence
	($stash, $id_ad, $user, $p_min, $p_max, $t_e, $t_c);
    
    return $class->_charge_user_for_ad($stash, $user, $ant_nest, $price);
}

sub _charge_user_for_ad{
    my ($class, $stash, $user, $ant_nest, $price) = @_;

    #ok, now I get the accounts
    my ($us_act_e, $us_act_t) = Bcd::Data::Users->get_users_accounts($stash, $user);

    #ok, now I get the partial income euro for the ant nest...
    my $par_tao_ads_incomes = Bcd::Data::AntNests->get_tao_ads_partial_income_id($stash, $ant_nest);

    #ok, now I can have a simple transaction...
    my $list_debit_accounts = [$us_act_t];
    my $list_debit_amounts  = [$price];

    my $list_credit_accounts= [$par_tao_ads_incomes];
    my $list_credit_amounts = [$price];


    my ($debits, $credits) = 
	Bcd::Data::Accounting::Transaction->add_new_transaction_in_db
	($list_debit_accounts, $list_debit_amounts,
	 $list_credit_accounts, $list_credit_amounts, 
	 Bcd::Constants::it::TransactionsTemplates::USER_PUT_AD, $stash);

    my $new_total_tao  = $debits->[0];

    return $new_total_tao;

}

#this function should simply debit the user account
sub charge_user_for_this_ad{
    my ($class, $stash, $user, $ant_nest, $p_min, $p_max, $t_e, $t_c) = @_;

    #firs of all I compute the price for this ad 

    my $price = $class->get_price_for_this_ad($stash, $user, $p_min, $p_max, $t_e, $t_c);
    return $class->_charge_user_for_ad($stash, $user, $ant_nest, $price);
}

1;
