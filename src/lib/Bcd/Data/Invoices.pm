package Bcd::Data::Invoices;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Data::Dumper;

use Bcd::Constants::InvoicesConstants;
use Bcd::Data::Token;
use Digest::SHA1;

use constant {
    INSERT_NEW_INVOICE                              =>
	qq{INSERT INTO invoices(id_status, id_ad, id_locus, user_to, amount) VALUES (?,?,?,?,?)},

	GET_INVOICE                                 =>
	qq{SELECT id_status, id_ad, id_locus, user_to, date, amount, quantity FROM invoices WHERE id = ?},

	EMIT_INVOICE                                =>
	qq{UPDATE invoices set quantity = ?, id_status = ?  WHERE id = ?},

	UPDATE_INVOICE_STATE                        =>
	qq{UPDATE invoices SET id_status = ? WHERE id = ?},

	CREATE_INVOICE_TOKEN                        =>
	qq{INSERT INTO invoices_tokens(id_invoice, amount_paid_by_credit, }.
	   qq{ full_token, secret_token) VALUES(?,?,?,?)},

	GET_INVOICE_TOKEN                           =>
	qq{SELECT amount_paid_by_credit, full_token, secret_token FROM invoices_tokens WHERE id_invoice = ?},

	DELETE_INVOICE_TOKEN                        =>
	qq{DELETE FROM invoices_tokens WHERE id_invoice = ? },

	GET_RECEIVED_INVOICES_SUMMARY_BY_STATE      =>
	qq{SELECT count(*), sum(quantity * amount) as total FROM invoices WHERE user_to = ? AND id_status = ?},

	GET_RECEIVED_INVOICES_LIST_BY_STATE         =>
	qq{SELECT i.id, a.id_user as seller, i.amount, i.quantity, (i.amount * i.quantity) AS total }.
	qq{FROM invoices AS i, ads AS a }.
	qq{WHERE i.id_ad = a.id AND i.user_to = ? AND i.id_status = ?},

	GET_EMITTED_INVOICES_SUMMARY_BY_STATE       =>
	qq{SELECT count(i.*), sum(i.quantity * i.amount) as total FROM invoices AS i, ads AS a }.
	qq{WHERE i.id_ad = a.id AND a.id_user = ? AND i.id_status = ?},

	GET_EMITTED_INVOICES_LIST_BY_STATE          =>
	qq{SELECT i.id, i.user_to as buyer, i.amount, i.quantity, (i.amount * i.quantity) AS total }.
	qq{FROM invoices AS i, ads AS a }.
	qq{WHERE i.id_ad = a.id AND a.id_user = ? AND id_status = ?},

	GET_NOT_COLLECTED_INVOICES_COUNT_FOR_LOCUS  =>
	qq{SELECT count(*) FROM invoices WHERE id_locus = ? AND id_status != }.
	Bcd::Constants::InvoicesConstants::COLLECTED,

	GET_INVOICES_COUNT_FOR_LOCUS                =>
	qq{SELECT count(*) FROM invoices WHERE id_locus = ?}
};

sub get_invoices_count_for_this_locus{
    my ($class, $stash, $id_locus) = @_;
    my $arr = $stash->select_one_row_arr(GET_INVOICES_COUNT_FOR_LOCUS, $id_locus);
    return $arr->[0];
}

sub are_there_active_invoices_in_this_locus{
    my ($class, $stash, $id_locus) = @_;
    my $arr = $stash->select_one_row_arr(GET_NOT_COLLECTED_INVOICES_COUNT_FOR_LOCUS, $id_locus);

    if (!defined($arr)){
	#no invoice...
	return 0;
    }

    my $count = $arr->[0];

    if ($count != 0){
	return 1;
    } else {
	return 0;
    }
}

sub get_received_invoices_list_by_state_ds{
    my ($class, $stash, $buyer, $state) = @_;
    
    return $stash->select_all_ds
	(GET_RECEIVED_INVOICES_LIST_BY_STATE,
	 $buyer, $state);
				  
}

sub get_emitted_invoices_list_by_state_ds{
    my ($class, $stash, $seller, $state) = @_;
    
    return $stash->select_all_ds
	(GET_EMITTED_INVOICES_LIST_BY_STATE,
	 $seller, $state);
}

sub get_received_invoices_summary_by_state_arr{
    my ($class, $stash, $buyer, $state) = @_;
    
    return $stash->select_one_row_arr
	(GET_RECEIVED_INVOICES_SUMMARY_BY_STATE,
	 $buyer, $state);
}

sub get_emitted_invoices_summary_by_state_arr{
    my ($class, $stash, $seller, $state) = @_;

    return $stash->select_one_row_arr
	(GET_EMITTED_INVOICES_SUMMARY_BY_STATE,
	 $seller, $state);
}

sub get_invoice_token_hash{
    my ($class, $stash, $invoice_id) = @_;
    return $stash->select_one_row_hash(GET_INVOICE_TOKEN, $invoice_id);
}


#this function should create a new invoice
sub create_new_invoice{
    my ($class, $stash, $buyer, $id_ad, $id_locus, $amount) = @_;

    $stash->prep_exec(INSERT_NEW_INVOICE,
		      Bcd::Constants::InvoicesConstants::AUTHORIZED, 
		      $id_ad,$id_locus, $buyer, $amount);

    return $stash->get_last_invoice_id();
}

#this function should simply emit the invoice, with a quantity
sub emit_invoice{
    my ($class, $stash, $invoice_id, $quantity) = @_;

    $stash->prep_exec(EMIT_INVOICE,
		      $quantity,
		      Bcd::Constants::InvoicesConstants::EMITTED,
		      $invoice_id);

}

#when I pay the invoice I generate the token...
sub pay_invoice_and_get_token{
    my ($class, $stash, $invoice_id, $amount_paid_by_credit) = @_;

    #ok, first of all I update the state of the invoice.
    $stash->prep_exec(UPDATE_INVOICE_STATE,
		      Bcd::Constants::InvoicesConstants::PAID,
		      $invoice_id);

    #ok, then I generate two tokens...
    my ($long_token, $blinded_token, $secret_token) = 
	Bcd::Data::Token::generate_long_token();

    #digest the secret token
    my $sha = Digest::SHA1->new;
    $sha->add($secret_token);
    my $digest = $sha->hexdigest;

    #ok, I will now add this row
    $stash->prep_exec(CREATE_INVOICE_TOKEN,
		      $invoice_id,
		      $amount_paid_by_credit,
		      $blinded_token,
		      $digest);


    #I return the long token for the buyer.
    return ($long_token, $blinded_token);
}

sub collect_invoice{
    my ($class, $stash, $invoice_id) = @_;

    #I should simply change the state and delete the tokens row (which is useless now).
    $stash->prep_exec(UPDATE_INVOICE_STATE,
		      Bcd::Constants::InvoicesConstants::COLLECTED,
		      $invoice_id);

    $stash->prep_exec(DELETE_INVOICE_TOKEN,
		      $invoice_id);
}

sub get_invoice_arr{
    my ($class, $stash, $invoice_id) = @_;
    return $stash->select_one_row_arr(GET_INVOICE, $invoice_id);
}

sub get_invoice_hash{
    my ($class, $stash, $invoice_id) = @_;
    return $stash->select_one_row_hash(GET_INVOICE, $invoice_id);
}


sub init_db {
    my ($class, $stash) = @_;

    my $conn = $stash->get_connection();

    #ok, now I should insert the values in the table...
    my $sth = $conn->prepare(qq{insert into invoices_statuses values(?,?)});

    foreach (Bcd::Constants::InvoicesConstants::LIST_INVOICES_STATES){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }
}


1;
