package Bcd::Data::Deposits;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#use Math::BigInt lib => "GMP";

use Bcd::Constants::DepositsConstants;
use Bcd::Data::Token;
use Bcd::Data::Bank;

use Digest::SHA1;

use constant {

    INSERT_DEPOSIT_OR_WITHDRAWAL_REQUEST =>
	qq{INSERT into deposits_and_withdrawals(id_user, is_deposit, amount, status, booking_token, }.
	qq{full_receipt_token, secret_token) }.
	qq{values (?, ?, ?, } . Bcd::Constants::DepositsConstants::CREATION_STATE . qq{, ?, ?, ?)},

    SELECT_DEPOSIT_OR_WITHDRAWAL_USER_BOOKING =>
    qq{SELECT * from deposits_and_withdrawals where id_user = ? and is_deposit = ? and booking_token = ?},

    SELECT_BOOKING_FROM_TOKEN =>
    qq{SELECT * from deposits_and_withdrawals where booking_token = ?},

    SELECT_BOOKING_FROM_ID =>
    qq{SELECT * from deposits_and_withdrawals where id = ?},

    SELECT_COUNT_PENDING_USERS_REQUESTS =>
    qq{SELECT count(*) from deposits_and_withdrawals where id_user=? and }.
    qq{status !=}.Bcd::Constants::DepositsConstants::COLLECTED,

    SELECT_PENDING_USERS_REQUEST =>
    qq{SELECT * from deposits_and_withdrawals where id_user=? and }.
    qq{status !=}.Bcd::Constants::DepositsConstants::COLLECTED,

    DELETE_BOOKING_ID               =>
    qq{DELETE FROM deposits_and_withdrawals WHERE id = ? },

    SELECT_ALL_ANT_NEST_BOOKINGS_STATUS =>
    qq{SELECT d.booking_token, d.amount FROM deposits_and_withdrawals AS d, users as u WHERE }.
    qq{ d.id_user = u.id AND u.id_ant_nest = ? AND d.is_deposit = ? AND d.status = ? }.
    qq{ ORDER BY d.id },

    SELECT_ALL_ANT_NEST_BOOKINGS_STATUS_OFFLINE =>
    qq{SELECT d.booking_token, d.amount, d.full_receipt_token FROM deposits_and_withdrawals AS d, users as u WHERE }.
    qq{ d.id_user = u.id AND u.id_ant_nest = ? AND d.is_deposit = ? AND d.status = ? }.
    qq{ ORDER BY d.booking_token },

    SELECT_ALL_ANT_NEST_BOOKINGS_FOR_TREASURER =>
    qq{SELECT d.id, d.id_user, d.booking_token, d.amount, d.is_deposit, d.full_receipt_token  }.
    qq{ FROM deposits_and_withdrawals AS d, users as u WHERE }.
    qq{ d.id_user = u.id AND u.id_ant_nest = ? AND d.status = ? },

};


use constant{
    SELECT_USER_DEPOSIT_TO_CLAIM   => "Deposits_select_user_deposit_to_claim",
    CLAIM_USER_DEPOSIT             => "Deposits_claim_user_deposit",
    INSERT_NEW_DEPOSIT_REQUEST     => "Deposits_insert_deposit_request",
    CHANGE_STATE_TO_DEPOSIT        => "Deposits_change_state_to_deposit",
    SELECT_DEPOSIT_USER_WITH_TOKEN => "Deposits_select_deposit_user_with_token",
    SELECT_DEPOSIT_WITH_ID         => "Deposits_select_deposit_with_id",
};

#this function is one shot... so it does not put the commands in the
#stash
sub init_db{
    my ($self, $stash) = @_;
    my $conn = $stash->get_connection();

    #ok, now I should insert the values in the table...
    my $sth = $conn->prepare(qq{insert into deposit_withdrawal_statuses values(?,?)});

    foreach (Bcd::Constants::DepositsConstants::LIST_DEPOSITS_STATES){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }

    $sth->finish();

}


sub get_deposits_and_withdrawals_to_acknowledge_offline{
    my ($self, $stash, $ant_nest) = @_;

    my $st = $stash->prepare_cached(SELECT_ALL_ANT_NEST_BOOKINGS_FOR_TREASURER);
    $st->bind_param(1, $ant_nest);
    $st->bind_param(2, Bcd::Constants::DepositsConstants::CREATION_STATE);
    $st->execute();

    my $arr = $st->fetchall_arrayref();
    return $arr;
}

sub get_deposits_to_do_offline_ds{
    my ($self, $stash, $ant_nest) = @_;

    return $self->_get_bookings_to_collect_offline_ds
	($stash, $ant_nest, 1, Bcd::Constants::DepositsConstants::CREATION_STATE);
}

sub get_withdrawals_to_do_offline_ds{
    my ($self, $stash, $ant_nest) = @_;

    return $self->_get_bookings_to_collect_offline_ds
	($stash, $ant_nest, 0, Bcd::Constants::DepositsConstants::CREATION_STATE);
}

sub _get_bookings_to_collect_offline_ds{
    my ($self, $stash, $ant_nest, $is_deposit, $status) = @_;

    my $st = $stash->prepare_cached(SELECT_ALL_ANT_NEST_BOOKINGS_STATUS_OFFLINE);
    $st->bind_param(1, $ant_nest);
    $st->bind_param(2, $is_deposit);
    $st->bind_param(3, $status);

    $st->execute();

    my $var = $st->{NAME_lc};
    my $arr = $st->fetchall_arrayref();

    #I put at the front of the recordset the columns names.
    unshift (@{$arr}, $var);

    return $arr; 

}

sub get_withdrawals_to_collect_offline_ds{
    my ($self, $stash, $ant_nest) = @_;

    return $self->_select_all_ant_nest_booking_status_ds
	($stash, $ant_nest, 0, Bcd::Constants::DepositsConstants::OFFLINE_ACKNOWLEDGED);
}

sub get_all_created_deposits_ant_nest_ds {
    my ($self, $stash, $ant_nest) = @_;

    return $self->_select_all_ant_nest_booking_status_ds
	($stash, $ant_nest, 1, Bcd::Constants::DepositsConstants::CREATION_STATE);
}

sub get_all_created_withdrawals_ant_nest_ds {
    my ($self, $stash, $ant_nest) = @_;

    return $self->_select_all_ant_nest_booking_status_ds
	($stash, $ant_nest, 0, Bcd::Constants::DepositsConstants::CREATION_STATE);
}

sub _select_all_ant_nest_booking_status_ds{
    my ($self, $stash, $ant_nest, $is_deposit, $status) = @_;

    my $st = $stash->prepare_cached(SELECT_ALL_ANT_NEST_BOOKINGS_STATUS);
    $st->bind_param(1, $ant_nest);
    $st->bind_param(2, $is_deposit);
    $st->bind_param(3, $status);

    $st->execute();

    my $var = $st->{NAME_lc};
    my $arr = $st->fetchall_arrayref();

    #I put at the front of the recordset the columns names.
    unshift (@{$arr}, $var);

    return $arr; 
}

sub delete_booking_id{
    my ($self, $stash, $booking_id) = @_;

    my $st = $stash->prepare_cached(DELETE_BOOKING_ID);
    $st->bind_param(1, $booking_id);
    $st->execute();
}

sub get_pending_booking_from_user_id_arr{
    my ($self, $stash, $user_id) = @_;

    my $st = $stash->prepare_cached(SELECT_PENDING_USERS_REQUEST);
    $st->bind_param(1, $user_id);

    $st->execute();
    my $res = $st->fetchrow_arrayref();
    $st->finish();

    return $res;
}

=head1 are_there_pending_deposit_for_this_user

    This function simply looks if the user has already some OR withdrawals active...

=cut 
sub is_there_a_pending_request_for_this_user{
    my ($self, $stash, $user_id) = @_;

    my $st = $stash->prepare_cached(SELECT_COUNT_PENDING_USERS_REQUESTS);
    $st->bind_param(1, $user_id);

    $st->execute();
    my $res = $st->fetchrow_arrayref();
    $st->finish();

    if ( $res->[0] != 0 ) {
	return 1;
    } else {
	return 0;
    }
    
}

sub _create_deposit_or_withdrawal_booking{
   my ($self, $stash, $user_id, $amount, $is_deposit) = @_;

   my $booking_token = Bcd::Data::Token::generate_new_token();
   my ($long_token, $blinded_token, $secret_token) = Bcd::Data::Token::generate_long_token();

   #ok, now I should make the hash of the secret token
   my $sha = Digest::SHA1->new;
   $sha->add($secret_token);
   my $digest = $sha->hexdigest;

   #ok, now I am ready to insert the data in the table
   my $st = $stash->prepare_cached(INSERT_DEPOSIT_OR_WITHDRAWAL_REQUEST);
   
   $st->bind_param(1, $user_id);
   $st->bind_param(2, $is_deposit);
   $st->bind_param(3, $amount);
   $st->bind_param(4, $booking_token);

   #if it is a withdrawal I record in the db the blinded token, not the full one
   if ($is_deposit != 0){
       $st->bind_param(5, $long_token);
   } else {
       $st->bind_param(5, $blinded_token);
   }

   $st->bind_param(6, $digest);

   $st->execute();

   #I return these tokens, but one of them is not returned to the user.
   return ($booking_token, $long_token, $blinded_token);
}

=head1

    This function should see create a user deposit

=cut 

sub create_deposit_booking{
    my ($self, $stash, $user_id, $amount) = @_;

    my ($booking_token, $long_token, $blinded_token) = 
	$self->_create_deposit_or_withdrawal_booking
	($stash, $user_id, $amount, 1);

    #the long token is NOT returned for the deposits
    return ($booking_token, $blinded_token);

}

sub create_withdrawal_booking{

    my ($self, $stash, $user_id, $amount) = @_;

    my ($booking_token, $long_token, $blinded_token) = 
	$self->_create_deposit_or_withdrawal_booking
	($stash, $user_id, $amount, 0);

    #the blinded_token is NOT returned for the withdrawals
    return ($booking_token, $long_token);
}

=head2

    This function simply changes the state of the booking

=cut
sub treasurer_acknowledged {
    my ($self, $stash, $booking_id) = @_;

    my $st = $stash->get_statement(CHANGE_STATE_TO_DEPOSIT, $self);

    $st->bind_param(1, Bcd::Constants::DepositsConstants::OFFLINE_ACKNOWLEDGED);
    $st->bind_param(2, $booking_id);

    return $st->execute();
}

sub get_deposit_booking_from_user_and_token{

    my ($self, $stash, $user_id, $booking) = @_;

    return $self->_get_deposit_or_withdrawal_booking_from_user_token
	($stash, $user_id, $booking, '1');
}

sub get_withdrawal_booking_from_user_and_token{

    my ($self, $stash, $user_id, $booking) = @_;

    return $self->_get_deposit_or_withdrawal_booking_from_user_token
	($stash, $user_id, $booking, '0');
}

sub _get_deposit_or_withdrawal_booking_from_user_token{

    my ($self, $stash, $user_id, $booking, $is_deposit) = @_;

    my $st = $stash->prepare_cached(SELECT_DEPOSIT_OR_WITHDRAWAL_USER_BOOKING);

    $st->bind_param(1, $user_id);
    $st->bind_param(2, $is_deposit);
    $st->bind_param(3, $booking);

    $st->execute();

    my $row = $st->fetchrow_arrayref();
    $st->finish();

    return $row;
}

sub get_booking_from_token_arr{
    my ($self, $stash, $booking) = @_;

    my $st = $stash->prepare_cached(SELECT_BOOKING_FROM_TOKEN);

    $st->bind_param(1, $booking);

    $st->execute();

    my $arr = $st->fetchrow_arrayref();
    $st->finish();

    return $arr;
}

sub get_booking_from_token_hash{

    my ($self, $stash, $booking) = @_;

    my $st = $stash->prepare_cached(SELECT_BOOKING_FROM_TOKEN);

    $st->bind_param(1, $booking);

    $st->execute();

    my $hash = $st->fetchrow_hashref();
    $st->finish();

    return $hash;

}

sub get_booking_from_id_hash{
    my ($self, $stash, $id) = @_;

    my $st = $stash->prepare_cached(SELECT_BOOKING_FROM_ID);

    $st->bind_param(1, $id);

    $st->execute();

    my $hash = $st->fetchrow_hashref();
    $st->finish();

    return $hash;
}

sub get_booking_from_id_arr{
    my ($self, $stash, $id) = @_;

    my $st = $stash->prepare_cached(SELECT_BOOKING_FROM_ID);
    $st->bind_param(1, $id);
    $st->execute();

    my $arr = $st->fetchrow_arrayref();
    $st->finish();
    return $arr;
}

sub _claim_this_booking_object{
    my ($class, $stash, $ant_nest_code, $booking) = @_;

    my $ticket_id  = $booking->[0];
    my $user       = $booking->[2];
    my $amount     = $booking->[3];

    my $amount_deposited_or_withdrawn;
    my $new_total;

    if ($booking->[1] == 1){
	($amount_deposited_or_withdrawn, $new_total) = Bcd::Data::Bank->deposit_user_account
	    ($stash, $user, $ant_nest_code, $amount);
    } else {
	($amount_deposited_or_withdrawn, $new_total) = Bcd::Data::Bank->withdraw_user_account
	    ($stash, $user, $ant_nest_code, $amount);
    }

    $class->change_state_to_booking
	($stash, $ticket_id, Bcd::Constants::DepositsConstants::COLLECTED);

    return ($amount_deposited_or_withdrawn, $new_total);
}

sub change_state_to_booking{
    my ($class, $stash, $booking_id, $new_status) = @_;

    my $st = $stash->get_statement(CHANGE_STATE_TO_DEPOSIT, $class);

    $st->bind_param(1, $new_status);
    $st->bind_param(2, $booking_id);
    $st->execute();
    
}

sub claim_this_withdrawal_object{
    my ($class, $stash, $ant_nest_code, $booking) = @_;
    $class->_claim_this_booking_object($stash, $ant_nest_code, $booking);
}

sub claim_this_deposit_object{
    my ($class, $stash, $ant_nest_code, $booking) = @_;
    $class->_claim_this_booking_object($stash, $ant_nest_code, $booking);
}

sub claim_this_ticket{
    my ($self, $stash, $ant_nest_code, $ticket_id) = @_;

    #first of all I should get the ticket from the db.
    my $st = $stash->get_statement(SELECT_DEPOSIT_WITH_ID, $self);
    $st->bind_param(1, $ticket_id);

    $st->execute();
    
    my $row = $st->fetchrow_arrayref();
    $st->finish();


    my $is_deposit = $row->[1];
    my $user       = $row->[2];
    my $amount     = $row->[3];


    #this should be given to the bank
    my $amount_deposited_or_withdrawn;
    my $new_total;

    if ( $is_deposit == '1'){
	($amount_deposited_or_withdrawn, $new_total) = Bcd::Data::Bank->deposit_user_account
	    ($stash, $user, $ant_nest_code, $amount);
    } else {
	($amount_deposited_or_withdrawn, $new_total) = Bcd::Data::Bank->withdraw_user_account
	    ($stash, $user, $ant_nest_code, $amount);
    }

    #then I should change the status of the ticket
    $st = $stash->get_statement(CHANGE_STATE_TO_DEPOSIT, $self);

    $st->bind_param(1, Bcd::Constants::DepositsConstants::COLLECTED);
    $st->bind_param(2, $ticket_id);
    $st->execute();

    return ($amount_deposited_or_withdrawn, $new_total);
}

sub populate_the_stash{
    my ($self, $db_stash) = @_;

    my $sql;

    $sql = qq{INSERT into deposits_and_withdrawals(id_user, is_deposit, amount, status, booking_token, }.
	   qq{full_receipt_token, secret_token) }.
	   qq{values (?, 't', ?, } . Bcd::Constants::DepositsConstants::CREATION_STATE . qq{, ?, ?, ?)};
    $db_stash->record_this_statement(INSERT_NEW_DEPOSIT_REQUEST, $sql);

    $sql = qq{UPDATE deposits_and_withdrawals SET status=? where id=?};
    $db_stash->record_this_statement(CHANGE_STATE_TO_DEPOSIT, $sql);

    $sql = qq{SELECT * from deposits_and_withdrawals where id_user=? and is_deposit='1' and booking_token = ?};
    $db_stash->record_this_statement(SELECT_DEPOSIT_USER_WITH_TOKEN, $sql);

    $sql = qq{SELECT * from deposits_and_withdrawals where id= ?};
    $db_stash->record_this_statement(SELECT_DEPOSIT_WITH_ID, $sql);
}


1;
