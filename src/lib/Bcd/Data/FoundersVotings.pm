package Bcd::Data::FoundersVotings;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this module is responsible to create the new ant nests in the system

use strict;
use warnings;

use Data::Dumper;
use Bcd::Constants::FoundersConstants;
use Bcd::Data::StatementsStash;

use constant {
    
    DELETE_ALL_VOTES_FOR_ANT_NEST                      =>
	qq{DELETE from ant_nest_initial_votings WHERE id_ant_nest = ?},

    ADD_A_VOTE                                         =>
    qq{INSERT INTO ant_nest_initial_votings VALUES (?, ?, ?) },

    GET_BOSSES_VOTES                                   =>
    qq{ select distinct(boss_preference), count(boss_preference) }.
    qq{ from ant_nest_initial_votings WHERE id_ant_nest = ? group by boss_preference},

    GET_TREASURERS_VOTES                               =>
    qq{ select distinct(treasurer_preference), count(treasurer_preference) }.
    qq{ from ant_nest_initial_votings WHERE id_ant_nest = ? group by treasurer_preference},


};


sub delete_all_votes_for_this_ant_nest{
    my ($class, $stash, $code) = @_;

    my $st = $stash->prepare_cached(DELETE_ALL_VOTES_FOR_ANT_NEST);
    $st->bind_param(1, $code);
    $st->execute();
}

sub add_a_vote{
    my ($class, $stash, $code, $boss_vote, $treasurer_vote) = @_;

    #the votes could be undef..., they are anonymous
    my $st = $stash->prepare_cached(ADD_A_VOTE);
    $st->bind_param(1, $code);
    $st->bind_param(2, $boss_vote);
    $st->bind_param(3, $treasurer_vote);
    $st->execute();
    
}

sub get_votes_result{
    my ($class, $stash, $code) = @_;

    my $st = $stash->prepare_cached(GET_BOSSES_VOTES);
    $st->bind_param(1, $code);
    $st->execute();

    my $res = {};
    $res->{bosses} = $st->fetchall_arrayref();

    $st = $stash->prepare_cached(GET_TREASURERS_VOTES);
    $st->bind_param(1, $code);
    $st->execute();

    $res->{treasurers} = $st->fetchall_arrayref();

    return $res;
}

1;
