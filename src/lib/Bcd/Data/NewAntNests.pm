package Bcd::Data::NewAntNests;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this module is responsible to create the new ant nests in the system

use strict;
use warnings;

use Bcd::Common::BasicAccounts;
use Bcd::Common::Mail;
use Bcd::Constants::AntNestsConstants;
use Bcd::Constants::NewAntNestsConstants;
use Bcd::Data::Accounting::AccountsPlan;
use Bcd::Data::Trust;
use Data::Dumper;
use Bcd::Constants::FoundersConstants;

use constant{

    UPDATE_ASSIGNED_ID             =>
    qq{UPDATE ant_nests_bookings set assigned_id=? where id=?},	

    UPDATE_ANT_NEST_STATE          =>
    qq{UPDATE ant_nests_bookings set state=?, last_status_change = localtimestamp where id=?},


    SELECT_FOUNDER_DATA_POST_NICK  =>
    qq{SELECT * from ant_nests_founders where id_ant_nest = ? and nick_or_name = ?},

    SELECT_ANT_NEST_FOUNDERS_COUNT =>
	qq{SELECT count(*) from ant_nests_founders where id_ant_nest = ?},

    SELECT_FOUNDERS_COUNT_STATE    =>
    qq{SELECT count(*) from ant_nests_founders where id_ant_nest = ? and id_status = ?},

    SELECT_REACHED_STATE_FOUNDERS_COUNT =>
    qq{SELECT count(*) from ant_nests_founders where id_ant_nest = ? and id_status >= ?},

    SELECT_FOUNDERS_STATE    =>
    qq{SELECT * from ant_nests_founders where id_ant_nest = ? and id_status = ?},
    
    SELECT_FOUNDERS_ANT_NEST =>
    qq{SELECT * from ant_nests_founders where id_ant_nest = ?},

    SELECT_ANT_NEST_LOOKERS_COUNT  =>
    qq{SELECT count(*) from ant_nests_looking_users where id_ant_nest = ?},

    INSERT_NEW_ANT_NEST_BOOKING    =>
    qq{INSERT into ant_nests_bookings(id, state, proposed_name) values(?, }. 
	   Bcd::Constants::NewAntNestsConstants::WAITING_FOR_INITIAL_FOUNDERS. qq{, ?)},

    SELECT_NEW_ANT_NEST_BOOKING    =>
    qq{SELECT * from ant_nests_bookings WHERE id = ?},

    INSERT_NEW_FOUNDER_USER        =>
qq{INSERT into ant_nests_founders(id_status , id_ant_nest, nick_or_name,  password, id_personal_data) } .
qq{ values (} . Bcd::Constants::FoundersConstants::USER_BOOKED_CREATED . qq{, ?, ?, ?, ?)},

    INSERT_NEW_LOOKER              =>
    qq{INSERT into ant_nests_looking_users values(?, ?, ?)},

    SELECT_ALL_LOOKERS             =>
    qq{SELECT * from ant_nests_looking_users where id_ant_nest = ?},

    INSERT_NEW_BOOKED_USER_TOKEN   =>
    qq{INSERT into ant_nests_founders_tokens values(lastval(), ?)},

    DELETE_FOUNDER_TOKEN           =>
    qq{DELETE FROM ant_nests_founders_tokens where id_user = ?},

    SELECT_FOUNDER_TOKEN           =>
    qq{SELECT * from ant_nests_founders_tokens where id_user = ?},
    
    SELECT_ANT_NESTS_SUMMARY       =>
    qq{SELECT id, state, proposed_name FROM ant_nests_bookings WHERE state != }.
    Bcd::Constants::NewAntNestsConstants::CREATED,

    SELECT_FOUNDERS_SUMMARY        =>
	qq{SELECT id_user, nick_or_name, id_status, email FROM ant_nests_founders, personal_users_data } .
	qq{ WHERE id_ant_nest = ? AND ant_nests_founders.id_personal_data = personal_users_data.id},

};

sub get_all_the_observers{
    my ($class, $stash, $ant_nest_id) = @_;

    my $st = $stash->prepare_cached(SELECT_ALL_LOOKERS);
    $st->bind_param(1, $ant_nest_id);
    $st->execute();
    return $st->fetchall_arrayref();
}

sub update_assigned_id{
    my ($class, $stash, $booked_id, $assigned_id) = @_;

    my $st = $stash->prepare_cached(UPDATE_ASSIGNED_ID);
    $st->bind_param(1, $assigned_id);
    $st->bind_param(2, $booked_id);
    $st->execute();
}

sub get_founders_summary{
    my ($class, $stash, $ant_nest_id) = @_;

    #ok, I should get first of all all the founders
    my $st = $stash->prepare_cached(SELECT_FOUNDERS_SUMMARY);
    $st->bind_param(1, $ant_nest_id);
    $st->execute();

    my $var = $st->{NAME_lc};
    my $arr = $st->fetchall_arrayref();

    unshift (@{$arr}, $var);
    return $arr; 
}

sub update_ant_nest_state{
    my ($class, $stash, $ant_nest_id, $state) = @_;

    my $st = $stash->prepare_cached(UPDATE_ANT_NEST_STATE);
    $st->bind_param(1, $state);
    $st->bind_param(2, $ant_nest_id);
    $st->execute();
}

sub get_founders_ant_nest_arr{
    my ($class, $stash, $ant_nest_id) = @_;

    my $st = $stash->prepare_cached(SELECT_FOUNDERS_ANT_NEST);
    $st->bind_param(1, $ant_nest_id);
    $st->execute();

    return $st->fetchall_arrayref();
}

#this function simply returns all the founders...
sub get_confirmed_founders{
    my ($class, $stash, $ant_nest_id) = @_;

    my $st = $stash->prepare_cached(SELECT_FOUNDERS_STATE);
    $st->bind_param(1, $ant_nest_id);
    $st->bind_param(2, Bcd::Constants::FoundersConstants::USER_BOOKED_CONFIRMED);
    $st->execute();

    return $st->fetchall_arrayref();
}

sub get_ant_nests_summary {
    my ($class, $stash) = @_;

    my $st = $stash->prepare_cached(SELECT_ANT_NESTS_SUMMARY);
    $st->execute();
    return $st->fetchall_arrayref();
}

sub login{
    my ($class, $stash, $code, $nick, $password) = @_;

    #I should simply get the founders data
    my $st = $stash->prepare_cached(SELECT_FOUNDER_DATA_POST_NICK);
    $st->bind_param(1, $code);
    $st->bind_param(2, $nick);
    
    $st->execute();

    my $arr = $st->fetchrow_arrayref();
    $st->finish();

    #if the user is not existing return error
    if (! defined($arr) ) {
	return 1;
    } else {

	#the user is existing, is he enabled to login?
	if ($arr->[1] == Bcd::Constants::FoundersConstants::USER_BOOKED_CREATED){
	    return 2;
	}

	if ($password eq $arr->[5]){
	    if ($arr->[1] == Bcd::Constants::FoundersConstants::BOSS_SET_UP_TO_DO){
		return (0, Bcd::Data::Users::ROLE_FOR_A_FOUNDER_BOSS, $arr->[0]); #I am temporary a boss
	    } elsif ($arr->[1] == Bcd::Constants::FoundersConstants::TREASURER_SET_UP_TO_DO){
		return (0, Bcd::Data::Users::ROLE_FOR_A_FOUNDER_TREASURER, $arr->[0]); 
	    } else {
		return (0, Bcd::Data::Users::ROLE_FOR_A_FOUNDER_ANT, $arr->[0]); #ok, I return also my id
	    }
	} else {
	    return 1; #not valid password
	}
    }
}

sub select_new_ant_nest_booking_arr{

    my ($class, $stash, $code) = @_;

    my $st = $stash->prepare_cached(SELECT_NEW_ANT_NEST_BOOKING);
    $st->bind_param(1, $code);

    $st->execute();
    my $arr =  $st->fetchrow_arrayref();
    $st->finish();

    return $arr;
}

sub select_new_ant_nest_booking_hash{
    my ($class, $stash, $code) = @_;

    my $st = $stash->prepare_cached(SELECT_NEW_ANT_NEST_BOOKING);
    $st->bind_param(1, $code);

    $st->execute();
    my $hash =  $st->fetchrow_hashref();
    $st->finish();

    return $hash;
}

sub get_ant_nest_founders_count{
    my ($class, $stash, $code) = @_;

    my $st = $stash->prepare_cached(SELECT_ANT_NEST_FOUNDERS_COUNT);

    $st->bind_param(1, $code);
    $st->execute();

    my $count = $st->fetch()->[0];
    
    $st->finish();
    return $count;
}

sub get_founders_with_state_count{
    my ($class, $stash, $code, $state) = @_;

    my $st = $stash->prepare_cached(SELECT_FOUNDERS_COUNT_STATE);
    $st->bind_param(1, $code);
    $st->bind_param(2, $state);

    $st->execute();

    my $count = $st->fetch()->[0];

    $st->finish();
    return $count;
}

sub get_founders_with_personal_data_count{
    my ($class, $stash, $code) = @_;

    my $st = $stash->prepare_cached(SELECT_FOUNDERS_COUNT_STATE);
    $st->bind_param(1, $code);
    $st->bind_param(2, Bcd::Constants::FoundersConstants::PERSONAL_DATA_IMMITTED);

    $st->execute();

    my $count = $st->fetch()->[0];

    $st->finish();
    return $count;
}

sub get_confirmed_founders_count{
    my ($class, $stash, $code) = @_;

    my $row = $stash->select_one_row_arr
	(SELECT_REACHED_STATE_FOUNDERS_COUNT, $code, 
	 Bcd::Constants::FoundersConstants::USER_BOOKED_CONFIRMED);

    return $row->[0];
}

sub get_ant_nest_lookers_count{
    my ($class, $stash, $code) = @_;

    my $st = $stash->prepare_cached(SELECT_ANT_NEST_LOOKERS_COUNT);

    $st->bind_param(1, $code);
    $st->execute();

    my $count = $st->fetch()->[0];
    
    $st->finish();
    return $count;
}

sub create_new_ant_nest_booking{
    my ($class, $stash, $code, $name_ant_nest) = @_;

    my $st = $stash->prepare_cached(INSERT_NEW_ANT_NEST_BOOKING);

    $st->bind_param(1, $code);
    $st->bind_param(2, $name_ant_nest);

    $st->execute();
    
}

sub insert_a_new_founder{
    my ($class, $stash, $code, $name, $email, $password) = @_;

    #I should insert the personal data
    my $personal_data_id = Bcd::Data::Users->create_personal_data_for_user
	($stash, 
	 {
	     email => $email
	 });

    my $st = $stash->prepare_cached(INSERT_NEW_FOUNDER_USER);

    $st->bind_param(1, $code);
    $st->bind_param(2, $name);
    $st->bind_param(3, $password);
    $st->bind_param(4, $personal_data_id);

    $st->execute();


    #the founding users are those which can login 
    #then I should insert the token...
    my $token = Bcd::Data::Token::generate_new_token_without_spaces();

    $st = $stash->prepare_cached(INSERT_NEW_BOOKED_USER_TOKEN);
    $st->bind_param(1, $token);
    $st->execute();
    return $token;
}

sub new_founder_confirmed{
    my ($class, $stash, $id_user) = @_;

    Bcd::Data::Founders->update_founder_state
	($stash, $id_user, Bcd::Constants::FoundersConstants::USER_BOOKED_CONFIRMED);

    #ok, now I should also delete the token
    my $st = $stash->prepare_cached(DELETE_FOUNDER_TOKEN);
    $st->bind_param(1, $id_user);

    $st->execute();
}





#this is a generic function used to get the token assigned to a
#particular user
sub get_founder_token_hash{
    my ($class, $stash, $id_user) = @_;

    my $st = $stash->prepare_cached(SELECT_FOUNDER_TOKEN);

    $st->bind_param(1, $id_user);
    $st->execute();

    my $hash = $st->fetchrow_hashref();
    $st->finish();

    return $hash;
}

sub insert_a_new_looker{
    my ($class, $stash, $code, $name, $email) = @_;

    #no password, the looker is not enabled to login
    my $st = $stash->prepare_cached(INSERT_NEW_LOOKER);
    $st->bind_param(1, $code);
    $st->bind_param(2, $name);
    $st->bind_param(3, $email);
    $st->execute();

    #No token
}







sub select_new_founder_hash_post_nick {
    my ($class, $stash, $post_code, $nick) = @_;

    my $st = $stash->prepare_cached(SELECT_FOUNDER_DATA_POST_NICK);
    $st->bind_param(1, $post_code);
    $st->bind_param(2, $nick);
   
    $st->execute();

    my $hash = $st->fetchrow_hashref();
    $st->finish();
    return $hash;
}


sub init_db{

    my ($self, $stash) = @_;
    my $conn = $stash->get_connection();

    my $sth = $conn->prepare(qq{insert into ant_nests_bookings_statuses values(?,?)});

    foreach (Bcd::Constants::NewAntNestsConstants::LIST_NEW_ANT_NEST_STATES){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }

    $sth->finish();

}

1;
