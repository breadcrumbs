package Bcd::Data::Token;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU. General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#this file should simply implement the token mechanism in the Bcd

use constant{
    TOKEN_LENGTH => 9,
    TOKEN_CHUNK_LENGTH => 3, #This should be a perfect divisor of TOKEN_LENGTH.
};

sub is_valid_token{
    my $token = shift;

    #this regular expression should be changed if the token length changes
    if ($token =~ /^
		  ([A-Za-z0-9]{3}
		   [ ]){2}
		  [A-Za-z0-9]{3}$
	/x){
	return 1;
    } else {
	return 0;
    }
}



#this function simply generates a new token.
sub generate_new_token{
    my @set = ('a'..'z','A'..'Z', 0..9);

    my $token = "";
    foreach(1..TOKEN_LENGTH){
	$token .= @set[rand($#set+1)];
	if (($_ % TOKEN_CHUNK_LENGTH) == 0){
	    $token .= " ";
	}
    }
    chop $token; #the last space is irrelevant
    return $token;
}

sub generate_new_token_without_spaces{
    my @set = ('a'..'z','A'..'Z', 0..9);

    my $token = "";
    foreach(1..TOKEN_LENGTH){
	$token .= @set[rand($#set+1)];
    }

    return $token;
}

#this function simply obtains the secret from the full and the blinded
sub unblind_the_token{
    my ($full, $blinded) = @_;

    my $secret_chars = 0;
    my $secret = "";

    for(0..length($full)-1){
	my $char         = substr($full, $_, 1);
	my $blinded_char = substr($blinded, $_, 1);

	if ($blinded_char eq "#"){
	    $secret .= $char;
	    $secret_chars ++;
	    if ($secret_chars % TOKEN_CHUNK_LENGTH == 0){
		$secret .= " ";
	    }
	}
    }

    chop $secret;

    return $secret;
}

#this function generates the long token with the mask
#it returns the complete token and the blinded token
sub generate_long_token{

    #first of all I should generate a long token, with double lenght

    my $token = generate_new_token();
    $token .= " ";
    $token .= generate_new_token();

    #Ok, now I have a very long token.
    my $chars_remaining = TOKEN_LENGTH;
    my $chars_clear = 0;
    my $chars_blinded = 0;

    my $blinded_token = "";
    my $secret_token  = "";

    for (0..length($token)-1){
	my $char = substr($token, $_, 1);
	
	if ($char eq " "){
	    $blinded_token .= $char;
	    next;
	}

	#ok, now I should decide to blind or not to blind this char
	if ((rand() > 0.5 && $chars_remaining > 0) or
	    ($chars_clear >= TOKEN_LENGTH)){
	    #I blind it...
	    $chars_remaining--;
	    $blinded_token .= "#";
	    $secret_token  .= $char;
	    $chars_blinded ++;
	    if ($chars_blinded % TOKEN_CHUNK_LENGTH == 0){
		$secret_token .= " ";
	    }
	    next;
	}

	#I copy it
	$blinded_token .= $char;
	$chars_clear ++;
	
    }

    chop $secret_token;

    return ($token, $blinded_token, $secret_token);
}

1;

