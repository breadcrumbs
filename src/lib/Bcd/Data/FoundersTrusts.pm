package Bcd::Data::FoundersTrusts;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this module is responsible to create the new ant nests in the system

use strict;
use warnings;
use Data::Dumper;

use constant{

    SELECT_DIRECT_FOUNDER_TRUST_FIRST                 =>
	qq{select trust_u1_u2 as trust FROM ant_nests_founders_trusts WHERE u1 = ? AND u2 = ?},

    UPDATE_SECOND_FOUNDER_TRUST                       =>
    qq{UPDATE ant_nests_founders_trusts SET trust_u2_u1 = ? WHERE u1 = ? AND u2 = ?},

    INSERT_FIRST_FOUNDER_TRUST                        =>
    qq{INSERT INTO ant_nests_founders_trusts(u1,u2,trust_u1_u2) VALUES(?,?,?)},

    SELECT_ALL_TRUSTS_USER                            =>
    qq{SELECT * FROM ant_nests_founders_trusts WHERE u1 = ?},


};

sub get_all_trusts_for_this_user_arr{
    my ($class, $stash, $user) = @_;
    
    my $st = $stash->prepare_cached(SELECT_ALL_TRUSTS_USER);
    $st->bind_param(1, $user);
    $st->execute();

    return $st->fetchall_arrayref();
}

#ok, the main function is the function that adds a trust.
sub add_founder_trust{
    my ($class, $stash, $user1, $user2, $trust_1_2) = @_;

    #first of all I see if there is a direct trust between 2 and 1.
    my $st = $stash->prepare_cached(SELECT_DIRECT_FOUNDER_TRUST_FIRST);
    $st->bind_param(1, $user2);
    $st->bind_param(2, $user1);

    $st->execute();
    my $row = $st->fetchrow_arrayref();
    $st->finish();
    
    if (defined($row)){
	#ok, I should simply update this row...
	$st = $stash->prepare_cached(UPDATE_SECOND_FOUNDER_TRUST);
	$st->bind_param(1, $trust_1_2);
	$st->bind_param(2, $user2);
	$st->bind_param(3, $user1);
	$st->execute();
    } else {
	#there is not a row, I should insert a row with this user as the first
	#in the relationship
	$st = $stash->prepare_cached(INSERT_FIRST_FOUNDER_TRUST);
	$st->bind_param(1, $user1);
	$st->bind_param(2, $user2);
	$st->bind_param(3, $trust_1_2);
	$st->execute();
    }
    #ok, all done.
}


1;
