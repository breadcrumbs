package Bcd::Data::Model;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this file should be the main container of the breadcrumbs model.
#this is the other singleton in the application (the other is the
#manager of the listeners)

use strict;
use warnings;

use Bcd::Commands::CommandFactory;
use Data::Dumper;

my $myself = undef;

sub instance(){
    if (! defined ($myself)){
	#ok, I should create a model
	my $model = Bcd::Data::Model->_new_instance();
	$myself = $model;
    }

    return $myself;
}

sub _new_instance{

    my $class = shift;

    my $self = {};
    $self->{id} = 39;

    my $factory = Bcd::Commands::CommandFactory->new();
    $self->{factory} = $factory;
    bless ($self, $class);

    return $self;
}

sub get_id{
    my $self = shift;
    return $myself->{id} ++;
}

sub get_command{
    my ($self, $command, $in, $out) = @_;
    return $self->get_factory()->get_command($command, $in, $out);
}

sub get_factory{
    my $self = shift;
    return $self->{factory};
}

1;
