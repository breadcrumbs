package Bcd::Data::CreditsAndDebits;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Data::Users;
use Data::Dumper;

use constant PI                                => 4 * atan2(1, 1);
use constant CREDIT_SCORE_COEFF                => - 200 / PI;

use constant {
    MINIMUM_DEBIT_CREDIT                       => 50_000,

    GET_TOTAL_ANT_CREDIT                       =>
	qq{SELECT sum(amount) as sum FROM credits_and_debits WHERE id_creditor = ?},

    GET_TOTAL_ANT_DEBIT                        =>
	qq{SELECT sum(amount) as sum FROM credits_and_debits WHERE id_debitor = ?},

    GET_ANT_FROZEN_DEBITS                      =>
    qq{SELECT amount FROM frozen_credits WHERE id_debitor = ? },

    GET_POSITION_BETWEEN_TWO_ANTS              =>
    qq{SELECT amount FROM credits_and_debits WHERE id_creditor = ? AND id_debitor = ?},

    CREATE_POSITION_BETWEEN_TWO_ANTS           =>
    qq{INSERT INTO credits_and_debits(id_creditor, id_debitor, amount) VALUES (?,?,?)},

    UPDATE_POSITION_BETWEEN_TWO_ANTS           =>
    qq{UPDATE credits_and_debits SET amount=? WHERE id_creditor = ? AND id_debitor = ?},

    DELETE_POSITION_BETWEEN_TWO_ANTS           =>
    qq{DELETE FROM credits_and_debits WHERE id_creditor = ? AND id_debitor = ?},

    INSERT_FROZEN_CREDIT                       =>
    qq{INSERT into frozen_credits(id_debitor, amount) VALUES(?,?)},

    ADD_FROZEN_CREDIT                          =>
    qq{UPDATE frozen_credits SET amount = amount + ? WHERE id_debitor = ?},
    
    SUBTRACT_FROZEN_CREDIT                     =>
    qq{UPDATE frozen_credits SET amount = amount - ? WHERE id_debitor = ?},

};

sub ant_freezes_credit{
    my ($class, $stash, $buyer, $amount) = @_;

    my $row = $stash->select_one_row_arr(GET_ANT_FROZEN_DEBITS, $buyer);

    if (!defined($row)){
	#ok, there is not a frozen credit, create one
	$stash->prep_exec(INSERT_FROZEN_CREDIT, $buyer, $amount);
    } else {
	#I should simply update
	$stash->prep_exec(ADD_FROZEN_CREDIT, $amount, $buyer);
    }
}

#this is a pure function... I have a price estimate for a payment,
#with all the details
sub get_price_estimate_report_for_a_payment{
    my ($class, $stash, $buyer, $seller, $price, $can_use_credit) = @_;

    #ok, first of all I check if I have some credit towards the seller.

    my ($ans, $amount) = $class->get_net_position_between_two_ants
	($stash, $buyer, $seller);

    my $credits_towards_seller = 0;
    my $cheque_amount = 0;
    my $cheque_price = 0;
    my $convertible_tao = 0;
    my $total_price = 0;
    my $can_be_payed_now = 0;


    if ($ans != 0){
	if ($amount > 0){
	    #ok, I have a credit towards this ant... is it sufficient?
	    if ($amount >= $price){
		#yes it is, so I pay only with my credits.
		$credits_towards_seller = $price;
		$price = 0; #I have completed the payment
	    } else {
		#no, it is not, so I can pay only a part...
		$credits_towards_seller = $amount;
		$price -= $amount;
	    }
	}
    }


    #ok, now I should see if I can emit cheque..., 
    #if there is something left to pay...
    if ($price != 0){
	if ($can_use_credit == 1){

	    #ok, let's see the maximum cheque that the user can emit...
	    my $maximum_cheque = Bcd::Data::Bank->get_ant_maximum_cheque_considering_score
		($stash, $buyer);

	    if ($maximum_cheque >= $price){
		#ok, I can pay the rest with a cheque.
		$cheque_amount = $price;
		$price = 0;
	    } else {
		#no, it is not sufficient
		$cheque_amount = $maximum_cheque;
		$price -= $maximum_cheque;
	    }
	    $cheque_price = Bcd::Data::Bank->get_price_for_this_cheque($cheque_amount);
	    $total_price += $cheque_price;
	}
    }

    #is there something left to pay?
    if ($price != 0){
	#ok, the rest is payed with "normal" taos
	$convertible_tao = $price;
	$total_price += $price;
    }

    #ok, now the user can withdraw from his account the total price
    if (Bcd::Data::Bank->can_this_user_withdraw_tao($stash, $buyer, $total_price)){
	$can_be_payed_now = 1;
    }
 
    return 
	(
	 $credits_towards_seller, 
	 $cheque_amount, 
	 $cheque_price, 
	 $convertible_tao, 
	 $total_price,
	 $can_be_payed_now);
}

sub get_price_estimate_for_a_payment{
}

sub get_credit_reports_towards_ants{
    my ($class, $stash, $user_id, $post_code) = @_;

    my $ants = Bcd::Data::Users->get_all_ants_in_this_ant_nest
	($stash, $post_code);

    
    my @report;

    foreach(@{$ants}){
	#the relationship towards myself is not so important..
	next if $_ == $user_id;

	my @row;

	#ok, then I get the situation with this ant
	my ($ans, $amount) = $class->get_net_position_between_two_ants
	    ($stash, $user_id, $_->[0]);

	my $nick = Bcd::Data::Users->get_nick_from_id($stash, $_->[0]);

	if ($ans == 0){
	    @row = ($_->[0], $nick, 0);
	} else {
	    @row = ($_->[0], $nick, $amount);
	}

	push(@report, \@row);

    }

    my @fields = ('id', 'nick', 'position');
    unshift (@report, \@fields);
    
    return \@report;
}

sub ant_thaws_credit{
    my ($class, $stash, $creditor, $debitor, $amount_to_credit) = @_;

    #first of all I should delete a frozen credit
    $stash->prep_exec(SUBTRACT_FROZEN_CREDIT, $amount_to_credit, $debitor);

    #then I ask the handling of the credit function
    $class->ant_credits_ant($stash, $creditor, $debitor, $amount_to_credit);
}

#this function simply manages the amount, which cannot be negative.
#in this case it simply creates another row... with the integers swapped
sub ant_credits_ant{
    my ($class, $stash, $creditor, $debitor, $amount_to_credit) = @_;

    my ($res, $amount) = $class->_get_amount_between_two_ants($stash, $creditor, $debitor);

    if ($res == 1){
	#ok, there is an amount, so the creditor will be more creditor
	#print ">>>>>>>>>>>>>>>>>> ok creditor $creditor has this credit: $amount\n";
	$amount += $amount_to_credit;
	$stash->prep_exec(UPDATE_POSITION_BETWEEN_TWO_ANTS, $amount, $creditor, $debitor);
    } else {
	#print ">>>>>>>>>>>>>>>>>> ko, $creditor is NOT a creditor...\n";
	#mm, is this creditor a debitor now? I invert the roles
	($res, $amount) = $class->_get_amount_between_two_ants($stash, $debitor, $creditor);
	
	if ($res == 1){
	    #print "]]]]]]]]]] creditor $creditor actually is a debitor of amount $amount\n";
	    #ok, this one was a debitor, it will become less debitor... let's see if
	    #he will become a creditor.
	    $amount -= $amount_to_credit;
	    if ($amount >= 0){
		#no swap needed, only an update, but the roles are swapped!!!!!
		#print "]]]]]]]]]] The debit is still here, The updated debit is now $amount\n";
		$stash->prep_exec(UPDATE_POSITION_BETWEEN_TWO_ANTS, $amount, $debitor, $creditor);
	    } else {
		#ok, I must swap the roles, the creditor becomes a debitor
		$amount *= -1;
		$stash->prep_exec(DELETE_POSITION_BETWEEN_TWO_ANTS, $debitor, $creditor);
		
		#and I create a new row
		$stash->prep_exec(CREATE_POSITION_BETWEEN_TWO_ANTS, $creditor, $debitor, $amount);
	    }

	} else {
	    #print "..... I should create a relationship cred $creditor, deb $debitor am $amount_to_credit\n";
	    #no... this is the first time of the relationship... I can directly insert a new row.
	    $stash->prep_exec(CREATE_POSITION_BETWEEN_TWO_ANTS, $creditor, $debitor, $amount_to_credit);
	}
    }

    
}

sub get_net_position_between_two_ants{
    my ($class, $stash, $ant1, $ant2) = @_;

    my ($res, $amount) = $class->_get_amount_between_two_ants($stash, $ant1, $ant2);

    if ($res == 1){
	#ok, ant1 is actually a creditor of ant2
	return (1, $amount);
    } else {
	($res, $amount) = $class->_get_amount_between_two_ants($stash, $ant2, $ant1);

	if ($res == 1){
	    #ok, ant1 is actually a debitor of ant2
	    return (1, $amount * -1);
	} else {
	    #no position recorded
	    return (0, undef);
	}

    }


}

sub _get_amount_between_two_ants{
    my ($class, $stash, $creditor, $debitor) = @_;

    my $row = $stash->select_one_row_arr(GET_POSITION_BETWEEN_TWO_ANTS, $creditor, $debitor);
    return (defined($row)) ? (1, $row->[0]) : (0, undef);
}

#this method should simply get the net credit of an ant
#that is the sum (credits - debits - frozen debits)
sub get_net_ant_credit{
    my ($class, $stash, $id_user) = @_;

    my $credit = $class->get_ant_credit($stash, $id_user);
    my $debit = $class->get_ant_debit($stash, $id_user);
    my $frozen_debit = $class->get_ant_frozen_credit($stash, $id_user);

    return ($credit - $debit - $frozen_debit);
}

sub get_ant_credit{
    my ($class, $stash, $id_user) = @_;
    return $class->_get_ant_debit_credit($stash, GET_TOTAL_ANT_CREDIT, $id_user);
}

sub get_ant_debit{
    my ($class, $stash, $id_user) = @_;
    return $class->_get_ant_debit_credit($stash, GET_TOTAL_ANT_DEBIT, $id_user);
}

sub get_ant_frozen_credit{
    my ($class, $stash, $id_user) = @_;

    my $row = $stash->select_one_row_arr(GET_ANT_FROZEN_DEBITS, $id_user);
    my $frozen_debit = (defined($row)) ? $row->[0] : 0;
}

sub _get_ant_debit_credit{
    my ($class, $stash, $sql, $id_user) = @_;

    my $row = $stash->select_one_row_arr($sql, $id_user);
    my $credit_or_debit = (defined($row->[0])) ? $row->[0] : 0;
    return $credit_or_debit;
}



sub get_ant_credit_score{
    my ($class, $stash, $id_user) = @_;
    my $credit = $class->get_ant_credit($stash, $id_user);
    my $debit = $class->get_ant_debit($stash, $id_user);
    my $frozen_debit = $class->get_ant_frozen_credit($stash, $id_user);

    return credit_score($credit, $debit + $frozen_debit);
}

sub get_ant_credit_summary{
    my ($class, $stash, $id_user) = @_;

    my $credit = $class->get_ant_credit($stash, $id_user);
    my $debit = $class->get_ant_debit($stash, $id_user);
    my $frozen_debit = $class->get_ant_frozen_credit($stash, $id_user);
    my $score = credit_score($credit, $debit + $frozen_debit);

    return ($credit, $debit, $frozen_debit, $score);
}

sub get_ant_credit_score_if_it_emits_this_cheque{
    my ($class, $stash, $id_user, $amount_cheque) = @_;

    my $credit = $class->get_ant_credit($stash, $id_user);
    my $debit = $class->get_ant_debit($stash, $id_user);
    my $frozen_debit = $class->get_ant_frozen_credit($stash, $id_user);
    $debit += $amount_cheque;
    $debit += $frozen_debit;
    return credit_score($credit, $debit);
}

sub get_maximum_debt_to_reach_this_rate{
    my ($class, $stash, $id_user, $rate) = @_;

    my $total_credit = Bcd::Data::CreditsAndDebits->get_ant_credit($stash, $id_user);
    my $total_debit  = Bcd::Data::CreditsAndDebits->get_ant_debit ($stash, $id_user);
    my $frozen_debit = $class->get_ant_frozen_credit($stash, $id_user);
    $total_debit += $frozen_debit;

    #mmm if the total debit and total credit are below the minimum
    #then I can reach the minimum
    if ($total_credit == 0 ){
	#mm this ant has no credit, let's get the debit
	if ($total_debit < MINIMUM_DEBIT_CREDIT){
	    #ok, you can reach the MINIMUM_DEBIT_CREDIT
	    return (MINIMUM_DEBIT_CREDIT - $total_debit);
	} else {
	    #too much debit... strange... it should not happen, in any case
	    #the answer is zero
	    return 0;
	}
    }

    #ok, you have some credit.
    my $inside_window = 0;
    my $maximum_debit_in_the_window;
    if (
	($total_credit < MINIMUM_DEBIT_CREDIT )
	and
	($total_debit  < MINIMUM_DEBIT_CREDIT)
	){
	#ok, you are inside the initial window...
	$maximum_debit_in_the_window = MINIMUM_DEBIT_CREDIT - $total_debit;
	$inside_window = 1;
    }

    my $debit_lim = $rate * $total_credit;
    $debit_lim -= $total_debit;

    if ($inside_window == 1){
	if ($debit_lim < $maximum_debit_in_the_window){
	    $debit_lim = $maximum_debit_in_the_window;
	}
    }

    return $debit_lim;
}

#this is the function for the credit score... It is a pure function,
#no side effects.
#precondition $credit >= 0, $debit >= 0
sub credit_score{
    my ($credit, $debit) = @_;

    if (
	($credit < MINIMUM_DEBIT_CREDIT)
	and
	($debit < MINIMUM_DEBIT_CREDIT)
	){
	#the score has a certain hysteresis
	return 100;
    }

    #now... if one of them is zero, the credit score is undefined, I define it
    #using the continuity of the function or its limit
    if ($credit == 0){
	return -100;
    }

    if ($debit == 0){
	return 100;
    }

    #ok, I can compute the formula
    my $rate = $debit / $credit;

    my $arg = ($rate-1)/sqrt($rate);
    my $score = CREDIT_SCORE_COEFF * atan2($arg, 1);

    return $score;
}




1;
