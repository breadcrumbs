package Bcd::Data::Configuration;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Constants::ConfigurationKeys;

#this module is responsible to make the configuration

use constant{

    INSERT_ANT_NEST_VALUE                        =>
	qq{INSERT INTO ant_nests_configuration VALUES (?,?,?)},

	UPDATE_ANT_NEST_VALUE                    =>
	qq{UPDATE ant_nests_configuration SET value = ? where id_ant_nest = ? AND key_id = ?},

	GET_ANT_NEST_VALUE                       =>
	qq{SELECT value from ant_nests_configuration WHERE id_ant_nest = ? AND key_id = ?},

	GET_KEY_DEFAULT_VALUE                    =>
	qq{SELECT default_value FROM configuration_keys WHERE id = ?},
};

sub insert_ant_nest_value{
    my ($class, $stash, $code, $key_id, $value) = @_;

    #first of all I should check that there is not already a value for the given key

    my $st = $stash->prepare_cached(GET_ANT_NEST_VALUE);
    $st->bind_param(1, $code);
    $st->bind_param(2, $key_id);

    $st->execute();
    my $row = $st->fetch();

    $st->finish();

    if (!defined($row)){
	$st = $stash->prepare_cached(INSERT_ANT_NEST_VALUE);
	$st->bind_param(1, $code);
	$st->bind_param(2, $key_id);
	$st->bind_param(3, $value);
	$st->execute();
    } else {
	#only an update needed.
	$st = $stash->prepare_cached(UPDATE_ANT_NEST_VALUE);
	$st->bind_param(1, $value);
	$st->bind_param(2, $code);
	$st->bind_param(3, $key_id);
	$st->execute();
    }

}

sub get_key_default_value{
    my ($class, $stash, $key_id) = @_;

    my $st = $stash->prepare_cached(GET_KEY_DEFAULT_VALUE);
    $st->bind_param(1, $key_id);
    $st->execute();

    my $row = $st->fetch();
    $st->finish();

    return $row->[0];
}

#this function returns the value specific to this ant nest, otherwise
#the default
sub get_ant_nest_value{
    my ($class, $stash, $code, $key_id) = @_;

    my $st = $stash->prepare_cached(GET_ANT_NEST_VALUE);
    $st->bind_param(1, $code);
    $st->bind_param(2, $key_id);

    $st->execute();
    my $row = $st->fetch();

    $st->finish();

    if (!defined($row)){

	#ok, I take the default
	$st = $stash->prepare_cached(GET_KEY_DEFAULT_VALUE);
	$st->bind_param(1, $key_id);
	$st->execute();

	$row = $st->fetch();
	$st->finish();
	
    }

    return $row->[0];
}

sub init_db{
    my ($self, $stash) = @_;
    my $conn = $stash->get_connection();

    #ok, I put the keys in the db...

    #ok, now I should insert the values in the table...
    my $sth = $conn->prepare(qq{insert into configuration_keys values(?,?,?)});

    foreach (Bcd::Constants::ConfigurationKeys::LIST_KEYS){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->bind_param(3, $_->[2]);
	$sth->execute();
    }

    $sth->finish();

}




1;
