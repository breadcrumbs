package Bcd::Data::TrustsNet;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Data::Trust;

use constant {
    SELECT_USER_NETS                       =>
	qq{SELECT id_user, id_net, refresh_count, IS_LOCKED  FROM TRUST_NETS_INDEX},

	OUTDATE_USER_NETS                       =>
	qq{UPDATE trust_nets_index SET refresh_count = refresh_count + 1 WHERE id_user = ?},

	UPDATE_USER_NETS                       =>
	qq{UPDATE trust_nets_index SET refresh_count = refresh_count - 1, id_net = ? WHERE id_user = ?},

	SELECT_USER_NET                        =>
	qq{SELECT id_net, refresh_count, is_locked FROM trust_nets_index WHERE id_user = ?},

	INSERT_NETS_FOR_USER                   =>
	qq{INSERT INTO trust_nets_index(id_user, id_net, refresh_count, is_locked) VALUES (?,?,?,?)},

	DELETE_NETS_FOR_USER                   =>
	qq{DELETE from trust_nets_index WHERE id_user = ? },

	INSERT_INTO_TRUST_NET                  =>
	qq{INSERT INTO trusts_cache(u1,un,is_locked,trust_u1_un,path) VALUES(?,?,?,?,?)},

	SELECT_TRUST_NET_ROW                   =>
	qq{SELECT is_locked, trust_u1_un, path FROM trusts_cache WHERE u1 = ? AND un = ?},

	UPDATE_TRUST_NET_ROW                   =>
	qq{UPDATE trusts_cache SET trust_u1_un = ?, path = ? WHERE u1 = ? AND un = ? },


	DELETE_OUTGOING_TRUSTS_NETS                 =>
	qq{DELETE FROM trusts_cache WHERE is_locked = '0' AND u1 IN (%s) AND un NOT IN (%s)},

	DELETE_INCOMING_TRUSTS_NETS                 =>
	qq{DELETE FROM trusts_cache WHERE is_locked = '0' AND u1 NOT IN (%s) AND un IN (%s)},

	DELETE_TRUSTS_NETS_UNLIMITED       =>
	qq{DELETE FROM trusts_cache WHERE is_locked = '0' AND (u1 IN (%s) or un IN (%s))},

    };


sub update_nets_for_user{
    my ($class, $stash, $user) = @_;

    #ok, I should update the nets, first of all I calculate them
    my $outgoing_trust_net = Bcd::Data::Trust->get_outgoing_trust_net($stash, $user);

    #ok, let's put this net in db
    $class->_write_net_in_db($stash, $user, $outgoing_trust_net, 1, 1);

    #then the incoming
    my $incoming_trust_net = Bcd::Data::Trust->get_incoming_trust_net($stash, $user);
    $class->_write_net_in_db($stash, $user, $incoming_trust_net, 0, 1);

    #ok, now let's put them in cache, just to make them available to the outside world...
    my $user_net = $class->_compile_net_for_user($stash, $user, $outgoing_trust_net, $incoming_trust_net);

    #ok, then I set that the net in db is calculated...
    my $st = $stash->prepare_cached(UPDATE_USER_NETS);
    my $time = time();
    $st->bind_param(1, $time);
    $st->bind_param(2, $user);
    
    $st->execute();

    #I store them in cache...
    my $key = "net_$user";
    my $value = {
	time => $time,
	net => $user_net,
    };

    $stash->get_cache()->set($key, $value);


    #I return the user net complete...
    return $user_net;
    
}


sub outdate_net_for_ant_nest{
    my ($class, $stash, $post_code) = @_;

    #I should get all the nets which are in the same ant nest of this
    #user and outdate them all...

    my $st = $stash->prepare_cached(SELECT_USER_NETS);
    $st->execute();
    my $arr = $st->fetchall_arrayref();

    my @users_to_outdate;
    foreach(@{$arr}){
	my $user = $_->[0];

	#get the post code of this user
	my $user_data = Bcd::Data::Users->get_user_base_data_arr($stash, $user);

	if ($user_data->[1] == $post_code){
	    push(@users_to_outdate, $user);
	}
    }

    #ok, now I outdate all this users....
    $st = $stash->prepare_cached(OUTDATE_USER_NETS);
    
    foreach(@users_to_outdate){
	$st->bind_param(1, $_);
	$st->execute();
    }

}

#this is only a helper module which can ask for a ingoing or outgoing
#net and then puts the result in the cache

sub get_user_net{
    my ($class, $stash, $user) = @_;

    my $st = $stash->prepare_cached(SELECT_USER_NET);
    $st->bind_param(1, $user);
    $st->execute();
    my $row = $st->fetch();
    $st->finish();
    return $row;
}

sub get_trust_data_arr{
    my ($class, $stash, $u1, $un) = @_;

    my $st = $stash->prepare_cached(SELECT_TRUST_NET_ROW);
    $st->bind_param(1, $u1);
    $st->bind_param(2, $un);
    $st->execute();
    my $arr = $st->fetch();
    $st->finish();
    return $arr;
}

sub get_nets_user_set{
    my ($class, $stash) = @_;

    my $st = $stash->prepare_cached(SELECT_USER_NETS);
    $st->execute();
    my $set = $st->fetchall_hashref("id_user");
    return $set;
}

###mainly a bookkeeping function... used for tests and other things
sub get_nets_for_user{
    my ($class, $stash, $user) = @_;

    my $key = "net_$user";
    my $value = $stash->get_cache()->get($key);

    if (!defined($value)){
	#the default is to create them unlocked.
	return $class->create_nets_for_user($stash, $user, 0);
    } else {
	return $value->{net};
    }
    
}


sub create_nets_for_user{
    my ($class, $stash, $user, $locked) = @_;
    
    #the locked parameters is used whenever I compute the nets for a
    #daemon which does not really make a login
    my $st = $stash->prepare_cached(INSERT_NETS_FOR_USER);
    my $time = time();
    $st->bind_param(1, $user);
    $st->bind_param(2, $time);
    $st->bind_param(3, 0);
    $st->bind_param(4, $locked);
    $st->execute();

    #well, I should really take the two nets for this user...
    my $outgoing_trust_net = Bcd::Data::Trust->get_outgoing_trust_net($stash, $user);

    #ok, let's put this net in db
    $class->_write_net_in_db($stash, $user, $outgoing_trust_net, 1, 0);

    #then the incoming
    my $incoming_trust_net = Bcd::Data::Trust->get_incoming_trust_net($stash, $user);
    $class->_write_net_in_db($stash, $user, $incoming_trust_net, 0, 0);

    #ok, now let's put them in cache, just to make them available to the outside world...
    my $user_net = $class->_compile_net_for_user($stash, $user, $outgoing_trust_net, $incoming_trust_net);

    #I store them in cache...
    my $key = "net_$user";
    my $value = {
	time => $time,
	net => $user_net,
    };

    $stash->get_cache()->set($key, $value);

    #I return the user net complete...
    return $user_net;
}

sub _compile_net_for_user{
    my ($class, $stash, $user, $outgoing, $incoming) = @_;

    my %user_net;

    #I put in the same net the outgoing and the incoming
    foreach(keys(%{$outgoing})){
	#the direct path must be symmetric.
	#if there is a direct outgoing path there MUST be a direct incoming path.

	my $direct = 0;
	if (scalar(@{$outgoing->{$_}->[0]} == 2)){
	    $direct = 1;
	}

	$user_net{$_} = [$outgoing->{$_}->[2], $incoming->{$_}->[2], $direct];
    }

    return \%user_net;
}

sub _write_net_in_db{
    my ($class, $stash, $user, $net, $outgoing, $overwrite) = @_;

    #ok, I should make a cycle for all the keys in the net
    my $st = $stash->prepare_cached(INSERT_INTO_TRUST_NET);
    my $st_update = $stash->prepare_cached(UPDATE_TRUST_NET_ROW);
    my $u1;
    my $un;
    foreach(keys(%{$net})){
	if ($outgoing == 1){
	    $u1 = $user;
	    $un = $_
	} else {
	    $u1 = $_;
	    $un = $user;
	}

	my $path = join (" | ", @{$net->{$_}->[0]});

	#ok, now I should see if the row exists already
	my $row = $class->get_trust_data_arr($stash, $u1, $un);
	if (defined($row) and $overwrite == 0){
	    next;
	} elsif (defined($row) and $overwrite == 1) {
	    #ok, I must overwrite the preceding trust, IF it is NOT LOCKED
	    next if ($row->[0] == 1);

	    #ok, It is not locked! update it
	    $st_update->bind_param(1, $net->{$_}->[2]);
	    $st_update->bind_param(2, $path);
	    $st_update->bind_param(3, $u1);
	    $st_update->bind_param(4, $un);
	    $st_update->execute();

	    next;
	}

	$st->bind_param(1, $u1);
	$st->bind_param(2, $un);
	$st->bind_param(3, 0);
	$st->bind_param(4, $net->{$_}->[2]);

	#the path is stored as an array, I join it

	$st->bind_param(5, $path);

	$st->execute();
    }
}

sub unlock_nets_for_user{
}


sub _delete_nets_for_user{
    my ($class, $stash, $user) = @_;

    my $st = $stash->prepare_cached(DELETE_NETS_FOR_USER);
    $st->bind_param(1, $user);
    $st->execute();
}

sub delete_nets{
    my ($class, $stash, $users_to_delete, $users_to_keep) = @_;

    #In the users to keep I have also the users which are locked here

    #no delete, no party
    return if (scalar(@{$users_to_delete}) == 0);

    my $users_string = join (',', @{$users_to_delete});

    if (scalar(@{$users_to_keep}) == 0 ){
	#a single statement is sufficient

	my $sql = sprintf(DELETE_TRUSTS_NETS_UNLIMITED, $users_string, $users_string);
	my $st = $stash->get_connection()->prepare($sql);
	$st->execute();

    } else {

	my $users_keep_string = join (',', @{$users_to_keep});

	my $sql = sprintf(DELETE_OUTGOING_TRUSTS_NETS, $users_string, $users_keep_string);
	my $st = $stash->get_connection()->prepare($sql);
	$st->execute();

	$sql = sprintf(DELETE_INCOMING_TRUSTS_NETS, $users_keep_string, $users_string);
	$st = $stash->get_connection()->prepare($sql);
	$st->execute();
    }


    #ok, I can really perform the delete
 
    #ok, the I delete the nets for the users
    foreach(@{$users_to_delete}){
	$class->_delete_nets_for_user($stash, $_);
    }
}

1;
