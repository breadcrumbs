package Bcd::Data::StatementsStash;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use DBI;
use Cache::FastMmap;
use File::Basename;
use File::Find;
use File::Spec;
use Template;
use Bcd::Common::Mail;
use Data::Dumper;
use FindBin;

#this file should simply model the stash which holds the statements
#to query and modify the db...

sub new {
    #my ($class, $connection, $cache)  = @_;
    my ($class, $server, $init_file) = @_;

    my $is_testing = $server->{test};

    my $self = {};

    my $conn_string = $server->{conn_string};
    my $user        = $server->{user_db};
    my $share_file  = $server->{share_file};
    my $expire_time = $server->{expire_time};

#     if ($is_testing == 0){
# 	$conn_string = 'DBI:Pg:database=breadcrumb';
# 	$user = "";
# 	$share_file = "/tmp/bcd_cache";
#     } else {
# 	$conn_string = 'DBI:Pg:database=bcdb-test';
# 	$user = "lino";
# 	$share_file = "/tmp/TEST_bcd_cache";
#     }

    my $conn = DBI->connect_cached( $conn_string, $user, '',
				    { RaiseError => 1, AutoCommit => 0 });

    #I have also a cache...
    my $cache;


    #The $init file is only a hack. It is defined by the first one
    #which creates the stash, In this version is the manager of the
    #bots, because is created whenever the daemon starts.

    if (defined($init_file)){
	$cache = Cache::FastMmap->new(
				      share_file => $share_file,
				      init_file  => 1,
				      expire_time=> $expire_time,
				      );

	#in the first I initialize the command id
	$cache->set("current_id_cmd", 0);
    } else {
	$cache = Cache::FastMmap->new(
				      share_file => $share_file,
				      expire_time=> $expire_time,
				      );
    }

    $self->{conn}   = $conn;
    $self->{cache}  = $cache;
    $self->{mailer} = Bcd::Common::Mail->new($server);
    $self->{testing}= 0;

    bless ($self, $class);

    $self->_create_the_template($server->{bcd_home});
    
    return $self;
}

#this is only used by the tests...
sub in_testing{
    my $self = shift;
    $self->{testing} = 1;
    $self->{mailer}->testing_do_not_send_mails();
}

#this gets the bc-root password, hashed
sub get_bc_root_password{
    my $self = shift;
    if ($self->{testing} == 0){
	local $/ = "\n";
	#the user wants to be root. The password is stored in a local file, hashed
	#otherwise it is stored in the configuration file...
	my $name = "$FindBin::Bin/../local-admin-passwd";
	open PASSWD, "< $name" or die "Not found the bc-root password file $name\n";
	my $password = <PASSWD>;
	chomp $password;
	close PASSWD;
	return $password;
    } else {
	#if I am testing the password is fixed to "qpqpqp", and this is the hash...
	return "55a16180593910a43c0bd2a1344296b6fa4ee802";
    }
}

sub get_next_cmd_id{
    my $self = shift;
    #atomically increment and get the value of the next command id
    return $self->{cache}->get_and_set("current_id_cmd", sub { return ++$_[1]; });
}

sub get_mailer{
    my $self = shift;
    return $self->{mailer};
}

sub _create_the_template{
    my ($self, $bcd_home) = @_;

#     my $file = __FILE__;
#     my $abs_path = File::Spec->rel2abs( $file ) ;
#     my $canon_path = File::Spec->canonpath($abs_path);
#     my $base = File::Basename::dirname($canon_path);


    #the templates are inside /templates, and then I put "it" because
    #in this directory there are the italian templates...
    my $templates_root_dir = $bcd_home . '/templates/it';

    my $tt = Template->new({
	INCLUDE_PATH => $templates_root_dir,
	INTERPOLATE  => 1,
	WRAPPER      => 'mail_wrapper.tt2',
    });

    $self->{template} = $tt;
}

sub process_this_template{
    my ($self, $template_name, $vars, $output) = @_;

    $self->{template}->
	process($template_name, $vars, $output) 
	|| die $self->{template}->error();

    return 1;
}

#simple pass thru
sub prepare_cached{
    my ($self, $sql) = @_;
    return $self->{conn}->prepare_cached($sql);
}

sub get_statement{
    my ($self, $statement, $model) = @_;

    #I try to get the statement, if not, I will simply try to ask
    #kindly the model to populate the stash with its commands

    if ( ! exists($self->{$statement})){
	$model->populate_the_stash($self);
    }

    #if it does not exist, return undef...
    return $self->{$statement};
}

#this function should simply record a statement in the connection for
#future reuse, so it is prepared only once.
sub record_this_statement{
    my ($self, $statement_name, $sql_string) = @_;

    my $sth = $self->{conn}->prepare_cached( $sql_string );
    $self->{$statement_name} = $sth;
}

sub insert_statement{
    my ($self, $statement_string, $statement) = @_;
    $self->{$statement_string} = $statement;
}

sub get_connection{
    my $self = shift;
    return $self->{"conn"};
}

sub get_cache{
    my $self = shift;
    return $self->{"cache"};
}

sub get_last_personal_data_id {
    my $self = shift;
    return $self->_get_last_sequence_value("personal_users_data_id_seq");
}

sub get_id_of_last_user{
    my $self = shift;
    return $self->_get_last_sequence_value("users_id_seq");
}

sub get_id_of_last_site{
    my $self = shift;
    return $self->_get_last_sequence_value("bc_sites_id_seq");
}

sub get_last_founder_id{
   my $self = shift;
   return $self->_get_last_sequence_value("ant_nests_founders_id_user_seq");
}

sub get_last_activity_id{
    my ($self, $seq) = @_;
    return $self->_get_last_sequence_value($seq);
}

sub get_last_public_site_presence{
    my $self = shift;
    return $self->_get_last_sequence_value("users_in_sites_id_seq");
}

sub get_last_ad_id{
    my $self = shift;
    return $self->_get_last_sequence_value("ads_id_seq");
}

sub get_last_invoice_id{
    my $self = shift;
    return $self->_get_last_sequence_value("invoices_id_seq");
}

sub _get_last_sequence_value{
    my ($self, $seq_name) = @_;

    my $id = $self->{conn}->last_insert_id(undef, undef, undef, undef, 
					   {sequence => $seq_name});
    return $id;
}


#this method returns the post code stored in the session.
sub get_session_post_code{
    my ($self, $session) = @_;

    my $value = $self->get_cache()->get($session);
    return $value->{ant_nest};
}

sub get_session_id{
    my ($self, $session) = @_;

    my $value = $self->get_cache()->get($session);
    return $value->{user_id};
}

sub get_session_role{
    my ($self, $session) = @_;

    my $value = $self->get_cache()->get($session);
    return $value->{user_role};
}

sub get_session_nick{
   my ($self, $session) = @_;

    my $value = $self->get_cache()->get($session);
    return $value->{user_connected};
}

#just a convenience function
sub select_one_row_arr{
    my ($self, $st_sql, @pars) = @_;

    my ($st, $res) = $self->_prepare_bind_execute($st_sql, @pars);
    my $row = $st->fetch();
    $st->finish();

    return $row;
}

sub _prepare_bind_execute{
    my ($self, $st_sql, @pars) = @_;
    
    my $st = $self->prepare_cached($st_sql);
    my $res = $st->execute(@pars);

    return ($st, $res);
}

sub select_one_row_hash{
    my ($self, $st_sql, @pars) = @_;

    my ($st, $res) = $self->_prepare_bind_execute($st_sql, @pars);
    my $hash = $st->fetchrow_hashref();
    $st->finish();

    return $hash;
}

sub prep_exec{
    my ($self, $st_sql, @pars) = @_;

    my ($st, $res) = $self->_prepare_bind_execute($st_sql, @pars);
    return $res;
}

sub select_all_ds{
    my ($self, $st_sql, @pars) = @_;
    
    my ($st, $res) = $self->_prepare_bind_execute($st_sql, @pars);

    my $data_set = $st->fetchall_arrayref();
    my $var = $st->{NAME_lc};
    unshift (@{$data_set}, $var);
    return $data_set;
}

sub select_all_arr{
    my ($self, $st_sql, @pars) = @_;
    my ($st, $res) = $self->_prepare_bind_execute($st_sql, @pars);
    my $arr = $st->fetchall_arrayref();
    return $arr;
}

1;
