package Bcd::Data::Ads;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Data::Dumper;

use Bcd::Constants::AdsConstants;
use Bcd::Constants::ActivitiesConstants;

use constant {
    INSERT_NEW_AD                             =>
	qq{INSERT INTO ads(id_act, id_user, id_state, ad_text) VALUES (?,?,?,?)},

	INSERT_NEW_AD_LOCALITY                =>
	qq{INSERT INTO ads_user_localities(id_ad, id_locus, p_min, p_max, t_e, t_c) }.
	qq{VALUES( ? , ?, ?, ?, ?, ?)},

	GET_AD                                =>
	qq{SELECT id_act, id_user, id_state, created_on, last_price_calculated_on, ad_text }.
	qq{ FROM ads WHERE id = ?},

	GET_ADS_USER_LOCALITIES               =>
	qq{SELECT id_locus, p_min, p_max, t_e, t_c FROM ads_user_localities WHERE }.
	qq{id_ad = ? },

	GET_AD_USER_LOCALITY                  =>
	qq{SELECT p_min, p_max, t_e, t_c FROM ads_user_localities WHERE }.
	qq{id_locus = ? AND id_ad = ?},

	GET_USER_ADS                          =>
	qq{SELECT a.id, a.id_act, act.description, a.created_on, a.ad_text }.
	qq{ FROM ads as a, activities as act WHERE a.id_user = ? AND a.id_state = }.
	Bcd::Constants::AdsConstants::ACTIVE . 
	qq{ AND a.id_act = act.id},

	GET_ADS_FOR_THIS_ACTIVITY             =>
	qq{SELECT a.id, a.id_user, a.created_on, a.ad_text FROM ads as a, users as u }.
	qq{ WHERE a.id_act = ? AND a.id_user = u.id AND u.id_ant_nest = ? AND a.id_state = }.
	Bcd::Constants::AdsConstants::ACTIVE,

	GET_COUNT_ADS_FOR_ACTIVITY            =>
	qq{SELECT COUNT(*) FROM ads as a, users as u }.
	qq{ WHERE a.id_act = ? AND a.id_user = u.id AND u.id_ant_nest = ? AND a.id_state = }.
	Bcd::Constants::AdsConstants::ACTIVE,

	GET_COUNT_ADS_FOR_LOCUS               =>
	qq{SELECT COUNT(*) FROM ads_user_localities as aul , ads as a WHERE aul.id_locus = ?}.
	qq{AND a.id_state = } . Bcd::Constants::AdsConstants::ACTIVE . qq{ AND aul.id_ad = a.id},

	GET_ADS_FOR_LOCUS                     =>
	qq{select a.id, act.description, a.ad_text, aul.p_min FROM ads AS a, }.
	qq{ads_user_localities as aul, activities as act WHERE a.id_act = act.id AND }.
	qq{aul.id_ad = a.id AND aul.id_locus = ?},

	GET_ADS_IDS_FOR_LOCUS                 =>
	qq{SELECT id_ad FROM ads_user_localities WHERE id_locus = ?},

	UPDATE_AD_STATE                       =>
	qq{UPDATE ads SET id_state = ? WHERE id = ? },

	CHANGE_LOCUS_FOR_ADS                  =>
	qq{UPDATE ads_user_localities SET id_locus = ? WHERE id_locus = ? },

};

sub is_there_an_ad_which_has_this_two_loci{
    my ($class, $stash, $id_locus1, $id_locus2) = @_;

    #I should simply make a double select. In this case it does not
    #count if the ad is active or not, it simply should not happen

    my $ads_ids = $stash->select_all_arr(GET_ADS_IDS_FOR_LOCUS, $id_locus1);

    #Ok, now I iterate over this list
    foreach(@{$ads_ids->[0]}){
	#is there an ad with also the second locus
	my $ad = $stash->select_one_row_arr(GET_AD_USER_LOCALITY, $id_locus2, $_);
	if (defined($ad)){
	    return 1; #ok, there exists one ad...
	}
    }

    return 0; #no one has these two loci
}

sub change_locus_for_ads{
    my ($class, $stash, $new_locus, $old_locus) = @_;
    $stash->prep_exec(CHANGE_LOCUS_FOR_ADS, $new_locus, $old_locus);
}

sub get_ads_for_locus_ds{
    my ($class, $stash, $id_locus) = @_;
    return $stash->select_all_ds(GET_ADS_FOR_LOCUS, $id_locus);
}

sub get_count_ads_for_locus{
    my ($class, $stash, $id_locus) = @_;
    my $row =  $stash->select_one_row_arr(GET_COUNT_ADS_FOR_LOCUS, $id_locus);
    return $row->[0];
}

sub this_ad_is_inactive_now{
    my ($class, $stash, $id_ad) = @_;
    $stash->prep_exec
	(UPDATE_AD_STATE, Bcd::Constants::AdsConstants::INACTIVE, $id_ad);
}

sub get_ad_locality_hash{
    my ($class, $stash, $id_ad, $id_locus) = @_;
    return $stash->select_one_row_hash(GET_AD_USER_LOCALITY, $id_locus, $id_ad);
}

sub get_ad_locality_arr{
    my ($class, $stash, $id_ad, $id_locus) = @_;
    return $stash->select_one_row_arr(GET_AD_USER_LOCALITY, $id_locus, $id_ad);
}

sub get_activity_post_code_ads_arr{
    my ($class, $stash, $id_act, $post_code) = @_;
    return $stash->select_all_arr(GET_ADS_FOR_THIS_ACTIVITY, $id_act, $post_code);
}

sub get_count_activity_post_code_ads{
    my ($class, $stash, $id_act, $post_code) = @_;
    my $row = $stash->select_one_row_arr(GET_COUNT_ADS_FOR_ACTIVITY, $id_act, $post_code);
    return (!defined($row)) ? 0 : $row->[0];
}

sub get_user_ads_ds{
    my ($class, $stash, $user_id) = @_;
    return $stash->select_all_ds(GET_USER_ADS, $user_id);
}


sub get_all_localities_for_ad_arr{
    my ($class, $stash, $id) = @_;
    return $stash->select_all_arr(GET_ADS_USER_LOCALITIES, $id);
}

sub get_all_localities_for_ad_ds{
    my ($class, $stash, $id) = @_;
    return $stash->select_all_ds(GET_ADS_USER_LOCALITIES, $id);
}

sub get_ad_hash{
    my ($class, $stash, $id) = @_;
    return $stash->select_one_row_hash
	(GET_AD, $id);
}

sub create_new_ad{
    my ($class, $stash, $id_act, $id_user, 
	$text, $id_presence, $p_min, $p_max, $t_e, $t_c) = @_;

    #ok, I should simply insert two rows in the db...
    #one row is the ad
    $stash->prep_exec
	(INSERT_NEW_AD, $id_act, $id_user, 
	 Bcd::Constants::AdsConstants::ACTIVE, $text);

    #then I get the id of this ad...
    my $ad_id = $stash->get_last_ad_id();

    #ok, Now I add the ads_localities row
    $class->add_locus_to_ad($stash, $ad_id, $id_presence, $p_min, $p_max, $t_e, $t_c);

    return $ad_id;
}

sub decode_ad_type_from_activity{
    my ($class, $id_act) = @_;
    
    my $ad_type;

    if ($id_act <= Bcd::Constants::ActivitiesConstants::MAX_SERVICE_ID){
	$ad_type = 'service';
    } elsif ($id_act < Bcd::Constants::ActivitiesConstants::MAX_HOMEMADE_ID){
	$ad_type = 'homemade';
    } else {
	$ad_type = 'object';
    }

    return $ad_type;
}

sub add_locus_to_ad{
    my ($class, $stash, $ad_id, $id_presence, $p_min, $p_max, $t_e, $t_c) = @_;

    $stash->prep_exec
	(INSERT_NEW_AD_LOCALITY, $ad_id, $id_presence, $p_min, $p_max, $t_e, $t_c);
}


sub init_db {
    my ($class, $stash) = @_;

    my $conn = $stash->get_connection();

    #ok, now I should insert the values in the table...
    my $sth = $conn->prepare(qq{insert into ads_states values(?,?)});

    foreach (Bcd::Constants::AdsConstants::LIST_ADS_STATES){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }
}


1;
