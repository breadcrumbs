package Bcd::Data::Accounting::Account;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Data::Dumper;

#this class should simply model an account in the system
#only the accounts plan should be able to create an account

#these constants stores the string to access the statements for the
#accounts
use constant{
    COUNT_ST => "Account_count_split_statement",
    SPLIT_ST => "Account_splits_statement",
    GET_BALANCE => "Account_get_bal_statement",
    ADD_SPLIT => "Account_add_split_statement",
    GET_CHILDREN => "Account_get_children_statement",
};

#my $get_children_statement;

#The get_children_statement is equal for all the accounts, fake and real
# BEGIN{
#     my $sql = qq{ select * from master_detail_accounts where id_parent_account = ?};
#     my $conn = data::Db->get_connection();
#     $get_children_statement = $conn->prepare($sql);
# }

# #this function is called whenever the derived classes are built.
# sub _INITIALIZE_THE_COMMANDS_FOR_THIS_TABLE{
#     my $class = shift;
#     my $table = shift;
#     my $hash_ref = shift; #this holds the commands.

#     my $conn = data::Db->get_connection();
    
#     my $sql_count = qq{select count(*) from $table where id_account = ?};
#     $hash_ref->{COUNT_ST} = $conn->prepare($sql_count);

#     #now the other statement

#     my $sql_split = qq{ select * from $table where id_account = ?};
#     $hash_ref->{SPLIT_ST} = $conn->prepare($sql_split);

#     #this query should get the balance of the account (without considering children).
#     my $sql_balance = qq{select total from $table where id_account = ? order by id_transaction desc};
#     $hash_ref->{GET_BALANCE} = $conn->prepare($sql_balance);

#     my $add_split = qq{insert into $table(id_transaction, id_account, amount_debit, amount_credit, total)
# 			   values(lastval(), ?, ?, ?, ?)};
#     $hash_ref->{ADD_SPLIT} = $conn->prepare($add_split);
# }

#this function is called whenever there is a connection without the
#commands initialized
sub populate_the_stash{

    #this function should be called by the connection threads.
    my ($self, $db_stash) = @_;

    #I have Euros and Tao, so I have two tables and two set of commands.
    my $suffix = $self->_get_suffix();
    
    my $table = "transactions_splits" . $suffix;

    #ok, let's build the commands...
    my $sql = qq{select count(*) from $table where id_account = ?};
    my $sth = $db_stash->get_connection()->prepare( $sql );
    $db_stash->insert_statement(COUNT_ST . $suffix, $sth);

    $sql = qq{ select * from $table where id_account = ?};
    $sth = $db_stash->get_connection()->prepare ( $sql );
    $db_stash->insert_statement(SPLIT_ST . $suffix, $sth);

    $sql = qq{select total from $table where id_account = ? order by id_transaction desc LIMIT 1};
    $sth = $db_stash->get_connection()->prepare ( $sql );
    $db_stash->insert_statement(GET_BALANCE . $suffix, $sth);

    $sql = qq{insert into $table(id_transaction, id_account, amount_debit, amount_credit, total)
		  values(lastval(), ?, ?, ?, ?)};
    $sth = $db_stash->get_connection()->prepare ( $sql );
    $db_stash->insert_statement(ADD_SPLIT . $suffix, $sth);

    #this statement should get all the childs for a parent...
    #this statement is NOT divided for E or T
    $sql = qq{ select id from accounts_plan where id_parent_account = ?};
    $sth = $db_stash->get_connection()->prepare ( $sql );
    $db_stash->insert_statement(GET_CHILDREN, $sth);
    
}

sub new{
    my $class = shift;

    my $self = {};
    bless ($self, $class);

    $self->{"type"} = shift;
    $self->{"id_parent"} = shift;
    $self->{"description"} = shift;
    $self->{"id_account"} = shift;

    return $self;
}

sub get_type_cippo{
    my $self = shift;
    return $self->{"type"};
}

sub get_description_cippo{
    my $self = shift;
    return $self->{"description"};
}

sub get_parent_cippo{
    my $self = shift;
    return $self->{"id_parent"};
}

#this function simply test if there are some transactions in this
#account.
sub have_you_got_any_transactions_cippo{

    my ($self, $stash) = @_;
    my $num_splits;

    my $suffix = $self->_get_suffix();

    my $st_str = COUNT_ST . $suffix;

    my $st = $stash->get_statement($st_str, $self);

    $st->bind_param(1, $self->{"id_account"});

    $st->execute();

    $st->bind_columns(undef, \$num_splits);
    $st->fetch();

    
    $st->finish();

    if ($num_splits != 0){
	return 1;
    } else {
	return 0;
    }

}

#This method should simply return the balance of an account without
#considering the children
sub get_balance{
    my ($self, $stash) = @_;
    my $balance;

    my $st = $stash->get_statement(GET_BALANCE . $self->_get_suffix(), $self);
    $st->bind_param(1, $self->{"id_account"});
    $st->execute();

    $st->bind_columns(undef, \$balance);
    $st->fetch();

    $st->finish();

    #if the balance is undef (no transactions) I assume it zero.
    return (defined($balance)) ? $balance : 0;
}

#This function simply gets the balance regarding all the childs...
sub get_recursive_balance{
    my ($self, $stash) = @_;

    #ok, let's get all the children
    my $get_children_statement = $stash->get_statement(GET_CHILDREN, $self);
    $get_children_statement->bind_param(1, $self->{"id_account"});
    $get_children_statement->execute();

    my $id_child;
    $get_children_statement->bind_columns(undef, \$id_child);

    my $total_balance = 0;
    my @list_of_children;

    while ( defined($get_children_statement->fetch()) ) {
	#ok, now I should get the recursive balance of the children
	push (@list_of_children, $id_child);
    }

    $get_children_statement->finish();

    #if I have no children, then I return the normal balance
    if (scalar(@list_of_children) == 0){
	return $self->get_balance($stash); #normal balance
    }


    foreach(@list_of_children){

	my $child_account;

	$child_account = Bcd::Data::Accounting::AccountsPlan->get_account_with_number($_, $stash);
 	$total_balance += $child_account->get_recursive_balance($stash);
    }

    return $total_balance;

}

#this method should add the splits
sub add_transaction_split{

    #ok, I should observe the amount of this transaction.  if
    #debit>credit is a "debit" transaction, otherwise is a credit if
    #credit = debit, then the transaction is "0", and not recorded.

    my ($self, $debit, $credit, $stash) = @_;

    #nothing to save...
    return undef if ($debit == $credit);

    if ($debit > $credit){
	return $self->add_debit_split($debit-$credit, $stash);
    } else {
	return $self->add_credit_split($credit-$debit, $stash);
    }
}

sub add_debit_split{

    my ($self, $amount_debit, $stash) = @_;

    my $total = $self->get_balance($stash);
    $total += $amount_debit;
    
    my $st_insert = $stash->get_statement(ADD_SPLIT . $self->_get_suffix(), $self);
    $st_insert->bind_param(1, $self->{"id_account"});
    $st_insert->bind_param(2, $amount_debit);
    $st_insert->bind_param(3, 0);
    $st_insert->bind_param(4, $total);

    $st_insert->execute();
    $st_insert->finish();

    return $total;
    
}

sub add_credit_split{

    my ($self, $amount_credit, $stash) = @_;

    my $total = $self->get_balance($stash);
    $total -= $amount_credit;
    
    my $st_insert = $stash->get_statement(ADD_SPLIT . $self->_get_suffix(), $self);
    $st_insert->bind_param(1, $self->{"id_account"});
    $st_insert->bind_param(2, 0);
    $st_insert->bind_param(3, $amount_credit);
    $st_insert->bind_param(4, $total);

    $st_insert->execute();
    $st_insert->finish();

    return $total;
}

1;
