package Bcd::Data::Accounting::RealCurrencyAccount;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
#xuse accounting::Account;

#this class simply is the class which models a real currency account.

our @ISA = qw(Bcd::Data::Accounting::Account);

# my %st_holder;

# BEGIN{
#     my $table = "transactions_splits_e";
#     @ISA = qw(accounting::Account);

#     #I call the base class function to initialize the commands...
#     $ISA[0]->_INITIALIZE_THE_COMMANDS_FOR_THIS_TABLE($table, \%st_holder);
# }

sub new {
    my $class = shift;
    my $self  = $class->SUPER::new(@_);

    bless ($self, $class);
    return $self;
}

#this function simply returns the suffix with which I distinguish the two tables
#which hold the splits
sub _get_suffix{
    return "_e";
}

1;
