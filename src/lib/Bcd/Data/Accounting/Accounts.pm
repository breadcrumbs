package Bcd::Data::Accounting::Accounts;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use Bcd::Data::Accounting::AccountsPlan;

use Data::Dumper;

#this file should simply unify the two types of accounts (which are
#divided only because the two currencies have a different db
#representation

use constant {
    EURO_SUFFIX => "e",
    TAO_SUFFIX  => "t",
};

use constant {

    ADD_GENERIC_SPLIT => qq[insert into transactions_splits_%s].
	qq[ (id_transaction, id_account, amount_debit, amount_credit, total) values(lastval(), ?, ?, ?, ?)],

	

	GET_GENERIC_BALANCE => qq{select total from transactions_splits_%s }.
	qq{where id_account = ? order by id_transaction desc LIMIT 1},

	
	GET_CHILDREN  => qq{ select id from accounts_plan where id_parent_account = ?},

	COUNT_SPLITS  => qq{select count(*) from transactions_splits_%s where id_account = ?},

	GET_GENERIC_ACCOUNT_SUMMARY =>
	qq{SELECT t.date, t.description, s.amount_debit, s.amount_credit, s.total FROM }.
	qq{ transactions_splits_%s as s, transactions as t WHERE }.
	qq{ s.id_transaction = t.id AND s.id_account = ? }.
	qq{ ORDER BY t.id},
	
    };


my %statements = 
(
 
 "ADD_SPLIT".EURO_SUFFIX() => sprintf(ADD_GENERIC_SPLIT, EURO_SUFFIX), 
 "ADD_SPLIT".TAO_SUFFIX()  => sprintf(ADD_GENERIC_SPLIT, TAO_SUFFIX), 
 
 "GET_BALANCE".EURO_SUFFIX() => sprintf(GET_GENERIC_BALANCE, EURO_SUFFIX),
 "GET_BALANCE".TAO_SUFFIX()  => sprintf(GET_GENERIC_BALANCE, TAO_SUFFIX),

 "COUNT_SPLITS".EURO_SUFFIX() => sprintf(COUNT_SPLITS, EURO_SUFFIX),
 "COUNT_SPLITS".TAO_SUFFIX()  => sprintf(COUNT_SPLITS, TAO_SUFFIX),

 "GET_GENERIC_ACCOUNT_SUMMARY".EURO_SUFFIX() => 
 sprintf(GET_GENERIC_ACCOUNT_SUMMARY, EURO_SUFFIX),

 "GET_GENERIC_ACCOUNT_SUMMARY".TAO_SUFFIX() => 
 sprintf(GET_GENERIC_ACCOUNT_SUMMARY, TAO_SUFFIX),

 		  
);

sub get_account_summary_ds{
    my ($class, $stash, $id) = @_;

    my $st;
    my $euro_account;

    if (Bcd::Data::Accounting::AccountsPlan->is_in_real_currency($stash, $id)){
	$euro_account = 1;
	$st = $stash->prepare_cached($statements{"GET_GENERIC_ACCOUNT_SUMMARY".EURO_SUFFIX()});
    } else {
	$euro_account = 0;
	$st = $stash->prepare_cached($statements{"GET_GENERIC_ACCOUNT_SUMMARY".TAO_SUFFIX()});
    }

    $st->bind_param(1, $id);
    $st->execute();

    my $var = $st->{NAME_lc};
    my $arr = $st->fetchall_arrayref();

    #I put at the front of the recordset the columns names.
    unshift (@{$arr}, $var);

    return ($arr, $euro_account); 
}

sub get_recursive_balance{
    my ($class, $stash, $id) = @_;

    if (Bcd::Data::Accounting::AccountsPlan->is_in_real_currency($stash, $id)){

	return $class->_get_recursive_balance_suffixed($stash, EURO_SUFFIX, $id);
    } else {

	return $class->_get_recursive_balance_suffixed($stash, TAO_SUFFIX,  $id);
    }
}

sub _get_recursive_balance_suffixed{
    my ($class, $stash, $suffix, $id) = @_;

    #ok, let's get all the children
    my $get_children_statement = $stash->prepare_cached(GET_CHILDREN);

    $get_children_statement->bind_param(1, $id);
    $get_children_statement->execute();

    my $id_child;
    $get_children_statement->bind_columns(undef, \$id_child);

    my $total_balance = 0;
    my @list_of_children;

    while ( defined($get_children_statement->fetch()) ) {
	#ok, now I should get the recursive balance of the children
	push (@list_of_children, $id_child);
    }

    $get_children_statement->finish();

    #if I have no children, then I return the normal balance
    if (scalar(@list_of_children) == 0){
	return $class->get_balance($stash, $id); #normal balance
    }


    foreach(@list_of_children){

	$total_balance += $class->_get_recursive_balance_suffixed($stash, $suffix, $_);
    }

    return $total_balance;
}




sub add_transaction_split{
    my ($class, $stash, $id, $debit, $credit) = @_;


    #I should simply take the account...
    my ($is_in_real_currency, $is_positive) = 
	Bcd::Data::Accounting::AccountsPlan->get_account_type($stash, $id);

    if ($is_in_real_currency){

	return $class->_add_currency_transaction_split($stash, EURO_SUFFIX, $id, $debit, $credit, $is_positive);

    } else {

	return $class->_add_currency_transaction_split($stash, TAO_SUFFIX,  $id, $debit, $credit, $is_positive);

    }
    
}

sub _add_currency_transaction_split{
    my ($class, $stash, $suffix, $id, $debit, $credit, $is_positive) = @_;

    if ($debit > $credit){
	return $class->_add_debit_split ($stash, $suffix, $id, $debit-$credit, $is_positive);
    } else {
	return $class->_add_credit_split($stash, $suffix, $id, $credit-$debit, $is_positive);
    }
}

sub get_transactions_count{
    my ($class, $stash, $id) = @_;

    if (Bcd::Data::Accounting::AccountsPlan->is_in_real_currency($stash, $id)){
	return $class->_get_transactions_count_suffixed($stash, EURO_SUFFIX, $id);
    } else {
	return $class->_get_transactions_count_suffixed($stash, TAO_SUFFIX,  $id);
    }
}

sub _get_transactions_count_suffixed{
    my ($class, $stash, $suffix, $id) = @_;

    my $num_splits;

    my $st = $stash->prepare_cached($statements{"COUNT_SPLITS$suffix"});

    $st->bind_param(1, $id);

    $st->execute();

    $st->bind_columns(undef, \$num_splits);
    $st->fetch();

    
    $st->finish();

    return $num_splits;
    
}

sub get_balance{
    my ($class, $stash, $id) = @_;


    if (Bcd::Data::Accounting::AccountsPlan->is_in_real_currency($stash, $id)){

	return $class->_get_balance_suffixed($stash, EURO_SUFFIX, $id);

    } else {

	return $class->_get_balance_suffixed($stash, TAO_SUFFIX,  $id);

    }
}


sub _get_balance_suffixed{
    my ($self, $stash, $suffix, $id) = @_;
    my $balance;

    my $st = $stash->prepare_cached($statements{"GET_BALANCE$suffix"});
    $st->bind_param(1, $id);
    $st->execute();

    $st->bind_columns(undef, \$balance);
    $st->fetch();

    $st->finish();

    #if the balance is undef (no transactions) I assume it zero.
    return (defined($balance)) ? $balance : 0;
}

sub _add_debit_split{
    my ($class, $stash, $suffix, $id, $amount_debit, $is_positive) = @_;

    my $total = $class->_get_balance_suffixed($stash, $suffix, $id);

    if ($is_positive){
	$total += $amount_debit;
    } else {
	$total -= $amount_debit;
    }
    
    my $st_insert = $stash->prepare_cached($statements{"ADD_SPLIT$suffix"});

    $st_insert->bind_param(1, $id);
    $st_insert->bind_param(2, $amount_debit);
    $st_insert->bind_param(3, 0);
    $st_insert->bind_param(4, $total);

    $st_insert->execute();
    $st_insert->finish();

    return $total;
    
}

sub _add_credit_split{

    my ($class, $stash, $suffix, $id, $amount_credit, $is_positive) = @_;

    my $total = $class->_get_balance_suffixed($stash, $suffix, $id);

    if ($is_positive) {
	$total -= $amount_credit;
    } else {
	$total += $amount_credit;
    }
    
    my $st_insert = $stash->prepare_cached($statements{"ADD_SPLIT$suffix"});

    $st_insert->bind_param(1, $id);
    $st_insert->bind_param(2, 0);
    $st_insert->bind_param(3, $amount_credit);
    $st_insert->bind_param(4, $total);

    $st_insert->execute();
    $st_insert->finish();

    return $total;
}

1;
