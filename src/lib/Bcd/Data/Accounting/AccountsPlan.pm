package Bcd::Data::Accounting::AccountsPlan;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#use data::Db;
use Bcd::Data::Accounting::FakeCurrencyAccount;
use Bcd::Data::Accounting::RealCurrencyAccount;

use Bcd::Common::BasicAccounts;

#ok, now I have some statements to make my work...
use constant{
    SELECT_ACCOUNT => "AccountsPlan_select_account",
    GET_TYPE => "AccountsPlan_get_type",
    INSERT_ACCOUNT => "AccountsPlan_insert_account",
    SELECT_ACCOUNT_BY_DESC => "AccountsPlan_select_account_by_desc",
};

sub populate_the_stash{

    my ($self, $db_stash) = @_;

    #ok, let's build the commands...
    my $sql = qq{select * from accounts_plan where id= ?};
    my $sth = $db_stash->get_connection()->prepare( $sql );
    $db_stash->insert_statement(SELECT_ACCOUNT, $sth);

    $sql = qq{select * from master_accounts where id = ?};
    $sth = $db_stash->get_connection()->prepare( $sql );
    $db_stash->insert_statement(GET_TYPE, $sth);

    $sql = qq{ insert into accounts_plan values(nextval(?), ?, ?, ?)};
    $sth = $db_stash->get_connection()->prepare( $sql );
    $db_stash->insert_statement(INSERT_ACCOUNT, $sth);

    $sql = qq{ select * from  accounts_plan where description=?};
    $sth = $db_stash->get_connection()->prepare( $sql );
    $db_stash->insert_statement(SELECT_ACCOUNT_BY_DESC, $sth);
}

#This package is the package which should manage the accounts in the
#system

#I want the parent account and its description.
#The type is implicit from the parent.
sub create_a_normal_account{
    my ($self, $id_parent, $description, $stash) = @_;
    return $self->_create_new_account_with_sequence($id_parent, $description, 'normal_accounts_seq', $stash);
}

#this is the same of the preceding method: it does use another sequence, though
sub create_a_user_account{
    my ($self, $id_parent, $description, $stash) = @_;
    return $self->_create_new_account_with_sequence($id_parent, $description, 'user_accounts_seq', $stash);
}


#this is the normal select on a account
sub select_account_data {
    my ($self, $stash, $id) = @_;

    my $st = $stash->get_statement(SELECT_ACCOUNT, $self);
    $st->bind_param(1, $id);

    $st->execute();
    my $row = $st->fetchrow_arrayref();
    $st->finish();

    return $row;
}

sub select_account_hash{
    my ($self, $stash, $id) = @_;

    my $st = $stash->get_statement(SELECT_ACCOUNT, $self);
    $st->bind_param(1, $id);

    $st->execute();
    my $hash = $st->fetchrow_hashref();
    $st->finish();

    return $hash;
}

sub is_in_real_currency{
    my ($self, $stash, $id) = @_;

    my $acc = $self->select_account_data($stash, $id);

    if ($acc->[1] % 2 == 0){
	#even... real currency
	return 1;
    } else {
	return 0;
    }
}

sub get_account_type{
    my ($self, $stash, $id) = @_;

    my $acc = $self->select_account_data($stash, $id);

    my $is_in_real_currency;
    if ($acc->[1] % 2 == 0){
	#even... real currency
	$is_in_real_currency = 1;
    } else {
	$is_in_real_currency = 0;
    }

    #integer division
    my $type = $acc->[1];


    if ( ! $is_in_real_currency){
	$type --;
    }

    $type /= 2;

    my $is_positive;

    if ( $type == 0 or $type == 3){
	$is_positive = 1;
    } else {
	$is_positive = 0;
    }

    return ($is_in_real_currency, $is_positive);
}

#the method returns the id of the newly created child account
sub _create_new_account_with_sequence{
    my ($self, $id_parent, $description, $sequence, $stash) = @_;

    #my $parent = $self->get_account_with_number($id_parent, $stash);
    #then I should get the type...
    #my $child_type = $parent->get_type();

    my $acc = $self->select_account_data($stash, $id_parent);
    my $child_type = $acc->[1];

    my $st_insert = $stash->get_statement(INSERT_ACCOUNT, $self);
    $st_insert->bind_param(1, $sequence);
    $st_insert->bind_param(2, $child_type);
    $st_insert->bind_param(3, $id_parent);
    $st_insert->bind_param(4, $description);
    $st_insert->execute();


    $st_insert->finish();

    #I should return the last insert id
    my $child_id = $stash->get_connection()->last_insert_id(undef, undef, undef, undef, 
							    {sequence => $sequence});



    #there is not here the master detail relationship
    return $child_id;
}

sub get_account_with_number_deprecated{

    my ($self, $id_to_get, $stash) = @_;

    my $st_select = $stash->get_statement(SELECT_ACCOUNT, $self);
    $st_select->bind_param(1, $id_to_get);
    $st_select->execute();

    #at this time I should get the accounts type and initialize it
    my $account_type;
    my $description;
    my $id_parent;

    $st_select->bind_columns(undef, undef, \$account_type, \$id_parent, \$description);

    $st_select->fetch();
    $st_select->finish();

    #I should create an account with this type and id
    return $self->_create_account_object($id_to_get, $account_type, $id_parent, $description, $stash);


}

use Data::Dumper;

#this function should simply create the account object, but the
#account should already be existing in the db.
sub _create_account_object{
    
    my ($self, $id, $type, $id_parent, $desc, $stash) = @_;

    my $st_type = $stash->get_statement(GET_TYPE ,$self);
    $st_type->bind_param(1, $type);
    $st_type->execute();

    my $is_in_real_currency;
    $st_type->bind_columns(undef, undef, undef, \$is_in_real_currency);
    $st_type->fetch();
    $st_type->finish();

    if ($is_in_real_currency){
	return Bcd::Data::Accounting::RealCurrencyAccount->new($type, $id_parent, $desc, $id);
    } else {
	return Bcd::Data::Accounting::FakeCurrencyAccount->new($type, $id_parent, $desc, $id);
    }
}

#this method should create a normal ant nest code
sub _create_ant_nest_account_code{
    my ($self, $id_parent, $code, $radix, $stash) = @_;
    
    my $name = sprintf($radix, $code);

    return $self->create_a_normal_account
	($id_parent, $name, $stash);
}

sub _create_ant_nest_income_account_code{
    my ($self, $id_parent, $code, $coin_name, $par_total, $radix, $stash) = @_;
    
    my $name = sprintf($radix, $code, $coin_name, $par_total);

    return $self->create_a_normal_account
	($id_parent, $name, $stash);
}

sub get_ant_nest_income_account_id{
    my ($self, $stash, $radix, $code, $coin_name, $par_total) = @_;

    my $name = sprintf($radix, $code, $coin_name, $par_total);

    return $self->get_id_account_from_name($name, $stash);
    
}

#this function simply gets an account from the name
#the name is unique...
sub get_id_account_from_name{
    my ($self, $name, $stash) = @_;

    my $st = $stash->get_statement(SELECT_ACCOUNT_BY_DESC, $self);
    $st->bind_param(1, $name);
    $st->execute();

    my $id;

    $st->bind_columns(undef, \$id, undef, undef, undef);
    $st->fetch();
    $st->finish();

    return $id;
}


#this method should create an ant nest code...
sub create_ant_nest_accounts{
    my ($self, $code, $stash) = @_;

    #there are several accounts for each ant nest.
    
    #the Euro cash, this is hold by the treasurer
    $self->_create_ant_nest_account_code
	(Bcd::Common::BasicAccounts::ANT_NEST_CASHES->[0], $code,
	 Bcd::Common::BasicAccounts::ANT_NEST_CASH, $stash);

    #the total incomes in Euro
    my $total_incomes_euro = $self->_create_ant_nest_account_code
	(Bcd::Common::BasicAccounts::ANT_NEST_INCOMES_E->[0], $code,
	 Bcd::Common::BasicAccounts::ANT_NEST_INCOME_E, $stash);

    #the partial incomes in Euro
    my $partial_incomes_euro = $self->_create_ant_nest_account_code
	(Bcd::Common::BasicAccounts::PARTIAL_INCOMES_E->[0], $code, 
	 Bcd::Common::BasicAccounts::ANT_NEST_PAR_INC_E, $stash);

    #the partial incomes in Tao
    my $partial_incomes_tao = $self->_create_ant_nest_account_code
	(Bcd::Common::BasicAccounts::PARTIAL_INCOMES_T->[0], $code, 
	 Bcd::Common::BasicAccounts::ANT_NEST_PAR_INC_T ,$stash);

    #the users' accounts in Euro
    $self->_create_ant_nest_account_code
	(Bcd::Common::BasicAccounts::USERS_ACCOUNTS_E->[0], $code, 
	 Bcd::Common::BasicAccounts::ANT_NEST_USER_ACC_E, $stash);

    #the users' accounts in Tao
    $self->_create_ant_nest_account_code
	(Bcd::Common::BasicAccounts::USERS_ACCOUNTS_T->[0], $code,
	 Bcd::Common::BasicAccounts::ANT_NEST_USER_ACC_T, $stash);

    #the "parking" Euro account
    $self->_create_ant_nest_account_code
	(Bcd::Common::BasicAccounts::PARKING_EURO_FUND->[0], $code, 
	 Bcd::Common::BasicAccounts::PARKING_EURO_FUND_RAD, $stash);

    #the account which holds the tao for the ant nest (the taxes...)
    $self->_create_ant_nest_account_code
	(Bcd::Common::BasicAccounts::ANT_NEST_ACCOUNTS_T->[0], $code,
	 Bcd::Common::BasicAccounts::ANT_NEST_ACC_T, $stash);
    
    #the account which holds the Euro for the ant nests (the result of the taxes)
    $self->_create_ant_nest_account_code
	(Bcd::Common::BasicAccounts::ANT_NEST_ACCOUNTS_E->[0], $code, 
	 Bcd::Common::BasicAccounts::ANT_NEST_ACC_E, $stash);

    #now I should create the accounts which holds the incomes.
    my $par = Bcd::Common::BasicAccounts::PARTIAL;
    my $tot = Bcd::Common::BasicAccounts::TOTAL;
    my $tao = Bcd::Common::BasicAccounts::TAO;
    my $eur = Bcd::Common::BasicAccounts::EURO;
    
    #demourrage income: partial E, total E, partial T
    $self->_create_ant_nest_income_account_code
	($total_incomes_euro, $code, $eur, $tot, Bcd::Common::BasicAccounts::DEMOURRAGE_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_euro, $code, $eur, $par, Bcd::Common::BasicAccounts::DEMOURRAGE_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_tao, $code, $tao, $par, Bcd::Common::BasicAccounts::DEMOURRAGE_INCOMES, $stash);

    #exchange incomes: partial and total Euro

    $self->_create_ant_nest_income_account_code
	($total_incomes_euro, $code, $eur, $tot, Bcd::Common::BasicAccounts::EXCHANGE_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_euro, $code, $eur, $par, Bcd::Common::BasicAccounts::EXCHANGE_INCOMES, $stash);


    #deposits incomes: partial and total Euro

    $self->_create_ant_nest_income_account_code
	($total_incomes_euro, $code, $eur, $tot, Bcd::Common::BasicAccounts::DEPOSITS_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_euro, $code, $eur, $par, Bcd::Common::BasicAccounts::DEPOSITS_INCOMES, $stash);


    #activities incomes, partial Euro and Tao and total Euro

    $self->_create_ant_nest_income_account_code
	($total_incomes_euro, $code, $eur, $tot, Bcd::Common::BasicAccounts::ACTIVITIES_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_euro, $code, $eur, $par, Bcd::Common::BasicAccounts::ACTIVITIES_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_tao, $code, $tao, $par, Bcd::Common::BasicAccounts::ACTIVITIES_INCOMES, $stash);

    
    #the objects incomes, partial Euro and Tao and total Euro

    $self->_create_ant_nest_income_account_code
	($total_incomes_euro, $code, $eur, $tot, Bcd::Common::BasicAccounts::OBJECTS_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_euro, $code, $eur, $par, Bcd::Common::BasicAccounts::OBJECTS_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_tao, $code, $tao, $par, Bcd::Common::BasicAccounts::OBJECTS_INCOMES, $stash);


    #The cheques emissions: partial Euro and Tao and total Euro

    $self->_create_ant_nest_income_account_code
	($total_incomes_euro, $code, $eur, $tot, Bcd::Common::BasicAccounts::CHEQUES_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_euro, $code, $eur, $par, Bcd::Common::BasicAccounts::CHEQUES_INCOMES, $stash);

    $self->_create_ant_nest_income_account_code
	($partial_incomes_tao, $code, $tao, $par, Bcd::Common::BasicAccounts::CHEQUES_INCOMES, $stash);

}

sub init_db{
    my ($class, $stash) = @_;

    #ok, I should insert some values in the db
    my $sql = qq{INSERT INTO account_types VALUES(?,?)};
    my $st  = $stash->get_connection()->prepare($sql);

    for(Bcd::Common::BasicAccounts::LIST_TYPES){
	$st->bind_param(1, $_->[0]);
	$st->bind_param(2, $_->[1]);
	$st->execute();
    }

    #ok, then the master types
    $sql = qq{insert into master_accounts values(? , ?, ?)};
    $st = $stash->get_connection()->prepare($sql);

    for(Bcd::Common::BasicAccounts::LIST_MASTER_ACCOUNTS){
	$st->bind_param(1, $_->[0]);
	$st->bind_param(2, $_->[1]);
	$st->bind_param(3, $_->[2]);
	$st->execute();
    }

    #And, finally, the basic accounts
    $sql = qq{insert into accounts_plan values(?,?,?,?)};
    $st = $stash->get_connection()->prepare($sql);

    for(Bcd::Common::BasicAccounts::LIST_BASIC_ACCOUNTS){
	$st->bind_param(1, $_->[0]);
	$st->bind_param(2, $_->[1]);
	$st->bind_param(3, $_->[2]);
	$st->bind_param(4, $_->[3]);
	$st->execute();
    }
    
}


1;
