package Bcd::Data::Accounting::Transaction;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Data::Accounting::AccountsPlan;
use Bcd::Data::Accounting::Accounts;


use constant{

    INSERT_TRANSACTION => qq{insert into transactions(date, description) values (now(), ?)},
};

#this function simply stores the splits in the db
#it should be regarded as a "static" function
sub add_new_transaction_in_db{

    my ($class, $list_debits, $list_debit_amounts, $list_credits, $list_credit_amounts, 
	$description, $stash) = @_;

    my $sth = $stash->prepare_cached(INSERT_TRANSACTION);
    $sth->bind_param(1, $description);
    $sth->execute();
    $sth->finish();

    my $index = 0;

    my @debit_totals;

    foreach (@{$list_debits}){


	my $new_total = 
	    Bcd::Data::Accounting::Accounts->add_transaction_split
	    ($stash, $_, $list_debit_amounts->[$index], 0);

	$index ++;

	push(@debit_totals, $new_total);
	
    }

    $index = 0; #return to the start
    my @credit_totals;

    foreach(@{$list_credits}){

	my $new_total;

	$new_total = Bcd::Data::Accounting::Accounts->add_transaction_split
	    ($stash, $_, 0, $list_credit_amounts->[$index]);

	$index ++;

	push(@credit_totals, $new_total);
    }

    return (\@debit_totals, \@credit_totals);
    
}

1;



