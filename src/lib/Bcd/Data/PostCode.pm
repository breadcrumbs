package Bcd::Data::PostCode;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU. General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#this file should simply define some static methods to deal with post
#codes.

#The italian post code is simple: it is a 5 digit integer. Not all
#integers are valid. With some exceptions the Post codes are
#increasing from North to South of Italy. The exception is the zone
#around Rome which starts at 00000. The the Post Code from 10000 and
#above are in the upper right corner of Italy: Piemonte, Liguria.

# In the breadcrumbs project I have also the sub code length
use constant{
    POST_CODE_LENGTH => 5,
    SUB_CODE_LENGTH  => 2
};

use constant {
    TOTAL_CODE_LENGTH => POST_CODE_LENGTH + SUB_CODE_LENGTH,
};

#this hash will enumerate all the codes which are too generic,
#because some cities in Italy have subzones
my %generic_post_codes = (
    "60100" => "Ancona",
    "70100" => "Bari",
    "24100" => "Bergamo",
    "40100" => "Bologna",
    "25100" => "Brescia",
    "09100" => "Cagliari",
    "95100" => "Catania",
    "50100" => "Firenze",
    "16100" => "Genova",
    "19100" => "La Spezia",
    "57100" => "Livorno",
    "98100" => "Messina",
    "20100" => "Milano",
    "80100" => "Napoli",
    "35100" => "Padova",
    "90100" => "Palermo",
    "06100" => "Perugia",
    "65100" => "Pescara",
    "56100" => "Pisa",
    "89100" => "Reggio Calabria",
    "00100" => "Roma",
    "84100" => "Salerno",
    "10100" => "Torino",
    "34100" => "Trieste",
    "30100" => "Venezia",
    "28900" => "Verbania",
    "37100" => "Verona",
);

sub is_this_a_generic_post_code{
    my $code = shift;

    if (defined($generic_post_codes{$code})){
	return (1, $generic_post_codes{$code});
    } else {
	return 0;
    }
}

sub is_this_a_test_post_code{
    my $code = shift;

    #the 6th character is a 9
    if ($code =~ /^.{5}9/){
	return 1;
    }

    return 0;
}

#these variables are needed to put them in the regular expression
#below
my $POST_CODE_LENGTH_HACK  = POST_CODE_LENGTH;
my $SUB_CODE_LENGTH_HACK   = SUB_CODE_LENGTH;
my $TOTAL_CODE_LENGTH_HACK = TOTAL_CODE_LENGTH;

sub is_valid_post_code{
    my $code = shift;
    if ($code =~ /^\d{$POST_CODE_LENGTH_HACK}$/o){
	return 1;
    } else {
	return 0;
    }
}

sub is_valid_post_code_with_sub{
    my $code = shift;
    if ($code =~ /^\d{$TOTAL_CODE_LENGTH_HACK}$/o){
	return 1;
    } else {
	return 0;
    }
}

#the search pattern is very simple: from 0 to $TOTAL_CODE_LENGTH
#digits
sub is_valid_search_pattern{
    my $code = shift;
    if ($code =~ /^\d{0,$TOTAL_CODE_LENGTH_HACK}$/o){
	return 1;
    } else {
	return 0;
    }
}

sub get_min_max_search_range{
    my $post_code = shift;
    my $min_code;
    my $max_code;

    if (is_valid_search_pattern($post_code))
    {
	$min_code = $post_code;
	$max_code = $post_code;

	for ( length($post_code) .. TOTAL_CODE_LENGTH-1){
	    $min_code .= '0';
	    $max_code .= '9';
	}
    }

    return ($min_code, $max_code);
}

#converts a seven digit number in this more readable form #####-##
sub pretty_print_post_code{
    my $code = shift;

    return if !is_valid_post_code_with_sub($code);

    my $pretty = substr($code, 0, POST_CODE_LENGTH);
    $pretty .= '-';
    $pretty .= substr($code, POST_CODE_LENGTH-1, SUB_CODE_LENGTH);

    return $pretty;
}

1;
