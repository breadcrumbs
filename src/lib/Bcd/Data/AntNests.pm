package Bcd::Data::AntNests;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).


#this file models the set of ant nests in the system
use strict;
use warnings;
use Bcd::Common::BasicAccounts;
use Bcd::Common::Mail;
use Bcd::Constants::AntNestsConstants;
use Bcd::Data::Accounting::AccountsPlan;
use Bcd::Data::Trust;
use Bcd::Data::PostCode;
use Data::Dumper;

use constant{
    CREATE_ANT_NEST                => 
	qq{insert into ant_nests values(?, }
	   .Bcd::Constants::AntNestsConstants::CREATION_STATE.
	       qq{,now(), ?, ?)},

    SELECT_ANT_NESTS               => "AntNests_select_ant_nests",
    SELECT_ANT_NESTS_MIN_MAX       => "AntNests_select_ant_nests_min_max",
    SELECT_COUNT_ANT_NESTS_MIN_MAX => "AntNests_select_count_min_max",
    CREATE_INITIAL_USERS_ROW       => "AN_create_initial_users_row",
    HOW_MANY_INITIAL_ANTS          => "AN_how_many_initial_ants",
    DECREMENT_INITIAL_ANTS_COUNT   => "AN_decrement_initial_ants_count",
    IS_BOSS_MISSING                => "AN_is_boss_missing",
    IS_TRESAURER_MISSING           => "AN_is_tresaurer_missing",
    NEW_STARTING_BOSS              => "AN_new_starting_boss",
    NEW_STARTING_TREASURER         => "AN_new_starting_tresaurer",
    DELETE_BEGINNING_ANT_NEST      => "AN_delete_beginning_ant_nest",
    UPDATE_ANT_NEST_STATE          => "AN_update_ant_nest_state",

    SELECT_CODES_IN_THIS_RANGE     =>
	qq{SELECT id from ant_nests WHERE id >= ? and id <= ?}


};

sub get_codes_in_this_range_arr {
    my ($class, $stash, $min_code, $max_code) = @_;

    my $st = $stash->prepare_cached(SELECT_CODES_IN_THIS_RANGE);
    $st->bind_param(1, $min_code);
    $st->bind_param(2, $max_code);
    $st->execute();

    return $st->fetchall_arrayref();
}



#this method should simply get the ant nests "near" this post code...
sub get_near_ant_nests{
   my ($class, $stash, $code) = @_;

   #print "My code is $code\n";

   #I simply remove the last two characters...
   my $base_code = substr($code, 0, -2);

   my $code_min = $base_code . '0000';
   my $code_max = $base_code . '9999';

   #print "base code $base_code $code_min $code_max\n";
   
   return $class->get_ant_nests_from_post_code
       ($stash, $code_min, $code_max);
}

#this function creates an ant nest from a booking
sub create_ant_nest_from_booking{
    my ($class, $stash, $code, $name) = @_;

    #the other parameters are undef...
    #first of all I should find a free code for this ant nest
    my $assigned_code = $class->_get_assigned_code($stash, $code);

    #ok, now I can create the ant nest...
    my $st = $stash->prepare_cached(CREATE_ANT_NEST);

    $st->bind_param(1, $assigned_code);
    $st->bind_param(2, $name);
    $st->bind_param(3, undef);
    #$st->bind_param(4, undef);
    #$st->bind_param(5, undef);

    $st->execute();

    #I should create the accounts for this ant nest
    Bcd::Data::Accounting::AccountsPlan->create_ant_nest_accounts($assigned_code, $stash);

    return $assigned_code;
}

sub _get_assigned_code{
    my ($class, $stash, $code) = @_;

    my ($min_code, $max_code) = Bcd::Data::PostCode::get_min_max_search_range($code);

    #I should get an assigned code.
    my $count = $class->get_count_ant_nests_min_max($stash, $min_code, $max_code);

    #this algorithm is not robust, FIXME it does not take into the
    #account the test ant nests

    my $suffix = sprintf("%02s", $count);

    return $code.$suffix;
}


#this function will create a ant nest...
sub create_a_ant_nest{
    
    my ($class, $stash, $code, $name, $motto, $initial_ants) = @_;

    #now I should have the command to create the ant nest...
    my $sth = $stash->prepare_cached(CREATE_ANT_NEST);

    #then I should be able to create the ant nest
    $sth->bind_param(1, $code);
    $sth->bind_param(2, $name);
    $sth->bind_param(3, $motto);
    #$sth->bind_param(4, $hq);
    #$sth->bind_param(5, $openings);

    $sth->execute();

    #ok, now I should simply create the initial count for the ant nest
    $sth = $stash->get_statement(CREATE_INITIAL_USERS_ROW, $class);
    $sth->bind_param(1, $code);
    $sth->bind_param(2, $initial_ants);
    $sth->execute();

    #I should create the accounts for this ant nest
    Bcd::Data::Accounting::AccountsPlan->create_ant_nest_accounts($code, $stash);
    
}

sub a_new_initial_ant_has_been_created{
    my ($class, $stash, $ant_nest_code) = @_;

    my $sth = $stash->get_statement(DECREMENT_INITIAL_ANTS_COUNT, $class);
    $sth->bind_param(1, $ant_nest_code);
    $sth->execute();

    #let's see if this ant was the last ant in the nest
    my $remaining = $class->get_initial_ants_remaining($stash, $ant_nest_code);
    
    if ($remaining == 0){
	#ok, I can build the initial geometry of this ant nest
	$class->_build_initial_geometry($stash, $ant_nest_code);
    }

}

sub _build_initial_geometry{
    my ($class, $stash, $ant_nest_code) = @_;

    #I should get all the ants of this ant nest.
    my $home_ants = Bcd::Data::Users->get_all_ants_in_this_ant_nest($stash, $ant_nest_code);

    my $max_index = $#{$home_ants};
    for (0..$max_index){
	my $pivot_ant = $home_ants->[$_]->[0];
	for (($_+1)..$max_index){
	    my $second_ant = $home_ants->[$_]->[0];
	    Bcd::Data::Trust->create_trust_between($stash, $pivot_ant, $second_ant, 0.99, 0.99);
	}
    }
}

#this function will tell if there are ants remaining
sub get_initial_ants_remaining{
    my ($class, $stash, $ant_nest_code) = @_;

    my $sth = $stash->get_statement(HOW_MANY_INITIAL_ANTS, $class);
    $sth->bind_param(1, $ant_nest_code);
    $sth->execute();
    my $arr = $sth->fetchrow_arrayref();
    $sth->finish();

    return defined($arr) ? $arr->[0] : 0;
}

sub new_starting_boss{
    my ($class, $stash, $ant_nest_code) = @_;

    #I simply see if the tresaurer is missing
    if ($class->is_missing_treasurer($stash, $ant_nest_code)){
	#ok, I simply update the fact that the boss is there
	my $sth = $stash->get_statement(NEW_STARTING_BOSS, $class);
	$sth->bind_param(1, $ant_nest_code);
	$sth->execute();
    } else {
	#ok, the ant nest is ready
	$class->_this_ant_nest_is_ready($stash, $ant_nest_code);
    }
}

sub new_starting_treasurer{
    my ($class, $stash, $ant_nest_code) = @_;

    #I simply see if the tresaurer is missing
    if ($class->is_missing_boss($stash, $ant_nest_code)){
	#ok, I simply update the fact that the boss is there
	my $sth = $stash->get_statement(NEW_STARTING_TREASURER, $class);
	$sth->bind_param(1, $ant_nest_code);
	$sth->execute();
    } else {
	#ok, the ant nest is ready
	$class->_this_ant_nest_is_ready($stash, $ant_nest_code);
    }
}

#ok, this ant nest is ready... I can delete the row in the ant_nests_beginnings table
sub _this_ant_nest_is_ready{
    my ($class, $stash, $ant_nest_code) = @_;

    #ok, I simply have to delete the row from the beginnings
    my $st = $stash->get_statement(DELETE_BEGINNING_ANT_NEST, $class);
    $st->bind_param(1, $ant_nest_code);
    $st->execute();

    #then I should update the state
    $st = $stash->get_statement(UPDATE_ANT_NEST_STATE, $class);
    $st->bind_param(1, Bcd::Constants::AntNestsConstants::TESTING_STATE);
    $st->bind_param(2, $ant_nest_code);
    $st->execute();
}

sub is_missing_boss{
    my ($class, $stash, $ant_nest_code) = @_;

    my $sth = $stash->get_statement(IS_BOSS_MISSING, $class);
    $sth->bind_param(1, $ant_nest_code);
    $sth->execute();
    my $arr = $sth->fetchrow_arrayref();
    $sth->finish();
    
    return defined($arr) ? $arr->[0] : 0;
}

sub is_missing_treasurer{
    my ($class, $stash, $ant_nest_code) = @_;

    my $sth = $stash->get_statement(IS_TRESAURER_MISSING, $class);
    $sth->bind_param(1, $ant_nest_code);
    $sth->execute();
    my $arr = $sth->fetchrow_arrayref();
    $sth->finish();
    
    return defined($arr) ? $arr->[0] : 0;
}

#this is a "static" method....
sub get_ant_nests_list{
    my ($class, $stash) = @_;

    my $sth = $stash->get_statement(SELECT_ANT_NESTS, $class);

    $sth->execute();

    #I slurp all the rows...
    return $sth->fetchall_arrayref();
}

sub get_cash_euro_account_id{
    my ($class, $stash, $ant_nest_code) = @_;

    #I simply build the name
    my $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_CASH, $ant_nest_code);
    my $id = Bcd::Data::Accounting::AccountsPlan->get_id_account_from_name($name, $stash);

    return $id;
}

sub get_parking_euro_account_id{
    my ($class, $stash, $ant_nest_code) = @_;

    my $name = sprintf(Bcd::Common::BasicAccounts::PARKING_EURO_FUND_RAD, $ant_nest_code);
    my $id = Bcd::Data::Accounting::AccountsPlan->get_id_account_from_name($name, $stash);

    return $id;
    
}

sub get_euro_exchange_partial_income_id{
    my ($class, $stash, $ant_nest_code) = @_;

    return Bcd::Data::Accounting::AccountsPlan->get_ant_nest_income_account_id
	(
	 $stash, 
	 Bcd::Common::BasicAccounts::EXCHANGE_INCOMES,
	 $ant_nest_code,
	 Bcd::Common::BasicAccounts::EURO,
	 Bcd::Common::BasicAccounts::PARTIAL
	 );

}

sub get_euro_deposit_partial_income_id{
    my ($class, $stash, $ant_nest_code) = @_;

    return Bcd::Data::Accounting::AccountsPlan->get_ant_nest_income_account_id
	(
	 $stash, 
	 Bcd::Common::BasicAccounts::DEPOSITS_INCOMES,
	 $ant_nest_code,
	 Bcd::Common::BasicAccounts::EURO,
	 Bcd::Common::BasicAccounts::PARTIAL
	 );
									       
}

sub get_tao_ads_partial_income_id{
    my ($class, $stash, $ant_nest_code) = @_;

    return Bcd::Data::Accounting::AccountsPlan->get_ant_nest_income_account_id
	(
	 $stash, 
	 Bcd::Common::BasicAccounts::OBJECTS_INCOMES,
	 $ant_nest_code,
	 Bcd::Common::BasicAccounts::TAO,
	 Bcd::Common::BasicAccounts::PARTIAL
	 );
									       
}

sub get_tao_cheques_partial_income_id{
    my ($class, $stash, $ant_nest_code) = @_;

    return Bcd::Data::Accounting::AccountsPlan->get_ant_nest_income_account_id
	(
	 $stash, 
	 Bcd::Common::BasicAccounts::CHEQUES_INCOMES,
	 $ant_nest_code,
	 Bcd::Common::BasicAccounts::TAO,
	 Bcd::Common::BasicAccounts::PARTIAL
	 );
									       
}

sub get_users_accounts_id{
    my ($class, $stash, $ant_nest_code) = @_;

    my $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_USER_ACC_E, $ant_nest_code);
    my $act_e_id = Bcd::Data::Accounting::AccountsPlan->get_id_account_from_name($name, $stash);

    #the same with the tao
    $name = sprintf(Bcd::Common::BasicAccounts::ANT_NEST_USER_ACC_T, $ant_nest_code);
    my $act_t_id = Bcd::Data::Accounting::AccountsPlan->get_id_account_from_name($name, $stash);

    return ($act_e_id, $act_t_id);
}

sub get_ant_nest_state{
    my ($class, $stash, $ant_nest_code) = @_;

    #I simply get the ant nest
    my $st = $stash->get_statement(SELECT_ANT_NESTS_MIN_MAX, $class);
    $st->bind_param(1, $ant_nest_code);
    $st->bind_param(2, $ant_nest_code);

    $st->execute();
    my $arr_ref = $st->fetchrow_arrayref();
    $st->finish();

    return $arr_ref->[1];
}

sub is_this_ant_nest_starting{
    my ($class, $stash, $ant_nest_code) = @_;

    my $state = $class->get_ant_nest_state($stash, $ant_nest_code);
    if (!defined($state)){
	return 0; #probably the ant nest is not existing
    }

    if (
	$state == Bcd::Constants::AntNestsConstants::CREATION_STATE
	){
	return 1;
    } else {
	return 0;
    }
}

sub is_this_ant_nest_active{
   my ($class, $stash, $ant_nest_code) = @_;

   my $state = $class->get_ant_nest_state($stash, $ant_nest_code);

   #not existing ant nest
   if (!defined($state)){
       return 0;
   }

   if (
       $state == Bcd::Constants::AntNestsConstants::TESTING_STATE
       or
       $state == Bcd::Constants::AntNestsConstants::RUNNING_STATE
       or
       $state == Bcd::Constants::AntNestsConstants::DYING_STATE
       ){
       return 1;
   }
   else{
       return 0;
   }
}

#this function is called whenever there is a connection without the
#commands initialized
sub populate_the_stash{

    #this function should be called by the connection threads.
    my ($self, $db_stash) = @_;

    #ok, let's build the commands...
    my $sql;
    my $sth;

    $sql = qq{select * from ant_nests};
    $sth = $db_stash->get_connection()->prepare ( $sql );
    $db_stash->insert_statement(SELECT_ANT_NESTS, $sth);

    $sql = qq{select * from ant_nests where id>=? AND id<=?};
    $sth = $db_stash->get_connection()->prepare ( $sql );
    $db_stash->insert_statement(SELECT_ANT_NESTS_MIN_MAX, $sth);

    $sql = qq{select count(*) from ant_nests where id>=? AND id<=?};
    $sth = $db_stash->get_connection()->prepare ( $sql );
    $db_stash->insert_statement(SELECT_COUNT_ANT_NESTS_MIN_MAX, $sth);

    $sql = qq{insert into ant_nests_beginnings values(?,?)};
    $sth = $db_stash->get_connection()->prepare ( $sql );
    $db_stash->insert_statement(CREATE_INITIAL_USERS_ROW, $sth);

    $sql = qq{select how_many_remaining_ants from ant_nests_beginnings where id=?};
    $db_stash->record_this_statement(HOW_MANY_INITIAL_ANTS, $sql);

    $sql = qq{update ant_nests_beginnings set how_many_remaining_ants = how_many_remaining_ants - 1 where id=?};
    $db_stash->record_this_statement(DECREMENT_INITIAL_ANTS_COUNT, $sql);

    $sql = qq{select missing_boss from ant_nests_beginnings where id=?};
    $db_stash->record_this_statement(IS_BOSS_MISSING, $sql);

    $sql = qq{select missing_tresaurer from ant_nests_beginnings where id=?};
    $db_stash->record_this_statement(IS_TRESAURER_MISSING, $sql);

    $sql = qq{UPDATE ant_nests_beginnings SET missing_boss = '0' where id=?};
    $db_stash->record_this_statement(NEW_STARTING_BOSS, $sql);

    $sql = qq{UPDATE ant_nests_beginnings SET missing_tresaurer = '0' where id=?};
    $db_stash->record_this_statement(NEW_STARTING_TREASURER, $sql);

    $sql = qq{DELETE FROM  ant_nests_beginnings WHERE id=?};
    $db_stash->record_this_statement(DELETE_BEGINNING_ANT_NEST, $sql);

    $sql = qq{UPDATE ant_nests SET state_id = ?  WHERE id=?};
    $db_stash->record_this_statement(UPDATE_ANT_NEST_STATE, $sql);
    
}

#this function is one shot... so it does not put the commands in the
#stash
sub init_db{
    my ($self, $stash) = @_;
    my $conn = $stash->get_connection();

    #ok, now I should insert the values in the table...
    my $sth = $conn->prepare(qq{insert into ant_nest_states values(?,?)});

    foreach (Bcd::Constants::AntNestsConstants::LIST_ANT_NEST_STATES){
	$sth->bind_param(1, $_->[0]);
	$sth->bind_param(2, $_->[1]);
	$sth->execute();
    }

    $sth->finish();



}

#this method should simply get the ant nests from the post code.  the
#code is in the form dd**, it starts with digits and then it can have
#some wildcards
sub get_ant_nests_from_post_code{
    my ($class, $stash, $code_min, $code_max) = @_;

    #I should simply return the ant nests between two post codes.

    my $sth = $stash->get_statement(SELECT_ANT_NESTS_MIN_MAX, $class);

    $sth->bind_param(1, $code_min);
    $sth->bind_param(2, $code_max);

    $sth->execute();

    #print "I will return these columns\n";
    #print Dumper($var);
    my $var = $sth->{NAME_lc};
    my $arr = $sth->fetchall_arrayref();

    #I put at the front of the recordset the columns names.
    unshift (@{$arr}, $var);

    return $arr; 
}

#this method returns only the count of the ant nests in a particular range
sub get_count_ant_nests_min_max{
    my ($class, $stash, $code_min, $code_max) = @_;

    my $sth = $stash->get_statement(SELECT_COUNT_ANT_NESTS_MIN_MAX, $class);

    $sth->bind_param(1, $code_min);
    $sth->bind_param(2, $code_max);

    $sth->execute();

    my $count;
    $sth->bind_columns(undef, \$count);
    $sth->fetch();
    $sth->finish();

    return $count;
}

sub get_ant_nest_detail{
    my ($class, $stash, $code) = @_;

    my $st = $stash->get_statement(SELECT_ANT_NESTS_MIN_MAX, $class);
    $st->bind_param(1, $code);
    $st->bind_param(2, $code);
    
    $st->execute();
    my $data = $st->fetchrow_arrayref();
    $st->finish();

    return $data;
    
}


1;
