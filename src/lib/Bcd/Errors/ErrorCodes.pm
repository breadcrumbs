package Bcd::Errors::ErrorCodes;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

# BEC => Breadcrumbs Error Code

#this file simply stores the constant error codes of the server

use constant {

    BEC_OK                                           =>    0, #all ok, output follows
    BEC_UNKNOWN_COMMAND                              =>    1, 
    BEC_NOT_IMPLEMENTED                              =>    2, 
    BEC_UNAUTHORIZED                                 =>    3, 
    BEC_INVALID_TOKEN                                =>    4,
    BEC_INVALID_TOTEM                                =>    5,
    BEC_INVALID_CURRENCY                             =>    6,
    BEC_INVALID_STATE                                =>    7,
    BEC_NO_DATA                                      =>    8,
    BEC_DATA_NOT_CHANGED                             =>    9,
    BEC_DATA_NOT_READY                               =>   10,

#Users errors    
    BEC_UNKNOWN_USER                                 =>  100,
    BEC_INVALID_PASSWORD                             =>  101,
    BEC_USER_ALREADY_EXISTING                        =>  102, 
    BEC_NOT_CONNECTED                                =>  104,
    BEC_RESERVED_ID                                  =>  105,
    BEC_LOGIN_DENIED_ANT_NEST_NOT_READY              =>  106,
    BEC_CANNOT_GRANT_BC_ROOT_ROLE                    =>  107,
    BEC_DUPLICATE_INITIAL_BOSS_OR_TREASURER          =>  108,
    BEC_LENGTH_OR_CHAR_NOT_VALID_IN_ID               =>  109,
    BEC_ACCOUNT_NOT_READY                            =>  110,
    BEC_THE_TWO_TUTORS_MUST_BE_DIFFERENT             =>  111,
    BEC_THE_TWO_TUTORS_MUST_TRUST_THEMSELVES         =>  112,
    BEC_NEW_USER_ALREADY_CONFIRMED                   =>  113,
    BEC_INVALID_STATE_FOR_THIS_USER                  =>  114,
    BEC_CANNOT_CHANGE_PASSWORD_FOR_TEST_USERS        =>  115,


#bank errors
    BEC_ACCOUNT_NOT_ZEROED                           =>  200,
    BEC_PENDING_BOOKING_PRESENT                      =>  201,
    BEC_DEPOSIT_TOO_SMALL                            =>  202,
    BEC_DEPOSIT_TOO_BIG                              =>  203,
    BEC_UNKNOWN_DEPOSIT                              =>  204,
    BEC_INVALID_SECRET_TOKEN                         =>  205,
    BEC_THIS_BOOKING_IS_A_WITHDRAWAL                 =>  206,
    BEC_THIS_BOOKING_HAS_BEEN_ACKNOWLEDGED           =>  207,
    BEC_THIS_BOOKING_HAS_BEEN_COLLECTED              =>  208,
    BEC_WITHDRAWAL_TOO_SMALL                         =>  209,
    BEC_WITHDRAWAL_TOO_BIG                           =>  210,
    BEC_WITHDRAWAL_WILL_RENDER_NEGATIVE_THE_ACCOUNT  =>  211,
    BEC_THIS_BOOKING_IS_A_DEPOSIT                    =>  212,
    BEC_UNKNOWN_BOOKING                              =>  213,
    BEC_THIS_IS_A_DEPOSIT_NOT_YET_COLLECTED          =>  214,
    BEC_INSUFFICIENT_FUNDS                           =>  215,



#post code errors
    BEC_INVALID_POST_CODE                            =>  300,

#ant nests errors
    BEC_EXISTING_ANT_NEST_CODE                       =>  400,
    BEC_EVEN_INITIAL_SIZE                            =>  401,
    BEC_INITIAL_SIZE_OUT_OF_RANGE                    =>  402,
    BEC_NO_MORE_INITIAL_ANTS_PLEASE                  =>  403,
    BEC_TRYING_TO_CREATE_ANT_OUTSIDE_YOUR_NEST       =>  404,
    BEC_NO_ANT_NEST_IN_THIS_CODE                     =>  405,
    BEC_CANNOT_CREATE_AN_ANT_NEST_IN_GENERIC_CODE    =>  406,
    BEC_NO_MORE_FOUNDERS_FOR_NOW                     =>  407,
    BEC_YOU_CANNOT_BE_BOSS_AND_TREASURER             =>  408,
    BEC_INVALID_VOTE                                 =>  409,
    BEC_THIS_ANT_NEST_HAS_NOT_BEEN_CREATED_YET       =>  410,
    BEC_THIS_ANT_NEST_HAS_BEEN_CREATED               =>  411,

#Trust errors
    BEC_INVALID_TRUST                                =>  500,
    BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST       =>  501,
    BEC_DUPLICATE_TRUST_NOT_ALLOWED                  =>  502,

    BEC_NO_TRUST_TO_CHANGE                           =>  504,
    BEC_DUPLICATE_BOOKING                            =>  505,
    BEC_UNKNOWN_PERSONAL_DATA_ITEM                   =>  506,
    BEC_INSUFFICIENT_TRUST                           =>  507,

#Activities errors
    BEC_INVALID_TREE_REQUESTED                       =>  600,

#Sites, presences errors
    BEC_INVALID_PRESENCE_SPECIFICATION               =>  700,
    BEC_INVALID_SPECIAL_SITE_SPECIFICATION           =>  701,
    BEC_SITE_OUTSIDE_YOUR_NEST                       =>  702,
    BEC_UNKNOWN_PRESENCE                             =>  703,

#Ads errors
    BEC_AD_LOCALITY_MISMATCH                         =>  800,
    BEC_CANNOT_BUY_FROM_YOURSELF                     =>  801,
    BEC_YOU_DO_NOT_OWN_THIS_AD                       =>  802,
    BEC_THIS_INVOICE_IS_NOT_FOR_YOU                  =>  803,
    BEC_UNKNOWN_AD                                   =>  804,
    BEC_DUPLICATE_AD_LOCALITY                        =>  805,
    BEC_INCOMPATIBLE_PRESENCE_TYPE                   =>  806,
    BEC_CANNOT_EDIT_ACTIVE_PRESENCE                  =>  807,
    BEC_AD_WITH_BOTH_LOCI_PRESENT                    =>  808,



#THESE ARE "SYNTACTICAL" ERRORS
    BEC_TOO_CHARACTERS_IN_INPUT_LINE                 => 7001,
    BEC_TOO_FEW_PARAMETERS                           => 7002,
    BEC_TOO_MANY_PARAMETERS                          => 7003,
    BEC_INVALID_NUMBER_OF_PARAMETERS                 => 7004,
    BEC_REQUIRED_PARAMETER_MISSING                   => 7005,
    BEC_BOOLEAN_PARAMETER_OUT_OF_RANGE               => 7006,
    BEC_INTEGER_OUT_OF_RANGE                         => 7007,
    BEC_ENUM_VALUE_UNDEFINED                         => 7008,
    BEC_INVALID_ID                                   => 7009,
    BEC_INVALID_TAO_IMPORT                           => 7010,
    BEC_INVALID_EMAIL                                => 7011,



#mainly internal errors, you should not receive them, I hope
    
    #a command could not complete, repeating can be useful
    BEC_ABORTED_TRANSACTION                          => 9998, 
    #bug in the server, contact administrator
    BEC_INTERNAL_ERROR                               => 9999
};


1;
