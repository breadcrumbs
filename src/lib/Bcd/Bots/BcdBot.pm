package Bcd::Bots::BcdBot;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this bot will simply watch the creation of the ant nests...

use strict;
use warnings;
use Data::Dumper;
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($INFO);

sub new {
    my $class = shift;
    my $self = {};

    bless($self, $class);
    return $self;
}


sub _add_rule{
    my ($self, $rule) = @_;

    push(@{$self->{rules}}, $rule);
}

#this here is a pure function.
sub get_frequency{
    die;
}

#this function is called whenever the bot is awakened after a cycle.
sub awake{
    die;
}

sub _go{
    my ($self, $stash, $ant_nest_to_check, $ignore_transaction) = @_;

    my $cycle_finished = 0;

    my $rules_fired = [];
    my $exit_flag = 0;

    #each rule should perform a transaction, if it fires.
    foreach (@{$self->{rules}}){

	eval {
	    my $fired = $_->($self, $stash, $ant_nest_to_check);
	    if (defined($fired)){
		push (@{$rules_fired}, $fired);
		get_logger()->info($fired) if (! $ignore_transaction);
		$stash->get_connection()->commit() if (! $ignore_transaction);
		#a rule has fired, this cycle has finished...
		$exit_flag = 1;
	    }

	};

	if ($@){
	    print STDERR "Transation aborted in the server... error: $@\n";

	    eval{ 
		$stash->get_connection()->rollback() if ( ! $ignore_transaction);
	    };
	    if ($@){
		#I cannot even rollback... this is very serious...
		print STDERR "I cannot even rollback... error: $@\n";
	    }
	}

	last if ($exit_flag == 1);
    }

    if (scalar(@{$rules_fired}) == 0 and ! $ignore_transaction){
	#no rule has fired, advance the ant nest, if I am not inside a test...
	$cycle_finished = $self->_advance_ant_nest($stash) == 0 ? 1 : 0;
    }

    #if at least a rule has fired the cycle is not finished.
    #print Dumper($rules_fired);
    return ($rules_fired, $cycle_finished);
}

sub _advance_ant_nest{
    die "pure function called";
}


#this is called by the manager, I call the _go inside a transaction
sub go{
    my ($self, $stash) = @_;

    return $self->_go($stash, $self->{current_ant_nest}, 0);
}

#this is called by the tests. They are identical, but the test does
#not make a commit
sub go_test{
    my ($self, $stash, $ant_nest_to_check) = @_; 

    #I return the list of rules fired...
    return $self->_go($stash, $ant_nest_to_check, 1); #ignore the transactions
}


1;
