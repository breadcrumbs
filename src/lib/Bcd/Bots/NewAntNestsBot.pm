package Bcd::Bots::NewAntNestsBot;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this bot will simply watch the creation of the ant nests...

use strict;
use warnings;
use Data::Dumper;


use Bcd::Data::Founders;
use Bcd::Data::FoundersVotings;
use Bcd::Data::Users;
use Bcd::Constants::NewAntNestsConstants;
use Bcd::Bots::BcdBot;
use base(qw/Bcd::Bots::BcdBot/);

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($DEBUG);

#all times in seconds
use constant {
    MAXIMUM_TIME_TO_CHECK_IN => 86400,
    MY_FREQUENCY             => 7200,
};


#of course you could use the %s syntax to perform a substitution...
use constant {

    SELECT_FIRST_NEW_ANT_NEST  =>
	qq{SELECT id from ant_nests_bookings ORDER BY id LIMIT 1},

    SELECT_NEXT_NEW_ANT_NEST  =>
	qq{SELECT id from ant_nests_bookings WHERE id > ? ORDER BY id LIMIT 1},

    DELETE_INITIAL_VOTINGS     =>
    qq{DELETE FROM ant_nest_initial_votings WHERE id_ant_nest = ?},

    DELETE_LOOKERS             =>
    qq{DELETE FROM ant_nests_looking_users WHERE id_ant_nest = ? },

    DELETE_FOUNDERS_TRUSTS     =>
    qq{DELETE FROM ant_nests_founders_trusts WHERE u1 = ? or u2 = ?},

    SELECT_OLD_FOUNDERS_STATE  =>
	qq{SELECT * from ant_nests_founders where id_status = ? and id_ant_nest = ? } .
	qq{and localtimestamp - last_status_change > interval '} . MAXIMUM_TIME_TO_CHECK_IN . " seconds'",


};

#"

sub new {
    my ($class, $server) = @_;
    my $self = $class->SUPER::new(@_);

    $self->_add_rule(\&delete_founders_ants_not_checked);
    $self->_add_rule(\&all_ants_have_checked_in);
    $self->_add_rule(\&all_ants_have_immitted_data);
    $self->_add_rule(\&all_ants_have_built_the_web_of_trust);
    $self->_add_rule(\&after_the_candidate_choice_rule);
    $self->_add_rule(\&votes_count_rule);
    $self->_add_rule(\&new_ant_nest_final_rule);


    $self->{my_frequency} = defined($server->{new_ant_nest_bot_frequency}) ? 
	$server->{new_ant_nest_bot_frequency} : MY_FREQUENCY;


    bless($self, $class);
    return $self;
}

sub get_frequency {
    my $self = shift;
    return $self->{my_frequency};
}

#I should start a new cycle.
sub awake{
    my ($self, $stash) = @_;

    my $st = $stash->prepare_cached(SELECT_FIRST_NEW_ANT_NEST);
    $st->execute();

    my $row = $st->fetch();
    $st->finish();

    if ( ! defined($row)){
	#nothing to do
	return 0;
    } else {
	$self->{current_ant_nest} = $row->[0];
	return 1;
    }
}

sub _advance_ant_nest{
    my ($self, $stash) = @_;

    my $st = $stash->prepare_cached(SELECT_NEXT_NEW_ANT_NEST);
    $st->bind_param(1, $self->{current_ant_nest});

    $st->execute();

    my $row = $st->fetch();
    $st->finish();

    if (! defined($row)){
	return 0;
    } else {
	$self->{current_ant_nest} = $row->[0];
	return 1;
    }
}

#this is the final rule, it is triggered when the ant nest has
#finished the creation phase
sub new_ant_nest_final_rule{
    my ($self, $stash, $ant_nest_to_check) = @_;

    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr($stash, $ant_nest_to_check);
    return undef if (!defined($ant_nest));
    return undef if ($ant_nest->[1] != Bcd::Constants::NewAntNestsConstants::WAITING_FOR_INITIAL_SET_UP);

    #ok, the ant nest is in the right state... now I simply see if the boss and the treasurer
    #have finished their setup
    my $count = Bcd::Data::NewAntNests->get_founders_with_state_count
	($stash, $ant_nest_to_check, Bcd::Constants::FoundersConstants::SET_UP_DONE);

    #I have two ants in this state, they are the boss and the treasurer
    if ($count != 2){
	return undef;
    }

    my $assigned_code = $ant_nest->[4];

    my $template_vars = {
	id_ant_nest      => $ant_nest_to_check,
	assigned_code    => $assigned_code,
    };

    #ok, I can send the mail

    $self->_mail_to_all_the_founders_and_change_their_state
	($stash, $ant_nest_to_check, "ant_nest_created.tt2", 
	 Bcd::Constants::FoundersConstants::FINISHED_CREATED_AS_AN_ANT, $template_vars);

    #I can update the ant nest state...
    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $ant_nest_to_check, 
	 Bcd::Constants::NewAntNestsConstants::CREATED);

    #ok, now I can update also the state for all the ants in the assigned code
    Bcd::Data::Users->update_state_of_all_users_in_ant_nest
	($stash, $ant_nest->[4], Bcd::Constants::Users::NORMAL_ACTIVE_ANT);

    #ok, I can mail all the observers... to do... this is not a priority
    $self->_mail_all_the_observers($stash, $ant_nest_to_check, "observed_ant_nest_is_ready.tt2", $template_vars);
    

    #I must change the state of the ant nest...
    Bcd::Data::AntNests->_this_ant_nest_is_ready($stash, $assigned_code);

    #the last act is the removal of all the data of this ant nest
    $self->_delete_the_new_ant_nest_booking($stash, $ant_nest_to_check);

    return "Final creation: the ant nest with code=$assigned_code is ready";    
}

sub _delete_the_new_ant_nest_booking{
    my ($self, $stash, $ant_nest_to_check) = @_;


    #ok, I have to delete all the things which were of this ant nest...

    #the votings
    $stash->prep_exec(DELETE_INITIAL_VOTINGS, $ant_nest_to_check);
    #the lookers
    $stash->prep_exec(DELETE_LOOKERS, $ant_nest_to_check);

    #the trusts...
    my $founders = Bcd::Data::Founders->get_all_founders_ant_nest_arr
	($stash, $ant_nest_to_check);

    foreach(@{$founders}){
	$stash->prep_exec(DELETE_FOUNDERS_TRUSTS, $_->[0], $_->[0]);
	#then I can delete also the user...
	#the user in this case it is not lazy, the function should change name
	$self->_delete_this_lazy_to_check_in_founder_ant($stash, $_->[0]);
    }

    #ok, now I can delete also the new ant nest
    $self->_delete_this_new_ant_nest_aborted($stash, $ant_nest_to_check);

}

sub _mail_all_the_observers{
    my ($self, $stash, $code, $letter_to_send, $template_vars) = @_;

    my $lookers = Bcd::Data::NewAntNests->get_all_the_observers($stash, $code);

    foreach(@{$lookers}){
	$template_vars->{name} = $_->[1];
	$stash->get_mailer()->mail_this_message
	    ($stash, $_->[2], $letter_to_send, $template_vars);
    }

    
}

#this rule is used whenever the ants have finished to vote...
sub votes_count_rule{
    my ($self, $stash, $ant_nest_to_check) = @_;

    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr($stash, $ant_nest_to_check);
    return undef if (!defined($ant_nest));
    return undef if ($ant_nest->[1] != Bcd::Constants::NewAntNestsConstants::MAKING_POOL);

    my $count = Bcd::Data::NewAntNests->get_founders_with_state_count
	($stash, $ant_nest_to_check, Bcd::Constants::FoundersConstants::VOTED);


    if ($count != Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){
	return undef;
    }

    #ok, all have voted... let's get the votes...
    my $boss_vote      = 0;
    my $treasurer_vote = 0;

    my $bosses     = Bcd::Data::Founders->get_candidates_bosses    ($stash, $ant_nest_to_check);
    my $treasurers = Bcd::Data::Founders->get_candidates_treasurers($stash, $ant_nest_to_check);

    #ok, now I should be able to convert this array in a hash... just for convenience

    my $bosses_hash     = {};
    my $treasurers_hash = {};

    foreach(@{$bosses}){
	$bosses_hash->{$_->[0]} = $_->[1];
    }

    foreach(@{$treasurers}){
	$treasurers_hash->{$_->[0]} = $_->[1];
    }

    my $winner_boss;
    my $winner_treasurer;

    #I should see if this ant nest has voted for boss or treasurer
    if (scalar(@{$bosses}) > 1){
	$boss_vote = 1;
    } else {
	#the winner is this without any vote
	$winner_boss = $bosses->[0]->[0];
    }

    if (scalar(@{$treasurers}) > 1){
	$treasurer_vote = 1;
    } else {
	$winner_treasurer = $treasurers->[0]->[0];
    }

    #ok, now I should get the votes
    my $votes = Bcd::Data::FoundersVotings->get_votes_result($stash, $ant_nest_to_check);

    #I should check the validity of the bosses votes
    my $fail = 0;

    if ( $boss_vote == 1 ){
	($fail, $winner_boss) = $self->_check_votes_validity_fail($votes->{bosses})
    }

    #print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FAIL $fail \n";

    if ( $fail == 0 and $treasurer_vote == 1){
	($fail, $winner_treasurer) = $self->_check_votes_validity_fail($votes->{treasurers});
    }

    #print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FAIL after t $fail \n";

    my $template_vars = {
	bosses           => $bosses_hash,
	treasurers       => $treasurers_hash,
	votes            => $votes,
	winner_boss      => $winner_boss,
	winner_treasurer => $winner_treasurer,
	boss_vote        => $boss_vote,
	treasurer_vote   => $treasurer_vote,
	id_ant_nest      => $ant_nest_to_check,
    };

    if ($fail == 1) {

	$template_vars->{vote_result} = '0';

	#ko, vote is not valid
	$self->_mail_to_all_the_founders_and_change_their_state
	    ($stash, $ant_nest_to_check, "initial_votes_result.tt2", 
	     Bcd::Constants::FoundersConstants::NO_CHOICE_OF_CANDIDATE, $template_vars);

	Bcd::Data::NewAntNests->update_ant_nest_state
	    ($stash, $ant_nest_to_check, Bcd::Constants::NewAntNestsConstants::CANDIDATES_RESEARCH);
	return "vote failed, downgraded to CANDIDATES_RESEARCH";

    } else {
	#ok, vote valid
	$template_vars->{vote_result} = '1';



	my $assigned_code = 
	    $self->_create_ant_nest($stash, $template_vars, $ant_nest->[2], $winner_boss, $winner_treasurer);

	#advance the state of the ant nest and the ants...
        $self->_mail_to_all_the_founders($stash, $ant_nest_to_check, 
					 "initial_votes_result.tt2",
					 $template_vars);

	#and then I grant the role to boss and treasurer in the new ant nest
	
	return "vote OK, created ant nest code=$assigned_code";
    }


}

sub _create_ant_nest{

    my ($self, $stash, $template_vars, $name_ant_nest, $winner_boss, $winner_treasurer) = @_;

    #print "[[[[[[[[[\n\n\n ok you have won boss $winner_boss treasurer $winner_treasurer \n\n\n]]]]]]]]]";

    #nominate the boss and treasurer, reset their flag
    Bcd::Data::Founders->nominate_boss_and_treasurer
	($stash, $template_vars->{id_ant_nest}, $winner_boss, $winner_treasurer);

    #create the new ant nest... to comunicate to the ants that has been created...
    my $assigned_code = Bcd::Data::AntNests->create_ant_nest_from_booking
	($stash, $template_vars->{id_ant_nest}, $name_ant_nest);

    $template_vars->{assigned_code} = $assigned_code;    

    #ok, now I should create the users..., I get the ids of the boss and treasurer
    #because I grant to them the role
    my ($boss_id, $treasurer_id) = 
	$self->_create_the_users($stash, $template_vars->{id_ant_nest}, $assigned_code);

    #print "\nI HAVE new boss $boss_id and treasurer $treasurer_id\n";
    $self->_grant_role_to_boss_and_treasurer($stash, $boss_id, $treasurer_id);


    Bcd::Data::NewAntNests->update_ant_nest_state
	($stash, $template_vars->{id_ant_nest}, 
	 Bcd::Constants::NewAntNestsConstants::WAITING_FOR_INITIAL_SET_UP);

    #You should put the $assigned_code into the table of the ant nest booking
    Bcd::Data::NewAntNests->update_assigned_id
	($stash, $template_vars->{id_ant_nest}, $assigned_code);
    
    #send instructions to treasurer and boss to connect to the site and do the initial setup
    #to do... but this is simple...

    $self->_send_instructions_to_new_boss_and_treasurer
	($stash, $winner_boss, $winner_treasurer, $template_vars);

    return $assigned_code;

}

sub _grant_role_to_boss_and_treasurer{
    my ($self, $stash, $boss_id, $treasurer_id) = @_;

    #grant the role to these two users.
    Bcd::Data::Users->grant_role_to_user_id($stash, $boss_id,      Bcd::Data::Users::BOSS_ROLE);
    Bcd::Data::Users->grant_role_to_user_id($stash, $treasurer_id, Bcd::Data::Users::TREASURER_ROLE);
}

sub _send_instructions_to_new_boss_and_treasurer{
    my ($self, $stash, $winner_boss, $winner_treasurer, $template_vars) = @_;

    #ok, first I should get the boss...
    my $boss = Bcd::Data::Founders->get_founder_with_email($stash, $winner_boss);
    $self->_mail_to_a_founder($stash, $boss, "new_boss_mail.tt2", $template_vars);

    #and then the treasurer.
    my $treasurer = Bcd::Data::Founders->get_founder_with_email($stash, $winner_treasurer);
    $self->_mail_to_a_founder($stash, $treasurer, "new_treasurer_mail.tt2", $template_vars);
    
}

sub _create_the_users{
    my ($self, $stash, $booking_code, $assigned_code) = @_;

    #Ok, I should get all the founders...
    my $founders = Bcd::Data::Founders->get_all_founders_ant_nest_arr($stash, $booking_code);
    my $boss_id;
    my $treasurer_id;

    #this hash stores the connection between old and new ids.
    my %old_to_new_id;

    foreach(@{$founders}){

	my $user_id = Bcd::Data::Users->create_user_from_booking
	    ($stash, $assigned_code, $_->[3], $_->[5], $_->[6]);

	my $old = $_->[0];

	#print "THE USER WHO was $old now is $user_id\n";
	#my $user_new = Bcd::Data::Users->select_user_data($stash, $user_id);
	#my $user_new_from_db = $user_new->{id};
	#my $nick = $user_new->{nick};
	#my $ant_nest = $user_new->{id_ant_nest};
	#print "From the db I have id=$user_new_from_db nick=$nick, ant_nest=$ant_nest\n";
	
	$old_to_new_id{$old} = $user_id;

	  #is he a boss?
	  if ($_->[7] == '1'){
	      $boss_id = $user_id;
	  } elsif ($_->[8] == '1'){
	      $treasurer_id = $user_id;
	  } 
	  
    }

    #ok, now I have the correspondence, I can transfer also the trusts...
    $self->_transfer_trusts($stash, $founders, \%old_to_new_id);

    return ($boss_id, $treasurer_id);
}

sub _transfer_trusts{
    my ($self, $stash, $founders, $old_to_new_id) = @_;

    #I have another cycle
    foreach(@{$founders}){
	my $trusts_to_move = Bcd::Data::FoundersTrusts
	    ->get_all_trusts_for_this_user_arr($stash, $_->[0]);

	foreach(@{$trusts_to_move}){

	    my $new_1 = $old_to_new_id->{$_->[0]};
	    my $new_2 = $old_to_new_id->{$_->[1]};

	    #print "ADDING>>>>>>>>>>>>>>>>>>>> trust between $new_1 and $new_2\n";

	    Bcd::Data::Trust->create_trust_between
		(
		 $stash,
		 $new_1,
		 $new_2,
		 $_->[2],
		 $_->[3]
		 );
		 
	}
    }

    
}

sub _check_votes_validity_fail{
    my ($self, $votes) = @_;

    my $winner_id;
    my $winner_votes = 0;
    my $number_of_winners = 2; #initially fail

    for (@{$votes}){
	if (defined($_->[0])){
	    #this is a vote to a real person
	    if ($_->[1] > $winner_votes){
		#I have a new winner
		$winner_id    = $_->[0];
		$winner_votes = $_->[1];
		$number_of_winners = 1;
	    } elsif ($_->[1] == $winner_votes){
		$number_of_winners++;
	    }
	}
    }

    if ($number_of_winners == 1){
	#all ok
	return (0, $winner_id);
    } else {
	#fail
	return (1, undef);
    }
	
}


#this rule is called whenever the founders have chosen their candidates
sub after_the_candidate_choice_rule{
    my ($self, $stash, $ant_nest_to_check) = @_;

    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr($stash, $ant_nest_to_check);
    return undef if (!defined($ant_nest));
    return undef if ($ant_nest->[1] != Bcd::Constants::NewAntNestsConstants::CANDIDATES_RESEARCH);

    my $count = Bcd::Data::NewAntNests->get_founders_with_state_count
	($stash, $ant_nest_to_check, Bcd::Constants::FoundersConstants::CHOICE_MADE);


    if ($count != Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){
	return undef;
    }

    #ok, all the ants have made a choice.
    
    my $count_of_bosses;
    my $count_of_treasurers;

    my $bosses     = Bcd::Data::Founders->get_candidates_bosses    ($stash, $ant_nest_to_check);
    my $treasurers = Bcd::Data::Founders->get_candidates_treasurers($stash, $ant_nest_to_check);

    $count_of_bosses     = scalar(@{$bosses});
    $count_of_treasurers = scalar(@{$treasurers});

    my $template_vars = {
	bosses    => $bosses,
	treasurers => $treasurers,
	id_ant_nest => $ant_nest_to_check,
    };

    #print Dumper($bosses);
    #print Dumper($treasurers);

    if ($count_of_treasurers == 0 or $count_of_bosses == 0){
	#this step is not valid... You should repeat it

	#all the founders downgraded to NO_CHOICE_OF_CANDIDATE

	$self->_mail_to_all_the_founders_and_change_their_state
	    ($stash, $ant_nest_to_check, "no_candidates_you_have_to_repeat.tt2", 
	     Bcd::Constants::FoundersConstants::NO_CHOICE_OF_CANDIDATE, $template_vars);

	#ant nest not change

	return "no boss or no treasurer found. Downgraded to CANDIDATES_RESEARCH";

    } elsif ($count_of_bosses == 1 and $count_of_treasurers == 1){

	#founders upgraded to waiting_to_be_created

	#print Dumper($bosses);
	#print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> tr\n\n\n";
	#print Dumper($treasurers);
	#print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END\n\n\n";

	my $assigned_code = $self->_create_ant_nest
	    ($stash, $template_vars, $ant_nest->[2], $bosses->[0]->[0], $treasurers->[0]->[0]);

	$self->_mail_to_all_the_founders
	    ($stash, $ant_nest_to_check, "only_one_candidate_vote_not_needed.tt2", $template_vars);

	#ant_nest upgraded to WAITING_FOR_INITIAL_SET_UP

	Bcd::Data::NewAntNests->update_ant_nest_state
	    ($stash, $ant_nest_to_check, Bcd::Constants::NewAntNestsConstants::WAITING_FOR_INITIAL_SET_UP);

	return "all ok, no vote needed created ant nest with assigned_code=$assigned_code";

    } else {

	#founders upgraded to NO_VOTED_YET
	#ant_nest upgraded to MAKING_POOL

	$self->_mail_to_all_the_founders_and_change_their_state
	    ($stash, $ant_nest_to_check, "you_have_to_vote_now.tt2", 
	     Bcd::Constants::FoundersConstants::NO_VOTED_YET, $template_vars);

	Bcd::Data::NewAntNests->update_ant_nest_state
	    ($stash, $ant_nest_to_check, Bcd::Constants::NewAntNestsConstants::MAKING_POOL);

	#I should delete all the votes
	Bcd::Data::FoundersVotings->delete_all_votes_for_this_ant_nest($stash, $ant_nest_to_check);

	return "all ok, vote needed";

    }
}

sub _mail_to_a_founder{
    my ($self, $stash, $founder, $letter_to_send, $stash_vars) = @_;

    $stash_vars->{name} = $founder->[1];
    $stash_vars->{nick} = $founder->[0];

    $stash->get_mailer()->mail_this_message
	($stash, $founder->[2], $letter_to_send, $stash_vars);
}

sub _mail_to_all_the_founders{
   my ($self, $stash, $ant_nest_to_check, $letter_to_send, $stash_vars) = @_;

   $stash_vars = {} if (!defined($stash_vars));

   my $founders = Bcd::Data::Founders->get_founders_to_email($stash, $ant_nest_to_check);

   foreach(@{$founders}){

       $self->_mail_to_a_founder($stash, $_, $letter_to_send, $stash_vars);
   }
}

sub _mail_to_all_the_founders_and_change_their_state{
    my ($self, $stash, $ant_nest_to_check, $letter_to_send, $upgraded_founder_state, $stash_vars) = @_;

    $stash_vars = {} if (!defined($stash_vars));

    my $founders = Bcd::Data::Founders->get_founders_to_email($stash, $ant_nest_to_check);

    foreach(@{$founders}){

	$self->_mail_to_a_founder($stash, $_, $letter_to_send, $stash_vars);
	
	#I should update the state of all the founders...
	Bcd::Data::Founders->update_founder_state($stash, $_->[3], $upgraded_founder_state);
    }
}

#this is a generic rule for all the intial states where I must have
#all the ants which run in parallel.
sub _all_ants_have_reached_a_particular_state_rule {
    #a very short list of parameters...
    my ($self, $stash, $ant_nest_to_check, 
	$name_of_rule, $triggering_state, $required_founders_state,
	$upgraded_ant_nest_state, $letter_to_send, $upgraded_founder_state,
	$message_to_return) = @_;

    #this rule checks if an ant nest have completed the checkin
    #get_logger()->debug("NewAntNestsBot: $name_of_rule : $ant_nest_to_check");

    #first of all I get the state of this ant nest
    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr($stash, $ant_nest_to_check);
    #the ant nest can be not defined if for example I have deleted it before
    return undef if (!defined($ant_nest));

    #the rule is valid only in this state.
    return undef if ($ant_nest->[1] != $triggering_state);

    #I should get the count of the founders in a particular state.
    my $count = Bcd::Data::NewAntNests->get_founders_with_state_count
	($stash, $ant_nest_to_check, $required_founders_state);


    if ($count == Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE){
	#ok, I can upgrade this ant nest...
	Bcd::Data::NewAntNests->update_ant_nest_state
	    ($stash, $ant_nest_to_check, $upgraded_ant_nest_state);


	  #I should advise all founders
	  $self->_mail_to_all_the_founders_and_change_their_state
	      ($stash, $ant_nest_to_check, $letter_to_send, $upgraded_founder_state, 
	       { id_ant_nest => $ant_nest_to_check});

# 	  my $founders = Bcd::Data::Founders->get_founders_to_email($stash, $ant_nest_to_check);

# 	  foreach(@{$founders}){

# 	      $stash->get_mailer()->mail_this_message
# 		  ($stash, $_->[2],
# 		   $letter_to_send, 
# 		   {
# 		       name => $_->[1],
# 		       nick => $_->[0],
# 		       id_ant_nest => $ant_nest_to_check,
# 		   }
# 		   );

# 	      #I should update the state of all the founders...
# 	      Bcd::Data::Founders->update_founder_state($stash, $_->[3], $upgraded_founder_state);
# 	  }

	  return "ant nest $ant_nest_to_check: upgraded to $message_to_return";
      }

    return undef;
}

sub all_ants_have_built_the_web_of_trust{
    my ($self, $stash, $ant_nest_to_check) = @_;

    return $self->_all_ants_have_reached_a_particular_state_rule
	(
	 $stash,
	 $ant_nest_to_check,
	 "all_ants_have_built_the_web_of_trust",
	 Bcd::Constants::NewAntNestsConstants::TRUSTS_NETWORK_BUILDING,
	 Bcd::Constants::FoundersConstants::TRUST_PRESENT,
	 Bcd::Constants::NewAntNestsConstants::CANDIDATES_RESEARCH,
	 "you_have_to_candidate_yourself.tt2",
	 Bcd::Constants::FoundersConstants::NO_CHOICE_OF_CANDIDATE,
	 "selecting candidates."
	 );
}

sub all_ants_have_immitted_data{
   my ($self, $stash, $ant_nest_to_check) = @_;

   return $self->_all_ants_have_reached_a_particular_state_rule
       (
	$stash,
	$ant_nest_to_check,
	"all_ants_have_immitted_data",
	Bcd::Constants::NewAntNestsConstants::FOUNDERS_IMMITTING_DATA,
	Bcd::Constants::FoundersConstants::PERSONAL_DATA_IMMITTED,
	Bcd::Constants::NewAntNestsConstants::TRUSTS_NETWORK_BUILDING,
	"you_have_to_compile_the_trust.tt2",
	Bcd::Constants::FoundersConstants::TRUST_NOT_PRESENT,
	"waiting for trust."
	);
}

sub all_ants_have_checked_in{
    my ($self, $stash, $ant_nest_to_check) = @_;

    return $self->_all_ants_have_reached_a_particular_state_rule
	(
	 $stash,
	 $ant_nest_to_check,
	 "all_ants_have_checked_in",
	 Bcd::Constants::NewAntNestsConstants::WAITING_FOR_CHECKING_IN,
	 Bcd::Constants::FoundersConstants::USER_BOOKED_CONFIRMED,
	 Bcd::Constants::NewAntNestsConstants::FOUNDERS_IMMITTING_DATA,
	 "you_should_give_me_your_personal_data.tt2",
	 Bcd::Constants::FoundersConstants::WAITING_FOR_PERSONAL_DATA,
	 "immitting data state."
	 );
}

sub delete_founders_ants_not_checked{
    my ($self, $stash, $ant_nest_to_check) = @_;

    #get_logger()->debug("delete_founders_ants_not_checked rule called ant nest $ant_nest_to_check");


    my $st = $stash->prepare_cached(SELECT_OLD_FOUNDERS_STATE);

    $st->bind_param(1, Bcd::Constants::FoundersConstants::USER_BOOKED_CREATED);
    $st->bind_param(2, $ant_nest_to_check);


    $st->execute();

    my $row;

    while (defined($row = $st->fetch())){

	#OK... This ant is too lazy, I should delete it...
	$self->_delete_this_lazy_to_check_in_founder_ant($stash, $row->[0]);

	#if this was the last ant (no founders and no lookers)
	#, then the ant nest should be deleted too.

	my $founders = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, $ant_nest_to_check);
	my $lookers = Bcd::Data::NewAntNests->get_ant_nest_founders_count($stash, $ant_nest_to_check);

	my $status;
	if ($founders + $lookers == 0){
	    #I should delete also the ant nest
	    $self->_delete_this_new_ant_nest_aborted($stash, $row->[2]);
	    $status = "deleted ONLY lazy ant $row->[3] in ant nest $row->[2], deleted also the ant nest";
	} else	{
	    # there are some founders or lookers left, let's see if I should downgrade this ant nest
	    my $downgraded = $self->_check_this_ant_nest_state($stash, $ant_nest_to_check, $row->[3]);
	    if ($downgraded == 1){
		$status = "deleted lazy ant $row->[3]; DOWNGRADED ant nest $row->[2]";
	    } else {
		$status = "deleted lazy ant $row->[3] in ant nest $row->[2]";
	    }
	}

	$st->finish();

	return $status;
    }

    $st->finish();

    #no firing....
    return undef;
}

sub _delete_this_new_ant_nest_aborted{

   my ($self, $stash, $id_aborted_ant_nest) = @_;

   #first of all I delete the token
   my $sql = qq{delete from ant_nests_bookings where id = ?};
   my $st2 = $stash->get_connection()->prepare($sql);
   $st2->bind_param(1, $id_aborted_ant_nest);
   $st2->execute();
}

sub _delete_this_lazy_to_check_in_founder_ant{
    my ($self, $stash, $id_lazy_ant) = @_;

    #first of all I delete the token
    my $sql = qq{delete from ant_nests_founders_tokens where id_user = ?};
    my $st2 = $stash->get_connection()->prepare($sql);
    $st2->bind_param(1, $id_lazy_ant);
    $st2->execute();

    #then I delete the user
    $sql = qq{delete from ant_nests_founders where id_user = ?};
    $st2 = $stash->get_connection()->prepare($sql);
    $st2->bind_param(1, $id_lazy_ant);
    $st2->execute();
}

sub _check_this_ant_nest_state{
    my ($self, $stash, $ant_nest_to_check, $lazy_ant) = @_;

    #ok, now I should test if this ant nest was in the state waiting for check_in
    my $ant_nest = Bcd::Data::NewAntNests->select_new_ant_nest_booking_arr($stash, $ant_nest_to_check);

    #if it was in the WAITING_FOR_CHECKING_IN state I downgrade it to WAITING_FOR_INITIAL_FOUNDERS state
    if ($ant_nest->[1] == Bcd::Constants::NewAntNestsConstants::WAITING_FOR_CHECKING_IN){
	
	#downgrade the state
	Bcd::Data::NewAntNests->update_ant_nest_state
	    ($stash, $ant_nest_to_check, Bcd::Constants::NewAntNestsConstants::WAITING_FOR_INITIAL_FOUNDERS);

	#Ok, now I should inform the other ants...

	  my $founders = Bcd::Data::Founders->get_confirmed_founders_to_email
	      ($stash, $ant_nest_to_check);

	  foreach(@{$founders}){

	          $stash->get_mailer()->mail_this_message
		  ($stash, $_->[2],
		   'downgraded_ant_nest_because_of_lazy_ant.tt2', 
		   {
		       name => $_->[1],
		       nick => $_->[0],
		       id_ant_nest => $ant_nest_to_check,
		       lazy_ant    => $lazy_ant,
		   }
		   );
	  }
	  
	return 1;
    } else {
	return 0; #no downgrade necessary.
    }
}

1;
