package Bcd::Bots::Manager;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this is the Manager of the bots... It has a main thread of execution,
#when started using the model

use strict;
use warnings;

use Bcd::Data::StatementsStash;
use Bcd::Bots::NewAntNestsBot;
use Bcd::Bots::TrustBot;

use constant {
    MANAGER_THREAD_CYCLE => 2,
};


sub run{
    my $server = shift;

    #ok, now I have the connection, then I will have the stash
    #HACK, HACK, I init the cache here...
    my $stash = Bcd::Data::StatementsStash->new($server, 1);
    my $test = $server->{test};

    my @bots = ();

    my $bot = Bcd::Bots::NewAntNestsBot->new($server);

    my $bot_info = {
	bot           => $bot,
	seconds_to_go => $bot->get_frequency(),
	is_running    => 0,
    };

    push(@bots, $bot_info);

    $bot = Bcd::Bots::TrustBot->new();

    my $bot_info2 = {
	bot           => $bot,
	seconds_to_go => $bot->get_frequency(),
	is_running    => 0,
    };

    push(@bots, $bot_info2);


    while (1){


	my $num_bots = scalar(@bots);
	#now, I should have the possibility to run the bot
	for (@bots){

	    sleep(MANAGER_THREAD_CYCLE);

	    #this bot can be running or not.
	    if ( $_->{is_running} == 1){
		my ($rules, $cycle_finished ) = $_->{bot}->go($stash);
		if ($cycle_finished == 1){
		    #ok, this bot has finished, wait for another epoch
		    $_->{is_running} = 0;
		    $_->{seconds_to_go} = $_->{bot}->get_frequency();
		}
		next;
	    }

	    #not running...
	    #this is not very precise, but I don't need here precision...
	    $_->{seconds_to_go} -= MANAGER_THREAD_CYCLE * $num_bots;

	    if ($_->{seconds_to_go} <= 0){
		#ok, let's awake this bot
		if ($_->{bot}->awake($stash) == 1){
		    #this bot starts a new cycle
		    $_->{is_running} = 1;
		} else {
		    #wait for another epoch
		    $_->{seconds_to_go} = $_->{bot}->get_frequency();
		}
	    }

	}
	
    }
}

1;
