package Bcd::Bots::TrustBot;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

#this bot will simply watch the creation of the ant nests...

use strict;
use warnings;
use Data::Dumper;

use Bcd::Bots::BcdBot;
use base(qw/Bcd::Bots::BcdBot/);

use Bcd::Data::TrustsNet;
use Bcd::Data::Users;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($DEBUG);

use Bcd::Common::CommonConstants;

#The frequency of the trust bot is frequent... it should compute
#the nets....
use constant {
    MY_FREQUENCY             => 3,
};


#of course you could use the %s syntax to perform a substitution...
use constant {
};


sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    $self->_add_rule(\&create_delete_trust_nets_rule);
    $self->_add_rule(\&update_trust_nets_rule);


    bless($self, $class);
    return $self;
}

sub get_frequency {
    return MY_FREQUENCY;
}

#I should start a new cycle.
sub awake{
    my ($self, $stash) = @_;
    return 1;
}

sub _advance_ant_nest{
    my ($self, $stash) = @_;
    return 1;
}

sub update_trust_nets_rule{
    my ($self, $stash) = @_;

    my $set = Bcd::Data::TrustsNet->get_nets_user_set($stash);
    my $rule;
    my $list_of_refreshed = "";

    for (keys(%{$set})){
	#I see if this net needs refreshing
	if ($set->{$_}->{refresh_count} != 0){
	    $list_of_refreshed .= "$_ ";
	    Bcd::Data::TrustsNet->update_nets_for_user($stash, $_);
	}
    }
    if ($list_of_refreshed ne ""){
	return "Refreshed nets for : $list_of_refreshed";
    } else {
	return undef;
    }
}

sub create_delete_trust_nets_rule{
    my ($self, $stash) = @_;

    #this rule has no current ant nest... I update the nets as soon as
    #the users log in the system

    my $users_connected = $stash->get_cache()->get
	(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY);

    my $set = Bcd::Data::TrustsNet->get_nets_user_set($stash);
    my $rule;    

    my @users_to_delete;
    my @users_to_keep;

    #I should remove from both sets the common values
    if(defined($users_connected)){
	#the keys which start with "net" are reserved for the trusts nets.

	my @sessions_expired;
	foreach my $user_in_cache (keys(%{$users_connected})){

	    #the founders have not really a net...
	    my $cur_session = $stash->get_cache()->get($users_connected->{$user_in_cache});
	    if (!defined($cur_session)){
		#the session is not defined, this means that it is expired...
		push(@sessions_expired, $user_in_cache);
		next;
	    }
	    next if ($cur_session->{user_role} & Bcd::Data::Users::FOUNDER_ROLE );
	    push(@users_to_keep, $user_in_cache);
	    #$users_to_keep .= "$user_in_cache,";

	    if (exists($set->{$user_in_cache})){
		delete $set->{$user_in_cache};
	    } else {
		#this does not exist, create please, unlocked
		#get_logger()->debug("TRUST BOT : added nets for user $user_in_cache ");
		Bcd::Data::TrustsNet->create_nets_for_user($stash, $user_in_cache, 0);
		$rule .= "add: $user_in_cache ";
	    }
	}

	#ok, now I force the logout of the users which have the session expired
	if (scalar(@sessions_expired) > 0){

	    foreach(@sessions_expired){

		delete $users_connected->{$_};
		#was this user a normal user?
		my $nick = Bcd::Data::Users->get_nick_from_id($stash, $_);

		if (defined($nick)){
		    #ok, this was a normal user
		    $rule .= "delete expired session: $nick ";
		    push(@users_to_delete, $_);
		}

	    }

	    $stash->get_cache()->set
		(Bcd::Common::CommonConstants::USERS_CONNECTED_KEY, $users_connected);
	}

    }


    #the ones which remain in set should be deleted

    foreach(keys(%{$set})){
	#the locked users have their nets locked...
	if ($set->{$_}->{is_locked} == 1){
	    push(@users_to_keep, $_);
	    next;
	}

	#get_logger()->debug("TRUST BOT : deleted nets for user $_ ");
	#Bcd::Data::TrustsNet->delete_nets_for_user($stash, $_);
	$rule .= "del: $_ ";
	push (@users_to_delete, $_);
    }
    

    #print Dumper(\@keys);

    #ok, now I ask to delete the users
    Bcd::Data::TrustsNet->delete_nets($stash, \@users_to_delete, \@users_to_keep);

    #the rule fires if the
    return $rule;
}


1;
