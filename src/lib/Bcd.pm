package Bcd;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use constant EOL => "\015\012";

use base qw(Net::Server::Fork);
use Bcd::Data::Model;
use Bcd::Data::StatementsStash;
use Bcd::Commands::OutputCommand;
use Bcd::Bots::Manager;
use Bcd::Commands::InputCommandParser;
use POSIX ":sys_wait_h";
use Data::Dumper;
use Pod::Usage;

### over-ridden subs below

#this does not work, because I have the 0.94 version...
# sub default_values{
#     return {
# 	conf_file => './etc/bcd.conf',
#     };
#}

sub options {
    my $self     = shift;
    my $prop     = $self->{server};
    my $template = shift;

    ### setup options in the parent classes
    $self->SUPER::options($template);

    
    #I add some parameters
    foreach(qw/test script help bcd_home 
	    mailer new_ant_nest_bot_frequency
	    conn_string share_file user_db 
	    expire_time/){
	$template->{$_} = \$prop->{$_};
    }
}

sub post_configure_hook{
    my $self = shift;
    my $prop = $self->{server};

    if (defined($prop->{help}) && $prop->{help} == 1){
	pod2usage(2);
    }

    if (!defined($prop->{test}) || ($prop->{test} != 1)){
	#I assume not test
	$prop->{test} = 0;
	$self->log(2, $self->log_time . " using normal db, connect to port 9000");
	$prop->{port} = 9000;
    } else {
	$prop->{port} = 9001;
	$self->log(2, $self->log_time . " using TEST db, connect to port 9001");
    }

    if (! defined($prop->{script}) or $prop->{script} != 1){
	$self->log(2, $self->log_time . " NOT scripting");
	$prop->{script} = 0;
    } else {
	$self->log(2, $self->log_time . " I AM scripting");
    }
	
    if ($prop->{script} == 1 && $prop->{test} == 0){
	pod2usage(2);
    }
	
}

sub process_request {
    my $self = shift;

    eval {

	#I FORCE THE TEST DATABASE
	my $stash = Bcd::Data::StatementsStash->new($self->{server});

	#at first the parsers are in text mode, the script for now it is off..
	#then I will find a way to pass the parameters...

	my $input_parser = Bcd::Commands::InputCommandParser->new
	    (Bcd::Common::CommonConstants::TEXT_MODE, $self->{server}->{script});
	my $output_stream = Bcd::Commands::OutputCommand->new
	    (Bcd::Common::CommonConstants::TEXT_MODE);
	my $factory = Bcd::Data::Model->instance()->get_factory();

	#local $SIG{'ALRM'} = sub { die "Timed Out!\n" };

	local $/ = "\015\012";
	#my $timeout = 60; # give the user 30 seconds to type some lines

	#my $previous_alarm = alarm($timeout);
	print main::greetings() . EOL;
	print "Ready for commands: \\q: quit \\b: binary \\t: text \\h: help." . EOL;
	while (<STDIN>) {
	    chomp;
	    if ($_ eq '\q'){
		last;
	    } elsif ($_ eq '\b'){
		$input_parser->select_mode(Bcd::Common::CommonConstants::BINARY_MODE);
		$output_stream->select_mode(Bcd::Common::CommonConstants::BINARY_MODE);
		print "mode set to BIN" . EOL;
		next;
	    } elsif ($_ eq '\t'){
		$input_parser->select_mode(Bcd::Common::CommonConstants::TEXT_MODE);
		$output_stream->select_mode(Bcd::Common::CommonConstants::TEXT_MODE);
		print "mode set to TEXT" . EOL;
		next;
	    } elsif ($_ eq '\h'){
		print "Help not implemented so far. Sorry." . EOL;
		next;
	    }
	    
	    my $cmd_id = $stash->get_next_cmd_id();
	    my $cmd = $factory->get_command($_, $cmd_id, $input_parser, $output_stream);

	    if (!defined($cmd)){
		print "Unknown command '$_' type \\q to exit." . EOL;
	    } else {
		$cmd->exec($stash);
		$self->log(2, $self->log_time . " " . $cmd->{id_cmd} .  " " . 
				   $cmd->get_name() . 
				   $cmd->get_parameters_for_logging());
	    }
	    #alarm($timeout);
	}
	#alarm($previous_alarm);

	$input_parser->end_of_input();

    };

    if ($@ =~ /timed out/i) {
	print STDOUT "Timed Out." . EOL;
	return;
    } else {
	print STDOUT "bye...$@" . EOL;
	return;
    }

}

sub pre_loop_hook{
    my $self = shift;

    my $pid;
    if (!defined($pid = fork)) {
	die "cannot fork: $!";
	return;
    } elsif ($pid) {
	# I'm the parent
	$self->{bcd_bot_pid} = $pid;
	#this sleep is just to be almost sure that the manager
	#will init the cache file before any client arrives.
	sleep(2);
	return; 
    }

    #ok, I am the child... go on and start the manager...
    Bcd::Bots::Manager::run($self->{server});
}

sub pre_server_close_hook{
    my $self = shift;

    if (!defined($self->{bcd_bot_pid})){
	return;
    }

    kill "INT", $self->{bcd_bot_pid};
    waitpid($self->{bcd_bot_pid}, 0);
}

1;
