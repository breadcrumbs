<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="/usr/share/xml/docbook/stylesheet/nwalsh/html/chunk.xsl"/>
<xsl:param name="passivetex.extensions" select="1"/>
<xsl:param name="tex.math.in.alt" select="'latex'"/>
<xsl:param name="navig.showtitles" select="1"/>
<xsl:param name="chunker.output.encoding" select="'UTF-8'"/>
</xsl:stylesheet>

