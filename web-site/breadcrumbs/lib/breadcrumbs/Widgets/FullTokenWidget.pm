package breadcrumbs::Widgets::FullTokenWidget;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs;

use Data::Dumper;


sub _make_simple_control{
    my ($class, $w, $container, $cap, $fname, $regex, $value) = @_;

    my $e = $container->element('Textfield', $fname)->label($cap);

    if (defined($value)){
	$e->value($value);
	$e->attrs->{readonly} = "readonly";
    }

    my $c =$w->constraint('Regex', "$fname" );

    $c->message('Non mi sembra un gettone valido');
    #YOU CANNOT COMPILE HERE THE REGEX WITH THE qr operator
    #OTHERWISE IT IS LOST IN THE SESSION (bug?)
    $c->regex($regex);

    return $w;
}

sub _get_token{
    #I get the result of the widget
    my ($self, $res, $name) = @_;

    my $token = $res->param("$name");
    return $token;
        
}

1;
