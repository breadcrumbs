package breadcrumbs::Widgets::TokenWidget;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs;

use Data::Dumper;

sub make_token_widget{
    my ($class, $c, $caption) = @_;

#    my $self = {};

    my $cap = defined($caption) ? $caption : "Gettone:";

    # Create an HTML::Widget to build the form
    my $w = $c->widget('token_form')->method('post');
    my $fs = $w->element('Fieldset', 'new_user')->legend('Ho bisogno di un gettone:');

    $fs->element('Textfield', "token")->label($cap);

    my $constraint = $w->constraint('Regex', "token");
    $constraint->message('un gettone ha questa forma: (3 caratteri)<spazio>(3 caratteri)<spazio>(3 caratteri)');
    #YOU CANNOT COMPILE HERE THE REGEX WITH THE qr operator
    #OTHERWISE IT IS LOST IN THE SESSION (bug?)
    $constraint->regex('^([A-Za-z0-9]{3}[ ]){2}[A-Za-z0-9]{3}$');

    $w->constraint('All', "token")->message('Manca il gettone');

    $fs->element('Submit',    'token_submit' )->value('Ok');

#     $self->{widget} = $w;
#     bless($self, $class);

    return $w;
}

sub make_simple_control{
    my ($class, $w, $container, $required, $caption, $field_name) = @_;

    my $cap = defined($caption) ? $caption : "Gettone:";
    my $fname = defined($field_name) ? $field_name : "token";

    $container->element('Textfield', $fname)->label($cap);

    my $c = $w->constraint('Regex', "$fname");
    $c->message('un gettone ha questa forma: (3 caratteri)<spazio>(3 caratteri)<spazio>(3 caratteri)');
    #YOU CANNOT COMPILE HERE THE REGEX WITH THE qr operator
    #OTHERWISE IT IS LOST IN THE SESSION (bug?)
    $c->regex('^([A-Za-z0-9]{3}[ ]){2}[A-Za-z0-9]{3}$');

    #print Dumper($c);

    if (defined ($required) and $required == 1){
	$w->constraint('All', "$fname")->message('Manca il gettone');
    }


    return $w;
}

sub get_token{
    #I get the result of the widget
    my ($self, $res, $name_d) = @_;

    my $name = defined($name_d) ? $name_d : "token";
    my $token = $res->param("$name");
    return $token;
        
}

1;
