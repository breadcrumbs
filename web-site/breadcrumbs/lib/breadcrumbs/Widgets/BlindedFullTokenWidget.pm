package breadcrumbs::Widgets::BlindedFullTokenWidget;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs;

use Data::Dumper;

use breadcrumbs::Widgets::FullTokenWidget;

sub make_simple_control{
    my ($class, $w, $container, $value, $caption, $field_name) = @_;


    my $cap = defined($caption) ? $caption : "Gettone mascherato:";
    my $fname = defined($field_name) ? $field_name : "blinded_token";


    breadcrumbs::Widgets::FullTokenWidget->_make_simple_control
	($w, $container, $cap, $fname, '^([A-Za-z0-9#]{3}[ ]){5}[A-Za-z0-9#]{3}$', $value);

    return $w;
}

sub get_token{
    #I get the result of the widget
    my ($self, $res, $name_d) = @_;

    my $name = defined($name_d) ? $name_d : "blinded_token";

    return breadcrumbs::Widgets::FullTokenWidget->_get_token($res, $name);
}

1;
