package breadcrumbs::Widgets::ActivityWidget;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs;


sub make_simple_control{
    my ($class, $w, $container, $caption, $field_name) = @_;

    my $cap = defined($caption) ? $caption : "Attivit_ampersand_agrave;:";
    my $fname = defined($field_name) ? $field_name : "master_act";

    # Create an HTML::Widget to build the form
    my $e = $container->element('Select', $fname)->label($cap);
    $e->options(foo => 'foo', bar => 'bar');
    $e->constrain_options(1);

    return $w;
}

########################################
## static accessor functions

sub get_value{
    my ($class, $result, $name_d) = @_;

    return 39;
}


1;
