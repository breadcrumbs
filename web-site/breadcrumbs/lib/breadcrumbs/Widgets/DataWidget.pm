package breadcrumbs::Widgets::DataWidget;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs;


sub make_simple_control{
    my ($class, $w, $container, $caption, $field_name) = @_;

    my $cap = defined($caption) ? $caption : "Data:";
    my $fname = defined($field_name) ? $field_name : "date";

    # Create an HTML::Widget to build the form

    $container->element('Span', 'id3993')->content('Data di nascita:')->class('form-label');    
    my $block = $container->element('Block', 'block2')->class('test-inline-controls');
    $block->element('Textfield', "${fname}_day");
    $block->element('Span', 'id1122')->content('/');
    $block->element('Textfield', "${fname}_month");
    $block->element('Span', 'id11209')->content('/');
    $block->element('Textfield', "${fname}_year");

    $w->constraint('Date', "${fname}_year", "${fname}_month", "${fname}_day")
	->message('Questa _ampersand_egrave; una data inesistente.');

    return $w;
}

##############################3
## static accessors...

sub get_value{
    my ($class, $result, $fname) = @_;

    my $name = defined($fname) ? $fname : "date";
    
    my $year = $result->param("${fname}_year");
    my $month = $result->param("${fname}_month");
    my $day = $result->param("${fname}_day");

    #the date returned as in ISO format YYYY-MM-DD
    return "$year" . "-" . "$month" . "-" . "$day";
}


1;
