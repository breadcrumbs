package breadcrumbs::util::MenuUtils;

# This file is part of the breadcrumbs daemon (bcd).
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#static function just to add a menu.
sub add_menu_entry{
    my ($class, $url, $caption, $menu_so_far, $c) = @_;

    my $url_coded = $c->uri_for($url);
    my @item = ($caption, "$url_coded");
    unshift (@{$menu_so_far}, \@item);
}

sub add_trail{
    my ($class, $url, $caption, $trail_so_far, $c) = @_;

    my $url_coded = $c->uri_for("$url");
    my @item = ("$caption", "$url_coded");
    push(@{$trail_so_far}, \@item);

}

sub add_action{
    my ($class, $url, $caption, $actions_so_far, $c) = @_;

    my $url_coded = $c->uri_for("$url");
    my @item = ("$caption", "$url_coded");
    push(@{$actions_so_far}, \@item);

}


1;
