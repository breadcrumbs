package breadcrumbs::Dialogs::FounderVoteDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::TrustWidget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $bosses, $treasurers) = @_;

    
    my $w = $c->widget('founder_vote_form')->method('post');
    my $fs = $w->element('Fieldset', 'votes')
	->legend('Qui puoi votare');

    if (@{$bosses} != 1){
	$class->_build_block_of_radio_buttons($fs, 1, "Scelta capo:", 'boss', $bosses);
    }

    if (@{$treasurers} != 1){
	$class->_build_block_of_radio_buttons($fs, 1, "Scelta tesoriere:", 'treasurer', $treasurers);
    }


    #I need three radio buttons...

    $fs->element('Submit', 'trust_submit' )->value('Ok');

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

#just to build a block for the boss and the treasurer
sub _build_block_of_radio_buttons{
    my ($self, $fs, $id, $caption, $fname, $candidates) = @_;

    my $block = $fs->element('Block', 'block$id')->class('form-block');
    $block->element( 'Span', 'divider$id')->content($caption)->class('form-divider-small');

    for(@{$candidates}){
	
	my $e = $block->element( 'Radio', $fname );
	$e->label($_->{nick_or_name});
	$e->value($_->{id_user});

    }

    #the last element is always present

    my $e = $block->element( 'Radio', $fname );
    $e->label('Scheda bianca:');
    $e->checked('checked');
    $e->value('NULL');
}

##################################
## static accessors

sub get_trust{
    my ($class, $result, $index) = @_;
    my $field_name = "trust_to_$index";
    return breadcrumbs::Widgets::TrustWidget->get_value($result, $field_name);
}

1;
