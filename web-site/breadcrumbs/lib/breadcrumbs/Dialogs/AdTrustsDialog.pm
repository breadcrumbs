package breadcrumbs::Dialogs::AdTrustsDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::BbTrustWidget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $for_wizard_d) = @_;

    my $for_wizard = defined($for_wizard_d) ? $for_wizard_d : 0;

    


    my $self = {};

    bless ($self, $class);
    $self->{for_wizard} = $for_wizard;

    return $self;
}

sub on_init_dialog{
    my ($self, $c) = @_;


    my $w = $c->widget('ad_trusts_form')->method('post');

    my $fs = $w->element('Fieldset', 'ad_trusts_fs')
	->legend('Ho bisogno una o due fiducie:');

    if (defined($c->session->{steps}->{edit_t_e})){
	breadcrumbs::Widgets::BbTrustWidget->
	    make_simple_control($w, $fs, "Fiducia minima per vedere l'annuncio:", "t_e");
      }
    if (defined($c->session->{steps}->{edit_t_c})){
	breadcrumbs::Widgets::BbTrustWidget->
	    make_simple_control($w, $fs, "Fiducia minima per ricevere assegni:", "t_c");
      }
    
    if ( ! $self->{for_wizard}) {
	$fs->element('Submit', 'trust_submit' )->value('Ok');
    }

    $self->{widget} = $w;
}

########################
## static accessors

sub get_trust_emit{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_e");
}

sub get_trust_credit{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_c");
}

1;
