package breadcrumbs::Dialogs::ConfirmTutorsDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::TrustWidget;

use breadcrumbs::Dialogs::BcDialog;

use base(qw/breadcrumbs::Dialogs::BcDialog/);

#this dialog is a simple dialog with two inputs, the trusts for the
#two tutors
sub new{
    my ($class, $c, $first_tutor_name, $second_tutor_name) = @_;

    
    my $w = $c->widget('trust_to_tutors')->method('post');
    my $fs = $w->element('Fieldset', 'trust')->legend('Ho bisogno di due fiducie:');

    my $cap1 = "Fiducia a $first_tutor_name";
    my $cap2 = "Fiducia a $second_tutor_name";
    
    breadcrumbs::Widgets::TrustWidget->make_simple_control($w, $fs, $cap1, "trust_first");
    breadcrumbs::Widgets::TrustWidget->make_simple_control($w, $fs, $cap2, "trust_second");

    $fs->element('Submit', 'trust_submit' )->value('Ok');

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}




#####################################
## static methods.
sub get_first_trust{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::TrustWidget->get_value($result, "trust_first");
}

sub get_second_trust{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::TrustWidget->get_value($result, "trust_second");
}

1;
