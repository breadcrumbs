package breadcrumbs::Dialogs::LoginDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;


use breadcrumbs::Dialogs::BcDialog;

use base(qw/breadcrumbs::Dialogs::BcDialog/);

use breadcrumbs::Widgets::NickWidget;

sub new{
    my ($class, $c) = @_;

    
    my $w = $c->widget('login_form')->method('post');
    my $fs = $w->element('Fieldset', 'login_fs')
	->legend('Qualificati, per favore:');

    breadcrumbs::Widgets::NickWidget->make_simple_control($w, $fs);

    $fs->element('Textfield', 'ant_nest_id')->label("Codice formicaio:");
    $fs->element('Password', 'password')->label("Password:");
    $fs->element('Submit', 'trust_submit' )->value('Ok');

    $w->constraint( 'All', 'ant_nest_id')->message('Valore mancante.');

    $w->constraint( 'Regex', 'ant_nest_id')
	->message("Il codice postale deve essere un numero a 5 o 7 cifre. 5 solo per i formicai in costruzione.")
	->regex('^(\d{5})|(\d{7})$'); #the first is a letter

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

########################
## static accessors


1;
