package breadcrumbs::Dialogs::ChangePasswordDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;


use breadcrumbs::Widgets::PasswordsPairWidget;
use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c) = @_;

    my $w = $c->widget('change_password_form')->method('post');

    my $fs = $w->element('Fieldset', 'pass_change_fs')
	->legend('Qui puoi cambiare la password:');

    #I make a control for the old password
    $fs->element('Password', 'old_password')->label("Vecchia password:");
    
    #this is the new password
    breadcrumbs::Widgets::PasswordsPairWidget->make_simple_control($w, $fs);

    $fs->element('Submit', 'password_submit' )->value('Ok');

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

####################
#accessors

sub get_password{
    my ($class, $result) = @_;

    return breadcrumbs::Widgets::PasswordsPairWidget->get_value($result);
}


1;
