package breadcrumbs::Dialogs::CandidatesFlagsDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::TrustWidget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c) = @_;

    
    my $w = $c->widget('candidates_flags_form')->method('post');
    my $fs = $w->element('Fieldset', 'candidates_flag')
	->legend('Qui ti puoi candidare');

    #I need three radio buttons...
    my $block = $fs->element('Block', 'block1')->class('form-block');
    $block->element( 'Span', 'divider6')->content('Scelta candidatura:')->class('form-divider-small');
    

    my $e = $block->element( 'Radio', 'flag' );
    $e->label('Capo formicaio:');
    $e->value('b');

    $e = $block->element( 'Radio', 'flag' );
    $e->label('Tesoriere:');
    $e->value('t');

    $e = $block->element( 'Radio', 'flag' );
    $e->label('Nessuno dei due:');
    $e->checked('checked');
    $e->value('none');


    $fs->element('Submit', 'trust_submit' )->value('Ok');

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

##################################
## static accessors

sub get_trust{
    my ($class, $result, $index) = @_;
    my $field_name = "trust_to_$index";
    return breadcrumbs::Widgets::TrustWidget->get_value($result, $field_name);
}

1;
