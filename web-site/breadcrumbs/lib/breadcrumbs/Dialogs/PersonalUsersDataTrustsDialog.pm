package breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::BbTrustWidget;;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $for_wizard_d) = @_;

    my $for_wizard = defined($for_wizard_d) ? $for_wizard_d : 0;

    
    my $w = $c->widget('personal_data_form')->method('post');
    my $fs = $w->element('Fieldset', 'personal_data_fs')
	->legend('Ho bisogno delle fiducie per i dati personali:');

    breadcrumbs::Widgets::BbTrustWidget->
	make_simple_control($w, $fs, "Fiducia nome:", "t_first_name");
    breadcrumbs::Widgets::BbTrustWidget->
	make_simple_control($w, $fs, "Fiducia cognome:", "t_last_name");
    breadcrumbs::Widgets::BbTrustWidget->
	make_simple_control($w, $fs, "Fiducia indirizzo:", "t_address");
    breadcrumbs::Widgets::BbTrustWidget->
	make_simple_control($w, $fs, "Fiducia telefono di casa:", "t_home_phone");
    breadcrumbs::Widgets::BbTrustWidget->
	make_simple_control($w, $fs, "Fiducia cellulare:", "t_mobile_phone");
    breadcrumbs::Widgets::BbTrustWidget->
	make_simple_control($w, $fs, "Fiducia maschio/femmina:", "t_sex");
    breadcrumbs::Widgets::BbTrustWidget->
	make_simple_control($w, $fs, "Fiducia data di nascita:", "t_birthdate");
    breadcrumbs::Widgets::BbTrustWidget->
	make_simple_control($w, $fs, "Fiducia email:", "t_email");

    if ( ! $for_wizard) {
	$fs->element('Submit', 'trust_submit' )->value('Ok');
    }

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

########################
## static accessors

sub get_t_first_name{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_first_name");
}

sub get_t_last_name{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_last_name");
}

sub get_t_address{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_address");
}

sub get_t_home_phone{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_home_phone");
}

sub get_t_mobile_phone{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_mobile_phone");
}

sub get_t_sex{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_sex");
}

sub get_t_birthdate{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_birthdate");
}

sub get_t_email{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::BbTrustWidget->get_value($result, "t_email");
}

1;
