package breadcrumbs::Dialogs::GetTotemNickDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::TrustWidget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $for_wizard_d) = @_;

    my $for_wizard = defined($for_wizard_d) ? $for_wizard_d : 0;

    
    my $w = $c->widget('get_totem_nick_form')->method('post');
    my $fs = $w->element('Fieldset', 'nick_totem')
	->legend('Scrivi per favore il nick ed il totem della persona:');


    $fs->element('Textfield', "nick")->label("Nick:");
    $fs->element('Textfield', "totem")->label("Totem:");
    #breadcrumbs::Widgets::TrustWidget->make_simple_control($w, $fs);

    if ( ! $for_wizard ) {
	$fs->element('Submit', 'get_nick_totem_submit' )->value('Ok');
    }

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

########################
## static accessors

sub get_nick{
    my ($class, $result) = @_;

    my $nick = $result->param("nick");
    return $nick;

}

sub get_totem{
    my ($class, $result) = @_;

    my $totem = $result->param("totem");
    return $totem;
}

1;
