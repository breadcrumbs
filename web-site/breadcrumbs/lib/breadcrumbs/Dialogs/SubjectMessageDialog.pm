package breadcrumbs::Dialogs::SubjectMessageDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;


use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $sender) = @_;

    my $w = $c->widget('subject_message_form')->method('post');

    my $fs = $w->element('Fieldset', 'founder')
	->legend('Messaggio da spedire:');

    
    $fs->element('Textfield', "subject")->label("Oggetto messaggio:")->value("ciao da $sender");
    $w->constraint('All', 'subject')->message('mi manca il messaggio');

    my $area = $fs->element('Textarea', "message")
	->label("Commento o messaggio:");    
    $area->cols(60);
    $area->rows(10);


    $fs->element('Submit', 'comment_submit' )->value('Spedisci');

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

####################
#accessors


1;
