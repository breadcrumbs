package breadcrumbs::Dialogs::ChoiceOfDepositsSiteDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::TrustWidget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $for_wizard_d) = @_;

    my $for_wizard = defined($for_wizard_d) ? $for_wizard_d : 0;

    
    my $w = $c->widget('choice_of_deposits_site')->method('post');
    my $fs = $w->element('Fieldset', 'deposits_site')
	->legend('Scelta del luogo dove vuoi ricevere/dare soldi:');

    #I should put a radio group
    my $block = $fs->element('Block', 'block1')->class('form-block');
    $block->element( 'Span', 'divider6')->content('Dove vuoi ricevere/dare soldi?')->class('form-divider-small');
    

    my $e = $block->element( 'Radio', 'site_deposits' );
    $e->label('In sede:');
    $e->value('hq');

    $e = $block->element( 'Radio', 'site_deposits' );
    $e->label('Non in sede:');
    $e->value('no-hq');


    if ( ! $for_wizard ) {
	$fs->element('Submit', 'get_nick_totem_submit' )->value('Ok');
    }

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

1;
