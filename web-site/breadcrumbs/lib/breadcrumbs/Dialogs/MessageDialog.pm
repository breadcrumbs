package breadcrumbs::Dialogs::MessageDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;

use breadcrumbs::Widgets::EmailWidget;
use breadcrumbs::Dialogs::BcDialog;

use base(qw/breadcrumbs::Dialogs::BcDialog/);

#this dialog is a simple dialog with two inputs, the trusts for the
#two tutors
sub new{
    my ($class, $c) = @_;

    
    my $w = $c->widget('subscribe_mailing_list_dlg')->method('post');
    my $fs = $w->element('Fieldset', 'data')->legend('Qui puoi darmi un messaggio da spedire:');

    $fs->element('Textfield', "subject")->label("Oggetto:");

    my $area = $fs->element('Textarea', "comment_or_message")->label("Commento o messaggio:");    

    $area->cols(50);
    $area->rows(10);

    $fs->element('Submit', 'comment_submit' )->value('Spedisci');

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}



######################
## static

sub get_comment{
    my ($class, $result) = @_;

    return $result->param("comment_or_message");

}

sub get_subject{
    my ($class, $result) = @_;

    return $result->param("subject");

}

1;
