package breadcrumbs::Dialogs::SecretTokenDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::TokenWidget;
use breadcrumbs::Widgets::BlindedFullTokenWidget;
use breadcrumbs::Widgets::NormalFullTokenWidget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

use Bcd::Data::Token;

sub new{
    my ($class, $c, $for_wizard_d, $blinded_token_d, $full_token_d) = @_;

    my $for_wizard = defined($for_wizard_d) ? $for_wizard_d : 0;

    
    my $w = $c->widget('secret_token_form')->method('post');
    my $fs = $w->element('Fieldset', 'trust')
	->legend('Ho bisogno del gettone segreto:');

    my $id = rand();
    my $block = $fs->element('Block', 'block$id')->class('form-block');
    $block->element( 'Span', 'divider$id')
	->content("Puoi darmi direttamente il gettone segreto:")
	->class('form-divider-small');

    $w = breadcrumbs::Widgets::TokenWidget->make_simple_control
	($w, $fs, 0, "Gettone segreto:", "secret_token");


    $id = rand();
    $block = $fs->element('Block', 'block$id')->class('form-block');
    $block->element( 'Span', 'divider$id')
	->content("Oppure puoi darmi due gettoni, quello mascherato e quello intero, io " .
		  "calcoler_ampersand_ograve; il segreto per te:")
	->class('form-divider-small');

    $w = breadcrumbs::Widgets::BlindedFullTokenWidget->make_simple_control
	($w, $fs, $blinded_token_d);

    $w = breadcrumbs::Widgets::NormalFullTokenWidget->make_simple_control
	($w, $fs, $full_token_d);


    if ( ! $for_wizard) {
	$fs->element('Submit', 'trust_submit' )->value('Ok');
    }

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

########################
## static accessors

sub get_secret_token{
    my ($class, $result) = @_;

    my $value = breadcrumbs::Widgets::TokenWidget->get_token($result, "secret_token");

    #only spaces...
    if ($value =~ /^\s*$/){

	#ok, I should get the secret token from the blinded and the full one.
	my $full_token    = breadcrumbs::Widgets::NormalFullTokenWidget->get_token($result);
	my $blinded_token = breadcrumbs::Widgets::BlindedFullTokenWidget->get_token($result);

	#ok, now I should obtain the secret by the full and the blinded
	$value = Bcd::Data::Token::unblind_the_token
	    ($full_token, $blinded_token);
    }

    return $value;
    
}

1;
