package breadcrumbs::Dialogs::InvoiceQuantityDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

use Bcd::Data::Token;

sub new{
    my ($class, $c, $for_wizard_d, $blinded_token_d, $full_token_d) = @_;

    my $for_wizard = defined($for_wizard_d) ? $for_wizard_d : 0;

    
    my $w = $c->widget('invoice_quantity_form')->method('post');
    my $fs = $w->element('Fieldset', 'trust')
	->legend('Ho bisogno di una quantit_ampersand_agrave;:');

    $fs->element('Textfield', "quantity")->label("Numero di ore o di oggetti:");

    $w->constraint('Range', "quantity")
	->message('Il numero deve stare fra 1 e 999')->min(1)->max(999);
    
    $w->constraint( 'All', "quantity")->message('Valore mancante');

    if ( ! $for_wizard) {
	$fs->element('Submit', 'trust_submit' )->value('Ok');
    }
    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

########################
## static accessors


1;
