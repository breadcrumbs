package breadcrumbs::Dialogs::FounderTrustsDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::TrustWidget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $founders_summary) = @_;

    
    my $w = $c->widget('founder_trust_form')->method('post');
    my $fs = $w->element('Fieldset', 'founders_trust')
	->legend('Questo _ampersand_egrave il modulo delle fiducie dei fondatori:');


    my $i = 0;
    foreach(@{$founders_summary}){

	if ($_->{id_user} == $c->user->my_profile()->{id_user}){
	    next; #no trust with myself.
	}

	my $field_name = "trust_to_$i";
	my $caption = "Fiducia a $_->{nick_or_name}:";
	my $hidden_field_name = "id_user_$i";

	#I add a hidden field
	$fs->element("Hidden", $hidden_field_name)->value($_->{id_user});
	breadcrumbs::Widgets::TrustWidget->make_simple_control($w, $fs, $caption, $field_name);

	$i++;
    }

    $fs->element('Submit', 'trust_submit' )->value('Ok');

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

##################################
## static accessors

sub get_trust{
    my ($class, $result, $index) = @_;
    my $field_name = "trust_to_$index";
    return breadcrumbs::Widgets::TrustWidget->get_value($result, $field_name);
}

1;
