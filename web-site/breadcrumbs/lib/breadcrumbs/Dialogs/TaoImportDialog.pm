package breadcrumbs::Dialogs::TaoImportDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;
use breadcrumbs::Widgets::TaoCurrencyWidget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $for_wizard_d, $cap) = @_;

    my $for_wizard = defined($for_wizard_d) ? $for_wizard_d : 0;
    
    my $w = $c->widget('tao_import_form')->method('post');
    my $fs = $w->element('Fieldset', 'tao_form')
	->legend('Ho bisogno di un importo in tao:');

    breadcrumbs::Widgets::TaoCurrencyWidget->make_simple_control($w, $fs, $cap);

    if ( ! $for_wizard) {
	$fs->element('Submit', 'tao_submit' )->value('Ok');
    }

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

########################
## static accessors

sub get_amount{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::TaoCurrencyWidget->get_value($result);
}

1;
