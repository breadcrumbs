package breadcrumbs::Dialogs::SelectPresenceInSiteDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;

use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $for_wizard_d, $presences) = @_;

    my $for_wizard = defined($for_wizard_d) ? $for_wizard_d : 0;
    
    my $w = $c->widget('choose_presence_form')->method('post');
    my $fs = $w->element('Fieldset', 'presence_select')
	->legend('Scelta della presenza:');

    my $e = $fs->element( 'Select', 'presence' );
    $e->label('Presenza che vuoi scegliere:');

    if ( ! $for_wizard ) {
	$fs->element('Submit', 'get_public_site_submit' )->value('Ok');
    }

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    if (defined($presences)){
	$self->init_with_presences($c, $presences);
    }

    return $self;
}

sub init_with_presences{
    my ($self, $c, $presences) = @_;

    #Ok, I should now have the presences....
    my $e = $self->{widget}
    ->get_element( type => "Fieldset" )
	->get_element(type => "Select"); 


    my %options;
    $options{NULL} = "Nessuna di queste va bene";
    foreach(keys(%{$presences})){

	my $presence_text = $presences->{$_}->{presence};
	if ($presences->{$_}->{on_request} eq "1"){
	    $presence_text .= " a richiesta";
	}

	$options{$_} = $presence_text;
    }

    $e->options(%options);
}

sub on_init_dialog{
    my ($self, $c) = @_;

    my $presences = $c->session->{steps}->{presences};
    $self->init_with_presences($c, $presences) if defined($presences);
}


1;
