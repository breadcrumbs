package breadcrumbs::Dialogs::ChangeTotemDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;


use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c) = @_;

    my $w = $c->widget('change_totem_form')->method('post');

    my $fs = $w->element('Fieldset', 'totem_change_fs')
	->legend('Qui puoi cambiare il tuo totem:');

    my $old_totem = $c->user->my_profile->{totem};

    my $e = $fs->element('Textfield', 'old_totem')->label("Vecchio totem:")->value($old_totem);
    $e->attrs->{readonly} = "readonly";
    
    #then a simple text field for the new totem
    $fs->element('Textfield', 'new_totem')->label("Nuovo totem:");

    #I have the constraint
    $w->constraint("All", "new_totem")->message("Valore mancante");

    $fs->element('Submit', 'password_submit' )->value('Ok');

    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

####################
#accessors

sub get_password{
    my ($class, $result) = @_;

    return breadcrumbs::Widgets::PasswordsPairWidget->get_value($result);
}


1;
