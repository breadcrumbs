package breadcrumbs::Dialogs::PersonalUserDataDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;

use breadcrumbs::Widgets::EmailWidget;
use breadcrumbs::Widgets::PasswordsPairWidget;
use breadcrumbs::Widgets::NickWidget;
use breadcrumbs::Widgets::DataWidget;
use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c) = @_;

    my $w = $c->widget('personal_user_data')->method('post');

    my $fs = $w->element('Fieldset', 'founder_personal_data')
	->legend('Dati personali del fondatore:');

    $fs->element( 'Span', 'divider1' )->content('Nuovo fondatore, dati personali: ')->class('form-divider');;

    $fs->element('Textfield', "first_name")->label("Nome:");
    $fs->element('Textfield', "last_name")->label("Cognome:");
    $fs->element('Textfield', "address")->label("Indirizzo:");

    my $block = $fs->element('Block')->class('form-block');
    $block->element( 'Span', 'divider5')->content('Numeri di telefono')->class('form-divider-small');

    $fs->element('Textfield', 'home_phone')->label('Casa:')->size(40);
    $fs->element('Textfield', 'mobile_phone')->label('Cellulare:')->size(40);

    $block = $fs->element('Block', 'block1')->class('form-block');
    $block->element( 'Span', 'divider6')->content('Sesso:')->class('form-divider-small');
    
    my $e = $block->element( 'Radio', 'sex' );
    $e->label('maschio');
    $e->checked('checked');
    $e->value('m');

    $e = $block->element( 'Radio', 'sex' );
    $e->label('femmina');
    $e->value('f');

    #I should put the data widget
    breadcrumbs::Widgets::DataWidget->make_simple_control($w, $fs, "Data di nascita:", "birthdate");

    $fs->element('Textfield', 'document_number')->label("Carta d'identit_ampersand_agrave;")->size(40);

    $fs->element('Submit', 'comment_submit' )->value('Spedisci');
    $fs->element( 'Reset', 'reset_user')->value('Annulla');


    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

#######################
## static accessors

sub get_birthdate{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::DataWidget->get_value($result, "birthdate");
}

1;
