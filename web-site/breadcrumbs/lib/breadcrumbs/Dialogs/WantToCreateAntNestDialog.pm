package breadcrumbs::Dialogs::WantToCreateAntNestDialog;

# This file is part of the breadcrumbs web site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use HTML::Widget;

use breadcrumbs::Widgets::EmailWidget;
use breadcrumbs::Widgets::PasswordsPairWidget;
use breadcrumbs::Widgets::NickWidget;
use breadcrumbs::Dialogs::BcDialog;
use base(qw/breadcrumbs::Dialogs::BcDialog/);

sub new{
    my ($class, $c, $want_to_be_founder, $bookings_count, $founders_count) = @_;

    my $w = $c->widget('founder_ant_nest')->method('post');

    my $fs = $w->element('Fieldset', 'founder')
	->legend('Dati del fondatore:');

    #$fs->element('Textfield', "name")->label("Nome:");
    breadcrumbs::Widgets::NickWidget->make_simple_control($w, $fs);

    $fs->element('Hidden', 'want_to_be_founder')->value($want_to_be_founder);

    breadcrumbs::Widgets::EmailWidget->make_simple_control($w, $fs);

    if ($bookings_count == 0){
	$fs->element('Textfield', "ant_nest_name")->label("Nome del formicaio:");
	$w->constraint('All', 'ant_nest_name')->message('mi manca il nome');
    }

    if ($want_to_be_founder == 1){
	breadcrumbs::Widgets::PasswordsPairWidget->make_simple_control($w, $fs);
      }
    

    if ($founders_count > 0 && $want_to_be_founder == 1){

	my $area = $fs->element('Textarea', "comment_or_message")->label("Commento o messaggio:");    
	$area->cols(50);
	$area->rows(10);

    } 

    $fs->element('Submit', 'comment_submit' )->value('Spedisci');



    my $self = {};
    $self->{widget} = $w;
    bless ($self, $class);

    return $self;
}

####################
#accessors

sub get_nick{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::NickWidget->get_value($result);
}

sub get_ant_nest_name{
    my ($class, $result) = @_;
    return $result->param("ant_nest_name");
}

sub get_password{
    my ($class, $result) = @_;

    return breadcrumbs::Widgets::PasswordsPairWidget->get_value($result);
}

sub get_email{
    my ($class, $result) = @_;
    return breadcrumbs::Widgets::EmailWidget->get_value($result);
}

sub get_comment{
    my ($class, $result) = @_;

    if (defined($result->param("comment_or_message"))){
	return $result->param("comment_or_message");
    } else {
	return "";
    }
}

1;
