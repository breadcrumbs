package breadcrumbs::Controller::NewUsers;

use strict;
use warnings;

use Bcd::Constants::FoundersConstants;

use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;

=head1 NAME

breadcrumbs::Controller::NewUsers - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);


    bless ($self, $class);
    return $self;
}


#this is the action to view a local user.
sub view : LocalRegex('^view_(\d+)$') {
    my ($self, $c) = @_;


    my $id_to_serve = $c->req->captures->[0];

    #I should be able to see a new user... the page depends on the
    #relationship between myself and this user 

    my $id = $c->user->my_profile()->{id};
    
    if ( $id  == $id_to_serve){
	#well, I should go to the page..
	$c->res->redirect($c->uri_for('/newuser'));
	return 0;
    }

    #ok, let's see if I am in the correct state...
    if ($c->user->my_profile()->{id_status} <= 
	Bcd::Constants::FoundersConstants::WAITING_FOR_PERSONAL_DATA){
	#I see a normal page
	$c->flash->{status_msg} = 
	    "Non puoi vedere i dati personali di questo fondatore, perché tu non hai dato i tuoi";
	$c->res->redirect($c->req->referer());
    } else {
	#I can see the details of the other ant...
	$c->stash->{status_msg} = 
	    "ok, ecco i dati di questo fondatore";

	#ok, let's see the founder's profile.
	my $res = $c->model("BcdModel")->get_founder_profile($c, $id_to_serve);
	$self->success_needed($c, $res);

	$c->{stash}->{founder_details} = $res->{founder_details};

	my $trails = $c->stash->{trails};
	breadcrumbs::util::MenuUtils->add_trail
	    ("/newusers/view_$id_to_serve", $res->{founder_details}->{nick_or_name}, $trails, $c);
    }

    $c->stash->{template} = "other_founder_details.tt";


}

#I can be here ONLY if I am a founder
sub auto : Private {
    my ( $self, $c ) = @_;

    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    if ( ! $c->check_user_roles("founder")){
	$c->flash->{status_msg} = "Solo i fondatori di un formicaio possono entrare qui.";
	$c->res->redirect($c->uri_for('/'));
	return 0;
    }


    my $trails = $c->stash->{trails};
    my $an_id = $c->session->{current_ant_nest_id};

    #I should find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail
	("/antnestscontroller", "I formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail
	("/newantnests/view_building_ant_nests", "formicai in costruzione", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail
	("/newantnest/view_$an_id", "formicaio $an_id", $trails, $c);


    return 1;
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
