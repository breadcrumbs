package breadcrumbs::Controller::Users;

use strict;
use warnings;

use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;

=head1 NAME

breadcrumbs::Controller::Users - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);


    bless ($self, $class);
    return $self;
}

#this is the action to view a normal ant in the system
sub view : LocalRegex('^view_(\d+)$') {
    my ($self, $c) = @_;

    my $id_to_serve = $c->req->captures->[0];

    #I should be able to see a new user... the page depends on the
    #relationship between myself and this user 

    if ($c->user_exists()){
	my $id = $c->user->my_profile()->{id};

	#$c->log->debug("io ho id = $id e id_to_serve = $id_to_serve");
	
	if ( $id  == $id_to_serve){
	    #well, I should go to the user page...
	    $c->res->redirect($c->uri_for('/user'));
	    return 0;
	}

	#ok, I should get the profile of the other ant,
	#which includes his/her presence in the public sites...

	my $res = $c->model('BcdModel')->get_other_ant_profile
	    ($c, $id_to_serve);

	$c->stash->{res} = $res;

	if ($self->_is_valid_res($c, $res)){
	    $c->flash->{status_msg} = "ok";
	    my $trails = $c->stash->{trails};
	    breadcrumbs::util::MenuUtils->add_trail
		("/users/view_$id_to_serve", "$res->{other_ant}->{nick}", $trails, $c);

	    #if I trust this ant I can see his personal data...
	    #I can see the personal data in any case

	    my $menu = $c->stash->{menu_items};

	    breadcrumbs::util::MenuUtils->add_menu_entry
		('/users/view_this_ant_profile', 'Dati personali', $menu, $c);


	    $c->session->{sack}->{other_profile} = $res;
	    $c->session->{sack}->{id_to_serve} = $id_to_serve;
	    $c->session->{hold_the_sack} = 1;
	    

	} else {
	    $c->response->redirect($c->uri_for("/user"));
	    return $res->{exit_code}; 
	}
    }



    $c->stash->{template} = "other_ant.tt";

}

sub view_this_ant_profile : Local {
    my ($self, $c) = @_;

    $c->stash->{res} = $c->session->{sack}->{other_profile};
    $c->session->{hold_the_sack} = 1;

    $c->stash->{template} = "other_ant_personal_data.tt";

    my $trails = $c->stash->{trails};
    my $nick = $c->stash->{res}->{other_ant}->{nick};
    my $id = $c->session->{sack}->{id_to_serve};
    breadcrumbs::util::MenuUtils->add_trail
	("/users/view_$id", $nick, $trails, $c);
}

sub auto : Private {
    my ( $self, $c ) = @_;

    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};


    #I should find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);

    return 1;

}




=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
