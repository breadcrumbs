package breadcrumbs::Controller::NewAntNest;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use Bcd::Constants::NewAntNestsConstants;

use NEXT;

=head1 NAME

breadcrumbs::Controller::NewAntNest - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut



sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    bless ($self, $class);
    return $self;
}

sub founder_confirm : Local {
    my ( $self, $c ) = @_;

    #I should get the parameters...

    my $name = $c->req->param("nick");
    my $id_ant_nest = $c->req->param("id");
    my $token = $c->req->param("token");

    my $res = $c->model("BcdModel")->founder_confirm($id_ant_nest, $name, $token);

    $c->stash->{res} = $res;

    if ( $res->{exit_code} == Bcd::Errors::ErrorCodes::BEC_OK){
	$c->flash->{status_msg} = "Ok, $name, ora sei un fondatore registrato per il formicaio $id_ant_nest. Puoi collegarti";
	$c->res->redirect($c->uri_for('/authentication/login'));
    } else {
	my $msg = $breadcrumbs::ServerErrors::error_messages{$res->{exit_code}};
	$c->flash->{error_msg} = "Errore: $msg ($res->{exit_code})";
	$c->flash->{status_msg} = "$name, ho avuto un problema, o il gettone non è giusto o ti sei già registrato.";
	$c->res->redirect($c->uri_for('/'));
    }
}


sub view_ant_nest : LocalRegex('^view_(\d+)$') {
   my ( $self, $c ) = @_;

   my $id = $c->request->captures->[0];

   $c->session->{current_ant_nest_id} = $id;

   #ok, I should take the details of this ant nest

   my $res = $c->model('BcdModel')->get_new_ant_nest_detail($id);

   $c->session->{sack}->{new_ant_nest_details} = $res;
   $c->session->{hold_the_sack} = 1;


   #if I am a home ant I should be able to see all the other founders here...

   


   $c->detach(qq{view_booked_details});

}

sub view_booked_details : Local {
    my ($self, $c) = @_;

    $self->_process_table_sorting_orders($c);
    
    $c->stash->{template} = "new_ant_nest_details.tt";

    $c->stash->{new_ant_nest_details} = $c->session->{sack}->{new_ant_nest_details};
    $c->session->{hold_the_sack} = 1;

    if ( $c->user_exists()){
	#ok, I should see if the current user is in this ant nest
	my $id = $c->session->{current_ant_nest_id};

	if ($c->check_user_roles("guest_ant_$id")){
	    $self->_put_action_in_stash($c, "AntNest_guest_ant");
	} else {

	    #ok, the user is seeing his/her ant nest
	    #$self->_put_action_in_stash($c, "AntNest_home_ant");

	    #ok, I can have the list of all the founders here
	    my $res = $c->model("BcdModel")->get_founders_summary($c);
	    $c->session->{sack}->{founders_summary} = $res->{founders_summary};

	    $self->_put_action_in_stash($c, "ANC_want_to_send_a_message");
	}
    } else {
	if 
	    ($c->stash->{new_ant_nest_details}->{state} == 
	     Bcd::Constants::NewAntNestsConstants::WAITING_FOR_INITIAL_FOUNDERS)
	{
	    $self->_put_action_in_stash($c, "ANC_want_to_found");
	}
	$self->_put_action_in_stash($c, "ANC_want_to_be_informed");
    }
}

sub auto : Private {
    my ( $self, $c) = @_;

    my $trails = $c->stash->{trails};

    

    #I should find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail
	("/antnestscontroller", "I formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail
	("/newantnests/view_building_ant_nests", "formicai in costruzione", $trails, $c);

    #Now, if there is a current ant nest... I put also my trail
    #my $an_id = $c->session->{current_ant_nest_id};

    my $an_id  = $c->request->captures->[0];
    $an_id = $c->session->{current_ant_nest_id} if (!defined($an_id));

    if (Bcd::Data::PostCode::is_valid_post_code($an_id)){
    breadcrumbs::util::MenuUtils->add_trail
	("/newantnest/view_$an_id", "formicaio $an_id", $trails, $c);
    }
 
    return 1; #You can continue
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
