package breadcrumbs::Controller::AnPoints;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;
=head1 NAME

breadcrumbs::Controller::AnPages - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    #I fill some actions for this controller...

    bless ($self, $class);
    return $self;
}

sub index : Private {
    my ( $self, $c ) = @_;

    my $res = $c->model("BcdModel")->get_public_sites($c);
    $c->stash->{an_points} = $res->{public_sites};

    #ok, I can now list the public sites of this ant nest
    #if I am a boss I can create a public site, or modify it.

    $c->stash->{template} = 'an_points_index.tt';
}

sub view : LocalRegex('^view_(\d+)$') {
    my ( $self, $c ) = @_;

    #ok, I should get the details of this an point.
    my $id_site = $c->req->captures->[0];
    my $res= $c->model("BcdModel")->get_site_details($c, $id_site);
    $c->stash->{site_details} = $res->{site_details};

    $c->stash->{template} = 'an_point_view.tt';
}


sub auto : Private {
    my ( $self, $c) = @_;


    my $id = $c->session->{current_ant_nest_id};

    my $trails = $c->stash->{"trails"};
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$id", "Formicaio $id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/anpoints", "Bricio punti", $trails, $c);

    return 1; #You can continue
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
