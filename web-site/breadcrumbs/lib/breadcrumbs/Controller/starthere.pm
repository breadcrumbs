package breadcrumbs::Controller::starthere;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';

=head1 NAME

breadcrumbs::Controller::starthere - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub index : Private {
    my ( $self, $c ) = @_;

    $c->stash->{template} = "start_here_index.tt";
}

sub view_page : LocalRegex('^page_(\d+)$') {
    my ( $self, $c ) = @_;
    my $id_page = $c->request->captures->[0];
    $c->stash->{template} = "start_here_page_${id_page}.tt";
}

sub _make_menu{
    my ( $self, $c) = @_;

    my @menu = ();

    breadcrumbs::util::MenuUtils->add_menu_entry
	('give_me_the_recipe', 'Che buono!,  mi dai la ricetta?', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry
	('washing_machine_leaks', 'La lavatrice perde!', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry
	('mary_to_school', 'Chi accompagna Maria?', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry
	('vhs_trash', 'Butto il vecchio VHS?', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry
	('page_1', 'Presentazione', \@menu, $c);
	  
    $c->stash->{"menu_items"} = \@menu;
    
}

sub give_me_the_recipe : Local {
    my ( $self, $c) = @_;
    $c->stash->{template} = "give_me_the_recipe.tt";
}

sub vhs_trash : Local {
    my ( $self, $c) = @_;
    $c->stash->{template} = "vhs_trash.tt";
}

sub mary_to_school : Local {
    my ( $self, $c) = @_;
    $c->stash->{template} = "mary_to_school.tt";
}

sub washing_machine_leaks : Local {
    my ( $self, $c) = @_;
    $c->stash->{template} = "washing_machine_leaks.tt";
}



sub auto : Private {
    my ( $self, $c) = @_;

    my $trails = $c->stash->{"trails"};

    breadcrumbs::util::MenuUtils->add_trail("/help", "Aiuto", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/starthere", "Inizia da qui", $trails, $c);

    return 1; #You can continue
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
