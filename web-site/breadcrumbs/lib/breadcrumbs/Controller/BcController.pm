package breadcrumbs::Controller::BcController;

# This file is part of the breadcrumbs site.
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;


#this is the base class for all the controllers in the project
use base 'Catalyst::Controller';
use Bcd::Errors::ErrorCodes;

#this is the hash which stores all the possible actions in the system.
#it is a static hash, shared by all the controllers.
our %actions_hash;


sub end : ActionClass('RenderView') {
    my ( $self, $c ) = @_;

    #$self->_fill_actions($c); 
#the actions are done by every method...
    $self->_make_menu($c);
}

sub _make_menu{
}

sub _fill_actions{
    #you should redefine it in the child classes
}

#simply stores the action in the hash.
sub _register_action{
    my ($self, $action_name, $action) = @_;
    $actions_hash{$action_name} = $action;
}

#this will simply store in the hash an action of type href
sub _register_action_href{
    my ($self, $name, $url, $caption, @wrappers_to_do) = @_;

    my %action = (
		  type => "a",
		  base_url => $url, 
		  caption => $caption,
		  wrappers => \@wrappers_to_do,
		  );

    $self->_register_action($name, \%action);
}

#I can have a simple form to make an action
sub _register_form_action{
    my ($self, $name, $action, $caption_pre_input, $caption_post_input, $input_value, $submit_name) = @_;

    #the type is html, but the template is fixed
    my %action = (
		  type => "form",
		  html => "parametric_action_form_one_text.tt",
		  action => $action, 
		  caption_pre_input => $caption_pre_input,
		  caption_post_input => $caption_post_input,
		  input_value => $input_value,
		  submit_name => $submit_name,
		  );

    $self->_register_action($name, \%action);
}

#this method should simply put the named action in the stash
sub _put_action_in_stash{
    
    #$args should be a hash reference.
    my ($self, $c, $action_name, @args ) = @_;

    my $act_vector = $c->stash->{actions};

    #I should get the action from the hash...
    my $act = $actions_hash{$action_name};

    #something horrible happened
    die "unknown action!" if !$act; 



    #what type is it?
    if ($act->{type} eq "html"){
	#ok, this is an html action...
	my $html = $act->{html};
	my %hash = (
		    "html" => $html,
		    "caption" => $args[0] ? $args[0] : $act->{caption},
		    );

	push(@{$act_vector}, \%hash);
    } elsif ($act->{type} eq "a") {

	#my $url;
	#my $caption;
	#$act->{url} = !$args[0] ? $act->{url} : sprintf($act->{url}, $args[0]);
	#$act->{caption} = !$args[1] ? $act->{caption} : sprintf($act->{caption}, $args[1]);
	#breadcrumbs::util::MenuUtils->add_action($url, $caption, $act_vector, $c);	
	$act->{url} = $c->uri_for($act->{base_url});
	push(@{$act_vector}, $act);
      } elsif ($act->{type} eq "form" ){
	  push(@{$act_vector}, $act);
      }

}

#this method is called to process a dialog
sub _process_this_dialog{
    my ($self, $c, $dlg, $page_tt, $on_ok_action) = @_;

    #I should simply process the dialog...
    $dlg->action($c->uri_for("process_this_dialog_do"));

    #then I should store the things in the session:
    my %dialog_processing = 
	(
	 dialog_to_serve => $dlg,
	 page_which_stores_the_dialog => $page_tt,
	 on_ok_action => $on_ok_action,
	 );

    $c->session->{dialog_processing} = \%dialog_processing;
    $c->stash->{template} = $page_tt;
    $c->stash->{widget_result} = $dlg->result;
}

sub _make_wizard {
    my ($self, $c, $wizard_steps, $on_finish_action, $wizard_title) = @_;

    #I start to make an empty store for my steps.
    $c->session->{steps} = {};

    my $steps = $c->session->{steps};
    $steps->{on_finish_action} = $on_finish_action;
    $steps->{steps_number}     = scalar(@{$wizard_steps});
    $steps->{current_step}     = 0;
    $steps->{wizard_title}     = $wizard_title;

    my $index = 0;

    #I have to prepare the steps...
    foreach (@{$wizard_steps}){
	my $name = "step$index";
	$steps->{$name} = {};
	my $current_step = $steps->{$name};

	my $dlg = $_->[0];

	#all the dialogs share the same action.
	if (!defined($dlg)){
	    $current_step->{step_without_dialog} = 1;
	}

	$current_step->{dialog_to_serve}              = $_->[0];
	$current_step->{page_which_stores_the_dialog} = $_->[1];
	$current_step->{on_step_action}               = $_->[2];

	$index++;
    }

    #ok, now I should process this step
    $self->_process_current_wizard_step($c);
}

sub _process_current_wizard_step{
    my ($self, $c) = @_;

    my $current_step = $c->session->{steps}->{current_step};
    my $name = "step$current_step";

    my $steps = $c->session->{steps};

    $c->stash->{template}      = "wizard_page.tt";
    $c->stash->{template_inside_wizard}      
    = $steps->{$name}->{page_which_stores_the_dialog};

    #some pages can also be without a dialog 
    #(mainly the starting and the finish...).
    my $dlg = $steps->{$name}->{dialog_to_serve};
    if (defined ($dlg)){
	$dlg->on_init_dialog($c);
	$c->stash->{widget_result} = $dlg->result;
	$dlg->action($c->uri_for('_next_wizard_do'));
    }
}

sub _next_wizard_do : Local {
    my ($self, $c) = @_;

    #I should now make a dialog processing...
    my $current_step = $c->session->{steps}->{current_step};
    my $name = "step$current_step";
    my $step = $c->session->{steps}->{$name};


    if ( defined ($step->{step_without_dialog})){
	#ok, this step is without dialog, I continue
	$self->_choose_next_wizard_step($c, $current_step, $step);
	return 1;
    }

    my $dlg = $step->{dialog_to_serve};

    if ( ! defined($dlg)){
	#the session is invalid, maybe the user has messed with the browser back button a bit...
	$c->stash->{template} = "no_more_dialog.tt" ;
	$c->detach();
    }

    my $result = $dlg->process($c->req);
    $c->stash->{widget_result} = $result;

    if ($result->has_errors) {

	#remain in this step, please.
	$c->stash->{status_msg} = "Ci sono degli errori nel modulo, controlla per favore.";

	$c->stash->{template}      = "wizard_page.tt";
	$c->stash->{template_inside_wizard}      
	= $step->{page_which_stores_the_dialog};

    } else {
	#all ok
	$c->stash->{status_msg} = "Ok";


	#mmm, I should go to the next step...
	#if there is a next step, otherwise go to the finish action
	$c->stash->{result} = $result;

	$self->_choose_next_wizard_step($c, $current_step, $step);

	$c->detach();

    }
}

sub _choose_next_wizard_step : Private {
    my ($self, $c, $current_step, $step) = @_;

    if ($current_step < ($c->session->{steps}->{steps_number}-1)){

	my $steps_mov  = $c->forward($step->{on_step_action});
	my $error_code = $c->stash->{last_error};

	#I should control if I can go on...

	if ($steps_mov == 0){

	    if (defined($error_code)){
		my $msg = $breadcrumbs::ServerErrors::error_messages{$error_code};
		$c->stash->{error_msg} = "Errore: $msg ($error_code)";
	    }
	    $c->stash->{status_msg} = "Ho avuto un errore, ricontrolla per favore.";

	    $c->stash->{template}      = "wizard_page.tt";
	    $c->stash->{template_inside_wizard}      
	    = $step->{page_which_stores_the_dialog};
	    $c->detach();
	} else {
	    $c->session->{steps}->{current_step} += $steps_mov;

	    my $next_step = $c->session->{steps}->{current_step};
	    my $name = "step$next_step";
	    my $steps = $c->session->{steps};
	    #I memorize the backward step, in order to be able to repeat it
	    $steps->{$name}->{backward_movement} = $steps_mov;

	    $self->_process_current_wizard_step($c);
	}
    } else {

	#I can go to the finish action
	my $steps_mov = $c->forward($c->session->{steps}->{on_finish_action});

	if ( $steps_mov == 0 ){
	    $c->stash->{status_msg} = "Ho avuto un errore, ricontrolla per favore.";
	    $c->stash->{template}      = "wizard_page.tt";
	    $c->stash->{template_inside_wizard}      
	    = $step->{page_which_stores_the_dialog};
	    $c->detach();
	} else {
	    #ok... I can go to the first step
	    #$c->session->{steps}->{current_step} = 0;
	    #$self->_process_current_wizard_step($c);

	    #the wizard is useless now
	    #delete ($c->session->{steps});
	}
	
    }
}

sub starting_page : Local {
    my ($self, $c) = @_;

    $c->session->{steps}->{current_step} = 0;
    $self->_process_current_wizard_step($c);
}

sub previous_page : Local {
    my ($self, $c) = @_;

    my $current_step = $c->session->{steps}->{current_step};
    my $name = "step$current_step";
    my $steps = $c->session->{steps};
    #I memorize the backward step, in order to be able to repeat it
    my $steps_mov = $steps->{$name}->{backward_movement};

    if (!defined($steps_mov)){
	$steps_mov = 1;
    }

    $c->session->{steps}->{current_step} -= $steps_mov;
    $self->_process_current_wizard_step($c);
}

sub _is_valid_res {
    my ($self, $c, $res) = @_;

    if ( $res->{exit_code} != 0){
	my $msg = $breadcrumbs::ServerErrors::error_messages{$res->{exit_code}};
	$c->flash->{error_msg} = "$msg (Errore $res->{exit_code})";
	return 0;
    } else {
	return 1;
    }
    
}

sub no_more_dialog : Private {
   my ($self, $c) = @_;

   $c->stash->{template} = "no_more_dialog.tt";
}

#this is a local action which has a dialog inside the session.
sub process_this_dialog_do : Local{
    my ($self, $c) = @_;

    ##################################3
    ############ ONLY TO TEST
    $c->stash->{res} = $c->session->{res};
    ##################################

    #hold the sack, please.
    $c->session->{hold_the_sack} = 1;

    my $dlg = $c->session->{dialog_processing}->{dialog_to_serve};

    if ( ! defined($dlg)){
	#the session is invalid, maybe the user has messed with the browser back button a bit...
	$c->detach("no_more_dialog"); #go to home...
    }

    my $result = $dlg->process($c->req);
    $c->stash->{widget_result} = $result;

    if ($result->has_errors) {
	$c->stash->{status_msg} = "Ci sono degli errori nel modulo, controlla per favore.";
    } else {
	#all ok
	#$c->stash->{status_msg} = "Ok";





	#$c->log->debug("Sto per dirottarti a $copy->{on_ok_action} con dlg $dlg");
	$c->stash->{result} = $result;
	my $res = $c->forward($c->session->{dialog_processing}->{on_ok_action});

	if ($res == 0){
	    #all ok, 
	    delete($c->session->{dialog_processing});
	    delete($c->session->{res});
	    $c->flash->{status_msg} = "Operazione riuscita"
		if (!defined($c->flash->{status_msg}));
	    $c->detach('');
	} else {
	    my $msg = $breadcrumbs::ServerErrors::error_messages{$res};
	    $c->flash->{error_msg} = "Errore: $msg ($res)";
	    $c->flash->{status_msg} = "Operazione NON riuscita";
	}
    }

    $c->stash->{template} = $c->session->{dialog_processing}->{page_which_stores_the_dialog};
}

sub _process_table_sorting_orders{
    my ($self, $c) = @_;

    $c->stash->{sorted_tables} = $c->session->{data_cache}->{sorted_tables};

    my $table_to_sort = $c->req->param("table_to_sort");
    my $field_to_sort = $c->req->param("field_to_sort");

    return if (!defined($table_to_sort) or !defined($field_to_sort));

    $c->stash->{sorted_tables}->{$table_to_sort} = $field_to_sort;
    $c->session->{data_cache}->{sorted_tables} = $c->stash->{sorted_tables};

}

sub success_needed{
    my ($self, $c, $res) = @_;
    
    if ($res->{exit_code} != Bcd::Errors::ErrorCodes::BEC_OK){
	my $msg = $breadcrumbs::ServerErrors::error_messages{$res->{exit_code}};
	$c->stash->{error_msg} = "Errore: $msg ($res)";
	$c->stash->{status_msg} = "Mi dispiace, ho avuto un errore. Prova più tardi.";
	$c->res->redirect($c->uri_for("/"));
    }
}

sub valid_session_presence{
    my ($self, $c) = @_;
    if (! $c->user_exists()){
	$c->flash->{status_msg} = "Scollegato, o sezione scaduta; per favore ricollegati, grazie.";
	$c->res->redirect($c->uri_for(qq{/authentication/login}));
	return 0;
    } else {
	return 1;
    }
}

#this function simply should get a recordset and return an hash keyed in
#a particular field
sub _hashify_a_recordset{
    my ($self, $dataset, $key) = @_;

    my %hash;
    foreach(@{$dataset}){
	$hash{$_->{$key}} = $_;
	delete $_->{$key};
    }

    return \%hash;
}


1;
