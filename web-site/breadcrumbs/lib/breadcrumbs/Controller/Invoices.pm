package breadcrumbs::Controller::Invoices;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;

use breadcrumbs::Dialogs::SecretTokenDialog;
use breadcrumbs::Dialogs::InvoiceQuantityDialog;

=head1 NAME

breadcrumbs::Controller::Invoices - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);


    bless ($self, $class);
    return $self;
}

=head2 index 

=cut

sub index : Private {
    my ( $self, $c ) = @_;

    #this will show the summary of all the invoices for the user
    my $res = $c->model("BcdModel")->get_invoices_summary($c);
    $c->stash->{invoices_summary} = $res;

    $c->stash->{template} = "user_invoices.tt";
}

sub view_invoice_list : Local {
    my ( $self, $c ) = @_;

    #well, I should now list the invoices
    my $invoice_type = $c->req->param("invoice_type");
    my $required_state = $c->req->param("invoice_status");

    #ok, now I ask the list to the model
    my $res = $c->model("BcdModel")->get_invoices_list($c, $invoice_type, $required_state);

    #I have the dataset, and I should be able to sort it.
    $c->session->{sack}->{invoices_list}   = $res->{invoices_list};
    $c->session->{sack}->{invoice_type}    = $invoice_type;
    $c->session->{sack}->{invoice_status}  = $required_state;
    $c->session->{hold_the_sack} = 1;
    $c->response->redirect($c->uri_for("/invoices/view_invoice_list_cached"));
}

sub view_invoice_list_cached : Local {
    my ( $self, $c ) = @_;

    $self->_process_table_sorting_orders($c);
    $c->stash->{template} = 'view_invoices_list.tt';

    $c->stash->{invoices_list}  = $c->session->{sack}->{invoices_list};
    $c->stash->{invoice_status} = $c->session->{sack}->{invoice_status};
    $c->stash->{invoice_type}   = $c->session->{sack}->{invoice_type};
    $c->session->{hold_the_sack} = 1;
}

sub view_invoice : LocalRegex('^view_invoice_(\d+)$') {
    my ( $self, $c ) = @_;
    my $invoice_id = $c->req->captures->[0];

    #ok, I should simply ask the invoice details...
    my $res = $c->model("BcdModel")->get_invoice_details($c, $invoice_id);
    $c->stash->{invoice} = $res->{invoice_details};

    $c->stash->{template} = "invoices_view_invoice.tt";
}

sub pay_invoice : LocalRegex('^pay_(\d+)$') {
    my ( $self, $c ) = @_;

    my $invoice_id = $c->req->captures->[0];

    #ok, I should ask the payment plan
    my $res = $c->model("BcdModel")->get_price_estimate_for_invoice
	($c, $invoice_id);

    $c->session->{hold_the_sack} = 1;
    $c->session->{sack}->{price_estimate} = $res;
    $c->stash->{price_estimate} = $res;

    my @step0 = (undef, "pay_invoice_confirm.tt",  "pay_invoice_on_ok");
    my @step1 = (undef, "finished_pay_invoice.tt");

    my @steps = (\@step0, \@step1);

    $c->session->{sack}->{invoice_id} = $invoice_id;

    $self->_make_wizard($c, \@steps, '', "Pagamento fatture.");

}

sub pay_invoice_on_ok : Local {
    my ( $self, $c ) = @_;

    #I should really pay the invoice, here...

    $c->session->{hold_the_sack} = 1;
    $c->stash->{price_estimate} = $c->session->{sack}->{price_estimate};

    my $res = $c->model("BcdModel")->pay_invoice
	($c, $c->session->{sack}->{invoice_id});

    if ($self->_is_valid_res($c, $res)){
	$c->stash->{res} = $res;
	return 1;
    } else {
	return 0; 
    }
}

sub fill_invoice : LocalRegex('^fill_(\d+)$') {
    my ( $self, $c ) = @_;

    my $invoice_id = $c->req->captures->[0];

    my $dlg = breadcrumbs::Dialogs::InvoiceQuantityDialog->new($c, 1);
    my @step0 = ($dlg,  "fill_invoice_ask_quantity.tt",  "fill_invoice_quantity_on_ok");
    my @step1 = (undef, "finished_fill_invoice.tt");

    my @steps = (\@step0, \@step1);

    $c->session->{hold_the_sack} = 1;
    $c->session->{sack}->{invoice_id} = $invoice_id;

    $self->_make_wizard($c, \@steps, '', "Emissione fatture.");
    
}

sub fill_invoice_quantity_on_ok : Local {
    my ( $self, $c ) = @_;

    my $quantity = $c->req->param("quantity");
    $c->session->{hold_the_sack} = 1;

    #I should send the quantity to the daemon.
    my $res = $c->model("BcdModel")->emit_invoice
	($c, $c->session->{sack}->{invoice_id}, $quantity);

    if ($self->_is_valid_res($c, $res)){
	return 1;
    } else {
	return 0; 
    }
}

sub claim_invoice : LocalRegex('^claim_(\d+)$') {
    my ( $self, $c ) = @_;

    my $invoice_id = $c->req->captures->[0];
    
    #ok, let's get the blinded token
    my $res = $c->model("BcdModel")->get_invoice_blinded_token($c, $invoice_id);

    my $dlg = breadcrumbs::Dialogs::SecretTokenDialog->new($c, 1, $res->{blinded_token});

    my @step0 = ($dlg,  "claim_invoice_get_secret.tt",  "seller_has_secret_code_on_ok");
    my @step1 = (undef, "finished_claim_invoice.tt");

    my @steps = (\@step0, \@step1);

    $c->session->{hold_the_sack} = 1;
    $c->session->{sack}->{invoice_id} = $invoice_id;

    $self->_make_wizard($c, \@steps, '', "Incasso fatture.");
}

sub seller_has_secret_code_on_ok : Local {
    my ( $self, $c ) = @_;
    #ok, I should now get the secret token...

    $c->session->{hold_the_sack} = 1;
    
    my $result = $c->stash->{result};
    my $secret = breadcrumbs::Dialogs::SecretTokenDialog->get_secret_token($result);
    my $res = $c->model('BcdModel')->collect_invoice($c, $c->session->{sack}->{invoice_id}, $secret);

    if ($self->_is_valid_res($c, $res)){
	return 1;
    } else {
	return 0; 
    }
}

#here you can access ONLY if you are an ant
sub auto : Private {
    my ( $self, $c ) = @_;

    if ($self->valid_session_presence($c) == 0){
	return 0;
    }


    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();


    #I shouold find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/user", "$name", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/invoices", "Fatture", $trails, $c);

    return 1;

}

sub _make_menu{
    my ( $self, $c ) = @_;

    my $menu = $c->stash->{"menu_items"};;

#     breadcrumbs::util::MenuUtils->add_menu_entry('/census/view_ant_nest_census', 'Dettaglio fatture emesse', $menu, $c);
#     breadcrumbs::util::MenuUtils->add_menu_entry('/trust/view_my_trusts', 'Dettaglio fatture ricevute', $menu, $c);
#     breadcrumbs::util::MenuUtils->add_menu_entry('/invoices/view_my_invoices', 'Fatture emesse per attività', $menu, $c);
#     breadcrumbs::util::MenuUtils->add_menu_entry('/help/who_we_are', 'Fatture ricevute per attività', $menu, $c);
#     breadcrumbs::util::MenuUtils->add_menu_entry('view_balance', 'Emissione fatture', $menu, $c);
#     breadcrumbs::util::MenuUtils->add_menu_entry('/help/who_we_are', 'Pagamento fatture', $menu, $c);
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
