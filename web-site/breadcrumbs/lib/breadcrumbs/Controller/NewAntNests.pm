package breadcrumbs::Controller::NewAntNests;

use strict;
use warnings;
use base 'Catalyst::Controller';

use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;

=head1 NAME

breadcrumbs::Controller::NewAntNests - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    bless ($self, $class);
    return $self;
}




sub auto : Private {
    my ( $self, $c) = @_;

    my $trails = $c->stash->{trails};

    

    #I should find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail
	("/antnestscontroller", "I formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail
	("/newantnests/view_building_ant_nests", "formicai in costruzione", $trails, $c);
 
    return 1; #You can continue
}

sub view_building_ant_nests : Local {
    my ($self, $c) = @_;

    my $res = $c->model("BcdModel")->get_new_ant_nests_summary();

    #then I put the response in the sack
    $c->session->{sack}->{new_ant_nests} = $res->{new_ant_nests_summary};
    $c->session->{hold_the_sack}         = 1;



    #ok, let's redirect to the page that holds the table.

    $c->response->redirect($c->uri_for("view_building_ant_nests_table"));
}

sub view_building_ant_nests_table : Local {
    my ($self, $c) = @_;

    $self->_process_table_sorting_orders($c);

    #I still need the dataset
    $c->session->{hold_the_sack} = 1;

    #some actions in the stash...
    $self->_put_action_in_stash($c, "ANC_go_to_current_ant_nest");
    $self->_put_action_in_stash($c, "AntNestsController_search_ant_nests", 
				"Restringi la ricerca, trova solo quelli che iniziano per:");
    $self->_put_action_in_stash($c, "AntNestsController_create_ant_nest");

    $c->stash->{template} = 'new_ant_nests_summary.tt';
}

=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
