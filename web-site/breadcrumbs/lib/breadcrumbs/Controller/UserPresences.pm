package breadcrumbs::Controller::UserPresences;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;
use breadcrumbs::Dialogs::PresenceInPublicSiteDialog;
use breadcrumbs::Dialogs::AdLocalityDialog;
use breadcrumbs::Dialogs::SelectPublicSiteDialog;

=head1 NAME

breadcrumbs::Controller::UserPresences - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);


    bless ($self, $class);
    return $self;
}


sub index : Private {
    my ( $self, $c ) = @_;

    my $res = $c->model("BcdModel")->get_user_presences_summary($c);
    $c->stash->{res} = $res;
    $c->stash->{template} = 'up_index.tt';

}

sub view_presence_details : LocalRegex('^view_(\d+)$') {
    my ( $self, $c) = @_;

    my $id_locus= $c->req->captures->[0];

    #ok, I get the details of this ad!
    my $res = $c->model("BcdModel")->get_ads_for_locus($c, $id_locus);
    $c->stash->{ads_in_locus} = $res->{ads_in_locus};
    $c->stash->{presence}     = $res->{presence};
    $c->stash->{id_locus}     = $id_locus;
    
    $c->stash->{template} = "up_presence_details.tt";

    #this is used if the user wants to edit the presence
    $c->session->{sack}->{presence} = $res->{presence};
    $c->session->{hold_the_sack} = 1;
}

sub modify_presence_space : LocalRegex('^edit_space_(\d+)$') {
    my ( $self, $c) = @_;
    my $id_locus= $c->req->captures->[0];
    $c->session->{hold_the_sack} = 1;
    $c->session->{sack}->{id_locus_old} = $id_locus;

    #I don't have similar presences in time (yet), so I simply make a
    #wizard choosing another locus for this presence

    my $pub_res      = $c->model("BcdModel")->get_public_sites($c);
    my $public_sites_ar = $pub_res->{public_sites};

    my $public_sites = $self->_hashify_a_recordset($public_sites_ar, "id");

    my $dlg0 = breadcrumbs::Dialogs::AdLocalityDialog->new($c, 1);
    my $dlg1 = breadcrumbs::Dialogs::SelectPublicSiteDialog->new($c, 1, $public_sites);

    my @step0 = ($dlg0, "up_choose_new_locality.tt", "enter_new_locality_on_ok");
    my @step1 = ($dlg1, "up_choose_new_public_site.tt", "choose_new_public_site_on_ok");
    my @step2 = (undef, "up_edit_presence_done.tt");
    
    my @steps = (\@step0, \@step1, \@step2);
    $self->_make_wizard($c, \@steps, '', "Modifica di una presenza");

}

sub enter_new_locality_on_ok : Local {
    my ($self, $c) = @_;

    #I just see if the presence is a "special presence"
    my $loc = $c->req->param("flag");

    if ($loc eq "an_point"){
	#ok, the user wants a ant nest point...
	$c->session->{hold_the_sack} = 1;
	return 1;
    } else {
	#I should get the presences for this special point
	#I simply can issue the command
	my $res = $c->model("BcdModel")->change_site_presence
	    ($c, $c->session->{sack}->{id_locus_old}, 'NULL', $loc);

	$c->stash->{last_error} = $res->{exit_code};

	if ($res->{exit_code} == 0){
	    return 2;
	} else {
	    return 0;
	}
    }
}

sub choose_new_public_site_on_ok : Local {
    my ($self, $c) = @_;

    #ok, I now have to get the public site...
    my $public_site_id = $c->req->param("public_site");

    my $res = $c->model("BcdModel")->change_site_presence
	($c, $c->session->{sack}->{id_locus_old}, $public_site_id, 'NULL');

    $c->stash->{last_error} = $res->{exit_code};

    if ($res->{exit_code} == 0){
	return 1;
    } else {
	return 0;
    }
}

sub modify_presence : LocalRegex('^edit_(\d+)$') {
    my ( $self, $c) = @_;
    my $id_locus= $c->req->captures->[0];

    $c->session->{hold_the_sack} = 1;
    $c->session->{sack}->{id_locus_old} = $id_locus;

    #ok, I should get the similar presences to this one...
    my $res = $c->model("BcdModel")->get_similar_presences_in_space($c, $id_locus);
    $c->stash->{similar_presences} = $res->{similar_presences};

    #Now let's see if there are similar presences
    if (scalar(@{$res->{similar_presences}}) == 0){
	#ok, I must go to modify the presence
	$c->detach('edit_presence');
    } else {
	#I make a wizard, just to let the user choose the presence to put in place

	#I should hashify the similar presences
	my $presences_hashified = $self->_hashify_a_recordset($res->{similar_presences}, "id");

	my $dlg0 = breadcrumbs::Dialogs::SelectPresenceInSiteDialog->new($c, 1, $presences_hashified);
	my $dlg1 = breadcrumbs::Dialogs::PresenceInPublicSiteDialog->new($c, 1);

	#ok, then I make the wizard
	my @step0 = ($dlg0, "up_choose_similar_presence.tt", "choose_similar_presence_on_ok");
	my @step1 = ($dlg1, "enter_new_presence.tt", "edit_presence_on_ok");
	my @step2 = (undef, "up_edit_presence_done.tt");
	
	my @steps = (\@step0, \@step1, \@step2);
	$self->_make_wizard($c, \@steps, '', "Modifica di una presenza");
	$c->session->{steps}->{presence} = $c->session->{sack}->{presence};
    }

}

sub choose_similar_presence_on_ok : Local {
    my ($self, $c) = @_;

    #ok, let's see if the user has choosen something reasonable...
    my $presence = $c->req->param('presence');
    $c->session->{hold_the_sack} = 1;
    $c->session->{steps}->{presence_id} = $presence;
    $c->stash->{old_presence} = $c->session->{steps}->{presence};

    if ($presence eq "NULL"){
	#ok, the user wants to create a presence

	return 1;
    } else {
	#the user wants to simply change the presence
	my $res = $c->model("BcdModel")->move_ads_presence
	    ($c, $c->session->{sack}->{id_locus_old}, $presence);

	$c->stash->{last_error} = $res->{exit_code};

	if ($res->{exit_code} == 0){
	    return 2;
	} else {
	    return 0;
	}
    }
}

sub edit_presence : Private {
    my ( $self, $c) = @_;
    #mmm, I should simply present a dialog to the user...
    $c->session->{hold_the_sack} = 1;

    my $dlg = breadcrumbs::Dialogs::PresenceInPublicSiteDialog->new($c);

    $self->_process_this_dialog
	($c,
	 $dlg, 
	 "enter_new_presence.tt", 
	 'edit_presence_on_ok'); 
    $c->stash->{old_presence} = $c->session->{sack}->{presence};

    $c->detach();
}

sub edit_presence_on_ok : Local {
    my ( $self, $c) = @_;

    #ok, I should modify the presence

    my $presence_text = $c->req->param("available_hours");
    my $request = $c->req->param("request");

    my $req_flag = '1';
    if (!defined($request)){
	$req_flag = '0';
    }

    my $id_locus_to_change = $c->session->{sack}->{id_locus_old};

    #ok, I can issue the command...
    my $res = $c->model("BcdModel")->edit_user_presence
	($c, $id_locus_to_change, $req_flag, $presence_text);

    $c->res->redirect($c->uri_for('/userpresences'));

    return $res->{exit_code};

}


#I can be here only if there is a opened session
sub auto : Private {
    my ( $self, $c) = @_;

    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();


    #I shouold find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/user", "$name", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/userads", "I miei annunci", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/userpresences", "Luoghi e tempi", $trails, $c);

    return 1;

	
}



=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
