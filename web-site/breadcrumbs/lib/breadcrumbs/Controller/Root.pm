package breadcrumbs::Controller::Root;

use strict;
use warnings;

use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
#use base 'Catalyst::Controller';
use breadcrumbs::util::MenuUtils;
use breadcrumbs::Dialogs::MailingListSubscribeDialog;
#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config->{namespace} = '';

=head1 NAME

breadcrumbs::Controller::Root - Root Controller for breadcrumbs

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=cut

sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);


    $self->_register_action_href
 	("Root_demo", "want_demo", "Fammi fare un giro di prova");

    bless ($self, $class);
    return $self;
}

sub want_demo : Local {
    my ( $self, $c ) = @_;
    $c->stash->{template} = "want_demo.tt";
}


=head2 default

This is the main function of the application.

=cut

sub default : Private {
    my ( $self, $c ) = @_;

    $self->_put_action_in_stash($c, "ANC_go_to_current_ant_nest");
    $self->_put_action_in_stash($c, "Root_demo");
    #$self->_put_action_in_stash($c, "Root_comment_subscribe");

    $c->stash->{trail} = 1;

    my $w = $c->controller("AntNestsController")
	->make_search_from_post_code_widget
	($c,"Trova il formicaio a te pi_ampersand_ugrave; vicino, scrivi qui il tuo CAP:");
    $w->action($c->uri_for('antnestscontroller/get_from_post_code_exact'));
    $c->stash->{widget_result} = $w->result;
    $c->stash->{template} = "bc_hello.tt";
}

#this function should simply make the menus for this controller
sub _make_menu{
    my ( $self, $c) = @_;

    my @menu = ();

    if ( $c->namespace !~ /simplehelp/){
	breadcrumbs::util::MenuUtils->add_menu_entry('/help', 'Aiuto', \@menu, $c);
      }

    if ( $c->namespace eq ""){

	  breadcrumbs::util::MenuUtils->add_menu_entry
	      ('/antnestscontroller/', 'I Formicai', \@menu, $c);
          #breadcrumbs::util::MenuUtils->add_menu_entry('/help/who_we_are', 'Chi siamo', \@menu, $c);
      }


    $c->stash->{"menu_items"} = \@menu;
}

sub auto : Private {
    my ( $self, $c) = @_;
    $self->_make_menu($c);

    #I should start the trail from here
    my @trails = ();

    breadcrumbs::util::MenuUtils->add_trail('/', 'Inizio', \@trails, $c);

    $c->stash->{"trails"} = \@trails;

    #I initialize the actions vector.
    my @actions = ();
    $c->stash->{"actions"} = \@actions;

    #delete the sack, if it is not more used
    if (! exists($c->session->{hold_the_sack}) or $c->session->{hold_the_sack} == 0){
	if (exists($c->session->{sack})){
	    $c->log->info("posing the sack, too heavy...");
	    delete $c->session->{sack};
	    delete $c->session->{hold_the_sack};
	}
    } else {
	$c->session->{hold_the_sack} = 0; #next time I will pose it
    }

#     if (! exists($c->session->{hold_the_steps}) or $c->session->{hold_the_steps} == 0){
# 	if (exists($c->session->{steps})){
# 	    $c->log->info("posing the steps...");
# 	    delete $c->session->{steps};
# 	    delete $c->session->{hold_the_steps};
# 	}
#     } else {
# 	$c->session->{hold_the_steps} = 0; #next time I will pose it
#     }

    return 1; #You can continue
}

=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
