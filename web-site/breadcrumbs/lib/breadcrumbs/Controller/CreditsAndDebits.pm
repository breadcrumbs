package breadcrumbs::Controller::CreditsAndDebits;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;

sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    bless ($self, $class);
    return $self;
}

=head1 NAME

breadcrumbs::Controller::CreditsAndDebits - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub index : Private {
    my ( $self, $c ) = @_;

    #ok, I should get the report from the model
    my $res = $c->model("BcdModel")->get_my_credit_report($c);

    $c->session->{sack}->{credit_report} = $res;
    $c->session->{hold_the_sack} = 1;

    #$c->stash->{template} = "cad_index.tt";
    $c->response->redirect($c->uri_for("/creditsanddebits/view_credits_table"));
}

sub view_credits_table : Local {
    my ( $self, $c ) = @_;

    $self->_process_table_sorting_orders($c);
    $c->stash->{template} = 'cad_index.tt';

    $c->session->{hold_the_sack} = 1;
    $c->stash->{report} = $c->session->{sack}->{credit_report};
}


sub auto : Private {
    my ( $self, $c ) = @_;

    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();


    #I shouold find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/user", "$name", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/creditsanddebits", "Crediti e debiti", $trails, $c);
    
    return 1;

}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
