package breadcrumbs::Controller::authentication;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use breadcrumbs::authentication::bcstore;
use NEXT;

use breadcrumbs::Dialogs::LoginDialog;

=head1 NAME

breadcrumbs::Controller::authentication - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    #I fill some actions for this controller...

    $self->_register_action_href
	("Authentication_forgot_password", "forgotten_password", "Mi sono dimenticato la password");

    bless ($self, $class);
    return $self;
}

sub forgotten_password : Local {
    my ( $self, $c ) = @_;
    $c->stash->{template} = 'forgotten_password.tt';
}


our $users = breadcrumbs::authentication::bcstore->new();


=head2 login

    This method should perform the login.

=cut

sub login : Local {
   my ( $self, $c ) = @_;

   my $dlg = breadcrumbs::Dialogs::LoginDialog->new($c);

   $self->_process_this_dialog
       ($c,
	$dlg, 
	"login.tt2", 
	'/authentication/login_on_ok'); 

   $self->_put_action_in_stash($c, "Authentication_forgot_password");

   $c->detach();
}

sub fake_login : Local {
    my ($self, $c) = @_;

    my $username = "sergio";
    my $password = "sergiop";
    my $ant_nest = "1827300";
    my $id = $username . '.' . $ant_nest;
    my $user = $users->get_user($id, $c);

    if ($c->login($user, $password)){
	$c->flash->{status_msg} = "Ok. Ciao, $username";
	#I change the current ant nest...?
	$c->session->{current_ant_nest_id} = $ant_nest;
	$c->res->redirect($c->uri_for('/user'));
    } else {
	die "?????? somebody has changed sergio's password";
    }
}

sub login_on_ok : Local {
    my ($self, $c) = @_;

    my $username = $c->request->params->{nick};
    my $ant_nest = $c->request->params->{ant_nest_id};
    my $password = $c->request->params->{password};

    my $id = $username . '.' . $ant_nest;

    my $user = $users->get_user($id, $c);

    if ($c->login($user, $password)){
	$c->flash->{status_msg} = "Ok. Ciao, $username";
	#I change the current ant nest...?
	$c->session->{current_ant_nest_id} = $ant_nest;
	$c->res->redirect($c->uri_for('/user'));
	return 0; #all ok.
    } else {
	my $code = $user->{reason_for_not_login};
	$c->flash->{status_msg} = "Ho avuto un problema nell'autenticarti";
	$self->_put_action_in_stash($c, "Authentication_forgot_password");

	return $code;
    }
    
}

sub logout : Local {

    my ($self, $c) = @_;

    if ($c->user_exists()){
	# Clear the user's state
	$c->user->logout($c);
	$c->logout;
	$c->flash->{status_msg} = 'Ti sei scollegata/o con successo';
    } else {
	$c->flash->{status_msg} = 'Non sei collegato, non puoi scollegarti.';
    }


    $c->response->redirect($c->uri_for('login'));
}



# I should update the trail
sub auto : Private {
    my ( $self, $c) = @_;

    my $trails = $c->stash->{"trails"};
    my $url = $c->uri_for('login');
    my @item = ('Autenticazione', "$url");

    push(@{$trails}, \@item);

    return 1; #You can continue
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
