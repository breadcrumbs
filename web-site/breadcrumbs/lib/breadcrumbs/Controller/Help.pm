package breadcrumbs::Controller::Help;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';

use Data::Dumper;

=head1 NAME

breadcrumbs::Controller::Help - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub index : Private {
    my ( $self, $c ) = @_;

    $c->stash->{template} = "map_of_documentation.tt";
}

sub who_we_are : Local{
    my ( $self, $c ) = @_;
    $c->stash->{template} = "who_we_are.tt";
}

sub record_this_answer{
    my ($current_question, $current_answer, $list_of_faqs) = @_;
    my $quest = $$current_question;
    my $answ  = $$current_answer;
    my @faq = ($quest, $answ);
    push(@{$list_of_faqs}, \@faq);

    $$current_question = "";
    $$current_answer = "";

   #  print "<!--  REGISTRO LA DOMANDA \n";
#     print Dumper($list_of_faqs);
#     print "\n-->";
    
}

sub faq : Local{
   my ( $self, $c ) = @_;

   my $config = $c->config();
   $c->stash->{config} = $config;
   my $faq_file = $config->{home} . "/root/src/breadcrumbs-faq";

   open (INFILE, "<$faq_file") or die "Non trovo il file delle domande $faq_file!\n";

#    if ($ENV{"TEST_DB"}){
       
#        #open (INFILE, "<root/src/breadcrumbs-faq") or die "Non trovo il file delle domande in debug!\n";
#        open (INFILE, "<$faq_file") or die "Non trovo il file delle domande in debug!\n";
#    } else {
#        open (INFILE, "</var/www/bc/web-site/breadcrumbs/root/src/breadcrumbs-faq") or die "Non trovo il file delle domande!\n";
#    }

   my @faqs = ();
   #ok, now I read this file
   my $READING_SECTION  = 0;
   my $READING_QUESTION = 1;
   my $READING_ANSWER   = 2;
   my $INITIAL_STATE    = 3;

   my $state = $INITIAL_STATE;

   my $current_question;
   my $current_answer;
   my $current_section;

   my @current_faqs;

   while(<INFILE>){
       #chomp;

 #       print "<!--                             NUOVA RIGA $_  -->\n";
#        print "<!-- per ora la mia hash vale \n";
#        print Dumper(\%faqs);
#        print "\n ed il vettore vale : \n";
#        print Dumper(\@current_faqs);
#        print "\n-->";


       if (/--S--/){
	   if ($state == $READING_ANSWER){
	       #Ok, I was reading an answer, I should push it
	       record_this_answer(\$current_question, \$current_answer, \@current_faqs);

	       #I should finish the section...
	       my @temp = @current_faqs;
	       #$faqs{$current_section} = \@temp;
	       my @temp_vec = ($current_section, \@temp);
	       push (@faqs, \@temp_vec);

	       #print "<!-- registro la sezione $current_section \n";


	       @current_faqs = ();
	   }

	   $state = $READING_SECTION;
	   next;
       } elsif (/--Q--/){
	   if ($state == $READING_ANSWER){
	       #Ok, I was reading an answer, I should push it
	       record_this_answer(\$current_question, \$current_answer, \@current_faqs);
	   }
	   $state = $READING_QUESTION;
	   next;
       } elsif (/--A--/){
	   $state = $READING_ANSWER;
	   next;
       }
       
       #ok, I don't have a control section
       if ($state == $READING_SECTION){
	   chomp;
	   $current_section = $_;
       } elsif ($state == $READING_QUESTION){
	   if (/^$/){
	       $current_question .= "<p>";
	       next;
	   }
	   $current_question .= $_;
       } elsif ($state == $READING_ANSWER){
	   $current_answer .= $_;
	   if (/^$/){
	       $current_answer .= "<p>";
	       next;
	   }
       }

   }

   if ($state == $READING_ANSWER){
       #OK, I should register the answer and the section
       record_this_answer(\$current_question, \$current_answer, \@current_faqs);

       my @temp = @current_faqs;
       my @temp_vec = ($current_section, \@temp);
       push (@faqs, \@temp_vec);
   }

   close (INFILE);

   $c->stash->{faqs} = \@faqs;
   $c->stash->{template} = "faq.tt";
}

sub release : Local{
  my ( $self, $c ) = @_;
  $c->stash->{template} = "release.tt";
}

sub licence : Local{
  my ( $self, $c ) = @_;
  $c->stash->{template} = "licence_and_conditions.tt";
}

sub fake_users : Local{
  my ( $self, $c ) = @_;
  $c->stash->{template} = "fake_users.tt";
}

sub breadcrumb_and_gpg : Local {
    my ( $self, $c ) = @_;
    $c->stash->{template} = "breadcrumb_and_gpg.tt";
}


sub _make_menu{
    my ( $self, $c) = @_;

    my @menu = ();

    breadcrumbs::util::MenuUtils->add_menu_entry('licence', 'Licenza', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('breadcrumb_and_gpg', 'Sicurezza', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('faq', 'FAQ', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('/bcd-doc', 'Manuale', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('/simplehelp', 'Schede', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('/help/who_we_are', 'Chi siamo', \@menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('/starthere', 'Inizia da qui', \@menu, $c);
	  
    $c->stash->{"menu_items"} = \@menu;
    
}



sub auto : Private {
    my ( $self, $c) = @_;

    my $trails = $c->stash->{"trails"};
    my $url = $c->uri_for('/help');
    my @item = ('Aiuto', "$url");
    push(@{$trails}, \@item);

    return 1; #You can continue
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
