package breadcrumbs::Controller::User;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;
use breadcrumbs::ServerErrors;
use breadcrumbs::Widgets::TokenWidget;
use breadcrumbs::Dialogs::GetTotemNickDialog;
use breadcrumbs::Dialogs::TrustDialog;
use breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog;
use breadcrumbs::Dialogs::ChangePasswordDialog;
use breadcrumbs::Dialogs::ChangeTotemDialog;

use breadcrumbs::authentication::bcuser;

=head1 NAME

breadcrumbs::Controller::User - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    #I fill some actions for this controller...

    $self->_register_action_href
	("User_invite_ant", "invite_new_ant", "Voglio invitare un'altra persona in questo formicaio");

    $self->_register_action_href
	("User_confirm_ant", "confirm_new_ant", "Ho il gettone, voglio confermare una nuova formica");

    $self->_register_action_href
	("User_have_totem", "have_totem", "Una formica si fida di me: ho il suo totem.");

    $self->_register_action_href
	("User_insert_personal_trusts", "insert_personal_trusts", 
	 "Voglio specificare come divulgare i miei dati personali.");

    $self->_register_action_href
	("User_change_password", "change_password", "Voglio cambiare la mia password");

    $self->_register_action_href
	("User_change_totem", "change_totem", "Voglio cambiare il mio totem");

    bless ($self, $class);
    return $self;
}

sub change_password : Local {
    my ($self, $c) = @_;

    my $dlg = breadcrumbs::Dialogs::ChangePasswordDialog->new($c);

    $self->_process_this_dialog
	($c,
	 $dlg, 
	 "user_change_password.tt", 
	 '/user/change_password_on_ok'); 

    $c->detach();    
}

sub change_password_on_ok : Local {
    my ($self, $c) = @_;
    
    #I should get the result
    my $result = $c->stash->{result};
    my $old_password = $c->req->param("old_password");
    my $new_password = breadcrumbs::Dialogs::ChangePasswordDialog->get_password($result);

    #then I should issue the command to the daemon
    my $res = $c->model("BcdModel")->change_password($c, $old_password, $new_password);
    $c->res->redirect($c->uri_for("/user"));
    return $res->{exit_code};
}

sub change_totem : Local {
    my ($self, $c) = @_;

    my $dlg = breadcrumbs::Dialogs::ChangeTotemDialog->new($c);

    $self->_process_this_dialog
	($c,
	 $dlg, 
	 "user_change_totem.tt", 
	 '/user/change_totem_on_ok'); 
    $c->detach();    
}

sub change_totem_on_ok : Local {
    my ($self, $c) = @_;
    
    #I should get the result
    my $new_totem = $c->req->param("new_totem");

    #then I should issue the command to the daemon
    my $res = $c->model("BcdModel")->change_totem($c, $new_totem);
    $c->res->redirect($c->uri_for("/user"));

    if ($res->{exit_code} == 0){
	$c->user->outdate_profile();
    }
    return $res->{exit_code};
}

sub have_totem : Local {

    my ($self, $c) = @_;

    #I should prepare a "wizard"...
    my $dlg0 = breadcrumbs::Dialogs::GetTotemNickDialog->new($c, 1);
    my $dlg1 = breadcrumbs::Dialogs::TrustDialog->new($c, 1);

    #let's prepare the steps.
    my @step0 = ($dlg0, "have_totem.tt", '/user/have_totem_on_ok');
    #the last action is the finish action...
    my @step1 = ($dlg1, "confirm_trust_to_this_ant.tt", "/user/finished_trust");
    my @step2 = (undef, "trust_processed.tt");

    my @steps = (\@step0, \@step1, \@step2);

    $self->_make_wizard($c, \@steps, '', "Dare e ricevere fiducia.");

#     $self->_process_this_dialog
# 	($c,
# 	 $dlg, 
# 	 "have_totem.tt", 
# 	 '/user/have_totem_on_ok'); 

#     $c->detach();
}

sub finished_trust : Private {

    my ($self, $c) = @_;

    my $result = $c->stash->{result};

    my $trust = breadcrumbs::Dialogs::TrustDialog->get_trust($result);
    my $nick  = $c->session->{steps}->{user_details}->{nick};
    my $totem = $c->session->{steps}->{user_details}->{totem};

    #ok, I can call the model
    my $res = $c->model('BcdModel')->new_trust($c, $nick, $totem, $trust);

    if ( $self->_is_valid_res($c, $res)){
	$c->session->{steps}->{new_booking} = $res->{new_booking};
	return 1;
    } else {
	return 0;
    }

}


sub have_totem_on_ok : Private {
    my ($self, $c) = @_;

    
    #ok, I should get the user details
    #first I get the controls...
    my $result = $c->stash->{result};

    my $nick  = breadcrumbs::Dialogs::GetTotemNickDialog->get_nick($result);
    my $totem = breadcrumbs::Dialogs::GetTotemNickDialog->get_totem($result);

    #$c->stash->{status_msg} = "Vorresti i dettagli di $nick con il totem $totem";

    #ok, now let's call the daemon to get the details...
    my $res = $c->model('BcdModel')->get_user_details_from_totem($c, $nick, $totem);

    if ( $self->_is_valid_res($c, $res)){

	$c->session->{steps}->{user_details} = $res->{user_details};
	return 1;
    } else {
	return 0;
    }
    
}


=head2 index 

=cut

sub confirm_new_ant : Local {
    my ($self, $c) = @_;

    # Create the widget and set the action for the form
    my $w = breadcrumbs::Widgets::TokenWidget->make_token_widget($c);
    $w->action($c->uri_for('confirm_new_ant_do'));

    # Write form to stash variable for use in template
    $c->stash->{widget_result} = $w->result;
    $c->stash->{template} = "confirm_new_ant.tt";
}

sub confirm_new_ant_do : Local {
    my ( $self, $c ) = @_;

    my $w = breadcrumbs::Widgets::TokenWidget->make_token_widget($c);
    $w->action($c->uri_for('confirm_new_ant_do'));

    # Validate the form parameters
    my $result = $w->process($c->req);

    # Write form (including validation error messages) to
    # stash variable for use in template
    $c->stash->{widget_result} = $result;

    # Were their validation errors?
    if ($result->has_errors) {
	# Warn the user at the top of the form that there were errors.
	# Note that there will also be per-field feedback on
	# validation errors because of '$w->process($c->req)' above.
	$c->stash->{status_msg} = 'Non sembra un gettone valido.';
    } else {

	#ok, now I should be able to get the new user details from the model
	my $token = breadcrumbs::Widgets::TokenWidget->get_token($result);
	my $res = $c->model('BcdModel')->get_new_user_details($c, $token);

	if ($res->{exit_code} == 0){

	    $c->stash->{status_msg} = 'Ok, fatto.';
	    $c->stash->{template} = 'new_user_details.tt';
	    $c->session->{new_user} = $res->{new_user_details};
	    $c->stash->{new_user} = $res->{new_user_details};

	    my $w = $self->make_trust_widget($c);
	    $w->action($c->uri_for('confirm_new_ant_trust_given'));
	    $c->stash->{widget_result} = $w->result;

	    #my $req = $c->req;
	    #my $token = 
		#$req->param("token1") . " " . $req->param("token2") . " " . $req->param("token3");

	    $c->flash->{token} = $token;
	    $c->session->{token} = $token;

	    return;
	} else {
	    my $msg = $breadcrumbs::ServerErrors::error_messages{$res->{exit_code}};
	    $c->stash->{error_msg} = "Errore $res->{exit_code}: $msg";
	}

      
    }

    # Set the template
    $c->stash->{template} = 'confirm_new_ant.tt';
}

sub confirm_new_ant_trust_given : Local {
    my ( $self, $c ) = @_;

    #ok, let's try to confirm this ant
    my $w = $self->make_trust_widget($c);
    $w->action($c->uri_for('confirm_new_ant_trust_given'));

    # Validate the form parameters
    my $result = $w->process($c->req);

    # Write form (including validation error messages) to
    # stash variable for use in template
    $c->stash->{widget_result} = $result;

    # Were their validation errors?
    if ($result->has_errors) {
	$c->stash->{status_msg} = qq{Non va bene la fiducia};
	$c->stash->{new_user} = $c->session->{new_user};
	$c->stash->{template} = 'new_user_details.tt';
	return;
    } else {
	#delete the new user from session
	delete($c->session->{new_user});
	#ok, I can confirm the ant
	my $res = $c->model('BcdModel')->confirm_new_user($c, 
							  $c->session->{token}, 
							  $c->req->param("trust_second") / 100.0);

	delete($c->session->{token});

	if ($res->{exit_code} == 0){
	    $c->stash->{status_msg} = "Ok, xx può ora entrare nel formicaio, datele il gettone.";
	} else {
	    my $msg = $breadcrumbs::ServerErrors::error_messages{$res->{exit_code}};
	    $c->stash->{error_msg} = "Errore $res->{exit_code}: $msg";
	}
	    $c->detach('confirm_new_ant');
    }


}

sub invite_new_ant : Local {
    my ( $self, $c ) = @_;

    # Create the widget and set the action for the form
    my $w = $self->make_user_widget($c);
    $w->action($c->uri_for('make_user_do'));

    # Write form to stash variable for use in template
    $c->stash->{widget_result} = $w->result;
    $c->stash->{template} = "invite_new_ant.tt";
}

sub make_user_do : Local{
    my ( $self, $c ) = @_;

    #ok, I should validate the widget.

# Create the widget and set the action for the form
    my $w = $self->make_user_widget($c);
    $w->action($c->uri_for('make_user_do'));

    # Validate the form parameters
    my $result = $w->process($c->req);

    # Write form (including validation error messages) to
    # stash variable for use in template
    $c->stash->{widget_result} = $result;

    # Were their validation errors?
    if ($result->has_errors) {
	# Warn the user at the top of the form that there were errors.
	# Note that there will also be per-field feedback on
	# validation errors because of '$w->process($c->req)' above.
	$c->stash->{error_msg} = 'Ci sono degli errori nel modulo, per favore controlla...';
    } else {
	#ok, in this case I should be able to communicate to the server
	my $res = $c->model('BcdModel')->create_normal_user($c);

	if ($res->{exit_code} == 0){
	    my $token = $res->{token};
	    $c->stash->{status_msg} = 'Ok, fatto.';
	    $c->stash->{template} = 'invite_new_ant_got_first_token.tt';
	    $c->stash->{token} = $res->{token};
	    $c->stash->{new_nick} = $c->req->param('nick');
	    return;
	} else {
	    my $msg = $breadcrumbs::ServerErrors::error_messages{$res->{exit_code}};
	    $c->stash->{error_msg} = "Errore $res->{exit_code}: $msg";
	}
      
    }

    # Set the template
    $c->stash->{template} = 'invite_new_ant.tt';
}

sub make_trust_widget{
    my ($self, $c, $caption) = @_;

    my $cap = defined($caption) ? $caption : "Fiducia:";

    # Create an HTML::Widget to build the form
    my $w = $c->widget('trust_form')->method('post');
    my $fs = $w->element('Fieldset', 'new_user')->legend('Ho bisogno di una fiducia:');

    $fs->element('Textfield', 'trust_second')->label('Fiducia (1-100):');
    $fs->element('Submit',    'trust_submit' )->value('Ok');

    $w->constraint('Range', 'trust_second')
	->message('La fiducia va da 1 a 100')->min(1)->max(100);
    
    $w->constraint( 'All', 'trust_second')->message('Valore mancante');

    return $w;
}


sub make_user_widget {
    my ($self, $c) = @_;

    # Create an HTML::Widget to build the form
    my $w = $c->widget('user_form')->method('post');

    my $fs = $w->element('Fieldset', 'new_user')->legend('Dettagli nuova formica');

    $fs->element( 'Span', 'divider1')->content('Primo tutore: ')->class('form-divider');

    $fs->element('Textfield', 'nick_first_tutor')->label('Nick:')->size(40)
	->value($c->user->name());
    $fs->element('Textfield', 'trust_first')->label('Fiducia (1-100):');

    $fs->element( 'Span', 'divider2' )->content('Secondo tutore: ')->class('form-divider');

    $fs->element( 'Span', 'divider3' )->content('Nuova formica, dati generici: ')->class('form-divider');;

    # Create the form feilds
    $fs->element('Textfield', 'nick')->label('Nick:')->size(40);
    $fs->element('Textfield', 'email')->label('E-mail:')->size(40);
    
    $fs->element( 'Span', 'divider4')->content('Nuova formica, dati personali: ')->class('form-divider');

    $fs->element('Textfield', 'first_name')->label('Nome:')->size(40);
    $fs->element('Textfield', 'last_name')->label('Cognome:')->size(40);
    $fs->element('Textfield', 'address')->label('Indirizzo:')->size(40);

    my $block = $fs->element('Block')->class('form-block');
    $block->element( 'Span', 'divider5')->content('Numeri di telefono')->class('form-divider-small');

    $fs->element('Textfield', 'home_phone')->label('Casa:')->size(40);
    $fs->element('Textfield', 'mobile_phone')->label('Cellulare:')->size(40);

    $block = $fs->element('Block', 'block1')->class('form-block');
    $block->element( 'Span', 'divider6')->content('Sesso:')->class('form-divider-small');
    
    my $e = $block->element( 'Radio', 'sex' );
    $e->label('maschio');
    $e->checked('checked');
    $e->value('m');

    $e = $block->element( 'Radio', 'sex' );
    $e->label('femmina');
    $e->value('f');


    $fs->element('Span', 'id3993')->content('Data di nascita:')->class('form-label');    
    $block = $fs->element('Block', 'block2')->class('test-inline-controls');
    $block->element('Textfield', 'day');
    $block->element('Span', 'id1122')->content('/');
    $block->element('Textfield', 'month');
    $block->element('Span', 'id11209')->content('/');
    $block->element('Textfield', 'year');

#     $fs->element('Span', 'id3994')->content('Gettone avuto:')->class('form-label');    
#     $block = $fs->element('Block', 'block3')->class('test-inline-controls');
#     $block->element('Textfield', 'token1')->class('token-input');
#     $block->element('Textfield', 'token2')->class('token-input');
#     $block->element('Textfield', 'token3')->class('token-input');

    $fs->element('Textfield', 'document_number')->label("Carta d'identit_ampersand_agrave;")->size(40);

    $fs->element('Submit',    'submit' )->value('Invia');
    $fs->element( 'Reset', 'reset_user')->value('Cancella');


    $w->constraint(Integer => qw/home_phone mobile_phone/)
	->message('Vorrei un intero, grazie');

    $w->constraint(Email => qw/email/)
	->message('Deve essere un indirizzo email valido');

    $w->constraint('Date', 'year', 'month', 'day')
	->message('Questa _ampersand_egrave; una data inesistente.');

    $w->constraint('Range', 'trust_first')
	->message('La fiducia va da 1 a 100')->min(1)->max(100);
    
    $w->constraint( 'All', 
		    'first_name', 'trust_first',
		    'last_name', 'address', 
		    'home_phone', 'nick', 'email', 
		    'day', 'month', 'year' )->message('Valore mancante');

#     $w->constraint('Regex', 'token1', 'token2', 'token3')
# 	->message('<-- controlla qui, per favore.')->regex(qr/^[a-zA-Z0-9]{3}$/);
    
    # Return the widget
    return $w;
}

sub index : Private {
    my ( $self, $c ) = @_;

    if ($c->user->{role} & breadcrumbs::authentication::bcuser::FOUNDER_ROLE){

	#I am a founder... I should not be here
	$c->res->redirect($c->uri_for('/newuser'));

    } else {

	#I put some activities
	$self->_put_action_in_stash($c, "User_invite_ant");
	$self->_put_action_in_stash($c, "User_confirm_ant");
	$self->_put_action_in_stash($c, "User_have_totem");
	#$self->_put_action_in_stash($c, "User_open_new_activity");
	#$self->_put_action_in_stash($c, "User_manage_accounts");
	$c->stash->{template} = "user_profile.tt";
    }
}

sub my_profile : Local {
    my ( $self, $c ) = @_;
    
    my ($res, $net) =  $c->user->my_trust_net();


    if ($c->user->my_profile->{missing_trusts} == 0){




	my $personal_data_trusts = $c->user->my_profile->{personal_data_trusts};

	#ok, I can also have the table with the personal data trusts
	my @personal_trusts_dataset;

	foreach(keys(%{$personal_data_trusts})){
	    my $trust_item;
	    if ($_ eq "t_first_name"){
		$trust_item = "Nome:"
	    } elsif ($_ eq "t_last_name"){
		$trust_item = "Cognome:"
	    } elsif ($_ eq "t_address"){
		$trust_item = "Indirizzo:"
	    } elsif ($_ eq "t_home_phone"){
		$trust_item = "Telefono di casa:"
	    } elsif ($_ eq "t_mobile_phone"){
		$trust_item = "Cellulare:"
	    } elsif ($_ eq "t_sex"){
		$trust_item = "Sesso:"
	    } elsif ($_ eq "t_birthdate"){
		$trust_item = "Data di nascita:"
	    } elsif ($_ eq "t_email"){
		$trust_item = "email:"
	    }

	    my $record = {
		trust_item      => $trust_item,
		trust_item_orig => $_,
		trust_value     => $personal_data_trusts->{$_},
	    };

	    push (@personal_trusts_dataset, $record);
	}

	$c->session->{sack}->{personal_trusts_dataset} = \@personal_trusts_dataset;

	my @personal_diffusion;
	foreach(keys(%{$net})){
	    
	    #ok, I make some records
	    my $nick  = $net->{$_}->[0];
	    my $trust = $net->{$_}->[1];
	    my $direct = $net->{$_}->[2];

	    my $view_items;

	    if ($direct == 0){

		if ($trust > $personal_data_trusts->{t_first_name}){
		    $view_items .= "nome, ";
		}

		if ($trust > $personal_data_trusts->{t_last_name}){
		    $view_items .= "cognome, ";
		}

		if ($trust > $personal_data_trusts->{t_address}){
		    $view_items .= "indirizzo, ";
		}

		if ($trust > $personal_data_trusts->{t_home_phone}){
		    $view_items .= "telefono di casa, ";
		}

		if ($trust > $personal_data_trusts->{t_mobile_phone}){
		    $view_items .= "cellulare, ";
		}

		if ($trust > $personal_data_trusts->{t_sex}){
		    $view_items .= "sesso, ";
		}

		if ($trust > $personal_data_trusts->{t_birthdate}){
		    $view_items .= "data di nascita, ";
		}

		if ($trust > $personal_data_trusts->{t_email}){
		    $view_items .= "email,";
		}
	    } else {
		$view_items = "È fidato, quindi può vedere tutto";
	    }

	    my $record1 = {
		nick => $nick,
		trust => $trust,
		viewable => $view_items,
		id => $_,
	    };

	    push(@personal_diffusion, $record1);

	}


	
	$c->session->{sack}->{personal_diffusion} = \@personal_diffusion;
	$c->session->{hold_the_sack} = 1;
    }

    $c->response->redirect($c->uri_for("/user/my_profile_table"));
}

sub change_trust_p_d : Local {
    my ($self, $c) = @_;

    $c->session->{hold_the_sack} = 1;

    my $trust_item_to_change  = $c->req->param("trust_item_orig");
    my $new_trust = $c->req->param("new_trust");

    my $trust = $c->req->param("new_trust");
    if ($trust < 1 or $trust > 100) {
	$c->flash->{status_msg} = "La fiducia deve stare tra 1 e 100";
	$c->response->redirect($c->req->referer);
	$c->detach();
    }

    my $res = $c->model("BcdModel")->change_personal_data_trust($c, $trust_item_to_change, $new_trust);

    if ($res->{exit_code} != 0)
    {
	$c->flash->{status_msg} = "Ho avuto un errore...";
	$c->detach('my_profile_table');
    } else {
	$c->flash->{status_msg} = "Ok, fiducia cambiata.";
    }

    $c->user->outdate_profile();
    $c->response->redirect($c->uri_for('/user/my_profile'));
}

sub my_profile_table : Local {

    my ( $self, $c ) = @_;

    $self->_process_table_sorting_orders($c);

    if ($c->user->my_profile->{missing_trusts} == 1){
	$self->_put_action_in_stash($c, "User_insert_personal_trusts");
    }

    $self->_put_action_in_stash($c, "User_change_password");
    $self->_put_action_in_stash($c, "User_change_totem");

    $c->stash->{personal_diffusion} = $c->session->{sack}->{personal_diffusion};
    $c->stash->{personal_trusts_dataset} = $c->session->{sack}->{personal_trusts_dataset};
    $c->session->{hold_the_sack} = 1;

    $c->stash->{template} = "user_personal_data.tt";
}

sub insert_personal_trusts : Local {
    my ($self, $c) = @_;

    #ok, I should simply make a dialog, and in that dialog I will ask
    #the user how he want to diffuse the personal data.

    my $dlg = breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog->new($c);

    $self->_process_this_dialog
	($c,
	 $dlg, 
	 "insert_personal_trusts.tt", 
	 '/user/insert_personal_trusts_on_ok'); 

    $c->detach();
}

sub insert_personal_trusts_on_ok : Local {
    my ($self, $c) = @_;

    #ok, now I get all this trusts from the result
    my $result = $c->stash->{result};

    my $t_first_name = breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog->get_t_first_name($result);
    my $t_last_name = breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog->get_t_last_name($result);
    my $t_address = breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog->get_t_address($result);
    my $t_home_phone = breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog->get_t_home_phone($result);
    my $t_mobile_phone = breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog->get_t_mobile_phone($result);
    my $t_sex = breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog->get_t_sex($result);
    my $t_birthdate = breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog->get_t_birthdate($result);
    my $t_email = breadcrumbs::Dialogs::PersonalUsersDataTrustsDialog->get_t_email($result);

    #ok, now I can issue the command to the daemon
    my $res = $c->model("BcdModel")->insert_personal_users_data_trusts
	($c, $t_first_name, $t_last_name, $t_address, 
	 $t_home_phone, $t_mobile_phone, $t_sex, $t_birthdate, $t_email);

    if ( $self->_is_valid_res($c, $res)){
	$c->flash->{status_msg} = "Ok, fatto";
	$c->res->redirect($c->uri_for('/user/my_profile'));
	$c->user->outdate_profile();
	return 0;
    } else {
	return $res->{exit_code};
    }
}

sub _make_menu{
    my ( $self, $c ) = @_;

    my $menu = $c->stash->{"menu_items"};;

    breadcrumbs::util::MenuUtils->add_menu_entry('my_profile', 'Dati personali', $menu, $c);

    if ($c->check_user_roles('treasurer')){
	breadcrumbs::util::MenuUtils->add_menu_entry('/cashantnesteuro', 'Gestione cassa', $menu, $c);
      }

    breadcrumbs::util::MenuUtils->add_menu_entry('/invoices', 'Fatture', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('/creditsanddebits', 'Crediti e debiti', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('/cashusertao', 'C/C in Tao', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('/cashusereuro', 'C/C in Euro', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('/userads', 'I miei annunci', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('/trust/view_my_trusts', 'Gestione fiducia', $menu, $c);

    #these are useless now, because they add clutter and are fake...
    
    #breadcrumbs::util::MenuUtils->add_menu_entry('/help/who_we_are', 'Assegni', $menu, $c);
    #breadcrumbs::util::MenuUtils->add_menu_entry('view_balance', 'Bilancio', $menu, $c);
    #breadcrumbs::util::MenuUtils->add_menu_entry('/help/who_we_are', 'Attività', $menu, $c);
}

sub view_balance : Local{
   my ( $self, $c ) = @_;

   $c->stash->{template} = "user_balance.tt";
}

#here you can access ONLY if you are an ant
sub auto : Private {
    my ( $self, $c ) = @_;




    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    #the current ant nest is different
    $c->session->{current_ant_nest_id} = $c->user->ant_nest;


    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();


    #I shouold find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/user", "$name", $trails, $c);

    return 1;

}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
