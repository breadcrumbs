package breadcrumbs::Controller::Trust;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';

=head1 NAME

breadcrumbs::Controller::Trust - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    $self->_register_action_href
	("Trust_have_totem", "register_ant_nest", "Una persona si fida di me, ho il suo totem");


    $self->_register_action_href
	("Trust_commit_changes", "/trust/commit_trust_changes", "Ho finito, rendi permanenti i cambiamenti");

    $self->_register_action_href
	("Trust_undo_changes", "/trust/rollback_trust_changes", "Annulla tutti i cambiamenti fatti.");

    #my ($self, $name, $action, $caption_pre_input, $caption_post_input, $submit_name) = @_;
    $self->_register_form_action
	("Trust_search_coverage", 
	 "/trust/change_trust_limit", 
	 "Fammi vedere le formiche raggiungibili con fiducia maggiore di ", " bB", 
	 "trust_limit",
	 "trust_limit_submit"
	 );

    bless ($self, $class);
    return $self;
}

=head2 index 

=cut

sub index : Private {
    my ( $self, $c ) = @_;

    $c->response->body('Matched breadcrumbs::Controller::Trust in Trust.');
}

sub commit_trust_changes : Local {

  my ($self, $c) = @_;
  my $res = $c->model("BcdModel")->commit_trust_changes($c);
  $c->response->redirect($c->uri_for('/trust/view_my_trusts'));
}

sub rollback_trust_changes : Local {

  my ($self, $c) = @_;
  my $res = $c->model("BcdModel")->rollback_trust_changes($c);
  $c->response->redirect($c->uri_for('/trust/view_my_trusts'));
}

sub change_trust : Local {
   my ($self, $c) = @_;

   my $nick  = $c->req->param("nick");
   my $trust = $c->req->param("new_trust");

   if ($trust < 1 or $trust > 100) {
       $c->flash->{status_msg} = "La fiducia deve stare tra 1 e 100";
       $c->response->redirect($c->req->referer);
       $c->detach();
   }

   my $res = $c->model("BcdModel")->change_trust($c, $nick, $trust/100.0);

   if ($res->{exit_code} != 0)
   {
       $c->flash->{status_msg} = "Ho avuto un errore...";
       $c->detach('view_my_trusts_cached');
   } else {
       $c->flash->{status_msg} = "Ok, fiducia cambiata, ci vorrà qualche secondo per vedere tua la nuova rete";
   }

   #I issue the command to the model... 
   $c->response->redirect($c->uri_for('/trust/view_my_trusts'));
}

sub view_my_trusts_cached : Local {
    my ($self, $c) = @_;

    $self->_process_table_sorting_orders($c);

    $c->stash->{template} = 'user_trusts.tt';

    $c->stash->{trusted_ants} = $c->session->{sack}->{trusted_ants};
    $c->stash->{trust_limit}  = $c->session->{sack}->{trust_limit};

    $self->get_the_trust_net_from_sack($c);

    $self->_put_action_in_stash($c, "Trust_search_coverage");
    $self->_put_action_in_stash($c, "Trust_commit_changes");
    $self->_put_action_in_stash($c, "Trust_undo_changes");

    #I still need the dataset
    $c->session->{hold_the_sack} = 1;
}

sub view_my_trust_net_limited_table : Local {
    my ($self, $c) = @_;

    $self->_process_table_sorting_orders($c);

    $c->stash->{template} = 'user_view_ad_reachable_set.tt';

    $c->stash->{trust_limit}  = $c->session->{sack}->{trust_limit};
    $c->stash->{pay_credit}   = $c->session->{sack}->{pay_credit};

    $self->get_the_trust_net_from_sack($c);

    #I still need the dataset
    $c->session->{hold_the_sack} = 1;
}

sub get_the_trust_net_from_sack{
    my ($self, $c) = @_;

    my ($res, $net) =  $c->user->my_trust_net();

    if ($c->user->{last_error} == Bcd::Errors::ErrorCodes::BEC_OK){
	#The net has changed!
	$self->compile_the_trust_net($c);
    } elsif ($c->user->{last_error} == Bcd::Errors::ErrorCodes::BEC_DATA_NOT_READY){
	$c->stash->{user_net}    = undef;
	$c->stash->{server_code} = $c->user->{last_error};
    } else {
	$c->stash->{user_net}    = $c->session->{sack}->{user_net};
	$c->stash->{server_code} = $c->session->{sack}->{server_code};
    }
}

sub compile_the_trust_net{
    my ($self, $c) = @_;

    #I simply put in stash my net
    my ($res, $net) =  $c->user->my_trust_net();

    if ($res != 1){

	$c->session->{sack}->{server_code}  = $c->user->{last_error};

	#I should convert the net in a dataset;
	my @dataset;
	foreach(keys(%{$net})){
	    my $record = {
		id => $_,
		nick  => $net->{$_}->[0],
		trust => $net->{$_}->[1],
	    };
	    push (@dataset, $record);
	    
	}

	$c->session->{sack}->{user_net} = \@dataset;

    } else {
	$c->session->{sack}->{user_net} = undef;
    }
}

sub view_trust_net_for_ad : Local {
    my ($self, $c) = @_;
    
    $c->session->{sack}->{trust_limit} = $c->req->param('trust_limit');
    $c->session->{sack}->{pay_credit}  = $c->req->param('pay_credit');
    $self->compile_the_trust_net($c);
    $c->session->{hold_the_sack} = 1;
    $c->response->redirect($c->uri_for("/trust/view_my_trust_net_limited_table"));
}

#this function should simply list the trust from this user...
sub view_my_trusts : Local{
    my ($self, $c) = @_;

    my $res = $c->model("BcdModel")->get_trusted_ants($c);
    
    $self->compile_the_trust_net($c);

    $c->session->{sack}->{trusted_ants}  = $res->{trusted_ants};

    if ( ! defined($c->session->{sack}->{trust_limit}) ){
	$c->session->{sack}->{trust_limit} = 60;
    }

    $c->session->{hold_the_sack} = 1;
    $c->response->redirect($c->uri_for("/trust/view_my_trusts_cached"));
}

sub change_trust_limit : Local {
    my ($self, $c) = @_;

    $c->session->{sack}->{trust_limit} = $c->req->param("trust_limit");
    $c->detach("view_my_trusts");
}

#here you can access ONLY if you are an ant
sub auto : Private {
    my ( $self, $c ) = @_;

    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();


    #I shouold find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/user", "$name", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/trust/view_my_trusts", "Gestione Fiducia", $trails, $c);
    
    return 1;

}

=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
