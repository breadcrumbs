package breadcrumbs::Controller::AnPages;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;
=head1 NAME

breadcrumbs::Controller::AnPages - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    #I fill some actions for this controller...

    $self->_register_action_href
	("ap_want_to_buy_this_object", "buy_object", "Voglio comprare questo oggetto");

    $self->_register_action_href
	("ap_want_to_book_object_or_service", "book_object_or_service", 
	 "Voglio prenotare questo servizio o questo prodotto");

    bless ($self, $class);
    return $self;
}

sub buy_object : Local {
    my ($self, $c) = @_;

    $c->stash->{price_estimate} = $c->session->{sack}->{price_estimate};
    $c->stash->{ad} = $c->session->{sack}->{ad};
    $c->session->{hold_the_sack} = 1;

    my @step0 = (undef, "ap_buy_object_confirm.tt", "confirm_buy_object_on_ok");
    my @step1 = (undef, "ap_buy_object_got_token.tt");

    my @steps = (\@step0, \@step1);

    #I take the tree from the model

    $self->_make_wizard($c, \@steps, '', "Comprare un oggetto");
}

sub confirm_buy_object_on_ok : Local {
    my ($self, $c) = @_;

    $c->session->{hold_the_sack} = 1;

    #ok, I should simply issue the command to buy an object.
    my $res = $c->model("BcdModel")->buy_object
	($c, $c->session->{sack}->{ad_id},
	 $c->session->{sack}->{ad_locus});

    $c->stash->{res} = $res;

    return $self->_is_valid_res($c, $res);
}

sub book_object_or_service : Local {
    my ($self, $c) = @_;

    #ok, I should ask for a confirmation? 
    $c->stash->{ad} = $c->session->{sack}->{ad};
    $c->session->{hold_the_sack} = 1;

    my @step0 = (undef, "ap_book_object_confirm.tt", "confirm_book_object_on_ok");
    my @step1 = (undef, "ap_book_object_confirmed.tt");

    my @steps = (\@step0, \@step1);

    $self->_make_wizard($c, \@steps, '', "Prenotare un oggetto o un servizio.");

}

sub confirm_book_object_on_ok : Local {
    my ( $self, $c ) = @_;
    
    my $res = $c->model("BcdModel")->authorize_invoice
	($c, $c->session->{sack}->{ad_id}, $c->session->{sack}->{ad_locus});
    $c->session->{hold_the_sack} = 1;
    $c->stash->{ad} = $c->session->{sack}->{ad};
    return $self->_is_valid_res($c, $res);
}

sub index : Private {
    my ( $self, $c ) = @_;

    $c->stash->{template} = 'an_pages_index.tt';
}

sub _make_menu
{
    my ($self, $c) = @_;

    my $menu = $c->stash->{"menu_items"};;
    my $id = $c->session->{current_ant_nest_id};
    #Only if I am a home ant I see this items
    if ($c->check_user_roles("home_ant_$id")){

	breadcrumbs::util::MenuUtils->add_menu_entry('view_services', 'Servizi', $menu, $c);
	breadcrumbs::util::MenuUtils->add_menu_entry('view_homemade_products', 'Artigianato', $menu, $c);
	breadcrumbs::util::MenuUtils->add_menu_entry('view_used_products', 'Commercio', $menu, $c);
      }


}

sub view_services : Local {
    my ( $self, $c) = @_;

    my $res = $c->model("BcdModel")->get_services_tree_with_summary($c);
    $c->stash->{act_summary_tree} = $res->{act_summary_tree};

    $c->stash->{page_title} = "Vista di tutti i servizi per il formicaio";

    $c->session->{sack}->{ads_branch} = "services";
    $c->session->{hold_the_sack} = 1;
    delete $c->session->{sack}->{act_to_serve};
    $self->_add_trails($c);
    $c->stash->{template} = "an_pages_summary_tree.tt";
}

sub view_homemade_products : Local {
    my ( $self, $c) = @_;

    my $res = $c->model("BcdModel")->get_homemade_tree_with_summary($c);
    $c->stash->{act_summary_tree} = $res->{act_summary_tree};

    $c->stash->{page_title} = "Vista di tutti gli oggetti prodotti dal formicaio";
    $c->session->{sack}->{ads_branch} = "homemade";
    $c->session->{hold_the_sack} = 1;
    delete $c->session->{sack}->{act_to_serve};
    $self->_add_trails($c);
    $c->stash->{template} = "an_pages_summary_tree.tt";
}

sub view_used_products : Local {
    my ( $self, $c) = @_;

    my $res = $c->model("BcdModel")->get_objects_tree_with_summary($c);
    $c->stash->{act_summary_tree} = $res->{act_summary_tree};

    $c->stash->{page_title} = "Vista di tutti gli oggetti usati venduti nel formicaio";
    $c->session->{sack}->{ads_branch} = "objects";
    $c->session->{hold_the_sack} = 1;
    delete $c->session->{sack}->{act_to_serve};
    $self->_add_trails($c);
    $c->stash->{template} = "an_pages_summary_tree.tt";
}

sub _add_trails{
    my ($self, $c) = @_;

    my $trails = $c->stash->{"trails"};
    if ($c->session->{sack}->{ads_branch} eq "services"){
	breadcrumbs::util::MenuUtils->add_trail("/anpages/view_services", "Servizi", $trails, $c);
    } elsif ($c->session->{sack}->{ads_branch} eq "homemade"){
	breadcrumbs::util::MenuUtils->add_trail("/anpages/view_homemade_products", "Artigianato", $trails, $c);
    } else {
	breadcrumbs::util::MenuUtils->add_trail("/anpages/view_used_products", "Commercio", $trails, $c);
    }

    if (defined($c->session->{sack}->{act_to_serve})){
	my $id = $c->session->{sack}->{act_to_serve};
	my $name = $c->session->{sack}->{act_name};
	breadcrumbs::util::MenuUtils->add_trail
	    ("/anpages/view_act_ads_${id}?act_name=${name}", "$name", $trails, $c);
    }
}

sub viewad : Local {
    my ( $self, $c) = @_;

    $c->session->{hold_the_sack} = 1;

    my $ad_id    = $c->req->param("id");
    my $ad_locus = $c->req->param("id_locus");

    #ok, now I should be able to catch the ad from the db.
    my $res = $c->model('BcdModel')->get_ad($c, $ad_id, $ad_locus);
    $c->stash->{ad} = $res->{ad};

    #ok, if the ad type is an object, then I ask also the estimate
    if ($res->{ad}->{ad_type} eq "object"){
	$res = $c->model('BcdModel')->get_price_estimate
	    ($c, $res->{ad}->{id_user}, $res->{ad}->{p_min},
	     $res->{ad}->{pay_credit});

	$c->stash->{price_estimate} = $res;

	if ($res->{can_be_payed_now} == 1){
	    #the object is bought in one step only, so I must have the cash...
	    $self->_put_action_in_stash($c, "ap_want_to_buy_this_object");
	    $c->session->{sack}->{price_estimate} = $res;
	}

    } else {
	#the ad type is an object or a service, I get four different estimates.
	my $price = $res->{ad}->{p_min};
	my $seller = $res->{ad}->{id_user};
	my $pay_credit = $res->{ad}->{pay_credit};

	$self->_put_action_in_stash($c, "ap_want_to_book_object_or_service");

	$res = $c->model('BcdModel')->get_price_estimate
	    ($c, $seller, $price,
	     $pay_credit);

	$c->stash->{price_estimate_1} = $res;

	$res = $c->model('BcdModel')->get_price_estimate
	    ($c, $seller, $price * 2,
	     $pay_credit);

	$c->stash->{price_estimate_2} = $res;

	$res = $c->model('BcdModel')->get_price_estimate
	    ($c, $seller, $price * 5,
	     $pay_credit);

	$c->stash->{price_estimate_5} = $res;

	$res = $c->model('BcdModel')->get_price_estimate
	    ($c, $seller, $price * 10,
	     $pay_credit);

	$c->stash->{price_estimate_10} = $res;
    }

    $c->session->{sack}->{ad} = $c->stash->{ad};
    $c->session->{hold_the_sack} = 1;
    $c->session->{sack}->{ad_id}    = $ad_id;
    $c->session->{sack}->{ad_locus} = $ad_locus;

    $self->_add_trails($c);
    $c->stash->{template} = "an_pages_view_ad.tt";

}

sub view_act_ads : LocalRegex('^view_act_ads_(\d+)$') {
    my ( $self, $c) = @_;

    #I must process the sorting
    $self->_process_table_sorting_orders($c);

    $c->session->{hold_the_sack} = 1;

    my $act_to_serve = $c->req->captures->[0];
    my $act_name = $c->req->param("act_name");

    if (defined($act_name)){
	$c->session->{sack}->{act_name} = $act_name;
    }

    $c->session->{sack}->{act_to_serve} = $act_to_serve;

    $self->_add_trails($c);

    #ok, now let's ask to the model the ads for this activity
    my $res = $c->model("BcdModel")->get_ads_for_this_activity($c, $act_to_serve);

    #ok, now I put the ads in the stash
    $c->stash->{ads} = $res->{ads};

    $c->stash->{template} = "an_pages_view_ads_list.tt";
    
}

sub auto : Private {
    my ( $self, $c) = @_;

    
    my $id = $c->session->{current_ant_nest_id};

    my $trails = $c->stash->{"trails"};
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$id", "Formicaio $id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/anpages", "Bricio pagine", $trails, $c);

    return 1; #You can continue
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
