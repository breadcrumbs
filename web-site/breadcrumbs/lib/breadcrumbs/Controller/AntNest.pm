package breadcrumbs::Controller::AntNest;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
#use base 'Catalyst::Controller';
use breadcrumbs::util::MenuUtils;
use NEXT;
use breadcrumbs::Widgets::TokenWidget;
use breadcrumbs::Widgets::TrustWidget;
use breadcrumbs::Dialogs::ConfirmTutorsDialog;
use breadcrumbs::Dialogs::MessageDialog;
use Data::Dumper;

=head1 NAME

breadcrumbs::Controller::AntNest - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    #I fill some actions for this controller...

    $self->_register_action_href
	("AntNest_register_ant_nest", "register_ant_nest", "Voglio iscrivermi a questo formicaio ma non ho inviti.");

    $self->_register_action_href
	("AntNest_have_invite", "invite_to_ant_nest", "Ho ricevuto un invito per questo formicaio");

    $self->_register_action_href
	("AntNest_write_to_boss", "write_to_boss", "Voglio mandare un messaggio al capo formicaio");

    $self->_register_action_href
	("AntNest_guest_ant", "register_ant_nest", "Azione per le formiche ospiti");

    $self->_register_action_href
	("AntNest_home_ant", "register_ant_nest", "Azione per le formiche di casa");

    bless ($self, $class);
    return $self;
}

sub register_ant_nest : Local {

    my ( $self, $c ) = @_;
    $c->stash->{template} = "no_invite_to_this_ant_nest.tt";
}

sub view_this_ant_nest : Private {
    my ( $self, $c ) = @_;

    $c->session->{current_ant_nest_id} = $c->stash->{ant_nest}->{id};
    $self->_view_ant_nest($c);
}

sub _view_ant_nest : Private {
    my ( $self, $c ) = @_;

    my $id = $c->session->{current_ant_nest_id};

    if (Bcd::Data::PostCode::is_this_a_test_post_code($id)){
	$c->stash->{test_ant_nest} = 1;
    } else {
	$c->stash->{test_ant_nest} = 0;
    }

    my $res = $c->model('BcdModel')->get_ant_nest_detail($c->session->{current_ant_nest_id});
    $c->stash->{ant_nest} = $res->{ant_nest};

    $res = $c->model('BcdModel')->get_ant_nest_hq($c->session->{current_ant_nest_id});
    $c->stash->{hq} = $res->{hq};

    $c->stash->{template} = 'ant_nest_view.tt';

    if ( $c->user_exists()){
	#ok, I should see if the current user is in this ant nest

	#if ($c->check_user_roles("guest_ant_$id")){
	    #I am a "guest"
	    #$self->_put_action_in_stash($c, "AntNest_guest_ant");
	#} else {
	    #ok, the user is seeing his/her ant nest
	    #$self->_put_action_in_stash($c, "AntNest_home_ant");
	#}
    } else {
	$self->_put_action_in_stash($c, "AntNest_write_to_boss");
	$self->_put_action_in_stash($c, "AntNest_register_ant_nest");
	$self->_put_action_in_stash($c, "AntNest_have_invite");
    }

    $self->_put_action_in_stash($c, "AntNestsController_search_ant_nests", "Cerca altri formicai: ");
}

sub view_ant_nest : LocalRegex('^view_(\d+)$') {
   my ( $self, $c ) = @_;

   $c->session->{current_ant_nest_id} = $c->request->captures->[0];



   $self->_view_ant_nest($c);
}

#ok, I have a token.
sub invite_to_ant_nest : Local{
    my ($self, $c) = @_;

    my $w = breadcrumbs::Widgets::TokenWidget->make_token_widget($c);
    $w->action($c->uri_for('invite_to_ant_nest_do'));

    # Write form to stash variable for use in template
    $c->stash->{widget_result} = $w->result;
    $c->stash->{template} = "invite_to_ant_nest.tt";
}

sub write_to_boss : Local {
    my ($self, $c) = @_;

    #I should simply process a dialog
    my $dlg = breadcrumbs::Dialogs::MessageDialog->new($c);

    $self->_process_this_dialog
	($c,
	 $dlg, 
	 "send_message_to_boss.tt", 
	 'write_to_boss_on_ok'); 

    $c->detach();

}

sub write_to_boss_on_ok : Local {
    my ($self, $c) = @_;

    my $subject = $c->req->param("subject");
    my $comment = $c->req->param("comment_or_message");
    my $id_ant_nest = $c->session->{current_ant_nest_id};

    #ok, I can issue the command
    my $res = $c->model("BcdModel")->write_to_boss
	($id_ant_nest, $subject, $comment);

    $c->res->redirect($c->uri_for('/antnest/view_') . $id_ant_nest);
    return $res->{exit_code};
}

sub invite_to_ant_nest_do : Local {
    my ($self, $c) = @_;

    my $w = breadcrumbs::Widgets::TokenWidget->make_token_widget($c);
    $w->action($c->uri_for('invite_to_ant_nest_do'));


    my $result = $w->process($c->req);
    $c->stash->{widget_result} = $result;


    if ($result->has_errors) {
	$c->stash->{status_msg} = 'Non sembra un gettone valido.';
    } else {

	my $token = breadcrumbs::Widgets::TokenWidget->get_token($result);

	#I should get the tutor details from the model.
	# If the user has two tutors, then he/she is able to confirm them,
	# otherwise can only see the first tutor.

	my $res = $c->model('BcdModel')->get_tutors_details($token);

	if ($res->{exit_code} == 0){
	    $c->flash->{status_msg} = "Ok";

	    $c->stash->{res} = $res;
	    $c->stash->{template} = "tutors_details.tt";

	    if ($res->{tutor_2_existing} == 1){
		#ok, I can make the widget to have the trust...

# 		$c->session->{first_tutor_name} = $res->{tutor_first}->{first_name};
# 		#$c->session->{first_tutor_name} = "cippo";
# 		$c->session->{second_tutor_name} = $res->{tutor_second}->{first_name};
# 		$c->session->{res} = $res;

# 		my $w = $self->make_two_tutors_trust_widget($c);
# 		$w->action($c->uri_for('confirm_my_tutors_do'));
# 		$c->stash->{widget_result} = $w->result;

		$c->session->{token} = $token;
		$c->session->{res} = $res;

		my $dlg = breadcrumbs::Dialogs::ConfirmTutorsDialog->new
		    ($c,
		     $res->{tutor_first}->{first_name},
		     $res->{tutor_second}->{first_name});

		$self->_process_this_dialog
		    ($c,
		     $dlg, 
		     "tutors_details.tt", 
		     '/antnest/confirm_my_tutors_on_ok'); 

		$c->detach();
	    }

	    return;
	} else {
	    my $msg = $breadcrumbs::ServerErrors::error_messages{$res->{exit_code}};
	    $c->stash->{error_msg} = "Errore $res->{exit_code}: $msg";
	}
    }

    # Set the template
    $c->stash->{template} = 'invite_to_ant_nest.tt';
}

sub confirm_my_tutors_on_ok : Private {
    my ($self, $c) = @_;

    my $result = $c->stash->{result};


    my $t1 = breadcrumbs::Dialogs::ConfirmTutorsDialog->get_first_trust($result);
    my $t2 = breadcrumbs::Dialogs::ConfirmTutorsDialog->get_second_trust($result);
    my $token = $c->session->{token};

    my $res = $c->model('BcdModel')->confirm_my_tutors($token, $t1, $t2);
    if ($res->{exit_code} == 0){

	my $nick = $res->{new_user}->{nick};
	$c->stash->{status_msg} = "Ok $nick. Benvenuto nel formicaio. Ti puoi collegare, ora...";
	delete ($c->session->{res});
	delete ($c->session->{token});
	$c->detach('/authentication/login');

    } else {

	my $msg = $breadcrumbs::ServerErrors::error_messages{$res->{exit_code}};
	$c->stash->{error_msg} = "Errore $res->{exit_code}: $msg";
	$c->detach('invite_to_ant_nest');

    }
    

}

sub make_two_tutors_trust_widget{
    my ($self, $c) = @_;

    my $w = $c->widget('trust_to_tutors')->method('post');
    my $fs = $w->element('Fieldset', 'trust')->legend('Ho bisogno di due fiducie:');

    my $tut1 = $c->session->{first_tutor_name};
    my $tut2 = $c->session->{second_tutor_name};

    my $cap1 = "Fiducia a $tut1";
    my $cap2 = "Fiducia a $tut2";
    
#     my $t1 = breadcrumbs::Widgets::TrustWidget->make_trust_widget($c, $cap1, "trust_first");
#     my $t2 = breadcrumbs::Widgets::TrustWidget->make_trust_widget($c, $cap2, "trust_second");

#     $w->embed($fs, $t1);
#     $w->embed($fs, $t2);

    breadcrumbs::Widgets::TrustWidget->make_simple_control($w, $fs, $cap1, "trust_first");
    breadcrumbs::Widgets::TrustWidget->make_simple_control($w, $fs, $cap2, "trust_second");

    $fs->element('Submit', 'trust_submit' )->value('Ok');

    return $w;
}

sub confirm_my_tutors_do : Local{
   my ($self, $c) = @_;

   my $w = $self->make_two_tutors_trust_widget($c);
   $w->action($c->uri_for('confirm_my_tutors_do'));

   my $result = $w->process($c->req);
   $c->stash->{widget_result} = $result;

   if ($result->has_errors) {
       $c->stash->{status_msg} = 'Ci sono degli errori nella fiducia';
       $c->stash->{res} = $c->session->{res};
   } else {

       #ok, I can trust my tutors.
       $c->stash->{status_msg} = "Ok. Benvenuto nel formicaio. Ti puoi collegare, ora..";

       delete ($c->session->{tutor_first_name});
       delete ($c->session->{tutor_second_name});
       delete ($c->session->{res});

       $c->detach('/authentication/login');

   }

   $c->stash->{template} = "tutors_details.tt";

}

sub _make_menu
{
    my ($self, $c) = @_;

    my $menu = $c->stash->{"menu_items"};

    my $id = $c->session->{current_ant_nest_id};

    #Only if I am a home ant I see this items
    if ($c->check_user_roles("home_ant_$id")){

	breadcrumbs::util::MenuUtils->add_menu_entry('/census/view_ant_nest_census', 'Anagrafica', $menu, $c);
	breadcrumbs::util::MenuUtils->add_menu_entry('/anpoints', 'Bricio Punti', $menu, $c);
	breadcrumbs::util::MenuUtils->add_menu_entry('/anpages', 'Bricio Pagine', $menu, $c);
      }


}

sub auto : Private {
    my ( $self, $c) = @_;

    my $id = $c->request->captures->[0];

    if (!defined($id) or $id eq "") {
	$id = $c->session->{current_ant_nest_id};
    }

    my $trails = $c->stash->{"trails"};
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("view_$id", "Formicaio $id", $trails, $c);

    return 1; #You can continue
}



=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
