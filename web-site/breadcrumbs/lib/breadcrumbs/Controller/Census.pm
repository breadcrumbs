package breadcrumbs::Controller::Census;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';

=head1 NAME

breadcrumbs::Controller::Census - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

# sub index : Private {
#     my ( $self, $c ) = @_;

#     $c->response->body('Matched breadcrumbs::Controller::Census in Census.');
# }

sub view_ant_nest_census : Local {
    my ( $self, $c ) = @_;

    my $id = $c->session->{current_ant_nest_id};
    if (!$id){
	$c->error("There is not current ant nest");
	return;
    }

    if ($c->check_user_roles(("boss", "home_ant_$id"))){
	$c->stash->{template} = "view_census_for_boss.tt";
    } else {
	#$c->response->body('o non sei un boss o sei un boss di un altro formicaio');
	$c->stash->{template} = "view_census_for_others.tt";
    }
}

sub get_informations_for_this_user : Private {

    my ( $self, $c ) = @_;

    my $user = "lino prova";
    $c->response->body("ti do le informazioni per $user, ma tu sei $c->user->{id}.");
    
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
