package breadcrumbs::Controller::CashAntNestEuro;

use strict;
use warnings;

use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;

use breadcrumbs::Dialogs::SingleTokenDialog;

sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);

#     $self->_register_action_href
# 	("cash_an_euro_deposit_online", "online_deposit", "Una formica vuole depositare sul suo conto");

#     $self->_register_action_href
# 	("cash_an_euro_withdrawal_online", "online_withdrawal", "Una formica vuole ritirare dal suo conto");

    $self->_register_action_href
 	("cash_an_euro_ack_offline", "ack_offline", 
"Ok, accetto queste prenotazioni: spediscimi il rapporto completo");


    bless ($self, $class);
    return $self;
}

=head1 NAME

breadcrumbs::Controller::CashAntNestEuro - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub ack_offline : Local {
    my ( $self, $c ) = @_;

    my $res = $c->model('BcdModel')->treasurer_offline_acknowledged($c);

    if ($self->_is_valid_res($c, $res)){
	$c->stash->{status_msg} = "ok";
    }

    $c->forward(qw/cashantnesteuro index/);

}

sub index : Private {
    my ( $self, $c ) = @_;

#     $self->_put_action_in_stash($c, "cash_an_euro_deposit_online");
#     $self->_put_action_in_stash($c, "cash_an_euro_withdrawal_online");

    my $res = $c->model('BcdModel')->get_ant_nest_cash_euro_summary($c);
    $c->stash->{account_summary} = $res->{account_summary};

    $res = $c->model('BcdModel')->get_ant_nest_bookings_summary($c);
    $c->stash->{total_withdrawals}   = $res->{total_withdrawals};
    $c->stash->{total_deposits}      = $res->{total_deposits};
    $c->stash->{withdrawals_summary} = $res->{withdrawals_summary};
    $c->stash->{deposits_summary}    = $res->{deposits_summary};
    $c->stash->{online}              = $res->{online};
    $c->stash->{tot_offline_withdrawals} = $res->{tot_offline_withdrawals};
    $c->stash->{offline_withdrawals} = $res->{offline_withdrawals};

    $c->session->{sack}->{bookings_summary} = $res;
    $c->session->{hold_the_sack} = 1;
    
    $c->stash->{template} = "cash_ant_nest_euro.tt";
}

sub online_deposit : Local {
    my ($self, $c) = @_;

    my $dlg0  = breadcrumbs::Dialogs::SingleTokenDialog->new($c, 1);

    #ok, I should make a wizard!
    my @step0 = ($dlg0, "get_booking_token.tt", 'deposit_booking_token_on_ok');
    my @step1 = (undef, "confirm_deposit_import.tt", "treasurer_has_the_money_on_ok");
    my @step2 = (undef, "finished_deposit.tt");

    my @steps = (\@step0, \@step1, \@step2);

    $self->_make_wizard($c, \@steps, '', "Deposito sul conto.");
    return 1;

}

sub treasurer_has_the_money_on_ok : Local {
    my ($self, $c) = @_;

    #ok, the treasurer has the money, I hope.

    my $res = $c->model('BcdModel')->treasurer_acknowledged($c, $c->session->{sack}->{booking_details}->{id});
    $c->session->{hold_the_sack} = 1;

    if ($self->_is_valid_res($c, $res)){
	return 1;
    } else {
	return 0; 
    }

    return 1;
    

}

sub confirm_withdrawal : Local {
    my ($self, $c) = @_;

    my $booking_token = $c->req->param("token");

    my $res = $c->model('BcdModel')->get_withdrawal_details($c, $booking_token);

    if ($self->_is_valid_res($c, $res)){


	my $dlg = breadcrumbs::Dialogs::SecretTokenDialog->new($c, 1, $res->{booking_details}->{full_token});

	my @step0 = (undef, "withdrawal_summary.tt", "treasurer_has_the_correct_full_token_on_ok");
	my @step1 = ($dlg,  "withdrawal_secret.tt",  "treasurer_has_secret_code_on_ok");
	my @step2 = (undef, "finished_withdrawal.tt");

	my @steps = (\@step0, \@step1, \@step2);

	$self->_make_wizard($c, \@steps, '', "Ritiro dal conto.");

	#ok, I put the tokens in the flash
	$c->stash->{booking_details} = $res->{booking_details};
	$c->stash->{online} = $c->session->{sack}->{bookings_summary}->{online};
	$c->session->{sack}->{booking_details} = $res->{booking_details};
	$c->session->{hold_the_sack} = 1;

	return 1;
	
    } else {
	$c->flash->{status_msg} = "ho avuto un problema";
	$c->res->redirect($c->uri_for(''));
	return 0;
    }
}

sub treasurer_has_the_correct_full_token_on_ok : Local {
    my ($self, $c) = @_;
    $c->session->{hold_the_sack} = 1;
    $c->stash->{booking_details} = $c->session->{sack}->{booking_details};
    $c->stash->{online} = $c->session->{sack}->{bookings_summary}->{online};
    return 1;
}

sub treasurer_has_secret_code_on_ok : Local {
    my ($self, $c) = @_;
    $c->session->{hold_the_sack} = 1;

    $c->stash->{online} = $c->session->{sack}->{bookings_summary}->{online};
    $c->stash->{booking_details} = $c->session->{sack}->{booking_details};

    #ok, I should get the secret token from the dialog and do the withdrawal...
    my $result = $c->stash->{result};
    my $secret = breadcrumbs::Dialogs::SecretTokenDialog->get_secret_token($result);
    my $res = $c->model('BcdModel')->claim_withdrawal($c, $c->session->{sack}->{booking_details}->{id}, $secret);

    if ($self->_is_valid_res($c, $res)){

	$c->stash->{amount_withdrawn} = $res->{amount_withdrawn};
	$c->stash->{new_total}        = $res->{new_total};
	
    } else {
	return 0; 
    }

    return 1;
}

sub confirm_deposit : Local {
    
    my ($self, $c) = @_;

    my $booking_token = $c->req->param("token");

    my $res = $c->model('BcdModel')->get_deposit_details($c, $booking_token);

    if ($self->_is_valid_res($c, $res)){

	my @step0 = (undef, "confirm_deposit_import.tt", "treasurer_has_the_money_on_ok");
	my @step1 = (undef, "finished_deposit.tt");

	my @steps = (\@step0, \@step1);

	$self->_make_wizard($c, \@steps, '', "Deposito sul conto.");

	#ok, I put the tokens in the flash
	$c->stash->{booking_details} = $res->{booking_details};
	$c->session->{sack}->{booking_details} = $res->{booking_details};
	$c->session->{hold_the_sack} = 1;

	return 1;
	
    } else {
	$c->flash->{status_msg} = "ho avuto un problema";
	$c->res->redirect($c->uri_for(''));
	return 0;
    }

}

sub deposit_booking_token_on_ok : Local {
    my ($self, $c) = @_;
    
    my $result = $c->stash->{result};

    #first of all I should get the token from the dialog
    my $booking_token = breadcrumbs::Dialogs::SingleTokenDialog->get_token($result);

    #ok, I can have the details of this booking
    my $res = $c->model('BcdModel')->get_deposit_details($c, $booking_token);

    if ($self->_is_valid_res($c, $res)){

	#ok, I put the tokens in the flash
	$c->stash->{booking_details} = $res->{booking_details};
	$c->session->{sack}->{booking_details} = $res->{booking_details};
	$c->session->{hold_the_sack} = 1;
	
    } else {
	return 0; 
    }

    return 1;
}

sub online_withdrawal : Local {
    my ($self, $c) = @_;

    $c->res->body("cippo");
}

sub _make_menu{
    my ( $self, $c ) = @_;

    my $menu = $c->stash->{"menu_items"};;

    breadcrumbs::util::MenuUtils->add_menu_entry('deposits_withdrawals', 'Depositi e Ritiri', $menu, $c);
}

sub deposits_withdrawals : Local {
    my ($self, $c) = @_;

    #I have the tables, which can be sorted...
    $self->_process_table_sorting_orders($c);
    
    $c->stash->{template} = "ant_nest_deposits_withdrawals.tt";

    $c->stash->{total_withdrawals}   = $c->session->{sack}->{bookings_summary}->{total_withdrawals};
    $c->stash->{total_deposits}      = $c->session->{sack}->{bookings_summary}->{total_deposits};
    $c->stash->{withdrawals_summary} = $c->session->{sack}->{bookings_summary}->{withdrawals_summary};
    $c->stash->{deposits_summary}    = $c->session->{sack}->{bookings_summary}->{deposits_summary};
    $c->stash->{online}              = $c->session->{sack}->{bookings_summary}->{online};
    $c->stash->{tot_offline_withdrawals} = $c->session->{sack}->{bookings_summary}->{tot_offline_withdrawals};
    $c->stash->{offline_withdrawals} = $c->session->{sack}->{bookings_summary}->{offline_withdrawals};

    my $num_pendings = scalar(@{$c->stash->{deposits_summary}}) + scalar(@{$c->stash->{withdrawals_summary}});
    if ($c->stash->{online} == 0 and $num_pendings != 0){
	$self->_put_action_in_stash($c, "cash_an_euro_ack_offline");
    }

    $c->session->{hold_the_sack} = 1;


}


#I should check the permission of this ant...
sub auto : Private {
    my ( $self, $c ) = @_;

    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    if (! $c->check_user_roles('treasurer')){
	$c->flash->{status_msg} = "Bisogna essere dei tesorieri per entrare qui.";
	$c->res->redirect($c->uri_for(qw(/user)));
	return 0;
    }

    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();


    #I shouold find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/user", "$name", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/cashantnesteuro", "Gestione cassa", $trails, $c);

    return 1;

}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
