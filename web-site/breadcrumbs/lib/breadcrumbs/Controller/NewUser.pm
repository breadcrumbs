package breadcrumbs::Controller::NewUser;

use strict;
use warnings;

use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;

use Bcd::Constants::NewAntNestsConstants;
use Bcd::Constants::FoundersConstants;

use breadcrumbs::Dialogs::PersonalUserDataDialog;
use breadcrumbs::Dialogs::FounderTrustsDialog;
use breadcrumbs::Dialogs::CandidatesFlagsDialog;
use breadcrumbs::Dialogs::FounderVoteDialog;
use breadcrumbs::Dialogs::PublicSiteDialog;
use breadcrumbs::Dialogs::PresenceInPublicSiteDialog;
use breadcrumbs::Dialogs::OnlineOfflineDepositsDialog;
use breadcrumbs::Dialogs::ChoiceOfDepositsSiteDialog;
use breadcrumbs::Dialogs::SubjectMessageDialog;

=head1 NAME

breadcrumbs::Controller::NewUser - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    #I fill some actions for this controller...

    $self->_register_action_href
	("Founder_abort", "abort_founding", "Voglio annullare la mia disponibilità a fondare questo formicaio");

    $self->_register_action_href
	("Founder_send_message", "send_message_to_founders", "Voglio spedire un messaggio a tutti gli altri fondatori");

    bless ($self, $class);
    return $self;
}

sub abort_founding : Local {
    my ($self, $c) = @_;

    $c->stash->{template} = "abort_founding.tt";
}

sub send_message_to_founders : Local {
    my ($self, $c) = @_;

    my $dlg = breadcrumbs::Dialogs::SubjectMessageDialog->new($c, $c->user->name);

    $self->_process_this_dialog
	($c,
	 $dlg, 
	 "send_message_to_founders.tt", 
	 '/newuser/send_message_on_ok'); 

    $c->detach();
}

sub send_message_on_ok : Local {
    my ($self, $c) = @_;

    my $res = $c->model('BcdModel')->send_message_to_founders
	($c, $c->req->param("subject"), $c->req->param("message"));

    $c->stash->{res} = $res;


    if ($self->_is_valid_res($c, $res)){
	$c->flash->{status_msg} = "messaggio spedito con successo";
	$c->response->redirect($c->uri_for("/newuser"));
	return 0;
    } else {
	return $res->{exit_code}; 
    }
}

sub index : Private {
    my ( $self, $c ) = @_;

    $self->_put_action_in_stash($c, "Founder_abort");
    $self->_put_action_in_stash($c, "Founder_send_message");

    my $state = $c->user->my_profile()->{id_status};

    #I differentiate among the statuses
    if ( $state == Bcd::Constants::FoundersConstants::USER_BOOKED_CONFIRMED){
	$c->stash->{subtemplate} = "user_booked_confirmed.tt";
    } elsif ( $state == Bcd::Constants::FoundersConstants::WAITING_FOR_PERSONAL_DATA){

	#I should process the dialog...


	my $dlg = breadcrumbs::Dialogs::PersonalUserDataDialog->new($c);

	$self->_process_this_dialog
	    ($c,
	     $dlg, 
	     "waiting_for_personal_data.tt", 
	     '/newuser/update_personal_data_on_ok'); 

	$c->detach();

    } elsif ($state == Bcd::Constants::FoundersConstants::PERSONAL_DATA_IMMITTED) {
	$c->stash->{subtemplate} = "founder_with_personal_data.tt";
    } elsif ($state == Bcd::Constants::FoundersConstants::TRUST_NOT_PRESENT){

	#I should get the founders summary
	my $res = $c->model("BcdModel")->get_founders_summary($c);
	#$c->session->{sack}->{founders_summary} = $res->{founders_summary};;
	#$c->session->{hold_the_sack}         = 1

	my $dlg = breadcrumbs::Dialogs::FounderTrustsDialog->new($c, $res->{founders_summary});

	$self->_process_this_dialog
	    ($c,
	     $dlg, 
	     "founder_get_trust.tt", 
	     '/newuser/founder_get_trust_on_ok'); 

	$c->detach();
    } elsif ($state == Bcd::Constants::FoundersConstants::TRUST_PRESENT){
	$c->stash->{subtemplate} = "founder_with_trusts_given.tt";
    } elsif ($state == Bcd::Constants::FoundersConstants::NO_CHOICE_OF_CANDIDATE){

	my $dlg = breadcrumbs::Dialogs::CandidatesFlagsDialog->new($c);

	$self->_process_this_dialog
	    ($c,
	     $dlg, 
	     "founder_get_candidates_flags.tt", 
	     '/newuser/founder_get_candidates_flag_on_ok'); 

	$c->detach();
    } elsif ($state == Bcd::Constants::FoundersConstants::CHOICE_MADE){
	$c->stash->{subtemplate} = "founder_with_candidates_choices_made.tt";
    } elsif ($state == Bcd::Constants::FoundersConstants::NO_VOTED_YET){

	#I should get the candidates summary
	my $res = $c->model("BcdModel")->get_candidates_summary($c);

	$c->stash->{dumper_res} = $res;

	my $dlg = breadcrumbs::Dialogs::FounderVoteDialog->new
	    ($c, $res->{bosses_candidates}, $res->{treasurers_candidates});

	$self->_process_this_dialog
	    ($c,
	     $dlg, 
	     "founder_get_vote.tt", 
	     '/newuser/founder_get_vote_on_ok'); 

	$c->detach();


    } elsif ($state == Bcd::Constants::FoundersConstants::VOTED){
	$c->stash->{subtemplate} = "founder_with_vote.tt";
    } elsif ($state == Bcd::Constants::FoundersConstants::WAITING_TO_BE_CREATED){
	$c->stash->{subtemplate} = "founder_waiting_to_be_created.tt";
    } elsif ($state == Bcd::Constants::FoundersConstants::BOSS_SET_UP_TO_DO){

	my $dlg0 = breadcrumbs::Dialogs::PublicSiteDialog->new($c, 1);
	my $dlg1 = breadcrumbs::Dialogs::PresenceInPublicSiteDialog->new($c, 1);

	#let's prepare the steps.
	my @step0 = ($dlg0, "initial_boss_module.tt", 'public_site_on_ok');
	#the last action is the finish action...
	my @step1 = ($dlg1, "presence_boss_in_public_site.tt", "presence_public_on_ok");
	my @step2 = (undef, "finished_boss_setup.tt");

	my @steps = (\@step0, \@step1, \@step2);

	$self->_make_wizard($c, \@steps, '', "Setup iniziale del capo.");
	return 1;

    } elsif ($state == Bcd::Constants::FoundersConstants::WAITING_BOSS_SET_UP){
	$c->stash->{subtemplate} = "waiting_boss_setup.tt";
    } elsif ($state == Bcd::Constants::FoundersConstants::SET_UP_DONE){
	$c->stash->{subtemplate} = "set_up_done.tt";
    } elsif ($state == Bcd::Constants::FoundersConstants::TREASURER_SET_UP_TO_DO){

	my $dlg0 = breadcrumbs::Dialogs::OnlineOfflineDepositsDialog->new($c, 1);
	my $dlg1 = breadcrumbs::Dialogs::ChoiceOfDepositsSiteDialog->new($c, 1);
	my $dlg2 = breadcrumbs::Dialogs::PublicSiteDialog->new($c, 1);
	my $dlg3 = breadcrumbs::Dialogs::PresenceInPublicSiteDialog->new($c, 1);

	#let's prepare the steps.
	my @step0 = ($dlg0, "online_offline_choice.tt", 'online_offline_on_ok');
	my @step1 = ($dlg1, "deposits_site_choice.tt", 'deposits_site_choice_on_ok');
	my @step2 = ($dlg2, "create_public_site.tt", "public_site_on_ok");
	my @step3 = ($dlg3, "presence_treasurer_in_public_site.tt", "presence_treasurer_on_ok");
	my @step4 = (undef, "finished_treasurer_setup.tt");

	my @steps = (\@step0, \@step1, \@step2, \@step3, \@step4);

	$self->_make_wizard($c, \@steps, '', "Setup iniziale del tesoriere.");
	return 1;
    }

    $c->stash->{template} = "founder_details.tt"
}

sub online_offline_on_ok : Local {
    my ($self, $c) = @_;

    #ok, I should simply now put the configuration in the ant nest...
    #the default is offline, so if it is online I should change it.

    #ok, now I should get the hq from the ant nest, because the
    #treasurer should be able to judge where to collect money.

    if ($c->req->param("online") eq "on"){
	$c->session->{steps}->{online_deposits} = '1';
    } else {
	$c->session->{steps}->{online_deposits} = '0';
    }

    my $res = $c->model("BcdModel")->get_ant_nest_hq($c->session->{current_ant_nest_id});
    $c->session->{steps}->{hq} = $res->{hq};

    return 1;
}

sub deposits_site_choice_on_ok : Local {
    my ($self, $c) = @_;

    if ($c->req->param("site_deposits") eq "hq"){
	$c->session->{steps}->{deposits_in_hq} = '1';

	#they are not important
	$c->session->{steps}->{name}            = 'foo';
	$c->session->{steps}->{location}        = 'foo';
	$c->session->{steps}->{directions}      = 'foo';
	$c->session->{steps}->{time_restricted} = 'foo';
	$c->session->{steps}->{opening_hours}   = 'foo';

	return 2; #I advance two steps
    } else {
	$c->session->{steps}->{deposits_in_hq} = '0';
	return 1;
    }

}

sub public_site_on_ok : Local {
    my ($self, $c) = @_;

    #I should simply store the user's options.
    $c->session->{steps}->{name}          = $c->req->param("name");	   
    $c->session->{steps}->{location}  	  = $c->req->param("location");
    $c->session->{steps}->{directions}    = $c->req->param("directions");

    if (defined($c->req->param("time_restricted"))){
	$c->session->{steps}->{time_restricted} = '1';
    } else {
	$c->session->{steps}->{time_restricted} = '0';
    }

    $c->session->{steps}->{opening_hours} = $c->req->param("opening_hours");

    return 1;
}

sub presence_treasurer_on_ok : Local {

    my ($self, $c) = @_;

    #ok, I should proceed giving the command to the server.

    my $on_request = '0';
    if (defined($c->req->param("request"))){
	$on_request = '1';
    }

    
    my $res = $c->model("BcdModel")
	->initial_treasurer_set_up
	(
	 $c,
	 $c->session->{steps}->{online_deposits},
	 $c->session->{steps}->{deposits_in_hq},
	 $c->session->{steps}->{name},
	 $c->session->{steps}->{location},
	 $c->session->{steps}->{directions},
	 $c->session->{steps}->{time_restricted},
	 $c->session->{steps}->{opening_hours},
	 $on_request,
	 $c->req->param("available_hours"));

    if ( $self->_is_valid_res($c, $res)){
	return 1;
    } else {
	return 0;
    }

}

sub presence_public_on_ok : Local {
    my ($self, $c) = @_;

    #ok, I should proceed giving the command to the server.

    my $on_request = '0';
    if (defined($c->req->param("request"))){
	$on_request = '1';
    }

    
    my $res = $c->model("BcdModel")
	->create_founder_hq
	(
	 $c,
	 $c->session->{steps}->{name},
	 $c->session->{steps}->{location},
	 $c->session->{steps}->{directions},
	 $c->session->{steps}->{time_restricted},
	 $c->session->{steps}->{opening_hours},
	 $on_request,
	 $c->req->param("available_hours"));

    if ( $self->_is_valid_res($c, $res)){
	return 1;
    } else {
	return 0;
    }

}

sub founder_get_vote_on_ok : Local {
    my ($self, $c) = @_;

    my $boss_vote      = defined ($c->req->param("boss"))      ? 
	                          $c->req->param("boss")       : "NULL";

    my $treasurer_vote = defined ($c->req->param("treasurer")) ? 
	                          $c->req->param("treasurer")  : "NULL";

    my $res = $c->model("BcdModel")->add_founder_vote($c, $boss_vote, $treasurer_vote);

    if ($res->{exit_code} == Bcd::Errors::ErrorCodes::BEC_OK){
	$c->flash->{status_msg} = "Ok, hai votato!";
	$c->user->outdate_profile(); #the profile is changed
    } else {
	return $res->{exit_code};
    }

    $c->response->redirect($c->uri_for("/newuser"));
    return 0;
    
}

sub founder_get_candidates_flag_on_ok : Local {
    my ($self, $c) = @_;

    my $flag = $c->req->param("flag");

    my ($boss_flag, $treasurer_flag);
    $boss_flag = $treasurer_flag = '0';

    #I have three cases
    if ($flag eq "none"){
	#this ant does not want to be anything
    } elsif ($flag eq "b"){
	$boss_flag = '1';
    } else {
	$treasurer_flag = '1';
    }

    my $res = $c->model("BcdModel")->update_founder_flags($c, $boss_flag, $treasurer_flag);

    if ($res->{exit_code} == Bcd::Errors::ErrorCodes::BEC_OK){
	$c->flash->{status_msg} = "Ok, scelta candidatura accettata";
	$c->user->outdate_profile(); #the profile is changed
    } else {
	return $res->{exit_code};
    }

    $c->response->redirect($c->uri_for("/newuser"));
    return 0;
    
}

sub founder_get_trust_on_ok : Local {
    my ($self, $c) = @_;

    my $result = $c->stash->{result};

    #ok, I should get the parameters and build an array
    my @params;

    for(0..Bcd::Constants::NewAntNestsConstants::ANT_NEST_INITIAL_SIZE - 2){
	my $other_id = $c->req->param("id_user_$_");
	my $trust    = breadcrumbs::Dialogs::FounderTrustsDialog->get_trust($result, $_);

	push(@params, $other_id);
	push(@params, $trust);
    }

    #let's issue the command to the daemon
    my $res = $c->model("BcdModel")->create_founder_trusts($c, @params);

    if ($res->{exit_code} == Bcd::Errors::ErrorCodes::BEC_OK){
	$c->flash->{status_msg} = "Ok, fiducia immessa";
	$c->user->outdate_profile(); #the profile is changed
    } else {
	return $res->{exit_code};
    }

    $c->response->redirect($c->uri_for("/newuser"));
    return 0;

}

sub update_personal_data_on_ok : Local {
    my ( $self, $c) = @_;

    my $result = $c->stash->{result};
    my $date = breadcrumbs::Dialogs::PersonalUserDataDialog->get_birthdate($result);

    #ok, then I should be able to have all the others parameters from the request.

    #ok, I issue the command to the daemon
    my $res = $c->model("BcdModel")->update_founder_personal_data
	(
	 $c,
	 $c->req->param("first_name"),
	 $c->req->param("last_name"),
	 $c->req->param("address"),
	 $c->req->param("home_phone"),
	 $c->req->param("mobile_phone"),
	 $c->req->param("sex"),
	 $date,
	 #the mail is NOT changed, I put the same as before
	 $c->user->my_profile()->{email},
	 $c->req->param("document_number"));

    if ($res->{exit_code} == Bcd::Errors::ErrorCodes::BEC_OK){
	$c->flash->{status_msg} = "Ok, dati cambiati";
	$c->user->outdate_profile(); #the profile is changed
    } else {
	return $res->{exit_code};
    }

    $c->response->redirect($c->uri_for("/newuser"));
    return 0;
	

}

sub auto : Private {
    my ( $self, $c) = @_;


    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    my $trails = $c->stash->{trails};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();

    #I should find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail
	("/antnestscontroller", "I formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail
	("/newantnests/view_building_ant_nests", "formicai in costruzione", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail
	("/newantnest/view_$an_id", "formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail
	("/newuser", "$name", $trails, $c);

    return 1; #You can continue
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
