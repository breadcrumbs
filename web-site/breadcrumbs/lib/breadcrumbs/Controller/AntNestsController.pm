package breadcrumbs::Controller::AntNestsController;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';

use breadcrumbs::Dialogs::WantToCreateAntNestDialog;

use Bcd::Data::PostCode;

use NEXT;

=head1 NAME

breadcrumbs::Controller::AntNestsController - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

This should simply provide some mechanism to control the ant nests...
bla bla bla.

=head1 METHODS

=cut

sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    #I fill some actions for this controller...
#     my $html = "search_ant_nest_action.tt";
#     my %action = (
# 		type => "html",
# 		html => $html,
# 	    );

#     $self->_register_action("AntNestsController_search_ant_nests", \%action);

    $self->_register_form_action
	("AntNestsController_search_ant_nests", 
	 "/antnestscontroller/get_from_post_code", 
	 "Cerca altri formicai: ", "", 
	 "post_code",
	 "search_ant_nest"
	 );

    #my ($self, $name, $action, $caption_pre_input, $caption_post_input, $submit_name) = @_;
    $self->_register_form_action
	("AntNestsController_create_ant_nest", 
	 "/antnestscontroller/create_ant_nest", 
	 "Voglio creare un formicaio per questo CAP: ", "", 
	 "post_code",
	 "create_ant_nest_submit"
	 );

    $self->_register_action_href
	("ANC_go_to_current_ant_nest", "/antnestscontroller/go_to_current_ant_nest", 
	 "Vai all'ultimo formicaio visitato: [% Catalyst.session.current_ant_nest_id %]", "exists_current_ant_nest.tt");

    $self->_register_action_href
	("ANC_create_current_ant_nest", "/antnestscontroller/create_[%current_ant_nest_id%]", 
	 "Voglio creare questo formicaio: [% current_ant_nest_id %]");

    $self->_register_action_href
	("ANC_want_to_found", "/antnestscontroller/want_to_found_ant_nest", 
	 "Mi piacerebbe fondare questo formicaio.");

    $self->_register_action_href
	("ANC_want_to_be_informed", "/antnestscontroller/want_to_be_informed", 
	 "Voglio essere informato soltanto per quando sarà aperto.");

    $self->_register_action_href
	("ANC_want_to_send_a_message", "/antnestscontroller/", 
	 "Voglio spedire un messaggio alle persone interessate alla creazione di questo formicaio.");

    bless ($self, $class);
    return $self;
}

sub want_to_be_informed : Local {
    my ( $self, $c ) = @_;
    
    #ok, I want to found... I should simply put a dialog

    #the user wants to found this ant nest


    my $dlg = breadcrumbs::Dialogs::WantToCreateAntNestDialog->new
	($c, "0", 
	 $c->session->{sack}->{new_ant_nest_details}->{total_interested},
	 $c->session->{sack}->{new_ant_nest_details}->{founders_count});

    $c->session->{sack}->{want_to_be_founder} = 0;
    $c->session->{hold_the_sack} = 1;

    $self->_process_this_dialog
	($c,
	 $dlg, 
	 "want_to_found_ant_nest.tt", 
	 '/antnestscontroller/want_to_found_ant_nest_on_ok'); 

    $c->detach();
}

sub want_to_found_ant_nest : Local {
    my ( $self, $c ) = @_;
    
    #ok, I want to found... I should simply put a dialog

    #the user wants to found this ant nest
    my $dlg = breadcrumbs::Dialogs::WantToCreateAntNestDialog->new
	($c, "1", $c->session->{sack}->{new_ant_nest_details}->{total_interested},
	 $c->session->{sack}->{new_ant_nest_details}->{founders_count});

    $c->session->{sack}->{want_to_be_founder} = 1;
    $c->session->{hold_the_sack} = 1;

    $self->_process_this_dialog
	($c,
	 $dlg, 
	 "want_to_found_ant_nest.tt", 
	 '/antnestscontroller/want_to_found_ant_nest_on_ok'); 

    $c->detach();
}

sub want_to_found_ant_nest_on_ok : Local {
    my ( $self, $c ) = @_;

    #I must have the name, the email and some other things...

    my $result = $c->stash->{result};

    my $nick = breadcrumbs::Dialogs::WantToCreateAntNestDialog->get_nick($result);

    #the id is given by the ant nest details
    my $id_ant_nest;

    if ( exists($c->session->{sack}->{new_ant_nest_details}->{id})){
	$id_ant_nest = $c->session->{sack}->{new_ant_nest_details}->{id};
    } else {
	$id_ant_nest = $c->session->{current_ant_nest_id};
    }


    my $ant_nest_name = breadcrumbs::Dialogs::WantToCreateAntNestDialog->get_ant_nest_name($result);
    my $password = breadcrumbs::Dialogs::WantToCreateAntNestDialog->get_password($result);
    my $email = breadcrumbs::Dialogs::WantToCreateAntNestDialog->get_email($result);

    my $comment;

    if ($c->session->{sack}->{new_ant_nest_details}->{total_interested}  == 0){
	$comment = "";
    } else {
	$comment = breadcrumbs::Dialogs::WantToCreateAntNestDialog->get_comment($result);
	$ant_nest_name = $c->session->{sack}->{new_ant_nest_details}->{proposed_name};
    }

    #with all these parameters I can issue the command to the daemon
    my $res;
    if ($c->req->param("want_to_be_founder") == 1){
	$res = $c->model("BcdModel")
	    ->create_new_founder($id_ant_nest, $ant_nest_name, $nick, $email, 
				 $password, $comment);
    } else {
	$res = $c->model("BcdModel")
	    ->create_new_looker($id_ant_nest, $ant_nest_name, $nick, $email);
    }

    if ($res->{exit_code} == 0){

	if ($c->req->param("want_to_be_founder") == 1){
	    $c->flash->{status_msg} = "Ok, $nick, riceverai presto un'email contentente il tuo gettone di conferma.";
	} else {
	    $c->flash->{status_msg} = "Ok, $nick, riceverai presto un'email di conferma.";
	}

    } else {
	return $res->{exit_code};
    }

    $c->response->redirect($c->uri_for(qq{/newantnests/view_building_ant_nests}));
    return $res->{exit_code};
}

sub view_summary_table : Local {
    my ( $self, $c ) = @_;

    $self->_process_table_sorting_orders($c);


    $self->_put_action_in_stash($c, "ANC_go_to_current_ant_nest");
    $self->_put_action_in_stash($c, "AntNestsController_search_ant_nests", 
				"Restringi la ricerca, trova solo quelli che iniziano per:");
    $self->_put_action_in_stash($c, "AntNestsController_create_ant_nest");

    #I still need the dataset
    $c->stash->{dataset} = $c->session->{dataset};
    $c->flash->{hold_the_dataset} = 1;

    $c->stash->{template} = 'an_index.tt';
}


=head2 index 

=cut

sub index : Private {
    my ( $self, $c ) = @_;



    #ok, now I should get the summary from the model

    my $res = $c->model('BcdModel')->get_ant_nests_summary();

    $c->session->{dataset} = $res->{"ant_nests_summary"};
    $c->flash->{hold_the_dataset} = 1;
    $c->response->redirect($c->uri_for("/antnestscontroller/view_summary_table"));

    #this is the initial index to the ant nests
    #$c->stash->{template} = 'an_index.tt';
}

sub go_to_current_ant_nest : Local {
    my ( $self, $c ) = @_;

    my $id = $c->session->{current_ant_nest_id};

    #I should differentiate between a "real" ant nest and a ant nest
    #which is under construction

    my $url;

    if ( Bcd::Data::PostCode::is_valid_post_code_with_sub($id)){
	$url = $c->uri_for("/antnest/view_$id");
    } else {
	$url = $c->uri_for("/newantnest/view_$id");
    }

    $c->res->redirect("$url");
}

sub create : LocalRegex('^create_(\d+)$') {
    my ( $self, $c ) = @_;

    $c->stash->{current_ant_nest_id} = $c->request->captures->[0];
    $c->detach(qq/failed_ant_nest_search/);
}

sub create_ant_nest : Local {
    my ( $self, $c ) = @_;

    my $id = $c->req->params->{"post_code"};

    $c->stash->{current_ant_nest_id} = $id;
    if (! Bcd::Data::PostCode::is_valid_post_code($id)){
	#invalid post code
	$c->flash->{status_msg} = "Il codice postale (CAP) in Italia è un numero a 5 cifre ma tu mi hai dato $id!";
	$c->res->redirect($c->req->referer());
	return 1;
    } else {


	#ok, now let's see if it is a generic code
	my ($ans, $city) = Bcd::Data::PostCode::is_this_a_generic_post_code($id);
	
	if ($ans == 1){
	    #no generic post codes, please
	    $c->flash->{status_msg} = "Non si può creare un formicaio grande quanto $city. Scegli un CAP meno generico...";
	    $c->session->{sack}->{post_code_requested} = $id;
	    $c->session->{hold_the_sack} = 1;
	
	    $c->res->redirect($c->req->referer());
	    return 1;
	}


	#the postal code is valid, let's see if there is already an ant nest here
	my $res = $c->model('BcdModel')->
	    get_ant_nests_from_post_code($id);

	my $count = scalar(@{$res->{"ant_nests"}});

	if ($count != 0){
	    #well, ok, there are some ant nests, but are they only to test?
	    $c->stash->{all_test} = 1;

	    foreach(@{$res->{ant_nests}}){
		if (!Bcd::Data::PostCode::is_this_a_test_post_code($_->{id})){
		    $c->stash->{all_test} = 0;
		    last;
		}
	    }
	}

	if ($c->stash->{all_test} == 0 and $count > 1){

	    #there are already some other ant nests...

	    $c->flash->{status_msg} = "Ci sono già dei formicai per questo codice... $id";
	    $c->session->{dataset} = $res->{"ant_nests"};
	    $c->flash->{hold_the_dataset} = 1;
	    $c->response->redirect($c->uri_for("/antnestscontroller/get_from_post_code_table"));

	} elsif ($c->stash->{all_test} == 0 and $count == 1) {
	    #only one ant nest... go for it
	    $c->stash->{current_ant_nest_id} = $id;
	    $c->stash->{ant_nest}     = $res->{ant_nests}->[0];
	    $c->stash->{status_msg} = "Esiste già questo formicaio a questo CAP $id";
	    $c->detach("/antnest/view_this_ant_nest");

	} else {
	    #or count == 0 or all are in test
	    $c->detach(qq/failed_ant_nest_search/);
	}
    }
}


sub _fill_actions{
    my ($self, $c) = @_;

    my $actions = $c->stash->{actions};

#     breadcrumbs::util::MenuUtils->add_action("/azione", 
# 					     "Cerca formicai ad esso vicini", \@actions, $c);
#     breadcrumbs::util::MenuUtils->add_action("/azione", 
# 					     "Voglio iscrivermi a questo formicaio", \@actions, $c);
#     breadcrumbs::util::MenuUtils->add_action("/azione", 
# 					     "Cerca formicai vicini per iscrivermi", \@actions, $c);

    #my $html = "ma che bello questo e` <strong>strong</strong>";
    my $html = "search_ant_nest_action.tt";
    my %hash = (
		"html" => $html,
		"caption" => "Puoi cliccare su uno di questi oppure ripetere la ricerca usando un altro valore: ",
	    );

    push(@{$actions}, \%hash);

    #$c->stash->{actions} = \@actions;
}

#this method should simply create a form to search an ant nest
sub make_search_from_post_code_widget {
    my ($self, $c, $label) = @_;

    # Create an HTML::Widget to build the form
    my $w = $c->widget('search_ant_nest')->method('post');

    my $fs = $w->element('Fieldset', 'search')->legend('Ricerca per codice postale');

    $fs->element('Textfield', 'post_code')
	->label(defined($label) ? $label : 'Dammi il codice postale:');
    $fs->element('Submit',    'submit' )->value('Ok');

    return $w;

    
}

sub get_from_post_code_exact : Local {
   my ($self, $c) = @_;
   my $req_code;


   if (defined ($c->request->param("post_code"))){
       $req_code = $c->req->param("post_code");
   } else {
       $req_code = $c->session->{sack}->{post_code_requested};
   }


   #valid post code?
   if (! Bcd::Data::PostCode::is_valid_post_code($req_code)){
       $c->flash->{status_msg} = "Dovresti darmi per piacere un codice postale valido (un numero a cinque cifre)";
       $c->res->redirect($c->req->referer());
       $c->detach();
   } else {
       #generic post code?
       my ($ans, $city) = Bcd::Data::PostCode::is_this_a_generic_post_code($req_code);

       if ($ans == 1){
	   #this is generic...
	   $c->session->{sack}->{generic_city} = $city;
	   $c->stash->{req_code} = $req_code;
	   $c->detach('get_generic_city_ant_nests');
       }
       
   }

   $c->detach(qq/get_from_post_code/);
}

sub get_generic_city_ant_nests : Private {

    my ($self, $c) = @_;

    my $code = $c->stash->{req_code};

    my $nearest =  $c->model('BcdModel')->get_nearest_ant_nests($code);

    $c->session->{sack}->{ant_nests}       = $nearest->{ant_nests};
    $c->session->{hold_the_sack} = 1;
    
    $c->flash->{status_msg} = "Ricerca di formicai in città divisa per zone postali";
    $c->response->redirect($c->uri_for("/antnestscontroller/generic_city_ant_nests_table"));
}

sub generic_city_ant_nests_table : Local {
    my ($self, $c) = @_;

    $self->_process_table_sorting_orders($c);

    $self->_put_action_in_stash($c, "ANC_go_to_current_ant_nest");
    $self->_put_action_in_stash($c, "AntNestsController_create_ant_nest");

    #I still need the sack
    $c->session->{hold_the_sack} = 1;
    $c->stash->{template} = "generic_city_search.tt";
}


sub get_from_post_code_table : Local  {
    my ($self, $c) = @_;

    $self->_process_table_sorting_orders($c);

    #nothing to do, the fields and dataset are already in the session
    $c->stash->{template} = 'ant_nests_list.tt2';

    $c->stash->{dataset} = $c->session->{dataset};

    $self->_put_action_in_stash($c, "ANC_go_to_current_ant_nest");

    $self->_put_action_in_stash($c, "AntNestsController_search_ant_nests", 
				"Cambia la ricerca, trova i formicai che iniziano per:");

    #I sthill need the dataset
    $c->flash->{hold_the_dataset} = 1;
}

sub get_from_post_code : Local {

    my ($self, $c, $args) = @_;

    my $req_code = $c->request->param("post_code");
    my $res = $c->model('BcdModel')->
 	get_ant_nests_from_post_code($req_code);

    my $exit_code = $res->{"exit_code"};

    if ($exit_code != 0){
	#something wrong, probably the post code is invalid
	$c->flash->{status_msg} = "Codice postale non valido";
	$c->stash->{template} = 'bc_hello.tt';
	$c->res->redirect($c->request->referer);
    } else {

	$c->session->{dataset} = $res->{"ant_nests"};
	$c->flash->{hold_the_dataset} = 1;


	#if there is only one ant nest, go directly to the ant nest controller
	if (scalar(@{$res->{ant_nests}}) == 1){

	    if ($c->action =~ /exact/){

		my $id = $res->{"ant_nests"}->[0]->{id};
		my $url = $c->uri_for("/antnest/view_$id");		
		$c->res->redirect("$url");
		$c->flash->{status_msg} = "C'è solo questo formicaio a questo cap";
		return;

	    } else {
		$c->stash->{status_msg} = "C'è solo questo formicaio al CAP $req_code";
	    }
	    #$c->stash->{current_ant_nest_id} = $id;
	    #$c->detach('/antnest/view_ant_nest');
	} elsif ($#{$res->{"ant_nests"}} == -1){
	    #no ant nests...
	    if (length($req_code) == 5){
		#ok, the user wanted an exact match
		my ($ans, $city) = Bcd::Data::PostCode::is_this_a_generic_post_code($req_code);

		if ($ans == 1){
		    #this is generic...
		    $c->session->{sack}->{generic_city} = $city;
		    $c->stash->{req_code} = $req_code;
		    $c->detach('get_generic_city_ant_nests');
		} else {

		    $c->stash->{current_ant_nest_id} = $c->request->param("post_code");

		    $c->flash->{status_msg} = "Nessun formicaio attivo al cap $req_code";
		    $c->detach("failed_ant_nest_search");
		}

	    } else {
		#no match, stay here
		$c->stash->{status_msg} = "Nessun formicaio che comincia per $req_code";		
	    }
	}
	
	#$c->stash->{template} = 'ant_nests_list.tt2';
	$c->response->redirect($c->uri_for("/antnestscontroller/get_from_post_code_table"));
    }

    $self->_put_action_in_stash($c, "ANC_go_to_current_ant_nest");

    $self->_put_action_in_stash($c, "AntNestsController_search_ant_nests", 
				"Cambia la ricerca, trova i formicai che iniziano per:");

}

#this function is called whenever the search does not have any result
sub failed_ant_nest_search : Private{
    my ($self, $c) = @_;

    my $id = $c->stash->{current_ant_nest_id};
    #$c->session->{current_ant_nest_id} = $id;

    #ok now I should get the nearest ant nests
    my $res = $c->model('BcdModel')->get_new_ant_nest_detail($id);

    if ( $res->{exit_code} == Bcd::Errors::ErrorCodes::BEC_OK ){
	#ok, there is already a booking for this ant nest.
	$c->session->{sack}->{new_ant_nest_details} = $res;
	$c->session->{hold_the_sack} = 1;
	$c->session->{current_ant_nest_id} = $id;
	$c->response->redirect($c->uri_for('/newantnest/view_booked_details'));
    } else {

	my $nearest =  $c->model('BcdModel')->get_nearest_ant_nests($id);
	$c->session->{sack}->{ant_nests}       = $nearest->{ant_nests};

	$c->session->{hold_the_sack} = 1;
	$c->session->{sack}->{new_ant_nest_details}->{total_interested} = 0; #of course
	$c->session->{sack}->{new_ant_nest_details}->{id}               = $id; 
	$c->session->{sack}->{all_test_ant_nest}                        = $c->stash->{all_test};
	$c->flash->{status_msg} = "Non esiste ancora un formicaio $id, qui lo puoi creare";
	$c->response->redirect($c->uri_for("/antnestscontroller/failed_ant_nest_search_table"));
    }

}

sub failed_ant_nest_search_table : Local {
    my ($self, $c) = @_;

    $self->_process_table_sorting_orders($c);

    $self->_put_action_in_stash($c, "ANC_want_to_found");
    $self->_put_action_in_stash($c, "ANC_want_to_be_informed");
    #$self->_put_action_in_stash($c, "ANC_want_to_send_a_message");
    $self->_put_action_in_stash($c, "ANC_go_to_current_ant_nest");
    $self->_put_action_in_stash($c, "AntNestsController_search_ant_nests");

    #$c->stash->{dataset} = $c->session->{sack}->{ant_nests};
    $c->session->{hold_the_sack} = 1;

    $c->stash->{template} = "not_existing_ant_nest.tt";
}

# I should update the trail
sub auto : Private {
    my ( $self, $c) = @_;

    my $trails = $c->stash->{"trails"};
    my $url = $c->uri_for('');
    my @item = ('I formicai', "$url");

    push(@{$trails}, \@item);

    #$c->stash->{"trails"} = \@item;

    return 1; #You can continue
}

sub _make_menu{
    my ( $self, $c) = @_;

    my $menu = $c->stash->{"menu_items"};;

    breadcrumbs::util::MenuUtils->add_menu_entry
	('/newantnests/view_building_ant_nests', 'Formicai in costruzione', $menu, $c);
    
}

=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
