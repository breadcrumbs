package breadcrumbs::Controller::UserAds;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;

use breadcrumbs::Dialogs::AdTextDialog;
use breadcrumbs::Dialogs::AdLocalityDialog;
use breadcrumbs::Dialogs::AdTrustsDialog;
use breadcrumbs::Dialogs::SelectPublicSiteDialog;
use breadcrumbs::Dialogs::SelectPresenceInSiteDialog;
use breadcrumbs::Dialogs::PresenceInPublicSiteDialog;
use breadcrumbs::Dialogs::AdChooseTrustTypeDialog;
use Bcd::Data::Trust;

=head1 NAME

breadcrumbs::Controller::UserAds - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new{

    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    $self->_register_action_href
	("u_ads_new_service", "new_service", "Voglio offrire un servizio");

    $self->_register_action_href
	("u_ads_new_used_item", "new_used", "Voglio vendere un oggetto usato");

    $self->_register_action_href
	("u_ads_new_homemade_item", "new_homemade", "Voglio vendere una mia creazione");
    

    bless ($self, $class);
    return $self;
}

sub new_homemade : Local {
    my ( $self, $c ) = @_;

    $c->forward('new_ad', ["homemade"]);
}

sub new_used : Local {
    my ( $self, $c ) = @_;

    $c->forward('new_ad', ["object"]);
}

sub new_service : Local {
    my ( $self, $c ) = @_;


    $c->forward('new_ad', ["service"]);
}

sub new_ad : Local {
    my ($self, $c, $ad_type) = @_;

    my $wizard_caption = "Nuovo ";

    my $res;
    my $tao_caption;
    if ($ad_type eq "service"){
	$wizard_caption .= "servizio";
	$res = $c->model("BcdModel")->get_services_tree();
	$tao_caption = "Tao all'ora:";
    } elsif ($ad_type eq "object"){
	$wizard_caption .= "oggetto usato";
	$res = $c->model("BcdModel")->get_used_tree();
	$tao_caption = "Tao:";
    } elsif ($ad_type eq "homemade"){
	$wizard_caption .= "oggetto fatto in casa";
	$res = $c->model("BcdModel")->get_homemade_tree();
	$tao_caption = "Tao al pezzo:";
    }

    #I should get the public sites of this dialog
    my $pub_res      = $c->model("BcdModel")->get_public_sites($c);
    my $public_sites_ar = $pub_res->{public_sites};
    my $public_sites = $self->_hashify_a_recordset($public_sites_ar, "id");
     
    #this is the generic function to create a new ad.
    my $dlg1 = breadcrumbs::Dialogs::AdTextDialog  ->new($c, 1);
    my $dlg2 = breadcrumbs::Dialogs::AdLocalityDialog->new($c, 1);

    my $dlg3 = breadcrumbs::Dialogs::SelectPublicSiteDialog->new($c, 1, $public_sites);
    my $dlg4 = breadcrumbs::Dialogs::SelectPresenceInSiteDialog->new($c, 1);
    my $dlg5 = breadcrumbs::Dialogs::PresenceInPublicSiteDialog->new($c, 1);


    my $dlg6 = breadcrumbs::Dialogs::TaoImportDialog->new($c, 1, $tao_caption);
    my $dlg7 = breadcrumbs::Dialogs::AdChooseTrustTypeDialog->new($c, 1);
    my $dlg8 = breadcrumbs::Dialogs::AdTrustsDialog->new($c, 1);

    #ok, I should make a wizard!
    my @step0 = (undef, "new_ad_choose_activity.tt", "choose_activity_on_ok");
    my @step1 = ($dlg1, "new_ad_enter_text.tt", "entered_ad_text_on_ok");
    my @step2 = ($dlg2, "new_ad_enter_locality.tt", "entered_locality_on_ok");
    my @step3 = ($dlg3, "new_ad_choose_loc.tt", "choose_loc_ok");
    my @step4 = ($dlg4, "new_ad_choose_presence.tt", "choose_presence_ok");
    my @step5 = ($dlg5, "new_ad_create_presence.tt", "create_presence_ok");
    my @step6 = ($dlg6, "new_ad_choose_price.tt", "choose_price_on_ok");
    my @step7 = ($dlg7, "new_ad_choose_trust_type.tt", "choose_trust_type_on_ok");
    my @step8 = ($dlg8, "new_ad_choose_trusts.tt", "choose_trusts_on_ok");
    my @step9 = (undef, "new_ad_review_data.tt", "review_data_on_ok");
    my @stepA = (undef, "new_ad_done.tt");

    my @steps = (\@step0, \@step1, \@step2, \@step3, \@step4, 
		 \@step5, \@step6, \@step7, \@step8, \@step9, \@stepA);

    #I take the tree from the model

    $self->_make_wizard($c, \@steps, '', $wizard_caption);

    $c->session->{steps}->{act_tree} = $res->{act_tree};
    $c->session->{steps}->{ad_type} = $ad_type;
    $c->session->{steps}->{public_sites} = $public_sites;
    return 1;
}

sub add_ad_presence : Local {
    my ($self, $c) = @_;
    my $ad_type = $c->req->param("ad_type");
    my $id_ad   = $c->req->param("id_ad");



    #probably this can be made common with the new ad
    my $tao_caption;
    if ($ad_type eq "service"){
	$tao_caption = "Tao all'ora:";
    } elsif ($ad_type eq "object"){
	$tao_caption = "Tao:";
    } elsif ($ad_type eq "homemade"){
	$tao_caption = "Tao al pezzo:";
    }

    my $pub_res      = $c->model("BcdModel")->get_public_sites($c);
    my $public_sites_ar = $pub_res->{public_sites};
    my $public_sites = $self->_hashify_a_recordset($public_sites_ar, "id");

    my $dlg0 = breadcrumbs::Dialogs::AdLocalityDialog->new($c, 1);
    my $dlg1 = breadcrumbs::Dialogs::SelectPublicSiteDialog->new($c, 1, $public_sites);
    my $dlg2 = breadcrumbs::Dialogs::SelectPresenceInSiteDialog->new($c, 1);
    my $dlg3 = breadcrumbs::Dialogs::PresenceInPublicSiteDialog->new($c, 1);
    my $dlg4 = breadcrumbs::Dialogs::TaoImportDialog->new($c, 1, $tao_caption);
    my $dlg5 = breadcrumbs::Dialogs::AdChooseTrustTypeDialog->new($c, 1);
    my $dlg6 = breadcrumbs::Dialogs::AdTrustsDialog->new($c, 1);

    #ok, I should add a presence
    my @step0 = ($dlg0, "new_ad_enter_locality.tt", "entered_locality_on_ok");
    my @step1 = ($dlg1, "new_ad_choose_loc.tt", "choose_loc_ok");
    my @step2 = ($dlg2, "new_ad_choose_presence.tt", "choose_presence_ok");
    my @step3 = ($dlg3, "new_ad_create_presence.tt", "create_presence_ok");
    my @step4 = ($dlg4, "new_ad_choose_price.tt",    "choose_price_on_ok");
    my @step5 = ($dlg5, "new_ad_choose_trust_type.tt", "choose_trust_type_on_ok");
    my @step6 = ($dlg6, "new_ad_choose_trusts.tt",   "choose_trusts_on_ok");
    my @step7 = (undef, "new_ad_review_data.tt",     "review_data_on_ok");
    my @step8 = (undef, "new_ad_done.tt");

    my @steps = (\@step0, \@step1, \@step2, \@step3, \@step4, 
		 \@step5, \@step6, \@step7, \@step8);
    $self->_make_wizard($c, \@steps, '', "Aggiunta di una presenza");

    $c->session->{steps}->{ad_type}      = $ad_type;
    $c->session->{steps}->{id_ad}        = $id_ad;
    $c->session->{steps}->{public_sites} = $public_sites;
    $c->session->{steps}->{reason}       = 'add_presence';
    $c->session->{steps}->{act_name} = $c->req->param("activity");
    $c->session->{steps}->{ad_text}  = $c->req->param("ad_text");

    return 1;
}

sub create_presence_ok : Local {
    my ($self, $c) = @_;

    my $presence_text = $c->req->param("available_hours");
    $c->session->{steps}->{available_hours} = $presence_text;
    my $request = $c->req->param("request");
    $c->session->{steps}->{request} = $request;

    if ($request eq "on"){
	$presence_text .= " a richiesta";
    }

    $c->session->{steps}->{create_presence} = 1;
    $c->session->{steps}->{presence_text} = $presence_text;

    return 1;
}

sub choose_presence_ok : Local {
    my ($self, $c) = @_;

    my $presence = $c->req->param('presence');
    $c->session->{steps}->{presence_id} = $presence;

    if ($presence eq "NULL"){
	#ok, the user wants to create a presence
	$c->session->{steps}->{create_presence} = 1;
	return 1;
    } else {
	$c->session->{steps}->{create_presence} = 0;
	my $presence_text = $c->session->{steps}->{presences}->{$presence}->{presence};

	if ($c->session->{steps}->{presences}->{$presence}->{on_request} eq "1"){
	    $presence_text .= " a richiesta";
	}

	$c->session->{steps}->{presence_text} = $presence_text;
	return 2;
    }
}

sub review_data_on_ok : Local {
    my ($self, $c) = @_;
    #ok, at last I can create a new ad...

    #let's see, first of all if I must create a presencee.
    my $presence_id;
    if( $c->session->{steps}->{create_presence} eq "1"){
	#ok, I must create a presence.

	#the id of the site, 
	my $loc = $c->session->{steps}->{ad_loc_type};
	
	my $id_site;
	my $id_special_site;
	if ($loc eq "an_point"){
	    #ok, it is a ant nest point
	    $id_site = $c->session->{steps}->{public_site};
	    $id_special_site = "NULL";
	} else {
	    #it is a special site
	    $id_site = "NULL";
	    $id_special_site = $loc;
	}

	#ok, now I should have the rel type, which is simply the ad type
	my $rel_type = $c->session->{steps}->{ad_type};

	#then the request
	my $request;
	if (!defined($c->session->{steps}->{request})){
	    $request = '0';
	} else {
	    $request = '1';
	}

	#then the presence
	my $presence = $c->session->{steps}->{available_hours};

	my $res = $c->model("BcdModel")->create_presence
	    ($c, $id_site, $id_special_site, $rel_type, $request, $presence);

	if (!$self->_is_valid_res($c, $res)){
	    return 0;
	}

	#ok, I have created a presence
	$presence_id = $res->{presence_id};
    } else {
	#ok, now I can create an ad...
	$presence_id = $c->session->{steps}->{presence_id};
    }

    my $p_min = $c->session->{steps}->{tao_import};

    my $res;
    if ($c->session->{steps}->{reason} ne "add_presence"){

	$res = $c->model("BcdModel")->create_ad
	    ($c, $c->session->{steps}->{act_id}, $c->session->{steps}->{ad_text},
	     $presence_id, $p_min, $p_min, 
	     $c->session->{steps}->{t_e},
	     $c->session->{steps}->{t_c});
    } else {
	$res = $c->model("BcdModel")->add_ad_presence
	    ($c, $c->session->{steps}->{id_ad},
	     $presence_id, $p_min, $p_min, 
	     $c->session->{steps}->{t_e},
	     $c->session->{steps}->{t_c});
    }

    $c->session->{steps}->{debug_res} = $res;

    if (!$self->_is_valid_res($c, $res)){
	return 0;
    } else {
	return 1;
    }
}

sub choose_trust_type_on_ok : Local {
    my ($self, $c) = @_;
    
    #i can return 1 or 2 depending on the trust type
    my $trust_type = $c->req->param("flag");
    my $t_e;
    my $t_c;
    if ($trust_type eq "all_see_all_credit"){

	#ok, I can simply ask the price for this ad..
	$t_e = "-INF";
	$t_c = "-INF";

    } elsif ($trust_type eq "all_see_nobody_credit"){

	$t_e = "-INF";
	$t_c = "100";

    } elsif ($trust_type eq "not_all_see_nobody_credit"){

	$c->session->{steps}->{t_c} = "100";
	delete $c->session->{steps}->{t_e};
	#t_e is to be determined after...
	$c->session->{steps}->{edit_t_e} = 1;
	delete $c->session->{steps}->{edit_t_c};
	return 1;

    } elsif ($trust_type eq "all_see_not_all_credit"){

	$c->session->{steps}->{t_e} = "-INF";
	delete $c->session->{steps}->{t_c};
	#t_c is to be determined after...
	$c->session->{steps}->{edit_t_c} = 1;
	delete $c->session->{steps}->{edit_t_e};
	return 1;

    } else {

	#the user want to determine the two trusts
	delete $c->session->{steps}->{t_e};
	delete $c->session->{steps}->{t_c};
	$c->session->{steps}->{edit_t_e} = 1;
	$c->session->{steps}->{edit_t_c} = 1;
	return 1;

    }
    delete $c->session->{steps}->{edit_t_c};
    delete $c->session->{steps}->{edit_t_e};

    $c->session->{steps}->{t_e} = $t_e;
    $c->session->{steps}->{t_c} = $t_c;
    my $p_min = $c->session->{steps}->{tao_import};

    my $res = $c->model("BcdModel")->get_price_for_ad
	($c, $p_min, $p_min, $t_e, $t_c);

    if (!$self->_is_valid_res($c, $res)){
	return 0;
    }
    
    $c->stash->{ad_price} = $res->{price};
    $c->stash->{credit_ants} = $res->{reachable_ants};
    $c->stash->{debug_res} = $res;
    return 2;


}

sub choose_trusts_on_ok : Local {
    my ($self, $c) = @_;

    my $result = $c->stash->{result};
    my $t_e;
    my $t_c;
    if (defined($c->session->{steps}->{edit_t_e})){
	$t_e = breadcrumbs::Dialogs::AdTrustsDialog->get_trust_emit($result);
	$c->session->{steps}->{t_e} = $t_e;
    } else {
	$t_e = $c->session->{steps}->{t_e};
    }

    if (defined($c->session->{steps}->{edit_t_c})){
	$t_c = breadcrumbs::Dialogs::AdTrustsDialog->get_trust_credit($result);
	$c->session->{steps}->{t_c} = $t_c;
    } else {
	$t_c = $c->session->{steps}->{t_c};
    }

    #I can compute here the percentage of ants which can pay by credit
    my $p_min = $c->session->{steps}->{tao_import};
    
    my $res = $c->model("BcdModel")->get_price_for_ad
	($c, $p_min, $p_min, $t_e, $t_c);

    if (!$self->_is_valid_res($c, $res)){
	return 0;
    }
    
    $c->stash->{ad_price} = $res->{price};
    $c->stash->{credit_ants} = $res->{reachable_ants};
    $c->stash->{debug_res} = $res;
    return 1;
}

sub choose_price_on_ok : Local {
    my ($self, $c) = @_;

    my $result = $c->stash->{result};
    my $tao = breadcrumbs::Dialogs::TaoImportDialog->get_amount($result);
    $c->session->{steps}->{tao_import} = $tao;

    return 1;
}

sub entered_locality_on_ok : Local {
    my ($self, $c) = @_;
    
    my $loc = $c->req->param("flag");
    $c->session->{steps}->{ad_loc_type} = $loc;
    if ($loc eq "an_point"){
	return 1;
    } else {
	#I should get the presences for this special point
	my $res = $c->model("BcdModel")->get_presences
	    ($c, "NULL", $loc, $c->session->{steps}->{ad_type});

	my $presences = $res->{presences};
	my $presences_hashified = $self->_hashify_a_recordset($presences, "id");
	$c->session->{steps}->{presences} = $presences_hashified;

	if (scalar(keys(%{$presences_hashified})) == 0){
	    $c->session->{steps}->{create_presence} = 1;
	    return 3;
	} else {
	    $c->session->{steps}->{create_presence} = 0;
	    return 2;
	}
    }
}

# sub _hashify_the_presences{
#     my ($self, $presences) = @_;

#     my %hash;
#     foreach(@{$presences}){
# 	$hash{$_->{id}} = 
# 	{
# 	    on_request => $_->{on_request},
# 	    presence   => $_->{presence},
# 	}
#     }

#     return \%hash;
# }

sub entered_ad_text_on_ok : Local {
    my ($self, $c) = @_;
    
    my $text = $c->req->param("ad_text");
    $c->session->{steps}->{ad_text} = $text;

    return 1;
}

sub choose_activity_on_ok : Local {
    my ($self, $c) = @_;

    my $act = $c->req->param("act_id");
    my $act_name = $c->req->param("act_name");

    $c->session->{steps}->{act_id} = $act;
    $c->session->{steps}->{act_name} = $act_name;
    
    return 1;
}

sub choose_loc_ok : Local {
    my ($self, $c) = @_;

    my $public_site_id = $c->req->param("public_site");
    $c->session->{steps}->{public_site} = $public_site_id;
    $c->session->{steps}->{public_site_name} = 
	$c->session->{steps}->{public_sites}->{$public_site_id}->{name};

    #ok, now I should get the presences in this site

    my $res = $c->model("BcdModel")->get_presences
	($c, $public_site_id, "NULL", $c->session->{steps}->{ad_type});

    my $presences = $res->{presences};
    my $presences_hashified = $self->_hashify_a_recordset($presences, "id");
    $c->session->{steps}->{presences} = $presences_hashified;

    if (scalar(keys(%{$presences_hashified})) == 0){
	$c->session->{steps}->{create_presence} = 1;
	return 2;
    } else {
	$c->session->{steps}->{create_presence} = 0;
	return 1;
    }
}


sub index : Private {
    my ( $self, $c ) = @_;

    #Ok, let's get the ads of this user.
    my $res = $c->model("BcdModel")->get_my_ads($c);

    my $ads = $res->{my_ads};
    $c->session->{sack}->{my_ads} = $ads;
    $c->session->{hold_the_sack} = 1;
    
    $c->response->redirect($c->uri_for("/userads/view_ads_table"));
}

sub view_my_ad : LocalRegex('^view_my_ad_(\d+)$') {
    my ( $self, $c) = @_;

    my $ad_id= $c->req->captures->[0];

    #ok, I get the details of this ad!
    my $res = $c->model("BcdModel")->get_my_ad_details($c, $ad_id);
    $c->stash->{ad}      = $res->{ad};
    $c->stash->{ads_loc} = $res->{ads_loc};
    $c->stash->{id_ad}   = $ad_id;
    
    $c->stash->{template} = "user_ad_details.tt";
}

sub view_ads_table : Local {
    my ( $self, $c ) = @_;

    $self->_process_table_sorting_orders($c);

    $self->_put_action_in_stash($c, "u_ads_new_homemade_item");
    $self->_put_action_in_stash($c, "u_ads_new_used_item");
    $self->_put_action_in_stash($c, "u_ads_new_service");

    $c->stash->{ads} = $c->session->{sack}->{my_ads};
    $c->session->{hold_the_sack} = 1;

    $c->stash->{template} = 'user_ads.tt';
}

sub _make_menu{
    my ( $self, $c ) = @_;

    my $menu = $c->stash->{"menu_items"};;

    breadcrumbs::util::MenuUtils->add_menu_entry('/userpresences', 'Luoghi e tempi', $menu, $c);
}

sub auto : Private {
    my ( $self, $c) = @_;

    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();


    #I shouold find another method to put the trails
    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/user", "$name", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/userads", "I miei annunci", $trails, $c);

    return 1;

	
}


=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
