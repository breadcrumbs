package breadcrumbs::Controller::CashUserEuro;

use strict;
use warnings;

use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;

use breadcrumbs::Dialogs::DepositWithdrawalRequestDialog;
use breadcrumbs::Dialogs::SecretTokenDialog;
use Bcd::Data::Token;

=head1 NAME

breadcrumbs::Controller::CashUserEuro - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    $self->_register_action_href
	("cash_us_euro_deposit", "deposit_request", "Voglio depositare Euro sul mio conto.");

    $self->_register_action_href
	("cash_us_euro_withdrawal", "withdrawal_request", "Voglio ritirare degli Euro dal mio conto");

    $self->_register_action_href
	("cash_us_euro_have_secret", "have_secret_deposit", "Ho il gettone segreto per depositare degli euro.");

    $self->_register_action_href
	("cash_us_euro_euro_in_tao", "change_euro_in_tao", "Voglio cambiare degli Euro in Tao");


    bless ($self, $class);
    return $self;
}

sub change_euro_in_tao : Local {

    my ($self, $c) = @_;

    #ok, also in this case I can have a wizard
    my $dlg0  = breadcrumbs::Dialogs::DepositWithdrawalRequestDialog->new($c, 1);

    #ok, I should make a wizard!
    my @step0 = ($dlg0, "get_euro_amount_to_change.tt", 'get_amount_to_change_on_ok');
    my @step1 = (undef, "finished_change.tt");

    my @steps = (\@step0, \@step1);

    $self->_make_wizard($c, \@steps, '', "Cambiare euro in tao.");
    return 1;
}

sub get_amount_to_change_on_ok : Local {
    my ($self, $c) = @_;
    
    my $result = $c->stash->{result};

    my $euro_amount = breadcrumbs::Dialogs::DepositWithdrawalRequestDialog->get_amount($result);

    #ok, I can now make a booking
    my $res = $c->model('BcdModel')->change_euro_in_tao($c, $euro_amount);

    if ($self->_is_valid_res($c, $res)){

	#ok, I put the tokens in the flash
	$c->stash->{amount} = $euro_amount;
	$c->stash->{tao_obtained} = $res->{tao_obtained};
	$c->stash->{new_tot_tao}  = $res->{new_tot_tao};
	$c->stash->{new_tot_euro} = $res->{new_tot_euro};
	
    } else {
	return 0; 
    }

    return 1;

}

sub withdrawal_request : Local {
    my ($self, $c) = @_;

    #ok, also in this case I can have a wizard
    my $dlg0  = breadcrumbs::Dialogs::DepositWithdrawalRequestDialog->new($c, 1);

    #ok, I should make a wizard!
    my @step0 = ($dlg0, "get_withdrawal_request_amount.tt", 'get_withdrawal_amount_on_ok');
    my @step1 = (undef, "finished_withdrawal_request.tt");

    my @steps = (\@step0, \@step1);

    $self->_make_wizard($c, \@steps, '', "Distinta di ritiro.");
    return 1;
}

sub get_withdrawal_amount_on_ok : Local{
    my ($self, $c) = @_;

    my $result = $c->stash->{result};

    #ok, I should have the amount from the dialog
    my $euro_amount = breadcrumbs::Dialogs::DepositWithdrawalRequestDialog->get_amount($result);

    #ok, I can now make a booking
    my $res = $c->model('BcdModel')->create_withdrawal_request($c, $euro_amount);

    if ($self->_is_valid_res($c, $res)){

	#ok, I put the tokens in the flash
	$c->stash->{booking_token} = $res->{booking_token};
	$c->stash->{full_token} = $res->{full_token};
	
    } else {
	return 0; 
    }

    return 1;
}


sub have_secret_deposit : Local {
    my ($self, $c) = @_;

    #ok, I can build a simple wizard with two steps...
    my $dlg0  = breadcrumbs::Dialogs::SecretTokenDialog->new($c, 1);

    #ok, I should make a wizard!
    my @step0 = ($dlg0, "get_secret_deposit_token.tt", 'get_secret_token_on_ok');
    my @step1 = (undef, "finished_user_deposit.tt");

    my @steps = (\@step0, \@step1);

    $self->_make_wizard($c, \@steps, '', "Versamento Euro sul conto.");
    return 1;
    
}

sub get_secret_token_on_ok : Local {
    my ($self, $c) = @_;

    #ok, now I should try to get the secret token, or, otherwise, I
    #get it from the full and the blinded

    my $result = $c->stash->{result};
    my $secret = breadcrumbs::Dialogs::SecretTokenDialog->get_secret_token($result);

    #ok, I put the $secret in $stash
    $c->stash->{secret_token} = $secret;

    #tell the daemon that we want to collect this deposit.
    my $res = $c->model('BcdModel')->claim_deposit($c, $secret);

    if ($self->_is_valid_res($c, $res)){

	$c->stash->{amount_deposited} = $res->{amount_deposited};
	$c->stash->{new_total}        = $res->{new_total};
	
    } else {
	return 0; 
    }

    return 1;
}

sub deposit_request : Local {
    my ( $self, $c ) = @_;

    #ok, I have simply a wizard with two steps...
    my $dlg0  = breadcrumbs::Dialogs::DepositWithdrawalRequestDialog->new($c, 1);

    #ok, I should make a wizard!
    my @step0 = ($dlg0, "get_deposit_request_amount.tt", 'get_deposit_amount_on_ok');
    my @step1 = (undef, "finished_deposit_request.tt");

    my @steps = (\@step0, \@step1);

    $self->_make_wizard($c, \@steps, '', "Distinta di versamento.");
    return 1;
}

sub get_deposit_amount_on_ok : Local {
    my ($self, $c ) = @_;

    my $result = $c->stash->{result};

    #ok, I should have the amount from the dialog
    my $euro_amount = breadcrumbs::Dialogs::DepositWithdrawalRequestDialog->get_amount($result);

    #ok, I can now make a booking
    my $res = $c->model('BcdModel')->create_deposit_request($c, $euro_amount);

    if ($self->_is_valid_res($c, $res)){

	#ok, I put the tokens in the flash
	$c->stash->{booking_token} = $res->{booking_token};
	$c->stash->{blinded_token} = $res->{blinded_token};
	
    } else {
	return 0; 
    }

    return 1;
}

sub index : Private {
    my ( $self, $c ) = @_;

    $self->_put_action_in_stash($c, "cash_us_euro_deposit");
    $self->_put_action_in_stash($c, "cash_us_euro_withdrawal");
    $self->_put_action_in_stash($c, "cash_us_euro_have_secret");
    $self->_put_action_in_stash($c, "cash_us_euro_euro_in_tao");

    #I should now get the deposit request pending from the model...
    my $res = $c->model('BcdModel')->lookup_my_booking($c);
    
    if ($res->{exit_code} != Bcd::Errors::ErrorCodes::BEC_NO_DATA){
	$c->{stash}->{pending_booking} = $res->{booking_details};
    }

    #and also the status of my account.
    $res = $c->model('BcdModel')->get_my_cash_euro_summary($c);
    
    #you don't need to put it into the sack... because it is displayed
    #only in this page, for now

    $c->{stash}->{account_summary} = $res->{account_summary};
    
    $c->stash->{template} = "cash_user_euro.tt";
}

sub cancel_my_booking : Local {
    my ($self, $c) = @_;

    my $res = $c->model('BcdModel')->cancel_my_booking($c);

    if (! $self->_is_valid_res($res)){
	$c->flash->{error_msg} = $c->stash->{error_msg};
    } else {
	$c->flash->{status_msg} = "ok, richiesta cancellata";
    }

    $c->res->redirect($c->uri_for('/cashusereuro'));

}

sub auto : Private {
    my ( $self, $c ) = @_;

    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();

    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/user", "$name", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/cashusereuro", "c/c Euro", $trails, $c);

    return 1;

}

=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
