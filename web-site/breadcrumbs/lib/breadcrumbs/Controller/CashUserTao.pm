
package breadcrumbs::Controller::CashUserTao;

use strict;
use warnings;

use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';
use NEXT;
use breadcrumbs::Dialogs::TaoImportDialog;

=head1 NAME

breadcrumbs::Controller::CashUserTao - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub new {
    my $class = shift;
    my $self  = $class->NEXT::new(@_);

    $self->_register_action_href
	("cash_us_tao_change_tao_euro", "change_tao_in_euro", "Voglio vendere dei tao per Euro.");


    bless ($self, $class);
    return $self;
}

sub change_tao_in_euro : Local {

    my ($self, $c) = @_;

    #ok, also in this case I can have a wizard
    my $dlg0  = breadcrumbs::Dialogs::TaoImportDialog->new($c, 1);

    #ok, I should make a wizard!
    my @step0 = ($dlg0, "get_tao_amount_to_change.tt", 'get_amount_to_change_on_ok');
    my @step1 = (undef, "finished_change_tao_in_euro.tt");

    my @steps = (\@step0, \@step1);

    $self->_make_wizard($c, \@steps, '', "Cambiare tao in euro.");
    return 1;
}

sub get_amount_to_change_on_ok : Local {
    my ($self, $c) = @_;
    
    my $result = $c->stash->{result};

    my $tao = breadcrumbs::Dialogs::TaoImportDialog->get_amount($result);

    #ok, I can now make a booking
    my $res = $c->model('BcdModel')->change_tao_in_euro($c, $tao);

    if ($self->_is_valid_res($c, $res)){

	#ok, I put the tokens in the flash
	$c->stash->{amount} = $tao;
	$c->stash->{euro_obtained} = $res->{euro_obtained};
	$c->stash->{new_tot_tao}  = $res->{new_tot_tao};
	$c->stash->{new_tot_euro} = $res->{new_tot_euro};
	$c->stash->{tao_converted} = $res->{tao_converted};
	
    } else {
	return 0; 
    }

    return 1;

}

sub index : Private {
    my ( $self, $c ) = @_;


    $self->_put_action_in_stash($c, "cash_us_tao_change_tao_euro");

    #and also the status of my account.
    my $res = $c->model('BcdModel')->get_my_cash_tao_summary($c);
    
    #you don't need to put it into the sack... because it is displayed
    #only in this page, for now

    $c->{stash}->{account_summary} = $res->{account_summary};
    
    $c->stash->{template} = "cash_user_tao.tt";
}


sub auto : Private {
    my ( $self, $c ) = @_;

    if ($self->valid_session_presence($c) == 0){
	return 0;
    }

    my $trails = $c->stash->{"trails"};
    my $an_id = $c->session->{current_ant_nest_id};
    my $name = $c->user->name();

    breadcrumbs::util::MenuUtils->add_trail("/antnestscontroller", "I Formicai", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/antnest/view_$an_id", "Formicaio $an_id", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/user", "$name", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/cashusertao", "c/c Tao", $trails, $c);

    return 1;
}

=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
