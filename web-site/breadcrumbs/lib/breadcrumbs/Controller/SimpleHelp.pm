package breadcrumbs::Controller::SimpleHelp;

use strict;
use warnings;
use breadcrumbs::Controller::BcController;
use base 'breadcrumbs::Controller::BcController';

=head1 NAME

breadcrumbs::Controller::SimpleHelp - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index 

=cut

sub index : Private {
    my ( $self, $c ) = @_;
    $c->stash->{template} = "simple_help.tt";
}

sub _make_menu{
    my ( $self, $c ) = @_;

    my $menu = $c->stash->{"menu_items"};;

    breadcrumbs::util::MenuUtils->add_menu_entry('prices', 'Tariffe', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('deposits_withdrawals', 'Depositi / versamenti', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('cheques', 'Assegni, fatture', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('activities_objects', 'Attività, oggetti', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('the_tao', 'Il Tao (&#964;)', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('trust', 'La fiducia', $menu, $c);
    breadcrumbs::util::MenuUtils->add_menu_entry('ant_nest', 'Il formicaio', $menu, $c);
}

sub prices : Local{
   my ( $self, $c ) = @_;
   $c->stash->{template} = "simple_help_prices.tt";
}

sub deposits_withdrawals : Local{
   my ( $self, $c ) = @_;
   $c->stash->{template} = "simple_help_deposits_withdrawals.tt";
}

sub cheques : Local{
   my ( $self, $c ) = @_;
   $c->stash->{template} = "simple_help_cheques.tt";
}

sub activities_objects : Local{
   my ( $self, $c ) = @_;
   $c->stash->{template} = "simple_help_activities_objects.tt";
}

sub the_tao : Local{
   my ( $self, $c ) = @_;
   $c->stash->{template} = "simple_help_the_tao.tt";
}

sub trust : Local{
   my ( $self, $c ) = @_;
   $c->stash->{template} = "simple_help_trust.tt";
}

sub ant_nest : Local{
   my ( $self, $c ) = @_;
   $c->stash->{template} = "simple_help_ant_nest.tt";
}

sub auto : Private {
    my ( $self, $c) = @_;

    my $trails = $c->stash->{"trails"};

    breadcrumbs::util::MenuUtils->add_trail("/help", "Aiuto", $trails, $c);
    breadcrumbs::util::MenuUtils->add_trail("/simplehelp", "Le schede", $trails, $c);

    return 1;
}




=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
