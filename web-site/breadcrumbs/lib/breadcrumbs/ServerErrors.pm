package breadcrumbs::ServerErrors;

# This file is part of the breadcrumbs site
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use Bcd::Errors::ErrorCodes;

#this file should simply make a translation from error codes to human
#error strings;


our %error_messages = 
    (

#   3
     Bcd::Errors::ErrorCodes::BEC_UNAUTHORIZED => 
     "Non sei autorizzato a fare questa azione, forse la tua sessione è scaduta, ricollegati, per favore.",
#   4
     Bcd::Errors::ErrorCodes::BEC_INVALID_TOKEN =>
     "non ho trovato questo gettone, controlla meglio.",
#   5
     Bcd::Errors::ErrorCodes::BEC_INVALID_TOTEM =>
     "Il totem che mi hai dato non è corretto.",

#   7
     Bcd::Errors::ErrorCodes::BEC_INVALID_STATE =>
     "Stato non valido.",

#100-200
     Bcd::Errors::ErrorCodes::BEC_RESERVED_ID => 
     "questo nick è riservato, per favore cambialo.",

# 100
     Bcd::Errors::ErrorCodes::BEC_UNKNOWN_USER =>
     "Non esiste questo utente in questo formicaio. Ricontrolla per favore",

# 101
     Bcd::Errors::ErrorCodes::BEC_INVALID_PASSWORD =>
     "Utente o password sbagliata",

# 102
     Bcd::Errors::ErrorCodes::BEC_USER_ALREADY_EXISTING => 
     "questo nick esiste già, per favore scegline un altro.",

# 106
     Bcd::Errors::ErrorCodes::BEC_LOGIN_DENIED_ANT_NEST_NOT_READY => 
     "questo formicaio non è ancora pronto, aspetta per favore.",

# 110
     Bcd::Errors::ErrorCodes::BEC_ACCOUNT_NOT_READY =>
     "Il tuo account non è ancora attivo, forse devi confermarlo o attendere i tutori",

# 111 
     Bcd::Errors::ErrorCodes::BEC_THE_TWO_TUTORS_MUST_BE_DIFFERENT =>
     "i due tutori non possono essere la stessa persona.",
# 113
     Bcd::Errors::ErrorCodes::BEC_NEW_USER_ALREADY_CONFIRMED =>
     "questo utente è già stato confermato, sto aspettanto che attivi il suo account.",

# 115
     Bcd::Errors::ErrorCodes::BEC_CANNOT_CHANGE_PASSWORD_FOR_TEST_USERS =>
     "Non puoi cambiare la password per gli utenti di test, altrimenti come fanno gli altri?",

# 201
     Bcd::Errors::ErrorCodes::BEC_PENDING_BOOKING_PRESENT =>
     "Hai già una richiesta di deposito o ritiro in corso. Non ne puoi avere due...",

# 203
     Bcd::Errors::ErrorCodes::BEC_DEPOSIT_TOO_BIG =>
     "Vuoi fare un deposito troppo grande; dividilo in più depositi più piccoli.",

# 204
     Bcd::Errors::ErrorCodes::BEC_UNKNOWN_DEPOSIT =>
     "Non hai un deposito in corso, stai forse cercando di spendere due volte un gettone?",

# 211
     Bcd::Errors::ErrorCodes::BEC_WITHDRAWAL_WILL_RENDER_NEGATIVE_THE_ACCOUNT =>
     "Stai cercando di ritirare più soldi di quelli che hai. Non puoi andare in rosso.",

# 215
     Bcd::Errors::ErrorCodes::BEC_INSUFFICIENT_FUNDS =>
     "Non hai abbastanza tao sul conto.",

# 300
     Bcd::Errors::ErrorCodes::BEC_INVALID_POST_CODE =>
     "Codice postale non valido (deve essere o 5 o 7 cifre)",

# 404
     Bcd::Errors::ErrorCodes::BEC_TRYING_TO_CREATE_ANT_OUTSIDE_YOUR_NEST =>
     "Non si può creare o confermare  una formica all'esterno del proprio formicaio.",

# 405
     Bcd::Errors::ErrorCodes::BEC_NO_ANT_NEST_IN_THIS_CODE =>
     "Non ho trovato un formicaio con il codice che mi hai dato.",

# 411
     Bcd::Errors::ErrorCodes::BEC_THIS_ANT_NEST_HAS_BEEN_CREATED =>
     "Questo formicaio non è più in costruzione, devi usare il suo codice nuovo per entrare.",

# 501
     Bcd::Errors::ErrorCodes::BEC_USER_MUST_BE_EXISTING_IN_SAME_ANT_NEST =>
     "Non ho trovato questo utente nel tuo formicaio.", 

# 501
     Bcd::Errors::ErrorCodes::BEC_DUPLICATE_TRUST_NOT_ALLOWED =>
     "Ti fidi già di questa persona.", 

# 505
     Bcd::Errors::ErrorCodes::BEC_DUPLICATE_BOOKING =>
     "Hai già prenotato la fiducia per questa persona.", 

# 805
     Bcd::Errors::ErrorCodes::BEC_DUPLICATE_AD_LOCALITY =>
     "Hai già aggiunto questa presenza per questo annuncio: forse intendevi modificarla.", 

# 807
     Bcd::Errors::ErrorCodes::BEC_CANNOT_EDIT_ACTIVE_PRESENCE =>
     "Hai delle fatture attive su questa presenza, non puoi modificarla", 

#9998
     Bcd::Errors::ErrorCodes::BEC_ABORTED_TRANSACTION =>
     "C'è stato un errore generico nel database, se si ripete contatta l'autore.",

     );






1;
