package breadcrumbs::authentication::bcuser;

# This file is part of the breadcrumbs site
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

use base qw{Catalyst::Plugin::Authentication::User};

#These are the same in the database... maybe I could ask the database
#the possible roles.
use constant{
    ANT_ROLE       => 1,
    BOSS_ROLE      => 2,
    TRESAURER_ROLE => 4,
    FOUNDER_ROLE   => 8,
};

sub new {
    my $class = shift;
    my $self = {};
    my $id = shift;

    $self->{"id"} = $id;
    $self->{"c"} = shift; #I store the context... it is needed to make the login

    bless($self, $class);
    return $self;
};

sub id {
    my $self = shift;
    return $self->{"id"};
}

sub name {
    my $self = shift;
    my $id = $self->{id};
    my @arr = split /\./,$id;
    my $name = $arr[0];
    return $name;
}

sub ant_nest{
    my $self = shift;
    my $id = $self->{id};
    my @arr = split /\./,$id;
    my $ant_nest = $arr[1];
    return $ant_nest;
}

#this is used to get the profile from the database...
sub my_profile{
    my $self = shift;

    if ( ! exists($self->{my_profile})){
	#get the profile from the db
	my $res = breadcrumbs->model("BcdModel")->get_my_profile($self->{server_session});

	if ( $res->{exit_code} != Bcd::Errors::ErrorCodes::BEC_OK){
	    $self->{last_error} = $res->{exit_code};
	    return undef;
	}

	$self->{my_profile} = $res->{user_details};
	$self->{my_profile}->{missing_trusts} = $res->{missing_trusts};
	$self->{my_profile}->{personal_data_trusts} = $res->{personal_trusts};
    }

    return $self->{my_profile};
}

sub my_trust_net{
    my $self = shift;

    #let's get the trust net from db
    my $time = defined($self->{net_time}) ? $self->{net_time} : 0;

    my $res = breadcrumbs->model("BcdModel")->get_my_trust_net($self->{server_session}, $time);

    my $exit_code = $res->{exit_code};
    $self->{last_error} = $exit_code;

    if ($exit_code == Bcd::Errors::ErrorCodes::BEC_OK){

	#ok, I have an updated net...
	$self->{my_net} = $res->{net};
	$self->{net_time} = $res->{net_time};
	return (0, $self->{my_net});

    } elsif ($exit_code == Bcd::Errors::ErrorCodes::BEC_DATA_NOT_CHANGED ){
	#I can return my old net...
	return (0, $self->{my_net});
    }
    
    #ok, there is no data... or the data is outdated...
    if (exists($self->{my_net})){
	return (2, $self->{my_net});
    } else {
	return (1, undef); #no data...
    }
}

sub outdate_profile{
    my $self = shift;
    delete $self->{my_profile};
}

#This is our store... ?!
sub store 
{ 
    return "breadcrumbs::authentication::bcstore";
}

sub for_session{
    my $self=shift;
    delete ($self->{"c"}); #I don't want to save the context....
    return $self;
}

sub supported_features {
    my $self = shift;

    return {
        password => {
	    self_check => 1,
	},
	session => 1,
	roles => {
	    self_check => 1,
	},
    };
}

#this method should self check the roles
sub check_roles{
    my ($self, @roles) = @_;

    for(@roles){
	if ($_ eq 'boss'){
	    if (!($self->{"role"} & BOSS_ROLE)){
		return 0; #immediate fail return
	    }
	} elsif ($_ eq 'treasurer'){
	    if (!($self->{"role"} & TRESAURER_ROLE)){
		return 0;
	    }
	} elsif ($_ =~ /home_ant_(\d+)/ ){
	    if (!($self->{"role"} & ANT_ROLE)){
		return 0;
	    }
	    
	    #HACK HACK: I don't have the context here, so I pass the id of the ant in the role

	    #I should check the current_ant_nest_id, which has been passed in the role
	    if ($self->{"id"} !~ /$1$/){
		return 0;
	    }

	} elsif ($_ =~ /guest_ant_(\d+)/ ){

	    if ($self->check_roles("home_ant_$1")){
		#You are an home ant, so you cannot be a guest ant
		return 0
	    }

	} elsif ($_ eq "founder"){
	    if (!($self->{role} & FOUNDER_ROLE)){
		return 0;
	    }
	} else {
	    die "unknown role";
	}
    }

    return 1; #all ok
    
}

#This method should be called by the credential verifier
sub check_password{
    my ($self, $pwd) = @_;

    #ok, I should try to reach the server 
    #with the id and the passwords.

    my $c = $self->{"c"};

    my $id = $self->{"id"};

    my $nick;
    my $ant_nest;
    
    my @arr = split /\./,$id;
    
    $nick = $arr[0];
    $ant_nest = $arr[1];
    
#     my ($exit_code, $id_session, $role) = $c->model('BcdModel')->
# 	login($nick, $ant_nest, $pwd);
    my $hash = $c->model('BcdModel')->login($nick, $ant_nest, $pwd);

    if ($hash->{"exit_code"} == 0){
	#ok, I should store the session...
	$self->{"server_session"} = $hash->{"session_id"};
	$self->{"role"} = $hash->{"role"};
	return 1;
    } else {
	$self->{reason_for_not_login} = $hash->{exit_code};
	return 0;
    }
    
}

#this method should logout from the server
sub logout{
    my ($self, $c) = @_;
    my $code = $c->model('BcdModel')->logout($self->{"server_session"});
    return $code;
}

1;
