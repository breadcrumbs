package breadcrumbs::authentication::bcstore;

# This file is part of the breadcrumbs site
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;

#use Catalyst::Plugin::Authentication::User::Hash;
use breadcrumbs::authentication::bcuser;

#this should be a simple store... used to access the bcd users in the daemon.

sub new{
    my $class = shift;
    
    my $self = {};
    bless($self, $class);
    
    return $self;
}


sub get_user{
    my ($self, $id, $c) = @_;
    my $user = breadcrumbs::authentication::bcuser->new($id, $c);
    return $user;
}

sub from_session {
	my ( $self, $c, $id ) = @_;

	return $id if ref $id;

	$self->get_user( $id );
}

1;
