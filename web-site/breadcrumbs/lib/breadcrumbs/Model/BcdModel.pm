package breadcrumbs::Model::BcdModel;

# This file is part of the breadcrumbs site (in Catalyst)
# Copyright (C) 2007 Pasqualino Ferrentino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

# Contact: lino.ferrentino@yahoo.it (in Italian, English or German).

use strict;
use warnings;
use base 'Catalyst::Model';

use IO::Socket; 
use Data::Dumper;
use MIME::QuotedPrint;
use MIME::Base64;
use Compress::Zlib;
use Storable qw(freeze thaw);

use constant EOL => "\015\012";
#$/ = "\015\012";

#use lib '../../src/server';
use Bcd::Commands::CommandParser;
use Bcd::Constants::ActivitiesConstants;

sub new{
    my $class = shift;
    my $self = $class->SUPER::new(@_);


    bless($self, $class);
    
    #$self->_create_the_connection();

    return $self;
}

#this function should get a safe sock, if the socket is not good it will
#return an error.
sub _get_safe_sock{
    my $self = shift;


    if ( ! exists $self->{"sock"}){

	my $port = breadcrumbs->config()->{server}->{port};
	my $peer = breadcrumbs->config()->{server}->{addr};

	my $sock = new IO::Socket::INET ( 
					  PeerAddr => $peer, 
					  PeerPort => $port, 
					  Proto => 'tcp', 
					  );

	#more error handling, maybe...
	#die "Sockect could not be estabilished: $!\n" unless $sock; 

	if ($sock){
	    local $/ = "\015\012";
	    #I store the socket for future use...
	    $self->{"sock"} = $sock;

	    #get rid of the greeting
	    my $greeting = <$sock>;
	    
	    my $ready_for_commands = <$sock>;

	    #I want binary mode, please
	    print $sock '\b' . EOL;
	    my $ans = <$sock>;
	} else {
	    #no socket, no party
	    return undef;
	}
    } 

    #ok, let's return the socket...
    return $self->{"sock"};

}

sub DESTROY{
    my $self = shift;
    $self->_clean_last_socket();
}

sub login{
    my ($self, $user, $ant_nest, $pass) = @_;
    return $self->_issue_command_to_daemon("sv_connect", $user, $ant_nest, $pass);
}

sub logout{
    my ($self, $session) = @_;
    return $self->_issue_command_to_daemon("sv.disconnect", $session);
}

sub get_ant_nest_detail{
    my ($self, $post_code) = @_;
    return $self->_issue_command_to_daemon("an.get_detail", $post_code);
}

sub get_ant_nests_from_post_code{
    my ($self, $post_code) = @_;
    return $self->_issue_command_to_daemon("an.lfpc", $post_code);
}

sub get_new_user_details{
    my ($self, $c, $token) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("an.get_new_user_details", $session, $token);
}

sub get_user_details_from_totem{
    my ($self, $c, $nick, $totem) = @_;

    my $session = $c->user->{server_session};

    return $self->_issue_command_to_daemon
	("an.get_user_details", 
	 $session,
	 $nick, 
	 $totem);
    
}

sub new_trust{
    my ($self, $c, $nick, $totem, $trust) = @_;

    my $session = $c->user->{server_session};

    return $self->_issue_command_to_daemon
	("tr.new_trust", 
	 $session,
	 $nick, 
	 $totem,
	 $trust);
    
}

sub get_trusted_ants{
    my ($self, $c) = @_;
    my $session = $c->user->{server_session};
    
    return $self->_issue_command_to_daemon
	("tr.get_trusted_ants",
	 $session);
}

#this is not more used
# sub get_reachable_set{
#     my ($self, $c, $trust) = @_;
#     my $session = $c->user->{server_session};
        
#     return $self->_issue_command_to_daemon
# 	("tr.get_reachable_set",
# 	 $session,
# 	 $trust);
# }


sub get_my_trust_net{
    my ($self, $session, $time) = @_;

    return $self->_issue_command_to_daemon
	("tr.get_user_nets", $session, $time);
    
}

sub get_tutors_details{
    my ($self, $token) = @_;

    return $self->_issue_command_to_daemon
	("an.get_tutors_details", 
	 $token);
}

sub confirm_new_user{
    my ($self, $c, $token, $trust) = @_;

    my $session = $c->user->{server_session};

    return $self->_issue_command_to_daemon
	("an.confirm_new_user", 
	 $session,
	 $token,
	 $trust);
}

sub change_trust {
    my ($self, $c, $nick, $trust) = @_;

    my $session = $c->user->{server_session};

    return $self->_issue_command_to_daemon
	("tr.change_trust", 
	 $session,
	 $nick,
	 $trust);
    
}

sub commit_trust_changes {
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};

    return $self->_issue_command_to_daemon
	("tr.commit_trust_change", 
	 $session);
}

sub rollback_trust_changes {

    my ($self, $c) = @_;

    my $session = $c->user->{server_session};

    return $self->_issue_command_to_daemon
	("tr.rollback_change_trust", 
	 $session);
}

sub confirm_my_tutors{
    my ($self, $token, $trust1, $trust2) = @_;

    return $self->_issue_command_to_daemon
	("an.confirm_my_tutors", 
	 $token,
	 $trust1,
	 $trust2);
}

sub get_ant_nests_summary{
    my $self = shift;

    return $self->_issue_command_to_daemon("an_get_summary");
}

sub create_normal_user{
    my ($self, $c) = @_;

    my $post_code = $c->user->ant_nest();
    my $session = $c->user->{server_session};
    my $req = $c->req;

    my $year = $req->param('year');
    my $month = $req->param('month');
    my $day = $req->param('day');

    my $date = "$year-$month-$day";
    my $trust = $req->param('trust_first') / 100.0;
    my $sex = $req->param('sex');

    $c->log->debug("date is $date, trust is $trust, sex is $sex\n");

    return $self->_issue_command_to_daemon
	("an.create_normal_user", 
	 $session,
	 $post_code,
	 $req->param('nick'),
	 $req->param('email'),
	 $req->param('first_name'),
	 $req->param('last_name'),
	 $req->param('address'),
	 $req->param('home_phone'),
	 $req->param('mobile_phone'),
	 $sex,
	 $date,
	 $req->param('document_number'),
	 $trust,
	 );
}

sub get_nearest_ant_nests {
    my ($self, $id) = @_;
    return $self->_issue_command_to_daemon("an_get_nearest_ant_nest", $id);
}

sub get_new_ant_nest_detail{
    my ($self, $id) = @_;
    return $self->_issue_command_to_daemon("na_details", $id);
}

sub create_new_founder {
   my ($self, $id_ant_nest, $name_ant_nest, $nick, $email, $password, $comment) = @_;


   my $name;

   $name = "an_create_new_founder";

   #I eliminate the "line feed" characters from the comment
   $comment =~ s/\015//sg;

   return $self->_issue_command_to_daemon
       ($name, $id_ant_nest, $name_ant_nest, $nick, $email, $password, $comment);
}

sub send_message_to_founders{
    my ($self, $c, $subject, $message) = @_;

    $message =~ s/\015//sg;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("na.send_message_to_founders", $session, 
					   $subject, $message);
}

sub create_founder_hq {
    my ($self, $c, $name, $loc, $dir, $tr, $op_hours, $on_request, $av_hours) = @_;

    $dir =~ s/\015//sg;
    #then I take the session...
    my $session = $c->user->{server_session};
    
    return $self->_issue_command_to_daemon
	("na.create_hq", $session, $name, $loc, $dir, $tr, $op_hours, $on_request, $av_hours);
    
}

sub initial_treasurer_set_up{
    my ($self, $c, $dep_online, $dep_in_hq, $name, $loc, $dir, $tr, $op_hours, $on_request, $av_hours) = @_;

    $dir =~ s/\015//sg;
    #then I take the session...
    my $session = $c->user->{server_session};
    
    return $self->_issue_command_to_daemon
	("na.treasurer_setup", $session, $dep_online, $dep_in_hq, $name, $loc, $dir, $tr, $op_hours, $on_request, $av_hours);

}

sub treasurer_offline_acknowledged{
    my ($self, $c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.treasurer_offline_acknowledge", $session);
}

sub get_ant_nest_hq{
    my ($self, $id_ant_nest) = @_;

    return $self->_issue_command_to_daemon("an_get_hq", $id_ant_nest);
}

sub create_new_looker{
    my ($self, $id_ant_nest, $name_ant_nest, $nick, $email) = @_;

   return $self->_issue_command_to_daemon
       ("an_create_new_looker", $id_ant_nest, $name_ant_nest, $nick, $email);
}

sub founder_confirm{
    my ($self, $id_ant_nest, $nick, $token) = @_;
    return $self->_issue_command_to_daemon("na_confirm_founder", $id_ant_nest, $nick, $token);
}

sub get_new_ant_nests_summary{
    my $self = shift;
    return $self->_issue_command_to_daemon("na_get_summary");
}

sub get_my_profile{
    my ($self, $session)= @_;
    return $self->_issue_command_to_daemon("us_get_my_profile", $session);
}

sub update_founder_personal_data{
    my ($self, $c, @args) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("na_update_founder_personal_data", $session, @args);
    
}

sub get_founders_summary{
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("na_get_founders_summary", $session);
}

sub get_founder_profile{
    my ($self, $c, $id_other) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("na_get_founder_profile", $session, $id_other);
}

sub create_founder_trusts{
    my ($self, $c, @params) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("na_create_founder_trusts", $session, @params);
}

sub update_founder_flags{
    my ($self, $c, $boss_flag, $treasurer_flag) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("nu_update_candidates_flags", $session, $boss_flag, $treasurer_flag);
}

sub get_candidates_summary{
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("na_get_candidates_summary", $session);
}

sub add_founder_vote{
    my ($self, $c, $boss_vote, $treasurer_vote) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("fv_create_voting", $session, $boss_vote, $treasurer_vote);
}

sub create_deposit_request{
    my ($self, $c, $euro_amount) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.create_deposit_booking", $session, $euro_amount);
}

sub create_withdrawal_request{
    my ($self, $c, $euro_amount) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.create_withdrawal_booking", $session, $euro_amount);
}

sub get_deposit_details{
    my ($self, $c, $booking_token) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.lookup_deposit", $session, $booking_token);
}

sub get_withdrawal_details{
    my ($self, $c, $booking_token) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.lookup_withdrawal", $session, $booking_token);
}

sub lookup_my_booking{
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.lookup_my_booking", $session);
}

sub cancel_my_booking{
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.delete_my_booking", $session);
}

sub get_my_cash_euro_summary{
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("us.get_summary_euro_cash", $session);
}

sub get_my_cash_tao_summary{
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("us.get_summary_tao_cash", $session);
}

sub get_ant_nest_cash_euro_summary{
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("an.get_summary_euro_cash", $session);
}

sub get_ant_nest_bookings_summary{
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("an.get_summary_bookings", $session);
}

sub treasurer_acknowledged{
    my ($self, $c, $booking_id) = @_;
   
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.treasurer_acknowledged", $session, $booking_id);
}

sub claim_deposit{
    my ($self, $c, $secret) = @_;
   
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.claim_deposit", $session, $secret);
}

sub claim_withdrawal{
    my ($self, $c, $id_booking, $secret) = @_;
   
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.claim_withdrawal", $session, $id_booking, $secret);
}

sub change_euro_in_tao{
    my ($self, $c, $amount) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.change_euro_in_tao", $session, $amount);
}

sub change_tao_in_euro{
    my ($self, $c, $amount) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("bk.change_tao_in_euro", $session, $amount);
}

sub get_other_ant_profile{
    my ($self, $c, $other_ant_id) = @_;
   
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("us.get_other_ant_profile", $session, $other_ant_id);
}

sub get_services_tree{
    my $self = shift;
    return $self->_issue_command_to_daemon("ac.get_tree", 0);
}

sub get_used_tree{
    my $self = shift;
    return $self->_issue_command_to_daemon("ac.get_tree", 2);
}

sub get_homemade_tree{
    my $self = shift;
    return $self->_issue_command_to_daemon("ac.get_tree", 1);
}

sub insert_personal_users_data_trusts{
    my ($self, $c, @args) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("us_insert_personal_data_trusts", $session, @args);
}

sub change_personal_data_trust{
    my ($self, $c, @args) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("us_change_personal_data_trust", $session, @args);
}

sub get_public_sites{
    my ($self, $c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("an_get_public_sites", $session);
}

sub get_presences{
    my ($self, $c, $id_site, $id_special_site, $id_rel_type) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("si.get_presences", $session, $id_site, $id_special_site, $id_rel_type);
}

sub get_price_for_ad{
    my ($self, $c, $p_min, $p_max, $t_e, $t_c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws.get_price_for_ad", $session, $p_min, $p_max, $t_e, $t_c);
}

sub create_presence{
    my ($self, $c, @args) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("si.create_presence", $session, @args);
    
}

sub create_ad{
    my ($self, $c, $id_act, $text, $locus, $p_min, $p_max, $t_e, $t_c) = @_;

    $text =~ s/\015//sg;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws.create_ad", $session, $id_act, $text, $locus, $p_min, $p_max, $t_e, $t_c);
}

sub get_my_ads{
    my ($self, $c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("ws.get_my_ads", $session);
}

sub get_services_tree_with_summary{
    my ($self, $c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("ac.get_summary_tree", 
					   $session, 
					   Bcd::Constants::ActivitiesConstants::PARENT_SERVICES);
}

sub get_objects_tree_with_summary{
    my ($self, $c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("ac.get_summary_tree", 
					   $session, 
					   Bcd::Constants::ActivitiesConstants::PARENT_USED_ITEMS);
}

sub get_homemade_tree_with_summary{
    my ($self, $c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("ac.get_summary_tree", 
					   $session, 
					   Bcd::Constants::ActivitiesConstants::PARENT_HOMEMADE);
}

sub get_ads_for_this_activity{
    my ($self, $c, $act_id) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("ws.get_ads", $session, $act_id);
}

sub get_ad{
    my ($self, $c, $ad_id, $ad_locus) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("ws.get_ad", $session, $ad_id, $ad_locus);
}

sub get_my_credit_report{
    my ($self, $c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon("cad_get_credit_report", $session);
}

sub get_price_estimate{
    my ($self, $c, $seller, $price, $can_use_credit) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("cad_get_price_estimate", $session, $seller, $price, $can_use_credit);
}

sub buy_object{
    my ($self, $c, $ad_id, $ad_locus) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws_buy_object", $session, $ad_id, $ad_locus);
}

sub get_invoices_summary{
    my ($self, $c) = @_;

    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws_get_invoices_summary", $session);
}

sub get_invoices_list{
    my ($self, $c, $invoice_type, $invoice_state) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws_get_invoices_list", $session, $invoice_type, $invoice_state);
}

sub get_invoice_blinded_token{
    my ($self, $c, $invoice_id) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws_get_invoice_blinded_token", $session, $invoice_id);
}

sub collect_invoice{
    my ($self, $c, $invoice_id, $secret) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws_collect_invoice", $session, $invoice_id, $secret);
}

sub authorize_invoice{
    my ($self, $c, $ad_id, $ad_locus) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws_authorize_invoice", $session, $ad_id, $ad_locus);
}

sub emit_invoice{
    my ($self, $c, $invoice_id, $quantity) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws_emit_invoice", $session, $invoice_id, $quantity);
}

sub get_price_estimate_for_invoice{
    my ($self, $c, $invoice_id) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("cad_get_price_estimate_for_invoice", $session, $invoice_id);
}

sub pay_invoice{
    my ($self, $c, $invoice_id) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws_pay_invoice", $session, $invoice_id);
}

sub change_password{
    my ($self, $c, $old_password, $new_password) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("us_change_password", $session, $old_password, $new_password);
}

sub change_totem{
    my ($self, $c, $new_totem) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("us_change_totem", $session, $new_totem);
}

sub get_my_ad_details{
    my ($self, $c, $ad_id) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws.get_my_ad_details", $session, $ad_id);
}

sub add_ad_presence{
    my ($self, $c, $ad_id, $id_locus, $p_min, $p_max, $t_e, $t_c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws.add_locus_to_ad", $session, $ad_id, $id_locus, $p_min, $p_max, $t_e, $t_c);
}

sub get_site_details{
    my ($self, $c, $id_site) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("an.get_site_details", $session, $id_site);
    
}

sub get_invoice_details{
    my ($self, $c, $invoice_id) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws_get_invoice_details", $session, $invoice_id);
}

sub get_user_presences_summary{
    my ($self, $c) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("si.get_user_presences_summary", $session);
}

sub get_ads_for_locus{
    my ($self, $c, $id_locus) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws.get_ads_in_locus", $session, $id_locus);
}

sub write_to_boss{
    my ($self, $id_ant_nest, $subject, $comment) = @_;
    $comment =~ s/\015//sg;

    $self->_issue_command_to_daemon
	("an.write_to_boss", $id_ant_nest, $subject, $comment);
}

sub get_similar_presences_in_space{
    my ($self, $c, $id_locus) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("si.get_similar_presences", $session, $id_locus, "space");

}

sub move_ads_presence{
    my ($self, $c, $id_locus_old, $id_locus_new) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws.move_ads_user_presence", $session, $id_locus_old, $id_locus_new);
}

sub change_site_presence{
    my ($self, $c, $id_locus_old, $id_site_new, $id_special_site) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws.change_site_in_presence", $session, $id_locus_old, $id_site_new, $id_special_site);
}

sub edit_user_presence{
    my ($self, $c, $id_locus_old, $on_request, $presence) = @_;
    my $session = $c->user->{server_session};
    return $self->_issue_command_to_daemon
	("ws.edit_user_presence", $session, $id_locus_old, $on_request, $presence);
}

sub _dead_socket{
    my $self =  shift;
    $self->_clean_last_socket();
    Catalyst::Exception->throw("dead socket");
}

sub _clean_last_socket{
    my $self =  shift;
    #I should simply close the socket
    close delete $self->{"sock"} if $self->{"sock"};; #the socket is useless now
}


#this method should simply connect to the daemon
sub _issue_command_to_daemon{
    my ($self, $command, @args) = @_;

    my $sock = $self->_get_safe_sock();
    return $self->_dead_socket if (!defined($sock));


    #first of all I issue the command
    print $sock $command . EOL or $self->_dead_socket();

    #I simply freeze the input array
    my $dumped_u = freeze \@args;
    my $must_compress = 0;
    my $output_serialized;

    #a reasonable limit
    my $mode;
    if (length($dumped_u) > 200){
	$must_compress = 1;
	$mode = "c";
    } else {
	$mode = "u";
    }

    if ($must_compress == 1){
	my $dumped_c = Compress::Zlib::memGzip($dumped_u);
	$output_serialized = encode_base64($dumped_c, EOL);
    } else {
	$output_serialized = encode_base64($dumped_u, EOL);
    }

    $output_serialized =~ s/^(\.?)/$1$1/sg; #also at the start of the string...
    $output_serialized =~ s/\015?\012(\.?)/\015\012$1$1/sg;

    print $sock $mode . EOL          or $self->_dead_socket();
    print $sock $output_serialized   or $self->_dead_socket();
    print $sock "." . EOL            or $self->_dead_socket();

    #then the answer...
    #my $code = <$sock>;
    #chomp $code;
    #push (@output, $code);

    #the first line is the interpretation of the stream
    my $mode_line = <$sock>;
    my $compressed = 0;

    if ($mode_line =~ /c/){
	$compressed = 1;
    } elsif ($mode_line =~ /u/){
	$compressed = 0;
    } else {
	die "unknown mode!";
    }

    my $output = "";
    while(1){
	local $/ = "\015\012";
	my $answer = <$sock>;
	if ( !defined ($answer)){
	    #something wrong with the socket
	    $self->_dead_socket();
	}
	chomp $answer;
	if ($answer =~ /^\.$/){
	    last;
	} elsif ( $answer =~ /^\.?/){
	    #remove the initial dot, if it is present...
	    $answer =~ s/^\.(.*)/$1/sg;
	}

	#push (@output, $answer);
	$output .= $answer;
    }

    #ok, I should thaw the answer...

    my $decoded = decode_base64($output);

    my $stream;
    if ($compressed == 1){
	#ok, I should decompress it!
	$stream = Compress::Zlib::memGunzip($decoded);
    } else {
	$stream = $decoded;
    }

    #my $res = eval("no strict; $output");
    my $res = thaw $stream;

    #ok, all done, let's parse the output
    my $hash_ref = Bcd::Commands::CommandParser->parse_command_output($res);
    return $hash_ref;

}


=head1 NAME

breadcrumbs::Model::BcdModel - Catalyst Model

=head1 DESCRIPTION

Catalyst Model.

=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
