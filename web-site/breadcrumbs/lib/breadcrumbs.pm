package breadcrumbs;

use strict;
use warnings;

use Catalyst::Runtime '5.70';
use YAML;


# Set flags and add plugins for the application
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a YAML file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root 
#                 directory

#use Catalyst qw//;
    #-Debug 
use Catalyst qw/

    ConfigLoader 
    Static::Simple
             
    Session
             
    Session::Store::FastMmap
             
    Session::State::Cookie

    Authentication
    Authentication::Credential::Password

    Authorization::Roles
    HTML::Widget
    /;

our $VERSION = '0.01';

# Configure the application. 
#
# Note that settings in breadcrumbs.yml (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with a external configuration file acting as an override for
# local deployment.

#__PACKAGE__->config( name => 'breadcrumbs' );
__PACKAGE__->config(YAML::LoadFile(__PACKAGE__->config->{'home'} . '/breadcrumbs.yml'));

if (__PACKAGE__->config()->{debug_mode} eq "true"){
    __PACKAGE__->setup( qw/-Debug/);
} else {
    __PACKAGE__->setup();
}


=head1 NAME

breadcrumbs - Catalyst based application

=head1 SYNOPSIS

    script/breadcrumbs_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<breadcrumbs::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Pasqualino Ferrentino,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
