use strict;
use warnings;
use Test::More tests => 3;

BEGIN { use_ok 'Catalyst::Test', 'breadcrumbs' }
BEGIN { use_ok 'breadcrumbs::Controller::Help' }

ok( request('/help')->is_success, 'Request should succeed' );


