use strict;
use warnings;
use Test::More tests => 2;

use Test::WWW::Mechanize::Catalyst 'breadcrumbs';

BEGIN { use_ok 'Catalyst::Test', 'breadcrumbs' }

ok( request('/')->is_success, 'Request should succeed' );
