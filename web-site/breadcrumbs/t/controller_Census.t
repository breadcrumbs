use strict;
use warnings;
use Test::More;

eval "use Test::WWW::Mechanize::Catalyst 'breadcrumbs'";
# plan $@
#     ? ( skip_all => 'Test::WWW::Mechanize::Catalyst required' )
#     : ( tests => 3 );

plan "no_plan";

ok( my $mech = Test::WWW::Mechanize::Catalyst->new, 'Created mech object' );

#I should go to home page
$mech->get_ok( 'http://localhost/census' );
$mech->content_contains("Benvenuta");

#I should not be able to see the census if there isn't an ant nest.
$mech->get( 'http://localhost/census/view_ant_nest_census' );
ok ($mech->status() == 500, "Mi aspettavo un 500");

#let's select a ant nest
$mech->get_ok( 'http://localhost/antnest/view_8012800' );
$mech->content_contains("Formicaio 8012800");

#ok, let's return to the census page
$mech->get_ok( 'http://localhost/census/view_ant_nest_census' );
$mech->content_contains("o non sei un boss");

