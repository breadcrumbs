[% META title="Le Bricioline, concetti base: l'artigianato" %]

<h3>Che buono, mi dai la ricetta?</h3>

<p>Una delle cose più importanti nell'amicizia femminile è lo scambio
di informazioni e di favori. Le donne parlano molto e, parlando, si
aiutano molto fra di loro sia fisicamente (con lavori) sia
scambiandosi dritte, consigli, e, in questo caso, ricette.</p>

<p>È proprio grazie all'iniziativa di donne che si sono avuti in
Italia i primi esperimenti di Banche del Tempo. Le Bricioline vuole
prendere spunto da questi esperimenti per proporre un sistema "a tutto
tondo" che possa riguardare tutti, o quasi, gli aspetti dello scambio
di favori.</p>

<p>Una cosa che non è possibile fare negli altri siti di annunci, e lo
è invece qui, è la vendita di oggetti autoprodotti all'interno dello
stesso formicaio. Anche in questo caso questo viene reso possibile
mediante l'uso della fiducia fra le formiche.</p>

<p>Oramai abbiamo imparato a giocare con la fiducia, proviamo a farlo
questa volta con una torta.</p>

<h3>Chi ha fatto questa torta?</h3>

<p>Avete l'hobby delle torte e vi fate una torta e ve la mangiate. Ah,
che soddisfazione. Questo è il primo passo.</p>

<p>Vostra moglie fa una torta. Probabilmente la mangiate, perché vi
fidate di quello che ci ha messo dentro (se avete litigato il giorno
prima magari un po' di dubbio lo avete, ma comunque la provate lo
stesso). "Buona, brava". Questo è il secondo passo. Una persona di cui
vi fidate fa un qualcosa e voi trasferite la fiducia che avete dalla
persona alla cosa fatta dalla persona e la mangiate.</p>

<p>Un altro giorno vostra moglie vi porta una fetta di torta. "L'ha
portata una mia collega oggi in ufficio, te ne ho portato una fetta
avanzata... so che ti piace". Terzo passo, un'amica di una persona di
fiducia ha fatto una torta. Vi fidate? La mangiate?
</p>

<p>E così, via, potrebbe essere un'amica di un'amica di un'amica... ad
un certo punto, poi, qualcuno di voi direbbe: "no, non mi fido, non so
chi l'ha fatta, non la mangio". Anche in questo caso il limite è
soggettivo. E dipende anche da quello che si è prodotto: per esempio
un risotto ai funghi è più rischioso di una torta; come anche un
barattolo di verdure sott'olio, saranno state sterilizzate bene?
Avranno il botulino? Magari fatte da nostra moglie le mangiamo, fatte
da un'amica anche, da un'amica di un'amica già un po' meno... "no,
grazie, oggi non mi va...".
</p>

<p>Nelle Bricioline è possibile vendere anche generi alimentari fatti
in casa od anche lavoretti fatti in casa (un maglione, un centrino o
altro), sempre perché le Bricioline tiene traccia dei cammini di
fiducia, e siamo noi a decidere se fidarci o meno di quella persona
che sappiamo essere distante da noi uno, due o più "passi" dalle
persone che direttamente consideriamo fidate (i nostri parenti, amici,
ecc...).
</p>

<h3>Conclusioni</h3>

<p>Le Bricioline aspirano a far emergere un commercio delle piccole
cose, basato sulla solidarietà e sulla fiducia. Ma senza utopie. Tutto
viene venduto con dei soldi, anche se non sempre veri, ma anche con
"assegni" che possono essere spesi all'interno del formicaio. Se
questa introduzione vi ha incuriosito potete provare a costruire un
formicaio. È tutto gratuito e non richiede grandi conoscenze. Una
volta costruito è tutto da esplorare. Potete cominciare a
familiarizzarvi con l'unità di misura della fiducia, il BricioBel e
poi, magari, cominciare a mettere degli annunci e a vendere e comprare.
</p>

<p>Le Bricioline può sembrare difficile al primo impatto, ma solo
perché cerca di mettere insieme varie cose: fiducia, prezzo,
località. Sono disponibile per eventuali modifiche e correzioni. Ogni
commento è chiaramente benvenuto.</p>
