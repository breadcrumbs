[% META title="Le Bricioline, concetti base: la località e l'uso" %]

<h2>Butto il vecchio vhs (computer, cellulare)?</h2>

<p>La situazione è un vissuto comune per molti: si compra il nuovo quando il
vecchio funziona ancora bene, però è obsoleto. I casi sono molti: da
VHS a DVD, da un cellulare senza fotocamera ad uno con fotocamera, da
un monitor antico ad un LCD. Oppure anche elettrodomestici: da un
frigo a una porta ad un frigo a due porte. Da una lavatrice a 800 giri
ad una a 1200. Da un ferro da stiro senza caldaia ad uno con
caldaia. Oppure si rompe qualche piatto ed il servizio è spaiato, però
gli altri piatti sono ancora buoni, invece che da sei si può fare un
servizio da tre.</p>

<p>Cosa si può fare con quello che rimane? La cosa migliore (da un
punto di vista economico e umano) sarebbe venderlo o comunque
<em>scambiarlo</em> con un altro oggetto o una promessa di un oggetto
(come vedremo). Gli usi "egoistici" (come tenerlo in cantina, o,
peggio, buttarlo), sono usi che tolgono qualcosa alla comunità, è come
tenere un libro chiuso a chiave, non serve a nessuno.</p>

<p>Ossia, per entrare nell'ottica delle Bricioline bisogna essere
d'accordo su una cosa: le cose sono fatte per essere usate, non per il
gusto "narcisistico" di possederle; finché funzionano è un danno
tenerle "ferme", sia per la Natura, sia per l'economia, sia per la
solidarietà... ecc... Scambiarle aumenta il loro ciclo di vita, e
quindi c'è meno bisogno di produzione di nuovo con minor impatto
ambientale.</p>

<h2>Ma a quale prezzo?</h2>

<p>Il problema dello scambiare le cose usate è che è difficile la
formazione del prezzo. Sia per ragioni "tecnologiche" che per ragioni
affettive.</p>

<p>Le ragioni tecnologiche sono evidenti: dopo qualche mese si trovano
delle cose nuove, migliori, allo stesso prezzo, o minore, di quello
che è costato l'oggetto in questione che quindi esce dal mercato
perché dovrebbe costare un prezzo sempre più basso, al limite
regalato.
</p>

<p>Le ragioni affettive sono anch'esse evidenti: è difficile rendersi
conto che il nuovo avanza e certe volte si tende a sovrastimare il
valore del proprio oggetto perché lo si carica di ricordi, o
semplicemente perché si ignora il progresso fatto nel frattempo.
</p>

<h2>L'euro è fatto per le cose nuove, non usate</h2>

<p>La constatazione delle Bricioline è che l'euro è una moneta che va
bene per le cose nuove, non usate. Per le cose usate ci vuole una
moneta che valga "meno" dell'euro; certo, ci sono i centesimi, ma non
bastano ancora, ci vogliono i millesimi di Euro, e magari anche
qualcosa in meno. La risposta è che c'è sempre un prezzo per ogni
cosa, ma questo prezzo può essere non misurabile comodamente in Euro,
e quindi non viene venduto quell'oggetto non perché non ha compratori,
ma perché è "difficile" la formazione di un prezzo in Euro
soddisfacente per entrambi. Questo avviene per un meccanismo
psicologico, esattamente uguale a quello che ha causato molti rialzi
dei prezzi nel 2002, quando 1 Euro si pose uguale a 1000 Lire e non
2000 e molti prezzi raddoppiarono senza ragione.</p>

<p>È come se voleste misurare l'altezza di una persona in kilometri:
"sono alto 0,00182 kilometri" è una frase corretta matematicamente, ma
quasi priva di senso in Italiano. Le persone si misurano in metri e
centimetri, non kilometri. Oppure, che giudizio daremmo di un libro di
ricette che dica: "prendete 0,0003 tonnellate di prosciutto cotto"?
</p>

<p>È così anche per i prezzi. Siccome non ha senso dire di un oggetto:
costa "0,003 euro", anche se magari quello sarebbe il suo prezzo, ecco
che non viene venduto, mentre magari potrebbe interessare ancora a
qualcuno. Le Bricioline si occupano appunto di dare un prezzo alle
cose che non hanno un prezzo agevole in Euro.</p>

<h2>Ma ha senso vendere qualcosa per 0,003 Euro?</h2>

<p>Certo, ha senso, se <b>nello stesso mercato</b> è possibile
comprare qualche altra cosa <b>ad un prezzo simile</b>. Un prezzo è
solo un numero. Non esistono più le monete d'oro o d'argento (se non
per collezionisti). Ricordiamo lo spirito delle Bricioline: non è
quello di guadagnare, ma quello di permettere transazioni che altrove
non potrebbero aver luogo, perché gli altri sistemi hanno delle "tare"
(dei costi fissi) che impediscono, in pratica, la vendita di oggetti
sotto una certa soglia. Impediscono perché è antieconomico vendere un
oggetto per, esempio, 10 centesimi, se ne devo spendere 100
(centesimi) per fare un bonifico su un conto corrente postale, o se ne
devo spendere 60 per un francobollo o 450 per un pacco.</p>

<h2>Le Bricioline come un mercato "stagno"</h2>

<p>Le Bricioline sono come un mercato a camera stagna, senza costi
fissi, senza spedizioni, perciò si possono al suo interno formare dei
prezzi bassi in modo economico. Ossia, entrambe le parti, domanda ed
offerta, possono reputare vantaggioso lo scambio anche a prezzi molto
più bassi che negli altri mercati "aperti" perché nelle Bricioline i
costi fissi sono suddivisi tra tutti i partecipanti al mercato.</p>

<p>Il mercato stagno è possibile perché tutti i partecipanti al
mercato sono vicini fisicamente, in questo modo si eliminano i costi
delle spedizioni.</p>

<p>Invece di portare le cose ad un negozio dell'usato si considerano
come negozi dei punti particolari od anche le case delle persone, così
si eliminano i costi dell'affitto del locale, energia, ecc...</p>

<p>Il sistema delle Bricioline tiene traccia della <b>località</b>
dell'oggetto venduto ed il prezzo si può anche variare in funzione
della località (ad esempio se vendo una lavatrice è ovvio che se la
vengono a ritirare a casa mia avrà un prezzo, se sono io a doverla
portare dal cliente ne avrà un altro..., ma sono sempre persone
vicine).</p>
