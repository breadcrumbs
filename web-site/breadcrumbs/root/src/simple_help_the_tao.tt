[% META title="Le Bricioline, il tao" %]
[% page_title = "Il Tao" %]

<h2> Il tao </h2>

<p>Il Tao (&#964;) è la moneta delle Bricioline. Essa è caratterizzata
da due caratteristiche fondamentali: è in teoria infinitamente
divisibile ed è anche coniabile dagli utenti.
</p>

<h2> Multipli e sottomultipli </h2>

<p>
Il &#964; è simile più al Kilo o al Metro che all'Euro. Infatti essa è
un'unità di misura del valore che può avere un'incertezza, come tutte
le misure.
</p>

<p>
Come per le altre unità di misura esistono i multipli ed i
sottomultipli. in questa tabella elenchiamo i più comuni.
</p>


<style type="text/css">
table{
border-style=outset; 
border-width=3px;
background-color: #fff2f2;
}
td{
border-width=4px;
border-style=outset; 
background-color: #ddd3d3;
}
th
{
border-width=4px;
border-style=groove; 
background-color: #a99494;
}
</style>

<table style="">
<tr>
<th>Simbolo:</th><th>Nome:</th><th>Valore:</th>
</tr>

<tr>
<td>
u&#964;
</td>
<td>
microTao
</td>
<td>
0,000 001 Tao
</td>
</tr>

<tr>
<td>
m&#964;
</td>
<td>
milliTao
</td>
<td>
0,001 Tao
</td>
</tr>

<tr>
<td>
k&#964;
</td>
<td>
kiloTao
</td>
<td>
1000 Tao
</td>
</tr>

<tr>
<td>
M&#964;
</td>
<td>
megaTao
</td>
<td>
1 000 000 Tao
</td>
</tr>


</table>

<h2> Cambio Euro -> Tao e Tao -> Euro </h2>

<p>Attualmente il cambio è fisso ed il Tao equivale almeno in teoria alla
vecchia lira: 1 Euro = 1936,27 Tao.</p>

<h2> Tao convertibili e non </h2>

<p>
Ci sono due tipi diversi di Tao, almeno in questa prima versione delle
Bricioline: quelli convertibili in Euro e quelli non convertibili.
</p>

<p>
Siccome il Tao, come abbiamo già accennato, è creabile dagli utenti,
risulta chiaro che non è direttamente convertibile in Euro, altrimenti
avremmo trovato il modo (illegale) di creare Euro dal nulla.
</p>

<p>
Per capire la differenza che c'è fra un Tao convertibile ed uno no
facciamo un esempio. Una ragazza vuole fare la baby sitter e mette un
annuncio. Chiamiamola Lucia. Ora Lucia decide un prezzo, ad esempio 15
k&#964; all'ora (circa 7,5 Euro). Quando Lucia mette questo annuncio
nelle Bricioline il sistema le chiede due numeri che noi chiamiamo
'fe' (pronunciato "effe-e") ed 'fc' (pronunciato "effe-c"). Di fe ne
parliamo nel capitolo sulla fiducia, ora pensiamo ad fc.
</p>

<h2> La fiducia per avere &#964; non convertibili </h2>

<p>"fc" rappresenta il valore minimo (espresso in bricioBel) della
fiducia che Lucia richiede per ricevere dei &#964; non
convertibili. Ossia, supponiamo che Lucia mette una fc=45bB. Se Lucia
fa la baby sitter a Carla e fra Lucia e Carla c'è una fiducia &gt; 45bB
allora Carla può pagare Lucia con dei &#964; non convertibili, ossia
può creare dei &#964; dal nulla (con un piccolo contributo dell'ordine
del 0,05% al sito) e pagare Lucia con questa banconota creata da
lei. Lucia è obbligata ad accettare la banconota di Carla perché l'ha
specificato quando ha creato l'annuncio.
</p>

<p>
Ora supponiamo che Carla si è trovata bene con Lucia e consiglia alla
sua amica Enrica questa ragazza. Lucia ora va a fare la baby sitter ad
Enrica. Ora, però, fra Lucia ed Enrica ci può essere una fiducia
minore di 45bB e dunque Enrica è costretta a pagare Lucia con dei
&#964; convertibili. Non può dunque crearli dal nulla, ma deve
versare degli Euro sul suo conto, convertirli in &#964; e con quei Tao
pagare Lucia.
</p>

<p>Il sistema tiene traccia della convertibilità o meno dei Tao e
dunque Lucia può cambiare i &#964; ricevuti da Enrica mentre non può
cambiare i &#964; ricevuti da Carla.
</p>